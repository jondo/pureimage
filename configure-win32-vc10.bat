setlocal

@echo off

:: First argument is interpreted as a sequence of build flags
if "%1" == "" (set FLAGS=cpex) else (set FLAGS=%1x)

:: If FLAGS contain the character
::   'm': minimal configuration (ignores all other flags)
::   'c': add core 
::   'p': add plugins
::   'e': add examples
::   'd': add documentation
:: Default is cpa, i.e. core, plugins, and examples without documentation
:: Test mechanism inspired by http://ss64.com/nt/syntax-replace.html

@echo Configuring build-win32-vc10:

set FLAGS_M=%FLAGS:m=%
if NOT %FLAGS%==%FLAGS_M% (
set _M=-D ADD_PI_MINIMAL=ON
set FLAGS=m
@echo   minimal configuration
) else (
set _M=-D ADD_PI_MINIMAL=OFF
)

set FLAGS_C=%FLAGS:c=%
if NOT %FLAGS%==%FLAGS_C% (
set _C=-D ADD_PI_CORE=ON
@echo   with core
) else (
set _C=-D ADD_PI_CORE=OFF
@echo   no core
)

set FLAGS_P=%FLAGS:p=%
if NOT %FLAGS%==%FLAGS_P% (
set _P=-D ADD_PI_PLUGINS=ON
@echo   with plugins
) else (
set _P=-D ADD_PI_PLUGINS=OFF
@echo   no plugins
)

set FLAGS_E=%FLAGS:e=%
if NOT %FLAGS%==%FLAGS_E% (
set _E=-D ADD_PI_EXAMPLES=ON
@echo   with examples
) else (
set _E=-D ADD_PI_EXAMPLES=OFF
@echo   no examples
)

set FLAGS_D=%FLAGS:d=%
if NOT %FLAGS%==%FLAGS_D% (
set _D=-D ADD_PI_DOCUMENTATION=ON
@echo   with documentation
) else (
set _D=-D ADD_PI_DOCUMENTATION=OFF
@echo   no documentation
)

mkdir build-win32-vc10
cd build-win32-vc10

@echo on

cmake -C ..\cmake-win32-libs-vc10.conf -C ..\cmake-win32-tools.conf %_M% %_C% %_P% %_E% %_D%  -G "Visual Studio 10" ..
cd ..

endlocal
