#
# Oliver uses his own eclipse generator which currently is discussed with the developer of CDT generator
# If you want to use it - please ask him (no install scripts yet)
#

export PATH=~/CMake/bin:$PATH
mkdir build-eclipse-CDT7
cd build-eclipse-CDT7
cmake -G "Eclipse CDT7 - Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -DCDT_ECLIPSE_OUTPUT_DIR=/home/oliver/pureImage_eclipse -DUSE_SOLUTION_FOLDERS=ON -DCDT_ENABLE_UTILITY_TARGETS=ON ..
cd ..
