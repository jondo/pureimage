/* ByteImage.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include <PureImage.hpp>
#include <Application.hpp>
#include <Arguments.hpp>

/*	Basic ByteImage example (C++)
	This example demonstrates the different ways how to use the ByteImage
	data type, the different levels of pixel access, and some best practices. */
int main (int argc, char** argv) {

    // 1.	Create a pureImage application.
    pI::Application app;

	// 2.	Load and register the pureImage CImg library wrapper.
    try {
        app.LoadPluginLibrary ("CImg_pIns");
		app.RegisterPlugins();
    } catch (pI::exception::PluginLibraryException& xc) {
        std::cerr << "Failed to load plugin library: " << xc.what() << std::endl;
        return 1;
    }

	// 3. Spawn a display plugin
	boost::shared_ptr<pI::pIn> display =
	 app.SpawnAndInitialize ("CImg/Display", "CImg window", pI_FALSE, pI_FALSE, 0, 0);
    pI::Arguments display_in = display->GetInputSignature();

	// 3.	C interface: create and handle a ByteImage (101x101x3)
	// create an empty argument
	pI::ArgumentPtr carg(
	 app.GetRuntime().GetCRuntime()->AllocateArgument (app.GetRuntime().GetCRuntime()));
	// set argument type
	carg->signature.type = T_pI_Argument_ByteImage;	// ByteImage "id"
	// set description
	carg->description.name = app.GetRuntime().CopyString ("My ByteImage");
	carg->description.description = app.GetRuntime().CopyString ("A hand-created and rather inconvenient C-style ByteImage");
	// let the runtime create memory for argument meta information (the "setup")
	carg->runtime->CreateArgumentSetup (carg->runtime, carg.get());
	// set the type-dependant meta information; the fields are named accordingly to type definition,
	// see pureImage/Core/include/DataTypes/Arguments/ByteImageArgument.def
	carg->signature.setup.ByteImage->width = 101;
	carg->signature.setup.ByteImage->height = 101;
	carg->signature.setup.ByteImage->channels = 3;
	// with the meta information set, the real argument memory can be allocated
	carg->runtime->CreateArgumentData (carg->runtime, carg.get());

	// note: the pitch information is NOT automatically set, the path is not set either
	std::cout << "C ByteImage pitch: " << carg->data.ByteImage->pitch << std::endl;

	// now it is possible to manipulate the image, there are two ways:
	// 3.a	the high performance, I KNOW WHAT I DO way:
	pI_byte* mem_block = &(***(carg->data.ByteImage->data));
	pI_size h, w, c;
	for (h = 0; h < carg->signature.setup.ByteImage->height; ++h) {
		for (w = 0; w < carg->signature.setup.ByteImage->width; ++w) {
			for (c = 0; c < carg->signature.setup.ByteImage->channels; ++c) {
				*mem_block++ = 0;
			}
		}
		// beware the padding bytes (padding is done on level 2 - channels * width - as speficied in ByteImageArgument.def)
		mem_block += 
		 PI_FIELD_PADDING - (carg->signature.setup.ByteImage->width * carg->signature.setup.ByteImage->channels) % PI_FIELD_PADDING;
	}

	display_in[0] = carg;
    display->Execute (display_in);

	// 3.b	the more convenient way:
	for (h = 0; h < carg->signature.setup.ByteImage->height; ++h)
		for (w = 0; w < carg->signature.setup.ByteImage->width; ++w)
			for (c = 0; c < carg->signature.setup.ByteImage->channels; ++c)
				carg->data.ByteImage->data[h][w][c] = c * 50 + 100;
	// note:	this convenience is achieved by artificial pointer fields, which have two MAJOR drawbacks:
	//				- slower access (pointer indirections)
	//				- increased memory consumption
	//			for the time being, there is no possibility to deactivate pointer field creation;
	//			however, it is likely that future releases will provide relevant functionality
	display_in[0] = carg;
    display->Execute (display_in);

	// 4.	Use the C++ ByteImage interface: creation and handling (101x101x3)
	pI::ByteImage img(app.GetRuntime());
	// set description
	img.SetName ("My ByteImage").SetDescription ("My C++ ByteImage");
	// set meta info AND allocate the data
	img.SetWidth (101).SetHeight (101).SetChannels (3).CreateData();

	// note: via C++, the pitch information is automatically set, the path empty
	std::cout << "C++ ByteImage pitch: " << img.GetPitch() << std::endl;

	// manipulate the image
	for (h = 0; h < img.GetHeight(); ++h)
		for (w = 0; w < img.GetWidth(); ++w)
			for (c = 0; c < img.GetChannels(); ++c)
				img.SetData (h, w, c, c * 127);

	display_in[0] = img;
    display->Execute (display_in);

	// 5.	Convenience trick: acquire ByteImage directly from plugin signature
	display_in = display->GetInputSignature();
	pI::ByteImage di(display_in[0]);	// will only work with compatible type
	// name and description are as they were specified in the plugin
	std::cout << "Display: IN[0] - " << di.GetName() << " (" << di.GetDescription() << ")" << std::endl;
	// because the ByteImage came from a signature, there is no created data
	di.SetWidth (101).SetHeight (101).SetChannels (3).CreateData();

    display->Execute (display_in);

    return 0;
}
