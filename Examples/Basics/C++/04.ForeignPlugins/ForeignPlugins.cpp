/* ForeignPlugins.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include <PureImage.hpp>
#include <Application.hpp>
#include <Arguments.hpp>

/*	Basic example ForeignPlugins (C++)
	This example shows how plugins written in other programming languages
	can be used from a C++ pureImage application. */
int main (int argc, char** argv) {

    // 1.	Create a pureImage application.
    pI::Application app;

	if (!app.GetRuntime().HasProperty ("CLASSPATH")) {
		app.GetRuntime().StoreProperty (pI::StringValue(app.GetRuntime()).SetName("CLASSPATH").SetData("."));
	}

	// 2.	Load and register relevant plugin libraries.
	//		Note:   in case the program stops subsequently, most likely the java vm was not installed
	//				correctly or not found; usually adding the java installation folder containing
	//				the jvm shared library suffices.
    try {
        app.LoadPluginLibrary ("CImg_pIns");
		app.LoadPluginLibrary ("Java_pIns");
		app.RegisterPlugins();
    } catch (pI::exception::PluginLibraryException& xc) {
        std::cerr << "Failed to load plugin library: " << xc.what() << std::endl;
        return 1;
    }

	// 3.	Now the necessary C++ plugins can be instantiated.
	//		For this simple example, we need a display and an image creation function.
    boost::shared_ptr<pI::pIn> display =
	 app.SpawnAndInitialize ("CImg/Display", "MirrorX((x + y) / 2)", pI_FALSE, pI_FALSE, 0, 0);
    pI::Arguments display_in = display->GetInputSignature(),
                  display_out = display->GetOutputSignature();
	// image creation plugin from CImg library
	boost::shared_ptr<pI::pIn> createPlugin = app.GetRuntime().SpawnPlugin ("CImg/Create");
    pI::Arguments createPluginIn  = createPlugin->GetInputSignature(),
                  createPluginOut = createPlugin->GetOutputSignature();
	// formula based image creation
	pI::StringValue(createPluginIn[0]).SetData ("(x + y) / 2");
	pI::IntValue(createPluginIn[1]).SetData (255);		// width
	pI::IntValue(createPluginIn[2]).SetData (255);		// height
	pI::IntValue(createPluginIn[3]).SetData (1);		// channels
   
	// 4.	In addition, load the Mirror plugin, implemented in Java.
	//		For an application, the implementation language of a plugin is transparent,
	//		usage looks and behaves exactly like a native language plugin.
	boost::shared_ptr<pI::pIn> mirrorPlugin = 
	 app.SpawnAndInitialize ("pIn/Geometric/MirrorJava", pI_TRUE, pI_FALSE, pI_FALSE);	// flip X, not Y, not channels
	pI::Arguments mirror_out = mirrorPlugin->GetOutputSignature();

    // 5.	Once again, we execute the plugin chain
	createPlugin->Execute (createPluginIn, createPluginOut);
	mirrorPlugin->Execute (createPluginOut, mirror_out);
    display->Execute (mirror_out);

    return 0;
}
