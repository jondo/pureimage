/* PluginLoading.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GNUC__
#include <conio.h>
#else
#include "gcc_conio.h"
#endif

#include <iostream>

#include <PureImage.hpp>
#include <Application.hpp>
#include <Arguments.hpp>


/*	Basic example PluginLoading (C++)
	This example summarizes the necessary steps for a simple pureImage application
	to load, register, instantiate and use plugins. */
int main (int argc, char** argv) {

    // 1.	Create a pureImage application.
	//		Internally, a pureImage runtime is established, 
	//		proper implementation is automatically loaded from shared libraries.
	//		This includes the actual implementation of the pureImage application
	//		itself, therefore no static link library is necessary...
    pI::Application app;

	// 2.	Load a specific plugin library (here CImg_pIns).
	//		The application just needs the name of the library, it used the pureImage
	//		default library path.
	//		Note: in case the CImg_pIns library is not found, an exception is thrown and the
	//			  program terminates.
	//		Note: after a specific library is loaded, it must be registered to the runtime manually.
	//		Note: the pureImage application loads the plugin in a configuration-aware way;
	//			  a debug build will only load debug plugins.
    try {
        app.LoadPluginLibrary ("CImg_pIns");
    } catch (pI::exception::PluginLibraryException& xc) {
        std::cerr << "Failed to load plugin library: " << xc.what() << std::endl;
        return 1;
    }

	std::cout << "When a specific plugin library is loaded, it is not automatically registered to the runtime - therefore the plugin count is zero (" 
			  << app.GetRuntime().GetCRuntime()->plugin_count << ")." << std::endl;

    //	Because of potential plugin dependencies, it is necessary to manually 
	//	register loaded plugins to the runtime.
	app.RegisterPlugins();

	std::cout << "The situation changes after manual registration - the loaded library contains " 
			  << app.GetRuntime().GetCRuntime()->plugin_count << " plugins." << std::endl;

	// 3.	Load all available plugin libraries.
	//		For convenience, it is also possible to load all available plugins at once.
	//		In this case, the relevant function defaults to auto-registration, so no
	//		manual registration call is necessary.
	//		Note: the pureImage application loads the plugins in a configuration-aware way;
	//			  a debug build will only load debug plugins.
	app.LoadPluginLibraries();

	std::cout << "After the loading of all available plugin libraries, the runtime knows " 
			  << app.GetRuntime().GetCRuntime()->plugin_count 
			  << " plugins, registrated autmatically." << std::endl;

    getch();

    return 0;
}
