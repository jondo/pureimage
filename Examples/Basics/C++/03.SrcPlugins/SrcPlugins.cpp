/* SrcPlugins.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include <PureImage.hpp>
#include <Application.hpp>
#include <Arguments.hpp>
#include <ArgumentChecks.hpp>

/*	declare and implement a puerImage plugin:
	the following implementation accepts a ByteImage and inverts its colors.
	The inverted image is returned as a copy.
*/
namespace pI {
namespace pIns {

class Invert: public pIn {

    typedef pIn Base;

public:

	Invert(Runtime runtime): Base(runtime) {}
	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Invert)
	virtual ~Invert() {}

    virtual const pI_int GetpInVersion() const { return 10000; }
	virtual const std::string GetAuthor() const { return "JKU Linz"; }
	virtual const std::string GetDescription() const {
        return "Inverts image colors.";
    }
    virtual const std::string GetName() const { return "Invert"; }

    virtual Arguments GetParameterSignature() const { return Arguments(); }

    virtual Arguments GetInputSignature() const {
        Arguments is;
		is.push_back (ByteImage(GetRuntime()).SetName ("image").SetDescription ("input image"));
		return is;
    }

    virtual Arguments GetOutputSignature() const {
        Arguments os;
		os.push_back (ByteImage(GetRuntime()).SetName ("image").SetDescription ("output image"));
		return os;
    }

protected:

	virtual void _Initialize (const Arguments& parameters) {}

    virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		CheckInputArguments (input_args, GetInputSignature());
		CheckOutputArguments (output_args, GetOutputSignature());

		output_args[0] = GetRuntime().CopyArgument (input_args[0]);
		ByteImage output(output_args[0]);
		// simple invert implementation
		for (pI_size y = 0; y < output.GetHeight(); ++y)
			for (pI_size x = 0; x < output.GetWidth(); ++x)
				for (pI_size c = 0; c < output.GetChannels(); ++c)
					output.SetData (y, x, c, 255 - output.GetData (y, x, c));
	}

}; // class Invert: public pIn

} // namespace pIns
} // namespace pI

/*	prevent shared-ptr deallocation as described in 
	http://www.boost.org/doc/libs/1_49_0/libs/smart_ptr/sp_techniques.html
	necessary for plugin registration, as the registration method takes ownership.
*/
struct null_deleter {
    void operator()(void const *) const {}
};


/*	Basic example SrcPlugins (C++)
	This examples shows how source-code plugins can be used with pureImage. */
int main (int argc, char** argv) {

    // 1.	Create a pureImage application.
    pI::Application app;

	// 2.	Load and register the pureImage CImg library wrapper.
    try {
        app.LoadPluginLibrary ("CImg_pIns");
		app.RegisterPlugins();
    } catch (pI::exception::PluginLibraryException& xc) {
        std::cerr << "Failed to load plugin library: " << xc.what() << std::endl;
        return 1;
    }

	// 3.	Create the source-code plugin from above
	// 3.a	Create an instance right from scratch via source-code access
	pI::pIns::Invert inv(app.GetRuntime());
	pI::Arguments invOut = inv.GetOutputSignature();

	// 3.b	Register the plugin to the runtime and spawn it from there
	//		This variant allows other plugins to use it right after registration
	//		Note:	plugin registration method takes plugin ownership, therefore 
	//				deletion from outside is prevented - null_deleter
	app.GetRuntime().RegisterPlugin (
	 pI::pInPtr(new pI::pIns::Invert(app.GetRuntime()), null_deleter()));
	boost::shared_ptr<pI::pIn> invertPlugin = app.GetRuntime().SpawnPlugin ("Invert");

	// 4.	Now the necessary plugins can be instantiated.
	//		For this simple example, we need a display and an image creation function.
    boost::shared_ptr<pI::pIn> display =
	 app.SpawnAndInitialize ("CImg/Display", "!(!((x * y) % 255))", pI_FALSE, pI_FALSE, 0, 0);
    pI::Arguments display_in = display->GetInputSignature(),
                  display_out = display->GetOutputSignature();
	// image creation plugin from CImg library
	boost::shared_ptr<pI::pIn> createPlugin = app.GetRuntime().SpawnPlugin ("CImg/Create");
    pI::Arguments createPluginIn  = createPlugin->GetInputSignature(),
                  createPluginOut = createPlugin->GetOutputSignature();
	// formula based image creation
	pI::StringValue(createPluginIn[0]).SetData ("(x * y) % 255");
	pI::IntValue(createPluginIn[1]).SetData (255);		// width
	pI::IntValue(createPluginIn[2]).SetData (255);		// height
	pI::IntValue(createPluginIn[3]).SetData (1);		// channels

	// 5.	Now we execute the plugin chain
	createPlugin->Execute (createPluginIn, createPluginOut);
	// execute "scratch" invert-instance
	inv.Execute (createPluginOut, invOut);
	// execute plugin invert-instance
	invertPlugin->Execute (invOut, invOut);
	// show the resulting image (which matches the original after inverting two times)
    display->Execute (invOut);

    return 0;
}
