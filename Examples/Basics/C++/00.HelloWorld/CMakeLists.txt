include_directories(
    ${Boost_INCLUDE_DIRS}
    ${PI_CPP_INCLUDE_DIRS}
)

add_executable(ExampleBasicsCpp_00_HelloWorld HelloWorld.cpp)

set_target_properties(ExampleBasicsCpp_00_HelloWorld PROPERTIES
            COMPILE_FLAGS "-DpI_USE_SHARED_LIBRARY"
            FOLDER "Examples/Basics/C++")

# sadly, there is no unix CRT to provide the implementation for shared library loading
if(NOT(WIN32))
target_link_libraries(ExampleBasicsCpp_00_HelloWorld
    dl
)
endif()

add_dependencies(ExampleBasicsCpp_00_HelloWorld pIRuntime pICppRuntimeImpl pIApplicationImpl CImg_pIns)
