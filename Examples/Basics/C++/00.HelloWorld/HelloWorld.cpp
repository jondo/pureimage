/* HelloWorld.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include <PureImage.hpp>
#include <Application.hpp>
#include <Arguments.hpp>

/*	Basic example HelloWorld (C++)
	This example summarizes the necessary steps for a simple pureImage application.
	In principle, a pureImage application instance is created that enables the
	usage of pureImage plugins.
	Here, two plugins are used, one to write text in an image, the other to display
	an image, in order to greet the world. */
int main (int argc, char** argv) {

    // 1.	Create a pureImage application.
	//		Internally, a pureImage runtime is established, 
	//		proper implementation is automatically loaded from shared libraries.
	//		This includes the actual implementation of the pureImage application
	//		itself, therefore no static link library is necessary...
    pI::Application app;

	// 2.	Load the pureImage CImg library wrapper.
	//		The application just needs the name of the library, it used the pureImage
	//		default library path.
	//		Note: in case the CImg wrapper is not found, an exception is thrown and the
	//			  program terminates.
    try {
        app.LoadPluginLibrary ("CImg_pIns");
    } catch (pI::exception::PluginLibraryException& xc) {
        std::cerr << "Failed to load plugin library: " << xc.what() << std::endl;
        return 1;
    }

    // 3.	Because of potential plugin dependencies, it is necessary to manually 
	//		register loaded plugins to the runtime.
	app.RegisterPlugins();

	// 4.	Now the necessery plugins can be instantiated.
	//		For this simple example, we need a display and a draw text function.
	//		For shorter code, we spawn and initialize relevant plugins in one step.
    boost::shared_ptr<pI::pIn> display =
	 app.SpawnAndInitialize ("CImg/Display", "CImg window", pI_FALSE, pI_FALSE, 0, 0);
    pI::Arguments display_in = display->GetInputSignature(),
                  display_out = display->GetOutputSignature();

	boost::shared_ptr<pI::pIn> textPlugin =
     app.SpawnAndInitialize ("CImg/Functions", 72);		// the draw text function has index 72
    pI::Arguments textPluginIn  = textPlugin->GetInputSignature(),
                  textPluginOut = textPlugin->GetOutputSignature();
   
	// 5.	Almost there - we just need to setup an image, a font size and a text to write
	pI::ByteImage (textPluginIn[0]).SetWidth (200).SetHeight (30).SetChannels (3).CreateData();
	pI::IntValue (textPluginIn[8]).SetData (30);		// font size 30
	pI::StringValue (textPluginIn[1]).SetData ("Hello pureImage!");

    // 6.	Finally, we plot the text into the image by executing the draw text plugin,
	//		and display the results, again via plugin
	textPlugin->Execute (textPluginIn, textPluginOut);
    display->Execute (textPluginOut);

    return 0;
}
