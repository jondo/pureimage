/* PluginWorkflow.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include <PureImage.hpp>
#include <Application.hpp>
#include <Arguments.hpp>

/*	Basic example PluginWorkflow (C++)
	This example summarizes the necessary steps for plugin usage:
	- plugin instantiation 
	- parameter signature query
	- plugin initialization
	- plugin I/O signature query
	- plugin execution
	- plugin deletion */
int main (int argc, char** argv) {

    // 1.	Create a pureImage application.
    pI::Application app;

	// 2.	Load and register the pureImage CImg library wrapper.
    try {
        app.LoadPluginLibrary ("CImg_pIns");
    } catch (pI::exception::PluginLibraryException& xc) {
        std::cerr << "Failed to load plugin library: " << xc.what() << std::endl;
        return 1;
    }

    // 3.	Manual plugin registration
	app.RegisterPlugins();

	// 4.	Now we already have multiple plugins, we will write their names to the console
	//		Note: atm., we do not have any nice iterator, so the inconvenit CRuntime must be used
	for (pI_size lv = 0; lv < app.GetRuntime().GetCRuntime()->plugin_count; ++lv) {
		std::cout << "plugin [" << lv << "]: " << app.GetRuntime().GetCRuntime()->plugins[lv]->name << std::endl;
	}

	// 5.	First, we will create a CImg/Display plugin instance
	boost::shared_ptr<pI::pIn> display =
	 app.GetRuntime().SpawnPlugin ("CImg/Display");
	if (display == 0) {
		std::cerr << "Failed to spawn necessary plugin." << std::endl;
        return 1;
	}

	// 6.	Then, we query the parameters for plugin initialization
	pI::Arguments display_params = display->GetParameterSignature();
	std::cout << std::endl << "CImg/Display parameters: " << std::endl;
	for (pI_size lv = 0; lv < display_params.size(); ++lv) {
		std::cout << " param[" << lv << "]: " << display_params[lv]->description.name << std::endl
				  << " \t" << display_params[lv]->description.description << std::endl;
	}

	// 7.	We fill the initialization parameters with desired values
	//		Note: we use C++ argument wrappers for initialization
	pI::StringValue(display_params[0]).SetData ("My window title");	// name
	pI::BoolValue(display_params[1]).SetData (pI_FALSE);			// enable live
	pI::BoolValue(display_params[2]).SetData (pI_FALSE);			// reserved
	pI::IntValue(display_params[3]).SetData (100);					// x
	pI::IntValue(display_params[4]).SetData (100);					// y

	// 8.	Plugin initialization with filled signature
	display->Initialize (display_params);

	// 9.	Query I/O argument signatures
	pI::Arguments display_in = display->GetInputSignature();
	std::cout << std::endl << "CImg/Display input signature: " << std::endl;
	for (pI_size lv = 0; lv < display_in.size(); ++lv) {
		std::cout << " Input[" << lv << "]: " << display_in[lv]->description.name << std::endl
				  << " \t" << display_in[lv]->description.description << std::endl;
	}
	pI::Arguments display_out = display->GetOutputSignature();
	std::cout << std::endl << "CImg/Display output signature: " << std::endl;
	for (pI_size lv = 0; lv < display_out.size(); ++lv) {
		std::cout << " Output[" << lv << "]: " << display_out[lv]->description.name << std::endl
				  << " \t" << display_out[lv]->description.description << std::endl;
	}

	// 10.	Fill input signature and execute plugin
	pI::ByteImage(display_in[0]).SetWidth(640).SetHeight(480).SetChannels(1).CreateData();
	display->Execute (display_in, display_out);

    return 0;
}
