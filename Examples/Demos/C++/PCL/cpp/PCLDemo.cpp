/* PCLTest.cpp
*
* Copyright 2011 Johannes Kepler Universität Linz,
* Institut für Wissensbasierte Mathematische Systeme.
*
* This file is part of pureImage.
*
* pureImage is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License, version 3,
* as published by the Free Software Foundation.
*
* pureImage is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty
* of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with pureImage. If not, see <http://www.gnu.org/licenses/>.
*/

#define NOMINMAX

#if 1
#ifndef __GNUC__
#include <conio.h>
#else
#include "gcc_conio.h"
#endif
#endif

#include <cmath>
#include <string>

#include <boost/shared_array.hpp>
#include <boost/lexical_cast.hpp>

#include <Poco/Path.h>
#include <Poco/Thread.h>

#include <PureImage.hpp>
#include <Application.hpp>
#include <Arguments.hpp>
#include <Arguments/IntValue.hpp>
#include <Arguments/StringSelection.hpp>
#include <Arguments/ShortMatrix.hpp>

#include <pcl/point_types.h>

#include <pcl/common/transform.h>

#include <pcl/io/pcd_io.h>

#include <pcl/features/normal_3d.h>

#include <pcl/filters/filter.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/conditional_removal.h>

#include <pcl/kdtree/kdtree.h>

#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/sample_consensus/ransac.h>
#include <pcl/sample_consensus/sac_model_circle.h>

#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <pcl/visualization/cloud_viewer.h>


using pcl::operator<<;


double norm (const pcl::PointXYZ& p) {
    return sqrt (p.x * p.x + p.y * p.y + p.z * p.z);
}

double euclidian (const pcl::PointXYZ& p, const pcl::PointXYZ& q) {
    pcl::PointXYZ r (p.x - q.x, p.y - q.y, p.z - q.z);
    return norm (r);
}


void save (const std::string& name, const pcl::PointCloud<pcl::PointXYZ>& cloud) {
    std::cout << "Saving " << name << "..." << std::endl;
    pcl::io::savePCDFileASCII (name, cloud);
}


pcl::PointCloud<pcl::PointXYZ> threeDimensionalize(const pcl::PointCloud<pcl::PointXY>& cloud, char posx, char posy) {
    pcl::PointCloud<pcl::PointXYZ> result;
    pcl::PointCloud<pcl::PointXY>::const_iterator it;

    for (it = cloud.begin(); it != cloud.end(); ++it) {
        pcl::PointXYZ p;
        p.x = p.y = p.z = 0.;

        switch (posx) {
            case 'x':
                p.x = it->x;
                break;
            case 'y':
                p.y = it->x;
                break;
            case 'z':
                p.z = it->x;
                break;
            default:
                break;
        }

        switch (posy) {
            case 'x':
                p.x = it->y;
                break;
            case 'y':
                p.y = it->y;
                break;
            case 'z':
                p.z = it->y;
                break;
            default:
                break;
        }

        result.push_back (p);
    }

    return result;
}


// Several methods how to convert Kinect depth to real world coordinates
// are suggested throughout the web.

// The most common method, mentioned for instance at
// http://nicolas.burrus.name/index.php/Research/KinectCalibration,
// is to take the inverse of a linear function of the depth.
double RawDepthToMeters_InvLinear (int depthValue) {
    const double k1 = 3.3309495161;
    const double k2 = -0.0030711016;

    if (depthValue < 2047) {
        return (1.0 / (k1 + k2 * (double) depthValue));
    }

    return 0.0;
}

// In http://tinyurl.com/6fxflsp, Stephane Magnenat mentions that using
// tan() is slightly better than the inverse.
double RawDepthToMeters_Tan1 (int depthValue) {
    const double k1 = 1.1863;
    const double k2 = 2842.5;
    const double k3 = 0.1236;

    if (depthValue < 2047) {
        return (k3 * tan (k1 + (double) depthValue / k2));
    }

    return 0.0;
}

//In http://vvvv.org/forum/the-kinect-thread, user marf reports this formula:
double RawDepthToMeters_Tan2 (int depthValue) {
    const double k1 = 0.5;
    const double k2 = 1024.;
    const double k3 = 33.825;
    const double k4 = 5.7;

    if (depthValue < 2047) {
        return 0.01 * (k3 * tan (k1 + (double) depthValue / k2) + k4);
    }

    return 0.0;
}

double RawDepthToMeters (int depthValue) {
    return RawDepthToMeters_Tan1 (depthValue);
}


//The following functionis mentioned at http://graphics.stanford.edu/~mdfisher/Kinect.html
pcl::PointXYZ DepthToWorld (int x, int y, int depthValue) {
    const double fx_d = 1.0 / 5.9421434211923247e+02;
    const double fy_d = 1.0 / 5.9104053696870778e+02;
    const double cx_d = 3.3930780975300314e+02;
    const double cy_d = 2.4273913761751615e+02;

    pcl::PointXYZ result;
    double depth = RawDepthToMeters (depthValue);
    result.x = (float) ( (x - cx_d) * depth * fx_d);
    result.y = (float) ( (y - cy_d) * depth * fy_d);
    result.z = (float) (depth);
    return result;
}


/// Loads and registers plugin libraries
bool LoadPlugins (pI::Application& app) {

    /*app.LoadPluginLibrary (app.FindPIPluginDir().c_str(), "CImg_pIns");
    app.LoadPluginLibrary (app.FindPIPluginDir().c_str(), "FLLL_pIns");
    app.LoadPluginLibrary (app.FindPIPluginDir().c_str(), "OpenCV_pIns");*/
    app.LoadPluginLibrary (app.FindPIPluginDir().c_str(), "Freenect_pIns");

    app.RegisterPlugins();
    return true;
}


int main (int argc, char** argv) {

    pcl::PointCloud<pcl::PointXYZ>::Ptr source (new pcl::PointCloud<pcl::PointXYZ>);

    bool GetLiveCloud = true; // If true, obtain source cloud from Kinect device;
                              // otherwise, read source cloud from file.

    if(GetLiveCloud) {

        pI::Application app;
        pI::Runtime runtime (app.GetRuntime());
        LoadPlugins (app);

        // Creates and initializes all the necessary plugins.
        const int deviceIndex = 0;

        // Freenect/GetVideoAndDepth returns both the RGB image and the depth map captured by a Kinect.
        boost::shared_ptr<pI::pIn> getImageAndDepth
        = app.SpawnAndInitialize ("Freenect/GetVideoAndDepth",
                                  deviceIndex,
                                  0,
                                  0);
        pI::Arguments getImageAndDepth_in = getImageAndDepth->GetInputSignature(),
                      getImageAndDepth_out = getImageAndDepth->GetOutputSignature();

        // Freenect/SetTiltAndLED sets the tilt angle and LED status of a Kinect.
        boost::shared_ptr<pI::pIn> setTiltAndLED = app.SpawnAndInitialize ("Freenect/SetTiltAndLED", deviceIndex);
        pI::Arguments setTiltAndLED_in = setTiltAndLED->GetInputSignature(),
                      setTiltAndLED_out = setTiltAndLED->GetOutputSignature();

        pI::IntValue newTilt (setTiltAndLED_in[0]);
        pI::IntValue newLED (setTiltAndLED_in[1]);

        newTilt.SetData (-20); // Tilt angle -20 degree
        newLED.SetData (1); // LED status Green

        setTiltAndLED->Execute (setTiltAndLED_in, setTiltAndLED_out);

        Poco::Thread::sleep(5000);

        getImageAndDepth->Execute (getImageAndDepth_in, getImageAndDepth_out);

        pI::ShortMatrix depth (getImageAndDepth_out[1]);
        int width = depth.GetCols();
        int height = depth.GetRows();

        for (int j = 0; j < height; ++j) {
            for (int i = 0; i < width; ++i) {
                int dep = depth.GetData (j, i);

                if (dep < 2048) {
                    source->push_back (DepthToWorld (i, j, dep));
                }
            }
        }

        newTilt.SetData (0); // Tilt angle 0 degree
        newLED.SetData (0); // LED status off

        setTiltAndLED->Execute (setTiltAndLED_in, setTiltAndLED_out);
    } 
    else {
        std::string dir ("D:\\data\\pureImage\\");
        std::string name ("PointCloud_2011-08-01_10-40-00");
        std::string ext (".pcd");

        std::cout << "Loading " << dir + name + ext << "..." << std::endl;
        pcl::io::loadPCDFile (dir + name + ext, *source);
    }

    std::cout << "Source cloud: " << source->size() << " points." << std::endl;

    // 1. Apply voxel filter to reduce data size
    const bool doVoxelFilter = true;
    pcl::PointCloud<pcl::PointXYZ>::Ptr voxelFiltered (new pcl::PointCloud<pcl::PointXYZ>);

    if (doVoxelFilter) {
        pcl::VoxelGrid<pcl::PointXYZ> vxl;
        vxl.setInputCloud (source);
        vxl.setLeafSize (0.005, 0.005, 0.005);
        vxl.filter (*voxelFiltered);

        std::cout << "Voxel filtered cloud: " << voxelFiltered->size() << " points." << std::endl;
    } else {
        voxelFiltered = source;
    }

    // 2. Remove points further than 95 cm away from sensor
    const float maxDistance = 0.95;

    pcl::PointIndices::Ptr nearPoints (new pcl::PointIndices ());
    nearPoints->indices.clear();

    for (int i = 0; i < voxelFiltered->size(); ++i) {
        if (norm (voxelFiltered->at (i)) <= maxDistance) {
            nearPoints->indices.push_back (i);
        }
    }

    pcl::ExtractIndices<pcl::PointXYZ> reduce;
    pcl::PointCloud<pcl::PointXYZ>::Ptr reduced (new pcl::PointCloud<pcl::PointXYZ>);

    reduce.setInputCloud (voxelFiltered);
    reduce.setIndices (nearPoints);
    reduce.setNegative (false);
    reduce.filter (*reduced);

    std::cout << "Pass filtered cloud: " << reduced->size() << " points." << std::endl;

    // In the sequel, the source cloud is split up into a plane and several objects.
    std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> parts;
    std::vector<pcl::PointIndices::Ptr> inliers;

    // 3. Estimate table plane with RANSAC
    pcl::ModelCoefficients::Ptr planeCoeffs (new pcl::ModelCoefficients);
    Eigen::VectorXf plane_coeffs;
    inliers.push_back (pcl::PointIndices::Ptr (new pcl::PointIndices()));

#if 1
    // Estimate point normals
    pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
    pcl::KdTreeFLANN<pcl::PointXYZ>::Ptr tree (new pcl::KdTreeFLANN<pcl::PointXYZ> ());
    ne.setSearchMethod (tree);
    ne.setInputCloud (reduced);
    ne.setKSearch (50);

    pcl::PointCloud<pcl::Normal>::Ptr normals (new pcl::PointCloud<pcl::Normal>);
    ne.compute (*normals);

    pcl::SACSegmentationFromNormals<pcl::PointXYZ, pcl::Normal> planeSegmentation;
    planeSegmentation.setOptimizeCoefficients (true);
    planeSegmentation.setModelType (pcl::SACMODEL_NORMAL_PLANE);
    planeSegmentation.setNormalDistanceWeight (0.1);
    planeSegmentation.setMethodType (pcl::SAC_RANSAC);
    planeSegmentation.setMaxIterations (100);
    planeSegmentation.setDistanceThreshold (0.05);
    planeSegmentation.setInputCloud (reduced);
    planeSegmentation.setInputNormals (normals);

    planeSegmentation.segment (*inliers.back(), *planeCoeffs);

    float a = planeCoeffs->values[0];
    float b = planeCoeffs->values[1];
    float c = planeCoeffs->values[2];
    float d = planeCoeffs->values[3];
#else
    pcl::SampleConsensusModelPlane<pcl::PointXYZ>::Ptr planeModel (
        new pcl::SampleConsensusModelPlane<pcl::PointXYZ> (reduced));

    pcl::RandomSampleConsensus<pcl::PointXYZ> ransac (planeModel);
    ransac.setMaxIterations (10000);
    ransac.setDistanceThreshold (0.05);
    ransac.computeModel();

    ransac.getModelCoefficients (plane_coeffs);
    float a = -plane_coeffs (0);
    float b = -plane_coeffs (1);
    float c = -plane_coeffs (2);
    float d = -plane_coeffs (3);

    ransac.getInliers (inliers.back()->indices);
#endif

    // 4. Rotate scene s.t. mugs are oriented upright
    Eigen::Vector3f x_axis (b / sqrt (a * a + b * b), -a / sqrt (a * a + b * b), 0.);
    Eigen::Vector3f y_direction (a, b, c);

    Eigen::Affine3f rotation = Eigen::Affine3f::Identity();
    rotation = pcl::getTransFromUnitVectorsXY (x_axis, y_direction);

    std::cout << "Plane coefficients: " << a << " " << b << " " << c << " " << d << " "
              << "rotation: " << rotation << std::endl;

    pcl::PointCloud<pcl::PointXYZ>::Ptr rotated (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::getTransformedPointCloud (*reduced, rotation, *rotated);

    pcl::ExtractIndices<pcl::PointXYZ> split;
    split.setInputCloud (rotated);
    split.setIndices (inliers.back());

    // 5. Remove all points belonging to the table
    parts.push_back (pcl::PointCloud<pcl::PointXYZ>::Ptr (new pcl::PointCloud<pcl::PointXYZ> ()));
    split.setNegative (false);
    split.filter (*parts.back());

    pcl::PointCloud<pcl::PointXYZ>::Ptr rotatedWithoutTable (new pcl::PointCloud<pcl::PointXYZ> ());
    split.setNegative (true);
    split.filter (*rotatedWithoutTable);

    std::cout << "Cloud without plane has " << rotatedWithoutTable->size() << " points";

    // 6. Divide remaining points into clusters
    pcl::KdTree<pcl::PointXYZ>::Ptr tree2 (new pcl::KdTreeFLANN<pcl::PointXYZ>);
    tree2->setInputCloud (rotatedWithoutTable);

    pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
    ec.setClusterTolerance (0.05); // in cm
    ec.setMinClusterSize (5);
    ec.setMaxClusterSize (rotatedWithoutTable->size());
    ec.setSearchMethod (tree2);
    ec.setInputCloud (rotatedWithoutTable);

    std::vector<pcl::PointIndices> clusters;
    ec.extract (clusters);

    std::cout << " divided into " << clusters.size() << " clusters." << std::endl;

    for (int c = 0; c < clusters.size(); ++c) {
        inliers.push_back (pcl::PointIndices::Ptr (new pcl::PointIndices (clusters[c])));

        split.setInputCloud (rotatedWithoutTable);
        split.setIndices (inliers.back());

        parts.push_back (pcl::PointCloud<pcl::PointXYZ>::Ptr (new pcl::PointCloud<pcl::PointXYZ> ()));
        split.setNegative (false);
        split.filter (*(parts.back()));
    }

    std::vector<pcl::PointCloud<pcl::PointXY>::Ptr> horizontal, vertical;

    // 7. For all clusters with  at least 100 points:
    for (int p = 1; p < parts.size(); ++p) {
        horizontal.push_back (pcl::PointCloud<pcl::PointXY>::Ptr (new pcl::PointCloud<pcl::PointXY> ()));
        vertical.push_back (pcl::PointCloud<pcl::PointXY>::Ptr (new pcl::PointCloud<pcl::PointXY> ()));

        if(parts[p]->size() >= 100) {
            pcl::PointCloud<pcl::PointXYZ>::const_iterator it;

            float minZ = FLT_MAX, maxZ = -FLT_MAX;

            // a) project horizontally => height
            // b) project vertically to obtain circle
            for (it = parts[p]->begin(); it != parts[p]->end(); ++it) {
                pcl::PointXY pxy;
                pxy.x = it->x;
                pxy.y = it->z;
                horizontal.back()->push_back (pxy);
                pxy.y = it->y;
                vertical.back()->push_back (pxy);

                minZ = std::min (minZ, it->z);
                maxZ = std::max (maxZ, it->z);
            }

            /*save (dir + name +  std::string ("_horizontal_" + boost::lexical_cast<std::string> (p)) + ext,
                  threeDimensionalize(*horizontal.back(), 'x', 'z'));

            save (dir + name +  std::string ("_vertical_" + boost::lexical_cast<std::string> (p)) + ext,
                  threeDimensionalize(*vertical.back(), 'x', 'y'));*/

            // c) estimate circle parameters with RANSAC => radius
            pcl::SampleConsensusModelCircle2D<pcl::PointXYZ>::Ptr circleModel (
                new pcl::SampleConsensusModelCircle2D<pcl::PointXYZ> (threeDimensionalize(*horizontal.back(), 'x', 'y').makeShared()));

            pcl::RandomSampleConsensus<pcl::PointXYZ> ransac (circleModel);
            ransac.setMaxIterations (1000);
            ransac.setDistanceThreshold (0.05);
            ransac.computeModel();

            Eigen::VectorXf circleCoeffs;
            ransac.getModelCoefficients (circleCoeffs);

            std::vector<int> inliers;
            ransac.getInliers (inliers);

            Eigen::VectorXf circleCoeffsRefined;
            circleModel->optimizeModelCoefficients (inliers, circleCoeffs, circleCoeffsRefined);

            // d) radius, height => volume
            std::cout << "minZ/maxZ: " << minZ << " " << maxZ << std::endl;
            std::cout << "Circle coefficients: " << circleCoeffsRefined << std::endl;

            float volume = circleCoeffsRefined(2) * circleCoeffsRefined(2) * M_PI * (maxZ - minZ);
            std::cout << "Cylinder volume = " << 1.e6 * volume << " cm^3" << std::endl << std::endl;
        }
    }

    pcl::visualization::CloudViewer viewer ("Cloud Viewer");

    for (int p = 0; p < parts.size(); ++p) {
        viewer.showCloud (parts[p], "part" + boost::lexical_cast<std::string> (p)); // HACK Primitive hack to get alternative colouring
        viewer.showCloud (parts[p], "part" + boost::lexical_cast<std::string> (p));
        std::cout << "part " << p << ": " << parts[p]->size() << std::endl;
    }

    while (!viewer.wasStopped ()) {
    }
}
