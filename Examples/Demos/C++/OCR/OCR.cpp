/* OCR.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdio>
#include <cstring>

#include <string>
#include <iostream>

#include <PureImage.hpp>
#include <Application.hpp>
#include <Arguments.hpp>


int main (int argc, char** argv) {

    std::cout << "Hello OCR!" << std::endl;

    // Creates runtime and base app
    pI::Application app;
    pI::Runtime runtime (app.GetRuntime());

    try {
        app.LoadPluginLibrary (app.FindPIPluginDir().c_str(), "OpenCV_pIns");
        app.LoadPluginLibrary (app.FindPIPluginDir().c_str(), "CImg_pIns");
    }
    catch (pI::exception::PluginLibraryException& xc) {
        std::cerr << "Error in LoadPluginLibrary(): " << xc.what() << std::endl;
        return 1;
    }

    // Text recognition by Tesseract is optional
    bool hasTextRecognition = false;
    try {
        app.LoadPluginLibrary ("Tesseract_pIns");
        hasTextRecognition = true;
    }
    catch (pI::exception::PluginLibraryException& xc) {
        std::cout << "No text recognition. (" << xc.what() << ")" << std::endl;
    }
    
    int nPlugins = app.RegisterPlugins();

    std::string baseDir = app.FindPIBaseDir();

	/*
    std::vector<std::string> pluginNames = app.GetPluginNames();
    std::vector<std::string> pluginDescr = app.GetPluginDescriptions();

    std::cout << nPlugins << " plugins were loaded from " << baseDir << ":" << std::endl;

    for (int i = 0; i < pluginNames.size(); i++) {
        std::cout << pluginNames[i] << " - " << pluginDescr[i] << std::endl;
    }
	*/

    // Creates and initializes all the necessary plugins:

    // OpenCV/IO/LoadImage loads images from disc.
    boost::shared_ptr<pI::pIn> load (runtime.SpawnPlugin ("OpenCV/IO/LoadImage"));
    pI::Arguments load_params = load->GetParameterSignature();
    load->Initialize (load_params);
    pI::Arguments load_in  = load->GetInputSignature(),
                  load_out = load->GetOutputSignature();

    boost::shared_ptr<pI::pIn> recognize;
    pI::Arguments recognize_in, recognize_out;

    boost::shared_ptr<pI::pIn> puttext;
    pI::Arguments puttext_in, puttext_out;

    if (hasTextRecognition) {
        // Tesseract/RecognizePage performs OCR based on the Tesseract library.
        recognize = runtime.SpawnPlugin ("Tesseract/RecognizePage");
        pI::Arguments recognize_params = recognize->GetParameterSignature();
        recognize->Initialize (recognize_params);
        recognize_in = recognize->GetInputSignature();
        recognize_out = recognize->GetOutputSignature();

        // OpenCV/Draw/PutText draws a text string into an image.
        puttext = app.SpawnAndInitialize (
                                  "OpenCV/Draw/PutText",
                                  0,                // font type: CV_FONT_HERSHEY_SIMPLEX
                                  pI_FALSE,         // render font italic: no
                                  1.1, 1.1,         // horizontal and vertical scale
                                  0.0,              // shear: no shear
                                  2,                // thickness of the text strokes: 2
                                  2                 // type of the line: CV_AA
                                 );
        puttext_in = puttext->GetInputSignature();
        puttext_out = puttext->GetOutputSignature();
    }
    
    // CImg/Display displays the resulting RGB image
    boost::shared_ptr<pI::pIn> display (runtime.SpawnPlugin ("CImg/Display"));
    pI::Arguments display_params = display->GetParameterSignature();
    display->Initialize (display_params);
    pI::Arguments display_in = display->GetInputSignature(),
                  display_out = display->GetOutputSignature();

    // Now that all plugins are initialized, executes them one after the other:

    // Reads an RGB image, ...
    std::string imgFile = baseDir + "/Examples/Resources/Images/HelloWorld_Courier.jpg";

    pI::StringValue path (load_in[0]);
    path.SetData (const_cast<pI_str> (imgFile.c_str()));

    try {
        load_out = load->Execute (load_in);
    }
    catch (pI::exception::Exception& xc) {
        std::cerr << "Error in load->Execute(): " << xc.what() << std::endl;
        return 1;
    }

    if (hasTextRecognition) {
        // ..., applies English recognition on the filtered image, ...
        try {
            recognize_out = recognize->Execute (load_out);
        }
        catch (pI::exception::Exception& xc) {
            std::cerr << "Error in recognize->Execute(): " << xc.what() << std::endl;
            return 1;
        }

        // ..., splits the text into lines, and draws each line onto the image, ...
        pI::StringValue puttext_text (puttext_in[1]);
        pI::IntValue (puttext_in[2]).SetData (160); // X position is fixed
        pI::IntValue puttext_y (puttext_in[3]);     // Y position varies for each line
        pI::IntValue (puttext_in[4]).SetData (0);   // Text color, R value
        pI::IntValue (puttext_in[5]).SetData (0);   // Text color, G value
        pI::IntValue (puttext_in[6]).SetData (128); // Text color, B value
        pI::BoolValue (puttext_in[7]).SetData (pI_TRUE); // Render the text

        puttext_in[0] = load_out[0];

        puttext_y.SetData (920);
        puttext_text.SetData ("Text recognized by the Tesseract/RecognizePage plugin:");

        puttext_out = puttext->Execute (puttext_in);

        char* str = pI::StringValue (recognize_out[0]).GetData();
        char* pch = strtok (str, "\n");

        for (int y = 980; pch != NULL; y += 50) {

            puttext_y.SetData (y);
            puttext_text.SetData (pch);

            puttext_out = puttext->Execute (puttext_in);

            pch = strtok (NULL, "\n");
        }
    }

    // ..., and finally displays the image.
    display_out = display->Execute (hasTextRecognition ? puttext_in : load_out);

    return 0;
}
