/* Cookies.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdio>
#ifndef __GNUC__
#include <conio.h>
#else
#include "gcc_conio.h"
#endif
#include <iostream>
#include <Poco/DateTimeFormatter.h>
#include <Poco/Path.h>

#include <PureImage.hpp>
#include <Application.hpp>
#include <Arguments.hpp>
#include <stdarg.h>

// enable next line to use FLLLame classifiers
//#define USE_FLLL_CLASSIFIERS

// ----------- constants
const pI_float c_area_treshold = 9.0;


void DiscardSmallObjects (
    pI::Runtime& runtime,
    pI::ArgumentPtr& mask,
    pI::ArgumentPtr& features,
    size_t area_index = 3) {

    // 1. adapt feature table
    pI::DoubleTable dt (features);
    // discard objects with area < c_area_threshold
    std::vector<pI_size> obj_indices;

    for (pI_size lv = 0; lv < dt.GetRows(); ++lv) {
        if (dt.GetData (lv, area_index) >= c_area_treshold) {
            obj_indices.push_back (lv);
        }
    }

    pI::DoubleTable ot (runtime);
    ot.SetCols (dt.GetCols())
    .SetRows (obj_indices.size())
    .CreateData();

    if (obj_indices.size() == 0) {
        return;
    }

    for (pI_size lv = 0; lv < dt.GetCols(); ++lv) {
        ot.SetHeader (lv, dt.GetHeader (lv));
    }

    for (pI_size r = 0; r < obj_indices.size(); ++r) {
        for (pI_size c = 0; c < dt.GetCols(); ++c) {
            ot.SetData (r, c, dt.GetData (obj_indices[r], c));
        }
    }

    // 2. change the mask according to the table
    pI::IntMatrix im (mask);

    for (pI_size rows = 0; rows < im.GetRows(); ++rows) {
        for (pI_size cols = 0; cols < im.GetCols(); ++cols) {
            pI_int p (im.GetData (rows, cols));

            if (p == 0) {
                continue;
            }

            // search in survivors list
            bool found (false);

            for (pI_size lv = 0; lv < obj_indices.size(); ++lv) {
                pI_int label (static_cast<pI_int> (dt.GetData (obj_indices[lv], 0 /* Label */)));

                if (label == p) {
                    im.SetData (rows, cols, lv + 1); // re-label the objects for domcolors
                    found = true;
                    break;
                }
            }

            if (!found) {
                im.SetData (rows, cols, 0);
            }
        }
    }

    features = ot;

    // 3. adapt labels in feature table
    for (pI_size r = 0; r < obj_indices.size(); ++r) {
        ot.SetData (r, 0, r);
    }
} // void DiscardSmallObjects (...)


int main (int argc, char** argv) {

    // use OpenCV for display (not CImg)
    const bool USE_OPENCV_DISPLAY = true;

    using pI::Application;

    pI::Application app;
    pI::Runtime runtime (app.GetRuntime());

    try {
        app.LoadPluginLibrary (app.FindPIPluginDir().c_str(), "OpenCV_pIns");
        app.LoadPluginLibrary (app.FindPIPluginDir().c_str(), "FLLL_pIns");
        app.LoadPluginLibrary (app.FindPIPluginDir().c_str(), "CImg_pIns");
    }
    catch (pI::exception::PluginLibraryException& xc) {
        std::cerr << "Error in LoadPluginLibrary(): " << xc.what() << std::endl;
        return 1;
    }
    app.RegisterPlugins();

    // In the sequel, all required plug-ins are created and initialized.

    // Creates and initializes the plugin that captures images from a camera
    boost::shared_ptr<pI::pIn> cam (runtime.SpawnPlugin ("OpenCV/IO/CaptureFromCAM"));
    pI::Arguments cam_params;
    cam_params.push_back (pI::IntValue (runtime).SetData (-1)); // Lets OS select the camera device

    try {
        cam->Initialize (cam_params);
    }
    catch (...) {
        std::cout << "Failed to initialize camera capture (is a camera connected?)." << std::endl;
        getch();
        return -1;
    }

    pI::Arguments cam_in = cam->GetInputSignature(),
                  cam_out = cam->GetOutputSignature();

    // Initializes the display for the final result.
    boost::shared_ptr<pI::pIn> display_final = app.SpawnAndInitialize (
                                                (USE_OPENCV_DISPLAY ? "OpenCV/IO/ShowImage" : "CImg/Display"),
                                                "Result window",     // Window title
                                                true,                // Enables live display
                                                false,               // Does not swap BGR to RGB
                                                250, 1               // Initial window position
                                                                      );


    pI::Arguments display_final_in = display_final->GetInputSignature(),
                  display_final_out = display_final->GetOutputSignature();

    // IO/FreeImageWriter plugin
    /*
    boost::shared_ptr<pI::pIn> write (runtime.SpawnPlugin ("FreeImage/IO/Writer"));
    pI::Arguments write_params;
    write_params.push_back (pI::BoolValue (runtime).SetData (pI_FALSE));    // Does not require RGB -> BGR conversion
    write->Initialize (write_params);*/
    boost::shared_ptr<pI::pIn> write (runtime.SpawnPlugin ("OpenCV/IO/SaveImage"));
    write->Initialize (write->GetParameterSignature());
    pI::Arguments write_in = write->GetInputSignature();
    pI::Arguments write_out = write->GetOutputSignature();

    // Initializes color to grayscale conversion
    boost::shared_ptr<pI::pIn> togray =
        app.SpawnAndInitialize ("pIn/Color/ToGrayscale", 0.33, 0.33, 0.33);
    pI::Arguments gray_out = togray->GetOutputSignature();

    // cv absdiff
    boost::shared_ptr<pI::pIn> absdiff (runtime.SpawnPlugin ("OpenCV/Core/AbsDiff"));
    pI::Arguments absdiff_in = absdiff->GetInputSignature(),
                  absdiff_out = absdiff->GetOutputSignature();

    // cv thresh
    boost::shared_ptr<pI::pIn> thresh (runtime.SpawnPlugin ("OpenCV/Segmentation/Threshold"));
    pI::Arguments thresh_params = thresh->GetParameterSignature();
    pI::DoubleValue (thresh_params[0]).SetData (20.0);   // pixel threshold
    thresh->Initialize (thresh_params);
    pI::Arguments thresh_in = thresh->GetInputSignature(),
                  thresh_out = thresh->GetOutputSignature();

    // OpenCV/Extensions/Contour/CVContourFeatures
    boost::shared_ptr<pI::pIn> contours (runtime.SpawnPlugin ("pIn/Contour/CVContourFeatures"));
    pI::Arguments contours_params;
    contours->Initialize (contours_params);
    pI::Arguments contours_in = contours->GetInputSignature(),
                  contours_out = contours->GetOutputSignature();

    // Types/DoubleTableCombiner to combine arbitrary feature tables using coloumn bit sets
    boost::shared_ptr<pI::pIn> table_combiner (runtime.SpawnPlugin ("pI/Arguments/Operations/DoubleTableCombiner"));
    pI::Arguments table_combiner_params (table_combiner->GetParameterSignature());
    // select shape features
    pI::BoolArray ba_contours (runtime);
    ba_contours.SetCount (44);
    ba_contours.CreateData();
    ba_contours.SetData (3, pI_TRUE); // enable Area feature
    ba_contours.SetData (37, pI_TRUE); // enable hu1 moment
    ba_contours.SetData (38, pI_TRUE); // enable hu2 moment
    ba_contours.SetData (39, pI_TRUE); // enable hu3 moment
    table_combiner_params[1] = ba_contours;
    table_combiner->Initialize (table_combiner_params);
    pI::Arguments table_combiner_in  = table_combiner->GetInputSignature(),
                  table_combiner_out = table_combiner->GetOutputSignature();

    // Initializes Color/GetDominantColors
    boost::shared_ptr<pI::pIn> domcolors (runtime.SpawnPlugin ("pIn/Color/GetDominantColors"));
    pI::Arguments domcolors_params = domcolors->GetParameterSignature();
    domcolors->Initialize (domcolors_params);
    pI::Arguments domcolors_in  = domcolors->GetInputSignature(),
                  domcolors_out = domcolors->GetOutputSignature();

    // Color/ColorName
    //boost::shared_ptr<pI::pIn> color_name (runtime.SpawnPlugin ("pIn/Color/ColorName"));
    //color_name->Initialize (color_name->GetParameterSignature());
    //pI::Arguments color_name_in  = color_name->GetInputSignature(),
    //            color_name_out = color_name->GetOutputSignature();

    // knn classifier
    boost::shared_ptr<pI::pIn> knn =
        app.SpawnAndInitialize ("OpenCV/ML/KNearest", 2 /* string-labels */, 1 /* k */);
    pI::Arguments knn_in = knn->GetInputSignature(),
                  knn_out = knn->GetOutputSignature();
    pI::BoolValue (knn_in[0]).SetData (pI_TRUE); // enable prediction mode

#ifdef USE_FLLL_CLASSIFIERS
    // eVQ classifier
    boost::shared_ptr<pI::pIn> evq =
        SpawnAndInitialize ("FLLLame/Classifier", 1 /* eVQ */, 2 /* string-labels */);
    pI::Arguments evq_in = evq->GetInputSignature(),
                  evq_out = evq->GetOutputSignature();
    pI::BoolValue (evq_in[2]).SetData (pI_TRUE); // enable prediction mode
#endif // #ifdef USE_FLLL_CLASSIFIERS

    // CImg draw_text
    boost::shared_ptr<pI::pIn> draw_text =
        app.SpawnAndInitialize ("CImg/Functions", 72);
    pI::Arguments draw_text_in  = draw_text->GetInputSignature(),
                  draw_text_out = draw_text->GetOutputSignature();


    cam->Execute (cam_in, cam_out);

    // master and madiff are both deep copies of cam_out
    pI::ArgumentPtr master (runtime.CopyArgument (cam_out[0], pI_TRUE));
    pI::ArgumentPtr madiff (runtime.CopyArgument (cam_out[0], pI_TRUE));
    bool do_save = false;
#ifdef USE_FLLL_CLASSIFIERS
    bool use_knn = false;
#endif // #ifdef USE_FLLL_CLASSIFIERS

    // main loop
    pI_bool do_loop = pI_TRUE;

    while (do_loop == pI_TRUE) {
        while (!kbhit()) {
            pI_int nObjects = 0;
            // Captures a new frame
            cam->Execute (cam_in, cam_out);

            if (do_save) {
                std::string name =
                    std::string ("cam_")
                    + Poco::DateTimeFormatter::format (Poco::LocalDateTime(), "%Y-%m-%d_%H-%M-%S")
                    + std::string (".png");
                write_in[0] = pI::StringValue (runtime).SetData (const_cast<char*> (name.c_str()));
                write_in[1] = cam_out[0];
                try {
                    write->Execute (write_in);
                    std::cout << "\nSaved " << name << std::endl;
                }
                catch (...) {
                    name = std::string (getenv ("tmp")) + Poco::Path::separator() + name;
                    write_in[0] = pI::StringValue (runtime).SetData (const_cast<char*> (name.c_str()));
                    try {
                        write->Execute (write_in);
                        std::cout << "\nSaved " << name << std::endl;
                    }
                    catch (...) {
                        std::cerr << "\nCould not save file " << name << std::endl;
                    }
                }

                do_save = false;
            }

            // First, the following path is executed:
            // AbsDiff(cam_out, master) -> ToGrayscale -> Threshold -> Contours

            absdiff_in[0] = cam_out[0];
            absdiff_in[1] = master;
            absdiff_out[0] = madiff;

            absdiff->Execute (absdiff_in, absdiff_out);
            togray->Execute (absdiff_out, gray_out);
            thresh->Execute (gray_out, thresh_out);

            gray_out[0] = thresh_out[0];
            draw_text_in[0] = cam_out[0];
            draw_text_out[0] = cam_out[0];

            // CVContourFeatures determines both objects and their features; additionally,
            // the contours are drawn into the camera image.
            contours_in[0] = gray_out[0];
            contours_in[1] = draw_text_in[0];
            contours->Execute (contours_in, contours_out);

            // Gets rid of objects too small.
            DiscardSmallObjects (runtime, contours_out[0], contours_out[1]);
            nObjects = pI::DoubleTable (contours_out[1]).GetRows();

            if (nObjects >= 0) {
                std::cout << '\r' << nObjects << (nObjects == 1 ? " object" : " objects") << " found ";
            }

            if (nObjects > 0) {

                domcolors_in[0] = cam_out[0];
                domcolors_in[1] = contours_out[0];
                domcolors->Execute (domcolors_in, domcolors_out);
                //std::string foo = runtime.SerializeArgument (contours_out[0]);

                // combine colour and shape features
                pI::DoubleTable dmc_dt (runtime);
                pI::DoubleTable dmc_dm (domcolors_out[0]);
                dmc_dt.SetCols (dmc_dm.GetCols());
                dmc_dt.SetRows (dmc_dm.GetRows());
                dmc_dt.CreateData();

                for (pI_size r = 0; r < dmc_dt.GetRows(); ++r)
                    for (pI_size c = 0; c < dmc_dt.GetCols(); ++c) {
                        dmc_dt.SetData (r, c, dmc_dm.GetData (r, c));
                    }

                table_combiner_in[0] = dmc_dt;
                table_combiner_in[1] = contours_out[1];
                table_combiner->Execute (table_combiner_in, table_combiner_out);

                knn_in[1] = table_combiner_out[0];
                knn_in[2] = pI::StringArray (runtime).SetCount (nObjects);
#ifdef USE_FLLL_CLASSIFIERS
                evq_in[0] = table_combiner_out[0];
                evq_in[1] = pI::StringArray (runtime).SetCount (nObjects);
#endif // #ifdef USE_FLLL_CLASSIFIERS

                try {
#ifdef USE_FLLL_CLASSIFIERS

                    if (use_knn) {
                        // do knn classification
                        knn->Execute (knn_in, knn_out);
                    }
                    else {
                        // do eVQ classification
                        evq->Execute (evq_in, evq_out);
                    }

#else // #ifdef USE_FLLL_CLASSIFIERS
                    knn->Execute (knn_in, knn_out);
#endif // #ifdef USE_FLLL_CLASSIFIERS
                }
                catch (...) {
                    /* execution may fail if no samples are given */
                }

                // display classification result
#ifdef USE_FLLL_CLASSIFIERS
                pI::StringArray labels (use_knn ? knn_in[2] : evq_in[1]);
#else // #ifdef USE_FLLL_CLASSIFIERS
                pI::StringArray labels (knn_in[2]);
#endif // #ifdef USE_FLLL_CLASSIFIERS

                if (labels.HasData()) {
                    for (pI_size lv = 0; lv < labels.GetCount(); ++lv) {
                        pI::StringValue (draw_text_in[1]).SetData (labels.GetData (lv));
                        pI::IntValue (draw_text_in[2]).SetData (static_cast<pI_int> (pI::DoubleTable (contours_out[1]).GetData (lv, 1)));
                        pI::IntValue (draw_text_in[3]).SetData (static_cast<pI_int> (pI::DoubleTable (contours_out[1]).GetData (lv, 2)));
                        draw_text->Execute (draw_text_in, draw_text_out);
                    }

                    draw_text_in[0] = draw_text_out[0];
                }
            } // if (nObjects > 0)

            // Displays final image
            display_final_in[0] = draw_text_in[0];
            display_final->Execute (display_final_in, display_final_out);
        } // while (!kbhit())

        int ch = getch();

        switch (ch) {
            case 'q':
            case 'Q':
            case 27 /* ESC */:
                // terminate
                do_loop = pI_FALSE;
                break;
            case 'm':
            case 'M':
                // reset master image
                cam->Execute (cam_in, cam_out);
                master = runtime.CopyArgument (cam_out[0], pI_TRUE);
                break;
            case '+': {
                // increase pixel threshold
                pI::DoubleValue (thresh_params[0]).SetData (pI::DoubleValue (thresh_params[0]).GetData() + 1.0);    // pixel threshold
                std::cout << std::endl << "Pixel threshold: " << pI::DoubleValue (thresh_params[0]).GetData() << std::endl;
                thresh->Initialize (thresh_params);
                break;
            }
            case '-': {
                // decrease pixel threshold
                if (pI::DoubleValue (thresh_params[0]).GetData() >= 2.0) {
                    pI::DoubleValue (thresh_params[0]).SetData (pI::DoubleValue (thresh_params[0]).GetData() - 1.0);    // pixel threshold
                    std::cout << std::endl << "Pixel threshold: " << pI::DoubleValue (thresh_params[0]).GetData() << std::endl;
                    thresh->Initialize (thresh_params);
                }
                else {
                    std::cout << std::endl << "Pixel threshold: " << pI::DoubleValue (thresh_params[0]).GetData() << std::endl;
                }

                break;
            }
            case 's':
            case 'S':
                // take "screenshot"
                do_save = true;
                break;
            case 'l':
            case 'L': {
                // label the objects
                pI::DoubleTable dt (table_combiner_out[0]);

                if (dt.GetRows() == 0) {
                    std::cout << "Labeling: no valid objects." << std::endl;
                    break;
                }

                std::string label_string;
                std::cout << std::endl << "Name " << (dt.GetRows() > 1 ? "the cookies: " : "the cookie: ") << std::endl;
                std::cin >> label_string;
                std::cout << std::endl;

                for (pI_size lv = 0; lv < dt.GetRows(); ++lv) {
                    pI::StringArray (knn_in[2]).SetData (lv, (pI_str) label_string.c_str());
#ifdef USE_FLLL_CLASSIFIERS
                    pI::StringArray (evq_in[1]).SetData (lv, (pI_str) label_string.c_str());
#endif // #ifdef USE_FLLL_CLASSIFIERS
                    /*for (pI_size feats = 0; feats < dt.GetCols(); ++feats) {
                        std::cout << " [" << feats << "]: " << dt.GetData (lv, feats);
                    }
                    std::cout << std::endl;*/
                }

                // train knn
                pI::BoolValue (knn_in[0]).SetData (pI_FALSE); // enable training
                knn->Execute (knn_in, knn_out);
                pI::BoolValue (knn_in[0]).SetData (pI_TRUE); // enable prediction
#ifdef USE_FLLL_CLASSIFIERS
                // train flllame classifier
                pI::BoolValue (evq_in[2]).SetData (pI_FALSE); // enable training
                evq->Execute (evq_in, evq_out);
                pI::BoolValue (evq_in[2]).SetData (pI_TRUE); // enable prediction
#endif // #ifdef USE_FLLL_CLASSIFIERS
                break;
            }
            case 'r':
            case 'R': {
                //reset the classifier(s)
                knn =
                    app.SpawnAndInitialize ("OpenCV/ML/KNearest", 2 /* string-labels */, 1 /* k */);
#ifdef USE_FLLL_CLASSIFIERS
                evq =
                    SpawnAndInitialize ("ML/FLLLameClassifiers", 1 /* EvQ */, 2 /* string-labels */);
#endif // #ifdef USE_FLLL_CLASSIFIERS
                std::cout << std::endl << "The models were initialized anew." << std::endl;
                break;
            }
#ifdef USE_FLLL_CLASSIFIERS
            case 'c':
            case 'C': {
                use_knn = !use_knn;
                std::cout << std::endl << "Current classifier: " << (use_knn ? "knn" : "evq") << std::endl;
                break;
            }
#endif // #ifdef USE_FLLL_CLASSIFIERS
            default:
                do_save = false;
        }
    }

    return 0;
}
