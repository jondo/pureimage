/* Launcher.java
 *
 * Copyright 2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.File;

import pI.LauncherUtil;

public class Launcher {

    public static void main(String[] args) {
        boolean doDebug = false;

        for (int i = 0; i < args.length; i++) {
            if(args[i].equals("--doDebug")) {
                doDebug = true;
            } else {
                System.out.println("Unknown argument: " + args[i]);
            }
        }

        String pIBaseDir = LauncherUtil.findPIBaseDir();
        String pILibraryDir = pIBaseDir + File.separator + LauncherUtil.libraryDirname();
        String thirdPartyDir = pIBaseDir + File.separator + LauncherUtil.thirdPartyDirname();

        // main loader uses PI related library dirs for look up of jar libraries
        ClassLoader mainLoader = LauncherUtil.createClassLoader(null, pIBaseDir, pILibraryDir, thirdPartyDir);

        ClassLoader pILoader = LauncherUtil.createClassLoader(mainLoader,
                "pI_java.jar",
                "Examples/Demos/Java/OCR/ExampleDemosJava_OCR.jar",
                "Examples/ExampleDemosJava_OCR.jar" // alternatively, in the Windows installer
                );

        try {
            LauncherUtil.launchClass(pILoader, "OCR", args);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
