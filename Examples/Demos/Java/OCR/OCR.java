/* OCR.java
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.IOException;
import java.util.StringTokenizer;

import pI.Argument;
import pI.ArgumentVector;
import pI.Application;
import pI.BoolValue;
import pI.ByteImage;
import pI.pIn;
import pI.IntValue;
import pI.PIUtils;
import pI.Runtime;
import pI.StringValue;
import pI.StringVector;
import pI.pI;
import pI.pIn;
import pI.LauncherUtil;

public class OCR {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

        System.out.println("Hello world!");
        
		String pureImagePath = LauncherUtil.findPIBaseDir();

		// Set path variables for loading pureImage JNI connector
		PIUtils.initJNI(pureImagePath);
		
		// Creates runtime and base app
		Runtime runtime = new Runtime(false);
		Application app = new Application(runtime);

		try {
			app.LoadPluginLibrary("OpenCV_pIns");
			app.LoadPluginLibrary("CImg_pIns");
		} catch (Exception xc) {
			xc.printStackTrace();
			return;
		}
		
	    // Text recognition by Tesseract is optional
	    boolean hasTextRecognition = false;
	    try {
	        app.LoadPluginLibrary("Tesseract_pIns");
	        hasTextRecognition = true;
	    }
	    catch (Exception xc) {
	        System.out.println("No text recognition.");
	    }

		long nPlugins = app.RegisterPlugins();

		String baseDir = PIUtils.getPIBaseDir();

		// Creates and initializes all the necessary plugins:

		// OpenCV/IO/LoadImage loads images from disc.
		pIn load = runtime.SpawnPlugin("OpenCV/IO/LoadImage");

		ArgumentVector load_params = load.GetParameterSignature();
		load.Initialize(load_params);

		ArgumentVector load_in = load.GetInputSignature();
		ArgumentVector load_out = load.GetOutputSignature();

		// HACK To prevent "uninitialized local variable" errors, dummy-initialize them.
		pIn recognize = new pIn(0,false);
		ArgumentVector recognize_in = new ArgumentVector(0,false),
				       recognize_out = new ArgumentVector(0,false);
		
		pIn puttext = new pIn(0,false);
		ArgumentVector puttext_in = new ArgumentVector(0,false),
				       puttext_out = new ArgumentVector(0,false);
		
	    if (hasTextRecognition) {
			// Tesseract/RecognizePage performs OCR based on the Tesseract library.
			recognize = runtime.SpawnPlugin("Tesseract/RecognizePage");
			ArgumentVector recognize_params = recognize.GetParameterSignature();
			recognize.Initialize(recognize_params);
			recognize_in = recognize.GetInputSignature(); 
			recognize_out = recognize.GetOutputSignature();
	
	
			// OpenCV/Draw/PutText Draws a text string.
			puttext = PIUtils.SpawnAndInitialize(runtime, 
					"OpenCV/Draw/PutText", 
					0,          // font type: CV_FONT_HERSHEY_SIMPLEX
					false,      // render font italic: no
					1.1, 1.1,   // horizontal and vertical scale
					0.0,        // shear: no shear
					2,          // thickness of the text strokes: 2
					2           // type of the line: CV_AA
					);
			puttext_in = puttext.GetInputSignature();
			puttext_out = puttext.GetOutputSignature();
		}

		// CImg/Display displays the resulting RGB image
		pIn display = runtime.SpawnPlugin("CImg/Display");
		ArgumentVector display_params = display.GetParameterSignature();
		display.Initialize(display_params);
		ArgumentVector display_in = display.GetInputSignature(),
		               display_out = display.GetOutputSignature();

        // Now that all plugins are initialized, executes them one after the other:

		// Reads an RGB image, ...
		String imgFile = baseDir + "/Examples/Resources/Images/HelloWorld_Courier.jpg";

		StringValue path = (StringValue) load_in.Get(0);
		path.SetData(imgFile);
		
		try {
			load_out = load.Execute(load_in);
		} catch (Exception xc) {
			xc.printStackTrace();
			return;
		}

		if (hasTextRecognition) {
			// ..., applies English recognition on the filtered image, ...
			try {
				recognize_out = recognize.Execute(load_out);
			} catch (Exception xc) {
				xc.printStackTrace();
				return;
			}
	
			// ..., splits the text into lines, and draws each line onto the image, ...
			StringValue puttext_text = (StringValue) puttext_in.Get(1);
			((IntValue) puttext_in.Get(2)).SetData(160);       // X position is fixed
			IntValue puttext_y = (IntValue) puttext_in.Get(3); // Y position varies
															// for each line
			((IntValue) puttext_in.Get(4)).SetData(0);         // Text color, R value
			((IntValue) puttext_in.Get(5)).SetData(0);         // Text color, G value
			((IntValue) puttext_in.Get(6)).SetData(128);       // Text color, B value
			((BoolValue) puttext_in.Get(7)).SetData(true);     // Render the text
	
			puttext_in.Set(0, load_out.Get(0));
	
			puttext_y.SetData(920);
			puttext_text.SetData("Text recognized by the Tesseract/RecognizePage plugin:");
	
			puttext_out = puttext.Execute(puttext_in);
	
			StringTokenizer pch = new StringTokenizer(
					((StringValue) recognize_out.Get(0)).GetData(), "\n");
	
			for (int y = 980; pch.hasMoreElements(); y += 50) {
	
				puttext_y.SetData(y);
				puttext_text.SetData(pch.nextToken());
	
				puttext_out = puttext.Execute(puttext_in);
			}
		}
		
		// ..., and finally displays the image.
		display_out = display.Execute(hasTextRecognition ? puttext_in : load_out);

		app.dispose();
	}
}
