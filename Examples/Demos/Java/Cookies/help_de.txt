﻿<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

Cookies - Demo (Java)
=====================

Zweck dieses Programms ist, den Einsatz von pureImage für eine relativ typische Aufgabenstellung im Bereich der Bildverarbeitung 
und Mustererkennung zu demonstrieren. Hierzu sollen verschiedene Typen von Keksen durch das Programm klassifiziert, 
d.h., von einander unterschieden können. Dazu muss dem Programm sukzessive die verschiedenen Sorten präsentiert 
und ein zugehöriger Name mitgeteilt werden. Das Programm erlernt die Unterscheidung schrittweise und ist schließlich 
in der Lage alle Keks-Arten gut voneinander zu unterschieden und jede einzelne wieder zu erkennen.

Ablauf
------

Das Erlernen einer Keks-Sorte funktioniert wie folgt:

 * ein oder mehrere Kekse einer Sorte in das Kamera-Feld legen
 * Kommando "l" oder "L" eingeben (mit Eingabetaste bestätigen)
 * Einen beliebigen Keks-Namen eingeben (mit Eingabetaste bestätigen)


Konsole
-------

Die Konsole ermöglicht die Steuerung des Lernens durch Eingabe folgender Kommandos:

<TABLE WIDTH=372 BORDER=1 BORDERCOLOR="#c0c0c0" CELLPADDING=4 CELLSPACING=0 FRAME=VOID>
    <TR VALIGN=TOP>
        <TD WIDTH=58>
            <P STYLE="margin-left: 0.01cm; margin-right: 0.38cm">l</P>
        </TD>
        <TD WIDTH=298>
            <P>Lernen einer Keks-Sorte</P>
        </TD>
    </TR>
    <TR VALIGN=TOP>
        <TD WIDTH=58>
            <P>q</P>
        </TD>
        <TD WIDTH=298>
            <P>Beenden des Programms</P>
        </TD>
    </TR>
    <TR VALIGN=TOP>
        <TD WIDTH=58>
            <P>r</P>
        </TD>
        <TD WIDTH=298>
            <P>Zurücksetzen des Gelernten</P>
        </TD>
    </TR>
    <TR VALIGN=TOP>
        <TD WIDTH=58>
            <P>m</P>
        </TD>
        <TD WIDTH=298>
            <P>Setzen des Masters</P>
        </TD>
    </TR>
    <TR VALIGN=TOP>
        <TD WIDTH=58>
            <P>c</P>
        </TD>
        <TD WIDTH=298>
            <P>Wechseln des Klassifikators</P>
        </TD>
    </TR>
    <TR VALIGN=TOP>
        <TD WIDTH=58>
            <P>s</P>
        </TD>
        <TD WIDTH=298>
            <P>Speichern des aktuellen Bildes</P>
        </TD>
    </TR>
    <TR VALIGN=TOP>
        <TD WIDTH=58>
            <P>+</P>
        </TD>
        <TD WIDTH=298>
            <P>Erh&ouml;hen des Schwellwerts</P>
        </TD>
    </TR>
    <TR VALIGN=TOP>
        <TD WIDTH=58>
            <P>-</P>
        </TD>
        <TD WIDTH=298>
            <P>Senken des Schwellwerts</P>
        </TD>
    </TR>
</TABLE>

Die einzelnen Kommandos müssen mit der Eingabetaste quittiert werden.
Das Schließen der Konsole beendet ebenfalls das Programm.

Simulierte Kamera
-----------------

Dieses Plugin ermöglicht die Durchführung des Cookie-Demos auch ohne USB-Kamera.
In der Symbol-Leiste auf der rechten Seite befinden sich verschiedene Varianten von artifiziellen Keksen.
Drückt man mit der Maus auf ein Symbol so erscheint in der SimCam ganz oben links ein neues Exemplar dieser Sorte.
Mittels der Maus lassen sich die Kekse verschieben und somit ohne Überlappung anordnen.
Drückt man beim Anklicken eines Keks gleichzeitig die Steuerungs-Taste (Strg / Ctrl), so wird der Keks entfernt.
