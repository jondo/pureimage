/* HelpMenu.java
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

package pI.cookies;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.URL;

import javax.swing.AbstractAction;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;

@SuppressWarnings("serial")
public class HelpMenu extends JMenu {

	private static final String COOKIES_HELP_FILE = "resources/help.html";

	public HelpMenu() throws IOException {
		super("Help");

		URL helpFileUrl = HelpMenu.class.getClassLoader().getResource(COOKIES_HELP_FILE);

		JMenuItem helpItem = new JMenuItem(new DisplayHelp(helpFileUrl));
		add(helpItem);
		
	}
	
	class DisplayHelp extends AbstractAction {
		
		private final URL helpFile;

		public DisplayHelp(URL helpFile) {
			super("Help");
			this.helpFile = helpFile;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			JFrame frame = new JFrame();
			frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			
			JEditorPane htmlDisplay;
			try {
				htmlDisplay = new JEditorPane();
				htmlDisplay.setContentType("text/html;charset=UTF-8");
				htmlDisplay.setPage(helpFile);
			} catch (IOException e1) {
				e1.printStackTrace();
				return;
			}
			JScrollPane jScrollPane = new JScrollPane(htmlDisplay);
			frame.getContentPane().add(jScrollPane);
			htmlDisplay.setCaretPosition(0);
			frame.setSize(800, 500);
			
			frame.setVisible(true);
		}
	}
}
