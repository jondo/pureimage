/* Cookies.java
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

package pI.cookies;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

import pI.Argument;
import pI.ArgumentVector;
import pI.Arguments;
import pI.Application;
import pI.BoolArray;
import pI.BoolValue;
import pI.DoubleTable;
import pI.DoubleValue;
import pI.pIn;
import pI.IntMatrix;
import pI.IntValue;
import pI.LauncherUtil;
import pI.PIUtils;
import pI.Runtime;
import pI.SimulationCamPlugin;
import pI.StringArray;
import pI.StringValue;

public class Cookies extends Application {

	// ----------- constants
	final static float c_area_treshold = 9.0f;

	static boolean USE_FLLL_CLASSIFIERS = false;

	// use a simulation cam plugin instead of OpenCV cam
	static boolean USE_SIM_CAM = true;

	// use simulation cam instance directly (not spawned by runtime)
	static boolean USE_SIM_CAM_IN_JAVA = true;

	// use OpenCV for display (not CImg)
	static boolean USE_OPENCV_DISPLAY = true;

	GuiConsole console;
	JFrame mainFrame;
	boolean isRunning = true;

	static Cookies app;

	public static void main(String[] args) {
		// Set PUREIMAGE_PATH variable
		// Note: this has to be done by each application
		String PUREIMAGE_PATH = LauncherUtil.findPIBaseDir();
		
		// loads the JNI shared library
		PIUtils.initJNI(PUREIMAGE_PATH);

		app = new Cookies();
		app.run();
		app.dispose();
	}

	public Cookies() {
		super(false);

		mainFrame = new JFrame();
		mainFrame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				isRunning = false;
			}

			@Override
			public void windowClosed(WindowEvent e) {
				super.windowClosed(e);
				isRunning = false;
			}
		});

		try {
			JMenuBar menuBar = new JMenuBar();
			menuBar.add(Box.createHorizontalGlue());
			menuBar.add(new HelpMenu());
			mainFrame.setJMenuBar(menuBar);
			mainFrame.pack();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		console = new GuiConsole();
		console.setPreferredSize(new Dimension(600, 200));
		console.setMinimumSize(new Dimension(600, 200));
		console.setBorder(BorderFactory.createTitledBorder("Console"));

		Container contentPane = mainFrame.getContentPane();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		mainFrame.add(console);
		mainFrame.pack();
		mainFrame.setVisible(true);

		// to reduce problems that may occur due to crashes when loading other Java pIns
		Runtime runtime = GetRuntime();
		String pluginDir = PIUtils.getPluginDir();		
		PIUtils.setRuntimeProperty(runtime, "AUTO_SEARCH_JAR_FILES", false);
		PIUtils.setRuntimeProperty(runtime, "PLUGIN_JAR_FILES", pluginDir + File.separator + "CookiesSimulationCamera.jar");

		LoadPluginLibrary (pluginDir, "OpenCV_pIns");
		LoadPluginLibrary (pluginDir, "FLLL_pIns");
		LoadPluginLibrary (pluginDir, "CImg_pIns");
		RegisterPlugins();
	}

	@Override
	protected void finalize() {
		super.finalize();
		this.dispose();
	}

	public void run() {

		isRunning = true;

		Runtime runtime = GetRuntime();

		// In the sequel, all required plug-ins are created and initialized.
		// Creates and initializes the plugin that captures images from a camera

		pIn cam;
		if (USE_SIM_CAM) {
			if (USE_SIM_CAM_IN_JAVA) {
				SimulationCamPlugin simCam = new SimulationCamPlugin(runtime);
				cam = simCam;
			} else {
				cam = runtime
						.SpawnPlugin(SimulationCamPlugin.ID_PIN_IO_CAPTURESIMCAM);
			}
		} else {
			cam = runtime.SpawnPlugin("OpenCV/IO/CaptureFromCAM");
		}

		ArgumentVector cam_params = cam.GetParameterSignature();
		cam_params.Set(0, new IntValue(runtime).SetData(-1)); // Lets OS select the
															// camera device
		if (USE_SIM_CAM) {
			// width of generated cam output image
			IntValue width = (IntValue) cam_params.Get(1);
			width.SetData(640);
			// height of generated cam output image
			IntValue height = (IntValue) cam_params.Get(2);
			height.SetData(480);
			DoubleValue displayRatio = (DoubleValue) cam_params.Get(3);
			displayRatio.SetData(0.75);
			BoolValue isStandAlone = (BoolValue) cam_params.Get(4);

			if (!USE_SIM_CAM_IN_JAVA) {
				isStandAlone.SetData(true);
			} else {
				isStandAlone.SetData(false);
			}
		}

		try {
			cam.Initialize(cam_params);
		} catch (Exception e) {
			System.err
					.println("Failed to initialize camera capture (is a camera connected?).");
			return;
		}

		if (USE_SIM_CAM && USE_SIM_CAM_IN_JAVA) {
			JPanel camPanel = ((SimulationCamPlugin) cam).getJPanel();
			camPanel.setBorder(BorderFactory
					.createTitledBorder("Camera Simulation"));
			mainFrame.getContentPane().add(camPanel, 0);
			mainFrame.pack();
		}

		ArgumentVector cam_in = cam.GetInputSignature();
		ArgumentVector cam_out = cam.GetOutputSignature();

		// Initializes the display for the final result.
		pIn display_final = PIUtils.SpawnAndInitialize (runtime,
                (USE_OPENCV_DISPLAY ? "OpenCV/IO/ShowImage" : "CImg/Display"),
                "Result window",     // Window title
                true,                // Enables live display
                false,               // Does not swap BGR to RGB
                250, 1               // Initial window position
                                      );

		ArgumentVector display_final_in = display_final.GetInputSignature();
		ArgumentVector display_final_out = display_final.GetOutputSignature();

		// IO/FreeImageWriter plugin --> OpenCV/SaveImage plugin
		/*
        pIn write = runtime.SpawnPlugin("FreeImage/IO/Writer");
		ArgumentVector write_params = write.GetParameterSignature();
				write_params[0] = new BoolValue(runtime).SetData(false); // Does not
																	// require
																	// RGB . BGR
																	// conversion
*/
        
        pIn write = runtime.SpawnPlugin("OpenCV/IO/SaveImage");
		ArgumentVector write_params = write.GetParameterSignature();
		write.Initialize(write_params);
		ArgumentVector write_in = write.GetInputSignature();

		// Initializes color to grayscale conversion
		pIn togray = PIUtils.SpawnAndInitialize(runtime, "pIn/Color/ToGrayscale",
				0.33, 0.33, 0.33);
		ArgumentVector gray_out = togray.GetOutputSignature();

		// cv absdiff
		pIn absdiff = runtime.SpawnPlugin("OpenCV/Core/AbsDiff");
		ArgumentVector absdiff_in = absdiff.GetInputSignature(), absdiff_out = absdiff
				.GetOutputSignature();

		// cv thresh
		pIn thresh = runtime.SpawnPlugin("OpenCV/Segmentation/Threshold");
		ArgumentVector thresh_params = thresh.GetParameterSignature();
		((DoubleValue) thresh_params.Get(0)).SetData(20.0); // pixel threshold
		thresh.Initialize(thresh_params);
		ArgumentVector thresh_out = thresh.GetOutputSignature();

		// OpenCV/Extensions/Contour/CVContourFeatures
		pIn contours = runtime.SpawnPlugin("pIn/Contour/CVContourFeatures");
		contours.Initialize(new ArgumentVector());
		ArgumentVector contours_in = contours.GetInputSignature();
		ArgumentVector contours_out = contours.GetOutputSignature();

		// Types/DoubleTableCombiner to combine arbitrary feature tables using
		// coloumn bit sets
		pIn table_combiner = runtime
				.SpawnPlugin("pI/Arguments/Operations/DoubleTableCombiner");
		ArgumentVector table_combiner_params = table_combiner
				.GetParameterSignature();
		// select shape features
		BoolArray ba_contours = new BoolArray(runtime);
		ba_contours.SetCount(44);
		ba_contours.CreateData();
		ba_contours.SetData(3, true); // enable Area feature
		ba_contours.SetData(37, true); // enable hu1 moment
		ba_contours.SetData(38, true); // enable hu2 moment
		ba_contours.SetData(39, true); // enable hu3 moment
		table_combiner_params.Set(1, ba_contours);
		table_combiner.Initialize(table_combiner_params);
		ArgumentVector table_combiner_in = table_combiner.GetInputSignature(), table_combiner_out = table_combiner
				.GetOutputSignature();

		// Initializes Color/GetDominantColors
		pIn domcolors = runtime.SpawnPlugin("pIn/Color/GetDominantColors");
		ArgumentVector domcolors_params = domcolors.GetParameterSignature();
		domcolors.Initialize(domcolors_params);
		ArgumentVector domcolors_in = domcolors.GetInputSignature();
		ArgumentVector domcolors_out = domcolors.GetOutputSignature();

		// knn classifier
		final int string_labels = 2;
		final int k = 1;
		pIn knn = PIUtils.SpawnAndInitialize(runtime, "OpenCV/ML/KNearest",
				string_labels, k);
		ArgumentVector knn_in = knn.GetInputSignature(), knn_out = knn
				.GetOutputSignature();
		((BoolValue) knn_in.Get(0)).SetData(true); // enable prediction mode

		pIn evq = null;
		ArgumentVector evq_in = null;
		ArgumentVector evq_out = null;
		if (USE_FLLL_CLASSIFIERS) {
			// eVQ classifier
			evq = PIUtils.SpawnAndInitialize(runtime, "FLLLame/Classifier", 1 /* eVQ */,
					2 /* string-labels */);
			evq_in = evq.GetInputSignature();
			evq_out = evq.GetOutputSignature();
			((BoolValue) evq_in.Get(2)).SetData(true); // enable prediction mode
		}

		// CImg draw_text
		pIn draw_text = PIUtils.SpawnAndInitialize(runtime, "CImg/Functions", 72);
		ArgumentVector draw_text_in = draw_text.GetInputSignature(), draw_text_out = draw_text
				.GetOutputSignature();

		cam.Execute(cam_in, cam_out);

		// master and madiff are both deep copies of cam_out
		Argument master = Arguments.copyArgument(runtime, cam_out.Get(0));
		Argument madiff =  Arguments.copyArgument(runtime, cam_out.Get(0));
		boolean do_save = false;
		boolean use_knn = false;

		int run = 0;
		// main loop
		while (isRunning == true) {
			while (!console.hasLine()) {

				if (!isRunning) {
					return;
				}

				long nObjects = 0;
				// Captures a new frame
				cam.Execute(cam_in, cam_out);
				if (do_save) {
					String date_str = new SimpleDateFormat(
							"yyyy-MM-dd_HH-mm-ss").format(new Date(System
							.currentTimeMillis()));
					String name = "cam_" + date_str + ".png";
					write_in.Set(0, new StringValue(runtime).SetData(name));
					write_in.Set(1, cam_out.Get(0));
					try{
						write.Execute(write_in);
						console.println("Image saved: " + name);
					} catch (Exception e1) {
						name = System.getenv("TMP") + "/" + name;
						write_in.Set(0, new StringValue(runtime).SetData(name));
						try {
							write.Execute(write_in);
							console.println("Image saved: " + name);
						}
						catch (Exception e2) {
							System.err.println("Could not save file " + name);
						}
					}

					do_save = false;
				}

				// First, the following path is executed:
				// AbsDiff(cam_out, master) . ToGrayscale . Threshold . Contours

				absdiff_in.Set(0, cam_out.Get(0));
				absdiff_in.Set(1, master);
				absdiff_out.Set(0, madiff);
				
				absdiff.Execute(absdiff_in, absdiff_out);
				togray.Execute(absdiff_out, gray_out);
				thresh.Execute(gray_out, thresh_out);

				gray_out.Set(0, thresh_out.Get(0));
				draw_text_in.Set(0, cam_out.Get(0));
				draw_text_out.Set(0, cam_out.Get(0));

				// CVContourFeatures determines both objects and their features;
				// additionally,
				// the contours are drawn into the camera image.
				contours_in.Set(0, gray_out.Get(0));
				contours_in.Set(1, draw_text_in.Get(0));

				contours.Execute(contours_in, contours_out);

				// Gets rid of objects too small.
				contours_out.Set(1, DiscardSmallObjects(runtime, (IntMatrix) contours_out.Get(0),
						(DoubleTable) contours_out.Get(1)));
				nObjects = ((DoubleTable) contours_out.Get(1)).GetRows();

				if (nObjects >= 0) {
					System.out.println(nObjects + " objects found ");
				}

				if (nObjects > 0) {

					domcolors_in.Set(0, cam_out.Get(0));
					domcolors_in.Set(1, contours_out.Get(0));
					domcolors.Execute(domcolors_in, domcolors_out);
					// std::string foo = runtime.SerializeArgument
					// (contours_out[0]);

					// combine colour and shape features
					DoubleTable dmc_dt = new DoubleTable(runtime);
					DoubleTable dmc_dm = (DoubleTable) domcolors_out.Get(0);
					dmc_dt.SetCols(dmc_dm.GetCols());
					dmc_dt.SetRows(dmc_dm.GetRows());
					dmc_dt.CreateData();

					for (int r = 0; r < dmc_dt.GetRows(); ++r)
						for (int c = 0; c < dmc_dt.GetCols(); ++c) {
							dmc_dt.SetData(r, c, dmc_dm.GetData(r, c));
						}

					table_combiner_in.Set(0, dmc_dt);
					table_combiner_in.Set(1, contours_out.Get(1));
					table_combiner.Execute(table_combiner_in,
							table_combiner_out);

					knn_in.Set(1, table_combiner_out.Get(0));
					knn_in.Set(2, new StringArray(runtime).SetCount(nObjects));

					if (USE_FLLL_CLASSIFIERS) {
						evq_in.Set(0, table_combiner_out.Get(0));
						evq_in.Set(1, new StringArray(runtime).SetCount(nObjects));
					}

					try {
						if (USE_FLLL_CLASSIFIERS) {
							if (use_knn) {
								// do knn classification
								knn.Execute(knn_in, knn_out);
							} else {
								// do EvQ classification
								evq.Execute(evq_in, evq_out);
							}
						} else {
							knn.Execute(knn_in, knn_out);
						}
					} catch (Exception e) {
						/* execution may fail if no samples are given */
					}

					// display classification result
					StringArray labels;
					if (USE_FLLL_CLASSIFIERS) {
						labels = (StringArray) (use_knn ? knn_in.Get(2) : evq_in.Get(1));
					} else {
						labels = (StringArray) knn_in.Get(2);
					}

					if (labels.HasData()) {
						for (int lv = 0; lv < labels.GetCount(); ++lv) {
							((StringValue) draw_text_in.Get(1)).SetData(labels
									.GetData(lv));
							((IntValue) draw_text_in.Get(2))
									.SetData((int) ((DoubleTable) contours_out.Get(1))
											.GetData(lv, 1));
							((IntValue) draw_text_in.Get(3))
									.SetData((int) ((DoubleTable) contours_out.Get(1))
											.GetData(lv, 2));
							draw_text.Execute(draw_text_in, draw_text_out);
						}

						draw_text_in.Set(0, draw_text_out.Get(0));
					}
				} // if (nObjects > 0)

				// Displays final image
				display_final_in.Set(0, draw_text_in.Get(0));
				display_final.Execute(display_final_in, display_final_out);
			} // while (!kbhit())

			String cmd = console.readLine().toLowerCase();

			if (cmd.length() == 1) {
				// skip the command if longer than 1
				// as all commands are single letters

				char ch = cmd.charAt(0);

				switch (ch) {
				case 'q':
				case 27 /* ESC */:
					// terminate
					isRunning = false;
					break;
				case 'm':
					// reset master image
					cam.Execute(cam_in, cam_out);
					master =  Arguments.copyArgument(runtime, cam_out.Get(0));
					console.println("Reset master.");
					break;
				case '+': {
					// increase pixel threshold
					((DoubleValue) thresh_params.Get(0))
							.SetData(((DoubleValue) thresh_params.Get(0)).GetData() + 1.0); // pixel
																						// threshold
					console.println("Pixel threshold: "
							+ ((DoubleValue) thresh_params.Get(0)).GetData());
					thresh.Initialize(thresh_params);
					break;
				}
				case '-': {
					// decrease pixel threshold
					if (((DoubleValue) thresh_params.Get(0)).GetData() >= 2.0) {
						((DoubleValue) thresh_params.Get(0))
								.SetData(((DoubleValue) thresh_params.Get(0))
										.GetData() - 1.0); // pixel
															// threshold
						console.println("Pixel threshold: "
								+ ((DoubleValue) thresh_params.Get(0)).GetData());
						thresh.Initialize(thresh_params);
					} else {
						console.println("Pixel threshold: "
								+ ((DoubleValue) thresh_params.Get(0)).GetData());
					}
					break;
				}
				case 's':
					// take "screenshot"
					do_save = true;
					break;
				case 'l': {
					// label the objects
					DoubleTable dt = (DoubleTable) table_combiner_out.Get(0);

					if (dt.GetRows() == 0) {
						console.println("Labeling: no valid objects.");
						break;
					}

					console.println("Name "
							+ (dt.GetRows() > 1 ? "the cookies: " : "the cookie: "));
					String label_string = console.readLine();

					for (int lv = 0; lv < dt.GetRows(); ++lv) {
						((StringArray) knn_in.Get(2)).SetData(lv, label_string);

						if (USE_FLLL_CLASSIFIERS) {
							((StringArray) evq_in.Get(1)).SetData(lv, label_string);
						}
					}

					// train knn
					((BoolValue) knn_in.Get(0)).SetData(false); // enable training
					try {
						knn.Execute(knn_in, knn_out);
					} catch (Exception e) {
						System.err.println("Oh oh. Training failed: " + e.getMessage());
					}
					((BoolValue) knn_in.Get(0)).SetData(true); // enable prediction

					if (USE_FLLL_CLASSIFIERS) {
						// train flllame classifier
						((BoolValue) evq_in.Get(2)).SetData(false); // enable
																// training
						evq.Execute(evq_in, evq_out);
						((BoolValue) evq_in.Get(2)).SetData(true); // enable
																// prediction
					}
					break;
				}
				case 'r': {
					// reset the classifier(s)
					knn = PIUtils.SpawnAndInitialize(runtime, "OpenCV/ML/KNearest",
							2 /*
							 * string- labels
							 */, 1 /* k */);

					if (USE_FLLL_CLASSIFIERS) {
						evq = PIUtils.SpawnAndInitialize(runtime,
								"ML/FLLLameClassifiers", 1 /* EvQ */, 2 /*
																		 * string-
																		 * labels
																		 */);
					}
					console.println("The models were initialized anew.");
					break;
				}
				case 'c': {
					if (USE_FLLL_CLASSIFIERS) {
						use_knn = !use_knn;
						console.println("Current classifier: "
								+ (use_knn ? "knn" : "evq"));
					}
                    else {
						console.println("This application version only contains one classifier.");
                    }
					break;
				}
				default:
					do_save = false;
				}
			}
		}

		// mainFrame.dispose();
	}

	public void dispose() {
		super.dispose();
		mainFrame.dispose();
	}

	private DoubleTable DiscardSmallObjects(Runtime runtime, IntMatrix mask,
			DoubleTable features) {
		return DiscardSmallObjects(runtime, mask, features, 3);
	}

	private DoubleTable DiscardSmallObjects(Runtime runtime, IntMatrix mask,
			DoubleTable features, int area_index) {

		// 1. adapt feature table
		DoubleTable dt = features;

		// discard objects with area < c_area_threshold
		List<Integer> obj_indices = new LinkedList<Integer>();

		for (int lv = 0; lv < dt.GetRows(); ++lv) {
			if (dt.GetData(lv, area_index) >= c_area_treshold) {
				obj_indices.add(lv);
			}
		}

		DoubleTable ot = new DoubleTable(runtime);
		ot.SetCols(dt.GetCols()).SetRows(obj_indices.size()).CreateData();

		if (obj_indices.size() == 0) {
			return ot;
		}

		for (int lv = 0; lv < dt.GetCols(); ++lv) {
			ot.SetHeader(lv, dt.GetHeader(lv));
		}

		for (int r = 0; r < obj_indices.size(); ++r) {
			for (int c = 0; c < dt.GetCols(); ++c) {
				ot.SetData(r, c, dt.GetData(obj_indices.get(r), c));
			}
		}

		// 2. change the mask according to the table
		IntMatrix im = mask;

		for (int rows = 0; rows < im.GetRows(); ++rows) {
			for (int cols = 0; cols < im.GetCols(); ++cols) {
				int p = im.GetData(rows, cols);

				if (p == 0) {
					continue;
				}

				// search in survivors list
				boolean found = false;

				for (int lv = 0; lv < obj_indices.size(); ++lv) {
					int label = (int) dt
							.GetData(obj_indices.get(lv), 0 /* Label */);

					if (label == p) {
						im.SetData(rows, cols, lv + 1); // re-label the objects
														// for domcolors
						found = true;
						break;
					}
				}

				if (!found) {
					im.SetData(rows, cols, 0);
				}
			}
		}

		// 3. adapt labels in feature table
		for (int r = 0; r < obj_indices.size(); ++r) {
			ot.SetData(r, 0, r);
		}
		
		return ot;
	} // void DiscardSmallObjects (...)

}
