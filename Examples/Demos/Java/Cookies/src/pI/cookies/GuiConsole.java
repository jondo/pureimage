/* GuiConsole.java
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

package pI.cookies;

import java.awt.GridLayout;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.concurrent.Semaphore;

import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;


@SuppressWarnings("serial")
public class GuiConsole extends JPanel {
	
	// the buffer used to store input
	StringBuffer inBuf;
	
	// a text area to display console out- and input
	JTextArea cons;
	
	// this semaphore is used to implement a blocking readLine method
	Semaphore syncOnReadLine;
		
	public GuiConsole() {
		
		setLayout(new GridLayout());

		cons = new JTextArea();
		// deactivates the standard editing behaviour		
		cons.setEditable(false);

		
		// removes standard key mapping		
		InputMap inputMap = new InputMap();
		cons.addKeyListener(new KeyInputHandler());
		
		cons.setInputMap(JComponent.WHEN_FOCUSED, inputMap);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportView(cons);
		
		add(scrollPane);
		
		inBuf = new StringBuffer();
		
		syncOnReadLine = new Semaphore(0);
		cons.addFocusListener(new FocusHandler());
		cons.addMouseListener(new MouseHandler());
	}
	
	public synchronized boolean hasChar() {
		return inBuf.length() > 0;
	}

	public synchronized boolean hasLine() {
		return syncOnReadLine.availablePermits()>0;
	}
	
	public String readLine() {
		
		cons.grabFocus();
		
		syncOnReadLine.acquireUninterruptibly();
		
		int endIdx = inBuf.indexOf("\n");
		
		if(endIdx<0) {
			endIdx = inBuf.length();
		}		
		
		String retVal = inBuf.substring(0, endIdx);
		inBuf.delete(0, endIdx+1);
		
		return retVal;
	}
	
	public synchronized void println(String msg) {
		cons.append(msg + "\n");
		cons.setCaretPosition(cons.getDocument().getLength());
	}

	public synchronized void println() {
		println("");
	}
	
	public synchronized char getChar() {
		char c = inBuf.charAt(0);
		inBuf.deleteCharAt(0);
		//cons.append(""+c);
		return c;
	}
	
	synchronized void append(char c) {
		inBuf.append(c);		
	}
	
	class KeyInputHandler extends KeyAdapter {
		
		@Override
		public void keyTyped(KeyEvent e) {
			char keyChar = e.getKeyChar();
			if(keyChar == KeyEvent.VK_BACK_SPACE) {				
				// only accept deletion if there is something
				// in the buffer
				if(inBuf.length() > 0) {
					int pos = inBuf.length()-1;
					inBuf.deleteCharAt(pos);
					inBuf.setLength(pos);
					Document doc = cons.getDocument();
					try {
						pos = doc.getLength()-1;
						doc.remove(pos, 1);
						cons.setCaretPosition(pos);
					} catch (BadLocationException e1) {
						e1.printStackTrace();
					}
				}
				e.consume();
			} else {
				append(keyChar);
				cons.append("" + keyChar);
				
				// unblock readLine calls 
				if( (keyChar == '\n') ) {
					syncOnReadLine.release();
				}
				e.consume();
			}

			super.keyTyped(e);
		}
	}
	
	static GuiConsole console = null;

	
	/**
	 * This FocusListener makes the cursor visible on focus.
	 * It is necessary as we use a non-editable JTextArea which does not show the caret by default. 
	 */
	class FocusHandler implements FocusListener {
		@Override
		public void focusGained(FocusEvent e) {
			cons.getCaret().setVisible(true);
		}

		@Override
		public void focusLost(FocusEvent e) {
			cons.getCaret().setVisible(false);
		}
	}

	/**
	 * Grabs the focus on mouse over. 
	 */
	class MouseHandler extends MouseAdapter {
		@Override
		public void mouseEntered(MouseEvent e) {
			cons.grabFocus();
			super.mouseEntered(e);			
		}		
	}
}
