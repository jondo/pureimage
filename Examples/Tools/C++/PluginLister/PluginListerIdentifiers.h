/* PluginListerIdentifiers.h
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PI_PLUGINLISTER_JSONIDENTIFIERS_H__
#define PI_PLUGINLISTER_JSONIDENTIFIERS_H__

#define PLUGINS "plugins"
#define PARAMETERS "parameters"
#define API_VERSION "apiVersion"
#define PLUGIN_COUNT "pluginCount"
#define NAME "name"
#define DESCRIPTION "description"
#define PLUGIN_VERSION "pluginVersion"
#define AUTHOR "author"
#define COPYRIGHT "copyright"
#define OPTIONAL_INIT "optionalInit"
#define DEFAULT_INIT "defaultInit"
#define INPUT_SIGNATURE "inputSignature"
#define OUTPUT_SIGNATURE "outputSignature"

#endif
