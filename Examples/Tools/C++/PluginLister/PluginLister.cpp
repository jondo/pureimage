/* PluginLister.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdio>
#include <string>
#include <iostream>

#include <PureImage.hpp>
#include <Application.hpp>
#include <Arguments.hpp>

#include "PluginListerIdentifiers.h"

void appendValue(std::ostream& os, std::string key, std::string value, bool withTrailingSeparator = true) {
	os << "\"" << key  << "\"" << ": " << "\"" << value << "\"";
    if (withTrailingSeparator) {
        os << ", ";
    }
    os << std::endl;
}

void appendValue(std::ostream& os, std::string key, pI_int value) {
	os << "\"" << key  << "\"" << ": " << "\"" << value << "\"" << ", " << std::endl;
}

std::vector<std::string> GetPluginNames(pI::Runtime& runtime) {

    CRuntime* c_runtime = runtime.GetCRuntime();
    pI_size plugin_count = c_runtime->plugin_count;
    std::vector<std::string> plugin_names;

    for (pI_size idx = 0; idx < plugin_count; idx++) {
        CpIn* cpIn = c_runtime->plugins[idx];
        plugin_names.push_back (cpIn->name);
    }

    return plugin_names;
} // std::vector<std::string> BaseApplication::GetPluginNames() const

int main (int argc, char** argv) {

    // Creates runtime and base app
	pI::Application app;
	pI::Runtime runtime(app.GetRuntime());

    std::cerr << "Plugin dir: " << app.FindPIPluginDir() << std::endl;
	app.LoadPluginLibraries (app.FindPIPluginDir().c_str());

	std::vector<std::string> names(GetPluginNames (runtime));
	const pI_int count(names.size());

	std::cout << "{" << std::endl;

	appendValue(std::cout, API_VERSION, runtime.GetRuntimeVersion());
	appendValue(std::cout, PLUGIN_COUNT, count);

	std::cout << "\"" << PLUGINS << "\":" << std::endl << "[" << std::endl;
	for (pI_int lv = 0; lv < count; ++lv) {
		std::cout << "{" << std::endl;
		appendValue(std::cout, NAME, names[lv]);

		if (names[lv] == "CImg/Functions") {
			// special treatment for CImg META plugin
			try {
				pI::pInPtr p(runtime.SpawnPlugin ((const pI_str) names[lv].c_str()));
				pI::Arguments params(p->GetParameterSignature());
				appendValue(std::cout, DESCRIPTION, p->GetDescription());
				appendValue(std::cout, PLUGIN_VERSION, p->GetpInVersion());
				appendValue(std::cout, API_VERSION, p->GetAPIVersion());
				appendValue(std::cout, AUTHOR, p->GetAuthor());
				appendValue(std::cout, COPYRIGHT, p->GetCopyright());
				appendValue(std::cout, OPTIONAL_INIT, "false");
				appendValue(std::cout, DEFAULT_INIT, "true");

				std::cout << "\"parameters\":" << std::endl << "[" << std::endl;
				for (pI_size lv2 = 0; lv2 < params.size(); ++lv2) {
					std::cout << runtime.SerializeArgument (params[lv2]);
					if (lv2 < params.size() - 1) {
						std::cout << ",";
					}
					std::cout << std::endl;
				}
				std::cout << "]," << std::endl;

				pI::StringSelection sts(params[0]);
				std::cout << "\"CImg functions\":" << std::endl << "[" << std::endl;
				for (pI_size lv2 = 0; lv2 < sts.GetCount(); ++lv2) {
					sts.SetIndex (lv2);
					p->Initialize (params);
					std::cout << "{" << std::endl;
					appendValue(std::cout, NAME, sts.GetSymbols (lv2));
					std::cout << "\"inputSignature\":" << std::endl << "[" << std::endl;
					pI::Arguments is(p->GetInputSignature());
					for (pI_size lv3 = 0; lv3 < is.size(); ++lv3) {
						std::cout << runtime.SerializeArgument (is[lv3]);
						if (lv3 < is.size() - 1)
							std::cout << ",";
					}
					std::cout << std::endl << "]," << std::endl;
					std::cout << "\"outputSignature\":" << std::endl << "[" << std::endl;
					pI::Arguments os(p->GetOutputSignature());
					for (pI_size lv3 = 0; lv3 < os.size(); ++lv3) {
						std::cout << runtime.SerializeArgument (os[lv3]);
						if (lv3 < os.size() - 1)
							std::cout << ",";
					}
					std::cout << std::endl << "]" << std::endl;
					std::cout << "}";
					if (lv2 < sts.GetCount() - 1)
						std::cout << ",";
					std::cout << std::endl;
				}
				std::cout << "]" << std::endl;
			} catch (...) {}
		} else {
			try {
				pI::pInPtr p(runtime.SpawnPlugin ((const pI_str) names[lv].c_str()));
				appendValue(std::cout, DESCRIPTION, p->GetDescription());
				appendValue(std::cout, PLUGIN_VERSION, p->GetpInVersion());
				appendValue(std::cout, API_VERSION, p->GetAPIVersion());
				appendValue(std::cout, AUTHOR, p->GetAuthor());
				appendValue(std::cout, COPYRIGHT, p->GetCopyright());
				bool optional_init((p->GetInputSignature().size() > 0) ||
								   (p->GetOutputSignature().size() > 0));
				appendValue(std::cout, OPTIONAL_INIT, (optional_init ? "true" : "false"));

				std::cout << "\"" << PARAMETERS << "\":" << std::endl << "[" << std::endl;
				pI::Arguments params(p->GetParameterSignature());
				for (pI_size lv2 = 0; lv2 < params.size(); ++lv2) {
					std::cout << runtime.SerializeArgument (params[lv2]);
					if (lv2 < params.size() - 1) {
						std::cout << ",";
					}
					std::cout << std::endl;
				}
				std::cout << "]," << std::endl;
				bool default_init;
				try {
					p->Initialize (params);
					default_init = true;
				} catch (...) {
					default_init = false;
				}
				appendValue(std::cout, DEFAULT_INIT, (default_init ? "true" : "false"), optional_init || default_init);

				if (optional_init || default_init) {
					std::cout << "\"" << INPUT_SIGNATURE << "\":" << std::endl << "[" << std::endl;
					pI::Arguments is(p->GetInputSignature());
					for (pI_size lv2 = 0; lv2 < is.size(); ++lv2) {
						std::cout << runtime.SerializeArgument (is[lv2]);
						if (lv2 < is.size() - 1)
							std::cout << ",";
					}
					std::cout << std::endl << "]," << std::endl;
					std::cout << "\"" << OUTPUT_SIGNATURE << "\":" << std::endl << "[" << std::endl;
					pI::Arguments os(p->GetOutputSignature());
					for (pI_size lv2 = 0; lv2 < os.size(); ++lv2) {
						std::cout << runtime.SerializeArgument (os[lv2]);
						if (lv2 < os.size() - 1)
							std::cout << ",";
					}
					std::cout << std::endl << "]" << std::endl;
				} else
					std::cout << std::endl;
			} catch (...) {}
		}
		std::cout << "}";
		if (lv < count - 1)
			std::cout << ",";
		std::cout << std::endl;
	}
	std::cout << "]" << std::endl << "}" << std::endl;
	return 0;
}
