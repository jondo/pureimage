/* KinectViewer.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#define NOMINMAX

#include <cstdio>
#include <cctype>

#ifndef __GNUC__
#include <conio.h>
#else
#include "gcc_conio.h"
#endif

#include <cmath>

#include <iostream>

#include <boost/format.hpp>
#include <boost/timer.hpp>

#include <Poco/Path.h>
#include <Poco/DateTimeFormatter.h>

#include <PureImage.hpp>
#include <Application.hpp>
#include <Arguments.hpp>

#include "ColorRamp.hpp"


ColorRamp createTerrainColorRamp() {

    std::map<size_t, uint32_t> freenect_glview_colors; // Coloring as used in Freenect's glview example
    freenect_glview_colors[0]    = (0xFFFFFF); // white
    freenect_glview_colors[256]  = (0xFF0000); // red
    freenect_glview_colors[512]  = (0xFFFF00); // yellow
    freenect_glview_colors[768]  = (0x00FF00); // green
    freenect_glview_colors[1024] = (0x00FFFF); // cyan
    freenect_glview_colors[1280] = (0x0000FF); // blue
    freenect_glview_colors[1536] = (0x000000); // black

    std::map<size_t, uint32_t> terrain; // Alternative coloring; names taken from http://en.wikipedia.org/wiki/List_of_colors
    terrain[0]     = (0xFFFFFF); // White
    terrain[256]   = (0x964B00); // Brown (traditional)
    terrain[512]   = (0xFBEC5D); // Maize
    terrain[768]   = (0x66FF00); // Bright green
    terrain[1024]  = (0x014421); // Forest green (traditional)
    terrain[1025]  = (0x87CEFA); // Light sky blue
    terrain[1280]  = (0x00008B); // Dark blue
    terrain[2047]  = (0x000000); // Black
    terrain[2048]  = (0x696969); // Dim gray

    return ColorRamp (terrain);
}


int main (int argc, char** argv) {

    const bool USE_OPENCV_DISPLAY = false; // Use OpenCV or CImg for display?

    std::string name ("Kinect");

    // Creates runtime and base app
    pI::Application app;
    pI::Runtime runtime (app.GetRuntime());

    try {
        app.LoadPluginLibrary ("CImg_pIns");
        app.LoadPluginLibrary ("FLLL_pIns");
        app.LoadPluginLibrary ("OpenCV_pIns");
        app.LoadPluginLibrary ("Freenect_pIns");
    }
    catch (pI::exception::PluginLibraryException& xc) {
        std::cerr << "Error in LoadPluginLibrary(): " << xc.what() << std::endl;
        return 1;
    }

    bool usePCL = true;
    try {
        app.LoadPluginLibrary ("PCL_pIns");
    }
    catch (pI::exception::PluginLibraryException& xc) {
        std::cerr << "PCL is disabled: " << xc.what() << std::endl;
        usePCL = false;
    }

    app.RegisterPlugins();

    // In the sequel, all required plug-ins are created and initialized.
    const int DeviceIndex = 0;
    const bool ShowRGB = true;

    // Freenect/GetVideoAndDepth returns both the RGB image and the depth map captured by a Kinect.
    boost::shared_ptr<pI::pIn> capture
    = app.SpawnAndInitialize ("Freenect/GetVideoAndDepth",
                              DeviceIndex,
                              ShowRGB ? 0 : 2,   // Video format: FREENECT_VIDEO_RGB or FREENECT_VIDEO_IR_8BIT
                              0                  // Depth format: FREENECT_DEPTH_11BIT
                             );
    pI::Arguments /*capture_in = capture->GetInputSignature(),*/
    capture_out = capture->GetOutputSignature();

    // OpenCV/IO/ShowImage or CImg/Display displays the image and the depth map.
    boost::shared_ptr<pI::pIn> displayImage = app.SpawnAndInitialize (
                (USE_OPENCV_DISPLAY ? "OpenCV/IO/ShowImage" : "CImg/Display"),
                (name + std::string (" viewer - RGB view")).c_str(),    // Window title
                pI_TRUE,                                                // Enables live display
                false,                                                  // Does not swap BGR to RGB
                0, 0                                                    // Initial window position
            );

    boost::shared_ptr<pI::pIn> displayDepth = app.SpawnAndInitialize (
                "OpenCV/IO/ShowImage",
                (name + std::string (" viewer - depth view")).c_str(),    // Window title
                pI_TRUE,                                                  // Enables live display
                false,                                                    // Does not swap BGR to RGB
                650, 0                                                    // Initial window position
            );

    pI::Arguments displayImage_in = displayImage->GetInputSignature(),
                  displayImage_out = displayImage->GetOutputSignature();

    pI::Arguments displayDepth_in = displayDepth->GetInputSignature(),
                  displayDepth_out = displayDepth->GetOutputSignature();

    // Color/ApplyRGBPalette applies an arbitrary RGB palette onto a matrix
    ColorRamp ramp = createTerrainColorRamp();

    boost::shared_ptr<pI::pIn> applyRGB (runtime.SpawnPlugin ("pIn/Color/ApplyRGBPalette"));
    pI::Arguments applyRGB_params = applyRGB->GetParameterSignature();

    pI::RGBPalette rgb (applyRGB_params[0]);
    rgb.SetCount (2048);

    for (int i = 0; i < 2048; i++) {
        float v = powf ( (static_cast<float> (i) / 2048.0f), 3.0f);
        uint16_t gamma = static_cast<uint16_t> (9216.0f * v);

        uint32_t c = ramp.GetColor (gamma);
        rgb.SetR (i, red (c));
        rgb.SetG (i, green (c));
        rgb.SetB (i, blue (c));
    }

    applyRGB->Initialize (applyRGB_params);

    pI::Arguments applyRGB_in = applyRGB->GetInputSignature(),
                  applyRGB_out = applyRGB->GetOutputSignature();

    // OpenCV/Draw/Rectangle draws the inner rectangle.
    boost::shared_ptr<pI::pIn> drawRectImage = app.SpawnAndInitialize ("OpenCV/Draw/Rectangle");
    pI::Arguments drawRectImage_in = drawRectImage->GetInputSignature(),
                  drawRectImage_out = drawRectImage->GetOutputSignature();

    boost::shared_ptr<pI::pIn> drawRectDepth = app.SpawnAndInitialize ("OpenCV/Draw/Rectangle");
    pI::Arguments drawRectDepth_in = drawRectDepth->GetInputSignature(),
                  drawRectDepth_out = drawRectDepth->GetOutputSignature();

    // OpenCV/Draw/PutText draws a text string into an image.
    boost::shared_ptr<pI::pIn> puttext
    = app.SpawnAndInitialize ("OpenCV/Draw/PutText",
                              0,                // font type: CV_FONT_HERSHEY_SIMPLEX
                              pI_FALSE,         // render font italic: no
                              0.75, 0.75,       // horizontal and vertical scale
                              0.0,              // shear: no shear
                              1,                // thickness of the text strokes: 1
                              2                 // type of the line: CV_AA
                             );
    pI::Arguments puttext_in = puttext->GetInputSignature(),
                  puttext_out = puttext->GetOutputSignature();

    // OpenCV/IO/SaveImage saves images to disk.
    boost::shared_ptr<pI::pIn> saveImage (runtime.SpawnPlugin ("OpenCV/IO/SaveImage"));
    saveImage->Initialize (saveImage->GetParameterSignature());
    pI::Arguments saveImage_in = saveImage->GetInputSignature(),
                  saveImage_out = saveImage->GetOutputSignature();

    // PCL/DepthToXYZ converts the depth matrix to a matrix containing real world distance in meters.
    boost::shared_ptr<pI::pIn> dep2meters;
    pI::Arguments dep2meters_in, dep2meters_out;

    boost::shared_ptr<pI::pIn> dep2xyz;
    pI::Arguments dep2xyz_in, dep2xyz_out;

    boost::shared_ptr<pI::pIn> savePoints;
    pI::Arguments savePoints_in, savePoints_out;

    if (usePCL) {
        dep2meters = app.SpawnAndInitialize ("PCL/DepthToMeters");
        dep2meters_in = dep2meters->GetInputSignature();
                  dep2meters_out = dep2meters->GetOutputSignature();

    // PCL/DepthToXYZ converts the depth matrix to a real world (x,y,z) array.
        dep2xyz = app.SpawnAndInitialize ("PCL/DepthToXYZ");
        dep2xyz_in = dep2xyz->GetInputSignature();
                  dep2xyz_out = dep2xyz->GetOutputSignature();

    // PCL/SaveXYZ saves point clouds to disk.
        savePoints = app.SpawnAndInitialize ("PCL/SaveXYZ");
        savePoints_in = savePoints->GetInputSignature();
                  savePoints_out = savePoints->GetOutputSignature();
    }

    // Now that all plugins are initialized, connect them.

    drawRectImage_in[0] = capture_out[0];
    pI::IntValue (drawRectImage_in[1]).SetData (310);
    pI::IntValue (drawRectImage_in[2]).SetData (230);
    pI::IntValue (drawRectImage_in[3]).SetData (330);
    pI::IntValue (drawRectImage_in[4]).SetData (250);
    pI::IntValue (drawRectImage_in[5]).SetData (255);
    pI::IntValue (drawRectImage_in[6]).SetData (255);
    pI::IntValue (drawRectImage_in[7]).SetData (255);
    pI::IntValue (drawRectImage_in[8]).SetData (1);
    pI::StringSelection (drawRectImage_in[9]).SetIndex (0);
    pI::IntValue (drawRectImage_in[10]).SetData (0);

    displayImage_in[0] = drawRectImage_in[0];

    pI::StringValue fileName (saveImage_in[0]);
    saveImage_in[1] = capture_out[0];

    applyRGB_in[0] =  capture_out[1];
    drawRectDepth_in[0] = applyRGB_out[0];
    pI::IntValue (drawRectDepth_in[1]).SetData (310);
    pI::IntValue (drawRectDepth_in[2]).SetData (230);
    pI::IntValue (drawRectDepth_in[3]).SetData (330);
    pI::IntValue (drawRectDepth_in[4]).SetData (250);
    pI::IntValue (drawRectDepth_in[5]).SetData (255);
    pI::IntValue (drawRectDepth_in[6]).SetData (255);
    pI::IntValue (drawRectDepth_in[7]).SetData (255);
    pI::IntValue (drawRectDepth_in[8]).SetData (1);
    pI::StringSelection (drawRectDepth_in[9]).SetIndex (0);
    pI::IntValue (drawRectDepth_in[10]).SetData (0);
    displayDepth_in[0] = drawRectDepth_in[0];

    pI::StringValue puttext_text (puttext_in[1]);
    pI::IntValue puttext_x (puttext_in[2]);
    pI::IntValue puttext_y (puttext_in[3]);
    pI::IntValue puttext_R (puttext_in[4]);
    pI::IntValue puttext_G (puttext_in[5]);
    pI::IntValue puttext_B (puttext_in[6]);
    pI::BoolValue puttext_do_draw (puttext_in[7]);

    puttext_in[0] = drawRectDepth_in[0];
    puttext_R.SetData (255);
    puttext_G.SetData (255);
    puttext_B.SetData (255);
    puttext_do_draw.SetData (pI_TRUE);

    puttext_x.SetData (285);
    puttext_y.SetData (272);

    // As both input arguments of Freenect/GetVideoAndDepth are optional, these are just
    // provided as needed. Initially, the Kinect is set to tile angle 0, and LED_GREEN.
    int tilt = 0;

    pI::Arguments capture_in;
    capture_in.push_back (pI::IntValue (runtime).SetData (0)); // Tilt angle 0 degree
    capture_in.push_back (pI::IntValue (runtime).SetData (1)); // LED status LED_GREEN

    boost::timer start, now;
    boost::timer lastTimeSaved;
    int seqNumber = 0;

    bool loop = true;
    enum {None, Once, Sequence} save = None;
    bool showDistance = false;

    do {
        // Tries to capture the next frame. If that fails, assumes the
        // camera is disconnected, and stop looping.

        try {
            // Get Depth and RGB data, ...
            capture->Execute (capture_in, capture_out);
            capture_in.clear(); // Remember, input arguments are just provided as needed.

            // ... apply depth to color image conversion, ...
            applyRGB->Execute (applyRGB_in, applyRGB_out);

            if (usePCL && showDistance) {
            // ... draw a white rectangle into the center of both image and map, ...
                drawRectImage->Execute (drawRectImage_in, drawRectImage_out);
                drawRectDepth->Execute (drawRectDepth_in, drawRectDepth_out);

                // ... and the result of distance measurement.
                pI::ShortMatrix depth (capture_out[1]);

                pI::ShortMatrix mid (dep2meters_in[0]);
                if(!mid.HasData()) {
                    mid.SetCols(21).SetRows(21);
                }

                for (int j = 230; j <= 250; ++j) {
                    for (int i = 310; i <= 330; ++i) {
                        int dep = depth.GetData (j, i);
                        mid.SetData (j-230,i-310,dep);
                    }
                }
                
                dep2meters->Execute (dep2meters_in, dep2meters_out);

                pI::DoubleMatrix dist (dep2meters_out[0]);

                double sumDistance = 0.;
                int sumCount = 0;

                for (int j = 0; j <= 20; ++j) {
                    for (int i = 0; i <= 20; ++i) {
                        sumDistance += dist.GetData(j,i);
                        ++sumCount;
                    }
                }

                double avgDistance = 0.;
                if (sumCount > 0) {
                    avgDistance = sumDistance / (double) sumCount;
                }

                char str[32];
                sprintf (str, "%.2f m", avgDistance);
                puttext_text.SetData (str);

                puttext->Execute (puttext_in, puttext_out);
            }
        }
        catch (pI::exception::ExecutionException&) {
            loop = false;
        }

        // Handles key strokes to quit, or to save a snapshot.
        if (kbhit()) {
            int ch = tolower (getch());

            switch (ch) {
                case 'q':
                case 27 /* ESC */: // Quits the application
                    capture_in.push_back (pI::IntValue (runtime).SetData (0)); // Tilt angle 0 degree
                    capture_in.push_back (pI::IntValue (runtime).SetData (4)); // LED status LED_BLINK_GREEN
                    capture->Execute (capture_in, capture_out);
                    loop = false;
                    break;

                case 'm':          // Measurement mode
                    showDistance = !showDistance;
                    break;

                case 's':          // Saves a single snapshot
                    save = Once;
                    break;
                case 't':          // Toggles snapshot sequence on/off
                    save = (save == None ? Sequence : None);
                    break;
                case '+':          // Increases tilt angle
                case 'u':
                    capture_in.push_back (pI::IntValue (runtime).SetData (++tilt));
                    break;
                case '-':          // Decreases tilt angle
                case 'd':
                    capture_in.push_back (pI::IntValue (runtime).SetData (--tilt));
                    break;
                default:
                    break;
            }
        }

        if (save == Once || (save == Sequence && lastTimeSaved.elapsed() > 1.)) {

            std::string frameNumber =
                (save == Once ? Poco::DateTimeFormatter::format (Poco::LocalDateTime(), "%Y-%m-%d_%H-%M-%S")
                 : (boost::format ("%06d") % (++seqNumber)).str()
                );

            std::string frameName = std::string ("frame_") + frameNumber + std::string (".png");

            saveImage_in[0] = pI::StringValue (runtime).SetData (frameName.c_str());
            saveImage_in[1] = capture_out[0];

            saveImage->Execute (saveImage_in);

            std::cout << "-> " << frameName << "     " << std::endl;

            // Saves PCL point cloud
            if (usePCL && save == Once) {
                dep2xyz_in[0] = capture_out[1];
                dep2xyz->Execute (dep2xyz_in, dep2xyz_out);

                std::string cloudName = std::string ("pointcloud_") + frameNumber + std::string (".pcd");
                savePoints_in[0] = pI::StringValue (runtime).SetData (cloudName.c_str());
                savePoints_in[1] = dep2xyz_out[0];

                savePoints->Execute (savePoints_in, savePoints_out);

                std::cout << "-> " << cloudName << std::endl;
            }

            if (save == Once) {
                save = None;
            }

            lastTimeSaved.restart();
        }

        // Shows some additional infos for camera.
        std::cout << (1. / now.elapsed()) << " fps        \r";
        now.restart();

        if (!loop) {
            std::cout << std::endl << "Capturing " << name << " finished after " << start.elapsed() << " secs." << std::endl;
        }

        // Display both the image and the depth map.
        displayImage->Execute (displayImage_in, displayImage_out);
        displayDepth->Execute (displayDepth_in, displayDepth_out);
    }
    while (loop);

    return 0;
}
