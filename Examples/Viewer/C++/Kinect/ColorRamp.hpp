/* ColorRamp.hpp
 *
 * Copyright 2011 Johannes Kepler Universit�t Linz,
 * Institut f�r Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KINECTVIEWER_CPP_COLORRAMP_HPP
#define KINECTVIEWER_CPP_COLORRAMP_HPP

#include <cstddef>
#include <stdint.h>
#include <cmath>
#include <map>


uint32_t red (uint32_t hex);

uint32_t green (uint32_t hex);

uint32_t blue (uint32_t hex);


/**
 * Builds a color ramp which ramps between n base colors given as a map.
 * Color values are given in hexadecimal form, i.e. red is 0xFF0000, green is 0x00FF00, etc.
 * Inspired by http://paulbourke.net/texture_colour/colourramp/.
*/
struct ColorRamp {
    ColorRamp (const std::map<size_t, uint32_t>& colors)
        : _colors (colors)
    {}

    uint32_t GetColor (size_t v) const;

private:
    std::map<size_t, uint32_t> _colors;
};

#endif // KINECTVIEWER_CPP_COLORRAMP_HPP
