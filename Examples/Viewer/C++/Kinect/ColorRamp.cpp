/* ColorRamp.cpp
 *
 * Copyright 2011 Johannes Kepler Universit�t Linz,
 * Institut f�r Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ColorRamp.hpp"

uint32_t red (uint32_t hex) {
    return (hex >> 16) & 0xFF;
}

uint32_t green (uint32_t hex) {
    return (hex >> 8) & 0xFF;
}

uint32_t blue (uint32_t hex) {
    return hex & 0xFF;
}

uint32_t ColorRamp::GetColor (size_t v) const {

    std::map<size_t, uint32_t>::const_iterator it = _colors.find (v);

    // If key v is contained in the map, returns the corresponding color.
    if (it != _colors.end()) {
        return it->second;
    }

    std::map<size_t, uint32_t>::const_iterator upr = _colors.upper_bound (v);

    // If v is smaller than the smallest, or larger than the largest key,
    // returns the color of the smallest or largest element, resp.
    if (upr == _colors.begin()) {
        return upr->second;
    }

    if (upr == _colors.end()) {
        return (--upr)->second;
    }

    // Otherwise, interpolates between the colors of the smaller and larger elements.
    std::map<size_t, uint32_t>::const_iterator lwr = upr;
    --lwr;

    double ratio = ( (double) v - (double) lwr->first) / ( (double) upr->first - (double) lwr->first);

    double cr = (1. - ratio) * red (lwr->second)   + ratio * red (upr->second);
    double cg = (1. - ratio) * green (lwr->second) + ratio * green (upr->second);
    double cb = (1. - ratio) * blue (lwr->second)  + ratio * blue (upr->second);

    uint32_t c = ( (uint32_t) floor (cr) << 16)
                 | ( (uint32_t) floor (cg) << 8) | ( (uint32_t) floor (cb));

    return c;
}
