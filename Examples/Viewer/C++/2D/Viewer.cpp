/* Viewer.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdio>
#include <cctype>

#ifndef __GNUC__
#include <conio.h>
#else
#include "gcc_conio.h"
#endif

#include <algorithm>
#include <iostream>

#include <boost/array.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>
#include <boost/timer.hpp>

#include <Poco/File.h>
#include <Poco/Path.h>
#include <Poco/DateTimeFormatter.h>

#include <PureImage.hpp>
#include <Application.hpp>
#include <Arguments.hpp>


int main (int argc, char** argv) {

    const bool USE_OPENCV_DISPLAY = true; // Use OpenCV or CImg for display?

    enum {Image, Video, Camera} type;

    std::string name;

    if (argc < 2) {
        type = Camera;
        name = std::string ("camera");
    }
    else {
        // Interprets the passed argument as a file name, ...
        std::string fileName = std::string (argv[1]);

        Poco::Path filePath (fileName);
        filePath.makeAbsolute();

        name = filePath.toString();
        std::string ext = filePath.getExtension();

        // ..., makes sure the file exists, ...
        if (! (Poco::File (filePath).canRead())) {
            std::cerr << "Cannot open file " << name << std::endl;
            return 1;
        }

        // ..., and decides whether it is an image or a video.
        boost::array<std::string, 13> ImageExts = { "BMP", "DIB", "JPEG", "JPG", "JPE", "PNG", "PBM", "PGM", "PPM", "SR", "RAS", "TIFF", "TIF" };

        bool isImage = (std::find (ImageExts.begin(), ImageExts.end(), boost::to_upper_copy (ext)) != ImageExts.end());

        type = (isImage ? Image : Video);

        std::cout << name << " (extension " << boost::to_upper_copy (ext) << ") will be treated as an "
                  << (type == Image ? "image file" : "video file") << std::endl;
    }

    // Creates runtime and base app
    pI::Application app;
    pI::Runtime runtime (app.GetRuntime());

    try {
		app.LoadPluginLibrary (app.FindPIPluginDir().c_str(), "CImg_pIns");
        app.LoadPluginLibrary (app.FindPIPluginDir().c_str(), "OpenCV_pIns");
    }
    catch (pI::exception::PluginLibraryException& xc) {
        std::cerr << "Error in LoadPluginLibrary(): " << xc.what() << std::endl;
        return 1;
    }

    app.RegisterPlugins();

    // In the sequel, all required plug-ins are created and initialized.

    // OpenCV/IO/CaptureFromCAM captures frames from a camera device.
    // OpenCV/IO/LoadImage loads an image file.
    // OpenCV/IO/CaptureFromFile captures frames from a video file.
    boost::shared_ptr<pI::pIn> capture = runtime.SpawnPlugin (
            (type == Camera ? "OpenCV/IO/CaptureFromCAM"
             : (type == Image ? "OpenCV/IO/LoadImage" : "OpenCV/IO/CaptureFromFile")));

    pI::Arguments capture_params = capture->GetParameterSignature();

    if (capture_params.size() >= 1) {
        if (type == Camera) {
            pI::IntValue (capture_params[0]).SetData (-1); // Lets OS select the camera device
        }
        else if (type == Video) {
            pI::StringValue (capture_params[0]).SetData (name.c_str());
        }
    }

    try {
        capture->Initialize (capture_params);
    }
    catch (pI::exception::InitializationException& /*xc*/) {
        std::cerr << "Failed to initialize capture plugin." << std::endl;
        return 1;
    }

    pI::Arguments capture_in  = capture->GetInputSignature(),
                  capture_out = capture->GetOutputSignature();

    if (type == Image) {
        pI::StringValue (capture_in[0]).SetData (name.c_str());
        pI::BoolValue (capture_in[2]).SetData (USE_OPENCV_DISPLAY ? false : true); // Swap BGR to RGB only if not using OpenCV's display method
    }

    // OpenCV/IO/ShowImage or CImg/Display displays the image(s).
    boost::shared_ptr<pI::pIn> display = app.SpawnAndInitialize (
            (USE_OPENCV_DISPLAY ? "OpenCV/IO/ShowImage" : "CImg/Display"),
            (std::string ("pureImage viewer - ") + name).c_str(),            // Window title
            ! (type == Image),                                               // Enables live display if not a stand-alone image.
            false,                                                           // Does not swap BGR to RGB
            300, 100                                                         // Initial window position
                                         );

    pI::Arguments display_in = display->GetInputSignature(),
                  display_out = display->GetOutputSignature();

    // OpenCV/IO/SaveImage saves images to disk.
    boost::shared_ptr<pI::pIn> write (runtime.SpawnPlugin ("OpenCV/IO/SaveImage"));
    write->Initialize (write->GetParameterSignature());
    pI::Arguments write_in = write->GetInputSignature(),
                  write_out = write->GetOutputSignature();

    boost::timer start, now;
    boost::timer lastTimeSaved;
    int seqNumber = 0;

    bool loop = ! (type == Image);
    enum {None, Once, Sequence} save = None;

    do {
        // Tries to capture the next frame. If that fails, assumes the video has ended,
        // or the camera is disconnected, and stop looping.
        try {
            capture_out = capture->Execute (capture_in);
        }
        catch (pI::exception::ExecutionException&) {
            loop = false;
        }

        // Handles key strokes to quit, or to save a snapshot.
        if (kbhit()) {
            int ch = tolower (getch());

            switch (ch) {
                case 'q':
                case 27 /* ESC */: // Quits the application
                    loop = false;
                    break;
                case 's':          // Saves a single snapshot
                    save = Once;
                    break;
                case 't':          // Toggles snapshot sequence on/off
                    save = (save == None ? Sequence : None);
                    break;
                default:
                    break;
            }
        }

        if (save == Once || (save == Sequence && lastTimeSaved.elapsed() > 1.)) {


            std::string frameNumber =
                (save == Once ? Poco::DateTimeFormatter::format (Poco::LocalDateTime(), "%Y-%m-%d_%H-%M-%S")
                 : (boost::format ("%06d") % (++seqNumber)).str()
                );

            std::string frameName = std::string ("frame_") + frameNumber + std::string (".png");

            write_in[0] = pI::StringValue (runtime).SetData (const_cast<char*> (frameName.c_str()));
            write_in[1] = capture_out[0];

            try {
                write->Execute (write_in);
                std::cout << "-> " << frameName << "     " << std::endl;
            }
            catch (...) {
                frameName = std::string (getenv ("tmp")) + Poco::Path::separator() + frameName;
                write_in[0] = pI::StringValue (runtime).SetData (const_cast<char*> (frameName.c_str()));

                try {
                    write->Execute (write_in);
                    std::cout << "-> " << frameName << "     " << std::endl;
                }
                catch (...) {
                    std::cerr << "Could not save file " << frameName << std::endl;
                }
            }

            if (save == Once) {
                save = None;
            }

            lastTimeSaved.restart();
        }

        // Shows some additional infos for camera or videos.
        if (type == Camera) {
            std::cout << (1. / now.elapsed()) << " fps        \r";
            now.restart();
        }
        else if (type == Video) {
            double cdc = pI::DoubleValue (capture_out[7]).GetData();
            char codec[4];
            memcpy (codec, &cdc, 4);

            std::cout << "Codec " << std::string (codec)
                      << ", " << pI::DoubleValue (capture_out[4]).GetData()
                      << "x " << pI::DoubleValue (capture_out[5]).GetData()
                      << ", " << pI::DoubleValue (capture_out[6]).GetData() << " fps"
                      << ", frame " << pI::DoubleValue (capture_out[2]).GetData()
                      << " of " << pI::DoubleValue (capture_out[8]).GetData()
                      << ", " << pI::DoubleValue (capture_out[1]).GetData() << " msec"
                      << " (" << (1. / now.elapsed()) << " fps)        \r";

            now.restart();
        }

        if (!loop) {
            std::cout << std::endl << "Capturing " << name << " finished after " << start.elapsed() << " secs." << std::endl;
        }

        // Displays the image
        display_in[0] = capture_out[0];
        display_out = display->Execute (display_in);
    }
    while (loop);

    return 0;
}
