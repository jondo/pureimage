rem configure pureImage with core, plugins, examples and documentation
call configure-win32-vc10.bat cped

rem Building ...
rem CAVEAT: This only works with CMake 2.8.5-rc1 or -rc2, see issues #133 and #187.
call "%VS100COMNTOOLS%vsvars32.bat"
msbuild build-win32-vc10\pureImage.sln /p:Configuration=Release /p:Platform=Win32

rem Compiling the installer ...
iscc installer-win32.iss /dBuildDir="build-win32-vc10"

pause
