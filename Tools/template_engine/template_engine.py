"""
    template_engine.py is a python module that provides a templating engine that allows templating in a pythonic way.
    
    Existing templating engines provide means to substitute variables.
    For more complex templating with logic and control flow -- which is what such a template engine should go for -- 
    typically a special syntax is provided to embed logic and control statements into the template.
    Though, from my experience this leads almost always to unreadable template code that is hard to understand
    after a certain time.

    
    Basic Templating:
    -----------------
    
    For basic, primitive templating the python 'string' library is adopted. string.Template allows to process templates
    containing '${<name>}' expressions (or '$<name>' as short version) to expand variables that are provided within a dictionary.

    Example:
        > mydict = {'foo': 'bar'}
        > print string.Template("The value of foo is ${foo}").substitute(mydict)
        The value of foo is bar

    Nevertheless, this API does not allow to evaluate more complex expressions, such as list access etc.
    For that, template_engine.py extends the mechanism by scanning the template for ${} expressions and evaluates these dynamically 
    in the callee's context (i.e., with access local and global variables).

    Example:
    
        > a = [1,2,3]
        > print expand_template(Template("The second element of a is ${a[1]}"))
        The second element of a is 2
    
    Note: only expressions are allowed, not statements (see difference between eval and exec)
    
    It is also possible to define helper functions and use them from the template. Such definitions
    are either defined in the callee's global or local context which are proposed to the dynamic evaluations during template processing.
    
    Example:
    
        > def hello():
        >   return 'hello'
        >
        > print expand_template(Template("Say Hello!\n${hello}"))
        Say Hello!
        hello

    Sometimes one does not want to use variables but instead use an extra dictionary.
     
    Example:
        > some_dict = {'foo', 'bar'}
        > print expand_template(Template(""), userdata=some_dict)

    Working with Templates
    ----------------------
    
    To keep the overall templating mechanism transparent and understandable it is a good idea to separate
    templates from logic and control flow.

    Example:
    
         from template_engine import *

        ##################################################################
        ## Templates: 

        t_doc = Template(
        " ""
        ${doc.title}
        ${title_bar(doc.title, level=1)}
            
        $SECTIONS
        " "")

        t_section = Template(
        " ""
        ${section.title}
        ${title_bar(section.title, level=2)}

        ${section.content}
        " "")

        ##################################################################
        # Helpers:

        def title_bar(s, level=1):
            c = None
            if level==1:
                c = '='
            else:
                c = '-'
            return len(s)*c

        ##################################################################
        ## Logic: 

        if __name__ == "__main__":
            doc = {
                "title" : "Some doc",
                "sections": [
                    {"title": "Section 1", "content": "Bla\nBlupp\n"},
                    {"title": "Section 2", "content": "Foo\nBar\n"}
                ]
            }
            # allows dict access in 'attribute' manner (i.e., 'a.b') 
            doc = to_attr_dict(doc)
            
            SECTIONS = StringBuffer()
            for section in doc.sections:
                SECTIONS << expand_template(t_section)
            
            DOC = StringBuffer()
            DOC << expand_template(t_doc)
            
            print DOC
    
"""
import string
import re
import inspect
import os
import json
import types
import codecs
from StringIO import StringIO
from os.path import isfile

class AttrDict(dict):

    def __init__(self, d):
        self.update(d)
        
    def __getattr__(self, name):
        if not self.has_key(name):
            raise KeyError(name)
        return self[name]

class StringBuffer(StringIO):

    def __lshift__(self, other):
        self.write(other)
        return self

    def println(self):
        self.write("\n")
        return self
        
    def __str__(self):
        return self.getvalue()
    
class Template():
    def __init__(self, s):
        caller = inspect.stack()[1]
        self.source_file = caller[1]
        self.end_line = caller[2]
        # note: removing first line as it is (typically) used to start template block '"""'
        idx = s.find("\n")
        self.string = s[idx+1:]
    
    def get_err_location(self, startpos, endpos):
        lines = self.string[startpos:].splitlines()
        line = self.end_line - len(lines)
        col = startpos - self.string.rfind("\n", 0, startpos)
        return "%s: line %d, column %d"%(os.path.basename(self.source_file), line, col)

def union(d1, d2):
    return dict(d1.items() + d2.items())

def to_attr_dict(d):
    if type(d) is types.DictType:
        d = AttrDict(d)
        for key in d.iterkeys():
            d[key] = to_attr_dict(d[key])
    elif type(d) is types.ListType or type(d) is types.TupleType:
        for idx,elem in enumerate(d):
            d[idx] = to_attr_dict(d[idx])
    
    return d
    
def expand_template(t, userdata={}, output=None, skip_locals=False):

    data = {}
    if not skip_locals:
        # retrieve locals from callee stack frame
        caller = inspect.stack()[1]
        data.update(caller[0].f_locals)
    data.update(userdata)
    
    _globals = caller[0].f_globals
        
    s = StringIO()
    vars = {}
    
    re_var = re.compile("[$]\{(.+?)\}")
    pos = 0
    idx = 0
    str = t.string
    
    def error(err_type):
        print("%s in template code:\n %s, %s"%(err_type, t.get_err_location(match.start(), match.end()), match.group(0)))
        exit()
    
    for match in re_var.finditer(str):
        if(match.start() >= pos):
            s.write(str[pos:match.start()])
            pos = match.end()
        id = '_%d'%(idx)
        s.write("${%s}"%(id))
        
        stmt = match.group(1)
        try:
            vars[id] = eval(stmt, _globals, data)

        except SyntaxError as se:
            print(se)
            error("SyntaxError")
        except AttributeError as ae:
            print(ae)
            error("AttributeError")
        except KeyError as ke:
            print(ke)
            error("KeyError")
        except NameError as ne:
            print(ne)
            error("NameError")
        idx += 1

    s.write(str[pos:])
    
    try:
        s = string.Template(s.getvalue()).substitute(union(data, vars))
    except ValueError as ve:
        print(ve)
        print("Invalid template:\n%s"%(s.getvalue()))
        exit()
    
    if output==None:
        return s
    else:
        print >>output, s

def mkdir_quiet(path):
    if not os.path.exists(path):
        os.makedirs(path)

def open_file(filename):
    mkdir_quiet(os.path.dirname(filename))
    return codecs.open(filename, 'w', 'utf-8')
            
def load_file(path):
    if not isfile(path):
        raise RuntimeError('File not found')

    content = ''
    with open(path, 'r') as f:
        content = f.read()

    return content

# this is used to sanitize 'un-strict' json (e.g., trailing comma in list, '' instead of "" in keys, ';' instead of ',' in lists etc.
def sanitized_json(s, reformat=False):
    # convert '' in keys to "": officially '<str>' is not a legal json string
    s = s.replace('\'', '"')
    s = s.replace(';', ',')
    
    #remove trailing commas at the end of lists
    def return_rbrack(m):
        return m.group(1)

    s = re.sub(",\s*([\]\}])", return_rbrack, s)
    
    # reformat? re-encode
    if reformat:
        content = json.JSONDecoder().decode(s)
        s = JSONEncoder().encode(content)
   
    return s

def load_json(path, sanitize=False):
    s = load_file(path)
    if sanitize:
        s = sanitized_json(s)
    
    # a wrapping function that creates dict instances that also can be accessed using keys as attributes
    def wrap_dict(d):
        return AttrDict(d)

    try:
        content = json.JSONDecoder(object_hook=wrap_dict).decode(s)
        return content
    except ValueError as e:
        print("Could not parse json:\n%s"%(s));
        raise e

def store_json(path, obj):
    with open(path, 'w') as f:
        json.dump(obj, f);

INDENT_SPACES = 4

def indent(n):
    return ' ' * INDENT_SPACES * n

ENDL = "\n"
