# encoding: utf-8
from template_engine import *

license_text = """
%s

Copyright 2010-2011 Johannes Kepler Universität Linz,
Institut für Wissensbasierte Mathematische Systeme.

This file is part of pureImage.

pureImage is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 3,
as published by the Free Software Foundation.

pureImage is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pureImage. If not, see <http://www.gnu.org/licenses/>.

"""

auto_generation_warning = """
Attention this file has been generated automatically.
Any changes will be probably lost.
Instead you should edit the generator file: %s

"""

def cpp_block_comment(s):
    out = StringBuffer()
    out << "/*" << ENDL
    for line in s.splitlines(True):
        out << "* " << line
    
    out << "*/" << ENDL
    return out
    
def matlab_line_comment(s):
    out = StringBuffer()
    for line in s.splitlines(True):
        out << "% " << line
    return out

def python_line_comment(s):
    out = StringBuffer()
    for line in s.splitlines(True):
        out << "# " << line
    return out
