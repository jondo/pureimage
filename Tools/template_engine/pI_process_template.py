from template_engine import *
from StringIO import StringIO
import json
import sys
from itertools import chain
import os.path

if __name__ == "__main__":

    typedefs_file = sys.argv[1]
    outdir = sys.argv[2]
    templates = sys.argv[3]

#    print("given templates %s"%(templates))

#    @next line: does not work as intended, as os.pathsep yields : under linux, yet paths are separated via ;
#    templates = templates.split(os.pathsep)
    templates = templates.split(';')
    
    try:
        typedefs = load_json(typedefs_file, sanitize=True)
    except ValueError as e:
        print("Could not parse json:\n%s"%(str));
        raise e

    vars = {
            "__root__": typedefs,
            "__outdir__": outdir
    }

    # gather the set of setup field names that are used for creating data
    sf = {}
    for arg in typedefs['arguments']:
        # WORKAROUND: using chain.from_iterable, as the created list does result in hashable strings - don't know why.
        sf[arg.name] = set(chain.from_iterable([d.sizes for d in arg.data if d.dim > 0]))
    vars['dependent_setup_fields'] = sf

        # create a mapping for intrinsic names to the specific type
    # this is necessary to lookup 
    intrinsic_types = dict([(t.name,t.type) for t in typedefs['intrinsics']])

    # Note: At this time recursive type definitions are resolved only shallowly.
    #       These types have to be resolved recursively.
    for key in intrinsic_types.iterkeys():
        _type = intrinsic_types[key]
        while intrinsic_types.has_key(_type):
            _type = intrinsic_types[_type]
        intrinsic_types[key] = _type
    vars['intrinsic_types'] = intrinsic_types
    
    argument_types = set([arg.name for arg in typedefs['arguments']])
    vars['argument_types'] = argument_types
    
    structure_types = set([s.name for s in typedefs['structures']])
    vars['structure_types'] = structure_types

    vars = to_attr_dict(vars)

    print("templates %s"%(templates))

    for template in templates:
        _dir,_file =  os.path.split(template)
        _module, _ = os.path.splitext(_file)
        print("Processing template %s"%(template))
	#print("Processing module %s"%(_module))
	#print("Processing dir %s"%(_dir))
	#print("Processing file %s"%(_file))
        try:
            tmp = __import__(_module, globals(), locals(), [], 0)
        except ImportError:
            sys.path.append(_dir)
            tmp = __import__(_module, globals(), locals(), [], 0)
            
        tmp.process(vars)
