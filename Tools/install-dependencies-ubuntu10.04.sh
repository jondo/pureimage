sudo apt-get install git-core
sudo apt-get install cmake
sudo apt-get install libboost1.40-all-dev
sudo apt-get install libpoco-dev # this is version 1.3.6
sudo apt-get install libjson0 libjson0-dev # JSON-C
sudo apt-get install libcv-dev libhighgui-dev # this is OpenCV 2.0.0
sudo apt-get install cimg-dev # this is CImg 1.3.2
sudo apt-get install default-jdk # this is openjdk-6-jdk
sudo apt-get install pandoc
sudo apt-get install doxygen
sudo apt-get install texlive # installs pdflatex and bibtex
sudo apt-get install flex
sudo apt-get install bison
sudo apt-get install tesseract-ocr-dev

# and install swig2.0 from
# https://launchpad.net/~fenics/+archive/ppa/+packages?field.name_filter=swig2.0&field.status_filter=published&field.series_filter=lucid
# sudo add-apt-repository ppa:fenics/ppa
# sudo apt-get update
# sudo apt-get install swig2.0

# also useful:
sudo apt-get install nautilus-open-terminal git-gui gitk
# Use the commands "LANG=C git gui" and "LANG=C gitk" to enforce English texts

