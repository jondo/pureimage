sudo apt-get install cmake
sudo apt-get install libboost-all-dev # also installs gcc
sudo apt-get install libpoco-dev
sudo apt-get install libjson0-dev # JSON-C
sudo apt-get install libhighgui-dev # OpenCV
sudo apt-get install libcvaux-dev # OpenCV
sudo apt-get install cimg-dev
sudo apt-get install default-jdk
sudo apt-get install pandoc
sudo apt-get install doxygen
sudo apt-get install texlive # installs pdflatex and bibtex
sudo apt-get install flex
sudo apt-get install bison
sudo apt-get install tesseract-ocr-dev
sudo apt-get install swig2.0

# and install libfreenect from https://launchpad.net/~floe/+archive/libtisch:
# sudo add-apt-repository ppa:floe/libtisch
# sudo apt-get update
# sudo apt-get install libfreenect-dev

# also useful:
sudo apt-get install nautilus-open-terminal git-gui gitk
# Use the commands "LC_ALL=C git gui" and "LC_ALL=C gitk" to enforce English texts

