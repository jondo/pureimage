Improving FindJava.cmake and FindJNI.cmake
==========================================

For hosting a jvm it is necessary to choose a concrete jdk to link against.
Under windows the existing FindJNI.cmake and also FindJava.cmake prefer windows registry entries above anything else. IMO, this reduces controlability unnecessarily.

A simple change ot these modules allows to control this using the environment variable JAVA_HOME.
Also, setting all java related paths manually is obsolete then.

OB filed an issue: http://public.kitware.com/Bug/view.php?id=12056

Status
......

Patched module file are versioned, patches acompany.
To generate patched FindJava.cmake and FindJNI.cmake get http://cmake.org/gitweb?p=stage/cmake.git;a=commit;h=7e977287543d03b5f280b9f0b00498cfab4f0cab
and apply patches.

The issue is fresh and open. Patches have been attached to issue. Awaiting comments from developers.


Files
.....

From http://cmake.org/gitweb?p=stage/cmake.git;a=commit;h=7e977287543d03b5f280b9f0b00498cfab4f0cab:
 - FindJava.cmake
 - FindJNI.cmake
 - FindPackageHandleStandardArgs.cmake

Patches:
 - FindJava.cmake patched with 0001-Allow-overriding-of-win-registry-search-in-FindJava.patch
 - FindJNI.cmake patched with 0002-Allow-overriding-of-win-registry-search-in-FindJNI.patch

