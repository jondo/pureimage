UseJava.cmake
=============

This is a new feature for better supp of Java by CMake
This work has been developed in a feature branch 'asn_java_support' on cmake stage

    http://cmake.org/gitweb?p=stage/cmake.git;a=shortlog;h=refs/heads/asn_java_support

Improving Rebuild Behaviour
---------------------------

OB was involved improving this module for better handling of java rebuilds.
This work is upstream since:
    http://cmake.org/gitweb?p=stage/cmake.git;a=commit;h=b01a50567a7ca190e9f39aa2db5f4b0042e9ca4c

Two more commits have been applied which are the two direct parents of this commit.
    - http://cmake.org/gitweb?p=stage/cmake.git;a=commit;h=f99c3120c91b9b659d4fce20d5578b6c6674dc3f
    - http://cmake.org/gitweb?p=stage/cmake.git;a=commit;h=f3233ba5211596df205e6d915b06bd412087c202


State
.....

The feature branch has been merged into a candidate branch for the next release:
    http://cmake.org/gitweb?p=stage/cmake.git;a=commit;h=7e977287543d03b5f280b9f0b00498cfab4f0cab

The head of this branch is here:
    http://cmake.org/gitweb?p=stage/cmake.git;a=shortlog;h=refs/heads/next

Dynamically generated Source Lists
----------------------------------

This feature has been issued by http://public.kitware.com/Bug/view.php?id=12055.

For now, a local patch is used to gain the desired functionality.
The versioned UseJava.cmake is patched already.

For monkey patching do:
> copy UseJava.cmake.orig UseJava.cmake
> patch UseJava.cmake 0001-Add-support-for-Java-source-list-files.patch
> patch UseJava.cmake 0002-Produce-a-LOCATION-variable-to-the-jar-target.patch
> patch UseJava.cmake 0003-Add-support-for-jar-entry-points.patch

UseJava.cmake.orig is 'Modules/UseJava.cmake' from http://cmake.org/gitweb?p=stage/cmake.git;a=commit;h=7e977287543d03b5f280b9f0b00498cfab4f0cab

State
.....

The issue is currently discussed with Andreas Schneider.
So far, no decision has been made.


Contacts
--------

- Author of CMake Java support: Andreas Schneider <asn_at_redhat.com>

- CMake bug tracker: http://public.kitware.com/Bug

Files
-----

From http://cmake.org/gitweb?p=stage/cmake.git;a=commit;h=7e977287543d03b5f280b9f0b00498cfab4f0cab
 - UseJava.cmake
 - UseJavaClassFilelist.cmake
 - UseJavaSymlinks.cmake

Patches:
 - UseJava.cmake with
    - 0001-Add-support-for-Java-source-list-files.patch
    - 0002-Produce-a-LOCATION-variable-to-the-jar-target.patch
    - 0003-Add-support-for-jar-entry-points.patch

