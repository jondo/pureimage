set(DIST_OUT_DIR "${CMAKE_CURRENT_BINARY_DIR}/libraries")

function(copy_to_dist _TARGET_NAME)
    get_target_property(_TARGET_OUTPUT ${_TARGET_NAME} LOCATION)

    add_custom_command( TARGET ${_TARGET_NAME}
                        POST_BUILD
                        #COMMAND ${CMAKE_COMMAND} -E make_directory ${DIST_OUT_DIR}
                        COMMAND ${CMAKE_COMMAND} -E copy ${_TARGET_OUTPUT} ${DIST_OUT_DIR}
                        COMMENT "Copy ${_TARGET_OUTPUT} to ${DIST_OUT_DIR}"
    )
endfunction (copy_to_dist)
