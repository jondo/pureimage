if(WIN32)
    set(SETENV set)
    set(PYTHON "python")
else()
    set(SETENV export)
    set(PYTHON "/usr/bin/python")
endif()

function(expand_templates)

    set(_state "none")
    set(_append "on")

    set (_OUTPUTS "")
    set (_DEPENDS "")
    set (_TEMPLATES "")
    set (_OUTDIR "")
    set (_WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
    
    foreach(a ${ARGN})
        if(${a} STREQUAL "OUTPUT")
            set(_state "_OUTPUTS")
            set(_append "ON")
        elseif(${a} STREQUAL "TEMPLATES")
            set(_state "_TEMPLATES")
            set(_append "ON")
        elseif(${a} STREQUAL "OUTDIR")
            set(_state "_OUTDIR")
            set(_append "OFF")
        elseif(${a} STREQUAL "DEPENDS")
            set(_state "_DEPENDS")
            set(_append "ON")
        elseif(${a} STREQUAL "WORKING_DIRECTORY")
            set(_state "_WORKING_DIRECTORY")
            set(_append "OFF")
        else()
            if(${_append})
                list(APPEND ${_state} ${a})
            else()
                set(${_state} ${a})
            endif()
        endif()
        
    endforeach()
    
    add_custom_command( OUTPUT ${_OUTPUTS}
                        DEPENDS ${_TEMPLATES} ${_DEPENDS}
                        WORKING_DIRECTORY ${_WORKING_DIRECTORY}
                        COMMAND ${SETENV} PYTHONPATH=${PROJECT_SOURCE_DIR}/Tools/template_engine
			COMMAND ${PYTHON} ${PROJECT_SOURCE_DIR}/Tools/template_engine/pI_process_template.py ${PI_TYPES_JSON} ${_OUTDIR} "${_TEMPLATES}"
                        COMMENT ${PYTHON} pI_process_template ${PI_TYPES_JSON} ${_OUTDIR} ${_TEMPLATES}
			VERBATIM
    )
endfunction (expand_templates)
