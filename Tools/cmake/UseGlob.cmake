set(GLOBBING_SCRIPT ${CMAKE_CURRENT_LIST_DIR}/CreateGlobFile.cmake)

function(add_filelist _BASE_DIR _GLOBBING_EXPRESSION _OUTPUT_FILE _DEPENDS)

#    message("Creating custom command for creating ${_OUTPUT_FILE}")
    add_custom_command(
        OUTPUT ${_OUTPUT_FILE}
        COMMAND ${CMAKE_COMMAND} 
            -DGLOBBING_EXPRESSION="${_GLOBBING_EXPRESSION}"
            -DBASE_DIRECTORY="${_BASE_DIR}"
            -DOUTPUT_FILE="${_OUTPUT_FILE}"
            -DGLOB_CREATE_RELATIVE_PATH=${GLOB_CREATE_RELATIVE_PATH}
            -P ${GLOBBING_SCRIPT}
        COMMENT "Creating ${_OUTPUT_FILE}"
        DEPENDS ${_DEPENDS}
        WORKING_DIRECTORY ${_BASE_DIR}
    )

endfunction (add_filelist)