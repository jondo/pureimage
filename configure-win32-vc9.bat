setlocal

set GENERATE_DOC=""
if %1x == x goto nodoc
set GENERATE_DOC=-D GENERATE_DOC=ON
:nodoc

mkdir build-win32-vc9
cd build-win32-vc9
cmake -C ..\cmake-win32-libs-vc9.conf -C ..\cmake-win32-tools.conf %GENERATE_DOC% -G "Visual Studio 9 2008" ..
cd ..

endlocal
