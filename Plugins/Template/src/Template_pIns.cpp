/* TEMPLATE_PINs.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <pInSharedLibraryExport.hpp>

#include "Template.hpp"

//                available
extern "C" pI_API pI_bool CreatepIns (pI_int index, struct _CRuntime* runtime, struct _CpIn** plugin) {

	if ((runtime == 0) || (index < 0))
        return pI_FALSE;

	// create a plugin if it has not been created already
    switch (index) {
		EXPORT_PIN_SC (0, runtime, pI::pIns::Template);
        default:
            return pI_FALSE;
    }
}; /* extern "C" pI_API pI_bool CreatepIns (pI_int index, struct _CRuntime* host, struct _CpIn** plugin) */

