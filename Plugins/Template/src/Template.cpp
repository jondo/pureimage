/* Template.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <ArgumentChecks.hpp>

#include "Template.hpp"

namespace pI {
namespace pIns {

Template::Template(Runtime& runtime): Base(runtime) {
}

/*virtual*/ Template::~Template() {}

/*virtual*/ void Template::_Initialize (const Arguments& parameters) {
	CheckParameters (parameters, _parameters);
	// put initialization code here...
}

/*virtual*/ void Template::_Execute (Arguments& input_args, Arguments& output_args) {

	// validate input arguments
	CheckInputArguments (input_args, GetInputSignature());
	CheckOutputArguments (output_args, GetOutputSignature());
	// put your actual implementation here...
}

} // namespace pIns
} // namespace pI
