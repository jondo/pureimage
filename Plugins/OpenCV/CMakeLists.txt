
if(WIN32)
    find_package(OpenCV REQUIRED) # For custom locations define OpenCV_DIR when calling CMake
    if (NOT ${OpenCV_FOUND})
        message(WARNING "Could not find OpenCV. It needs to be configured manually.")
        set (OpenCV_DIR "" CACHE PATH "from http://opencv.willowgarage.com/")
    endif()
else()
    # Try to find a custom OpenCV build
    # via the 'simple technique' described in http://opencv.willowgarage.com/wiki/FindOpenCV.cmake).
    #
    # Ubuntu's stock OpenCV cannot be found like this,
    # because it does not contain the OpenCVConfig.cmake file from OpenCV's CMake build.
    find_package(OpenCV) # For custom locations define OpenCV_DIR when calling CMake
endif()

include_directories(
    ${Boost_INCLUDE_DIRS}
    ${PI_CPP_INCLUDE_DIRS}
)

add_library(OpenCV_pIns MODULE
	${PROJECT_SOURCE_DIR}/Core/src/Adapters/C++/CpInAdapter.cpp
    src/CVCore.cpp
    src/CVDraw.cpp
    src/CVIO.cpp
    src/CVFeature.cpp
    src/CVSegment.cpp
    src/CVMLL.cpp
    src/Extensions/CVContourFeatures.cpp
    src/CVBase.cpp
    src/OpenCV_pIns.cpp
)

set_target_properties(OpenCV_pIns PROPERTIES
	DEBUG_POSTFIX "_d"
    COMPILE_FLAGS "-DpI_BUILD_SHARED_LIBRARY"
    FOLDER "Plugins"
)

# Link against Ubuntu's stock OpenCV release libraries, if no custom OpenCV build is configured
if(NOT ${OpenCV_FOUND})
    list(APPEND OpenCV_LIBS cv highgui ml)
endif()

target_link_libraries(
    OpenCV_pIns 
    ${OpenCV_LIBS}
)

#copy_to_dist(OpenCV_pIns)
# copy final shared library into libraries folder
ADD_CUSTOM_COMMAND(TARGET OpenCV_pIns
	POST_BUILD
	COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:OpenCV_pIns> ${LIBRARIES}/.
)

if(WIN32)
    configure_file(${OpenCV_DIR}/../doc/license.txt ${LIBRARIES}/OpenCV_license.txt COPYONLY)
    
    file(GLOB OPENCV_DLLS ${OpenCV_DIR}/bin/Release/opencv_*.dll) # Copies all Release DLLs
    foreach(OPENCV_DLL ${OPENCV_DLLS})
        configure_file(${OPENCV_DLL} ${LIBRARIES} COPYONLY)
    endforeach()
    
    file(GLOB OPENCV_D_DLLS ${OpenCV_DIR}/bin/Debug/opencv_*d.dll) # Copies all Debug DLLs
    foreach(OPENCV_D_DLL ${OPENCV_D_DLLS})
        configure_file(${OPENCV_D_DLL} ${LIBRARIES} COPYONLY)
    endforeach()    
endif()
