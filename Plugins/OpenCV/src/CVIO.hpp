/* CVIO.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CV_IO_HPP
#define CV_IO_HPP

#include "CVBase.hpp"
#include <opencv/highgui.h>

namespace pI {
namespace pIns {
namespace opencv {

class LoadImage: public CVBase {

    typedef CVBase Base;

public:

    LoadImage (Runtime& runtime) : Base (runtime) {

        _input_args.push_back (StringValue (runtime).SetName ("filename").SetDescription ("Name of file to be loaded"));
        StringSelection ssel (runtime);
        ssel.SetName ("iscolor")
        .SetDescription ("Specific color type of the loaded image: CV_LOAD_IMAGE_COLOR = the loaded image is forced to be a 3-channel color image; CV_LOAD_IMAGE_GRAYSCALE = the loaded image is forced to be grayscale; CV_LOAD_IMAGE_UNCHANGED = the loaded image will be loaded as is.")
        .SetCount (3);
        ssel.CreateData();
        ssel.SetSymbols (0, "CV_LOAD_IMAGE_COLOR");
        ssel.SetSymbols (1, "CV_LOAD_IMAGE_GRAYSCALE");
        ssel.SetSymbols (2, "CV_LOAD_IMAGE_UNCHANGED");
        ssel.SetIndex (2);
        _input_args.push_back (ssel);
        BoolValueInput ("swap_bytes", "if true, r and b bytes are swapped after loading", pI_TRUE);
        _output_args.push_back (ByteImage (runtime).SetName ("img").SetDescription ("file loaded with OpenCV"));
    }

    virtual ~LoadImage() {}

    DECLARE_VIRTUAL_COPY_CONSTRUCTOR (LoadImage)

    virtual const std::string GetDescription() const {
        return "Loads an image from a file.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/IO/LoadImage";
    }

protected:
	
	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
        CheckInputArguments (input_args, _input_args, 1);
        CheckOutputArguments (output_args, _output_args, 1);
        int iscolor = CV_LOAD_IMAGE_UNCHANGED;

        if (input_args.size() > 1) {
            CheckInputArgument (input_args[1], _input_args[1]);

            switch (StringSelection (input_args[1]).GetIndex()) {
                case 0: iscolor = CV_LOAD_IMAGE_COLOR;
                break;
                case 1: iscolor = CV_LOAD_IMAGE_GRAYSCALE;
                break;
            }
        }

        pI_bool swap (pI_TRUE);

        if (input_args.size() > 2) {
            CheckInputArgument (input_args[2], _input_args[2]);
            swap = BoolValue (input_args[2]).GetData();
        }

        IplImage* img (cvLoadImage (StringValue (input_args[0]).GetData(), iscolor));

        if (img == 0) {
            throw exception::ExecutionException ("Failed to load image using OpenCV");
        }

        // convert colour byte-ordering in-place (bgr->rgb)
        if (swap == pI_TRUE) {
            cvConvertImage (img, img, CV_CVTIMG_SWAP_RB);
        }

        output_args[0] = ByteImageFromIplImage (img);
		cvReleaseImage (&img);
    }
}; // class LoadImage: public pIn

class SaveImage: public CVBase {

    typedef CVBase Base;

public:

    SaveImage (Runtime& runtime) : Base (runtime) {

        _input_args.push_back (StringValue (runtime).SetName ("filename").SetDescription ("Name of the file."));
        CvArrInput ("image", "Image to be saved.");
    }

    virtual ~SaveImage() {}

    DECLARE_VIRTUAL_COPY_CONSTRUCTOR (SaveImage)

    virtual const std::string GetDescription() const {
        return "Saves an image to a specified file - only 8-bit single-channel or 3-channel (with 'BGR' channel order) images can be saved using this function.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/IO/SaveImage";
    }

protected:
	
	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
        Base::_Execute (input_args, output_args);

        if (cvSaveImage (StringValue (input_args[0]).GetData(), ArgumentAsCVArr (input_args[1]).get()) == 0) {
            throw exception::ExecutionException ("Failed to save image using OpenCV.");
        }
    }

}; // class SaveImage: public pIn

class CaptureFromCAM: public CVBase {

    typedef CVBase Base;

public:

    CaptureFromCAM (Runtime& runtime) : Base (runtime), _capt (0) {

        _parameters.push_back (
            IntValue (runtime).SetName ("index")
            .SetDescription ("Index of the camera to be used. If there is only one camera or it does not matter what camera is used -1 may be passed.")
            .SetData (-1));
    }

    virtual ~CaptureFromCAM() {
        if (_capt != 0) {
            cvReleaseCapture (&_capt);
        }
    }

    DECLARE_VIRTUAL_COPY_CONSTRUCTOR (CaptureFromCAM)

    virtual const std::string GetDescription() const {
        return "Captures a camera and returns single images from the stream.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/IO/CaptureFromCAM";
    }

protected:
	
	virtual void _Initialize (const Arguments& parameters) {

        CheckParameters (parameters, _parameters);

        if (_capt != 0) {
            cvReleaseCapture (&_capt);
            _capt = 0;
            _input_args.clear();
            _output_args.clear();
        }

        _capt = cvCaptureFromCAM (IntValue (parameters[0]).GetData());

        if (_capt == 0) {
            throw exception::InitializationException ("Failed to capture camera.");
        }

        _output_args.push_back (ByteImage (_runtime).SetName ("Image").SetDescription ("Image from camera"));
        DoubleValueOutput ("CV_CAP_PROP_POS_MSEC", "Film current position in milliseconds or video capture timestamp");
        DoubleValueOutput ("CV_CAP_PROP_POS_FRAMES", "0-based index of the frame to be decoded/captured next");
        DoubleValueOutput ("CV_CAP_PROP_POS_AVI_RATIO", "Relative position of the video file (0 - start of the film, 1 - end of the film)");
        DoubleValueOutput ("CV_CAP_PROP_FRAME_WIDTH", "Width of the frames in the video stream");
        DoubleValueOutput ("CV_CAP_PROP_FRAME_HEIGHT", "Height of the frames in the video stream");
        DoubleValueOutput ("CV_CAP_PROP_FPS", "Frame rate");
        DoubleValueOutput ("CV_CAP_PROP_FOURCC", "4-character code of codec");
        DoubleValueOutput ("CV_CAP_PROP_FRAME_COUNT", "Number of frames in the video file");
        DoubleValueOutput ("CV_CAP_PROP_FORMAT", "The format of the Mat objects returned by retrieve()");
        DoubleValueOutput ("CV_CAP_PROP_MODE", "A backend-specific value indicating the current capture mode");
        DoubleValueOutput ("CV_CAP_PROP_BRIGHTNESS", "Brightness of the image (only for cameras)");
        DoubleValueOutput ("CV_CAP_PROP_CONTRAST", "Contrast of the image (only for cameras)");
        DoubleValueOutput ("CV_CAP_PROP_SATURATION", "Saturation of the image (only for cameras)");
        DoubleValueOutput ("CV_CAP_PROP_HUE", "Hue of the image (only for cameras)");
        DoubleValueOutput ("CV_CAP_PROP_GAIN", "Gain of the image (only for cameras)");
        DoubleValueOutput ("CV_CAP_PROP_EXPOSURE", "Exposure (only for cameras)");
        DoubleValueOutput ("CV_CAP_PROP_CONVERT_RGB", "Boolean flags indicating whether images should be converted to RGB");
        //DoubleValueOutput("CV_CAP_PROP_WHITE_BALANCE", "Currently unsupported");
        //DoubleValueOutput("CV_CAP_PROP_RECTIFICATION", "TOWRITE (note: only supported by DC1394 v 2.x backend currently)");
    }

    virtual void _Execute (Arguments& input_args, Arguments& output_args) {
        //Base::_Execute (input_args, output_args);
        if (_capt == 0) {
            throw exception::NotInitializedException ("CaptureFromCAM was not (successfully) initialized.");
        }

        CheckOutputArguments (output_args, _output_args, 1);
        IplImage* img = cvQueryFrame (_capt);

        if (img == 0) {
            throw exception::ExecutionException ("Failed to grab an image from camera.");
        }

        CopyIplToByteImage (img, output_args[0]);
#ifdef WIN32
        {
            // also query optional meta information
            pI_size index (0);

            if (output_args.size() > ++index) { // CV_CAP_PROP_POS_MSEC
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_POS_MSEC));
            }

            if (output_args.size() > ++index) { // CV_CAP_PROP_POS_FRAMES
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_POS_FRAMES));
            }

            if (output_args.size() > ++index) { // CV_CAP_PROP_POS_AVI_RATIO
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_POS_AVI_RATIO));
            }

            if (output_args.size() > ++index) { // CV_CAP_PROP_FRAME_WIDTH
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_FRAME_WIDTH));
            }

            if (output_args.size() > ++index) { // CV_CAP_PROP_FRAME_HEIGHT
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_FRAME_HEIGHT));
            }

            if (output_args.size() > ++index) { // CV_CAP_PROP_FPS
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_FPS));
            }

            if (output_args.size() > ++index) { // CV_CAP_PROP_FOURCC
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_FOURCC));
            }

            if (output_args.size() > ++index) { // CV_CAP_PROP_FRAME_COUNT
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_FRAME_COUNT));
            }

            if (output_args.size() > ++index) { // CV_CAP_PROP_FORMAT
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_FORMAT));
            }

            if (output_args.size() > ++index) { // CV_CAP_PROP_MODE
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_MODE));
            }

            if (output_args.size() > ++index) { // CV_CAP_PROP_BRIGHTNESS
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_BRIGHTNESS));
            }

            if (output_args.size() > ++index) { // CV_CAP_PROP_CONTRAST
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_CONTRAST));
            }

            if (output_args.size() > ++index) { // CV_CAP_PROP_SATURATION
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_SATURATION));
            }

            if (output_args.size() > ++index) { // CV_CAP_PROP_HUE
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_HUE));
            }

            if (output_args.size() > ++index) { // CV_CAP_PROP_GAIN
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_GAIN));
            }

            if (output_args.size() > ++index) { // CV_CAP_PROP_EXPOSURE
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_EXPOSURE));
            }

            if (output_args.size() > ++index) { // CV_CAP_PROP_CONVERT_RGB
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_CONVERT_RGB));
            }

            /*
            if (output_args.size() > ++index) { // CV_CAP_PROP_WHITE_BALANCE
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue(output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_WHITE_BALANCE));
            }
            if (output_args.size() > ++index) { // CV_CAP_PROP_RECTIFICATION
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue(output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_RECTIFICATION));
            }
            */
        }
#endif // WIN32
    }

protected:

    CvCapture* _capt;

}; // class CaptureFromCAM: public pIn

class CaptureFromFile: public CVBase {

    typedef CVBase Base;

public:

    CaptureFromFile (Runtime& runtime) : Base (runtime), _capt (0) {

        _parameters.push_back (
            StringValue (runtime).SetName ("file")
            .SetDescription ("Path to video file to capture."));
    }

    virtual ~CaptureFromFile() {
        if (_capt != 0) {
            cvReleaseCapture (&_capt);
        }
    }

    DECLARE_VIRTUAL_COPY_CONSTRUCTOR (CaptureFromFile)

    virtual const std::string GetDescription() const {
        return "Captures a video file and returns single images from the stream.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/IO/CaptureFromFile";
    }

protected:
	
	virtual void _Initialize (const Arguments& parameters) {

        CheckParameters (parameters, _parameters);

        if (_capt != 0) {
            cvReleaseCapture (&_capt);
            _capt = 0;
            _input_args.clear();
            _output_args.clear();
        }

        _capt = cvCaptureFromFile (StringValue (parameters[0]).GetData());

        if (_capt == 0) {
            throw exception::InitializationException ("Failed to capture video file.");
        }

        _output_args.push_back (ByteImage (_runtime).SetName ("Image").SetDescription ("Image from video file"));
        DoubleValueOutput ("CV_CAP_PROP_POS_MSEC", "Film current position in milliseconds or video capture timestamp");
        DoubleValueOutput ("CV_CAP_PROP_POS_FRAMES", "0-based index of the frame to be decoded/captured next");
        DoubleValueOutput ("CV_CAP_PROP_POS_AVI_RATIO", "Relative position of the video file (0 - start of the film, 1 - end of the film)");
        DoubleValueOutput ("CV_CAP_PROP_FRAME_WIDTH", "Width of the frames in the video stream");
        DoubleValueOutput ("CV_CAP_PROP_FRAME_HEIGHT", "Height of the frames in the video stream");
        DoubleValueOutput ("CV_CAP_PROP_FPS", "Frame rate");
        DoubleValueOutput ("CV_CAP_PROP_FOURCC", "4-character code of codec");
        DoubleValueOutput ("CV_CAP_PROP_FRAME_COUNT", "Number of frames in the video file");
        DoubleValueOutput ("CV_CAP_PROP_FORMAT", "The format of the Mat objects returned by retrieve()");
        DoubleValueOutput ("CV_CAP_PROP_MODE", "A backend-specific value indicating the current capture mode");
        DoubleValueOutput ("CV_CAP_PROP_CONVERT_RGB", "Boolean flags indicating whether images should be converted to RGB");
        //DoubleValueOutput("CV_CAP_PROP_WHITE_BALANCE", "Currently unsupported");
        //DoubleValueOutput("CV_CAP_PROP_RECTIFICATION", "TOWRITE (note: only supported by DC1394 v 2.x backend currently)");
    }

    virtual void _Execute (Arguments& input_args, Arguments& output_args) {
        //Base::_Execute (input_args, output_args);
        if (_capt == 0) {
            throw exception::NotInitializedException ("CaptureFromFile was not (successfully) initialized.");
        }

        CheckOutputArguments (output_args, _output_args, 1);
        IplImage* img = cvQueryFrame (_capt);

        if (img == 0) {
            throw exception::ExecutionException ("Failed to grab an image from file.");
        }

        output_args[0] = ByteImageFromIplImage (img);
        {
            // also query optional meta information
            pI_size index (0);

            if (output_args.size() > ++index) { // CV_CAP_PROP_POS_MSEC
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_POS_MSEC));
            }

            if (output_args.size() > ++index) { // CV_CAP_PROP_POS_FRAMES
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_POS_FRAMES));
            }

            if (output_args.size() > ++index) { // CV_CAP_PROP_POS_AVI_RATIO
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_POS_AVI_RATIO));
            }

            if (output_args.size() > ++index) { // CV_CAP_PROP_FRAME_WIDTH
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_FRAME_WIDTH));
            }

            if (output_args.size() > ++index) { // CV_CAP_PROP_FRAME_HEIGHT
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_FRAME_HEIGHT));
            }

            if (output_args.size() > ++index) { // CV_CAP_PROP_FPS
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_FPS));
            }

            if (output_args.size() > ++index) { // CV_CAP_PROP_FOURCC
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_FOURCC));
            }

            if (output_args.size() > ++index) { // CV_CAP_PROP_FRAME_COUNT
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_FRAME_COUNT));
            }

            if (output_args.size() > ++index) { // CV_CAP_PROP_FORMAT
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_FORMAT));
            }

            if (output_args.size() > ++index) { // CV_CAP_PROP_MODE
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_MODE));
            }

            if (output_args.size() > ++index) { // CV_CAP_PROP_CONVERT_RGB
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue (output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_CONVERT_RGB));
            }

            /*
            if (output_args.size() > ++index) { // CV_CAP_PROP_WHITE_BALANCE
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue(output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_WHITE_BALANCE));
            }
            if (output_args.size() > ++index) { // CV_CAP_PROP_RECTIFICATION
                CheckOutputArgument (output_args[index], _output_args[index]);
                DoubleValue(output_args[index]).SetData (cvGetCaptureProperty (_capt, CV_CAP_PROP_RECTIFICATION));
            }
            */
        }
    }

protected:

    CvCapture* _capt;

}; // class CaptureFromFile: public pIn


class VideoWriter: public CVBase {

    typedef CVBase Base;

public:

    VideoWriter (Runtime& runtime) : Base (runtime), _vw (0) {

        _parameters.push_back (
            StringValue (runtime).SetName ("file")
            .SetDescription ("Path to video file to write to."));      // 0
        IntValueParameter ("fourcc", "4-character code of codec used to compress the frames. Use CV_FOURCC method to construct code words.", -1); // 1
        DoubleValueParameter ("fps", "Framerate of the created video stream.", 25.0);   // 2
        IntValueParameter ("width", "width of the video frames", 640);                  // 3
        IntValueParameter ("height", "height of the video frames", 480);                // 4
    }

    virtual ~VideoWriter() {
        if (_vw != 0) {
            cvReleaseVideoWriter (&_vw);
        }
    }

    DECLARE_VIRTUAL_COPY_CONSTRUCTOR (VideoWriter)

    virtual const std::string GetDescription() const {
        return "Creates a video file and writes single images into the file.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/IO/VideoWriter";
    }

protected:
	
	virtual void _Initialize (const Arguments& parameters) {

        CheckParameters (parameters, _parameters);

        if (_vw != 0) {
            cvReleaseVideoWriter (&_vw);
            _vw = 0;
            _input_args.clear();
            _output_args.clear();
        }

        CvSize fsize;
        fsize.width = IntValue (parameters[3]).GetData();
        fsize.height = IntValue (parameters[4]).GetData();
        _vw = cvCreateVideoWriter (
                  StringValue (parameters[0]).GetData(),
                  IntValue (parameters[1]).GetData(),
                  DoubleValue (parameters[2]).GetData(),
                  fsize,
                  1);

        if (_vw == 0) {
            throw exception::InitializationException ("Failed to initialize VideoWriter");
        }

        _input_args.push_back (
            ByteImage (_runtime).SetName ("image").SetDescription ("image to write to video file").SetReadOnly (pI_TRUE));
    }

    virtual void _Execute (Arguments& input_args, Arguments& output_args) {
        Base::_Execute (input_args, output_args);

        if (_vw == 0) {
            throw exception::NotInitializedException ("VideoWriter was not (successfully) initialized.");
        }

        if (cvWriteFrame (_vw, IplImageFromByteImage (input_args[0]).get()) == 0) {
            throw exception::ExecutionException ("Failed to append image to video file.");
        }
    }

protected:

    CvVideoWriter* _vw;

}; // class VideoWriter: public pIn

class ShowImage: public CVBase {

    typedef CVBase Base;

public:

    ShowImage (Runtime& runtime) : Base (runtime), _window_name (runtime), _swap (pI_TRUE) {

        _parameters.push_back (StringValue (runtime).SetName ("name").SetDescription ("Window name."));
        BoolValueParameter ("enable_live", "If false (default), displays image together with a hair cross."
                                            "If true, displays image as-is. Use this for subsequent calls of the plugin, i.e. for live images.",
                                            pI_FALSE);
        BoolValueParameter ("swap_bytes", "If true, R and B bytes are swapped before rendering", pI_FALSE);
        IntValueParameter ("x", "Horizontal position of top-left corner", 0);
        IntValueParameter ("y", "Vertical position of top-left corner.", 0);
    }

    virtual ~ShowImage() {
        if (_window_name.HasData()) {
            cvDestroyWindow (_window_name.GetData());
        }
    }

    DECLARE_VIRTUAL_COPY_CONSTRUCTOR (ShowImage)

    virtual const std::string GetDescription() const {
        return "Creates a window to render images.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/IO/ShowImage";
    }

    virtual const pI_int GetpInVersion() const {
        return 20000;
    }

protected:
	
	virtual void _Initialize (const Arguments& parameters) {

        CheckParameters (parameters, _parameters, 1);

        if (_window_name.HasData()) {
            cvDestroyWindow (_window_name.GetData());
            _window_name.SetData (0);
        }

        _window_name.SetData (StringValue (parameters[0]).GetData());

        if (parameters.size() >= 2) {
            CheckParameter (parameters[1], _parameters[1]);
            _enable_live = BoolValue (parameters[1]).GetData();
        }

        if (parameters.size() >= 3) {
            CheckParameter (parameters[2], _parameters[2]);
            _swap = BoolValue (parameters[2]).GetData();
        }

        int flags = CV_WINDOW_AUTOSIZE;
        if (cvNamedWindow (_window_name.GetData(), flags) == 0) {
            throw exception::ExecutionException ("Failed to initialize image renderer.");
        }

        if (parameters.size() >= 5) {
            CheckParameter (parameters[3], _parameters[3]);
            CheckParameter (parameters[4], _parameters[4]);
            pI_int posx = IntValue (parameters[3]).GetData();
            pI_int posy = IntValue (parameters[4]).GetData();
            if (posx > 0 && posy > 0) {
                cvMoveWindow (_window_name.GetData(), posx, posy);
            }
        }

        if (_input_args.size() == 0) {
            _input_args.push_back (
                ByteImage (_runtime).SetName ("image").SetDescription ("image to render"));
        }
    }

    virtual void _Execute (Arguments& input_args, Arguments& output_args) {

        if (!_window_name.HasData()) {
            throw exception::NotInitializedException ("ShowImage plugin was not initialized properly.");
        }

        Base::_Execute (input_args, output_args);
        _render_image = _runtime.CopyArgument (input_args[0], pI_TRUE);
        _ipl_image = IplImageFromByteImage (_render_image);

        // convert colour byte-ordering in-place (bgr->rgb)
        if (_swap == pI_TRUE) {
            cvConvertImage (_ipl_image.get(), _ipl_image.get(), CV_CVTIMG_SWAP_RB);
        }

        cvShowImage (_window_name.GetData(), _ipl_image.get());

        // From OpenCV's documentation: cvWaitKey() is "the only method in HighGUI that can fetch
        // and handle events, so it needs to be called periodically for normal event processing".
        // As a consequence, either
        // 1. Execute() is called periodically, e.g. to display a video; then, a call to
        //    cvWaitKey(1) here is okay; or
        // 2. Execute() is called only once, e.g. to display a single image. Then, there is
        //    no possibility to periodically call cvWaitKey(), hence cvWaitKey(0) has to
        //    be used, i.e. Execute() is blocking.
        // Note that there is a timing problem with cvWaitKey(1) under Linux; however, cvWaitKey(2) works.
        // See also notes of issue 136 (https://pureimage.flll.jku.at/issues/136)
#ifdef WIN32        
        cvWaitKey (_enable_live ? 1 : 0);
#else
        cvWaitKey (_enable_live ? 2 : 0);
#endif        
    }

protected:

    StringValue _window_name;
    boost::shared_ptr<IplImage> _ipl_image;
    ArgumentPtr _render_image;
    pI_bool _enable_live;
    pI_bool _swap;

}; // class ShowImage: public pIn

} // namespace opencv
} // namespace pIns
} // namespace pI

#endif // CV_IO_HPP
