/* CVCore.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CV_CORE_HPP
#define CV_CORE_HPP

#include "CVBase.hpp"

namespace pI {
namespace pIns {
namespace opencv {

class AbsDiff: public CVBase {

	typedef CVBase Base;

public:

	AbsDiff(Runtime& runtime): Base(runtime) {

		CvArrInput ("src1", "The first source array", pI_TRUE);
		CvArrInput ("src2", "The second source array", pI_TRUE);
		CvArrOutput ("dst", "The destination array");
	}

	virtual ~AbsDiff() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(AbsDiff)

	virtual const std::string GetDescription() const {
        return "Calculates absolute difference between two arrays.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/AbsDiff";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::CheckCvArrSizes (input_args[0], input_args[1]);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvAbsDiff (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (input_args[1]).get(),
		 ArgumentAsCVArr (output_args[0]).get());
	}

}; // class AbsDiff: public pIn

class AbsDiffS: public CVBase {

	typedef CVBase Base;

public:

	AbsDiffS(Runtime& runtime): Base(runtime) {

		CvArrInput ("src", "The source array", pI_TRUE);
		DoubleValueInput ("value", "The scalar", 0.0, pI_TRUE);
		CvArrOutput ("dst", "The destination array");
	}

	virtual ~AbsDiffS() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(AbsDiffS)

	virtual const std::string GetDescription() const {
        return "Calculates absolute difference between an array and a scalar.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/AbsDiffS";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvAbsDiffS (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (output_args[0]).get(),
		 DoubleValueAsCVScalar (input_args[1]));
	}
}; // class AbsDiffS: public pIn

class Add: public CVBase {

	typedef CVBase Base;

public:

	Add(Runtime& runtime): Base(runtime) {

		CvArrInput ("src1", "The first source array", pI_TRUE);
		CvArrInput ("src2", "The second source array", pI_TRUE);
		CvArrOutput ("dst", "The destination array");
	}

	virtual ~Add() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Add)

	virtual const std::string GetDescription() const {
        return "Computes the per-element sum of two arrays.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Add";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::CheckCvArrSizes (input_args[0], input_args[1]);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvAdd (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (input_args[1]).get(),
		 ArgumentAsCVArr (output_args[0]).get(),
		 NULL /* always use zero mask. */);
	}
}; // class Add: public pIn

class AddS: public CVBase {

	typedef CVBase Base;

public:

	AddS(Runtime& runtime): Base(runtime) {

		CvArrInput ("src", "The source array", pI_TRUE);
		DoubleValueInput ("value", "Added scalar", 0.0, pI_TRUE);
		CvArrOutput ("dst", "The destination array");
	}

	virtual ~AddS() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(AddS)

	virtual const std::string GetDescription() const {
        return "Computes the sum of an array and a scalar.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/AddS";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvAddS (
		 ArgumentAsCVArr (input_args[0]).get(),
		 DoubleValueAsCVScalar (input_args[1]),
		 ArgumentAsCVArr (output_args[0]).get(),
		 NULL /* always use zero mask. */);
	}
}; // class AddS: public pIn

class AddWeighted: public CVBase {

	typedef CVBase Base;

public:

	AddWeighted(Runtime& runtime): Base(runtime) {

		CvArrInput ("src1", "The first source array", pI_TRUE);
		DoubleValueInput ("alpha", "Weight for the first array elements", 1.0, pI_TRUE);
		CvArrInput ("src2", "The second source array", pI_TRUE);
		DoubleValueInput ("beta", "Weight for the second array elements", 1.0, pI_TRUE);
		CvArrOutput ("dst", "The destination array");
		DoubleValueInput ("gamma", "Scalar, added to each sum", 0.0, pI_TRUE);
	}

	virtual ~AddWeighted() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(AddWeighted)

	virtual const std::string GetDescription() const {
        return "Computes the weighted sum of two arrays.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/AddWeighted";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::CheckCvArrSizes (input_args[0], input_args[2]);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvAddWeighted (
		 ArgumentAsCVArr (input_args[0]).get(),
		 DoubleValue(input_args[1]).GetData(),
		 ArgumentAsCVArr (input_args[2]).get(),
		 DoubleValue(input_args[3]).GetData(),
		 DoubleValue(input_args[4]).GetData(),
		 ArgumentAsCVArr (output_args[0]).get());
	}
}; // class AddWeighted: public pIn

class And: public CVBase {

	typedef CVBase Base;

public:

	And(Runtime& runtime): Base(runtime) {

		CvArrInput ("src1", "The first source array", pI_TRUE);
		CvArrInput ("src2", "The second source array", pI_TRUE);
		CvArrOutput ("dst", "The destination array");
	}

	virtual ~And() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(And)

	virtual const std::string GetDescription() const {
        return "Calculates per-element bit-wise conjunction of two arrays.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/And";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::CheckCvArrSizes (input_args[0], input_args[1]);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvAnd (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (input_args[1]).get(),
		 ArgumentAsCVArr (output_args[0]).get(),
		 NULL /* always use zero mask. */);
	}
}; // class And: public pIn

class AndS: public CVBase {

	typedef CVBase Base;

public:

	AndS(Runtime& runtime): Base(runtime) {

		CvArrInput ("src", "The source array", pI_TRUE);
		DoubleValueInput ("value", "Scalar to use in the operation", 0.0, pI_TRUE);
		CvArrOutput ("dst", "The destination array");
	}

	virtual ~AndS() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(AndS)

	virtual const std::string GetDescription() const {
        return "Calculates per-element bit-wise conjunction of an array and a scalar.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/AndS";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvAndS (
		 ArgumentAsCVArr (input_args[0]).get(),
		 DoubleValueAsCVScalar (input_args[1]),
		 ArgumentAsCVArr (output_args[0]).get(),
		 NULL /* always use zero mask. */);
	}
}; // class AndS: public pIn

class Avg: public CVBase {

	typedef CVBase Base;

public:

	Avg(Runtime& runtime): Base(runtime) {

		CvArrInput ("arr", "The array", pI_TRUE);
		DoubleValueOutput ("result", "Resulting array mean");
	}

	virtual ~Avg() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Avg)

	virtual const std::string GetDescription() const {
        return "Calculates average (mean) of array elements.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Avg";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		DoubleValue(output_args[0]).SetData (
		 cvAvg (ArgumentAsCVArr (input_args[0]).get(),
				NULL /* always use zero mask. */).val[0]);
	}
}; // class Avg: public pIn

class AvgSdv: public CVBase {

	typedef CVBase Base;

public:

	AvgSdv(Runtime& runtime): Base(runtime) {

		CvArrInput ("arr", "The array", pI_TRUE);
		DoubleValueOutput ("mean", "Resulting array mean");
		DoubleValueOutput ("stdDev", "Resulting array standard deviation");
	}

	virtual ~AvgSdv() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(AvgSdv)

	virtual const std::string GetDescription() const {
        return "Calculates average (mean) of array elements.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/AvgSdv";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		CvScalar mean, stdDev;
		cvAvgSdv (ArgumentAsCVArr (input_args[0]).get(), &mean, &stdDev, NULL /* always use zero mask. */);
		DoubleValue(output_args[0]).SetData (mean.val[0]);
		DoubleValue(output_args[1]).SetData (stdDev.val[0]);
	}
}; // class AvgSdv: public pIn

class CalcCovarMatrix: public CVBase {

	typedef CVBase Base;

public:

	CalcCovarMatrix(Runtime& runtime): Base(runtime), _scrambled(pI_FALSE), _use_avg(pI_FALSE), _scale(pI_FALSE) {

		BoolValueParameter ("mode", "Mode of covariance matrix calculation. A value of pI_FALSE corresponds to CV_COVAR_NORMAL, whereas pI_TRUE corresponds to CV_COVAR_SCRAMBLED.");
		BoolValueParameter ("use avg", "If true, the avg array is not calculated, but used as pre-calculated input.");
		BoolValueParameter ("scale", "If true, the covariance matrix is scaled.");
	}

	virtual ~CalcCovarMatrix() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(CalcCovarMatrix)

	virtual const std::string GetDescription() const {
        return "Calculates covariance matrix of a set of vectors. Note that any number of input vectors of the same type and size is accepted.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/CalcCovarMatrix";
    }

protected:

	virtual void _Initialize (const Arguments& parameters) {
		
		CheckParameters (parameters, _parameters);
		_scrambled = BoolValue (parameters[0]).GetData();
		_use_avg = BoolValue (parameters[1]).GetData();
		_scale = BoolValue (parameters[2]).GetData();
		_input_args.clear();
		_output_args.clear();
		if (_use_avg == pI_TRUE) {
			CvArrInput ("avg", "The mean (average) vector of the input vectors", pI_TRUE);
		} else {
			CvArrOutput ("avg", "The mean (average) vector of the input vectors");
		}
		CvArrInput ("vect1", "The first input vector", pI_TRUE);
		CvArrInput ("vect2", "The second input vector", pI_TRUE);
		_output_args.push_back (
		 DoubleMatrix(_runtime).SetName ("covMat")
									.SetDescription ("The output covariance matrix that should be square"));

	}

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		int flags(
		 (_scrambled == pI_TRUE ? CV_COVAR_SCRAMBLED : CV_COVAR_NORMAL) |
		 (_use_avg == pI_TRUE ? CV_COVAR_USE_AVG : 0) |
		 (_scale == pI_TRUE ? CV_COVAR_SCALE : 0));
		int num_vectors(input_args.size() - (_use_avg == pI_TRUE ? 1 : 0));
		CvArr** vects = new CvArr*[num_vectors];
		// TODO: add check that given arguments are of same type and count
		for (int lv = 0; lv < num_vectors; ++lv) {
			vects[lv] = ArgumentAsCVArr (input_args[lv + (_use_avg == pI_TRUE ? 1 : 0)]).get();
		}
		// TODO: allocate covMat
		cvCalcCovarMatrix (
		 (const CvArr**) vects,
		 num_vectors,
		 ArgumentAsCVArr (output_args[_use_avg == pI_TRUE ? 0 : 1]).get(),
		 ArgumentAsCVArr (_use_avg == pI_TRUE ? input_args[0] : output_args[0]).get(),
		 flags);
		delete[] vects;
	}

protected:

	pI_bool _scrambled, _use_avg, _scale;
}; // class CalcCovarMatrix: public pIn

class CartToPolar: public CVBase {

	typedef CVBase Base;

public:

	CartToPolar(Runtime& runtime): Base(runtime), _angle_in_degrees(pI_FALSE) {

		BoolValueParameter ("angleInDegrees", "If true, angles are measured in degrees, otherwise in radians.");
		CvArrInput ("x", "The array of x-coordinates", pI_TRUE);
		CvArrInput ("y", "The array of y-coordinates", pI_TRUE);
		CvArrOutput ("magnitude", "The destination array of magnitudes");
		CvArrOutput ("angle", "The destination array of angles");
	}

	virtual ~CartToPolar() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(CartToPolar)

	virtual const std::string GetDescription() const {
        return "Calculates the magnitude and angle of 2d vectors.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/CartToPolar";
    }

protected:

	virtual void _Initialize (const Arguments& parameters) {
		CheckParameters (parameters, _parameters);
		_angle_in_degrees = BoolValue(parameters[0]).GetData();
	}

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[1]);
		cvCartToPolar (
		 ArgumentAsCVArr(input_args[0]).get(),
		 ArgumentAsCVArr(input_args[1]).get(),
		 ArgumentAsCVArr(output_args[0]).get(),
		 ArgumentAsCVArr(output_args[1]).get(),
		 _angle_in_degrees == pI_TRUE ? 1 : 0);
	}

protected:
	pI_bool _angle_in_degrees;
}; // class CartToPolar: public pIn

class Cbrt: public CVBase {

	typedef CVBase Base;

public:

	Cbrt(Runtime& runtime): Base(runtime) {

		DoubleValueInput ("value", "The input floating-point value", 1.0, pI_TRUE);
		DoubleValueOutput ("cbrt", "Cubic root of the input floating-point value");
	}

	virtual ~Cbrt() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Cbrt)

	virtual const std::string GetDescription() const {
        return "Calculates the cubic root.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Cbrt";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		DoubleValue(output_args[0]).SetData (
		 static_cast<double> (cvCbrt (static_cast<float> (DoubleValue (input_args[0]).GetData()))));
	}
}; // class Cbrt: public pIn

class Cmp: public CVBase {

	typedef CVBase Base;

public:

	Cmp(Runtime& runtime): Base(runtime), _cmp_op(CV_CMP_EQ) {

		// prepare StringSelection argument
		StringSelection ssel(_runtime);
		ssel.SetName ("cmpOp")
			.SetDescription ("The flag specifying the relation between the elements to be checked")
			.SetCount (6);
		ssel.CreateData();
		ssel.SetSymbols (0, "CV_CMP_EQ");
		ssel.SetSymbols (1, "CV_CMP_GT");
		ssel.SetSymbols (2, "CV_CMP_GE");
		ssel.SetSymbols (3, "CV_CMP_LT");
		ssel.SetSymbols (4, "CV_CMP_LE");
		ssel.SetSymbols (5, "CV_CMP_NE");
		ssel.SetIndex (0);
		_parameters.push_back (ssel);

		CvArrInput ("src1", "The first source array", pI_TRUE);
		CvArrInput ("src2", "The second source array. Both source arrays must have a single channel.", pI_TRUE);
		CvArrOutput ("dst", "The destination array.");
	}

	virtual ~Cmp() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Cmp)

	virtual const std::string GetDescription() const {
        return "Performs per-element comparison of two arrays.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Cmp";
    }

protected:

	virtual void _Initialize (const Arguments& parameters) {
		CheckParameters (parameters, _parameters);
		switch (StringSelection(parameters[0]).GetIndex()) {
			case 0: _cmp_op = CV_CMP_EQ; break;
			case 1: _cmp_op = CV_CMP_GT; break;
			case 2: _cmp_op = CV_CMP_GE; break;
			case 3: _cmp_op = CV_CMP_LT; break;
			case 4: _cmp_op = CV_CMP_LE; break;
			case 5: _cmp_op = CV_CMP_NE; break;
			default: _cmp_op = CV_CMP_EQ;
		}
	}

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::CheckCvArrSizes (input_args[0], input_args[1]);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvCmp (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (input_args[1]).get(),
		 ArgumentAsCVArr (output_args[0]).get(),
		 _cmp_op);
	}
protected:
	int _cmp_op;
}; // class Cmp: public pIn

class CmpS: public CVBase {

	typedef CVBase Base;

public:

	CmpS(Runtime& runtime): Base(runtime), _cmp_op(CV_CMP_EQ) {

		// prepare StringSelection argument
		StringSelection ssel(_runtime);
		ssel.SetName ("cmpOp")
			.SetDescription ("The flag specifying the relation between the elements to be checked")
			.SetCount (6);
		ssel.CreateData();
		ssel.SetSymbols (0, "CV_CMP_EQ");
		ssel.SetSymbols (1, "CV_CMP_GT");
		ssel.SetSymbols (2, "CV_CMP_GE");
		ssel.SetSymbols (3, "CV_CMP_LT");
		ssel.SetSymbols (4, "CV_CMP_LE");
		ssel.SetSymbols (5, "CV_CMP_NE");
		ssel.SetIndex (0);
		_parameters.push_back (ssel);

		CvArrInput ("src", "The source array, must have a single channel", pI_TRUE);
		DoubleValueInput ("value", "The scalar value to compare each array element with", 0.0, pI_TRUE);
		CvArrOutput ("dst", "The destination array.");
	}

	virtual ~CmpS() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(CmpS)

	virtual const std::string GetDescription() const {
        return "Performs per-element comparison of an array and a scalar.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/CmpS";
    }

protected:

	virtual void _Initialize (const Arguments& parameters) {
		CheckParameters (parameters, _parameters);
		switch (StringSelection(parameters[0]).GetIndex()) {
			case 0: _cmp_op = CV_CMP_EQ; break;
			case 1: _cmp_op = CV_CMP_GT; break;
			case 2: _cmp_op = CV_CMP_GE; break;
			case 3: _cmp_op = CV_CMP_LT; break;
			case 4: _cmp_op = CV_CMP_LE; break;
			case 5: _cmp_op = CV_CMP_NE; break;
			default: _cmp_op = CV_CMP_EQ;
		}
	}

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::CheckCvArrSizes (input_args[0], input_args[1]);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvCmpS (
		 ArgumentAsCVArr (input_args[0]).get(),
		 DoubleValue(input_args[1]).GetData(),
		 ArgumentAsCVArr (output_args[0]).get(),
		 _cmp_op);
	}
protected:
	int _cmp_op;
}; // class CmpS: public pIn

class ConvertScale: public CVBase {

	typedef CVBase Base;

public:

	ConvertScale(Runtime& runtime): Base(runtime) {

		CvArrInput ("src", "Source array", pI_TRUE);
		CvArrOutput ("dst", "Destination array");	
		DoubleValueInput ("scale", "Scale factor", 1.0, pI_TRUE);
		DoubleValueInput ("shift", "Value added to the scaled source array elements", 0.0, pI_TRUE);
	}

	virtual ~ConvertScale() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(ConvertScale)

	virtual const std::string GetDescription() const {
        return "Converts one array to another with optional linear transformation.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/ConvertScale";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		cvConvertScale (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (output_args[0]).get(),
		 DoubleValue(input_args[1]).GetData(),
		 DoubleValue(input_args[2]).GetData());
	}
}; // class ConvertScale: public pIn

class CountNonZero: public CVBase {

	typedef CVBase Base;

public:

	CountNonZero(Runtime& runtime): Base(runtime) {

		CvArrInput ("arr", "Source array", pI_TRUE);
		IntValueOutput ("nz", "Number of non-zero elements in arr");
	}

	virtual ~CountNonZero() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(CountNonZero)

	virtual const std::string GetDescription() const {
        return "Counts non-zero array elements.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/CountNonZero";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		IntValue(output_args[0]).SetData (
		 static_cast<pI_int> (cvCountNonZero (ArgumentAsCVArr (input_args[0]).get())));
	}
}; // class CountNonZero: public pIn

class CrossProduct: public CVBase {

	typedef CVBase Base;

public:

	CrossProduct(Runtime& runtime): Base(runtime) {

		CvArrInput ("src1", "The first source vector", pI_TRUE);
		CvArrInput ("src2", "The second source vector", pI_TRUE);
		CvArrOutput ("dst", "The destination vector");
	}

	virtual ~CrossProduct() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(CrossProduct)

	virtual const std::string GetDescription() const {
        return "Calculates the cross product of two 3D vectors.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/CrossProduct";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::CheckCvArrSizes (input_args[0], input_args[1]);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvCrossProduct (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (input_args[1]).get(),
		 ArgumentAsCVArr (output_args[0]).get());
	}
}; // class CrossProduct: public pIn

class DCT: public CVBase {

	typedef CVBase Base;

public:

	DCT(Runtime& runtime): Base(runtime), _inverse(pI_FALSE), _rows(pI_FALSE) {

		BoolValueParameter ("inverse", "If true, do an inverse 1D or 2D transform.");
		BoolValueParameter ("rows", "do a forward or inverse transform of every individual row of the input matrix.");
		CvArrInput ("src", "Source array, real 1D or 2D array", pI_TRUE);
		CvArrOutput ("dst", "Destination array of the same size and same type as the source");
	}

	virtual ~DCT() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(DCT)

	virtual const std::string GetDescription() const {
        return "Performs a forward or inverse Discrete Cosine transform of a 1D or 2D floating-point array.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/DCT";
    }

protected:

	virtual void _Initialize (const Arguments& parameters) {
		CheckParameters (parameters, _parameters);
		_inverse = BoolValue(parameters[0]).GetData();
		_rows = BoolValue(parameters[1]).GetData();
	}

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvDCT (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (output_args[0]).get(),
		 (_inverse == pI_TRUE ? CV_DXT_INVERSE : CV_DXT_FORWARD) |
		  (_rows == pI_TRUE ? CV_DXT_ROWS : 0));
	}

protected:
	pI_bool _inverse, _rows;
}; // class DCT: public pIn

class DFT: public CVBase {

	typedef CVBase Base;

public:

	DFT(Runtime& runtime): Base(runtime), _inverse(pI_FALSE), _scale(pI_FALSE), _rows(pI_FALSE) {

		BoolValueParameter ("inverse", "If true, do an inverse 1D or 2D transform.");
		BoolValueParameter ("scale", "scale the result: divide it by the number of array elements.");
		BoolValueParameter ("rows", "do a forward or inverse transform of every individual row of the input matrix.");
		CvArrInput ("src", "Source array, real 1D or 2D array", pI_TRUE);
		CvArrOutput ("dst", "Destination array of the same size and same type as the source");
	}

	virtual ~DFT() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(DFT)

	virtual const std::string GetDescription() const {
        return "Performs a forward or inverse Discrete Fourier transform of a 1D or 2D floating-point array.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/DFT";
    }

protected:

	virtual void _Initialize (const Arguments& parameters) {
		CheckParameters (parameters, _parameters);
		_inverse = BoolValue(parameters[0]).GetData();
		_scale = BoolValue(parameters[1]).GetData();
		_rows = BoolValue(parameters[2]).GetData();
	}

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvDFT (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (output_args[0]).get(),
		 (_inverse == pI_TRUE ? CV_DXT_INVERSE : CV_DXT_FORWARD) | 
		  (_scale == pI_TRUE ? CV_DXT_SCALE  : 0) |
		  (_rows == pI_TRUE ? CV_DXT_ROWS : 0));
	}

protected:
	pI_bool _inverse, _scale, _rows;
}; // class DFT: public pIn

class Det: public CVBase {

	typedef CVBase Base;

public:

	Det(Runtime& runtime): Base(runtime) {

		CvArrInput ("arr", "Source array", pI_TRUE);
		DoubleValueOutput ("det", "The determinant of a matrix.");
	}

	virtual ~Det() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Det)

	virtual const std::string GetDescription() const {
        return "Returns the determinant of a matrix.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Det";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		DoubleValue(output_args[0]).SetData (
		 cvDet (ArgumentAsCVArr (input_args[0]).get()));
	}
}; // class Det: public pIn

class Div: public CVBase {

	typedef CVBase Base;

public:

	Div(Runtime& runtime): Base(runtime) {

		CvArrInput ("src1", "The first source array", pI_TRUE);
		CvArrInput ("src2", "The second source array", pI_TRUE);
		CvArrOutput ("dst", "The destination array");
		DoubleValueInput ("scale", "Scale factor", 1.0, pI_TRUE);
	}

	virtual ~Div() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Div)

	virtual const std::string GetDescription() const {
        return "Performs per-element division of two arrays.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Div";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::CheckCvArrSizes (input_args[0], input_args[1]);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvDiv (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (input_args[1]).get(),
		 ArgumentAsCVArr (output_args[0]).get(),
		 DoubleValue (input_args[2]).GetData());
	}
}; // class Div: public pIn

class DotProduct: public CVBase {

	typedef CVBase Base;

public:

	DotProduct(Runtime& runtime): Base(runtime) {

		CvArrInput ("src1", "The first source array", pI_TRUE);
		CvArrInput ("src2", "The second source array", pI_TRUE);
		DoubleValueOutput ("dotp", "Euclidian dot product");
	}

	virtual ~DotProduct() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(DotProduct)

	virtual const std::string GetDescription() const {
        return "Calculates the dot product of two arrays in Euclidian metrics.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/DotProduct";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::CheckCvArrSizes (input_args[0], input_args[1]);
		DoubleValue(output_args[0]).SetData (
		 cvDotProduct (ArgumentAsCVArr (input_args[0]).get(), ArgumentAsCVArr (input_args[1]).get()));
	}
}; // class DotProduct: public pIn

class EigenVV: public CVBase {

	typedef CVBase Base;

public:

	EigenVV(Runtime& runtime): Base(runtime) {
	}

	virtual ~EigenVV() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(EigenVV)

	virtual const std::string GetDescription() const {
		return "Computes eigenvalues and eigenvectors of a symmetric matrix. NOT YET AVAILABLE.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/EigenVV";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		// TODO: implement EigenVV
		throw exception::ExecutionException("EigenVV op is not yet implemented.");
	}
}; // class EigenVV: public pIn

class Exp: public CVBase {

	typedef CVBase Base;

public:

	Exp(Runtime& runtime): Base(runtime) {

		CvArrInput ("src", "The source array", pI_TRUE);
		CvArrOutput ("dst", "The destination array");
	}

	virtual ~Exp() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Exp)

	virtual const std::string GetDescription() const {
        return "Calculates the exponent of every array element.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Exp";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvExp (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (output_args[0]).get());
	}
}; // class Exp: public pIn

class FastArctan: public CVBase {

	typedef CVBase Base;

public:

	FastArctan(Runtime& runtime): Base(runtime) {

		DoubleValueInput ("x", "x-coordinate of 2D vector.", 1.0, pI_TRUE);
		DoubleValueInput ("y", "y-coordinate of 2D vector.", 1.0, pI_TRUE);
		DoubleValueOutput ("angle", "angle of a 2D vector.");
	}

	virtual ~FastArctan() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(FastArctan)

	virtual const std::string GetDescription() const {
        return "Calculates the angle of a 2D vector.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/FastArctan";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		DoubleValue(output_args[0]).SetData (static_cast<pI_double> (
		 cvFastArctan (
		  static_cast<float> (DoubleValue(input_args[0]).GetData()),
		  static_cast<float> (DoubleValue(input_args[1]).GetData()))));
	}
}; // class FastArctan: public pIn


class Flip: public CVBase {

	typedef CVBase Base;

public:

	Flip(Runtime& runtime): Base(runtime), _inplace(pI_FALSE), _mode(0) {

		BoolValueParameter ("inplace", "If pI_TRUE, the flip operation is done in place.");
		IntValueParameter ("flipMode", "Specifies how to flip the array: 0 means flipping around the x-axis, positive (e.g., 1) means flipping around y-axis, and negative (e.g., -1) means flipping around both axes.");
	}

	virtual ~Flip() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Flip)

	virtual const std::string GetDescription() const {
        return "Flips a 2D array around vertical, horizontal or both axes.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Flip";
    }

protected:

	virtual void _Initialize (const Arguments& parameters) {
		CheckParameters (parameters, _parameters);
		_inplace = BoolValue(parameters[0]).GetData();
		_mode = IntValue(parameters[1]).GetData();
		if (_inplace != pI_TRUE) {
			CvArrInput ("src", "The source array", pI_TRUE);
			CvArrOutput ("dst", "The destination array");
		} else {
			CvArrInput ("array", "The array to flip inplace", pI_FALSE);
		}
	}

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		if (_inplace == pI_TRUE) {
			cvFlip (ArgumentAsCVArr (input_args[0]).get(), NULL, _mode);
		} else {
			Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
			cvFlip (
			 ArgumentAsCVArr (input_args[0]).get(),
			 ArgumentAsCVArr (output_args[0]).get(),
			 _mode);
		}
	}

protected:

	pI_bool _inplace;
	pI_int _mode;
}; // class Flip: public pIn

class GEMM: public CVBase {

	typedef CVBase Base;

public:

	GEMM(Runtime& runtime): Base(runtime) {

		CvArrInput ("src1", "The first source array", pI_TRUE);					// 0
		CvArrInput ("src2", "The second source array", pI_TRUE);				// 1
		DoubleValueInput ("alpha", "alpha", 1.0, pI_TRUE);						// 2
		CvArrInput ("src3", "The third source array", pI_TRUE);					// 3
		DoubleValueInput ("beta", "beta", 1.0, pI_TRUE);						// 4
		BoolValueInput ("T src1", "Transpose first source array", pI_FALSE, pI_TRUE);	// 5
		BoolValueInput ("T src2", "Transpose second source array", pI_FALSE, pI_TRUE);	// 6
		BoolValueInput ("T src3", "Transpose third source array", pI_FALSE, pI_TRUE);	// 7
		CvArrOutput ("dst", "The destination array");
	}

	virtual ~GEMM() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(GEMM)

	virtual const std::string GetDescription() const {
		return "Performs generalized matrix multiplication: dst = alpha src1 src2 + beta src3";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/GEMM";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::CheckCvArrSizes (input_args[0], input_args[1]);
		Base::CheckCvArrSizes (input_args[0], input_args[3]);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		pI_int tABC =
		 (BoolValue (input_args[5]).GetData() == pI_TRUE ? CV_GEMM_A_T : 0) |
		 (BoolValue (input_args[6]).GetData() == pI_TRUE ? CV_GEMM_B_T : 0) |
		 (BoolValue (input_args[7]).GetData() == pI_TRUE ? CV_GEMM_C_T : 0);
		cvGEMM (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (input_args[1]).get(),
		 DoubleValue (input_args[2]).GetData(),
		 ArgumentAsCVArr (input_args[3]).get(),
		 DoubleValue (input_args[4]).GetData(),
		 ArgumentAsCVArr (output_args[0]).get(),
		 tABC);
	}
}; // class GEMM: public pIn

class GetOptimalDFTSize: public CVBase {

	typedef CVBase Base;

public:

	GetOptimalDFTSize(Runtime& runtime): Base(runtime) {

		IntValueInput ("size0", "Vector size", 1, pI_TRUE);
		IntValueOutput ("DFT size", "Optimal DFT size");
	}

	virtual ~GetOptimalDFTSize() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(GetOptimalDFTSize)

	virtual const std::string GetDescription() const {
		return "Returns optimal DFT size for a given vector size.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/GetOptimalDFTSize";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		IntValue(output_args[0]).SetData (cvGetOptimalDFTSize (IntValue(input_args[0]).GetData()));
	}
}; // class GetOptimalDFTSize: public pIn

class InRange: public CVBase {

	typedef CVBase Base;

public:

	InRange(Runtime& runtime): Base(runtime) {

		CvArrInput ("src", "The first source array", pI_TRUE);
		CvArrInput ("lower", "The inclusive lower boundary array", pI_TRUE);
		CvArrInput ("upper", "The exclusive upper boundary array", pI_TRUE);
		CvArrOutput ("dst", "The destination array.");
	}

	virtual ~InRange() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(InRange)

	virtual const std::string GetDescription() const {
        return "The function does the range check for every element of the input array.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/InRange";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::CheckCvArrSizes (input_args[0], input_args[1]);
		Base::CheckCvArrSizes (input_args[0], input_args[2]);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvInRange (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (input_args[1]).get(),
		 ArgumentAsCVArr (input_args[2]).get(),
		 ArgumentAsCVArr (output_args[0]).get());
	}

}; // class InRange: public pIn

class InRangeS: public CVBase {

	typedef CVBase Base;

public:

	InRangeS(Runtime& runtime): Base(runtime) {

		CvArrInput ("src", "The source array", pI_TRUE);
		DoubleValueInput ("lower", "The inclusive lower boundary array", 1.0, pI_TRUE);
		DoubleValueInput ("upper", "The exclusive upper boundary array", 1.0, pI_TRUE);
		CvArrOutput ("dst", "The destination array.");
	}

	virtual ~InRangeS() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(InRangeS)

	virtual const std::string GetDescription() const {
        return "Checks that array elements lie between two scalars.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/InRangeS";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvInRangeS (
		 ArgumentAsCVArr (input_args[0]).get(),
		 DoubleValueAsCVScalar (input_args[1]),
		 DoubleValueAsCVScalar (input_args[2]),
		 ArgumentAsCVArr (output_args[0]).get());
	}

}; // class InRangeS: public pIn

class InvSqrt: public CVBase {

	typedef CVBase Base;

public:

	InvSqrt(Runtime& runtime): Base(runtime) {

		DoubleValueInput ("value", "The input floating-point value.", 1.0, pI_TRUE);
		DoubleValueOutput ("isqrt", "Inverse square root.");
	}

	virtual ~InvSqrt() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(InvSqrt)

	virtual const std::string GetDescription() const {
        return "Calculates the inverse square root.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/InvSqrt";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		DoubleValue(output_args[0]).SetData (static_cast<pI_double> (
		 cvInvSqrt (
		  static_cast<float> (DoubleValue(input_args[0]).GetData()))));
	}
}; // class InvSqrt: public pIn

class Inv: public CVBase {

	typedef CVBase Base;

public:

	Inv(Runtime& runtime): Base(runtime), _method(CV_LU) {

		// prepare StringSelection argument
		StringSelection ssel(_runtime);
		ssel.SetName ("method")
			.SetDescription ("Inversion method: CV_LU = Gaussian elimination with optimal pivot element chosen, CV_SVD = Singular value decomposition (SVD) method, CV_SVD_SYM = SVD method for a symmetric positively-defined matrix")
			.SetCount (3);
		ssel.CreateData();
		ssel.SetSymbols (0, "CV_LU");
		ssel.SetSymbols (1, "CV_SVD");
		ssel.SetSymbols (2, "CV_SVD_SYM");
		ssel.SetIndex (0);
		_parameters.push_back (ssel);

		CvArrInput ("src", "The source matrix", pI_TRUE);
		CvArrOutput ("dst", "The destination matrix");
	}

	virtual ~Inv() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Inv)

	virtual const std::string GetDescription() const {
        return "Finds the inverse or pseudo-inverse of a matrix.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Inv";
    }

protected:

	virtual void _Initialize (const Arguments& parameters) {
		CheckParameters (parameters, _parameters);
		switch (StringSelection(parameters[0]).GetIndex()) {
			case 0: _method = CV_LU; break;
			case 1: _method = CV_SVD; break;
			case 2: _method = CV_SVD_SYM; break;
			default: _method = CV_LU;
		}
	}

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvInvert (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (output_args[0]).get(),
		 _method);
	}

protected:
	int _method;
}; // class Inv: public pIn

class IsInf: public CVBase {

	typedef CVBase Base;

public:

	IsInf(Runtime& runtime): Base(runtime) {

		DoubleValueInput ("value", "The input floating-point value.", 0.0, pI_TRUE);
		BoolValueOutput ("IsInf", "True if input is infinity according to IEEE754.");
	}

	virtual ~IsInf() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(IsInf)

	virtual const std::string GetDescription() const {
        return "Determines if the argument is Infinity.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/IsInf";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		BoolValue(output_args[0]).SetData (
		 cvIsInf (DoubleValue(input_args[0]).GetData()));
	}
}; // class IsInf: public pIn

class IsNaN: public CVBase {

	typedef CVBase Base;

public:

	IsNaN(Runtime& runtime): Base(runtime) {

		DoubleValueInput ("value", "The input floating-point value.", 0.0, pI_TRUE);
		BoolValueOutput ("IsInf", "True if input is Not A Number according to IEEE754.");
	}

	virtual ~IsNaN() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(IsNaN)

	virtual const std::string GetDescription() const {
        return "Determines if the argument is Not A Number.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/IsNaN";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		BoolValue(output_args[0]).SetData (
		 cvIsNaN (DoubleValue(input_args[0]).GetData()));
	}
}; // class IsNaN: public pIn

class LUT: public CVBase {

	typedef CVBase Base;

public:

	LUT(Runtime& runtime): Base(runtime) {

		CvArrInput ("src", "Source array of 8-bit elements", pI_TRUE);
		CvArrOutput ("dst", "Destination array of a given depth and of the same number of channels as the source array");
		CvArrInput ("lut", "Look-up table of 256 elements; should have the same depth as the destination array.", pI_TRUE);
	}

	virtual ~LUT() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(LUT)

	virtual const std::string GetDescription() const {
        return "Performs a look-up table transform of an array.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/LUT";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::CheckCvArrSizes (input_args[0], input_args[1]);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvLUT (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (output_args[0]).get(),
		 ArgumentAsCVArr (input_args[1]).get());
	}

}; // class LUT: public pIn

class Log: public CVBase {

	typedef CVBase Base;

public:

	Log(Runtime& runtime): Base(runtime) {

		CvArrInput ("src", "The source array", pI_TRUE);
		CvArrOutput ("dst", "The destination array");
	}

	virtual ~Log() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Log)

	virtual const std::string GetDescription() const {
        return "Calculates the natural logarithm of every array element's absolute value.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Log";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvLog (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (output_args[0]).get());
	}
}; // class Log: public pIn

class Mahalonobis: public CVBase {

	typedef CVBase Base;

public:

	Mahalonobis(Runtime& runtime): Base(runtime) {

		CvArrInput ("vec1", "The first 1D source vector", pI_TRUE);
		CvArrInput ("vec2", "The second 1D source vector", pI_TRUE);
		CvArrInput ("mat", "The inverse covariance matrix (may be calculated using the CalcCovarMatrix function)", pI_TRUE);
		CvArrOutput ("Mahalonobis", "Mahalonobis distance");
	}

	virtual ~Mahalonobis() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Mahalonobis)

	virtual const std::string GetDescription() const {
        return "Calculates the Mahalonobis distance between two vectors.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Mahalonobis";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::CheckCvArrSizes (input_args[0], input_args[1]);
		DoubleValue(output_args[0]).SetData (
		 cvMahalanobis (
		  ArgumentAsCVArr (input_args[0]).get(),
		  ArgumentAsCVArr (input_args[1]).get(),
		  ArgumentAsCVArr (input_args[2]).get()));
	}
}; // class Mahalonobis: public pIn

class Max: public CVBase {

	typedef CVBase Base;

public:

	Max(Runtime& runtime): Base(runtime) {

		CvArrInput ("src1", "The first source array", pI_TRUE);
		CvArrInput ("src2", "The second source array", pI_TRUE);
		CvArrOutput ("dst", "The destination array");
	}

	virtual ~Max() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Max)

	virtual const std::string GetDescription() const {
        return "Finds per-element maximum of two arrays.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Max";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::CheckCvArrSizes (input_args[0], input_args[1]);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvMax (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (input_args[1]).get(),
		 ArgumentAsCVArr (output_args[0]).get());
	}
}; // class Max: public pIn

class MaxS: public CVBase {

	typedef CVBase Base;

public:

	MaxS(Runtime& runtime): Base(runtime) {

		CvArrInput ("src", "The source array", pI_TRUE);
		DoubleValueInput ("value", "The scalar", 0.0, pI_TRUE);
		CvArrOutput ("dst", "The destination array");
	}

	virtual ~MaxS() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(MaxS)

	virtual const std::string GetDescription() const {
        return "Finds per-element maximum of array and scalar.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/MaxS";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvMaxS (
		 ArgumentAsCVArr (input_args[0]).get(),
		 DoubleValue(input_args[1]).GetData(),
		 ArgumentAsCVArr (output_args[0]).get());
	}
}; // class MaxS: public pIn

class Min: public CVBase {

	typedef CVBase Base;

public:

	Min(Runtime& runtime): Base(runtime) {

		CvArrInput ("src1", "The first source array", pI_TRUE);
		CvArrInput ("src2", "The second source array", pI_TRUE);
		CvArrOutput ("dst", "The destination array");
	}

	virtual ~Min() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Min)

	virtual const std::string GetDescription() const {
        return "Finds per-element minimum of two arrays.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Min";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::CheckCvArrSizes (input_args[0], input_args[1]);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvMin (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (input_args[1]).get(),
		 ArgumentAsCVArr (output_args[0]).get());
	}
}; // class Min: public pIn

class MinMax: public CVBase {

	typedef CVBase Base;

public:

	MinMax(Runtime& runtime): Base(runtime) {

		CvArrInput ("src1", "The first source array", pI_TRUE);
		DoubleValueOutput ("minVal", "minimum value in array");
		DoubleValueOutput ("maxVal", "maximum value in array");
	}

	virtual ~MinMax() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(MinMax)

	virtual const std::string GetDescription() const {
        return "Finds global minimum and maximum in array.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/MinMax";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		double arr_min, arr_max;
		// TODO: also return positions
		cvMinMaxLoc (ArgumentAsCVArr (input_args[0]).get(), &arr_min, &arr_max, 0, 0, 0);
		DoubleValue(output_args[0]).SetData (arr_min);
		DoubleValue(output_args[1]).SetData (arr_max);
	}
}; // class MinMax: public pIn

class MinS: public CVBase {

	typedef CVBase Base;

public:

	MinS(Runtime& runtime): Base(runtime) {

		CvArrInput ("src", "The source array", pI_TRUE);
		DoubleValueInput ("value", "The scalar", 0.0, pI_TRUE);
		CvArrOutput ("dst", "The destination array");
	}

	virtual ~MinS() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(MinS)

	virtual const std::string GetDescription() const {
        return "Finds per-element minimum of an array and a scalar.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/MinS";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvMinS (
		 ArgumentAsCVArr (input_args[0]).get(),
		 DoubleValue(input_args[1]).GetData(),
		 ArgumentAsCVArr (output_args[0]).get());
	}
}; // class MinS: public pIn

class Mul: public CVBase {

	typedef CVBase Base;

public:

	Mul(Runtime& runtime): Base(runtime) {

		CvArrInput ("src1", "The first source array", pI_TRUE);
		CvArrInput ("src2", "The second source array", pI_TRUE);
		CvArrOutput ("dst", "The destination array");
		DoubleValueInput ("scale", "Scale factor", 1.0, pI_TRUE);
	}

	virtual ~Mul() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Mul)

	virtual const std::string GetDescription() const {
        return "Calculates the per-element product of two arrays.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Mul";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::CheckCvArrSizes (input_args[0], input_args[1]);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvMul (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (input_args[1]).get(),
		 ArgumentAsCVArr (output_args[0]).get(),
		 DoubleValue(input_args[2]).GetData());
	}
}; // class Mul: public pIn

class MulSpectrums: public CVBase {

	typedef CVBase Base;

public:

	MulSpectrums(Runtime& runtime): Base(runtime), _rows(pI_FALSE), _conj(pI_FALSE) {

		CvArrInput ("src1", "The first source array", pI_TRUE);
		CvArrInput ("src2", "The second source array", pI_TRUE);
		CvArrOutput ("dst", "The destination array");
		BoolValueParameter ("rows", "Treat each row of the arrays as a separate spectrum");
		BoolValueParameter ("conj", "Tconjugate the second source array before the multiplication");
	}

	virtual ~MulSpectrums() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(MulSpectrums)

	virtual const std::string GetDescription() const {
        return "Performs per-element multiplication of two Fourier spectrums.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/MulSpectrums";
    }

protected:

	virtual void _Initialize (const Arguments& parameters) {
		CheckParameters (parameters, _parameters);
		_rows = BoolValue(parameters[0]).GetData();
		_conj = BoolValue(parameters[1]).GetData();
	}

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::CheckCvArrSizes (input_args[0], input_args[1]);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvMulSpectrums (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (input_args[1]).get(),
		 ArgumentAsCVArr (output_args[0]).get(),
		 (_rows == pI_TRUE ? CV_DXT_ROWS : 0) | 
		  (_conj == pI_TRUE ? CV_DXT_MUL_CONJ : 0));
	}

protected:
	pI_bool _rows, _conj;
}; // class MulSpectrums: public pIn

class MulTransposed: public CVBase {

	typedef CVBase Base;

public:

	MulTransposed(Runtime& runtime): Base(runtime) {

		CvArrInput ("src", "The source matrix", pI_TRUE);
		CvArrOutput ("dst", "The destination matrix. Must be floating point.");
		IntValueInput ("order", "Order of multipliers", 1, pI_TRUE);
		CvArrInput ("delta", "An array, subtracted from src before multiplication", pI_TRUE);
		DoubleValueInput ("scale", "Scale factor", 1.0, pI_TRUE);
	}

	virtual ~MulTransposed() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(MulTransposed)

	virtual const std::string GetDescription() const {
        return "Calculates the product of an array and a transposed array.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/MulTransposed";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::CheckCvArrSizes (input_args[0], input_args[2]);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvMulTransposed (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (output_args[0]).get(),
		 IntValue (input_args[1]).GetData(),
		 ArgumentAsCVArr (input_args[2]).get(),
		 DoubleValue(input_args[3]).GetData());
	}
}; // class MulTransposed: public pIn

class Norm: public CVBase {

	typedef CVBase Base;

public:

	Norm(Runtime& runtime): Base(runtime), _norm(CV_RELATIVE_C) {

		IntValueParameter ("normType", "Type of norm; either 0 = CV_RELATIVE_C, 1 = CV_RELATIVE_L1 or 2 = CV_RELATIVE_L2");
		CvArrInput ("arr1", "The first source image", pI_TRUE);
		CvArrInput ("arr2", "The (optional) second source image. If not given, bsolute norm of arr1 is calculated, otherwise the absolute or relative norm of arr1 - arr2 is calculated.", pI_TRUE);
		DoubleValueOutput ("norm", "Calculated norm");
	}

	virtual ~Norm() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Norm)

	virtual const std::string GetDescription() const {
        return "Calculates absolute array norm, absolute difference norm, or relative difference norm.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Norm";
    }

protected:

	virtual void _Initialize (const Arguments& parameters) {
		CheckParameters (parameters, _parameters);
		pI_int norm = IntValue(parameters[0]).GetData();
		_norm = CV_RELATIVE_C;
		if (norm == 1)
			_norm = CV_RELATIVE_L1;
		else if (norm == 2)
			_norm = CV_RELATIVE_L2;
	}

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		DoubleValue(output_args[0]).SetData (
		 cvNorm (
		  ArgumentAsCVArr (input_args[0]).get(),
		  input_args.size() > 1 ? ArgumentAsCVArr (input_args[1]).get() : 0,
		  _norm,
		  NULL /* always use zero mask. */));
	}

protected:
	pI_int _norm;

}; // class Norm: public pIn

class Not: public CVBase {

	typedef CVBase Base;

public:

	Not(Runtime& runtime): Base(runtime) {

		CvArrInput ("src", "The source array", pI_TRUE);
		CvArrOutput ("dst", "The destination array");
	}

	virtual ~Not() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Not)

	virtual const std::string GetDescription() const {
        return "Performs per-element bit-wise inversion of array elements.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Not";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvNot (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (output_args[0]).get());
	}
}; // class Not: public pIn

class Or: public CVBase {

	typedef CVBase Base;

public:

	Or(Runtime& runtime): Base(runtime) {

		CvArrInput ("src1", "The first source array", pI_TRUE);
		CvArrInput ("src2", "The second source array", pI_TRUE);
		CvArrOutput ("dst", "The destination array");
	}

	virtual ~Or() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Or)

	virtual const std::string GetDescription() const {
        return "Calculates per-element bit-wise disjunction of two arrays.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Or";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::CheckCvArrSizes (input_args[0], input_args[1]);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvOr (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (input_args[1]).get(),
		 ArgumentAsCVArr (output_args[0]).get(),
		 NULL /* always use zero mask. */);
	}
}; // class Or: public pIn

class OrS: public CVBase {

	typedef CVBase Base;

public:

	OrS(Runtime& runtime): Base(runtime) {

		CvArrInput ("src", "The source array", pI_TRUE);
		DoubleValueInput ("value", "Scalar to use in the operation", 1.0, pI_TRUE);
		CvArrOutput ("dst", "The destination array");
	}

	virtual ~OrS() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(OrS)

	virtual const std::string GetDescription() const {
        return "Calculates per-element bit-wise disjunction of an array and a scalar.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/OrS";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvOrS (
		 ArgumentAsCVArr (input_args[0]).get(),
		 DoubleValueAsCVScalar (input_args[1]),
		 ArgumentAsCVArr (output_args[0]).get(),
		 NULL /* always use zero mask. */);
	}
}; // class OrS: public pIn

class PerspectiveTransform: public CVBase {

	typedef CVBase Base;

public:

	PerspectiveTransform(Runtime& runtime): Base(runtime) {

		CvArrInput ("src", "The source three-channel floating-point array", pI_TRUE);
		CvArrOutput ("dst", "The destination three-channel floating-point array");
		CvMatInput ("mat", "3 x 3 or 4 x 4 transformation matrix", pI_TRUE);
	}

	virtual ~PerspectiveTransform() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(PerspectiveTransform)

	virtual const std::string GetDescription() const {
        return "Performs perspective matrix transformation of a vector array.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/PerspectiveTransform";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvPerspectiveTransform (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (output_args[0]).get(),
		 ArgumentAsCVMat (input_args[1]).get());
	}
}; // class PerspectiveTransform: public pIn

class PolarToCart: public CVBase {

	typedef CVBase Base;

public:

	PolarToCart(Runtime& runtime): Base(runtime), _angle_in_degrees(pI_FALSE) {

		BoolValueParameter ("angleInDegrees", "If true, angles are measured in degrees, otherwise in radians.");
		CvArrInput ("magnitude", "The array of magnitudes", pI_TRUE);
		CvArrInput ("angle", "The array of angles, whether in radians or degrees", pI_TRUE);
		CvArrOutput ("x", "The destination array of x-coordinates");
		CvArrOutput ("y", "The destination array of y-coordinates");
	}

	virtual ~PolarToCart() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(PolarToCart)

	virtual const std::string GetDescription() const {
        return "Calculates Cartesian coordinates of 2d vectors represented in polar form.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/PolarToCart";
    }

protected:

	virtual void _Initialize (const Arguments& parameters) {
		CheckParameters (parameters, _parameters);
		_angle_in_degrees = BoolValue(parameters[0]).GetData();
	}

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[1]);
		cvPolarToCart (
		 ArgumentAsCVArr(input_args[0]).get(),
		 ArgumentAsCVArr(input_args[1]).get(),
		 ArgumentAsCVArr(output_args[0]).get(),
		 ArgumentAsCVArr(output_args[1]).get(),
		 _angle_in_degrees == pI_TRUE ? 1 : 0);
	}

protected:
	pI_bool _angle_in_degrees;
}; // class PolarToCart: public pIn

class Pow: public CVBase {

	typedef CVBase Base;

public:

	Pow(Runtime& runtime): Base(runtime) {

		CvArrInput ("src", "The source array", pI_TRUE);
		CvArrOutput ("dst", "The destination array, should be the same type as the source");
		DoubleValueInput ("power", "The exponent of power", 2.0, pI_TRUE);
	}

	virtual ~Pow() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Pow)

	virtual const std::string GetDescription() const {
        return "Raises every array element to a power.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Pow";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvPow (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (output_args[0]).get(),
		 DoubleValue (input_args[1]).GetData());
	}
}; // class Pow: public pIn

class RandArr: public CVBase {

	typedef CVBase Base;

public:

	RandArr(Runtime& runtime): Base(runtime), _seed(42), _normal(pI_FALSE) {

		_rng = cvRNG (static_cast<int64> (_seed));
		CvArrOutput ("arr", "The destination array");
		IntValueParameter ("seed", "The random number seed");
		BoolValueParameter ("normal", "If true, a normal or Gaussian, otherwise an uniform distribution is used.");
		DoubleValueInput ("param1", "The first parameter of the distribution. In the case of a uniform distribution it is the inclusive lower boundary of the random numbers range. In the case of a normal distribution it is the mean value of the random numbers.", 0.0, pI_TRUE);
		DoubleValueInput ("param2", "The second parameter of the distribution. In the case of a uniform distribution it is the exclusive upper boundary of the random numbers range. In the case of a normal distribution it is the standard deviation of the random numbers.", 1.0, pI_TRUE);
	}

	virtual ~RandArr() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(RandArr)

	virtual const std::string GetDescription() const {
        return "Fills an array with random numbers.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/RandArr";
    }

protected:

	virtual void _Initialize (const Arguments& parameters) {
		CheckParameters (parameters, _parameters);
		_seed = IntValue(parameters[0]).GetData();
		_rng = cvRNG (static_cast<int64> (_seed));
		_normal = BoolValue(parameters[1]).GetData();
	}

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		cvRandArr (
		 &_rng,
		 ArgumentAsCVArr(output_args[0]).get(),
		 _normal == pI_FALSE ? CV_RAND_UNI : CV_RAND_NORMAL,
		 DoubleValueAsCVScalar (input_args[0]),
		 DoubleValueAsCVScalar (input_args[1]));
	}

protected:
	pI_int _seed;
	pI_bool _normal;
	CvRNG _rng;
}; // class RandArr: public pIn

class RandInt: public CVBase {

	typedef CVBase Base;

public:

	RandInt(Runtime& runtime): Base(runtime), _seed(42) {

		_rng = cvRNG (static_cast<int64> (_seed));
		IntValueOutput ("rand", "Generated random number");
		IntValueParameter ("seed", "The random number seed");
	}

	virtual ~RandInt() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(RandInt)

	virtual const std::string GetDescription() const {
        return "Returns a 32-bit integer.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/RandInt";
    }

protected:

	virtual void _Initialize (const Arguments& parameters) {
		CheckParameters (parameters, _parameters);
		_seed = IntValue(parameters[0]).GetData();
		_rng = cvRNG (static_cast<int64> (_seed));
	}

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		IntValue(output_args[0]).SetData (static_cast<pI_int> (cvRandInt (&_rng)));
	}

protected:
	pI_int _seed;
	CvRNG _rng;
}; // class RandInt: public pIn

class RandReal: public CVBase {

	typedef CVBase Base;

public:

	RandReal(Runtime& runtime): Base(runtime), _seed(42) {

		_rng = cvRNG (static_cast<int64> (_seed));
		DoubleValueOutput ("rand", "Generated random number");
		IntValueParameter ("seed", "The random number seed");
	}

	virtual ~RandReal() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(RandReal)

	virtual const std::string GetDescription() const {
        return "Returns a floating-point random number.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/RandReal";
    }

protected:

	virtual void _Initialize (const Arguments& parameters) {
		CheckParameters (parameters, _parameters);
		_seed = IntValue(parameters[0]).GetData();
		_rng = cvRNG (static_cast<int64> (_seed));
	}

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		DoubleValue(output_args[0]).SetData (cvRandReal (&_rng));
	}

protected:
	pI_int _seed;
	CvRNG _rng;
}; // class RandReal: public pIn

class Reduce: public CVBase {

	typedef CVBase Base;

public:

	Reduce(Runtime& runtime): Base(runtime) {

		CvArrInput ("src", "The input matrix.", pI_TRUE);
		CvArrOutput ("dst", "The output single-row/single-column vector that accumulates somehow all the matrix rows/columns.");
		StringSelection ssel(_runtime);
		ssel.SetName ("op")
			.SetDescription ("The reduction operation. It can take of the following values: CV_REDUCE_SUM = The output is the sum of all of the matrix’s rows/columns; CV_REDUCE_AVG = The output is the mean vector of all of the matrix’s rows/columns; CV_REDUCE_MAX = The output is the maximum (column/row-wise) of all of the matrix’s rows/columns; CV_REDUCE_MIN = The output is the minimum (column/row-wise) of all of the matrix’s rows/columns.")
			.SetCount (4);
		ssel.CreateData();
		ssel.SetSymbols (0, "CV_REDUCE_SUM");
		ssel.SetSymbols (1, "CV_REDUCE_AVG");
		ssel.SetSymbols (2, "CV_REDUCE_MAX");
		ssel.SetSymbols (3, "CV_REDUCE_MIN");
		ssel.SetIndex (0);
		_input_args.push_back (ssel);
	}

	virtual ~Reduce() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Reduce)

	virtual const std::string GetDescription() const {
        return "Reduces a matrix to a vector.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Reduce";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		StringSelection ssel(input_args[1]);
		int op = CV_REDUCE_SUM;
		if (ssel.GetIndex() == 1)
			op = CV_REDUCE_AVG;
		else if (ssel.GetIndex() == 2)
			op = CV_REDUCE_MAX;
		else if (ssel.GetIndex() == 3)
			op = CV_REDUCE_MIN;
		cvReduce (
		 ArgumentAsCVArr(input_args[0]).get(),
		 ArgumentAsCVArr(output_args[0]).get(),
		 -1,
		 op);
	}
}; // class Reduce: public pIn

class Repeat: public CVBase {

	typedef CVBase Base;

public:

	Repeat(Runtime& runtime): Base(runtime) {

		CvArrInput ("src", "Source array, image or matrix.", pI_TRUE);
		CvArrOutput ("dst", "Destination array, image or matrix.");
	}

	virtual ~Repeat() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Repeat)

	virtual const std::string GetDescription() const {
        return "Fill the destination array with repeated copies of the source array.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Repeat";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		cvRepeat (
		 ArgumentAsCVArr(input_args[0]).get(),
		 ArgumentAsCVArr(output_args[0]).get());
	}
}; // class Repeat: public pIn

class Round: public CVBase {

	typedef CVBase Base;

public:

	Round(Runtime& runtime): Base(runtime) {

		DoubleValueInput ("value", "The input floating-point value", 1.0, pI_TRUE);
		IntValueOutput ("dst", "Rounded value");
	}

	virtual ~Round() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Round)

	virtual const std::string GetDescription() const {
        return "Converts a floating-point number to an integer.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Round";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		IntValue(output_args[0]).SetData (
		 cvRound (DoubleValue (input_args[0]).GetData()));
	}
}; // class Round: public pIn

class Floor: public CVBase {

	typedef CVBase Base;

public:

	Floor(Runtime& runtime): Base(runtime) {

		DoubleValueInput ("value", "The input floating-point value", 1.0, pI_TRUE);
		IntValueOutput ("dst", "Floor value");
	}

	virtual ~Floor() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Floor)

	virtual const std::string GetDescription() const {
        return "Converts a floating-point number to an integer.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Floor";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		IntValue(output_args[0]).SetData (
		 cvFloor (DoubleValue (input_args[0]).GetData()));
	}
}; // class Floor: public pIn

class Ceil: public CVBase {

	typedef CVBase Base;

public:

	Ceil(Runtime& runtime): Base(runtime) {

		DoubleValueInput ("value", "The input floating-point value", 1.0, pI_TRUE);
		IntValueOutput ("dst", "Ceil value");
	}

	virtual ~Ceil() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Ceil)

	virtual const std::string GetDescription() const {
        return "Converts a floating-point number to an integer.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Ceil";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		IntValue(output_args[0]).SetData (
		 cvCeil (DoubleValue (input_args[0]).GetData()));
	}
}; // class Ceil: public pIn

class ScaleAdd: public CVBase {

	typedef CVBase Base;

public:

	ScaleAdd(Runtime& runtime): Base(runtime) {

		CvArrInput ("src1", "The first source array", pI_TRUE);
		DoubleValueInput ("value", "Scalar to use in the operation", 1.0, pI_TRUE);
		CvArrInput ("src2", "The second source array", pI_TRUE);
		CvArrOutput ("dst", "The destination array");
	}

	virtual ~ScaleAdd() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(ScaleAdd)

	virtual const std::string GetDescription() const {
        return "Calculates the sum of a scaled array and another array.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/ScaleAdd";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::CheckCvArrSizes (input_args[0], input_args[2]);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);

		cvScaleAdd (
		 ArgumentAsCVArr (input_args[0]).get(),
		 DoubleValueAsCVScalar (input_args[1]),
		 ArgumentAsCVArr (input_args[2]).get(),
		 ArgumentAsCVArr (output_args[0]).get());
	}
}; // class ScaleAdd: public pIn

class SetIdentity: public CVBase {

	typedef CVBase Base;

public:

	SetIdentity(Runtime& runtime): Base(runtime) {

		CvMatInput ("mat", "The matrix to initialize (not necesserily square)", pI_FALSE);
		DoubleValueInput ("value", "The value to assign to the diagonal elements", 1.0, pI_TRUE);
	}

	virtual ~SetIdentity() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(SetIdentity)

	virtual const std::string GetDescription() const {
        return "Initializes a scaled identity matrix.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/SetIdentity";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);

		cvSetIdentity (
		 ArgumentAsCVArr (input_args[0]).get(),
		 DoubleValueAsCVScalar (input_args[1]));
	}
}; // class SetIdentity: public pIn

class Solve: public CVBase {

	typedef CVBase Base;

public:

	Solve(Runtime& runtime): Base(runtime) {

		CvArrInput ("A", "The source matrix", pI_TRUE);
		CvArrInput ("B", "The right-hand part of the linear system", pI_TRUE);
		CvArrOutput ("X", "The output solution");
		StringSelection ssel(_runtime);
		ssel.SetName ("method")
			.SetDescription ("The solution (matrix inversion) method: CV_LU = Gaussian elimination with optimal pivot element chosen; CV_SVD = Singular value decomposition (SVD) method; CV_SVD_SYM = SVD method for a symmetric positively-defined matrix.")
			.SetCount (3);
		ssel.CreateData();
		ssel.SetSymbols (0, "CV_LU");
		ssel.SetSymbols (1, "CV_SVD");
		ssel.SetSymbols (2, "CV_SVD_SYM");
		ssel.SetIndex (0);
		_input_args.push_back (ssel);
	}

	virtual ~Solve() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Solve)

	virtual const std::string GetDescription() const {
        return "Solves a linear system or least-squares problem.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Solve";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		StringSelection ssel(input_args[2]);
		int op = CV_LU;
		if (ssel.GetIndex() == 1)
			op = CV_SVD;
		else if (ssel.GetIndex() == 2)
			op = CV_SVD_SYM;
		int result = cvSolve (
		 ArgumentAsCVArr(input_args[0]).get(),
		 ArgumentAsCVArr(input_args[1]).get(),
		 ArgumentAsCVArr(output_args[0]).get(),
		 op);
		if ((op == CV_LU) && (result == 0)) {
			throw exception::ExecutionException("Gaussian elimination does not yield result.");
		}
	}
}; // class Solve: public pIn

class SolveCubic: public CVBase {

	typedef CVBase Base;

public:

	SolveCubic(Runtime& runtime): Base(runtime) {

		CvMatInput ("coeffs", "The equation coefficients, an array of 3 or 4 elements", pI_TRUE);
		CvMatOutput ("roots", "The output array of real roots which should have 3 elements");
	}

	virtual ~SolveCubic() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(SolveCubic)

	virtual const std::string GetDescription() const {
        return "The function finds the real roots of a cubic equation.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/SolveCubic";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		cvSolveCubic (ArgumentAsCVMat(input_args[0]).get(), ArgumentAsCVMat(output_args[0]).get());
	}
}; // class SolveCubic: public pIn

class Sqrt: public CVBase {

	typedef CVBase Base;

public:

	Sqrt(Runtime& runtime): Base(runtime) {

		DoubleValueInput ("value", "The input floating-point value.", 1.0, pI_TRUE);
		DoubleValueOutput ("sqrt", "Square root.");
	}

	virtual ~Sqrt() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Sqrt)

	virtual const std::string GetDescription() const {
        return "Calculates the square root.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Sqrt";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		DoubleValue(output_args[0]).SetData (static_cast<pI_double> (
		 cvSqrt (
		  static_cast<float> (DoubleValue(input_args[0]).GetData()))));
	}
}; // class Sqrt: public pIn

class Sub: public CVBase {

	typedef CVBase Base;

public:

	Sub(Runtime& runtime): Base(runtime) {

		CvArrInput ("src1", "The first source array", pI_TRUE);
		CvArrInput ("src2", "The second source array", pI_TRUE);
		CvArrOutput ("dst", "The destination array");
	}

	virtual ~Sub() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Sub)

	virtual const std::string GetDescription() const {
        return "Computes the per-element difference between two arrays.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Sub";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::CheckCvArrSizes (input_args[0], input_args[1]);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvSub (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (input_args[1]).get(),
		 ArgumentAsCVArr (output_args[0]).get(),
		 NULL /* always use zero mask. */);
	}
}; // class Sub: public pIn

class SubRS: public CVBase {

	typedef CVBase Base;

public:

	SubRS(Runtime& runtime): Base(runtime) {

		CvArrInput ("src", "The source array", pI_TRUE);
		DoubleValueInput ("value", "Subtracted scalar", 1.0, pI_TRUE);
		CvArrOutput ("dst", "The destination array");
	}

	virtual ~SubRS() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(SubRS)

	virtual const std::string GetDescription() const {
        return "The function subtracts every element of source array from a scalar.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/SubRS";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvSubRS (
		 ArgumentAsCVArr (input_args[0]).get(),
		 DoubleValueAsCVScalar (input_args[1]),
		 ArgumentAsCVArr (output_args[0]).get(),
		 NULL /* always use zero mask. */);
	}
}; // class SubRS: public pIn

class SubS: public CVBase {

	typedef CVBase Base;

public:

	SubS(Runtime& runtime): Base(runtime) {

		CvArrInput ("src", "The source array", pI_TRUE);
		DoubleValueInput ("value", "Subtracted scalar", 1.0, pI_TRUE);
		CvArrOutput ("dst", "The destination array");
	}

	virtual ~SubS() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(SubS)

	virtual const std::string GetDescription() const {
        return "The function subtracts a scalar from every element of the source array.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/SubS";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvSubS (
		 ArgumentAsCVArr (input_args[0]).get(),
		 DoubleValueAsCVScalar (input_args[1]),
		 ArgumentAsCVArr (output_args[0]).get(),
		 NULL /* always use zero mask. */);
	}
}; // class SubS: public pIn

class Sum: public CVBase {

	typedef CVBase Base;

public:

	Sum(Runtime& runtime): Base(runtime) {

		CvArrInput ("src", "The source array", pI_TRUE);
		DoubleValueOutput ("sum", "Sum of the array values");
	}

	virtual ~Sum() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Sum)

	virtual const std::string GetDescription() const {
        return "Adds up array elements.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Sum";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		DoubleValue(output_args[0]).SetData (
		 cvSum (ArgumentAsCVArr (input_args[0]).get()).val[0]);
	}
}; // class Sum: public pIn

class Trace: public CVBase {

	typedef CVBase Base;

public:

	Trace(Runtime& runtime): Base(runtime) {

		CvArrInput ("src", "The source array", pI_TRUE);
		DoubleValueOutput ("sum", "Sum of the array values");
	}

	virtual ~Trace() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Trace)

	virtual const std::string GetDescription() const {
        return "The function returns the sum of the diagonal elements of the matrix (the trace).";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Trace";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		DoubleValue(output_args[0]).SetData (
		 cvTrace (ArgumentAsCVArr (input_args[0]).get()).val[0]);
	}
}; // class Trace: public pIn

class Transpose: public CVBase {

	typedef CVBase Base;

public:

	Transpose(Runtime& runtime): Base(runtime) {

		CvArrInput ("src", "The source matrix", pI_TRUE);
		CvArrOutput ("dst", "The destination matrix");
	}

	virtual ~Transpose() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Transpose)

	virtual const std::string GetDescription() const {
        return "Transposes a matrix.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Transpose";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0], pI_TRUE);
		cvTranspose (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (output_args[0]).get());
	}
}; // class Transpose: public pIn

class Xor: public CVBase {

	typedef CVBase Base;

public:

	Xor(Runtime& runtime): Base(runtime) {

		CvArrInput ("src1", "The first source array", pI_TRUE);
		CvArrInput ("src2", "The second source array", pI_TRUE);
		CvArrOutput ("dst", "The destination array");
	}

	virtual ~Xor() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Xor)

	virtual const std::string GetDescription() const {
        return "Performs per-element bit-wise exclusive or operation on two arrays.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/Xor";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::CheckCvArrSizes (input_args[0], input_args[1]);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvXor (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (input_args[1]).get(),
		 ArgumentAsCVArr (output_args[0]).get(),
		 NULL /* always use zero mask. */);
	}
}; // class Xor: public pIn

class XorS: public CVBase {

	typedef CVBase Base;

public:

	XorS(Runtime& runtime): Base(runtime) {

		CvArrInput ("src", "The source array", pI_TRUE);
		DoubleValueInput ("value", "Scalar to use in the operation", 1.0, pI_TRUE);
		CvArrOutput ("dst", "The destination array");
	}

	virtual ~XorS() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(XorS)

	virtual const std::string GetDescription() const {
        return "Performs per-element bit-wise exclusive or operation on an array and a scalar.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Core/XorS";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
		cvXorS (
		 ArgumentAsCVArr (input_args[0]).get(),
		 DoubleValueAsCVScalar (input_args[1]),
		 ArgumentAsCVArr (output_args[0]).get(),
		 NULL /* always use zero mask. */);
	}
}; // class XorS: public pIn


} // namespace opencv
} // namespace pIns
} // namespace pI

#endif // CV_CORE_HPP
