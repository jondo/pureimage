/* CVDraw.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CV_DRAW_HPP
#define CV_DRAW_HPP

#include "CVBase.hpp"

namespace pI {
namespace pIns {
namespace opencv {

class Line: public CVBase {

	typedef CVBase Base;

public:

	Line(Runtime& runtime): Base(runtime) {

		CvArrInput ("img", "The image", pI_FALSE);								// 0
		IntValueInput ("x1", "x coordinate of first point", 0, pI_TRUE);		// 1
		IntValueInput ("y1", "y coordinate of first point", 0, pI_TRUE);		// 2
		IntValueInput ("x2", "x coordinate of second point", 1, pI_TRUE);		// 3
		IntValueInput ("y2", "y coordinate of second point", 1, pI_TRUE);		// 4
		IntValueInput ("R", "red colour component", 127, pI_TRUE);				// 5
		IntValueInput ("G", "green colour component", 127, pI_TRUE);			// 6
		IntValueInput ("B", "blue colour component", 127, pI_TRUE);				// 7
		IntValueInput ("thickness", "line thickness", 1, pI_TRUE);				// 8
		StringSelection ssel(_runtime);
		ssel.SetName ("type")
			.SetDescription ("Type of the line: 8 = 8-connected line; 4 = 4-connected line; CV_AA = antialiased line.")
			.SetCount (3);
		ssel.CreateData();
		ssel.SetSymbols (0, "8");
		ssel.SetSymbols (1, "4");
		ssel.SetSymbols (2, "CV_AA");
		ssel.SetIndex (0);
		_input_args.push_back (ssel);								// 9
		IntValueInput ("shift", "Number of fractional bits in the point coordinates", 0, pI_TRUE);	// 10
	}

	virtual ~Line() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Line)

	virtual const std::string GetDescription() const {
        return "Draws a line segment connecting two points.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Draw/Line";
    }

protected:
	
	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		
		boost::shared_ptr<CvArr> img(ArgumentAsCVArr(input_args[0]));
		CvPoint pt1, pt2;
		pt1.x = IntValue(input_args[1]).GetData();
		pt1.y = IntValue(input_args[2]).GetData();
		pt2.x = IntValue(input_args[3]).GetData();
		pt2.y = IntValue(input_args[4]).GetData();
		CvScalar color;
		int thickness = 1, linetype = 8, shift = 0;
		if (input_args.size() > 7)
			color = CV_RGB(IntValue(input_args[5]).GetData(), IntValue(input_args[6]).GetData(), IntValue(input_args[7]).GetData());
		else
			color = CV_RGB(127, 127, 127);
		if (input_args.size() > 8)
			thickness = IntValue(input_args[8]).GetData();
		if (input_args.size() > 9) {
			if (StringSelection(input_args[9]).GetIndex() == 1)
				linetype = 4;
			else if (StringSelection(input_args[9]).GetIndex() == 2)
				linetype = CV_AA;
		}
		if (input_args.size() > 10)
			shift = IntValue(input_args[10]).GetData();
		cvLine (img.get(), pt1, pt2, color, thickness, linetype, shift);
	}
}; // class Line: public pIn

class PolyLine: public CVBase {

	typedef CVBase Base;

public:

	PolyLine(Runtime& runtime): Base(runtime) {

		CvArrInput ("img", "The image", pI_FALSE);							// 0
		IntPolygon ip(_runtime);
		ip.SetName ("pts");
		ip.SetDescription ("Polygon points");
		_input_args.push_back (ip);											// 1
		BoolValueInput ("is_closed", "Indicates whether the polylines must be drawn closed. If closed, the function draws the line from the last vertex of every contour to the first vertex", pI_FALSE, pI_TRUE); // 2
		IntValueInput ("R", "red colour component", 127, pI_TRUE);			// 3
		IntValueInput ("G", "green colour component", 127, pI_TRUE);		// 4
		IntValueInput ("B", "blue colour component", 127, pI_TRUE);			// 5
		IntValueInput ("thickness", "line thickness", 1, pI_TRUE);			// 6
		StringSelection ssel(_runtime);
		ssel.SetName ("type")
			.SetDescription ("Type of the line: 8 = 8-connected line; 4 = 4-connected line; CV_AA = antialiased line.")
			.SetCount (3);
		ssel.CreateData();
		ssel.SetSymbols (0, "8");
		ssel.SetSymbols (1, "4");
		ssel.SetSymbols (2, "CV_AA");
		ssel.SetIndex (0);
		_input_args.push_back (ssel);										// 7
		IntValueInput ("shift", "Number of fractional bits in the point coordinates", 0, pI_TRUE);	// 8
	}

	virtual ~PolyLine() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(PolyLine)

	virtual const std::string GetDescription() const {
        return "Draws simple or thick polygons.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Draw/PolyLine";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		
		boost::shared_ptr<CvArr> img(ArgumentAsCVArr(input_args[0]));
		IntPolygon ip(input_args[1]);
		int npts(static_cast<int> (ip.GetCount()));
		CvPoint** pts = new CvPoint*[1];
		pts[0] = new CvPoint[npts];
		// copy polygon values
		for (int lv = 0; lv < npts; ++lv) {
			pts[0][lv].x = ip.GetX (lv);
			pts[0][lv].y = ip.GetY (lv);
		}

		int is_closed(BoolValue(input_args[2]).GetData() == pI_TRUE ? 1 : 0);

		CvScalar color;
		int thickness = 1, linetype = 8, shift = 0;
		if (input_args.size() > 5)
			color = CV_RGB(IntValue(input_args[3]).GetData(), IntValue(input_args[4]).GetData(), IntValue(input_args[5]).GetData());
		else
			color = CV_RGB(127, 127, 127);
		if (input_args.size() > 6)
			thickness = IntValue(input_args[6]).GetData();
		if (input_args.size() > 7) {
			if (StringSelection(input_args[7]).GetIndex() == 1)
				linetype = 4;
			else if (StringSelection(input_args[7]).GetIndex() == 2)
				linetype = CV_AA;
		}
		if (input_args.size() > 8)
			shift = IntValue(input_args[8]).GetData();

		cvPolyLine (img.get(), pts, &npts, 1, is_closed, color, thickness, linetype, shift);

		delete[] pts[0];
		delete[] pts;
	}
}; // class PolyLine: public pIn

class Rectangle: public CVBase {

	typedef CVBase Base;

public:

	Rectangle(Runtime& runtime): Base(runtime) {

		CvArrInput ("img", "The image", pI_FALSE);								// 0
		IntValueInput ("x1", "x coordinate of first point", 0, pI_TRUE);		// 1
		IntValueInput ("y1", "y coordinate of first point", 0, pI_TRUE);		// 2
		IntValueInput ("x2", "x coordinate of second point", 1, pI_TRUE);		// 3
		IntValueInput ("y2", "y coordinate of second point", 1, pI_TRUE);		// 4
		IntValueInput ("R", "red colour component", 127, pI_TRUE);				// 5
		IntValueInput ("G", "green colour component", 127, pI_TRUE);			// 6
		IntValueInput ("B", "blue colour component", 127, pI_TRUE);				// 7
		IntValueInput ("thickness", "line thickness", 1, pI_TRUE);				// 8
		StringSelection ssel(_runtime);
		ssel.SetName ("type")
			.SetDescription ("Type of the line: 8 = 8-connected line; 4 = 4-connected line; CV_AA = antialiased line.")
			.SetCount (3);
		ssel.CreateData();
		ssel.SetSymbols (0, "8");
		ssel.SetSymbols (1, "4");
		ssel.SetSymbols (2, "CV_AA");
		ssel.SetIndex (0);
		_input_args.push_back (ssel);											// 9
		IntValueInput ("shift", "Number of fractional bits in the point coordinates", 0, pI_TRUE);	// 10
	}

	virtual ~Rectangle() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Rectangle)

	virtual const std::string GetDescription() const {
        return "Draws a simple, thick, or filled rectangle.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Draw/Rectangle";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		
		boost::shared_ptr<CvArr> img(ArgumentAsCVArr(input_args[0]));
		CvPoint pt1, pt2;
		pt1.x = IntValue(input_args[1]).GetData();
		pt1.y = IntValue(input_args[2]).GetData();
		pt2.x = IntValue(input_args[3]).GetData();
		pt2.y = IntValue(input_args[4]).GetData();
		CvScalar color;
		int thickness = 1, linetype = 8, shift = 0;
		if (input_args.size() > 7)
			color = CV_RGB(IntValue(input_args[5]).GetData(), IntValue(input_args[6]).GetData(), IntValue(input_args[7]).GetData());
		else
			color = CV_RGB(127, 127, 127);
		if (input_args.size() > 8)
			thickness = IntValue(input_args[8]).GetData();
		if (input_args.size() > 9) {
			if (StringSelection(input_args[9]).GetIndex() == 1)
				linetype = 4;
			else if (StringSelection(input_args[9]).GetIndex() == 2)
				linetype = CV_AA;
		}
		if (input_args.size() > 10)
			shift = IntValue(input_args[10]).GetData();
		cvRectangle (img.get(), pt1, pt2, color, thickness, linetype, shift);
	}
}; // class Rectangle: public pIn

class PutText: public CVBase {

	typedef CVBase Base;

public:

	PutText(Runtime& runtime): Base(runtime), _vscale(1.0), _hscale(1.0) {

		StringSelection fsel(_runtime);
		fsel.SetName ("font")
			.SetDescription ("Font name identifier.")
			.SetCount (8);
		fsel.CreateData();
		fsel.SetSymbols (0, "CV_FONT_HERSHEY_SIMPLEX");
		fsel.SetSymbols (1, "CV_FONT_HERSHEY_PLAIN");
		fsel.SetSymbols (2, "CV_FONT_HERSHEY_DUPLEX");
		fsel.SetSymbols (3, "CV_FONT_HERSHEY_COMPLEX");
		fsel.SetSymbols (4, "CV_FONT_HERSHEY_TRIPLEX");
		fsel.SetSymbols (5, "CV_FONT_HERSHEY_COMPLEX_SMALL");
		fsel.SetSymbols (6, "CV_FONT_HERSHEY_SCRIPT_SIMPLEX");
		fsel.SetSymbols (7, "CV_FONT_HERSHEY_SCRIPT_COMPLEX");
		fsel.SetIndex (0);
		_parameters.push_back (fsel);												// 0
		BoolValueParameter ("italic", "If pI_TRUE, the font is rendered italic.", pI_FALSE);	// 1
		DoubleValueParameter ("hscale", "Horizontal scale", 1.0);					// 2
		DoubleValueParameter ("vscale", "Vertical scale", 1.0);						// 3
		DoubleValueParameter ("shear", "Approximate tangent of the character slope relative to the vertical line.", 0.0);	// 4
		IntValueParameter ("thickness", "Thickness of the text strokes", 1);		// 5
		
		StringSelection lsel(_runtime);
		lsel.SetName ("type")
			.SetDescription ("Type of the line: 8 = 8-connected line; 4 = 4-connected line; CV_AA = antialiased line.")
			.SetCount (3);
		lsel.CreateData();
		lsel.SetSymbols (0, "8");
		lsel.SetSymbols (1, "4");
		lsel.SetSymbols (2, "CV_AA");
		lsel.SetIndex (0);
		_parameters.push_back (lsel);												// 6
	}

	virtual ~PutText() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(PutText)

	virtual const std::string GetDescription() const {
        return "Draws a text string.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Draw/PutText";
    }

protected:

	virtual void _Initialize (const Arguments& parameters) {
		CheckParameters (parameters, _parameters, 1);
		StringSelection fsel(parameters[0]);
		int font_face = CV_FONT_HERSHEY_SIMPLEX;
		switch (fsel.GetIndex()) {
			case 0: font_face = CV_FONT_HERSHEY_SIMPLEX; break;
			case 1: font_face = CV_FONT_HERSHEY_PLAIN; break;
			case 2: font_face = CV_FONT_HERSHEY_DUPLEX; break;
			case 3: font_face = CV_FONT_HERSHEY_COMPLEX; break;
			case 4: font_face = CV_FONT_HERSHEY_TRIPLEX; break;
			case 5: font_face = CV_FONT_HERSHEY_COMPLEX_SMALL; break;
			case 6: font_face = CV_FONT_HERSHEY_SCRIPT_SIMPLEX; break;
			case 7: font_face = CV_FONT_HERSHEY_SCRIPT_COMPLEX; break;
		}
		if (parameters.size() > 1) {
			CheckParameter (parameters[1], _parameters[1]);
			if (BoolValue(parameters[1]).GetData() == pI_TRUE)
				font_face |= CV_FONT_ITALIC;
		}

		double shear(0.0);
		_vscale = 1.0, _hscale = 1.0;
		int thickness(1), line_type(8);
		if (parameters.size() > 2) {
			CheckParameter (parameters[2], _parameters[2]);
			_hscale = static_cast<double> (DoubleValue(parameters[2]).GetData());
		}
		if (parameters.size() > 3) {
			CheckParameter (parameters[3], _parameters[3]);
			_vscale = static_cast<double> (DoubleValue(parameters[3]).GetData());
		}
		if (parameters.size() > 4) {
			CheckParameter (parameters[4], _parameters[4]);
			shear = static_cast<double> (DoubleValue(parameters[4]).GetData());
		}
		if (parameters.size() > 5) {
			CheckParameter (parameters[5], _parameters[5]);
			thickness = static_cast<int> (IntValue(parameters[5]).GetData());
		}
		if (parameters.size() > 6) {
			CheckParameter (parameters[6], _parameters[6]);
			if (StringSelection(parameters[6]).GetIndex() == 1)
				line_type = 4;
			else if (StringSelection(parameters[6]).GetIndex() == 2)
				line_type = CV_AA;
		}

		// note: opencv lacks a font cleanup function, so next line probably holds a memory leak
		cvInitFont (&_font, font_face, _hscale, _vscale, shear, thickness, line_type);

		_input_args.clear(); _output_args.clear();
		CvArrInput ("img", "The image", pI_FALSE);							// 0
		_input_args.push_back (StringValue(_runtime).SetName ("text").SetDescription ("String to print")); // 1
		IntValueInput ("x", "x coordinate", 0, pI_TRUE);					// 2
		IntValueInput ("y", "y coordinate", 0, pI_TRUE);					// 3
		IntValueInput ("R", "red colour component", 127, pI_TRUE);			// 4
		IntValueInput ("G", "green colour component", 127, pI_TRUE);		// 5
		IntValueInput ("B", "blue colour component", 127, pI_TRUE);			// 6
		BoolValueInput ("do_draw", "if draw is pI_TRUE, the text is rendered, otherwise only the bounding box is calculated.", pI_TRUE, pI_TRUE); // 7
		
		IntValueOutput ("width", "width of the rendered text");
		IntValueOutput ("height", "height of the rendered text (does not include the height of character parts that are below the baseline)");
	}

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		
		CheckInputArguments (input_args, _input_args, 4);
		boost::shared_ptr<CvArr> img(ArgumentAsCVArr(input_args[0]));
		StringValue text(input_args[1]);
		CvPoint org;
		org.x = static_cast<int> (IntValue(input_args[2]).GetData());
		org.y = static_cast<int> (IntValue(input_args[3]).GetData());
		CvScalar color;
		if (input_args.size() > 6)
			color = CV_RGB(IntValue(input_args[4]).GetData(), IntValue(input_args[5]).GetData(), IntValue(input_args[6]).GetData());
		else
			color = CV_RGB(127, 127, 127);

		pI_bool do_draw(pI_TRUE);
		if (input_args.size() > 7)
			do_draw = BoolValue(input_args[7]).GetData();

		if (output_args.size() > 1) {
			CheckOutputArguments (output_args, _output_args);
			CvSize text_size;
			int baseline;
			cvGetTextSize(text.GetData(), &_font, &text_size, &baseline);
			IntValue(output_args[0]).SetData (text_size.width);
			IntValue(output_args[1]).SetData (text_size.height);
		}
		if (do_draw == pI_TRUE) {
			cvPutText (img.get(), text.GetData(), org, &_font, color);
		}
	}

protected:

	CvFont _font;
	pI_double _vscale, _hscale;
}; // class PutText: public pIn

class Circle: public CVBase {

	typedef CVBase Base;

public:

	Circle(Runtime& runtime): Base(runtime) {

		CvArrInput ("img", "The image", pI_FALSE);								// 0
		IntValueInput ("x", "x coordinate of circle center", 0, pI_TRUE);		// 1
		IntValueInput ("y", "y coordinate of circle center", 0 , pI_TRUE);		// 2
		IntValueInput ("radius", "radius of the circle", 1, pI_TRUE);			// 3
		IntValueInput ("R", "red colour component", 127, pI_TRUE);				// 4
		IntValueInput ("G", "green colour component", 127, pI_TRUE);			// 5
		IntValueInput ("B", "blue colour component", 127, pI_TRUE);				// 6
		IntValueInput ("thickness", "line thickness", 1, pI_TRUE);				// 7
		StringSelection ssel(_runtime);
		ssel.SetName ("type")
			.SetDescription ("Type of the line: 8 = 8-connected line; 4 = 4-connected line; CV_AA = antialiased line.")
			.SetCount (3);
		ssel.CreateData();
		ssel.SetSymbols (0, "8");
		ssel.SetSymbols (1, "4");
		ssel.SetSymbols (2, "CV_AA");
		ssel.SetIndex (0);
		_input_args.push_back (ssel);											// 8
		IntValueInput ("shift", "Number of fractional bits in the point coordinates", 0, pI_TRUE);	// 9
	}

	virtual ~Circle() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Circle)

	virtual const std::string GetDescription() const {
        return "The function draws a simple or filled circle with a given center and radius.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Draw/Circle";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		
		boost::shared_ptr<CvArr> img(ArgumentAsCVArr(input_args[0]));
		CvPoint center;
		pI_int radius;
		center.x = IntValue(input_args[1]).GetData();
		center.y = IntValue(input_args[2]).GetData();
		radius = IntValue(input_args[3]).GetData();
		CvScalar color;
		int thickness = 1, linetype = 8, shift = 0;
		if (input_args.size() > 6)
			color = CV_RGB(IntValue(input_args[4]).GetData(), IntValue(input_args[5]).GetData(), IntValue(input_args[6]).GetData());
		else
			color = CV_RGB(127, 127, 127);
		if (input_args.size() > 7)
			thickness = IntValue(input_args[7]).GetData();
		if (input_args.size() > 8) {
			if (StringSelection(input_args[8]).GetIndex() == 1)
				linetype = 4;
			else if (StringSelection(input_args[8]).GetIndex() == 2)
				linetype = CV_AA;
		}
		if (input_args.size() > 9)
			shift = IntValue(input_args[9]).GetData();
		cvCircle (img.get(), center, radius, color, thickness, linetype, shift);
	}
}; // class Circle: public pIn

class ClipLine: public CVBase {

	typedef CVBase Base;

public:

	ClipLine(Runtime& runtime): Base(runtime) {

		IntValueInput ("width", "image width", 0, pI_TRUE);					// 0
		IntValueInput ("height", "image height", 0, pI_TRUE);				// 1
		IntValueInput ("x1", "x coordinate of first point", 0, pI_FALSE);	// 2
		IntValueInput ("y1", "y coordinate of first point", 0, pI_FALSE);	// 3
		IntValueInput ("x2", "x coordinate of second point", 1, pI_FALSE);	// 4
		IntValueInput ("y2", "y coordinate of second point", 1, pI_FALSE);	// 5
	}

	virtual ~ClipLine() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(ClipLine)

	virtual const std::string GetDescription() const {
        return "Clips the line against the image rectangle.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Draw/ClipLine";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);
		CvSize img_size;
		img_size.width = IntValue(input_args[0]).GetData();
		img_size.height = IntValue(input_args[1]).GetData();
		CvPoint pt1, pt2;
		pt1.x = IntValue(input_args[2]).GetData();
		pt1.y = IntValue(input_args[3]).GetData();
		pt2.x = IntValue(input_args[4]).GetData();
		pt2.y = IntValue(input_args[5]).GetData();
		cvClipLine (img_size, &pt1, &pt2);
		IntValue(input_args[2]).SetData (pt1.x);
		IntValue(input_args[2]).SetData (pt1.y);
		IntValue(input_args[2]).SetData (pt2.x);
		IntValue(input_args[2]).SetData (pt2.y);
	}
}; // class ClipLine: public pIn


class Ellipse: public CVBase {

	typedef CVBase Base;

public:

	Ellipse(Runtime& runtime): Base(runtime) {

		CvArrInput ("img", "The image", pI_FALSE);												// 0
		IntValueInput ("x", "x coordinate of ellipse center", 0, pI_TRUE);						// 1
		IntValueInput ("y", "y coordinate of ellipse center", 0, pI_TRUE);						// 2
		IntValueInput ("len_x", "Length of the ellipse x-axis", 1, pI_TRUE);					// 3
		IntValueInput ("len_y", "Length of the ellipse y-axis", 1, pI_TRUE);					// 4
		DoubleValueInput ("angle", "Rotation angle", 0.0, pI_TRUE);								// 5
		DoubleValueInput ("start_angle", "Starting angle of the elliptic arc", 0.0, pI_TRUE);	// 6
		DoubleValueInput ("end_angle", "Ending angle of the elliptic arc", 0.0, pI_TRUE);		// 7
		IntValueInput ("R", "red colour component", 127, pI_TRUE);								// 8
		IntValueInput ("G", "green colour component", 127, pI_TRUE);							// 9
		IntValueInput ("B", "blue colour component", 127, pI_TRUE);								// 10
		IntValueInput ("thickness", "Thickness of the ellipse arc outline if positive, otherwise this indicates that a filled ellipse sector is to be drawn", 1, pI_TRUE);			// 11

		StringSelection ssel(_runtime);
		ssel.SetName ("type")
			.SetDescription ("Type of the line: 8 = 8-connected line; 4 = 4-connected line; CV_AA = antialiased line.")
			.SetCount (3);
		ssel.CreateData();
		ssel.SetSymbols (0, "8");
		ssel.SetSymbols (1, "4");
		ssel.SetSymbols (2, "CV_AA");
		ssel.SetIndex (0);
		_input_args.push_back (ssel);															// 12
		IntValueInput ("shift", "Number of fractional bits in the point coordinates", 0, pI_TRUE);	// 13
	}

	virtual ~Ellipse() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Ellipse)

	virtual const std::string GetDescription() const {
        return "The function draws a simple or thick elliptic arc or fills an ellipse sector.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Draw/Ellipse";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		
		CheckInputArguments (input_args, _input_args, 8);

		boost::shared_ptr<CvArr> img(ArgumentAsCVArr(input_args[0]));
		CvPoint center;
		center.x = IntValue(input_args[1]).GetData();
		center.y = IntValue(input_args[2]).GetData();
		CvSize axes;
		axes.width = IntValue(input_args[3]).GetData();
		axes.height = IntValue(input_args[4]).GetData();
		double angle = DoubleValue(input_args[5]).GetData();
		double start_angle = DoubleValue(input_args[6]).GetData();
		double end_angle = DoubleValue(input_args[7]).GetData();

		CvScalar color;
		int thickness = 1, linetype = 8, shift = 0;
		if (input_args.size() > 10)
			color = CV_RGB(IntValue(input_args[8]).GetData(), IntValue(input_args[9]).GetData(), IntValue(input_args[10]).GetData());
		else
			color = CV_RGB(127, 127, 127);
		if (input_args.size() > 11)
			thickness = IntValue(input_args[11]).GetData();
		if (input_args.size() > 12) {
			if (StringSelection(input_args[12]).GetIndex() == 1)
				linetype = 4;
			else if (StringSelection(input_args[12]).GetIndex() == 2)
				linetype = CV_AA;
		}
		if (input_args.size() > 13)
			shift = IntValue(input_args[13]).GetData();
		cvEllipse (img.get(), center, axes, angle, start_angle, end_angle, color, thickness, linetype, shift);
	}
}; // class Ellipse: public pIn

class EllipseBox: public CVBase {

	typedef CVBase Base;

public:

	EllipseBox(Runtime& runtime): Base(runtime) {

		CvArrInput ("img", "The image", pI_FALSE);												// 0
		DoubleValueInput ("x", "x coordinate of ellipse center", 0.0, pI_TRUE);					// 1
		DoubleValueInput ("y", "y coordinate of ellipse center", 0.0, pI_TRUE);					// 2
		DoubleValueInput ("len_x", "Length of the ellipse x-axis", 1.0, pI_TRUE);				// 3
		DoubleValueInput ("len_y", "Length of the ellipse y-axis", 1.0, pI_TRUE);				// 4		
		DoubleValueInput ("angle", "Rotation angle", 0.0, pI_TRUE);								// 5

		IntValueInput ("R", "red colour component", 127, pI_TRUE);								// 6
		IntValueInput ("G", "green colour component", 127, pI_TRUE);							// 7
		IntValueInput ("B", "blue colour component", 127, pI_TRUE);								// 8
		IntValueInput ("thickness", "Thickness of the ellipse arc outline if positive, otherwise this indicates that a filled ellipse sector is to be drawn", 1, pI_TRUE);			// 9

		StringSelection ssel(_runtime);
		ssel.SetName ("type")
			.SetDescription ("Type of the line: 8 = 8-connected line; 4 = 4-connected line; CV_AA = antialiased line.")
			.SetCount (3);
		ssel.CreateData();
		ssel.SetSymbols (0, "8");
		ssel.SetSymbols (1, "4");
		ssel.SetSymbols (2, "CV_AA");
		ssel.SetIndex (0);
		_input_args.push_back (ssel);															// 10
		IntValueInput ("shift", "Number of fractional bits in the point coordinates", 0, pI_TRUE);	// 11
	}

	virtual ~EllipseBox() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(EllipseBox)

	virtual const std::string GetDescription() const {
        return "The function draws a simple or thick ellipse outline, or fills an ellipse.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Draw/EllipseBox";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		
		CheckInputArguments (input_args, _input_args, 6);

		boost::shared_ptr<CvArr> img(ArgumentAsCVArr(input_args[0]));
		CvBox2D box;
		box.center.x = static_cast<float> (DoubleValue(input_args[1]).GetData());
		box.center.y = static_cast<float> (DoubleValue(input_args[2]).GetData());
		box.size.width = static_cast<float> (DoubleValue(input_args[3]).GetData());
		box.size.height = static_cast<float> (DoubleValue(input_args[4]).GetData());
		box.angle = static_cast<float> (DoubleValue(input_args[5]).GetData());

		CvScalar color;
		int thickness = 1, linetype = 8, shift = 0;
		if (input_args.size() > 8)
			color = CV_RGB(IntValue(input_args[6]).GetData(), IntValue(input_args[7]).GetData(), IntValue(input_args[8]).GetData());
		else
			color = CV_RGB(127, 127, 127);
		if (input_args.size() > 9)
			thickness = IntValue(input_args[9]).GetData();
		if (input_args.size() > 10) {
			if (StringSelection(input_args[10]).GetIndex() == 1)
				linetype = 4;
			else if (StringSelection(input_args[10]).GetIndex() == 2)
				linetype = CV_AA;
		}
		if (input_args.size() > 11)
			shift = IntValue(input_args[11]).GetData();

		cvEllipseBox (img.get(), box, color, thickness, linetype, shift);
	}
}; // class EllipseBox: public pIn

class FillPoly: public CVBase {

	typedef CVBase Base;

public:

	FillPoly(Runtime& runtime): Base(runtime) {

		CvArrInput ("img", "The image", pI_FALSE);							// 0
		IntPolygon ip(_runtime);
		ip.SetName ("pts");
		ip.SetDescription ("Polygon points");
		_input_args.push_back (ip);											// 1
		IntValueInput ("R", "red colour component", 127, pI_TRUE);			// 2
		IntValueInput ("G", "green colour component", 127, pI_TRUE);		// 3
		IntValueInput ("B", "blue colour component", 127, pI_TRUE);			// 4
		StringSelection ssel(_runtime);
		ssel.SetName ("type")
			.SetDescription ("Type of the line: 8 = 8-connected line; 4 = 4-connected line; CV_AA = antialiased line.")
			.SetCount (3);
		ssel.CreateData();
		ssel.SetSymbols (0, "8");
		ssel.SetSymbols (1, "4");
		ssel.SetSymbols (2, "CV_AA");
		ssel.SetIndex (0);
		_input_args.push_back (ssel);										// 5
		IntValueInput ("shift", "Number of fractional bits in the point coordinates", 0, pI_TRUE);	// 6
	}

	virtual ~FillPoly() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(FillPoly)

	virtual const std::string GetDescription() const {
        return "Fills a polygon's interior.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Draw/FillPoly";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		
		CheckInputArguments (input_args, _input_args, 2);
		boost::shared_ptr<CvArr> img(ArgumentAsCVArr(input_args[0]));
		IntPolygon ip(input_args[1]);
		int npts(static_cast<int> (ip.GetCount()));
		CvPoint** pts = new CvPoint*[1];
		pts[0] = new CvPoint[npts];
		// copy polygon values
		for (int lv = 0; lv < npts; ++lv) {
			pts[0][lv].x = ip.GetX (lv);
			pts[0][lv].y = ip.GetY (lv);
		}

		CvScalar color;
		int linetype = 8, shift = 0;
		if (input_args.size() > 4)
			color = CV_RGB(IntValue(input_args[2]).GetData(), IntValue(input_args[3]).GetData(), IntValue(input_args[4]).GetData());
		else
			color = CV_RGB(127, 127, 127);

		if (input_args.size() > 5) {
			if (StringSelection(input_args[5]).GetIndex() == 1)
				linetype = 4;
			else if (StringSelection(input_args[5]).GetIndex() == 2)
				linetype = CV_AA;
		}
		if (input_args.size() > 6)
			shift = IntValue(input_args[6]).GetData();

		cvFillPoly (img.get(), pts, &npts, 1, color, linetype, shift);

		delete[] pts[0];
		delete[] pts;
	}
}; // class FillPoly: public pIn

class FillConvexPoly: public CVBase {

	typedef CVBase Base;

public:

	FillConvexPoly(Runtime& runtime): Base(runtime) {

		CvArrInput ("img", "The image", pI_FALSE);							// 0
		IntPolygon ip(_runtime);
		ip.SetName ("pts");
		ip.SetDescription ("Polygon points");
		_input_args.push_back (ip);											// 1
		IntValueInput ("R", "red colour component", 127, pI_TRUE);			// 2
		IntValueInput ("G", "green colour component", 127, pI_TRUE);		// 3
		IntValueInput ("B", "blue colour component", 127, pI_TRUE);			// 4
		StringSelection ssel(_runtime);
		ssel.SetName ("type")
			.SetDescription ("Type of the line: 8 = 8-connected line; 4 = 4-connected line; CV_AA = antialiased line.")
			.SetCount (3);
		ssel.CreateData();
		ssel.SetSymbols (0, "8");
		ssel.SetSymbols (1, "4");
		ssel.SetSymbols (2, "CV_AA");
		ssel.SetIndex (0);
		_input_args.push_back (ssel);										// 5
		IntValueInput ("shift", "Number of fractional bits in the point coordinates", 0, pI_TRUE);	// 6
	}

	virtual ~FillConvexPoly() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(FillConvexPoly)

	virtual const std::string GetDescription() const {
        return "Fills a convex polygon.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Draw/FillConvexPoly";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		
		CheckInputArguments (input_args, _input_args, 2);
		boost::shared_ptr<CvArr> img(ArgumentAsCVArr(input_args[0]));
		IntPolygon ip(input_args[1]);
		int npts(static_cast<int> (ip.GetCount()));
		CvPoint* pts = new CvPoint[npts];
		// copy polygon values
		for (int lv = 0; lv < npts; ++lv) {
			pts[lv].x = ip.GetX (lv);
			pts[lv].y = ip.GetY (lv);
		}

		CvScalar color;
		int linetype = 8, shift = 0;
		if (input_args.size() > 4)
			color = CV_RGB(IntValue(input_args[2]).GetData(), IntValue(input_args[3]).GetData(), IntValue(input_args[4]).GetData());
		else
			color = CV_RGB(127, 127, 127);

		if (input_args.size() > 5) {
			if (StringSelection(input_args[5]).GetIndex() == 1)
				linetype = 4;
			else if (StringSelection(input_args[5]).GetIndex() == 2)
				linetype = CV_AA;
		}
		if (input_args.size() > 6)
			shift = IntValue(input_args[6]).GetData();

		cvFillConvexPoly (img.get(), pts, npts, color, linetype, shift);

		delete[] pts;
	}
}; // class FillConvexPoly: public pIn

} // namespace opencv
} // namespace pIns
} // namespace pI

#endif // CV_DRAW_HPP
