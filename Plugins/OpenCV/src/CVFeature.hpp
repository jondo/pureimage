/* CVFeature.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CV_FEATURE_HPP
#define CV_FEATURE_HPP

#include "CVBase.hpp"

namespace pI {
namespace pIns {
namespace opencv {

class ExtractSURF: public CVBase {

	typedef CVBase Base;

public:

	ExtractSURF(Runtime& runtime):
	 Base(runtime),
	 _storage(0) {
		
		// setup memory storage
		_storage = cvCreateMemStorage();
		if (_storage == 0)
			throw exception::MemoryException("Failed to allocate memory for local memory storage.");
		// setup params
		BoolValueParameter("extended", "pI_FALSE means basic descriptors are used", pI_FALSE);
		DoubleValueParameter("hessian_thresh", "hessian threshold to discard smaller features", 400.0);
		IntValueParameter("nOctaves", "the number of octaves to be used for extraction", 3);
		IntValueParameter("nOctaveLayers", "The number of layers within each octave", 4);
		// setup I/O
		CvArrInput ("img", "The image", pI_TRUE);
		CvArrInput ("mask", "Optional input mask", pI_TRUE);
		_output_args.push_back (
		 DoubleTable(runtime).SetName ("features").SetDescription ("detected SURF features"));
		// setup default SURF parameters in case initialize was omitted for some reason
		_surf_params.extended = 0;
		_surf_params.hessianThreshold = 400.0;
		_surf_params.nOctaves = 3;
		_surf_params.nOctaveLayers = 4;
	}

	virtual ~ExtractSURF() {
		cvReleaseMemStorage (&_storage);
	}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(ExtractSURF)

	virtual const std::string GetDescription() const {
        return "Checks that array elements lie between two scalars.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Feature/ExtractSURF";
    }

protected:

	virtual void _Initialize (const Arguments& parameters) {
		Base::_Initialize (parameters);
		_surf_params.extended = BoolValue(parameters[0]).GetData() == pI_FALSE ? 0 : 1;
		_surf_params.hessianThreshold = DoubleValue(parameters[1]).GetData();
		_surf_params.nOctaves = IntValue(parameters[2]).GetData();
		_surf_params.nOctaveLayers = IntValue(parameters[3]).GetData();
	}

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		CheckInputArguments (input_args, _input_args, 1);
		CheckOutputArguments (output_args, _output_args);
		
		boost::shared_ptr<CvArr> img(ArgumentAsCVArr(input_args[0]));
		boost::shared_ptr<CvArr> mask;
		if (input_args.size() > 1) {
			CheckInputArgument (input_args[1], _input_args[1]);
			mask = ArgumentAsCVArr(input_args[1]);
		}
		CvSeq* keypoints = 0;
		cvExtractSURF (img.get(), mask.get(), &keypoints, 0, _storage, _surf_params);

		pI_int num(keypoints ? keypoints->total : 0);
		DoubleTable retval(_runtime);
		retval.SetName ("features").SetDescription ("detected SURF features");
		retval.SetRows (num);
		retval.SetCols (2 + 4);
		retval.CreateData();
		retval.SetHeader (0, "x");
		retval.SetHeader (1, "y");
		retval.SetHeader (2, "laplacian");
		retval.SetHeader (3, "size");
		retval.SetHeader (4, "dir");
		retval.SetHeader (5, "hessian");
		for (pI_int i = 0; i < num; ++i) {
			CvSURFPoint kpt = *(CvSURFPoint*) cvGetSeqElem (keypoints, i);
			retval.SetData (i, 0, static_cast<pI_double> (kpt.pt.x));
			retval.SetData (i, 1, static_cast<pI_double> (kpt.pt.y));
			retval.SetData (i, 2, static_cast<pI_double> (kpt.laplacian));
			retval.SetData (i, 3, static_cast<pI_double> (kpt.size));
			retval.SetData (i, 4, static_cast<pI_double> (kpt.dir));
			retval.SetData (i, 5, static_cast<pI_double> (kpt.hessian));
		}
		cvClearSeq (keypoints);
		output_args[0] = retval;
	}

protected:

	CvMemStorage* _storage;
	CvSURFParams _surf_params;
}; // class ExtractSURF: public pIn

} // namespace opencv
} // namespace pIns
} // namespace pI

#endif // CV_FEATURE_HPP
