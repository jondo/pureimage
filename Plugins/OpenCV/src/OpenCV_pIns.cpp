/* OpenCV_PINs.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <pInSharedLibraryExport.hpp>

#include "CVCore.hpp"
#include "CVDraw.hpp"
#include "CVIO.hpp"
#include "CVFeature.hpp"
#include "CVSegment.hpp"
#include "CVMLL.hpp"
#include "Extensions/CVContourFeatures.hpp"


//                available
extern "C" pI_API pI_bool CreatepIns (pI_int index, struct _CRuntime* runtime, struct _CpIn** plugin) {

	if ((runtime == 0) || (index < 0))
        return pI_FALSE;

	// create a plugin if it has not been created already
    switch (index) {
		// core plugins
		EXPORT_PIN_SC (0, runtime, pI::pIns::opencv::AbsDiff);
		EXPORT_PIN_SC (1, runtime, pI::pIns::opencv::AbsDiffS);
		EXPORT_PIN_SC (2, runtime, pI::pIns::opencv::Add);
		EXPORT_PIN_SC (3, runtime, pI::pIns::opencv::AddS);
		EXPORT_PIN_SC (4, runtime, pI::pIns::opencv::AddWeighted);
		EXPORT_PIN_SC (5, runtime, pI::pIns::opencv::And);
		EXPORT_PIN_SC (6, runtime, pI::pIns::opencv::AndS);
		EXPORT_PIN_SC (7, runtime, pI::pIns::opencv::Avg);
		EXPORT_PIN_SC (8, runtime, pI::pIns::opencv::AvgSdv);
		EXPORT_PIN_SC (9, runtime, pI::pIns::opencv::CalcCovarMatrix);
		EXPORT_PIN_SC (10, runtime, pI::pIns::opencv::CartToPolar);
		EXPORT_PIN_SC (11, runtime, pI::pIns::opencv::Cbrt);
		EXPORT_PIN_SC (12, runtime, pI::pIns::opencv::Cmp);
		EXPORT_PIN_SC (13, runtime, pI::pIns::opencv::CmpS);
		EXPORT_PIN_SC (14, runtime, pI::pIns::opencv::ConvertScale);
		EXPORT_PIN_SC (15, runtime, pI::pIns::opencv::CountNonZero);
		EXPORT_PIN_SC (16, runtime, pI::pIns::opencv::CrossProduct);
		EXPORT_PIN_SC (17, runtime, pI::pIns::opencv::DCT);
		EXPORT_PIN_SC (18, runtime, pI::pIns::opencv::DFT);
		EXPORT_PIN_SC (19, runtime, pI::pIns::opencv::Det);
		EXPORT_PIN_SC (20, runtime, pI::pIns::opencv::Div);
		EXPORT_PIN_SC (21, runtime, pI::pIns::opencv::DotProduct);
		EXPORT_PIN_SC (22, runtime, pI::pIns::opencv::EigenVV); // not yet implemented!
		EXPORT_PIN_SC (23, runtime, pI::pIns::opencv::Exp);
		EXPORT_PIN_SC (24, runtime, pI::pIns::opencv::FastArctan);
		EXPORT_PIN_SC (25, runtime, pI::pIns::opencv::Flip);
		EXPORT_PIN_SC (26, runtime, pI::pIns::opencv::GEMM);
		EXPORT_PIN_SC (27, runtime, pI::pIns::opencv::GetOptimalDFTSize);
		EXPORT_PIN_SC (28, runtime, pI::pIns::opencv::InRange);
		EXPORT_PIN_SC (29, runtime, pI::pIns::opencv::InRangeS);
		EXPORT_PIN_SC (30, runtime, pI::pIns::opencv::InvSqrt);
		EXPORT_PIN_SC (31, runtime, pI::pIns::opencv::Inv);
		EXPORT_PIN_SC (32, runtime, pI::pIns::opencv::IsInf);
		EXPORT_PIN_SC (33, runtime, pI::pIns::opencv::IsNaN);
		EXPORT_PIN_SC (34, runtime, pI::pIns::opencv::LUT);
		EXPORT_PIN_SC (35, runtime, pI::pIns::opencv::Log);
		EXPORT_PIN_SC (36, runtime, pI::pIns::opencv::Mahalonobis);
		EXPORT_PIN_SC (37, runtime, pI::pIns::opencv::Max);
		EXPORT_PIN_SC (38, runtime, pI::pIns::opencv::MaxS);
		EXPORT_PIN_SC (39, runtime, pI::pIns::opencv::Min);
		EXPORT_PIN_SC (40, runtime, pI::pIns::opencv::MinMax);
		EXPORT_PIN_SC (41, runtime, pI::pIns::opencv::MinS);
		EXPORT_PIN_SC (42, runtime, pI::pIns::opencv::Mul);
		EXPORT_PIN_SC (43, runtime, pI::pIns::opencv::MulSpectrums);
		EXPORT_PIN_SC (44, runtime, pI::pIns::opencv::MulTransposed);
		EXPORT_PIN_SC (45, runtime, pI::pIns::opencv::Norm);
		EXPORT_PIN_SC (46, runtime, pI::pIns::opencv::Not);
		EXPORT_PIN_SC (47, runtime, pI::pIns::opencv::Or);
		EXPORT_PIN_SC (48, runtime, pI::pIns::opencv::OrS);
		EXPORT_PIN_SC (49, runtime, pI::pIns::opencv::PerspectiveTransform);
		EXPORT_PIN_SC (50, runtime, pI::pIns::opencv::PolarToCart);
		EXPORT_PIN_SC (51, runtime, pI::pIns::opencv::Pow);
		EXPORT_PIN_SC (52, runtime, pI::pIns::opencv::RandArr);
		EXPORT_PIN_SC (53, runtime, pI::pIns::opencv::RandInt);
		EXPORT_PIN_SC (54, runtime, pI::pIns::opencv::RandReal);
		EXPORT_PIN_SC (55, runtime, pI::pIns::opencv::Reduce);
		EXPORT_PIN_SC (56, runtime, pI::pIns::opencv::Repeat);
		EXPORT_PIN_SC (57, runtime, pI::pIns::opencv::Round);
		EXPORT_PIN_SC (58, runtime, pI::pIns::opencv::Floor);
		EXPORT_PIN_SC (59, runtime, pI::pIns::opencv::Ceil);
		EXPORT_PIN_SC (60, runtime, pI::pIns::opencv::ScaleAdd);
		EXPORT_PIN_SC (61, runtime, pI::pIns::opencv::SetIdentity);
		EXPORT_PIN_SC (62, runtime, pI::pIns::opencv::Solve);
		EXPORT_PIN_SC (63, runtime, pI::pIns::opencv::SolveCubic);
		EXPORT_PIN_SC (64, runtime, pI::pIns::opencv::Sqrt);
		EXPORT_PIN_SC (65, runtime, pI::pIns::opencv::Sub);
		EXPORT_PIN_SC (66, runtime, pI::pIns::opencv::SubRS);
		EXPORT_PIN_SC (67, runtime, pI::pIns::opencv::Sub);
		EXPORT_PIN_SC (68, runtime, pI::pIns::opencv::Sum);
		EXPORT_PIN_SC (69, runtime, pI::pIns::opencv::Trace);
		EXPORT_PIN_SC (70, runtime, pI::pIns::opencv::Transpose);
		EXPORT_PIN_SC (71, runtime, pI::pIns::opencv::Xor);
		EXPORT_PIN_SC (72, runtime, pI::pIns::opencv::XorS);
		/* Note: not (yet) wrapped are the following functions:
			Merge
			Split
			MixChannels
			Reduce
			SBBkSb
			SVD
			Transform
		*/

		// draw plugins
		EXPORT_PIN_SC (73, runtime, pI::pIns::opencv::Line);
		EXPORT_PIN_SC (74, runtime, pI::pIns::opencv::PolyLine);
		EXPORT_PIN_SC (75, runtime, pI::pIns::opencv::Rectangle);
		EXPORT_PIN_SC (76, runtime, pI::pIns::opencv::PutText);
		EXPORT_PIN_SC (77, runtime, pI::pIns::opencv::Circle);
		EXPORT_PIN_SC (78, runtime, pI::pIns::opencv::ClipLine);
		EXPORT_PIN_SC (79, runtime, pI::pIns::opencv::Ellipse);
		EXPORT_PIN_SC (80, runtime, pI::pIns::opencv::EllipseBox);
		EXPORT_PIN_SC (81, runtime, pI::pIns::opencv::FillPoly);
		EXPORT_PIN_SC (82, runtime, pI::pIns::opencv::FillConvexPoly);
		/* Note: not (yet) wrapped are the following functions:
			DrawContours
		*/

		// IO plugins
		EXPORT_PIN_SC (83, runtime, pI::pIns::opencv::LoadImage);
		EXPORT_PIN_SC (84, runtime, pI::pIns::opencv::SaveImage);
		EXPORT_PIN_SC (85, runtime, pI::pIns::opencv::CaptureFromCAM);
		EXPORT_PIN_SC (86, runtime, pI::pIns::opencv::CaptureFromFile);
		EXPORT_PIN_SC (87, runtime, pI::pIns::opencv::VideoWriter);
		EXPORT_PIN_SC (88, runtime, pI::pIns::opencv::ShowImage);

		// Feature plugins
		EXPORT_PIN_SC (89, runtime, pI::pIns::opencv::ExtractSURF);

		// Segmentation plugins
		EXPORT_PIN_SC (90, runtime, pI::pIns::opencv::PyrSegmentation);
		EXPORT_PIN_SC (91, runtime, pI::pIns::opencv::Threshold);
		//EXPORT_PIN_SC (97, runtime, pI::pIns::opencv::AdaptiveThreshold);

		// Machine learning plugins
		EXPORT_PIN_SC (92, runtime, pI::pIns::opencv::KNearest);
		EXPORT_PIN_SC (93, runtime, pI::pIns::opencv::NormalBayes);
		EXPORT_PIN_SC (94, runtime, pI::pIns::opencv::EM);
		EXPORT_PIN_SC (95, runtime, pI::pIns::opencv::KMeans2);

        EXPORT_PIN_SC (96, runtime, pI::pIns::opencv::CVContourFeatures);

        default:
            return pI_FALSE;
    }
}; /* extern "C" pI_API pI_bool CreatepIns (pI_int index, struct _CRuntime* host, struct _CpIn** plugin) */
