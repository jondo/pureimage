/* CVBase.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include "CVBase.hpp"

namespace pI {
namespace pIns {
namespace opencv {


CVBase::CVBase(Runtime& runtime): Base(runtime) {

	// create plugin parameters here...
}

/*virtual*/ CVBase::~CVBase() {

	// clean up private memory
}

/*virtual*/ void CVBase::_Initialize (const Arguments& parameters) {

	CheckParameters (parameters, _parameters);
	/*for (pI_size p = 0; p < _parameters.size(); ++p) {
		if (_parameters[p]->signature.type != T_pI_Argument_Empty) {
			CheckParameter (parameters[p], _parameters[p]);
		} else {
			if (CheckCvArrArgument (parameters[p]) != pI_TRUE)
				throw exception::ParameterException("Incompatible parameter.");
		}
	}*/
}

/*virtual*/ void CVBase::_Execute (Arguments& input_args, Arguments& output_args) {

	// validate arguments
	CheckInputArguments (input_args, _input_args);
	CheckOutputArguments (output_args, _output_args);
	/*
	for (pI_size i = 0; i < _input_args.size(); ++i) {
		if (_input_args[i]->signature.type != T_pI_Argument_Empty) {
			CheckInputArgument (input_args[i], _input_args[i]);
		} else {
			if (CheckCvArrArgument (input_args[i]) != pI_TRUE)
				throw exception::ParameterException("Incompatible input argument.");
		}
	}

	for (pI_size o = 0; o < _output_args.size(); ++o) {
		if (_output_args[o]->signature.type != T_pI_Argument_Empty) {
			CheckInputArgument (output_args[o], _output_args[o]);
		} else {
			if (CheckCvArrArgument (output_args[o]) != pI_TRUE)
				throw exception::ParameterException("Incompatible output argument.");
		}
	}
	*/
}

boost::shared_ptr<CvMat> CVBase::ArgumentAsCVMat (ArgumentPtr arg) const {

	boost::shared_ptr<CvMat> retval;

	switch (arg->signature.type) {
		case T_pI_Argument_DoubleMatrix: {
			DoubleMatrix dm(arg);
			retval = boost::shared_ptr<CvMat>(cvCreateMatHeader (dm.GetRows(), dm.GetCols(), CV_64FC1), CvArrDelete());
			cvSetData (retval.get(), &(dm.GetCPtr()->data.DoubleMatrix->data[0][0]), dm.GetCols() * sizeof(pI_double));
			break;
		}
		case T_pI_Argument_IntMatrix: {
			IntMatrix im(arg);
			retval = boost::shared_ptr<CvMat>(cvCreateMatHeader (im.GetRows(), im.GetCols(), CV_32SC1), CvArrDelete());
			cvSetData (retval.get(), &(im.GetCPtr()->data.IntMatrix->data[0][0]), im.GetCols() * sizeof(pI_int));
			break;
		}
		case T_pI_Argument_ShortMatrix: {
			ShortMatrix im(arg);
			retval = boost::shared_ptr<CvMat>(cvCreateMatHeader (im.GetRows(), im.GetCols(), CV_16SC1), CvArrDelete());
			cvSetData (retval.get(), &(im.GetCPtr()->data.ShortMatrix->data[0][0]), im.GetCols() * sizeof(pI_short));
			break;
		}
		case T_pI_Argument_ByteMatrix: {
			ByteMatrix bm(arg);
			retval = boost::shared_ptr<CvMat>(cvCreateMatHeader (bm.GetRows(), bm.GetCols(), CV_8UC1), CvArrDelete());
			cvSetData (retval.get(), &(bm.GetCPtr()->data.ByteMatrix->data[0][0]), bm.GetCols() * sizeof(pI_byte));
			break;
		}
		case T_pI_Argument_DoubleTable: {
			DoubleTable dt(arg);
			retval = boost::shared_ptr<CvMat>(cvCreateMatHeader (dt.GetRows(), dt.GetCols(), CV_64FC1), CvArrDelete());
			cvSetData (retval.get(), &(dt.GetCPtr()->data.DoubleTable->data[0][0]), dt.GetCols() * sizeof(pI_double));
			break;
		}
		default:
			throw exception::ArgumentException ("Cannot convert argument to CvMat - incompatible type");
	} // switch (arg->signature.type)
	return retval;
}

boost::shared_ptr<CvArr> CVBase::ArgumentAsCVArr (ArgumentPtr arg) const {

	boost::shared_ptr<CvArr> retval;

	switch (arg->signature.type) {
		case T_pI_Argument_DoubleArray: {
			DoubleArray da(arg);
			retval = boost::shared_ptr<CvArr>(cvCreateMatHeader (1, da.GetCount(), CV_64FC1), CvArrDelete());
			cvSetData (retval.get(), &(da.GetCPtr()->data.DoubleArray[0]), da.GetCount() * sizeof(pI_double));
			break;
		}
		case T_pI_Argument_IntArray: {
			IntArray da(arg);
			retval = boost::shared_ptr<CvArr>(cvCreateMatHeader (1, da.GetCount(), CV_32SC1), CvArrDelete());
			cvSetData (retval.get(), &(da.GetCPtr()->data.IntArray[0]), da.GetCount() * sizeof(pI_int));
			break;
		}
        case T_pI_Argument_ShortArray: {
			ShortArray da(arg);
			retval = boost::shared_ptr<CvArr>(cvCreateMatHeader (1, da.GetCount(), CV_16SC1), CvArrDelete());
			cvSetData (retval.get(), &(da.GetCPtr()->data.ShortArray[0]), da.GetCount() * sizeof(pI_short));
			break;
		}
		case T_pI_Argument_ByteArray: {
			ByteArray ba(arg);
			retval = boost::shared_ptr<CvArr>(cvCreateMatHeader (1, ba.GetCount(), CV_8UC1), CvArrDelete());
			cvSetData (retval.get(), &(ba.GetCPtr()->data.ByteArray[0]), ba.GetCount() * sizeof(pI_byte));
			break;
		}

		case T_pI_Argument_DoubleMatrix: {
			DoubleMatrix dm(arg);
			retval = boost::shared_ptr<CvArr>(cvCreateMatHeader (dm.GetRows(), dm.GetCols(), CV_64FC1), CvArrDelete());
			cvSetData (retval.get(), &(dm.GetCPtr()->data.DoubleMatrix->data[0][0]), dm.GetCols() * sizeof(pI_double));
			break;
		}
		case T_pI_Argument_IntMatrix: {
			IntMatrix im(arg);
			retval = boost::shared_ptr<CvArr>(cvCreateMatHeader (im.GetRows(), im.GetCols(), CV_32SC1), CvArrDelete());
			cvSetData (retval.get(), &(im.GetCPtr()->data.IntMatrix->data[0][0]), im.GetCols() * sizeof(pI_int));
			break;
		}
		case T_pI_Argument_ShortMatrix: {
			ShortMatrix im(arg);
			retval = boost::shared_ptr<CvArr>(cvCreateMatHeader (im.GetRows(), im.GetCols(), CV_16SC1), CvArrDelete());
			cvSetData (retval.get(), &(im.GetCPtr()->data.ShortMatrix->data[0][0]), im.GetCols() * sizeof(pI_short));
			break;
		}
		case T_pI_Argument_ByteMatrix: {
			ByteMatrix bm(arg);
			retval = boost::shared_ptr<CvArr>(cvCreateMatHeader (bm.GetRows(), bm.GetCols(), CV_8UC1), CvArrDelete());
			cvSetData (retval.get(), &(bm.GetCPtr()->data.ByteMatrix->data[0][0]), bm.GetCols() * sizeof(pI_byte));
			break;
		}

		case T_pI_Argument_DoubleTable: {
			DoubleTable dt(arg);
			retval = boost::shared_ptr<CvArr>(cvCreateMatHeader (dt.GetRows(), dt.GetCols(), CV_64FC1), CvArrDelete());
			cvSetData (retval.get(), &(dt.GetCPtr()->data.DoubleTable->data[0][0]), dt.GetCols() * sizeof(pI_double));
			break;
		}

		case T_pI_Argument_ByteImage: {
			ByteImage bi(arg);
			switch (bi.GetChannels()) {
				case 1:
					retval = boost::shared_ptr<CvArr>(cvCreateMatHeader (bi.GetHeight(), bi.GetWidth(), CV_8UC1), CvArrDelete());
					break;
				case 2:
					retval = boost::shared_ptr<CvArr>(cvCreateMatHeader (bi.GetHeight(), bi.GetWidth(), CV_8UC2), CvArrDelete());
					break;
				case 3:
					retval = boost::shared_ptr<CvArr>(cvCreateMatHeader (bi.GetHeight(), bi.GetWidth(), CV_8UC3), CvArrDelete());
					break;
				case 4:
					retval = boost::shared_ptr<CvArr>(cvCreateMatHeader (bi.GetHeight(), bi.GetWidth(), CV_8UC4), CvArrDelete());
					break;
				default:
					throw exception::ArgumentException ("OpenCV pIn cannot cope with images with more than 4 channels");
			}
			cvSetData (retval.get(), &(bi.GetCPtr()->data.ByteImage->data[0][0][0]), bi.GetPitch());
			break;
		}
		default:
			throw exception::ArgumentException ("Cannot convert argument to CvArr - incompatible type");
	} // switch (arg->signature.type)
	return retval;
} // boost::shared_ptr<CvArr> CVBase::ArgumentAsCVArr (ArgumentPtr arg) const


const pI_int CVBase::CheckCvArrSizes (ArgumentPtr arg1, ArgumentPtr arg2) const {

	switch (arg1->signature.type) {
		case T_pI_Argument_DoubleArray: {
			DoubleArray da1(arg1), da2(arg2);
			return da1.GetCount() == da2.GetCount();
		}
		case T_pI_Argument_IntArray: {
			IntArray ia1(arg1), ia2(arg2);
			return ia1.GetCount() == ia2.GetCount();
		}
        case T_pI_Argument_ShortArray: {
			ShortArray ia1(arg1), ia2(arg2);
			return ia1.GetCount() == ia2.GetCount();
		}
		case T_pI_Argument_ByteArray: {
			ByteArray ba1(arg1), ba2(arg2);
			return ba1.GetCount() == ba2.GetCount();
		}

		case T_pI_Argument_DoubleMatrix: {
			DoubleMatrix dm1(arg1), dm2(arg2);
			return (dm1.GetRows() == dm2.GetRows()) && (dm1.GetCols() == dm2.GetCols());
		}
		case T_pI_Argument_IntMatrix: {
			IntMatrix im1(arg1), im2(arg2);
			return (im1.GetRows() == im2.GetRows()) && (im1.GetCols() == im2.GetCols());
		}
        case T_pI_Argument_ShortMatrix: {
			ShortMatrix im1(arg1), im2(arg2);
			return (im1.GetRows() == im2.GetRows()) && (im1.GetCols() == im2.GetCols());
		}
		case T_pI_Argument_ByteMatrix: {
			ByteMatrix bm1(arg1), bm2(arg2);
			return (bm1.GetRows() == bm2.GetRows()) && (bm1.GetCols() == bm2.GetCols());
		}

		case T_pI_Argument_DoubleTable: {
			DoubleTable dt1(arg1), dt2(arg2);
			return (dt1.GetRows() == dt2.GetRows()) && (dt1.GetCols() == dt2.GetCols());
		}

		case T_pI_Argument_ByteImage: {
			ByteImage bi1(arg1), bi2(arg2);
			return (bi1.GetChannels() == bi2.GetChannels()) &&
				   (bi1.GetHeight() == bi2.GetHeight()) &&
				   (bi1.GetWidth() == bi2.GetWidth()) &&
				   (bi1.GetPitch() == bi2.GetPitch());
		}
		default:
			throw exception::ArgumentException ("Cannot convert argument to CvArr - incompatible type");
	} // switch (arg->signature.type)
} // const pI_int CVBase::CheckCvArrSizes (ArgumentPtr arg1, ArgumentPtr arg2) const

const pI_bool CVBase::AdaptCvArrOutputSize (ArgumentPtr in, ArgumentPtr out, pI_bool invert /*= pI_FALSE*/) const {

	switch (in->signature.type) {
		case T_pI_Argument_DoubleArray: {
			DoubleArray da1(in), da2(out);
			if (da2.HasData()) {
				if (da2.GetCount() == da1.GetCount())
					return pI_TRUE;
				_runtime.GetCRuntime()->FreeArgumentData (
				 _runtime.GetCRuntime(), da2.GetCPtr());
			}
			da2.SetCount (da1.GetCount());
			da2.CreateData();
			return pI_TRUE;
		}
		case T_pI_Argument_IntArray: {
			IntArray ia1(in), ia2(out);
			if (ia2.HasData()) {
				if (ia2.GetCount() == ia1.GetCount())
					return pI_TRUE;
				_runtime.GetCRuntime()->FreeArgumentData (
				 _runtime.GetCRuntime(), ia2.GetCPtr());
			}
			ia2.SetCount (ia1.GetCount());
			ia2.CreateData();
			return pI_TRUE;
		}
        case T_pI_Argument_ShortArray: {
			ShortArray ia1(in), ia2(out);
			if (ia2.HasData()) {
				if (ia2.GetCount() == ia1.GetCount())
					return pI_TRUE;
				_runtime.GetCRuntime()->FreeArgumentData (
				 _runtime.GetCRuntime(), ia2.GetCPtr());
			}
			ia2.SetCount (ia1.GetCount());
			ia2.CreateData();
			return pI_TRUE;
		}
		case T_pI_Argument_ByteArray: {
			ByteArray ba1(in), ba2(out);
			if (ba2.HasData()) {
				if (ba2.GetCount() == ba1.GetCount())
					return pI_TRUE;
				_runtime.GetCRuntime()->FreeArgumentData (
				 _runtime.GetCRuntime(), ba2.GetCPtr());
			}
			ba2.SetCount (ba1.GetCount());
			ba2.CreateData();
			return pI_TRUE;
		}

		case T_pI_Argument_DoubleMatrix: {
			DoubleMatrix dm1(in), dm2(out);
			if (dm2.HasData()) {
				if (invert == pI_FALSE) {
					if ((dm2.GetRows() == dm1.GetRows()) &&
						(dm2.GetCols() == dm1.GetCols()))
						return pI_TRUE;
				} else {
					if ((dm2.GetRows() == dm1.GetCols()) &&
						(dm2.GetCols() == dm1.GetRows()))
						return pI_TRUE;
				}
				_runtime.GetCRuntime()->FreeArgumentData (
				 _runtime.GetCRuntime(), dm2.GetCPtr());
			}
			if (invert == pI_FALSE) {
				dm2.SetCols (dm1.GetCols());
				dm2.SetRows (dm1.GetRows());
			} else {
				dm2.SetCols (dm1.GetRows());
				dm2.SetRows (dm1.GetCols());
			}
			dm2.CreateData();
			return pI_TRUE;
		}
		case T_pI_Argument_IntMatrix: {
			IntMatrix im1(in), im2(out);
			if (im2.HasData()) {
				if (invert == pI_FALSE) {
					if ((im2.GetRows() == im1.GetRows()) &&
						(im2.GetCols() == im1.GetCols()))
						return pI_TRUE;
				} else {
					if ((im2.GetRows() == im1.GetCols()) &&
						(im2.GetCols() == im1.GetRows()))
						return pI_TRUE;
				}
				_runtime.GetCRuntime()->FreeArgumentData (
				 _runtime.GetCRuntime(), im2.GetCPtr());
			}
			if (invert == pI_FALSE) {
				im2.SetCols (im1.GetCols());
				im2.SetRows (im1.GetRows());
			} else {
				im2.SetCols (im1.GetRows());
				im2.SetRows (im1.GetCols());
			}
			im2.CreateData();
			return pI_TRUE;
		}
		case T_pI_Argument_ShortMatrix: {
			ShortMatrix im1(in), im2(out);
			if (im2.HasData()) {
				if (invert == pI_FALSE) {
					if ((im2.GetRows() == im1.GetRows()) &&
						(im2.GetCols() == im1.GetCols()))
						return pI_TRUE;
				} else {
					if ((im2.GetRows() == im1.GetCols()) &&
						(im2.GetCols() == im1.GetRows()))
						return pI_TRUE;
				}
				_runtime.GetCRuntime()->FreeArgumentData (
				 _runtime.GetCRuntime(), im2.GetCPtr());
			}
			if (invert == pI_FALSE) {
				im2.SetCols (im1.GetCols());
				im2.SetRows (im1.GetRows());
			} else {
				im2.SetCols (im1.GetRows());
				im2.SetRows (im1.GetCols());
			}
			im2.CreateData();
			return pI_TRUE;
		}
		case T_pI_Argument_ByteMatrix: {
			ByteMatrix bm1(in), bm2(out);
			if (bm2.HasData()) {
				if (invert == pI_FALSE) {
					if ((bm2.GetRows() == bm1.GetRows()) &&
						(bm2.GetCols() == bm1.GetCols()))
						return pI_TRUE;
				} else {
					if ((bm2.GetRows() == bm1.GetCols()) &&
						(bm2.GetCols() == bm1.GetRows()))
						return pI_TRUE;
				}
				_runtime.GetCRuntime()->FreeArgumentData (
				 _runtime.GetCRuntime(), bm2.GetCPtr());
			}
			if (invert == pI_FALSE) {
				bm2.SetCols (bm1.GetCols());
				bm2.SetRows (bm1.GetRows());
			} else {
				bm2.SetCols (bm1.GetRows());
				bm2.SetRows (bm1.GetCols());
			}
			bm2.CreateData();
			return pI_TRUE;
		}

		case T_pI_Argument_DoubleTable: {
			DoubleTable dt1(in), dt2(out);
			if (dt2.HasData()) {
				if (invert == pI_FALSE) {
					if ((dt2.GetRows() == dt1.GetRows()) &&
						(dt2.GetCols() == dt1.GetCols()))
						return pI_TRUE;
				} else {
					if ((dt2.GetRows() == dt1.GetCols()) &&
						(dt2.GetCols() == dt1.GetRows()))
						return pI_TRUE;
				}
				_runtime.GetCRuntime()->FreeArgumentData (
				 _runtime.GetCRuntime(), dt2.GetCPtr());
			}
			if (invert == pI_FALSE) {
				dt2.SetCols (dt1.GetCols());
				dt2.SetRows (dt1.GetRows());
			} else {
				dt2.SetCols (dt1.GetRows());
				dt2.SetRows (dt1.GetCols());
			}
			dt2.CreateData();
			return pI_TRUE;
		}

		case T_pI_Argument_ByteImage: {
			ByteImage bi1(in), bi2(out);
			if (bi2.HasData()) {
				if (invert == pI_FALSE) {
					if ((bi2.GetChannels() == bi1.GetChannels()) &&
						(bi2.GetHeight() == bi1.GetHeight()) &&
						(bi2.GetWidth() == bi1.GetWidth()))
						return pI_TRUE;
				} else {
					if ((bi2.GetChannels() == bi1.GetChannels()) &&
						(bi2.GetHeight() == bi1.GetWidth()) &&
						(bi2.GetWidth() == bi1.GetHeight()))
						return pI_TRUE;
				}
				_runtime.GetCRuntime()->FreeArgumentData (
				 _runtime.GetCRuntime(), bi2.GetCPtr());
			}
			bi2.SetChannels (bi1.GetChannels());
			if (invert == pI_FALSE) {
				bi2.SetHeight (bi1.GetHeight());
				bi2.SetWidth (bi1.GetWidth());
			} else {
				bi2.SetHeight (bi1.GetWidth());
				bi2.SetWidth (bi1.GetHeight());
			}
			bi2.CreateData();
			// Note: currently, this is done in ByteImage::CreateData()
			// bi2.SetPitch ((((8 * bi2.GetWidth()) + 31) / 32) * 4);
			return pI_TRUE;
		}
		default:
			throw exception::ArgumentException ("Cannot convert argument to CvArr - incompatible type");
	} // switch (arg->signature.type)
} // const pI_bool CVBase::AdaptCvArrOutputSize (ArgumentPtr in, ArgumentPtr out) const

ArgumentPtr CVBase::ByteImageFromIplImage (const IplImage* img) const {

	ByteImage retval(_runtime);
	retval.SetName ("img");
	retval.SetDescription ("Converted image (from OpenCV)");
	retval.SetWidth (img->width);
	retval.SetHeight (img->height);
	retval.SetChannels (img->nChannels);
	try {
		retval.CreateData();
	} catch (exception::MemoryException) {
		throw exception::ExecutionException("Failed to create image data");
	}
	/* although a smarter implementation would be possible, for now copy the data */
	for (pI_int r = 0; r < img->height; ++r) {
		memcpy (&(retval.GetCPtr()->data.ByteImage->data[r][0][0]), &(img->imageData[r * img->widthStep]), img->widthStep);
	}
	retval.SetPitch (img->widthStep);
	return retval;
} // ArgumentPtr CVBase::ByteImageFromIplImage (const IplImage* img) const

void CVBase::CopyIplToByteImage (IplImage* ipli, ArgumentPtr bi) const {

	if ((ipli == 0) || (ipli->imageData == 0))
		throw exception::ExecutionException("Cannot copy invalid ipl image");
	pI::ByteImage dst(bi);
	pI_bool reallocate = pI_FALSE;
	if ((dst.HasData() == pI_FALSE) ||
		(dst.GetChannels() != ipli->nChannels) ||
		(dst.GetWidth() != ipli->width) ||
		(dst.GetHeight() != ipli->height))
		reallocate = pI_TRUE;
	if (reallocate == pI_TRUE) {
		ArgumentPtr retval = ByteImageFromIplImage (ipli);
		// swap pointers
		_runtime.GetCRuntime()->SwapArguments (
		 _runtime.GetCRuntime(),
		 bi.get(),
		 retval.get());
	} else {
		/* although a smarter implementation would be possible, for now copy the data */
		for (pI_int r = 0; r < ipli->height; ++r) {
			memcpy (&(bi->data.ByteImage->data[r][0][0]), &(ipli->imageData[r * ipli->widthStep]), ipli->widthStep);
		}
	}
} // void CopyIplToByteImage CVBase::(IplImage* ipli, ArgumentPtr bi) const

boost::shared_ptr<IplImage> CVBase::IplImageFromByteImage (ArgumentPtr ptr) const {

	boost::shared_ptr<IplImage> retval;
	ByteImage src(ptr);
	CvSize size;
	size.width = src.GetWidth();
	size.height = src.GetHeight();
	retval = boost::shared_ptr<IplImage>(cvCreateImageHeader (size, IPL_DEPTH_8U, src.GetChannels()), CvIplHeaderDelete());
	cvSetData (retval.get(), &(src.GetCPtr()->data.ByteImage->data[0][0][0]), src.GetPitch());
	return retval;
} // boost::shared_ptr<IplImage> CVBase::IplImageFromByteImage (ArgumentPtr ptr) const

void CVBase::CvMatParameter (
 const std::string name,
 const std::string description) {

	VariableArgument arg(_runtime);
	arg.SetName((pI_str) name.c_str())
	   .SetDescription((pI_str) (description + std::string("; accepted types are: DoubleMatrix, IntMatrix, ShortMatrix, ByteMatrix, DoubleTable")).c_str());
	arg.SetCount (T_pI_Argument_VariableArgument);
	arg.CreateData();
	arg.SetTypes (T_pI_Argument_DoubleMatrix, pI_TRUE);
	arg.SetTypes (T_pI_Argument_IntMatrix, pI_TRUE);
    arg.SetTypes (T_pI_Argument_ShortMatrix, pI_TRUE);
	arg.SetTypes (T_pI_Argument_ByteMatrix, pI_TRUE);
	arg.SetTypes (T_pI_Argument_DoubleTable, pI_TRUE);
	_parameters.push_back (arg);
} // void CVBase::CvMatParameter (...)

void CVBase::CvMatInput (
 const std::string name,
 const std::string description,
 const pI_bool readonly /*= pI_TRUE*/) {

	VariableArgument arg(_runtime);
	arg.SetName((pI_str) name.c_str())
	   .SetDescription((pI_str) (description + std::string("; accepted types are: DoubleMatrix, IntMatrix, ShortMatrix, ByteMatrix, DoubleTable")).c_str());
	arg.SetCount (T_pI_Argument_VariableArgument);
	arg.CreateData();
	arg.SetTypes (T_pI_Argument_DoubleMatrix, pI_TRUE);
	arg.SetTypes (T_pI_Argument_IntMatrix, pI_TRUE);
    arg.SetTypes (T_pI_Argument_ShortMatrix, pI_TRUE);
	arg.SetTypes (T_pI_Argument_ByteMatrix, pI_TRUE);
	arg.SetTypes (T_pI_Argument_DoubleTable, pI_TRUE);
	arg.SetReadOnly (readonly);
	_input_args.push_back (arg);
} // void CVBase::CvMatInput (...)

void CVBase::CvMatOutput (
 const std::string name,
 const std::string description) {

	VariableArgument arg(_runtime);
	arg.SetName((pI_str) name.c_str())
	   .SetDescription((pI_str) (description + std::string("; accepted types are: DoubleMatrix, IntMatrix, ShortMatrix, ByteMatrix, DoubleTable")).c_str());
	arg.SetCount (T_pI_Argument_VariableArgument);
	arg.CreateData();
	arg.SetTypes (T_pI_Argument_DoubleMatrix, pI_TRUE);
	arg.SetTypes (T_pI_Argument_IntMatrix, pI_TRUE);
    arg.SetTypes (T_pI_Argument_ShortMatrix, pI_TRUE);
	arg.SetTypes (T_pI_Argument_ByteMatrix, pI_TRUE);
	arg.SetTypes (T_pI_Argument_DoubleTable, pI_TRUE);
	_output_args.push_back (arg);
} // void CVBase::CvMatOutput (...)

void CVBase::CvArrParameter (
 const std::string name,
 const std::string description) {

	VariableArgument arg(_runtime);
	arg.SetName((pI_str) name.c_str())
	   .SetDescription((pI_str) (description + std::string("; accepted types are: DoubleArray, IntArray, ShortArray, ByteArray, DoubleMatrix, IntMatrix, ShortMatrix, ByteMatrix, DoubleTable, ByteImage")).c_str());
	arg.SetCount (T_pI_Argument_VariableArgument);
	arg.CreateData();
	arg.SetTypes (T_pI_Argument_DoubleArray, pI_TRUE);
	arg.SetTypes (T_pI_Argument_IntArray, pI_TRUE);
    arg.SetTypes (T_pI_Argument_ShortArray, pI_TRUE);
	arg.SetTypes (T_pI_Argument_ByteArray, pI_TRUE);
	arg.SetTypes (T_pI_Argument_DoubleMatrix, pI_TRUE);
	arg.SetTypes (T_pI_Argument_IntMatrix, pI_TRUE);
    arg.SetTypes (T_pI_Argument_ShortMatrix, pI_TRUE);
	arg.SetTypes (T_pI_Argument_ByteMatrix, pI_TRUE);
	arg.SetTypes (T_pI_Argument_DoubleTable, pI_TRUE);
	arg.SetTypes (T_pI_Argument_ByteImage, pI_TRUE);
	_parameters.push_back (arg);
} // void CVBase::CvArrParameter (...)

void CVBase::CvArrInput (
 const std::string name,
 const std::string description,
 const pI_bool readonly /*= pI_TRUE*/) {

	VariableArgument arg(_runtime);
	arg.SetName((pI_str) name.c_str())
	   .SetDescription((pI_str) (description + std::string("; accepted types are: DoubleArray, IntArray, ShortArray, ByteArray, DoubleMatrix, IntMatrix, ShortMatrix, ByteMatrix, DoubleTable, ByteImage")).c_str());
	arg.SetCount (T_pI_Argument_VariableArgument);
	arg.CreateData();
	arg.SetTypes (T_pI_Argument_DoubleArray, pI_TRUE);
	arg.SetTypes (T_pI_Argument_IntArray, pI_TRUE);
    arg.SetTypes (T_pI_Argument_ShortArray, pI_TRUE);
	arg.SetTypes (T_pI_Argument_ByteArray, pI_TRUE);
	arg.SetTypes (T_pI_Argument_DoubleMatrix, pI_TRUE);
	arg.SetTypes (T_pI_Argument_IntMatrix, pI_TRUE);
    arg.SetTypes (T_pI_Argument_ShortMatrix, pI_TRUE);
	arg.SetTypes (T_pI_Argument_ByteMatrix, pI_TRUE);
	arg.SetTypes (T_pI_Argument_DoubleTable, pI_TRUE);
	arg.SetTypes (T_pI_Argument_ByteImage, pI_TRUE);
	arg.SetReadOnly (readonly);
	_input_args.push_back (arg);
} // void CVBase::CvArrInput (...)

void CVBase::CvArrOutput (
 const std::string name,
 const std::string description) {

	VariableArgument arg(_runtime);
	arg.SetName((pI_str) name.c_str())
	   .SetDescription((pI_str) (description + std::string("; accepted types are: DoubleArray, IntArray, ShortArray, ByteArray, DoubleMatrix, IntMatrix, ShortMatrix, ByteMatrix, DoubleTable, ByteImage")).c_str());
	arg.SetCount (T_pI_Argument_VariableArgument);
	arg.CreateData();
	arg.SetTypes (T_pI_Argument_DoubleArray, pI_TRUE);
	arg.SetTypes (T_pI_Argument_IntArray, pI_TRUE);
    arg.SetTypes (T_pI_Argument_ShortArray, pI_TRUE);
	arg.SetTypes (T_pI_Argument_ByteArray, pI_TRUE);
	arg.SetTypes (T_pI_Argument_DoubleMatrix, pI_TRUE);
	arg.SetTypes (T_pI_Argument_IntMatrix, pI_TRUE);
    arg.SetTypes (T_pI_Argument_ShortMatrix, pI_TRUE);
	arg.SetTypes (T_pI_Argument_ByteMatrix, pI_TRUE);
	arg.SetTypes (T_pI_Argument_DoubleTable, pI_TRUE);
	arg.SetTypes (T_pI_Argument_ByteImage, pI_TRUE);
	_output_args.push_back (arg);
} // void CVBase::CvArrOutput (...)

void CVBase::DoubleValueParameter (
 const std::string name,
 const std::string description,
 const pI_double value /*= 0.0*/) {

	DoubleValue arg(_runtime);
    arg.SetName ((pI_str)name.c_str())
       .SetDescription ((pI_str)description.c_str())
       .SetData (value);
    _parameters.push_back (arg);
} // void CVBase::DoubleValueParameter (...)

void CVBase::DoubleValueInput (
 const std::string name,
 const std::string description,
 const pI_double value /*= 0.0*/,
 const pI_bool readonly /*= pI_TRUE*/) {

	DoubleValue arg(_runtime);
    arg.SetName ((pI_str)name.c_str())
       .SetDescription ((pI_str)description.c_str())
       .SetData (value)
	   .SetReadOnly (readonly);
    _input_args.push_back (arg);
} // void CVBase::DoubleValueInput (...)

void CVBase::DoubleValueOutput (
 const std::string name,
 const std::string description,
 const pI_double value /*= 0.0*/) {

	DoubleValue arg(_runtime);
    arg.SetName ((pI_str)name.c_str())
       .SetDescription ((pI_str)description.c_str())
       .SetData (value);
    _output_args.push_back (arg);
} // void CVBase::DoubleValueOutput (...)

CvScalar CVBase::DoubleValueAsCVScalar (ArgumentPtr dv) const {

	DoubleValue arg(dv);
	return cvScalarAll (arg.GetData());
} // CvScalar CVBase::DoubleValueAsCVScalar (ArgumentPtr arr) const

void CVBase::BoolValueParameter (
 const std::string name,
 const std::string description,
 const pI_bool value /*= pI_FALSE*/) {

	BoolValue arg(_runtime);
    arg.SetName ((pI_str)name.c_str())
       .SetDescription ((pI_str)description.c_str())
       .SetData (value);
    _parameters.push_back (arg);
} // void CVBase::BoolValueParameter (...)

void CVBase::BoolValueInput (
 const std::string name,
 const std::string description,
 const pI_bool value /*= pI_FALSE*/,
 const pI_bool readonly /*= pI_TRUE*/) {

	BoolValue arg(_runtime);
    arg.SetName ((pI_str)name.c_str())
       .SetDescription ((pI_str)description.c_str())
       .SetData (value)
	   .SetReadOnly (readonly);
    _input_args.push_back (arg);
} // void CVBase::BoolValueInput (...)

void CVBase::BoolValueOutput (
 const std::string name,
 const std::string description,
 const pI_bool value /*= pI_FALSE*/) {

	BoolValue arg(_runtime);
    arg.SetName ((pI_str)name.c_str())
       .SetDescription ((pI_str)description.c_str())
       .SetData (value);
    _output_args.push_back (arg);
} // void CVBase::BoolValueOutput (...)

void CVBase::IntValueParameter (
 const std::string name,
 const std::string description,
 const pI_int value /*= 0*/) {

	IntValue arg(_runtime);
    arg.SetName ((pI_str)name.c_str())
       .SetDescription ((pI_str)description.c_str())
       .SetData (value);
    _parameters.push_back (arg);
} // void CVBase::IntValueParameter (...)

void CVBase::IntValueInput (
 const std::string name,
 const std::string description,
 const pI_int value /*= 0*/,
 const pI_bool readonly /*= pI_TRUE*/) {

	IntValue arg(_runtime);
    arg.SetName ((pI_str)name.c_str())
       .SetDescription ((pI_str)description.c_str())
       .SetData (value)
	   .SetReadOnly (readonly);
    _input_args.push_back (arg);
} // void CVBase::IntValueInput (...)

void CVBase::IntValueOutput (
 const std::string name,
 const std::string description,
 const pI_int value /*= 0*/) {

	IntValue arg(_runtime);
    arg.SetName ((pI_str)name.c_str())
       .SetDescription ((pI_str)description.c_str())
       .SetData (value);
    _output_args.push_back (arg);
} // void CVBase::IntValueOutput (...)


} // namespace opencv
} // namespace pIns
} // namespace pI
