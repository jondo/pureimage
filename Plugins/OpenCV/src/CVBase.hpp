/* CVBase.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CV_BASE_HPP
#define CV_BASE_HPP

//#include <Poco/File.h>
//#include <Poco/Path.h>

#include <opencv/cv.h>

#include <PureImage.hpp>
#include <Arguments.hpp>
#include <ArgumentChecks.hpp>


namespace pI {
namespace pIns {
namespace opencv {

class CVBase: public pIn {

    typedef pIn Base;

public:

	CVBase(Runtime& runtime);
	virtual ~CVBase();

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(CVBase)

    virtual const pI_int GetpInVersion() const {
        return 10000;
    }

	virtual const std::string GetAuthor() const {
        return "JKU Linz";
    }

	virtual const std::string GetDescription() const {
        return "OpenCV plugin template.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/template";
    }

    virtual Arguments GetParameterSignature() const {
        return MEMBER_SIGNATURE(_parameters);
    }

    virtual Arguments GetInputSignature() const {
        return MEMBER_SIGNATURE(_input_args);
    }

    virtual Arguments GetOutputSignature() const {
        return MEMBER_SIGNATURE(_output_args);
    }

protected:

    virtual void _Initialize (const Arguments& parameters);

    virtual void _Execute (Arguments& input_args, Arguments& output_args);

	struct CvArrDelete {
		void operator() (CvMat* mat) const {
			cvReleaseMat (&mat);
		}
	}; // struct CvArrDelete

	struct CvIplHeaderDelete {
		void operator() (IplImage* mat) const {
			cvReleaseImageHeader (&mat);
		}
	}; // struct CvIplHeaderDelete

	boost::shared_ptr<CvMat> ArgumentAsCVMat (ArgumentPtr arg) const;

	boost::shared_ptr<CvArr> ArgumentAsCVArr (ArgumentPtr arg) const;

	//const pI_bool CheckCvArrArgument (ArgumentPtr arg) const;

	const pI_int CheckCvArrSizes (ArgumentPtr arg1, ArgumentPtr arg2) const;

	const pI_bool AdaptCvArrOutputSize (ArgumentPtr in, ArgumentPtr out, pI_bool invert = pI_FALSE) const;

	ArgumentPtr ByteImageFromIplImage (const IplImage* img) const;

	boost::shared_ptr<IplImage> IplImageFromByteImage (ArgumentPtr ptr) const;

	void CopyIplToByteImage (IplImage* ipli, ArgumentPtr bi) const;

	void CvMatParameter (
     const std::string name,
	 const std::string description);
	void CvMatInput (
     const std::string name,
	 const std::string description,
	 const pI_bool readonly = pI_TRUE);
	void CvMatOutput (
     const std::string name,
	 const std::string description);

	void CvArrParameter (
     const std::string name,
	 const std::string description);
	void CvArrInput (
     const std::string name,
	 const std::string description,
	 const pI_bool readonly = pI_TRUE);
	void CvArrOutput (
     const std::string name,
	 const std::string description);

	void DoubleValueParameter (
	 const std::string name,
	 const std::string description,
	 const pI_double value = 0.0);
	void DoubleValueInput (
	 const std::string name,
	 const std::string description,
	 const pI_double value = 0.0,
	 const pI_bool readonly = pI_TRUE);
	void DoubleValueOutput (
	 const std::string name,
	 const std::string description,
	 const pI_double value = 0.0);
	CvScalar DoubleValueAsCVScalar (ArgumentPtr dv) const;

	void BoolValueParameter (
	 const std::string name,
	 const std::string description,
	 const pI_bool value = pI_FALSE);
	void BoolValueInput (
	 const std::string name,
	 const std::string description,
	 const pI_bool value = pI_FALSE,
	 const pI_bool readonly = pI_TRUE);
	void BoolValueOutput (
	 const std::string name,
	 const std::string description,
	 const pI_bool value = pI_FALSE);

	void IntValueParameter (
	 const std::string name,
	 const std::string description,
	 const pI_int value = 0);
	void IntValueInput (
	 const std::string name,
	 const std::string description,
	 const pI_int value = 0,
	 const pI_bool readonly = pI_TRUE);
	void IntValueOutput (
	 const std::string name,
	 const std::string description,
	 const pI_int value = 0);

	//Poco::File CreateTempFile (pI_str file_name = 0) const;

	Arguments _parameters, _input_args, _output_args;

}; // class CVBase: public pIn

} // namespace opencv
} // namespace pIns
} // namespace pI

#endif // CV_BASE_HPP
