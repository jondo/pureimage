/* CVMLL.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CV_MLL_HPP
#define CV_MLL_HPP

#include "CVBase.hpp"
#include <opencv/ml.h>
#include <boost/bimap.hpp>

#ifdef WIN32
#include <windows.h>
#endif /* #ifdef WIN32 */

#include <fstream>

namespace pI {
namespace pIns {
namespace opencv {

class MLBase: public CVBase {

	typedef CVBase Base;

public:

	MLBase(Runtime& runtime, _pI_IntrinsicType label_type): Base(runtime), _label_type(label_type), _string_label_count(0) {}

	virtual ~MLBase() {}

protected:

	virtual void _Initialize (const Arguments& parameters) {
		_string_label_map.clear();
		_string_label_count = 0;
	}

	void DefaultMLParamterSignature() {
		_parameters.clear();
		StringSelection ssel(_runtime);
		ssel.SetName ("type")
			.SetDescription ("Specific type of the labels: double is for regression, int, string and bool for classification")
			.SetCount (4);
		ssel.CreateData();
		ssel.SetSymbols (0, "double");
		ssel.SetSymbols (1, "int");
		ssel.SetSymbols (2, "string");
		ssel.SetSymbols (3, "bool");
		switch (_label_type) {
			case _pI_double: ssel.SetIndex (0); break;
			case _pI_int: ssel.SetIndex (1); break;
			case _pI_str: ssel.SetIndex (2); break;
			case _pI_bool: ssel.SetIndex (3); break;
		}
		_parameters.push_back (ssel);
	} // void DefaultMLParamterSignature (_pI_IntrinsicType label_type)

	void DefaultMLArgumentSignature() {
		_input_args.clear();
		BoolValueInput ("predict", "if pI_TRUE, the given samples are used for prediction, otherwise they are used for training.", pI_FALSE, pI_TRUE);
		_input_args.push_back (DoubleTable(_runtime).SetName ("samples").SetDescription ("samples to use for prediction / training").SetReadOnly (pI_TRUE));
		switch (_label_type) {
			case _pI_double:
				_input_args.push_back (DoubleArray(_runtime).SetName ("responses").SetDescription ("regression responses"));
				break;
			case _pI_int:
				_input_args.push_back (IntArray(_runtime).SetName ("responses").SetDescription ("classification responses"));
				break;
			case _pI_str:
				_input_args.push_back (StringArray(_runtime).SetName ("responses").SetDescription ("classification responses"));
				break;
			case _pI_bool:
				_input_args.push_back (BoolArray(_runtime).SetName ("responses").SetDescription ("binary classification responses"));
				break;
		}
	} // void DefaultMLArgumentSignature (_pI_IntrinsicType label_type)

	pI_size GetLabelCount (ArgumentPtr arg) const {
		pI_size label_count(0);
		switch (_label_type) {
			case _pI_double: label_count = DoubleArray(arg).GetCount(); break;
			case _pI_int: label_count = IntArray(arg).GetCount(); break;
			case _pI_str: label_count = StringArray(arg).GetCount(); break;
			case _pI_bool: label_count = BoolArray(arg).GetCount(); break;
		}
		return label_count;
	} // pI_size GetLabelCount (ArgumentPtr arg) const

	void SetLabelType (const Arguments& parameters) {
		_label_type = _pI_double;
		if (parameters.size() > 0) {
			CheckParameter (parameters[0], _parameters[0]);
			switch (StringSelection(parameters[0]).GetIndex()) {
				case 1:
					_label_type = _pI_int; break;
				case 2:
					_label_type = _pI_str; break;
				case 3:
					_label_type = _pI_bool; break;
			}
		}
	} // void SetLabelType (const Arguments& parameters)

	boost::shared_ptr<CvMat> GetSamplesAsFloatCvMat (ArgumentPtr arg) {

		DoubleTable da(arg);
		boost::shared_ptr<CvMat> samples_cvmat(cvCreateMat (da.GetRows(), da.GetCols(), CV_32FC1), CvArrDelete());
		cv::Mat samples(samples_cvmat.get(), false);
		for (pI_size r = 0; r < da.GetRows(); ++r) {
			for (pI_size c = 0; c < da.GetCols(); ++c) {
				samples.at<float> (static_cast<int> (r), static_cast<int> (c)) = static_cast<float> (da.GetData (r, c));
			}
		}
		return samples_cvmat;
	} // boost::shared_ptr<CvMat> GetSamplesAsFloatCvMat (ArgumentPtr arg)

	boost::shared_ptr<CvMat> GetLabelsAsFloatCvMat (ArgumentPtr arg) {
		
		pI_size label_count(GetLabelCount (arg));
		boost::shared_ptr<CvMat> labels_cvmat(cvCreateMat (label_count, 1, CV_32FC1), CvArrDelete());
		cv::Mat labels(labels_cvmat.get(), false);
		
		switch (_label_type) {
			case _pI_double: {
				DoubleArray da(arg);
				for (pI_size lv = 0; lv < label_count; ++lv) {
					labels.at<float> (static_cast<int> (lv), 0) = static_cast<float> (da.GetData (lv));
				}
				break;
			}
			case _pI_int: {
				IntArray ia(arg);
				for (pI_size lv = 0; lv < label_count; ++lv) {
					labels.at<float> (static_cast<int> (lv), 0) = static_cast<float> (ia.GetData (lv));
				}
				break;
			}
			case _pI_str: {
				StringArray sa(arg);
				for (pI_size lv = 0; lv < label_count; ++lv) {
					pI_str label_string(sa.GetData (lv));
					pI_int ilabel;
					t_slm::left_const_iterator li(_string_label_map.left.find (label_string));
					if (li == _string_label_map.left.end()) {
						_string_label_map.left.insert (
						 t_slm::left_value_type (std::string(label_string), _string_label_count));
						ilabel = _string_label_count++;
					} else {
						ilabel = li->second;
					}
					labels.at<float> (static_cast<int> (lv), 0) = static_cast<float> (ilabel);
				}
				break;
			}
			case _pI_bool: {
				BoolArray ba(arg);
				for (pI_size lv = 0; lv < label_count; ++lv) {
					labels.at<float> (static_cast<int> (lv), 0) = static_cast<float> (ba.GetData (lv));
				}
				break;
			}
		} // switch (_label_type)
		return labels_cvmat;
	} // boost::shared_ptr<CvMat> GetLabelsAsDoubleCvMat (ArgumentPtr arg)

	ArgumentPtr CvMatToLabels (boost::shared_ptr<CvMat> mat) {

		cv::Mat predictions(mat.get(), false);
		ArgumentPtr prototype(GetInputSignature()[2]);
		pI_size label_count(mat->rows);
		switch (_label_type) {
			case _pI_double: {
				DoubleArray da(prototype); da.SetCount (label_count); da.CreateData();
				for (pI_size lv = 0; lv < label_count; ++lv) {
					da.SetData (lv, predictions.at<float> (static_cast<int> (lv), 0));
				}
				return da;
			}
			case _pI_int: {
				IntArray ia(prototype); ia.SetCount (label_count); ia.CreateData();
				for (pI_size lv = 0; lv < label_count; ++lv) {
					ia.SetData (lv, static_cast<pI_int> (predictions.at<float> (static_cast<int> (lv), 0)));
				}
				return ia;
			}
			case _pI_str: {
				StringArray sa(prototype); sa.SetCount (label_count); sa.CreateData();
				for (pI_size lv = 0; lv < label_count; ++lv) {
					pI_double pval(predictions.at<float> (static_cast<int> (lv), 0));
					t_slm::right_const_iterator ri(_string_label_map.right.find (static_cast<pI_int> (pval)));
					//if (ri != _string_label_map.right.end())
					sa.SetData (lv, const_cast<pI_str> (ri->second.c_str()));
				}
				return sa;
			}
			case _pI_bool: {
				BoolArray ba(prototype); ba.SetCount (label_count); ba.CreateData();
				for (pI_size lv = 0; lv < label_count; ++lv) {
					ba.SetData (lv, static_cast<pI_bool> (predictions.at<float> (static_cast<int> (lv), 0)));
				}
				return ba;
			}
			default:
				return ArgumentPtr();
		} // switch (_label_type)
	} // ArgumentPtr CvMatToLabels (boost::shared_ptr<CvMat> mat) {

	_pI_IntrinsicType _label_type;
	
	typedef boost::bimap<std::string, pI_int> t_slm;
	t_slm _string_label_map;
	pI_int _string_label_count;

}; // class MLBase: public pIn

class KNearest: public MLBase {

	typedef MLBase Base;

public:

	KNearest(Runtime& runtime):
	 Base(runtime, _pI_double),
	 _trained(false),
	 _k(32) {

		DefaultMLParamterSignature ();
		IntValueParameter("k", "up to k neighbours are regarded", 32);
		DefaultMLArgumentSignature();
		//_output_args.push_back (DoubleArray(runtime).SetName ("accuracy").SetDescription ("the accuracy of the responses"));
	}

	virtual ~KNearest() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(KNearest)

	virtual const std::string GetDescription() const {
        return "OpenCV KNN implementation.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/ML/KNearest";
    }

	virtual void Serialize (std::stringstream& serialization_data) const {

		return Base::Serialize (serialization_data);

//#ifdef WIN32
//		/* build serialization arguments */
//		Arguments s11nargs;
//		s11nargs.push_back (
//		 pI::BoolValue(_runtime).SetData (_trained ? pI_TRUE : pI_FALSE));
//		s11nargs.push_back (
//		 pI::IntValue(_runtime).SetData (_k));
//		if (_trained == pI_TRUE) {
//			/* fetch path to unique temporary file for opencv s11n */
//			ArgumentPtr arg_temp_path;
//			if (_runtimeGetProperty ("PATH_TEMP", arg_temp_path) == pI_FALSE) {
//				throw exception::SerializationException("KNearest serialization needs a set PATH_TEMP property");
//				return pI_FALSE;
//			}
//			pI_char temp_file_name_buf[MAX_PATH]; 
//
//			GetTempFileNameA ((PTSTR) StringValue(arg_temp_path).GetData(), "CV_KNN_", 0, (PTSTR) temp_file_name_buf);
//
//			/* let knn serialize into temporary file */
//			_knn.save ((char*) temp_file_name_buf);
//			/* read temp file into buffer */
//			std::ifstream ifs((PTSTR) temp_file_name_buf, std::ifstream::in);
//			std::string text_file, temp;
//			while (std::getline (ifs, temp))
//				text_file.append (temp);
//
//			s11nargs.push_back (
//			 pI::StringValue(_runtime).SetData ((pI_str) text_file.c_str()));
//		}
//		
//		serialization_data = _runtimeSerializeArguments (s11nargs);
//
//		return pI_TRUE;
//#else /* #ifdef WIN32 */
//		throw exception::SerializationException("KNearest serialization is only implemented for Windows atm.");
//		return pI_FALSE;
//#endif /* #ifdef WIN32 */
	} // virtual const pI_bool Serialize (std::string& serialization_data) const

protected:
	
	virtual void _Initialize (const Arguments& parameters) {
		
		Base::_Initialize (parameters);
		_knn.clear();
		_trained = false;
		SetLabelType (parameters);
		DefaultMLArgumentSignature();
		_k = 32;
		_knn_nearests.reset();
		if (parameters.size() > 1) {
			CheckParameter (parameters[1], _parameters[1]);
			_k = IntValue(parameters[1]).GetData();
		}
	}

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {

		if (input_args.size() < 3)
			throw exception::ArgumentException("KNearest needs three input arguments");
		CheckInputArguments (input_args, _input_args, 2);
		CheckArgumentType (input_args[2].get(), _input_args[2].get(), pI_TRUE);
		//if (output_args.size() > 0) {
		//	CheckOutputArguments (output_args, _output_args);
		//}
		pI_bool do_predict(BoolValue(input_args[0]).GetData());
		pI_size label_count(GetLabelCount (input_args[2]));
		if (label_count == 0)
			return;

		// acquire samples without copying -->
		//  this sadly does not work, since OpenCV cannot cope with double precision floating point samples
		// therefore, one could:
		//  1. introduce and use some FloatTable instead of the DoubleTable
		//  2. copy the data, which is slow.
		// in the following, possibility 2 is implemented.
		// boost::shared_ptr<CvMat> samples_cvmat(CVBase::ArgumentAsCVMat (input_args[1]));
		boost::shared_ptr<CvMat> samples_cvmat(GetSamplesAsFloatCvMat (input_args[1]));
		
		if (do_predict == pI_FALSE) { // TRAIN
			if (label_count != samples_cvmat->rows)
				throw exception::ArgumentException("number of given labels are not equal to number of given samples.");
			// acquire labels (with copying)
			boost::shared_ptr<CvMat> labels_cvmat = GetLabelsAsFloatCvMat (input_args[2]);
			// train with the labels
			_knn.train (samples_cvmat.get(), labels_cvmat.get(), 0, _label_type == _pI_double, _k, _trained);
			_trained = true;
		} else { // get RESPONSES
			if (!_trained)
				throw exception::ExecutionException ("KNearest cannot predict without being trained.");
			// cv matrix to temporarily receive the results
			boost::shared_ptr<CvMat> responses_cvmat(cvCreateMat(samples_cvmat->rows, 1, CV_32FC1), CvArrDelete());
			// if accuracies are needed, prepare proper structures
			if (output_args.size() > 0) {
				if (_knn_nearests.get() == 0)
					_knn_nearests = boost::shared_ptr<CvMat>(cvCreateMat (1, _k, CV_32FC1), CvArrDelete());
			}
			_knn.find_nearest (samples_cvmat.get(), _k, responses_cvmat.get(), 0, _knn_nearests.get(), 0);
			input_args[2] = CvMatToLabels (responses_cvmat);
			//if (output_args.size() > 0) {
			//	DoubleArray acc(output_args[0]);
			//	if (acc.GetCount() != label_count) {
			//		if (acc.HasData() == pI_TRUE)
			//			_runtime.GetCRuntime()->FreeArgumentData (_runtime.GetCRuntime(), output_args[0].get());
			//		acc.SetCount (label_count);
			//	}
			//	if (acc.HasData() == pI_FALSE)
			//		acc.CreateData();
			//	for (pI_size l = 0; l < label_count; ++l) {
			//		
			//	}
			//}
		}
	} // virtual void _Execute (Arguments& input_args, Arguments& output_args)

	bool _trained;
	pI_int _k;
	CvKNearest _knn;
	boost::shared_ptr<CvMat> _knn_nearests;
}; // class KNearest: public MLBase

class NormalBayes: public MLBase {

	typedef MLBase Base;

public:

	NormalBayes(Runtime& runtime):
	 Base(runtime, _pI_double),
	 _trained(false) {

		DefaultMLParamterSignature ();
		DefaultMLArgumentSignature();
	}

	virtual ~NormalBayes() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(NormalBayes)

	virtual const std::string GetDescription() const {
        return "OpenCV Bayes implementation, which assumes that the feature vectors of each class are normally distributed.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/ML/NormalBayes";
    }

protected:
	
	virtual void _Initialize (const Arguments& parameters) {
		
		Base::_Initialize (parameters);
		_nbc.clear();
		_trained = false;
		SetLabelType (parameters);
		DefaultMLArgumentSignature();
	}

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {

		if (input_args.size() < 3)
			throw exception::ArgumentException("NormalBayesClassifier needs three input arguments");
		CheckInputArguments (input_args, _input_args, 2);
		CheckArgumentType (input_args[2].get(), _input_args[2].get(), pI_TRUE);

		pI_bool do_predict(BoolValue(input_args[0]).GetData());
		pI_size label_count(GetLabelCount (input_args[2]));
		if (label_count == 0)
			return;

		// acquire samples without copying -->
		//  this sadly does not work, since OpenCV cannot cope with double precision floating point samples
		// therefore, one could:
		//  1. introduce and use some FloatTable instead of the DoubleTable
		//  2. copy the data, which is slow.
		// in the following, possibility 2 is implemented.
		// boost::shared_ptr<CvMat> samples_cvmat(CVBase::ArgumentAsCVMat (input_args[1]));
		boost::shared_ptr<CvMat> samples_cvmat(GetSamplesAsFloatCvMat (input_args[1]));
		
		if (do_predict == pI_FALSE) { // TRAIN
			if (label_count != samples_cvmat->rows)
				throw exception::ArgumentException("number of given labels are not equal to number of given samples.");
			// acquire labels (with copying)
			boost::shared_ptr<CvMat> labels_cvmat = GetLabelsAsFloatCvMat (input_args[2]);
			// train with the labels
			_nbc.train (samples_cvmat.get(), labels_cvmat.get(), 0, 0, _trained);
			_trained = true;
		} else { // get RESPONSES
			if (!_trained)
				throw exception::ExecutionException ("NormalBayes cannot predict without being trained.");
			// cv matrix to temporarily receive the results
			boost::shared_ptr<CvMat> responses_cvmat(cvCreateMat(samples_cvmat->rows, 1, CV_32FC1), CvArrDelete());
			_nbc.predict (samples_cvmat.get(), responses_cvmat.get());
			input_args[2] = CvMatToLabels (responses_cvmat);
		}
	} // virtual void _Execute (Arguments& input_args, Arguments& output_args)

	bool _trained;
	CvNormalBayesClassifier _nbc;
}; // class NormalBayes: public MLBase


//class SVM: public MLBase {
//
//	typedef MLBase Base;
//
//public:
//
//	SVM(Runtime& runtime):
//	 Base(runtime, _pI_double),
//	 _trained(false),
//	 _svm_auto_train(pI_TRUE) {
//
//		DefaultMLParamterSignature();
//		// SVN type parameter
//		StringSelection typesel(_runtime);
//		typesel.SetName ("type")
//			   .SetDescription ("SVM type")
//			   .SetCount (5);
//		typesel.CreateData();
//		typesel.SetSymbols (0, "C_SVC");
//		typesel.SetSymbols (1, "NU_SVC");
//		typesel.SetSymbols (2, "ONE_CLASS");
//		typesel.SetSymbols (3, "EPS_SVR");
//		typesel.SetSymbols (4, "NU_SVR");
//		typesel.SetIndex (0);
//		_parameters.push_back (typesel);
//
//		// kernel type parameter
//		StringSelection kernelsel(_runtime);
//		kernelsel.SetName ("kernel")
//			     .SetDescription ("SVM kernel")
//			     .SetCount (4);
//		kernelsel.CreateData();
//		kernelsel.SetSymbols (0, "LINEAR");
//		kernelsel.SetSymbols (1, "POLY");
//		kernelsel.SetSymbols (2, "RBF");
//		kernelsel.SetSymbols (3, "SIGMOID");
//		kernelsel.SetIndex (0);
//		_parameters.push_back (kernelsel);
//
//		// various SVN parameters
//		BoolValueParameter ("auto", "if true, the SVN training automatically determines its optimal parameter set by x-validation", pI_TRUE);
//		DoubleValueParameter ("degree", "degree parameter for poly kernels", 0.0);
//		DoubleValueParameter ("gamma", "gamma parameter for poly / rbf / sigmoid kernels", 0.0);
//		DoubleValueParameter ("coef0", "coef0 parameter for poly / sigmoid kernels", 0.0);
//		DoubleValueParameter ("C", "C parameter for SVN types C_SVC / EPS_SVR / NU_SVR", 0.0);
//		DoubleValueParameter ("nu", "nu parameter for SVN types C_SVC / ONE_CLASS / NU_SVR", 0.0);
//		DoubleValueParameter ("p", "p parameter for SVN type EPS_SVR", 0.0);
//
//		DefaultMLArgumentSignature();
//	}
//
//	virtual ~SVM() {}
//
//	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(SVM)
//
//	virtual const std::string GetDescription() const {
//        return "OpenCV Support Vector Machine implementation.";
//    }
//
//    virtual const std::string GetName() const {
//        return "OpenCV/ML/SVM";
//    }
//
//protected:
//	
//	virtual void _Initialize (const Arguments& parameters) {
//		
//		CheckParameters (parameters, _parameters, 1);
//		Base::_Initialize (parameters);
//		_svm.clear();
//		_trained = false;
//		SetLabelType (parameters);
//		DefaultMLArgumentSignature();
//
//	}
//
//	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
//	} // virtual void _Execute (Arguments& input_args, Arguments& output_args)
//
//	bool _trained;
//	CvSVM _svm;
//	int _svm_type, _svm_kernel;
//	pI_bool _svm_auto_train;
//	double _svm_gamma, _svm_coef0, _svm_C, _svm_nu, _svm_p;
//
//}; // class SVM: public MLBase

class EM: public MLBase {

	typedef MLBase Base;

public:

	EM(Runtime& runtime):
	 Base(runtime, _pI_int),
	 _trained(false),
	 _do_opt(pI_TRUE) {

		IntValueParameter ("N", "number of Gaussian mixtures", 2);
		StringSelection typesel(_runtime);
		typesel.SetName ("type")
			   .SetDescription ("type of covariance matrices")
			   .SetCount (3);
		typesel.CreateData();
		typesel.SetSymbols (0, "COV_MAT_SPHERICAL");
		typesel.SetSymbols (1, "COV_MAT_DIAGONAL");
		typesel.SetSymbols (2, "COV_MAT_GENERIC");
		typesel.SetIndex (0);
		_parameters.push_back (typesel);
		BoolValueParameter ("optimize", "if true, the model will be optimized", pI_TRUE);

		// initialize the em parameters
		_em_params.covs      = 0;
		_em_params.means     = 0;
		_em_params.weights   = 0;
		_em_params.probs     = 0;
		_em_params.nclusters = 2;
		_em_params.cov_mat_type = CvEM::COV_MAT_SPHERICAL;
		_em_params.start_step = CvEM::START_AUTO_STEP;
		_em_params.term_crit.max_iter = 10;
		_em_params.term_crit.epsilon = 0.1;
		_em_params.term_crit.type = CV_TERMCRIT_ITER | CV_TERMCRIT_EPS;
		
		_em_params_opt.cov_mat_type = CvEM::COV_MAT_DIAGONAL;
		_em_params_opt.start_step = CvEM::START_E_STEP;

		// define I/O signature
		_input_args.push_back (DoubleTable(_runtime).SetName ("samples").SetDescription ("samples to use for prediction / training").SetReadOnly (pI_TRUE));
		BoolValueInput ("predict", "if pI_TRUE, the given samples are used for prediction, otherwise they are used for training.", pI_FALSE, pI_TRUE);
		_output_args.push_back (IntArray(_runtime).SetName ("responses").SetDescription ("classification responses"));
	} // EM(Runtime& runtime)

	virtual ~EM() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(EM)

	virtual const std::string GetDescription() const {
        return "OpenCV EM implementation (unsupervised).";
    }

    virtual const std::string GetName() const {
        return "OpenCV/ML/EM";
    }

protected:
	
	virtual void _Initialize (const Arguments& parameters) {
		
		_em.clear();
		_em_opt.clear();
		_trained = false;
		if (parameters.size() > 0) {
			CheckParameter (parameters[0], _parameters[0]);
			pI_int N = IntValue(parameters[0]).GetData();
			_em_params.nclusters = N;
			_em_params_opt.nclusters = N;
		} else {
			_em_params.nclusters = 2;
			_em_params_opt.nclusters = 2;
		}
		_em_params.cov_mat_type = CvEM::COV_MAT_SPHERICAL;
		if (parameters.size() > 1) {
			CheckParameter (parameters[1], _parameters[1]);
			pI_int index = StringSelection(parameters[1]).GetIndex();
			if (index == 1) {
				_em_params.cov_mat_type = CvEM::COV_MAT_DIAGONAL;
			} else if (index == 2) {
				_em_params.cov_mat_type = CvEM::COV_MAT_GENERIC;
			}
		}
		if (parameters.size() > 2) {
			CheckParameter (parameters[2], _parameters[2]);
			_do_opt = BoolValue(parameters[2]).GetData();
		} else
			_do_opt = pI_TRUE;

	}

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {

		CheckInputArguments (input_args, _input_args, 1);
		CheckOutputArguments (output_args, _output_args);
		pI_bool do_predict(_trained);
		if (input_args.size() > 1) {
			do_predict = BoolValue(input_args[1]).GetData();
		}
		// acquire samples without copying -->
		//  this sadly does not work, since OpenCV cannot cope with double precision floating point samples
		// therefore, one could:
		//  1. introduce and use some FloatTable instead of the DoubleTable
		//  2. copy the data, which is slow.
		// in the following, possibility 2 is implemented.
		// boost::shared_ptr<CvMat> samples_cvmat(CVBase::ArgumentAsCVMat (input_args[1]));
		boost::shared_ptr<CvMat> samples_cvmat(GetSamplesAsFloatCvMat (input_args[0]));

		if (output_args[0]->signature.setup.IntArray->count != samples_cvmat->rows) {
			_runtime.GetCRuntime()->FreeArgumentData (_runtime.GetCRuntime(), output_args[0].get());
			output_args[0]->signature.setup.IntArray->count = samples_cvmat->rows;
			_runtime.GetCRuntime()->CreateArgumentData (_runtime.GetCRuntime(), output_args[0].get());
		}

		if (do_predict == pI_FALSE) { // TRAIN
			// acquire labels (with copying)
			boost::shared_ptr<CvMat>labels_cvmat(cvCreateMat (samples_cvmat->rows, 1, CV_32SC1), CvArrDelete());
			{
				cv::Mat labels(labels_cvmat.get(), false);
				switch (_label_type) {
					case _pI_int: {
						IntArray ia(output_args[0]);
						for (pI_int lv = 0; lv < samples_cvmat->rows; ++lv) {
							labels.at<float> (static_cast<int> (lv), 0) = static_cast<float> (ia.GetData (lv));
						}
						break;
					}
				}
			}

			// train with the labels
			_em.train (samples_cvmat.get(), 0, _em_params, labels_cvmat.get());
			if (_do_opt == pI_TRUE) {
				_em_params_opt.means = _em.get_means();
				_em_params_opt.covs = (const CvMat**) _em.get_covs();
				_em_params_opt.weights = _em.get_weights();
				_em_opt.train (samples_cvmat.get(), 0, _em_params_opt, labels_cvmat.get());
			}
			_trained = true;
		} else { // get RESPONSES
			if (!_trained)
				throw exception::ExecutionException ("EM cannot predict without being trained.");
			IntArray labels(output_args[0]);
			cv::Mat samples(samples_cvmat.get(), false);
			for (int lv = 0; lv < samples_cvmat->rows; ++lv) {
				if (_do_opt == pI_TRUE)
					labels.SetData (lv, cvRound (_em_opt.predict (samples.row (lv), 0)));
				else
					labels.SetData (lv, cvRound (_em.predict (samples.row (lv), 0)));
			}
		}
	} // virtual void _Execute (Arguments& input_args, Arguments& output_args)

	bool _trained;
	CvEM _em, _em_opt;
	CvEMParams _em_params, _em_params_opt;
	pI_bool _do_opt;

}; // class EM: public MLBase


class KMeans2: public MLBase {

	typedef MLBase Base;

public:

	KMeans2(Runtime& runtime):
	 Base(runtime, _pI_int),
	 _attempts(3),
	 _iterations(10),
	 _accuracy(1.0) {

		// define parameters
		IntValueParameter ("attempts", "specifies the number of executions with different labelings", _attempts);
		IntValueParameter ("iterations", "maximum number of iterations", _iterations);
		DoubleValueParameter ("accuracy", "maximum distance allowed for cluster movement between iterations", _accuracy);
		
		// define I/O signature
		_input_args.push_back (DoubleTable(_runtime).SetName ("samples").SetDescription ("input samples to cluster").SetReadOnly (pI_TRUE));
		IntValueInput ("N", "number of clusters", 3);
		_output_args.push_back (IntArray(_runtime).SetName ("labels").SetDescription ("calculated cluster indices"));
		_output_args.push_back (DoubleMatrix(_runtime).SetName ("cluster centers").SetDescription ("calculated cluster centers"));
		DoubleValueOutput ("compactness", "compactness value which represents the distances of the samples to their cluster centers");
	} // EM(Runtime& runtime)

	virtual ~KMeans2() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(KMeans2)

	virtual const std::string GetDescription() const {
        return "OpenCV KMeans2 implementation (unsupervised).";
    }

    virtual const std::string GetName() const {
        return "OpenCV/ML/KMeans2";
    }

protected:

	virtual void _Initialize (const Arguments& parameters) {
		
		if (parameters.size() > 0) {
			CheckParameter (parameters[0], _parameters[0]);
		} else {
			_attempts = IntValue(parameters[0]).GetData();
		}
		if (parameters.size() > 1) {
			CheckParameter (parameters[1], _parameters[1]);
		} else {
			_iterations = IntValue(parameters[1]).GetData();
		}
		if (parameters.size() > 2) {
			CheckParameter (parameters[2], _parameters[2]);
		} else {
			_accuracy = DoubleValue(parameters[2]).GetData();
		}
	}

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {

		CheckInputArguments (input_args, _input_args, 1);
		CheckOutputArguments (output_args, _output_args, 2);

		pI_int N(3);
		if (input_args.size() > 1) {
			CheckInputArgument (input_args[1], _input_args[1]);
			N = IntValue(input_args[1]).GetData();
		}

		// acquire samples without copying -->
		//  this sadly does not work, since OpenCV cannot cope with double precision floating point samples
		// therefore, one could:
		//  1. introduce and use some FloatTable instead of the DoubleTable
		//  2. copy the data, which is slow.
		// in the following, possibility 2 is implemented.
		// boost::shared_ptr<CvMat> samples_cvmat(CVBase::ArgumentAsCVMat (input_args[1]));
		boost::shared_ptr<CvMat> samples_cvmat(GetSamplesAsFloatCvMat (input_args[0]));

		if (output_args[0]->signature.setup.IntArray->count != samples_cvmat->rows) {
			_runtime.GetCRuntime()->FreeArgumentData (_runtime.GetCRuntime(), output_args[0].get());
			output_args[0]->signature.setup.IntArray->count = samples_cvmat->rows;
			_runtime.GetCRuntime()->CreateArgumentData (_runtime.GetCRuntime(), output_args[0].get());
		}
		if (output_args[1]->signature.setup.DoubleMatrix->rows != N ||
			output_args[1]->signature.setup.DoubleMatrix->cols != 2) {
			_runtime.GetCRuntime()->FreeArgumentData (_runtime.GetCRuntime(), output_args[1].get());
			output_args[1]->signature.setup.DoubleMatrix->rows = N;
			output_args[1]->signature.setup.DoubleMatrix->cols = 2;
			_runtime.GetCRuntime()->CreateArgumentData (_runtime.GetCRuntime(), output_args[1].get());
		}

		double compactness;
        cvKMeans2 (
		 samples_cvmat.get(),
		 N,
		 ArgumentAsCVArr (output_args[0]).get(),
		 cvTermCriteria (CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, _iterations, _accuracy),
		 _attempts,
		 0,
		 0,
		 ArgumentAsCVArr (output_args[1]).get(),
		 &compactness);
		if (output_args.size() > 2) {
			CheckOutputArgument (output_args[2], _output_args[2]);
			DoubleValue(output_args[2]).SetData (compactness);
		}

	} // virtual void _Execute (Arguments& input_args, Arguments& output_args)

	pI_int _attempts, _iterations;
	pI_double _accuracy;

}; // class KMeans2: public MLBase


} // namespace opencv
} // namespace pIns
} // namespace pI

#endif // CV_MLL_HPP
