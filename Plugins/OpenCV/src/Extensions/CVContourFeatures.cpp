/* CVContourFeatures.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include "CVContourFeatures.hpp"

#include <map>
#include <string>
#include <vector>

#include <boost/scoped_array.hpp>

#include <opencv/cv.h>

// DEBUG For debugging only:
#include <opencv/highgui.h>


#include <ArgumentChecks.hpp>
#include <Arguments/ByteImage.hpp>
#include <Arguments/DoubleTable.hpp>
#include <Arguments/IntMatrix.hpp>


#ifndef RAD2DEG
#define RAD2DEG 57.2957795130823208767981548141052
#endif


namespace pI {
namespace pIns {
namespace opencv {


CVContourFeatures::CVContourFeatures (Runtime& runtime) : Base (runtime) {

    _input_args.push_back (ByteImage (runtime)
                           .SetName ("Input image")
                           .SetDescription ("The input image passed to cvFindContours(): an 8-bit single channel image. "
                                            "Non-zero pixels are treated as 1’s, zero pixels remain 0’s - the image is treated as binary. "
                                            "The function modifies the source image’s content.")
                           .SetChannels (1)
                           .SetConstraints ("channels == 1"));

    _input_args.push_back (ByteImage (runtime)
                           .SetName ("Drawing image")
                           .SetDescription ("Image to draw the contours into."));

    _output_args.push_back (
        IntMatrix (runtime).SetName ("Contour matrix")
        .SetDescription ("Output matrix of the same size as the input image where each contour is filled with its label."));

    _output_args.push_back (
        DoubleTable (runtime).SetName ("Feature table")
        .SetDescription ("Output table where each row contains the features of one contour."));
}

/*virtual*/ CVContourFeatures::~CVContourFeatures() {
}

/*virtual*/ void CVContourFeatures::_Initialize (const Arguments& parameters) {

    CheckParameters (parameters, _parameters);

}

/*virtual*/ void CVContourFeatures::_Execute (Arguments& input_args, Arguments& output_args) {

    // Validates input and output arguments
    CheckInputArguments (input_args, GetInputSignature());
    CheckOutputArguments (output_args, GetOutputSignature());

    // TODO Should we use a deep copy of the input image here - or just overwrite it?
#if 0
    pI::ArgumentPtr deep_copy (this->_runtimeCopyArgument (input_args[0], pI_TRUE));
    ByteImage input (deep_copy);
#else
    ByteImage input (input_args[0]);
#endif

    if (!input.HasData()) {
        return;
    }

    int width  = input.GetWidth();
    int height = input.GetHeight();
    boost::shared_ptr<IplImage> iplIn = this->IplImageFromByteImage (input);

#ifdef _DEBUG
    //cvSaveImage ("CVContourFeatures_Input.png", iplIn);
#endif

    ByteImage draw (input_args[1]);
    boost::shared_ptr<IplImage> iplDraw;
    if (draw.HasData() && width == draw.GetWidth() && height == draw.GetHeight()) {
        iplDraw = this->IplImageFromByteImage (draw);
    }

    CvMemStorage* storage = cvCreateMemStorage (0);
    CvSeq* contour = 0;

    // Finds contours in the image. This function modifies its first argument!
    pI_size nContours = cvFindContours (iplIn.get(), storage, &contour, sizeof (CvContour),
                                        CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

    // Creates the matrix where contours are drawn into.
    IntMatrix labels (output_args[0]);
    pI_bool do_create_data (pI_TRUE);

    if (labels.HasData()) {
        if ( (labels.GetCols() == width) && (labels.GetRows() == height)) {
            do_create_data = pI_FALSE;
        }
    }

    if (do_create_data == pI_TRUE) {
        if (labels.HasData()) {
            _runtime.GetCRuntime()->FreeArgumentData (
                _runtime.GetCRuntime(), output_args[0].get());
        }

        labels.SetCols (width).SetRows (height);

		try {
			labels.CreateData();
		} catch (exception::MemoryException) {
            throw exception::ExecutionException ("Failed to create labels matrix.");
        }
    }

    // HACK The following lines are analogous to IplImageFromByteImage(), just  
    //      for IntMatrix instead of ByteImage.
    //      Here, we assume that pI_int, the data type used in IntMatrix, is defined
    //      to be int32_t, i.e. depth is IPL_DEPTH_32S. This is true for now - can it be tested?
    CvSize cvsize = cvSize (width, height);
    boost::shared_ptr<IplImage> iplOut (cvCreateImageHeader (cvsize, IPL_DEPTH_32S, 1), CvIplHeaderDelete());
    pI_int* dataptr = &(labels.GetCPtr()->data.IntMatrix->data[0][0]);
    cvSetData (iplOut.get(), dataptr, CV_AUTOSTEP /* = sizeof (pI_int) * labels.GetCols() */);

	// Zeros out the matrix.
	CvScalar zero = cvScalar (0.);
	cvSet (iplOut.get(), zero);

    // Now that the number of contours - i.e. number of rows - is known, creates the resulting DoubleTable.
    DoubleTable dt (output_args[1]);

    if (dt.HasData()) {
        _runtime.GetCRuntime()->FreeArgumentData (
            _runtime.GetCRuntime(), output_args[1].get());
    }

    dt.SetCols (44);
    dt.SetRows (nContours);

    dt.SetHeader (0, "Label"); // Repeat yourself

    dt.SetHeader (1, "Center_X");
    dt.SetHeader (2, "Center_Y");
    dt.SetHeader (3, "Area");

    const pI_size Box = 4;
    dt.SetHeader (Box + 0, "Box_X");
    dt.SetHeader (Box + 1, "Box_Y");
    dt.SetHeader (Box + 2, "Box_Width");
    dt.SetHeader (Box + 3, "Box_Height");

    dt.SetHeader (8, "ArcLength");
    dt.SetHeader (9, "Convexity");

    const pI_size MinAreaRect = 10;
    dt.SetHeader (MinAreaRect + 0, "MinAreaRect_X");
    dt.SetHeader (MinAreaRect + 1, "MinAreaRect_Y");
    dt.SetHeader (MinAreaRect + 2, "MinAreaRect_Width");
    dt.SetHeader (MinAreaRect + 3, "MinAreaRect_Height");
    dt.SetHeader (MinAreaRect + 4, "MinAreaRect_Angle");

    const pI_size FitEllipse = 15;
    dt.SetHeader (FitEllipse + 0, "FitEllipse_X");
    dt.SetHeader (FitEllipse + 1, "FitEllipse_Y");
    dt.SetHeader (FitEllipse + 2, "FitEllipse_Width");
    dt.SetHeader (FitEllipse + 3, "FitEllipse_Height");
    dt.SetHeader (FitEllipse + 4, "FitEllipse_Angle");

    const pI_size Moment = 20;
    dt.SetHeader (Moment + 0, "M00");
    dt.SetHeader (Moment + 1, "M10");
    dt.SetHeader (Moment + 2, "M01");
    dt.SetHeader (Moment + 3, "M20");
    dt.SetHeader (Moment + 4, "M11");
    dt.SetHeader (Moment + 5, "M02");
    dt.SetHeader (Moment + 6, "M30");
    dt.SetHeader (Moment + 7, "M21");
    dt.SetHeader (Moment + 8, "M12");
    dt.SetHeader (Moment + 9, "M03");

    dt.SetHeader (Moment + 10, "Mu20");
    dt.SetHeader (Moment + 11, "Mu11");
    dt.SetHeader (Moment + 12, "Mu02");
    dt.SetHeader (Moment + 13, "Mu30");
    dt.SetHeader (Moment + 14, "Mu21");
    dt.SetHeader (Moment + 15, "Mu12");
    dt.SetHeader (Moment + 16, "Mu03");

    dt.SetHeader (Moment + 17, "Hu1");
    dt.SetHeader (Moment + 18, "Hu2");
    dt.SetHeader (Moment + 19, "Hu3");
    dt.SetHeader (Moment + 20, "Hu4");
    dt.SetHeader (Moment + 21, "Hu5");
    dt.SetHeader (Moment + 22, "Hu6");
    dt.SetHeader (Moment + 23, "Hu7");

    if (nContours == 0) {
        cvReleaseMemStorage (&storage);

        return;
    }

    CvTreeNodeIterator iterator;
    cvInitTreeNodeIterator (&iterator, contour, 2);
    pI_size iContour = 0;

    while ( (contour = (CvSeq*) cvNextTreeNode (&iterator)) != 0) {

        if (iContour < nContours) {

            CvScalar color = { iContour+1 };
            cvDrawContours (iplOut.get(), contour, color, color, 0, CV_FILLED, 8);

            if (iplDraw) {
                CvScalar white = { 255, 255, 255 };
                cvDrawContours (iplDraw.get(), contour, white, white, 0, 1, 8);
            }

            dt.SetData (iContour, 0, (double)(iContour + 1));

            CvRect box = cvBoundingRect (contour);

            dt.SetData (iContour, Box + 0, (double) box.x);
            dt.SetData (iContour, Box + 1, (double) box.y);
            dt.SetData (iContour, Box + 2, (double) box.width);
            dt.SetData (iContour, Box + 3, (double) box.height);

            double arclength = cvArcLength (contour);
            dt.SetData (iContour, 6, arclength);

            int convexity = cvCheckContourConvexity (contour);
            dt.SetData (iContour, 7, (double) convexity);

            //cvFitLine (contour, CV_DIST_L2, 0, 0.01, 0.01, line);

            CvBox2D minarearect = cvMinAreaRect2 (contour);

            dt.SetData (iContour, MinAreaRect + 0, minarearect.center.x);
            dt.SetData (iContour, MinAreaRect + 1, minarearect.center.y);
            dt.SetData (iContour, MinAreaRect + 2, minarearect.size.width);
            dt.SetData (iContour, MinAreaRect + 3, minarearect.size.height);
            dt.SetData (iContour, MinAreaRect + 4, minarearect.angle);

            // CvBox2D ellipse = cvFitEllipse2 (contour);
            CvBox2D ellipse;
            ellipse.center.x = ellipse.center.y = 0.;
            ellipse.size.width = ellipse.size.height = 0.;
            ellipse.angle = 0.;

            dt.SetData (iContour, FitEllipse + 0, ellipse.center.x);
            dt.SetData (iContour, FitEllipse + 1, ellipse.center.y);
            dt.SetData (iContour, FitEllipse + 2, ellipse.size.width);
            dt.SetData (iContour, FitEllipse + 3, ellipse.size.height);
            dt.SetData (iContour, FitEllipse + 4, ellipse.angle);

            // Computes image moments
            CvMoments moments;
            cvMoments (contour, &moments);

            CvHuMoments huMoments;
            cvGetHuMoments (&moments, &huMoments);

            double area = abs (cvContourArea (contour));
            dt.SetData (iContour, 3, area/*moments.m00*/);

            // TODO What does "CenterX", "Center_Y" exactly stand for?
            //      Just the center of the bounding box? Or the centroid, as used here?
            double centroid_x = moments.m10 / moments.m00;
            double centroid_y = moments.m01 / moments.m00;

            dt.SetData (iContour, 1, centroid_x);
            dt.SetData (iContour, 2, centroid_y);
        
            // Old code - is this relevant?
            /*
            double uxx = moments.m00 * cvGetNormalizedCentralMoment (&moments, 2, 0);
            double uxy = moments.m00 * cvGetNormalizedCentralMoment (&moments, 1, 1);
            double uyy = moments.m00 * cvGetNormalizedCentralMoment (&moments, 0, 2);
            double common = sqrt ( (uxx - uyy) * (uxx - uyy) + 4.*uxy * uxy);

            feature["CentroidX"] = xc;
            feature["CentroidY"] = yc;
            feature["MajorAxis"] = sqrt (8.) * sqrt (uxx + uyy + common);
            feature["MinorAxis"] = sqrt (8.) * sqrt (uxx + uyy - common);
            feature["Orientation"] = RAD2DEG * atan2 (uxy, uxx - uyy) / 2.;

            feature["Roundness"] = feature["MinorAxis"] / feature["MajorAxis"];*/

            dt.SetData (iContour, Moment + 0, (double) moments.m00);
            dt.SetData (iContour, Moment + 1, (double) moments.m10);
            dt.SetData (iContour, Moment + 2, (double) moments.m01);
            dt.SetData (iContour, Moment + 3, (double) moments.m20);
            dt.SetData (iContour, Moment + 4, (double) moments.m11);
            dt.SetData (iContour, Moment + 5, (double) moments.m02);
            dt.SetData (iContour, Moment + 6, (double) moments.m30);
            dt.SetData (iContour, Moment + 7, (double) moments.m21);
            dt.SetData (iContour, Moment + 8, (double) moments.m12);
            dt.SetData (iContour, Moment + 9, (double) moments.m03);

            dt.SetData (iContour, Moment + 10, (double) moments.mu20);
            dt.SetData (iContour, Moment + 11, (double) moments.mu11);
            dt.SetData (iContour, Moment + 12, (double) moments.mu02);
            dt.SetData (iContour, Moment + 13, (double) moments.mu30);
            dt.SetData (iContour, Moment + 14, (double) moments.mu21);
            dt.SetData (iContour, Moment + 15, (double) moments.mu12);
            dt.SetData (iContour, Moment + 16, (double) moments.mu03);

            dt.SetData (iContour, Moment + 17, (double) huMoments.hu1);
            dt.SetData (iContour, Moment + 18, (double) huMoments.hu2);
            dt.SetData (iContour, Moment + 19, (double) huMoments.hu3);
            dt.SetData (iContour, Moment + 20, (double) huMoments.hu4);
            dt.SetData (iContour, Moment + 21, (double) huMoments.hu5);
            dt.SetData (iContour, Moment + 22, (double) huMoments.hu6);
            dt.SetData (iContour, Moment + 23, (double) huMoments.hu7);
        }

        ++iContour;
    }

	if (iContour != nContours) {
		throw pI::exception::ExecutionException("Not each contour was filled.");
	}

    cvReleaseMemStorage (&storage);
}

} // namespace opencv
} // namespace pIns
} // namespace pI
