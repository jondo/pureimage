/* CVContourFeatures.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PIN_TEMPLATE_HPP
#define PIN_TEMPLATE_HPP

#include "../CVBase.hpp"

namespace pI {
namespace pIns {
namespace opencv {

class CVContourFeatures: public CVBase {

    typedef CVBase Base;

public:

    CVContourFeatures (Runtime& runtime);
    virtual ~CVContourFeatures();

    DECLARE_VIRTUAL_COPY_CONSTRUCTOR(CVContourFeatures)

    virtual const pI_int GetpInVersion() const {
        return 10200;
    }

    virtual const std::string GetAuthor() const {
        return "JKU Linz";
    }

    virtual const std::string GetDescription() const {
        return "Returns a number of features derived from OpenCV's shape descriptors.";
    }

    virtual const std::string GetName() const {
        return "pIn/Contour/CVContourFeatures";
    }

    virtual Arguments GetParameterSignature() const {
        return MEMBER_SIGNATURE (_parameters);
    }

    virtual Arguments GetInputSignature() const {
        return MEMBER_SIGNATURE (_input_args);
    }

    virtual Arguments GetOutputSignature() const {
        return MEMBER_SIGNATURE (_output_args);
    }

protected:

    virtual void _Initialize (const Arguments& parameters);

    virtual void _Execute (Arguments& input_args, Arguments& output_args);

    Arguments _parameters, _input_args, _output_args;

}; // class CVContourFeatures: public pIn

} // namespace opencv
} // namespace pIns
} // namespace pI

#endif // PIN_TEMPLATE_HPP
