/* CVSegment.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CV_SEGMENT_HPP
#define CV_SEGMENT_HPP

#include "CVBase.hpp"

namespace pI {
namespace pIns {
namespace opencv {

class PyrSegmentation: public CVBase {

	typedef CVBase Base;

public:

	PyrSegmentation(Runtime& runtime):
	 Base(runtime),
	 _storage(0) {
		
		// setup memory storage
		_storage = cvCreateMemStorage();
		if (_storage == 0)
			throw exception::MemoryException("Failed to allocate memory for local memory storage.");
		
		// setup I/O
		_input_args.push_back(ByteImage(runtime).SetName ("src").SetDescription ("The source image"));
		IntValueInput ("level", "Maximum level of the pyramid for the segmentation", 3, pI_TRUE);
		DoubleValueInput ("threshold1", "Error threshold for establishing the links", 1.0, pI_TRUE);
		DoubleValueInput ("threshold2", "Error threshold for the segments clustering", 1.0, pI_TRUE);

		_output_args.push_back(ByteImage(runtime).SetName ("dst").SetDescription ("The destination image"));
	}

	virtual ~PyrSegmentation() {
		cvReleaseMemStorage (&_storage);
	}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(PyrSegmentation)

	virtual const std::string GetDescription() const {
        return "Implements image segmentation by pyramids.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Segmentation/PyrSegmentation";
    }

protected:

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		CheckInputArguments (input_args, _input_args, 1);
		CheckOutputArguments (output_args, _output_args);
		
		boost::shared_ptr<IplImage> src = IplImageFromByteImage (input_args[0]);
		output_args[0] = _runtime.CopyArgument (input_args[0], pI_TRUE);
		boost::shared_ptr<IplImage> dst = IplImageFromByteImage (output_args[0]);

		CvSeq* comp = 0;
		int level = 3;
		double thresh1 = 1.0, thresh2 = 1.0;
		if (input_args.size() > 1) {
			CheckInputArgument (input_args[1], _input_args[1]);
			level = IntValue(input_args[1]).GetData();
		}
		if (input_args.size() > 2) {
			CheckInputArgument (input_args[2], _input_args[2]);
			thresh1 = DoubleValue(input_args[2]).GetData();
		}
		if (input_args.size() > 3) {
			CheckInputArgument (input_args[3], _input_args[3]);
			thresh2 = DoubleValue(input_args[3]).GetData();
		}

		cvPyrSegmentation (src.get(), dst.get(), _storage, &comp, level, thresh1, thresh2);
		cvClearSeq (comp);
	}

protected:

	CvMemStorage* _storage;
}; // class PyrSegmentation: public pIn


class Threshold: public CVBase {

	typedef CVBase Base;

public:

	Threshold(Runtime& runtime):
	    Base(runtime), _thresh_type(CV_THRESH_BINARY), _threshold(127.0), _max_value(255.0) {

		// setup params
		DoubleValueParameter ("threshold", "Threshold value", 127.0);
		DoubleValueParameter ("maxValue", "Maximum value to use with CV_THRESH_BINARY and CV_THRESH_BINARY_INV thresholding types", 255.0);
		StringSelection ssel(runtime);
		ssel.SetName ("thresholdType")
			.SetDescription ("Thresholding type")
			.SetCount (5);
		ssel.CreateData();
		ssel.SetSymbols (0, "CV_THRESH_BINARY");
		ssel.SetSymbols (1, "CV_THRESH_BINARY_INV");
		ssel.SetSymbols (2, "CV_THRESH_TRUNC");
		ssel.SetSymbols (3, "CV_THRESH_TOZERO");
		ssel.SetSymbols (4, "CV_THRESH_TOZERO_INV");
		ssel.SetIndex (0);
		_parameters.push_back (ssel);
		BoolValueParameter("Otsu", "If pI_TRUE, the function determines the optimal threshold value using Otsu’s algorithm and uses it instead of the specified thresh", pI_FALSE);
		
        // setup I/O
        CvArrInput ("src", "The source array", pI_TRUE);
        CvArrOutput ("dst", "The destination array");
	}

	virtual ~Threshold() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Threshold)

	virtual const std::string GetDescription() const {
        return "Applies a fixed-level threshold to array elements.";
    }

    virtual const std::string GetName() const {
        return "OpenCV/Segmentation/Threshold";
    }

protected:

	virtual void _Initialize (const Arguments& parameters) {
		CheckParameters (parameters, _parameters);
		_threshold = DoubleValue(parameters[0]).GetData();
		_max_value = DoubleValue(parameters[1]).GetData();
		_thresh_type = CV_THRESH_BINARY;
		switch (StringSelection(parameters[2]).GetIndex()) {
			case 1: _thresh_type = CV_THRESH_BINARY_INV; break;
			case 2: _thresh_type = CV_THRESH_TRUNC; break;
			case 3: _thresh_type = CV_THRESH_TOZERO; break;
			case 4: _thresh_type = CV_THRESH_TOZERO_INV; break;
		}
		_thresh_type |= BoolValue(parameters[3]).GetData();
	}

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {
		Base::_Execute (input_args, output_args);

        // If the output argument is still VariableArgument, re-create it with
        // the type of the input argument; otherwise, Base::AdaptCvArrOutputSize()
        // would throw an exception. This fixes #225.
        if (output_args[0]->signature.type == T_pI_Argument_VariableArgument
            && input_args[0]->signature.type != output_args[0]->signature.type) {
                pI::ArgumentPtr new_output0 = this->GetRuntime().CreateArgument (
                    input_args[0]->signature.type, 
                    0, 
                    0,
                    output_args[0]->description.name,
                    output_args[0]->description.description);

                this->GetRuntime().GetCRuntime()->SwapArguments(
                    this->GetRuntime().GetCRuntime(), 
                    output_args[0].get(), new_output0.get());
        }

		Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);

		cvThreshold (
		 ArgumentAsCVArr (input_args[0]).get(),
		 ArgumentAsCVArr (output_args[0]).get(),
		 _threshold,
		 _max_value,
		 _thresh_type);
	}

protected:

	int _thresh_type;
	double _threshold, _max_value;
}; // class Threshold: public pIn



//class AdaptiveThreshold: public CVBase {
//
//	typedef CVBase Base;
//
//public:
//
//	AdaptiveThreshold(Runtime& runtime):
//	  Base(runtime), _thresh_type(CV_THRESH_BINARY), _threshold(127.0), _max_value(255.0), 
//		  double maxValue,
//		  int method, int type, int blockSize, double delta{
//
//		  // setup params
//		  DoubleValueParameter ("threshold", "Threshold value", 127.0);
//		  DoubleValueParameter ("maxValue", "Maximum value to use with CV_THRESH_BINARY and CV_THRESH_BINARY_INV thresholding types", 255.0);
//		  StringSelection ssel(runtime);
//		  ssel.SetName ("thresholdType")
//			  .SetDescription ("Thresholding type")
//			  .SetCount (5);
//		  ssel.CreateData();
//		  ssel.SetSymbols (0, "CV_THRESH_BINARY");
//		  ssel.SetSymbols (1, "CV_THRESH_BINARY_INV");
//		  ssel.SetSymbols (2, "CV_THRESH_TRUNC");
//		  ssel.SetSymbols (3, "CV_THRESH_TOZERO");
//		  ssel.SetSymbols (4, "CV_THRESH_TOZERO_INV");
//		  ssel.SetIndex (0);
//		  _parameters.push_back (ssel);
//		  BoolValueParameter("Otsu", "If pI_TRUE, the function determines the optimal threshold value using Otsu’s algorithm and uses it instead of the specified thresh", pI_FALSE);
//		  // setup I/O
//		  _input_args.push_back(ByteImage(runtime).SetName ("src").SetDescription ("The source image").SetChannels (1));
//		  _output_args.push_back(ByteImage(runtime).SetName ("dst").SetDescription ("The destination image").SetChannels (1));
//	  }
//
//	  virtual ~Threshold() {}
//
//	  DECLARE_VIRTUAL_COPY_CONSTRUCTOR(Threshold)
//
//	  virtual const std::string GetDescription() const {
//		  return "Applies an adaptive threshold to an array.";
//	  }
//
//	  virtual const std::string GetName() const {
//		  return "OpenCV/Segmentation/AdaptiveThreshold";
//	  }
//
//	  virtual void Initialize (const Arguments& parameters) {
//		  CheckParameters (parameters, _parameters);
//		  _threshold = DoubleValue(parameters[0]).GetData();
//		  _max_value = DoubleValue(parameters[1]).GetData();
//		  _thresh_type = CV_THRESH_BINARY;
//		  switch (StringSelection(parameters[2]).GetIndex()) {
//			case 1: _thresh_type = CV_THRESH_BINARY_INV; break;
//			case 2: _thresh_type = CV_THRESH_TRUNC; break;
//			case 3: _thresh_type = CV_THRESH_TOZERO; break;
//			case 4: _thresh_type = CV_THRESH_TOZERO_INV; break;
//		  }
//		  _thresh_type |= BoolValue(parameters[3]).GetData();
//	  }
//
//	  virtual void Execute (Arguments& input_args, Arguments& output_args) {
//		  Base::Execute (input_args, output_args);
//		  Base::AdaptCvArrOutputSize (input_args[0], output_args[0]);
//		  cvAdaptiveThreshold(
//			  ArgumentAsCVArr (input_args[0]).get(),
//			  ArgumentAsCVArr (output_args[0]).get(),
//			  _maxValue,
//			  _method,
//			  _type,
//			  _blockSize,
//			  _delta);
//	  }
//
//protected:
//
//	double _maxValue, _delta;
//	int _method, _type, _blockSize;
//}; // class Threshold: public pIn
//

} // namespace opencv
} // namespace pIns
} // namespace pI

#endif // CV_SEGMENT_HPP
