/* FreeImageReader.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <FreeImage.h>
#include <ArgumentChecks.hpp>

#include "FreeImageReader.hpp"
#include <Poco/File.h>

namespace pI {
namespace pIns {

FreeImageReader::FreeImageReader(Runtime& runtime): Base(runtime) {
}

/*virtual*/ FreeImageReader::~FreeImageReader() {}

/*virtual*/ void FreeImageReader::Initialize (const Arguments& parameters) {

	// check if input descriptor was already created, if so do nothing but return
	if (_input_args.size() == 1)
		return;

	// input signature
	_input_args.push_back (
	 _runtime.CreateArgument (T_pI_Argument_StringValue, pI_FALSE, 0, "Path", "Path to image file"));

	// output signature
	_output_args.push_back (
	 _runtime.CreateArgument (T_pI_Argument_ByteImage, pI_FALSE, 0, "Loaded image (FreeImage)", "Loaded image (using FreeImage lib)"));
	_output_args[0].get()->signature.setup.ByteImage->channels = 3;
}

/*virtual*/ void FreeImageReader::Execute (Arguments& input_args, Arguments& output_args) {

	// validate input arguments
	CheckInputArguments (input_args, GetInputSignature());
	CheckOutputArguments (output_args, GetOutputSignature());

	const pI_str path(input_args[0].get()->data.StringValue->data);
	
    // HACK For the time being, we want to avoid boost::filesystem; perhaps another solution is possible?
	if ( !Poco::File(path).exists() ) {

        throw exception::ExecutionException(
            "Illegal path or file does not exist.");

    } else {

		FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
		// check the file signature and deduce its format
		// (the second argument is currently not used by FreeImage)
		fif = FreeImage_GetFileType (path, 0);
		if (fif == FIF_UNKNOWN) {
			// no signature ?
			// try to guess the file format from the file extension
			fif = FreeImage_GetFIFFromFilename (path);
		}
		// check that the plugin has reading capabilities ...
		if ((fif != FIF_UNKNOWN) && FreeImage_FIFSupportsReading (fif)) {
			// ok, let's load the file
			FIBITMAP* dib = FreeImage_Load (fif, path, 0);
			if (dib != 0) {
				output_args[0] = _runtime.CopyArgument (_output_args[0], pI_FALSE);
				Argument* out_arg(output_args[0].get());

				unsigned bpp = FreeImage_GetBPP (dib);
				// if image is in unusual format, let freeimage convert it first
				if (bpp != 24) {
					FIBITMAP* dibconv = FreeImage_ConvertTo24Bits (dib);
					FreeImage_Unload (dib);
					dib = dibconv;
				}
				if (dib != 0) {
					pI_size width = FreeImage_GetWidth (dib);
					pI_size height = FreeImage_GetHeight (dib);
					out_arg->signature.setup.ByteImage->width = width;
					out_arg->signature.setup.ByteImage->height = height;
					// now let runtime allocate memory for target image
					_runtime.AllocateArgumentData (output_args[0]);
					// copy the pixel data
					for (pI_size h = 0; h < height; ++h) {
						BYTE* sl = FreeImage_GetScanLine (dib, (int) height-h-1);
						for (pI_size w = 0; w < width; ++w) {
							out_arg->data.ByteImage->data[h][w][0] = sl[w * 3 + 2];
							out_arg->data.ByteImage->data[h][w][1] = sl[w * 3 + 1];
							out_arg->data.ByteImage->data[h][w][2] = sl[w * 3];
						}
					}
					// unload freeimage image
					FreeImage_Unload (dib);
					// finally set path and pitch information
					out_arg->data.ByteImage->path = _runtime.CopyString (path);
					out_arg->data.ByteImage->pitch = (((bpp * width) + 31) / 32) * 4;
				}
			}
		} // if ((fif != FIF_UNKNOWN) && FreeImage_FIFSupportsReading (fif))
		else {
			throw exception::ExecutionException(
				"Illegal file format. File is not an image supported by FreeImage.");
		}
	} // if (path != 0)
}

} // namespace pIns
} // namespace pI
