/* FreeImageWriter.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PIN_FREE_IMAGE_WRITER_HPP
#define PIN_FREE_IMAGE_WRITER_HPP

#include <PureImage.hpp>

namespace pI {
namespace pIns {

class FreeImageWriter: public pIn {

typedef pIn Base;

public:

	FreeImageWriter(Runtime& runtime);
	virtual ~FreeImageWriter();

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(FreeImageWriter)

	virtual const pI_int GetpInVersion() const {
		return 10101;
	}

	virtual const std::string GetAuthor() const {
		return "JKU Linz";
	}

	virtual const std::string GetDescription() const {
		return "Writes an image to a given file path.";
	}

    virtual const std::string GetCopyright() const {
        return "This software uses the FreeImage open source image library. "
               "See http://freeimage.sourceforge.net for details. "
               "FreeImage is used under the GNU GPL, version 3.";
    }

	virtual const std::string GetName() const {
		return "FreeImage/IO/Writer";
	}

	virtual Arguments GetParameterSignature() const {
		return MEMBER_SIGNATURE(_parameters);
	}

	virtual void Initialize (const Arguments& parameters);

	virtual Arguments GetInputSignature() const {
		return MEMBER_SIGNATURE(_input_args);
	}

	virtual Arguments GetOutputSignature() const {
		return MEMBER_SIGNATURE(_output_args);
	}

	virtual void Execute (Arguments& input_args, Arguments& output_args);

protected:

	Arguments _parameters, _input_args, _output_args;

    bool _rgb2bgr;

}; // class FreeImageWriter: public ImagePlugin

} // namespace pIns
} // namespace pI

#endif // PIN_FREE_IMAGE_WRITER_HPP
