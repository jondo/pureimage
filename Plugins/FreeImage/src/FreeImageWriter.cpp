/* FreeImageWriter.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <FreeImage.h>
#include <ArgumentChecks.hpp>

#include "FreeImageWriter.hpp"

#include <Arguments/BoolValue.hpp>
using pI::BoolValue;
#include <Arguments/StringValue.hpp>
using pI::StringValue;
#include <Arguments/ByteImage.hpp>
using pI::ByteImage;

namespace pI {
namespace pIns {

FreeImageWriter::FreeImageWriter(Runtime& runtime): Base(runtime), _rgb2bgr(pI_TRUE) {

    _parameters.push_back (
		BoolValue(runtime)
			.SetName("rgb2bgr")
			.SetDescription("RGB to BGR conversion")
			.SetData(true));

    // input signature
	_input_args.push_back (
		StringValue(runtime)
			.SetName("Path")
			.SetDescription("Path to image file")
			.SetData(false));

	_input_args.push_back (
		ByteImage(runtime)
			.SetName("Image")
			.SetDescription("Image to save"));
}


/*virtual*/ FreeImageWriter::~FreeImageWriter() {}


/*virtual*/ void FreeImageWriter::Initialize (const Arguments& parameters) {

    if(parameters.size() == 0)
    {
        _rgb2bgr = pI_TRUE;
    }
    else
    {
        CheckParameters (parameters, _parameters);
        _rgb2bgr = (BoolValue(parameters[0]).GetData() != pI_FALSE);
    }
}


/*virtual*/ void FreeImageWriter::Execute (Arguments& input_args, Arguments& output_args) {

	// validate input arguments
	CheckInputArguments (input_args, GetInputSignature());

	bool success = true;

	const pI_str path(input_args[0].get()->data.StringValue->data);

	if (path != 0) {
        ByteImage img(input_args[1]);

        FIBITMAP* dib = 0;

        if (!_rgb2bgr) {
 		    dib = FreeImage_ConvertFromRawBits (
 		      &(input_args[1].get()->data.ByteImage->data[0][0][0]),
 		      input_args[1].get()->signature.setup.ByteImage->width,
 		      input_args[1].get()->signature.setup.ByteImage->height,
 		      input_args[1].get()->data.ByteImage->pitch,
 		      24,
 		      FI_RGBA_RED_MASK, 
              FI_RGBA_GREEN_MASK, 
              FI_RGBA_BLUE_MASK,
              TRUE);
        }
        // HACK One would expect that a call to FreeImage_ConvertFromRawBits() with 
        //      red_mask = FI_RGBA_BLUE_MASK, green_mask = FI_RGBA_GREEN_MASK, and blue_mask = FI_RGBA_RED_MASK
        //      would do RGB to BGR conversion, but that's not the case (tested against FreeImage 3.12.0).
        //      Hence, this does conversion byte by byte:
        else {
            pI_size width = img.GetWidth();
            pI_size height = img.GetHeight();
			pI_size channels = img.GetChannels();
            pI_byte*** rgb = img.GetCPtr()->data.ByteImage->data;

            dib = FreeImage_Allocate(width, height, 24);
            for (pI_size h = 0; h < img.GetHeight(); ++h) {
                // FreeImage has bottom line first layout
                BYTE* sl = FreeImage_GetScanLine (dib, (int) height-h-1);
                for (pI_size w = 0; w < width; ++w) {
                    // FreeImage uses BGR layout
                    sl[w * 3 + 2] = rgb[h][w][0];
					sl[w * 3 + 1] = rgb[h][w][channels > 1 ? 1 : 0];
					sl[w * 3] = rgb[h][w][channels > 2 ? 2 : 0];
                }
            }
        }

		if (dib != 0) {
			FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
			BOOL success = FALSE;
			// try to guess the file format from the file extension
			fif = FreeImage_GetFIFFromFilename (path);
			if (fif != FIF_UNKNOWN ) {
				// check that the plugin has sufficient writing and export capabilities ...
				WORD bpp = FreeImage_GetBPP (dib);
				if (FreeImage_FIFSupportsWriting (fif)) {
					if (FreeImage_FIFSupportsExportBPP (fif, bpp)) {
						success = FreeImage_Save (fif, dib, path, 0);
						if (success == false) {
							FreeImage_Unload (dib);
							throw exception::ExecutionException("FreeImageWriter failed to write image");
						}
					} else {
						throw exception::ExecutionException("FreeImageWriter failed to write image with given depth");
					}
				} else {
					throw exception::ExecutionException("FreeImageWriter does not support desired output format");
				}
			}
		} else {
			throw exception::ExecutionException("FreeImageWriter failed to write image");
		}
		FreeImage_Unload (dib);
	} else {
		throw exception::ExecutionException("FreeImageWriter received invalid path");
	}
}

} // namespace pIns
} // namespace pI
