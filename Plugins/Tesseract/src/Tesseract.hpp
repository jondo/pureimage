/* Tesseract.hpp
 *
 * Copyright 2009-2011 Johannes Kepler Universit�t Linz,
 * Institut f�r Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PI_PINS_TESSERACT_HPP
#define PI_PINS_TESSERACT_HPP

#include <PureImage.hpp>

namespace pI {
namespace pIns {

class Tesseract: public pIn {

    typedef pIn Base;

public:

    Tesseract (Runtime& runtime);
    virtual ~Tesseract();

    DECLARE_VIRTUAL_COPY_CONSTRUCTOR (Tesseract)

    virtual const pI_int GetpInVersion() const {
        return 10100;
    }

    virtual const std::string GetAuthor() const {
        return "JKU Linz";
    }

    virtual const std::string GetDescription() const {
        return "Performs OCR based on the Tesseract library.";
    }

    virtual const std::string GetName() const {
        return "Tesseract/RecognizePage";
    }

    virtual Arguments GetParameterSignature() const {
        return MEMBER_SIGNATURE (_parameters);
    }

    virtual Arguments GetInputSignature() const {
        return MEMBER_SIGNATURE (_input_args);
    }

    virtual Arguments GetOutputSignature() const {
        return MEMBER_SIGNATURE (_output_args);
    }

protected:

	virtual void _Initialize (const Arguments& parameters);

    virtual void _Execute (Arguments& input_args, Arguments& output_args);

    Arguments _parameters, _input_args, _output_args;

}; // class Tesseract: public pIn

} // namespace pIns
} // namespace pI

#endif // PI_PINS_TESSERACT_HPP
