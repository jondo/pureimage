/* Tesseract.cpp
 *
 * Copyright 2009-2011 Johannes Kepler Universit�t Linz,
 * Institut f�r Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Tesseract.hpp"

#include <ArgumentChecks.hpp>
#include <Arguments/ByteImage.hpp>
#include <Arguments/StringValue.hpp>

#include <baseapi.h>


namespace pI {
namespace pIns {

Tesseract::Tesseract (Runtime& runtime) : Base (runtime) {

    StringSelection lang (GetRuntime());
    lang.SetName ("Language")
    .SetDescription ("Language code in ISO 639-3 format.")
    .SetCount (4);

    // Right now, supports only some European languages, and defaults to English.
    lang.CreateData();
    lang.SetSymbols (0, "eng");
    lang.SetSymbols (1, "deu");
    lang.SetSymbols (2, "fra");
    lang.SetSymbols (3, "spa");
    lang.SetIndex (0);

    _parameters.push_back (lang);
}

/*virtual*/ Tesseract::~Tesseract() {
}

/*virtual*/ void Tesseract::_Initialize (const Arguments& parameters) {
    CheckParameters (parameters, _parameters);
    _parameters = GetRuntime().CopyArguments (parameters);

    _input_args.clear();
    _output_args.clear();

    _input_args.push_back (ByteImage (GetRuntime())
                           .SetName ("Input image")
                           .SetDescription ("Arbitrary 8-bit single channel input image.")
                           .SetChannels (1));

    _output_args.push_back (StringValue (GetRuntime())
                            .SetName ("Extracted text")
                            .SetDescription ("Text extracted from image using the Tesseract library."));
}


/*virtual*/ void Tesseract::_Execute (Arguments& input_args, Arguments& output_args) {
    CheckInputArguments (input_args, GetInputSignature());
    CheckOutputArguments (output_args, GetOutputSignature());

    StringSelection lang_selection (_parameters[0]);
    std::string lang (lang_selection.GetSymbols (lang_selection.GetIndex()));

    ByteImage input (input_args[0]);

    if (!input.HasData()) {
        return;
    }

    int width    = input.GetWidth();
    int height   = input.GetHeight();
    int channels = input.GetChannels();
    int bpp      = 8 * input.GetChannels();
    unsigned char* imgPtr = & (input.GetCPtr()->data.ByteImage->data[0][0][0]);

    TessBaseAPI::InitWithLanguage (NULL, NULL, lang.c_str(), NULL, false, 0, NULL);

    char* text =
        TessBaseAPI::TesseractRect (imgPtr, channels, width * channels,
                                    0, 0, width, height);

    GetRuntime().GetCRuntime()->FreeArgumentData (
        GetRuntime().GetCRuntime(),
        output_args[0].get());

    pI::StringValue str (output_args[0]);
    str.SetData (GetRuntime().GetCRuntime()->CopyString (GetRuntime().GetCRuntime(), text));

    // WTF TessBaseAPI's documentation says that "[t]he recognized text is returned as a char* which [...]
    //     must be freed with the delete [] operator."
    //     This works under Linux, but triggers an debug assertion _BLOCK_TYPE_IS_VALID(pHead->nBlockUse)
    //     under Win/VC10. Why?
    // delete[] text;
    TessBaseAPI::End();
}


} // namespace pIns
} // namespace pI
