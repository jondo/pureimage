/* JavaPInFactory.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <Adapters/C++/pInSharedLibraryExport.hpp>
#include <Adapters/C++/PluginAdapter.hpp>
#include <Adapters/C++/CpInAdapter.hpp>
#include <Adapters/C++/Arguments/StringValue.hpp>
//#include <Adapters/C++/Application.hpp>

#include <Poco/File.h>
//#include <Poco/Util/IniFileConfiguration.h>
#include <Poco/Path.h>
#include <Poco/Glob.h>
#include <Poco/AutoPtr.h>
#include <Poco/Environment.h>
#include <Poco/SharedLibrary.h>
//using Poco::Util::IniFileConfiguration;
using Poco::AutoPtr;

#include <boost/algorithm/string.hpp>
#include <boost/foreach.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>
using boost::shared_ptr;
using boost::format;

#include <string>
#include <vector>
#include <set>
using std::vector;
using std::string;
using std::set;

#include <jni.h>
#include <director.h>
#include <pI_java_jni.h>

// ##########################################
// DLL member variables:

string __last_error;
vector<string> m_plugin_names;

// jvm.dll and used methods
JavaVM* jvm;

// ##########################################

JNIEnv* JNIHelper_attachToThread(JavaVM* jvm)
// throw (JNIException)
{
    JNIEnv* env;
    jint error = jvm->AttachCurrentThread( reinterpret_cast<void**>( &env ), 0 );
    if ( error ) {
        throw "Unable to attach the current thread.";
    }
    return env;
}

void JavaPInFactory_CreateVm(const std::string& lib_path,
                             const std::string& pI_java_jar,
                             const std::set<std::string>& plugin_jars,
                             const std::string& user_class_path) {

    std::string classpath = "";

//    classpath += pI_java_jar;
    for(std::set<std::string>::const_iterator it = plugin_jars.begin();
        it != plugin_jars.end(); it++) {
        classpath += ";" + *it;
    }
    //classpath += ";" + user_class_path;
    classpath = "-Djava.class.path=" + classpath;

    JavaVMInitArgs initArgs;
    JavaVMOption options[1];
    options[0].optionString = (char*) classpath.c_str();

    // TODO: is there a way to auto-detect JNI version?
    // currently this is hard coded here
    initArgs.version = JNI_VERSION_1_6;
    initArgs.options = options;
    initArgs.nOptions = 1;
    initArgs.ignoreUnrecognized = JNI_FALSE;

    JNIEnv* env;
    int result = JNI_CreateJavaVM(&jvm,(void **)&env, &initArgs);

    if(result != 0) {
        // IDEA: result = -1 if there is a runnint VM on this thread...
        JavaVM* jvms;
        jsize jvm_count;
        JNI_GetCreatedJavaVMs(&jvms, 100, &jvm_count);

        if(jvm_count > 0) {
            // Heureka: found an existing JVM.
            jvm = jvms;
        }
    }

    if(jvm == 0) {
        throw pI::exception::PluginCreationException("Could not create JVM!");
    }

    /*
     * set some PI related properties
     */
    env = JNIHelper_attachToThread(jvm);
    jclass java_lang_system = env->FindClass("java/lang/System");

    jmethodID setProperty = env->GetStaticMethodID(java_lang_system, "setProperty", "(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;");
    jstring s_key = env->NewStringUTF("PI_DEBUG");
#ifdef _DEBUG
    jstring s_val = env->NewStringUTF("true");
#else // #ifdef _DEBUG
    jstring s_val = env->NewStringUTF("false");
#endif // #ifdef _DEBUG
    env->CallStaticObjectMethod(java_lang_system, setProperty, s_key, s_val);

    env->DeleteLocalRef(s_key);
    env->DeleteLocalRef(s_val);

    // need to set the "PI_JNI_LIB_PATH" so that JNI adapter can find dynamic library pI_java_jni
    // fixes issue #125
    s_key = env->NewStringUTF("PI_JNI_LIB_PATH");
    s_val = env->NewStringUTF(lib_path.c_str());
    env->CallStaticObjectMethod(java_lang_system, setProperty, s_key, s_val);
    env->DeleteLocalRef(s_key);
    env->DeleteLocalRef(s_val);

    jclass pI_PIUtils = env->FindClass("pI/PIUtils");
//    jmethodID _loadJNIFromPluginLibrary = env->GetStaticMethodID(pI_PIUtils, "loadJNIFromPluginLibrary", "()V");
    jmethodID _load_jni_lib = env->GetStaticMethodID(pI_PIUtils, "load_jni_lib", "()Z");
    env->CallStaticObjectMethod(pI_PIUtils, _load_jni_lib);
    if(env->ExceptionOccurred()) {
        env->ExceptionDescribe();
    }


}

/**
    Check if core classes can be found with the provided files.
    Just to be more verbose.
*/
void JavaPInFactory_CheckJavaInstallation() {

    // check for pI.IPlugin (in pI_java.jar)
    JNIEnv* env = JNIHelper_attachToThread(jvm);

    jclass _pI_IPlugin = env->FindClass("pI/DirectorPlugin");

    if(_pI_IPlugin == 0) {
        throw "Could not find java class 'pI.IPlugin'. Check specified archive 'pI_java.jar'.";
    }

    jclass _pI_pIJNI = env->FindClass("pI/pIJNI");
    if(env->ExceptionOccurred()) {
        env->ExceptionDescribe();
    }
    if(_pI_pIJNI == 0) {
        throw "Could not find java class 'pI.pIJNI'. Check specified archive 'pI_java.jar'.";
    }

}

/**
*
*/
vector<string> JavaPInFactory_GetPluginCtors(const set<string>& plugin_jars) {

    JNIEnv* env = JNIHelper_attachToThread(jvm);
    jclass pI_PIUtils = env->FindClass("pI/PIUtils");
    jclass java_util_list = env->FindClass("java/util/List");
    jmethodID list_get = env->GetMethodID(java_util_list,
        "get", "(I)Ljava/lang/Object;");
    jmethodID list_size = env->GetMethodID(java_util_list,
    "size", "()I");

    if(pI_PIUtils == 0) {
        throw "Could not find java class ´pI.PIUTils´";
    }

    // utility function implemented in pI.PIUtils:
    // public List getPluginClasses(String jarPath);
    jmethodID getPluginClasses =
        env->GetStaticMethodID(pI_PIUtils,
                                "getPluginClasses",
                                "(Ljava/lang/String;)Ljava/util/List;");
    if(getPluginClasses == 0) {
        throw "Could not find java method ´pI.PIUTils.getPluginClasses()´";
    }

    vector<string> plugin_names;

    for(std::set<std::string>::const_iterator it = plugin_jars.begin();
        it != plugin_jars.end(); it++) {
        jstring jar_path = env->NewStringUTF((*it).c_str());

        jobject jclassNames = env->CallStaticObjectMethod(pI_PIUtils, getPluginClasses, jar_path);
        jint nClassNames = env->CallIntMethod(jclassNames, list_size);

        for(int i=0; i<nClassNames; ++i) {
            jstring j_className = (jstring) env->CallObjectMethod(jclassNames, list_get, i);
            jboolean isCopy;
            const char* className = env->GetStringUTFChars(j_className, &isCopy);
            plugin_names.push_back(std::string(className));
            if(isCopy) {
                env->ReleaseStringUTFChars(j_className, className);
            }
        }

        env->DeleteLocalRef(jar_path);
    }

    return plugin_names;
}

// defined in pI_java_jni.cpp
extern "C" void JNICALL Java_pI_pIJNI_swig_1module_1init(JNIEnv *jenv, jclass jcls);

void JavaPInFactory_swig_module_init() {
    JNIEnv* jenv = JNIHelper_attachToThread(jvm);

    jclass pI_JNI_class = jenv->FindClass("pI/pIJNI");
    Java_pI_pIJNI_swig_1module_1init(jenv, pI_JNI_class);

    jenv->DeleteLocalRef(pI_JNI_class);
}

void JavaPInFactory_Initialize(struct _CRuntime* cruntime) {

    // Note: paths to "pI_java.jar" and "pI_java_jni.dll" 
    // are necessary to work
    // here we try to find some kind of configuration:
    // 1. "JavaPlugin.ini" in execution path
    // 2. "PUREIMAGE_PATH" variable and relative locations 'libraries' and 'Applications/Java'
    
    pI::Runtime runtime(cruntime);
    std::string PUREIMAGE_PATH = "";
    std::string CLASSPATH = "";
	std::string LIBRARY_PATH = "";
	std::string pI_java_jar = "";
    
	// retrieve pureImage path from runtime configuration
    if (runtime.HasProperty ("PUREIMAGE_PATH")) {
        pI::ArgumentPtr argPtr = runtime.GetProperty("PUREIMAGE_PATH");
        pI::StringValue prop(argPtr.get());
        PUREIMAGE_PATH = prop.GetData();
    } else {
		// runtime configuration lacks pureImage path; check environment variables
        try {
            PUREIMAGE_PATH = Poco::Environment::get("PUREIMAGE_PATH");
        } catch (Poco::NotFoundException& ) {}
    }
	// fail if not available
    if(PUREIMAGE_PATH.empty()) {
        throw pI::exception::PluginLibraryException("Could not find pureImage root directory. You can set a runtime or environment variable PUREIMAGE_PATH.");
    }

	// retrieve library path from runtime configuration
    if (runtime.HasProperty ("PUREIMAGE_LIBRARY_PATH")) {
        pI::ArgumentPtr argPtr = runtime.GetProperty("PUREIMAGE_LIBRARY_PATH");
        pI::StringValue prop(argPtr.get());
        LIBRARY_PATH = prop.GetData();
    } else {
		// runtime configuration lacks pureImage path; check environment variables
        try {
            LIBRARY_PATH = Poco::Environment::get("PUREIMAGE_LIBRARY_PATH");
        } catch (Poco::NotFoundException& ) {}
    }
	// fail if not available
    if(LIBRARY_PATH.empty()) {
        throw pI::exception::PluginLibraryException("Could not find pureImage library directory. You can set a runtime or environment variable PUREIMAGE_LIBRARY_PATH.");
    }

	// use classpath from runtime configuration
    if(runtime.HasProperty("CLASSPATH")) {
        pI::ArgumentPtr argPtr = runtime.GetProperty("CLASSPATH");
        pI::StringValue prop(argPtr.get());
        CLASSPATH = prop.GetData();
    } else {
		// runtime configuration lacks class path; check environment variables
        try {
            CLASSPATH = Poco::Environment::get("CLASSPATH");
        } catch (Poco::NotFoundException& ) {}
    }

    pI_java_jar = LIBRARY_PATH /*+ "/"*/ + "pI_java.jar";



    if(!Poco::File(pI_java_jar).exists()) {
        throw "Could not find ´pI_java.jar´.\
              Usually it resides in ´PUREIMAGE_PATH/$BUILD_CONFIG/libraries";
    }

    // find jar files in java plugin dirs
    set<string> pluginJarFiles;

    if(runtime.HasProperty("PLUGIN_JAR_FILES")) {
        pI::ArgumentPtr argPtr = runtime.GetProperty("PLUGIN_JAR_FILES");
        pI::StringValue prop(argPtr.get());
        std::string userPluginJars = prop.GetData();
        boost::split(pluginJarFiles, userPluginJars, boost::is_any_of(";"));
    }

    pI_bool auto_search_jars = pI_TRUE;

    if(runtime.HasProperty("AUTO_SEARCH_JAR_FILES")) {
        pI::ArgumentPtr argPtr = runtime.GetProperty("AUTO_SEARCH_JAR_FILES");
        pI::BoolValue prop(argPtr.get());
        auto_search_jars = prop.GetData();
    }

    if(auto_search_jars) {
        Poco::Path dir_path(LIBRARY_PATH);
        Poco::Glob::glob(dir_path.toString() + "/" + "*.jar", pluginJarFiles);
    }

    JavaPInFactory_CreateVm(LIBRARY_PATH, pI_java_jar, pluginJarFiles, CLASSPATH);

    JavaPInFactory_CheckJavaInstallation();

    m_plugin_names = JavaPInFactory_GetPluginCtors(pluginJarFiles);

    // this enables the director that allows to access Java Plugins
    // using swig generated C++ code
    JavaPInFactory_swig_module_init();
}


jobject JavaPInFactory_CreatePlugin(JNIEnv* env, CRuntime* runtime, jclass pin_cls)
{
    jclass SWIGTYPE_p_CRuntime_class = env->FindClass("pI/SWIGTYPE_p_CRuntimePtr");
    jmethodID SWIGTYPE_p_CRuntime_ctor = env->GetMethodID(SWIGTYPE_p_CRuntime_class, "<init>", "(JZ)V");
    jmethodID SWIGTYPE_p_CRuntime_getCPtr = env->GetStaticMethodID(SWIGTYPE_p_CRuntime_class, "getCPtr", "(LpI/SWIGTYPE_p_CRuntimePtr;)J");
    jclass Runtime_class = env->FindClass("pI/Runtime");
    jmethodID Runtime_ctor = env->GetMethodID(Runtime_class, "<init>", "(LpI/SWIGTYPE_p_CRuntimePtr;Z)V");

    // NOTE: this strange conversion to jlong is copied from swig generated code
    //      keep in mind that these jlongs managed by swig are pointers to the complex types
    //      this usually means, that for pointer types there is a doubled indirection (like here).
    jlong runtime_ptr;
    *(CRuntimePtr **)&runtime_ptr = &runtime; 
    jboolean no_own = JNI_FALSE;

    // SWIGTYPE_p_CRuntime cruntime = new SWIGTYPE_p_CRuntime(runtime_ptr, own);
    jobject j_cruntime_localref = env->NewObject(SWIGTYPE_p_CRuntime_class, SWIGTYPE_p_CRuntime_ctor, runtime_ptr, no_own);
    jobject j_cruntime = env->NewGlobalRef(j_cruntime_localref);

    // Runtime runtime = new Runtime(cruntime);
    jobject j_runtime_localref = env->NewObject(Runtime_class, Runtime_ctor, j_cruntime, JNI_FALSE);
    jobject j_runtime = env->NewGlobalRef(j_runtime_localref);
    if(env->ExceptionOccurred()) {
        env->ExceptionDescribe();
    }

    env->DeleteLocalRef(j_cruntime_localref);
    env->DeleteLocalRef(j_runtime_localref);

    env->DeleteLocalRef(SWIGTYPE_p_CRuntime_class);
    env->DeleteLocalRef(Runtime_class);

    // TODO: throw if not found
    jmethodID pin_ctor = env->GetMethodID(pin_cls, "<init>", "(LpI/Runtime;)V");

    // Plugin plugin = new Plugin(runtime);
    jobject plugin_obj = env->NewObject(pin_cls, pin_ctor, j_runtime);

    if (plugin_obj == 0) {
        return pI_FALSE;
    }

    return env->NewLocalRef(plugin_obj);
}

pI::PluginAdapter* CreatePInDirector(JNIEnv* jenv, CRuntime* cruntime, jobject j_pin, jclass j_pin_cls) {
    pI::Runtime runtime(cruntime);

    SwigDirector_PluginAdapter* pluginAdapter = new SwigDirector_PluginAdapter(jenv, runtime);

    pluginAdapter->swig_connect_director(jenv, j_pin, j_pin_cls, true, false);

    return pluginAdapter;
}


extern "C" pI_API pI_bool CreatepIns (pI_int index, struct _CRuntime* runtime, struct _CpIn** plugin) {

    if ((runtime == 0) || (index < 0))
        return pI_FALSE;

    static pI_bool is_initialized = pI_FALSE;

    if(!is_initialized) {
        try {
           JavaPInFactory_Initialize(runtime);
        } catch (const char* msg) {
            __last_error = msg;
            return pI_FALSE;
        }
        is_initialized = pI_TRUE;
    }

    // create a plugin if it has not been created already
    if(index >= 0 && index < m_plugin_names.size()) {
        JNIEnv* jenv = JNIHelper_attachToThread(jvm);

        jclass j_pin_cls = jenv->FindClass(m_plugin_names[index].c_str());
        if(j_pin_cls == 0) {

            if(jenv->ExceptionOccurred()) {
                jenv->ExceptionDescribe();
            }

            __last_error = "Could not find class " + m_plugin_names[index];
            *plugin = 0;
            return pI_TRUE;
        }
        jobject j_pin = JavaPInFactory_CreatePlugin(jenv, runtime, j_pin_cls);
        if(j_pin == 0) {
            if(jenv->ExceptionOccurred()) {
                jenv->ExceptionDescribe();
            }
            __last_error = "Could not instantiate java plugin " + m_plugin_names[index];
            *plugin = 0;
            return pI_TRUE;
        }
        pI::PluginAdapter* pluginAdapter = CreatePInDirector(jenv, runtime, j_pin, j_pin_cls);
        jenv->DeleteLocalRef(j_pin_cls);

        pI::pInPtr jplugin = pI::pInPtr(pluginAdapter);
        pI::CpInCloningAdapter* cpin =
            const_cast<pI::CpInCloningAdapter*>(pI::CpInCloningAdapter::Create(jplugin));
        *plugin = const_cast<struct _CpIn* > (cpin->GetCpIn());
        if (*plugin == 0)
            delete cpin;

        return pI_TRUE;
    }

    return pI_FALSE;
}; /* extern "C" pI_API pI_bool CreatepIns (pI_int index, struct _CRuntime* host, struct _CpIn** plugin) */

extern "C" pI_API pI_str GetLastErrorMsg() {
    return (pI_str) __last_error.c_str();
}
