/* MirrorPluginRunner.java
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

package pI.image;

import java.io.IOException;

import pI.Argument;
import pI.ArgumentVector;
import pI.Application;
import pI.BoolValue;
import pI.ByteImage;
import pI.LauncherUtil;
import pI.PIUtils;
import pI.Runtime;
import pI.StringValue;
import pI.StringVector;
import pI.pI;
import pI.pIn;

public class MirrorPluginRunner {

	public static void main(String[] args) {
		String PUREIMAGE_PATH = LauncherUtil.findPIBaseDir();		

		PIUtils.initJNI(PUREIMAGE_PATH);

		String pIBaseDir = PIUtils.getPIBaseDir();
		String pluginDir = PIUtils.getPluginDir();
		
		Runtime runtime = new Runtime(true);
		
		PIUtils.setRuntimeProperty(runtime, "PUREIMAGE_PATH", PUREIMAGE_PATH);
		
		String testImgPath = pIBaseDir + "/Applications/Images/Kommen und Gehen/Kulturzeichen Linz09.jpg";		

		Application app = new Application(runtime);
			
		try {
			app.LoadPluginLibrary(pluginDir, "Java_pIns");
			app.LoadPluginLibrary(pluginDir, "FreeImage_pIns");
			app.LoadPluginLibrary(pluginDir, "CImg_pIns");			
		} catch (RuntimeException e) {
			System.err.println("Could not load plugin libraries.");
			System.exit(-1);
		}	
		app.RegisterPlugins();

		/* note: runtime meta information query functions are disabled atm.
		StringVector pluginNames = app.GetPluginNames();
		for (int i = 0; i < pluginNames.size(); i++) {
			System.out.println(pluginNames.get(i));
		}
		*/
		
		pIn imgReader = runtime.SpawnPlugin("FreeImage/IO/Reader");
		pIn flipImg = runtime.SpawnPlugin(Mirror.NAME);
		pIn display = runtime.SpawnPlugin("CImg/Display");
		
		// ##########################################################
		// Initialization
		// ##########################################################

		imgReader.Initialize(new ArgumentVector());

		ArgumentVector flipParams = flipImg.GetParameterSignature();
		BoolValue flipX = (BoolValue) flipParams.Get(0);
		BoolValue flipY = (BoolValue) flipParams.Get(1);
		BoolValue flipChannel = (BoolValue) flipParams.Get(2);

		flipX.SetData(false);
		flipY.SetData(true);
		flipChannel.SetData(false);
		flipImg.Initialize(flipParams);

		display.Initialize(display.GetParameterSignature());

		// ##########################################################
		// Execution
		// ##########################################################

		ArgumentVector irIn = imgReader.GetInputSignature();
		ArgumentVector irOut = imgReader.GetOutputSignature();

		StringValue path = (StringValue) irIn.Get(0);
		path.SetData(testImgPath);
		imgReader.Execute(irIn, irOut);
		ByteImage img = (ByteImage) irOut.Get(0);
		
		ArgumentVector flipImg_in = new ArgumentVector();
		flipImg_in.Add(img);

		ByteImage flippedImg = (ByteImage) flipImg.Execute(flipImg_in).Get(0);

		ArgumentVector display_in = new ArgumentVector();
		display_in.Add(flippedImg);
		
		display.Execute(display_in);
	
		System.out.println("----------- Press key to continue -----------");
		try {
			System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
