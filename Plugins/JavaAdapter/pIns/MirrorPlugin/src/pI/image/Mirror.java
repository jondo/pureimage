/* Mirror.java
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

package pI.image;

import pI.ArgumentVector;
import pI.Arguments;
import pI.DirectorPlugin;
import pI.Argument;
import pI.BoolValue;
import pI.ByteImage;
import pI.Runtime;
import pI.pI;

public class Mirror extends DirectorPlugin {

	public static final String NAME = "pIn/Geometric/MirrorJava";
		
	boolean flipX = false;

	boolean flipY = false;

	boolean flipChannels = false;

	public Mirror(Runtime runtime) {
		super(runtime);
	}

	@Override
	public int GetpInVersion() {
		return 10000;
	}

	@Override
	public String GetAuthor() {
		return "JKU Linz";
	}

	@Override
	public String GetCopyright() {
		return "";
	}

	@Override
	public String GetDescription() {
		return "Mirrors a given image along x or/and y axis";
	}

	@Override
	public String GetName() {
		return NAME;
	}

	@Override
	public ArgumentVector GetParameterSignature() {

		BoolValue flipX = new BoolValue(runtime);
		flipX.SetName("flipX")
			 .SetDescription("Activate to flip the image along x axis.")
			 .SetData(false);
		BoolValue flipY = new BoolValue(runtime);
		flipY.SetName("flipY")
		     .SetDescription("Activate to flip the image along y axis.")
		     .SetData(false);
		BoolValue flipChannels = new BoolValue(runtime);
		flipChannels.SetName("flipChannels")
		     .SetDescription("Activate to flip the channel order (e.g., RGB->BGR).")
		     .SetData(false);

		return new ArgumentVector(flipX, flipY, flipChannels);
	}

	@Override
	protected void _Initialize(ArgumentVector parameters) {

		pI.CheckParameters(parameters, GetParameterSignature());
		
		BoolValue flipX = (BoolValue) parameters.Get(0);
		BoolValue flipY = (BoolValue) parameters.Get(1);
		BoolValue flipChannels = (BoolValue) parameters.Get(2);

		this.flipX = flipX.GetData();
		this.flipY = flipY.GetData();
		this.flipChannels = flipChannels.GetData();

	}

	@Override
	public ArgumentVector GetInputSignature() {

		ByteImage img = new ByteImage(runtime)
				.SetName("InputImage")
				.SetDescription("the image to be processed");

		return new ArgumentVector(img);
	}

	@Override
	public ArgumentVector GetOutputSignature() {

		ByteImage img = new ByteImage(runtime)
				.SetName("InputImage")
		  		.SetDescription("the processed image");

		return new ArgumentVector(img);
	}

	@Override
	protected void _Execute(ArgumentVector inputArgs,
			ArgumentVector outputArgs) {
		
		pI.CheckInputArguments(inputArgs, GetInputSignature());
		pI.CheckOutputArguments(outputArgs, GetOutputSignature());

		ByteImage inImg = (ByteImage) inputArgs.Get(0);
		ByteImage outImg = (ByteImage) pI.adaptArgument(runtime.CopyArgument(inImg.GetCPtr(), false), true);

		long width = inImg.GetWidth();
		long height = inImg.GetHeight();
		long channels = inImg.GetChannels();

		for(long row = 0; row < height; row++) {
			long rowDst = (flipY ? height-row-1 : row);

			for(long col = 0; col < width; col++) {
				long colDst = ( flipX ? width-col-1 : col);

				for(long channel = 0; channel < channels; channel++) {
					long channelDst = ( flipChannels ? channels-channel-1 : channel);

					short value = inImg.GetData(row, col, channel);
					outImg.SetData(rowDst, colDst, channelDst, value);
				}
			}
		}

		outputArgs.Set(0, outImg);
	}
}
