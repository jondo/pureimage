/* MyPlugin.java
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

package pI.myplugin;

import pI.AbstractPlugin;
import pI.Argument;
import pI.Runtime;

public class MyPlugin extends AbstractPlugin {

	public MyPlugin(Runtime runtime) {
		super(runtime);
	}

	@Override
	public String GetAuthor() {
		return "JKU Linz";
	}

	@Override
	public String GetCopyright() {
		return "";
	}

	@Override
	public String GetDescription() {
		return "A template plugin";
	}

	@Override
	public String GetName() {
		return "pIn/TemplatePlugin";
	}

	@Override
	public Argument[] GetParameterSignature() {
		// does not have any parameters
		return new Argument[0];
	}

	@Override
	public Argument[] GetInputSignature() {
		// does not have any input arguments
		return new Argument[0];
	}

	@Override
	public Argument[] GetOutputSignature() {
		// does not have any input arguments
		return new Argument[0];
	}

	@Override
	public int GetpInVersion() {
		return 10000;
	}

	@Override
	protected void _Initialize(Argument... params) {
		// does nothing
	}

	@Override
	protected void _Execute(Argument[] inArgs, Argument[] outArgs) {
		// does nothing
	}
}
