/* CircleCookie.java
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

package pI;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

public class CircleCookie implements Cookie {
	
	Ellipse2D.Double circle;
	Color color;
	
	public CircleCookie(double x, double y, double w, double h, Color rgb) {
		circle = new Ellipse2D.Double(x, y , w, h);
		color = rgb;
	}

	@Override
	public void draw(Graphics2D g) {
		g.setColor(color);
		g.fill(circle);
	}
	
	public void setColor(int r, int g, int b) {
		int rgb = (0xFF & r) << 16 | (0xFF & g) << 8 | (0xFF & b);
		color = new Color(rgb);
	}

	@Override
	public boolean isInside(double x, double y) {
		return circle.contains(x, y);
	}

	@Override
	public void shift(double x, double y) {
		circle.x += x;
		circle.y += y;
	}
	
	@Override
	public void setPos(double x, double y) {
		circle.x = x;
		circle.y = y;
	}

	@Override
	public Cookie createCopy() {
		return new CircleCookie(circle.x, circle.y, circle.width, circle.height, color);
	}
	
	@Override
	public Rectangle2D getBounds() {
		return circle.getBounds2D();
	}
	
}
