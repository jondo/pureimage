/* RectangleCookie.java
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

package pI;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

public class RectangleCookie implements Cookie {
	
	Rectangle2D.Double rect;
	Color color;
	
	public RectangleCookie(double x, double y, double w, double h, Color rgb) {
		rect = new Rectangle2D.Double(x, y, w, h);
		color = rgb;
	}

	@Override
	public void draw(Graphics2D g) {
		g.setColor(color);
		g.fill(rect);
	}
	
	public void setColor(int r, int g, int b) {
		color = new Color(r,g,b);
	}

	@Override
	public boolean isInside(double x, double y) {
		return rect.contains(x, y);
	}

	@Override
	public void shift(double x, double y) {
		rect.x += x;
		rect.y += y;
	}

	public void setPos(double x, double y) {
		rect.x = x;
		rect.y = y;
	}
	
	@Override
	public Cookie createCopy() {
		return new RectangleCookie(rect.x, rect.y, rect.width, rect.height, color);
	}
	
	@Override
	public Rectangle2D getBounds() {
		return rect.getBounds2D();
	}
	
}
