/* MyCookies.java
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

package pI;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.List;

import javax.swing.ImageIcon;

public class MyCookies {
	
	public static List<Cookie> createCookies(double scale) {
		
		CircleCookie c1 = new CircleCookie(0, 0, scale*100, scale*100, new Color(255,0,0));
		CircleCookie c2 = new CircleCookie(0, 0, scale*50, scale*100, new Color(0,255,0));
		CircleCookie c3 = new CircleCookie(0, 0, scale*50, scale*50, new Color(0,0,255));
		RectangleCookie c4 = new RectangleCookie(0, 0, scale*120, scale*40, new Color(255,0,0));
		RectangleCookie c5 = new RectangleCookie(0, 0, scale*10, scale*50, new Color(0,255,0));
		
		return Arrays.asList(c1,c2,c3,c4,c5);
	}
	
	public static ImageIcon createIcon(Cookie cookie, int iconWidth) {
		Rectangle2D bounds = cookie.getBounds();
		BufferedImage iconImgBuf = new BufferedImage(iconWidth, iconWidth, BufferedImage.TYPE_3BYTE_BGR);			
		Graphics2D g2d = iconImgBuf.createGraphics();

		AffineTransform tx = new AffineTransform();
		tx.setToTranslation(bounds.getMinX(), bounds.getMaxX());
		double maxSize = Math.max(bounds.getWidth(), bounds.getHeight());
		double s = iconWidth / maxSize; 
		tx.setTransform(s, 0, 0, s,0, 0);
		
		g2d.clearRect(0, 0, iconWidth, iconWidth);
		g2d.setTransform(tx);
		g2d.setBackground(Color.white);			
		cookie.draw(g2d);
		
		ImageIcon imageIcon = new ImageIcon(iconImgBuf);

		return imageIcon;
	}

}
