/* Cookie.java
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

package pI;

import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

public interface Cookie {
	
	public void draw(Graphics2D g);

	public boolean isInside(double x, double y);

	public void shift(double x, double y);

	public void setPos(double x, double y);
	
	public Cookie createCopy();
	
	public Rectangle2D getBounds();
}
