/* SimulationCam.java
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

package pI;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("serial")
public class SimulationCam extends Canvas {
	
	List<Cookie> items;
	
	public SimulationCam() {
		items = new LinkedList<Cookie>();
		setBackground(Color.white);		
	}
	
	@Override
	public void paint(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;

		g2d.setBackground(Color.white);
		g2d.clearRect(0, 0, getWidth(), getHeight());
		for(Cookie item: items) {
			item.draw(g2d);
		}
	}

	public void add(Cookie item) {
		items.add(item);
	}

	public void remove(Cookie item) {
		items.remove(item);
	}
	
	public Cookie getCookieAtPos(int x, int y) {
		
		for(Cookie item: items) {
			if(item.isInside(x, y)) {
				return item;
			}
		}
		
		return null;
	}
}
