/* SimulationCamPlugin.java
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

package pI;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class SimulationCamPlugin extends DirectorPlugin {
	
	public static final String ID_PIN_IO_CAPTURESIMCAM = "pIn/IO/CaptureSimCam";
	
	JFrame mainFrame;
	SimulationCamPanel cam;
	int width;
	int height;
	double displayScale;
	boolean isStandalone;
	
	BufferedImage imgBuf;
	
	public SimulationCamPlugin(Runtime runtime) {
		super(runtime);
	}
	
	@Override
	protected void finalize() {
		super.finalize();
		if(mainFrame != null) {
			mainFrame.dispose();
		}		
	}

	@Override
	public ArgumentVector GetParameterSignature() {
		// this is for compatibity with OpenCV cam plugin
		// not used in this plugin
		IntValue control = new IntValue(runtime).SetName("control").SetData(0);
		IntValue width = new IntValue(runtime).SetName("width").SetData(0);
		IntValue height = new IntValue(runtime).SetName("height").SetData(0);
		DoubleValue displayScale = new DoubleValue(runtime).SetName("displayScale").SetData(1.0);
		BoolValue isStandalone = new BoolValue(runtime).SetName("isStandalone").SetData(true);
		return new ArgumentVector(control, 
							  width, 
							  height,
							  displayScale, 
							  isStandalone);
	}
	
	@Override
	protected void _Initialize(ArgumentVector inputArgs) {
		pI.CheckParameters(inputArgs, GetParameterSignature());		
		this.width = ((IntValue) inputArgs.Get(1)).GetData();
		this.height = ((IntValue) inputArgs.Get(2)).GetData();
		this.displayScale = ((DoubleValue) inputArgs.Get(3)).GetData();
		this.isStandalone = ((BoolValue) inputArgs.Get(4)).GetData();
		
		cam = new SimulationCamPanel(width, height, displayScale);
		if(isStandalone) {
			if(mainFrame != null) {
				mainFrame.dispose();
			}
			new Thread(new Runnable() {
				@Override
				public void run() {
					mainFrame = new JFrame("SimCam");
					mainFrame.getContentPane().add(cam);
					mainFrame.pack();
					mainFrame.setVisible(true);
				}
			}).start();
		}
		
		imgBuf = new BufferedImage(this.width, this.height, BufferedImage.TYPE_3BYTE_BGR);
	}

	@Override
	public ArgumentVector GetInputSignature() {
		return new ArgumentVector();
	}

	@Override
	public ArgumentVector GetOutputSignature() {
		return new ArgumentVector(new ByteImage(runtime));
	}

	@Override
	protected void _Execute(ArgumentVector inputArgs, ArgumentVector outputArgs) {
		Rectangle bounds = cam.draw.getBounds();
		double scaleX = width/ bounds.getWidth();
		double scaleY = height/bounds.getHeight();
		Graphics2D g2d = imgBuf.createGraphics();
		g2d.setTransform(AffineTransform.getScaleInstance(scaleX, scaleY));
		cam.draw.paint(g2d);
						
//        PixelGrabber grabber = 
//            new PixelGrabber(img, 0, 0, -1, -1, false);
//        
//        try {
//			if(!grabber.grabPixels()) {
//				throw new RuntimeException("Could not grab pixels.");
//			}
//		} catch (InterruptedException e) {
//			throw new RuntimeException(e.getMessage());
//		}
//        
//        int w = grabber.getWidth();
//        int h = grabber.getHeight();
//        int[] pixels = (int[]) grabber.getPixels();
		
		// Note: as long there is no efficient way to transfer the byte data from java to C++ ArgumentAdapter
		//		 the easiest way is to use array access style methods for copying.
		ByteImage outImg;
		if(outputArgs.size()>0 && outputArgs.Get(0).HasData()) {
			outImg = (ByteImage) outputArgs.Get(0);
		} else {
			outImg = new ByteImage(runtime);
			int pitch = (((3* 8 * width) + 31) / 32) * 4;
			outImg.SetHeight(height)
			  .SetWidth(width)
			  .SetChannels(3)
			  .CreateData();
			outImg.SetPitch(pitch);
		}

		
		WritableRaster raster = imgBuf.getRaster();
		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {
				for (int c = 0; c < 3; c++) {
					int val = raster.getSample(col, row, c);
					outImg.SetData(row, col, 2-c, (short) val);
				}
			}
		}		
		outputArgs.Set(0, outImg);
	}

	@Override
	public int GetpInVersion() {
		return 10000;
	}

	@Override
	public String GetAuthor() {
		return "JKU Linz";
	}

	@Override
	public String GetCopyright() {
		return "";
	}

	@Override
	public String GetDescription() {
		return "Simulates a camera by a user controlled Widget";
	}

	@Override
	public String GetName() {
		return ID_PIN_IO_CAPTURESIMCAM;
	}
	
	public JPanel getJPanel() {
		return cam;
	}
}
