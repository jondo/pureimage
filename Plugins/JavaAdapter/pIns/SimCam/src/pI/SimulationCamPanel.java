/* SimulationCamPanel.java
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

package pI;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputAdapter;

@SuppressWarnings("serial")
public class SimulationCamPanel extends JPanel {

	JPanel control;
	SimulationCam draw;
	
	public SimulationCamPanel(int width, int height, double scale) {
		
		int w = (int) (width*scale);
		int h = (int) (height*scale);
		
		control = new JPanel();
		draw = new SimulationCam();
		
		// set the canvas to a fixed size as this has an effect on
		// the generated image.
		draw.setPreferredSize(new Dimension(w,h));
		draw.setMinimumSize(new Dimension(w,h));
		draw.setMaximumSize(new Dimension(w,h));
		
		// Note: some Layout mechanisms do not consider the size settings
		//		 AFAIK only FlowLayout and BoxLayout do well
		BoxLayout boxLayout = new BoxLayout(this, BoxLayout.X_AXIS);
		setLayout(boxLayout);
		
		
		CookieMouseHandler cookieMouseHandler = new CookieMouseHandler();
		draw.addMouseListener(cookieMouseHandler);
		draw.addMouseMotionListener(cookieMouseHandler);
		
		List<Cookie> availableCookies = MyCookies.createCookies(scale);		
		control.setLayout(new BoxLayout(control, BoxLayout.Y_AXIS));
		createButtons(availableCookies);

		add(draw);
		add(control);
	}
	
	public void addItem(Cookie shape) {
		draw.add(shape);
	}
	
	private void createButtons(List<Cookie> availableCookies) {
		int iconWidth = 25;
		for (final Cookie cookie : availableCookies) {
			ImageIcon imageIcon = MyCookies.createIcon(cookie, iconWidth);
			JButton button = new JButton(imageIcon);
			button.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					Cookie copy = cookie.createCopy();
					copy.setPos(0,0);
					draw.add(copy);
					draw.repaint();
				}
			});
			
			control.add(button);
		}
	}	

	class CookieMouseHandler extends MouseInputAdapter {

		Point lastPosition = null;
		Cookie currentCookie = null;

		@Override
		public void mousePressed(MouseEvent e) {
			super.mousePressed(e);

			Point point = e.getPoint();
			currentCookie = draw.getCookieAtPos(point.x, point.y);
			lastPosition = point;
		}
		
		@Override
		public void mouseClicked(MouseEvent e) {
			super.mouseClicked(e);

			if(e.getButton() == MouseEvent.BUTTON1 && e.isControlDown()) {
				Point point = e.getPoint();
				Cookie cookie = draw.getCookieAtPos(point.x, point.y);
				draw.remove(cookie);
				draw.repaint();
				
			}			
		}

		@Override
		public void mouseDragged(MouseEvent e) {

			if ( currentCookie != null) {
				Point point = e.getPoint();
				int xdiff = point.x - lastPosition.x;
				int ydiff = point.y - lastPosition.y;

				currentCookie.shift(xdiff, ydiff);
				lastPosition = point;

				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						draw.repaint();
					}
				});
			}
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			super.mouseReleased(e);

			currentCookie = null;
			lastPosition = null;
		}
	}
}
