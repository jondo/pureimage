Java Plugins
============

Java plugins are adapted using a generic C++-Java-Adapter 
which makes Java plugins available as C++-Plugins.

Java plugins must derive from pI.DirectorPlugin (pI_java.jar) which uses of director feature 
to achieve bi-directional calls (C++<->Java) between Plugin and Runtime.

Plugins are looked up automatically as long they reside within a jar file located in libraries folder.
