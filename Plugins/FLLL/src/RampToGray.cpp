/* RampToGray.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <ArgumentChecks.hpp>
#include <Arguments.hpp>

#include "RampToGray.hpp"

namespace pI {
namespace pIns {


RampToGray::RampToGray(Runtime& runtime): Base(runtime) {

	StringSelection typesel(GetRuntime());
	typesel.SetName ("Ramp")
		   .SetDescription ("type of ramp")
		   .SetCount (1);
	typesel.CreateData();
	typesel.SetSymbols (0, "JET");
	typesel.SetIndex (0);
	_parameters.push_back (typesel);

	_act_params = _parameters;
	_input_args.push_back (ByteImage(runtime).SetName("Input image").SetDescription("Arbitrary single-page input image").SetChannels(3));
	_output_args.push_back (ByteImage(runtime).SetName("Output image").SetDescription("Grayscale output image").SetChannels(1));
	InitColourModel();
}

/*virtual*/ RampToGray::~RampToGray() {}

/*virtual*/ void RampToGray::_Initialize (const Arguments& parameters) {
	
	CheckParameters (parameters, _parameters);
	_act_params = _runtime.CopyArguments (parameters);
	InitColourModel (static_cast<ColourModel> (StringSelection(parameters[0]).GetIndex()));
}

/*virtual*/ void RampToGray::_Execute (Arguments& input_args, Arguments& output_args) {

	// validate input arguments
	CheckInputArguments (input_args, GetInputSignature());
	CheckOutputArguments (output_args, GetOutputSignature());
	
	ByteImage in(input_args[0]), out(output_args[0]);
	pI_bool do_create_data(pI_TRUE);
	if (out.HasData()) {
		if ((out.GetChannels() == 1) &&
			(out.GetWidth() == in.GetWidth()) &&
			(out.GetHeight() == in.GetHeight()))
			do_create_data = pI_FALSE;
	}
	if (do_create_data == pI_TRUE) {
		if (out.HasData()) {
			_runtime.GetCRuntime()->FreeArgumentData (
			 _runtime.GetCRuntime(),
			 output_args[0].get());
		}
		out.SetWidth (in.GetWidth());
		out.SetHeight (in.GetHeight());
		out.SetChannels (1);
		try {
			out.CreateData();
		} catch (exception::MemoryException) {
			throw exception::ExecutionException ("Failed to create grayscale image.");
		}
	}

	pI_byte r, g, b;
	pI_float rf, gf, bf;
	pI_size index1, index2;
	// Note: currently, this is done in ByteImage::CreateData()
	// out.SetPitch ((((8 * in.GetWidth()) + 31) / 32) * 4);
	out.SetPath (in.GetPath());
	for (pI_size h = 0; h < in.GetHeight(); ++h) {
		for (pI_size w = 0; w < in.GetWidth(); ++w) {
			r = in.GetData (h, w, 0), g = in.GetData (h, w, 1), b = in.GetData (h, w, 2);
			rf = static_cast<pI_float> (r) / 255.0f;
			gf = static_cast<pI_float> (g) / 255.0f;
			bf = static_cast<pI_float> (b) / 255.0f;
			// in case of gray value, keep it
			if ((r == g) && (r == b)) {
				out.SetData (h, w, 0, r);
				continue;
			}
			GetBestCandidates (rf, gf, bf, index1, index2);
			// check if best candidates are neighbours
			if (std::abs (static_cast<int> (index1) - static_cast<int> (index2)) > 1) {
				// if not, use best candidate
				out.SetData (h, w, 0, static_cast<pI_byte> ((static_cast<float> (index1) / static_cast<float> (_colour_map.size())) * 255.0f));
			} else {
				// otherwise, interpolate
				t_cc cc1(_colour_map[index1 < index2 ? index1 : index2]);
				t_cc cc2(_colour_map[index1 > index2 ? index1 : index2]);
				t_cc diff_cc(Difference (cc1.r, cc2.r), Difference (cc1.g, cc2.g), Difference (cc1.b, cc2.b));
				t_cc diff_p_cc2(Difference (cc2.r, rf), Difference (cc2.g, gf), Difference (cc2.b, bf));
				pI_float norm_diff(0.0f);
				if (diff_cc.r > 0.0)
					norm_diff += diff_p_cc2.r / diff_cc.r;
				if (diff_cc.g > 0.0)
					norm_diff += diff_p_cc2.g / diff_cc.g;
				if (diff_cc.b > 0.0)
					norm_diff += diff_p_cc2.b / diff_cc.b;
				norm_diff /= 3.0f;
				out.SetData (h, w, 0, static_cast<pI_byte> (((static_cast<float> (index1) + norm_diff) / static_cast<float> (_colour_map.size())) * 255.0f));
			}
			
		}
	}
}

void RampToGray::InitColourModel (ColourModel model /*= CM_Jet*/) {

	_colour_map.clear();
	switch (model) {
		case CM_Jet: {
			_colour_map.push_back (t_cc(         0,        0,   0.5625));
			_colour_map.push_back (t_cc(         0,        0,   0.6250));
			_colour_map.push_back (t_cc(         0,        0,   0.6875));
			_colour_map.push_back (t_cc(         0,        0,   0.7500));
			_colour_map.push_back (t_cc(         0,        0,   0.8125));
			_colour_map.push_back (t_cc(         0,        0,   0.8750));
			_colour_map.push_back (t_cc(         0,        0,   0.9375));
			_colour_map.push_back (t_cc(         0,        0,   1.0000));
			_colour_map.push_back (t_cc(         0,   0.0625,   1.0000));
			_colour_map.push_back (t_cc(         0,   0.1250,   1.0000));
			_colour_map.push_back (t_cc(         0,   0.1875,   1.0000));
			_colour_map.push_back (t_cc(         0,   0.2500,   1.0000));
			_colour_map.push_back (t_cc(         0,   0.3125,   1.0000));
			_colour_map.push_back (t_cc(         0,   0.3750,   1.0000));
			_colour_map.push_back (t_cc(         0,   0.4375,   1.0000));
			_colour_map.push_back (t_cc(         0,   0.5000,   1.0000));
			_colour_map.push_back (t_cc(         0,   0.5625,   1.0000));
			_colour_map.push_back (t_cc(         0,   0.6250,   1.0000));
			_colour_map.push_back (t_cc(         0,   0.6875,   1.0000));
			_colour_map.push_back (t_cc(         0,   0.7500,   1.0000));
			_colour_map.push_back (t_cc(         0,   0.8125,   1.0000));
			_colour_map.push_back (t_cc(         0,   0.8750,   1.0000));
			_colour_map.push_back (t_cc(         0,   0.9375,   1.0000));
			_colour_map.push_back (t_cc(         0,   1.0000,   1.0000));
			_colour_map.push_back (t_cc(    0.0625,   1.0000,   0.9375));
			_colour_map.push_back (t_cc(    0.1250,   1.0000,   0.8750));
			_colour_map.push_back (t_cc(    0.1875,   1.0000,   0.8125));
			_colour_map.push_back (t_cc(    0.2500,   1.0000,   0.7500));
			_colour_map.push_back (t_cc(    0.3125,   1.0000,   0.6875));
			_colour_map.push_back (t_cc(    0.3750,   1.0000,   0.6250));
			_colour_map.push_back (t_cc(    0.4375,   1.0000,   0.5625));
			_colour_map.push_back (t_cc(    0.5000,   1.0000,   0.5000));
			_colour_map.push_back (t_cc(    0.5625,   1.0000,   0.4375));
			_colour_map.push_back (t_cc(    0.6250,   1.0000,   0.3750));
			_colour_map.push_back (t_cc(    0.6875,   1.0000,   0.3125));
			_colour_map.push_back (t_cc(    0.7500,   1.0000,   0.2500));
			_colour_map.push_back (t_cc(    0.8125,   1.0000,   0.1875));
			_colour_map.push_back (t_cc(    0.8750,   1.0000,   0.1250));
			_colour_map.push_back (t_cc(    0.9375,   1.0000,   0.0625));
			_colour_map.push_back (t_cc(    1.0000,   1.0000,        0));
			_colour_map.push_back (t_cc(    1.0000,   0.9375,        0));
			_colour_map.push_back (t_cc(    1.0000,   0.8750,        0));
			_colour_map.push_back (t_cc(    1.0000,   0.8125,        0));
			_colour_map.push_back (t_cc(    1.0000,   0.7500,        0));
			_colour_map.push_back (t_cc(    1.0000,   0.6875,        0));
			_colour_map.push_back (t_cc(    1.0000,   0.6250,        0));
			_colour_map.push_back (t_cc(    1.0000,   0.5625,        0));
			_colour_map.push_back (t_cc(    1.0000,   0.5000,        0));
			_colour_map.push_back (t_cc(    1.0000,   0.4375,        0));
			_colour_map.push_back (t_cc(    1.0000,   0.3750,        0));
			_colour_map.push_back (t_cc(    1.0000,   0.3125,        0));
			_colour_map.push_back (t_cc(    1.0000,   0.2500,        0));
			_colour_map.push_back (t_cc(    1.0000,   0.1875,        0));
			_colour_map.push_back (t_cc(    1.0000,   0.1250,        0));
			_colour_map.push_back (t_cc(    1.0000,   0.0625,        0));
			_colour_map.push_back (t_cc(    1.0000,        0,        0));
			_colour_map.push_back (t_cc(    0.9375,        0,        0));
			_colour_map.push_back (t_cc(    0.8750,        0,        0));
			_colour_map.push_back (t_cc(    0.8125,        0,        0));
			_colour_map.push_back (t_cc(    0.7500,        0,        0));
			_colour_map.push_back (t_cc(    0.6875,        0,        0));
			_colour_map.push_back (t_cc(    0.6250,        0,        0));
			_colour_map.push_back (t_cc(    0.5625,        0,        0));
			_colour_map.push_back (t_cc(    0.5000,        0,        0));
			break;
		}
	} // switch (model)
} // void RampToGray::InitColourModel (ColourModel model /*= CM_Jet*/)

void RampToGray::GetBestCandidates (pI_float r, pI_float g, pI_float b, pI_size& index1, pI_size& index2) {

	pI_float score, top_score(0.0f);
	index1 = 0,	index2 = 0;

	for (pI_size lv = 1; lv < _colour_map.size(); ++lv) {
		t_cc cc(_colour_map[lv]);
		score = 1.0f - Difference (r, cc.r);
		score += 1.0f - Difference (g, cc.g);
		score += 1.0f - Difference (b, cc.b);
		if (score > top_score) {
			index2 = index1;
			index1 = lv;
			top_score = score;
		}
	}
} // void RampToGray::GetBestCandidates (pI_byte r, pI_byte g, pI_byte b, pI_size& index1, pI_size& index2)

} // namespace pIns
} // namespace pI
