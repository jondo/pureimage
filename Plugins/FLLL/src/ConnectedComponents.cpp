/* ConnectedComponents.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <ArgumentChecks.hpp>
#include <Arguments/ByteImage.hpp>
#include <Arguments/IntMatrix.hpp>
#include <Arguments/DoubleTable.hpp>

#include "ConnectedComponents.hpp"

#include "ConnComponents/ConnComponents.h"


namespace pI {
namespace pIns {


ConnectedComponents::ConnectedComponents (Runtime& runtime) : Base (runtime) {

    _input_args.push_back (ByteImage (runtime)
                           .SetName ("Input image")
                           .SetDescription ("Arbitrary single-page input image")
                           .SetChannels (1)
                           .SetConstraints ("channels == 1"));

    _output_args.push_back (IntMatrix (runtime)
                            .SetName ("Labels")
                            .SetDescription ("Matrix holding the components labels"));

    _output_args.push_back (DoubleTable (runtime)
                            .SetName ("Components")
                            .SetDescription ("Table holding area, center, and bounding rectangle of each component"));
}


/*virtual*/ ConnectedComponents::~ConnectedComponents() {
}


/*virtual*/ void ConnectedComponents::_Initialize (const Arguments& parameters) {

    CheckParameters (parameters, _parameters);
}


/*virtual*/ void ConnectedComponents::_Execute (Arguments& input_args, Arguments& output_args) {

    // Validates input arguments
    CheckInputArguments (input_args, GetInputSignature());
    CheckOutputArguments (output_args, GetOutputSignature());

    ByteImage input (input_args[0]);

    if (!input.HasData()) {
        return;
    }

    unsigned char* imgPtr = &(input.GetCPtr()->data.ByteImage->data[0][0][0]);

    int width  = input.GetWidth();
    int height = input.GetHeight();
    int bypp   = input.GetChannels();
    int stride = input.GetPitch();

    IntMatrix labels (output_args[0]);
    pI_bool do_create_data (pI_TRUE);

    if (labels.HasData()) {
        if (labels.GetCols() == width && labels.GetRows() == height) {
            do_create_data = pI_FALSE;
        }
    }

    if (do_create_data == pI_TRUE) {
        if (labels.HasData()) {
            _runtime.GetCRuntime()->FreeArgumentData (
                _runtime.GetCRuntime(), output_args[0].get());
        }

        labels.SetCols (width).SetRows (height);

		try {
			labels.CreateData();
		} catch (exception::MemoryException) {
            throw exception::ExecutionException ("Failed to create labels matrix.");
        }
    }

    int* lblPtr = &(labels.GetCPtr()->data.IntMatrix->data[0][0]);

    Boxes boxes = connComponents (imgPtr, width, height, bypp, stride, lblPtr);

    DoubleTable dt (output_args[1]);

    // Now that the number of boxes - i.e. the number of rows - is known,
    // creates the resulting DoubleTable 
    {
        if (dt.HasData()) {
            _runtime.GetCRuntime()->FreeArgumentData (
                _runtime.GetCRuntime(), output_args[1].get());
        }

        dt.SetCols (8);
        dt.SetRows (boxes.n);

        dt.SetHeader (0, "Label"); // Repeat yourself

        dt.SetHeader (1, "Center_X");
        dt.SetHeader (2, "Center_Y");
        dt.SetHeader (3, "Area");

        const pI_size Box = 4;
        dt.SetHeader (Box + 0, "Box_X");
        dt.SetHeader (Box + 1, "Box_Y");
        dt.SetHeader (Box + 2, "Box_Width");
        dt.SetHeader (Box + 3, "Box_Height");
    }

    for (size_t k = 0; k < std::min (boxes.n, (size_t) 256); ++k) {
        dt.SetData (k, 0, (double)k);
        dt.SetData (k, 1, boxes.box[k].getMeanX());
        dt.SetData (k, 2, boxes.box[k].getMeanY());
        dt.SetData (k, 3, boxes.box[k].getArea());
        dt.SetData (k, 4, boxes.box[k].getMinX());
        dt.SetData (k, 5, boxes.box[k].getMinY());
        dt.SetData (k, 6, boxes.box[k].getMaxX() - boxes.box[k].getMinX() + 1);
        dt.SetData (k, 7, boxes.box[k].getMaxY() - boxes.box[k].getMinY() + 1);
    }
}

} // namespace pIns
} // namespace pI
