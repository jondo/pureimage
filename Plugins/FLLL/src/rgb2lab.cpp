/* rgb2lab.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include "rgb2lab.h"

#include <cmath>


double f_lab (double t) {
    return (t > 0.008856 ? pow (t, 1. / 3.) : (7.787 * t + 16. / 116.));
}

/// Performs RGB -> L*a*b* conversion as descriped in the OpenCV manual, method cvCvtColor().
boost::tuples::tuple<double, double, double> rgb2lab (unsigned char rgb_r, unsigned char rgb_g, unsigned char rgb_b) {
    double r = (double) rgb_r / 255.;
    double g = (double) rgb_g / 255.;
    double b = (double) rgb_b / 255.;

    double x = 0.412453 * r + 0.357580 * g + 0.180423 * b;
    double y = 0.212671 * r + 0.715160 * g + 0.072169 * b;
    double z = 0.019334 * r + 0.119193 * g + 0.950227 * b;

    const double x_n = 0.950456;
    x /= x_n;

    const double z_n = 1.088754;
    z /= z_n;

    const double delta = 0.; //128.; 

    double l_ast = (y > 0.008856 ? (116. * pow (y, 1. / 3.) - 16.) : (903.3 * y));
    double a_ast = 500. * (f_lab (x) - f_lab (y)) + delta;
    double b_ast = 200. * (f_lab (y) - f_lab (z)) + delta;

    return boost::tuples::make_tuple (l_ast, a_ast, b_ast);
}


boost::tuples::tuple<double, double, double> rgb2lab (const boost::tuples::tuple<unsigned char, unsigned char, unsigned char>& rgb) {
    return rgb2lab (rgb.get<0>(), rgb.get<1>(), rgb.get<2>());
}


boost::tuples::tuple<double, double, double> rgb2lab (int hex) {
    return rgb2lab( (hex >> 16) & 0xFF, (hex >> 8) & 0xFF, hex & 0xFF);
}
