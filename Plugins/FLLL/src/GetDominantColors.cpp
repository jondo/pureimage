/* GetDominantColors.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include "GetDominantColors.hpp"

#include <vector>
#include <boost/tuple/tuple.hpp>

#include <cmath>

#include <ArgumentChecks.hpp>
#include <Arguments/ByteImage.hpp>
#include <Arguments/DoubleTable.hpp>
#include <Arguments/IntMatrix.hpp>

#include "rgb2lab.h"


namespace pI {
namespace pIns {

GetDominantColors::GetDominantColors (Runtime& runtime) : Base (runtime) {

    _input_args.push_back (ByteImage (GetRuntime())
                           .SetName ("inputImage")
                           .SetDescription ("Input RGB image")
                           .SetChannels (3)
                           .SetConstraints ("channels == 3"));

    _input_args.push_back (IntMatrix (GetRuntime())
                           .SetName ("mask")
                           .SetDescription ("Image mask"));

    _output_args.push_back (DoubleTable (GetRuntime())
                            .SetName ("dominantColors")
                            .SetDescription ("Dominant colors in form of L*a*b* triplets"));
}


/*virtual*/ GetDominantColors::~GetDominantColors() {
}


/*virtual*/ void GetDominantColors::_Initialize (const Arguments& parameters) {

    CheckParameters (parameters, _parameters);
    _parameters = _runtime.CopyArguments (parameters);
}


typedef boost::tuples::tuple<double, double, double> LabTriplet;

LabTriplet operator+ (const LabTriplet& a, const LabTriplet& b) {
    return LabTriplet (a.get<0>() + b.get<0>(), a.get<1>() + b.get<2>(), a.get<2>() + b.get<2>());
}

LabTriplet operator/ (const LabTriplet& a, double b) {
    return LabTriplet (a.get<0>() / b, a.get<1>() / b, a.get<2>() / b);
}

double sqr (double x) {
    return x * x;
}

double distance (const LabTriplet& a, const LabTriplet& b) {
    return sqrt (sqr (a.get<0>() - b.get<0>())
                 + sqr (a.get<1>() - b.get<1>())
                 + sqr (a.get<2>() - b.get<2>()));
}


/*virtual*/ void GetDominantColors::_Execute (Arguments& input_args, Arguments& output_args) {

    CheckInputArguments (input_args, GetInputSignature());
    CheckOutputArguments (output_args, GetOutputSignature());

    ByteImage in (input_args[0]);
    pI_size width  = in.GetWidth();
    pI_size height = in.GetHeight();

    IntMatrix mask (input_args[1]);

    if (! (mask.GetCols() == width && mask.GetRows() == height)) {
        throw pI::exception::ArgumentException ("Dimensions of input image and mask do not fit.");
    }

    // For each RGB pixel within an object, the corresponding  L*a*b* color is calculated
    // and added to the corresponding sum.

    std::vector<LabTriplet> sums_lab;
    std::vector<int> sums;

    sums_lab.push_back (LabTriplet (0., 0., 0.));
    sums.push_back (0); // Dummy entries for label 0

    for (pI_size j = 0; j < in.GetHeight(); ++j) {
		for (pI_size i = 0; i < in.GetWidth(); ++i) {

            // The label of the current object ...
            pI_int label = mask.GetData (j, i);

            // ... is ignored if 0, indicating background.
            if (label > 0) {
				// If this is the first time that this label is encountered, resizes the vectors accordingly
				// s.t. sums_lab[label] and sums[label] are always defined.
				if (sums.size() <= (pI_size) label) {
					sums_lab.resize (label + 1, LabTriplet (0., 0., 0.));
					sums.resize (label + 1, 0);
				}

				// HACK Yes, this is an BGR image .... how do we know???
				pI_byte in_r = in.GetData (j, i, 2);
				pI_byte in_g = in.GetData (j, i, 1);
				pI_byte in_b = in.GetData (j, i, 0);

				LabTriplet lab = rgb2lab (in_r, in_g, in_b);

				if (0. <= lab.get<0>() && lab.get<0>() <= 100. && -127. <= lab.get<1>() && lab.get<1>() <= 127. && -127. <= lab.get<2>() && lab.get<2>() <= 127.) {

					sums_lab[label] = sums_lab[label] + lab;
					++sums[label];
				}
			}
        }
    }

    pI_size n = sums.size();

    // TODO Poor man's clustering:
    // As we do not have a reasonable fast clustering here, we have to assume that the colour of each
    // object is almost uniform, i.e. for each object there is just one cluster in the L*a*b* cube.
    // For each object, we compute:
    // - the mean of all L*a*b* triplets, as an approximation of the single clusters' center
    // - the mean distance of all cluster points to the center, as an indicator how uniform the colour really is

    std::vector<LabTriplet> mean_lab (n);

    for (size_t k = 1; k < n; ++k) {
        mean_lab[k] = sums_lab[k] / (double) sums[k];
    }

    std::vector< double > dists (n, 0.);

    // HACK Yes, it is *not* elegant to compute all L*a*b* triplets here a second time.
    //      A way to do it more "plugin-like" would be to have a RGB2Lab plugin which computes
    //      L*a*b* values for *all* pixels; then, these loops would just contain look-up operations ...
    for (size_t j = 0; j < in.GetHeight(); ++j) {
        for (size_t i = 0; i < in.GetWidth(); ++i) {

            // The label of the current object ...
            pI_int label = mask.GetData (j, i);

            // ... is ignored if 0, indicating background.
            if (label == 0) {
                continue;
            }

            // HACK Yes, this is an BGR image .... how do we know???
            pI_byte in_r = in.GetData (j, i, 2);
            pI_byte in_g = in.GetData (j, i, 1);
            pI_byte in_b = in.GetData (j, i, 0);

            LabTriplet lab = rgb2lab (in_r, in_g, in_b);

            if (0. <= lab.get<0>() && lab.get<0>() <= 100. && -127. <= lab.get<1>() && lab.get<1>() <= 127. && -127. <= lab.get<2>() && lab.get<2>() <= 127.) {
                dists[label] += distance (lab, mean_lab[label]);
            }
        }
    }

    for (size_t k = 1; k < n; ++k) {
        dists[k] = dists[k] / (double) sums[k];
    }

    DoubleTable dt (output_args[0]);

    if (dt.HasData()) {
        _runtime.GetCRuntime()->FreeArgumentData (
            _runtime.GetCRuntime(), output_args[0].get());
    }

    dt.SetCols (4).SetRows (n - 1);

	try {
		dt.CreateData();
	} catch (exception::MemoryException) {
        throw exception::ExecutionException ("Failed to create dominantColors matrix.");
    }

    dt.SetHeader (0, "L*");
    dt.SetHeader (1, "a*");
    dt.SetHeader (2, "b*");
    dt.SetHeader (3, "mean distance");

    for (size_t k = 1; k < n; ++k) {
        dt.SetData (k - 1, 0, mean_lab[k].get<0>());
        dt.SetData (k - 1, 1, mean_lab[k].get<1>());
        dt.SetData (k - 1, 2, mean_lab[k].get<2>());
        dt.SetData (k - 1, 3, dists[k]);
    }
}

} // namespace pIns
} // namespace pI
