/* DoubleTableAdd.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <ArgumentChecks.hpp>

#include <Arguments.hpp>

#include "DoubleTableAdd.hpp"

namespace pI {
namespace pIns {

DoubleTableAdd::DoubleTableAdd(Runtime& runtime): Base(runtime), _lazy(pI_TRUE) {

	_parameters.push_back (
	 BoolValue(runtime).SetName("lazy")
					   .SetDescription("If true, the plugin accumulates samples until output generation is triggered, which also resets the table buffer, otherwise it just adds up two tables.")
					   .SetData(pI_TRUE));
	_output_args.push_back (
	 DoubleTable(runtime).SetName("Accumulated tables").SetDescription("Accumulated input tables"));

} // DoubleTableAdd::DoubleTableAdd(Runtime& runtime)

/*virtual*/ DoubleTableAdd::~DoubleTableAdd() {}

/*virtual*/ void DoubleTableAdd::_Initialize (const Arguments& parameters) {
	
	if (parameters.size() > 0) {
		CheckParameters (parameters, _parameters);
		_lazy = BoolValue (parameters[0]).GetData();
	} else {
		_lazy = pI_TRUE;
	}
	_input_args.clear();
	if (_lazy == pI_TRUE) {
		_input_args.push_back (
		 DoubleTable(GetRuntime()).SetName("Table").SetDescription("Table to accumulate"));
		_input_args.push_back (
		 BoolValue(GetRuntime()).SetName("Trigger output").SetDescription("If true, the accumulated DoubleTable is generated."));
	} else {
		_input_args.push_back (
		 DoubleTable(GetRuntime()).SetName("Table one").SetDescription("First input table"));
		_input_args.push_back (
		 DoubleTable(GetRuntime()).SetName("Table two").SetDescription("Second input table"));
	}
	_tables.clear();
} // /*virtual*/ void DoubleTableAdd::_Initialize (...)

/*virtual*/ void DoubleTableAdd::_Execute (Arguments& input_args, Arguments& output_args) {

	if (_lazy == pI_TRUE) {
		if (input_args.size() == 0) {
			output_args[0] = AddTables();
			return;
		}
		CheckInputArguments (input_args, _input_args);
		_tables.push_back (input_args[0]);
		if (BoolValue(input_args[1]).GetData() == pI_TRUE) {
			CheckOutputArguments (output_args, _output_args);
			// generate output
			output_args[0] = AddTables();
		}
	} else {
		CheckOutputArguments (output_args, _output_args);
		for (pI_size lv = 0; lv < input_args.size(); ++lv)
			CheckInputArgument (input_args[lv], _input_args[0]);
		_tables = input_args;
		output_args[0] = AddTables();
	}
} // /*virtual*/ void DoubleTableAdd::_Execute (...)

ArgumentPtr DoubleTableAdd::AddTables() {

	ArgumentPtr retval =
	 _runtime.CreateArgument (
	  T_pI_Argument_DoubleTable,
	  pI_FALSE,
	  0,
	  "Accumulated tables",
	  "Accumulated input tables");
	if (_tables.size() == 0)
		return retval;
	DoubleTable dt(retval);

	// calculate final table dimensions
	pI_size rows(0), cols(DoubleTable(_tables[0]).GetCols());
	for (pI_size lv = 0; lv < _tables.size(); ++lv) {
		rows += DoubleTable(_tables[lv]).GetRows();
	}
	dt.SetCols (cols), dt.SetRows (rows);
	dt.CreateData();
	// copy the labels
	for (pI_size lv = 0; lv < cols; ++lv) {
		dt.SetHeader (lv, DoubleTable(_tables[0]).GetHeader (lv));
	}
	// copy the data
	pI_size row(0);
	for (pI_size lv = 0; lv < _tables.size(); ++lv) {
		DoubleTable act(_tables[lv]);
		if (act.GetCols() != cols)
			throw exception::ExecutionException("DoubleTableAdd encounted table with inconsistent geometry.");
		for (pI_size r = 0; r < act.GetRows(); ++r) {
			for (pI_size c = 0; c < cols; ++c) {
				dt.SetData (row, c, act.GetData (r, c));
			}
			++row;
		}
	}
	// clear private sample copies
	_tables.clear();
	return retval;
} // ArgumentPtr DoubleTableAdd::AddTables()

} // namespace pIns
} // namespace pI
