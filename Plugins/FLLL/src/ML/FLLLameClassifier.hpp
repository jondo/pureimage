/* FLLLameClassifier.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FLLLAME_CLASSIFIER_HPP
#define FLLLAME_CLASSIFIER_HPP

#include <set>
#include <boost/shared_array.hpp>
#include <boost/bimap.hpp>

#include <PureImage.hpp>
#include "FLLLame/ClassifierFactory.hpp"


namespace pI {
namespace pIns {

class FLLLameClassifier: public pIn {

    typedef pIn Base;

public:

	FLLLameClassifier(Runtime& runtime);
	virtual ~FLLLameClassifier();

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(FLLLameClassifier)

    virtual const pI_int GetpInVersion() const {
        return 10000;
    }

	virtual const std::string GetAuthor() const {
        return "JKU Linz";
    }

	virtual const std::string GetDescription() const {
        return "pureImage FLLLame classifier adapter plugin.";
    }

    virtual const std::string GetName() const {
        return "FLLLame/Classifier";
    }

    virtual Arguments GetParameterSignature() const {
        return MEMBER_SIGNATURE(_parameters);
    }

    virtual Arguments GetInputSignature() const {
        return MEMBER_SIGNATURE(_input_args);
    }

    virtual Arguments GetOutputSignature() const {
        return MEMBER_SIGNATURE(_output_args);
    }

protected:

    virtual void _Initialize (const Arguments& parameters);

    virtual void _Execute (Arguments& input_args, Arguments& output_args);

	pI_size GetLabelCount (ArgumentPtr arg) const;
	boost::shared_array<pI_int> GetLabelsAsIntArray (ArgumentPtr arg);
	void ConfidencesToLabels (
	 boost::shared_array<pI_double> confidences,
	 pI_size rows,
	 pI::ArgumentPtr& arg) const;

	Arguments _parameters, _input_args, _output_args;
	AbstractClassifier* _classifier;
	pI_bool _trained;
	ClassifierFactory::ClassifierFactory* _cf;
	
	typedef boost::bimap<std::string, pI_int> t_slm;
	t_slm _string_label_map;
	pI_int _label_count;

	std::set<pI_int> _int_label_map;

	pI_size _cl_index;

}; // class FLLLameClassifier: public pIn

} // namespace pIns
} // namespace pI

#endif // FLLLAME_CLASSIFIER_HPP
