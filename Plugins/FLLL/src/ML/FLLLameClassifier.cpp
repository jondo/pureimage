/* FLLLameClassifier.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <ArgumentChecks.hpp>
#include <Arguments.hpp>

#include "FLLLameClassifier.hpp"

namespace pI {
namespace pIns {


FLLLameClassifier::FLLLameClassifier(Runtime& runtime):
 Base(runtime),
 _classifier(0),
 _trained(pI_FALSE),
 _label_count(0),
 _cl_index(0) {

	static ClassifierFactory::ClassifierFactory cf;
	pI_size count(cf.GetClassifierCount());
	if (count == 0)
		throw exception::PluginCreationException(
		 "Cannot instantiate FLLLameClassifier plugin due to missing FLLLame classifiers");
	_cf = &cf;

	pI::StringSelection classifiers(runtime);
	classifiers.SetCount (count);
	classifiers.SetName ("Classifier");
	classifiers.SetDescription ("Classifier model");
	classifiers.CreateData();
	for (pI_size lv = 0; lv < count; ++lv) {
		classifiers.SetSymbols (lv, (pI_str) cf.GetClassifierId (lv));
	}
	classifiers.SetIndex (0);
	_parameters.push_back (classifiers);
	StringSelection ssel(runtime);
	ssel.SetName ("type")
		.SetDescription ("Specific type of the labels: double is for regression, int, string and bool for classification")
		.SetCount (4);
	ssel.CreateData();
	ssel.SetSymbols (0, "double");
	ssel.SetSymbols (1, "int");
	ssel.SetSymbols (2, "string");
	ssel.SetSymbols (3, "bool");
	ssel.SetIndex (2); // default to string labels
	_parameters.push_back (ssel);
	_input_args.push_back (
	 pI::DoubleTable(runtime).SetName ("samples")
							 .SetDescription ("sample data")
							 .SetReadOnly (pI_TRUE));
	_input_args.push_back (
	 pI::StringArray(runtime).SetName ("labels")
							 .SetDescription ("sample labels")
							 .SetReadOnly (pI_FALSE));
	_input_args.push_back (
	 pI::BoolValue(runtime).SetName ("predict")
						   .SetDescription ("prediction / training mode")
						   .SetData (pI_FALSE)
						   .SetReadOnly (pI_TRUE));
	_input_args.push_back (
	 pI::StringValue(runtime).SetName ("options")
							 .SetDescription ("classifier options")
							 .SetReadOnly (pI_FALSE));
} // FLLLameClassifier::FLLLameClassifier(...)

/*virtual*/ FLLLameClassifier::~FLLLameClassifier() {}

/*virtual*/ void FLLLameClassifier::_Initialize (const Arguments& parameters) {

	CheckParameters (parameters, _parameters, 1);
	_cl_index = pI::StringSelection(parameters[0]).GetIndex();
	// adapt labels according to given label parameter
	if (parameters.size() > 1) {
		CheckParameter (parameters[1], _parameters[1]);
		switch (pI::StringSelection(parameters[1]).GetIndex()) {
			case 0: _input_args[1] = (
					 pI::DoubleArray(GetRuntime()).SetName ("labels")
												   .SetDescription ("sample labels")
											       .SetReadOnly (pI_FALSE));
					_label_count = 1;
					break;
			case 1: _input_args[1] = (
					 pI::IntArray(GetRuntime()).SetName ("labels")
												.SetDescription ("sample labels")
											    .SetReadOnly (pI_FALSE));
					_label_count = 0;
					break;
			case 3: _input_args[1] = (
					 pI::BoolArray(GetRuntime()).SetName("labels")
												 .SetDescription("sample labels")
											     .SetReadOnly(pI_FALSE));
					_label_count = 2;
					break;
			case 2:
			default: _input_args[1] = (
					 pI::StringArray(GetRuntime()).SetName ("labels")
												   .SetDescription ("sample labels")
											       .SetReadOnly (pI_FALSE));
					_label_count = 0;
					break;
		}
	}
	// adapt options string
	if (parameters.size() > 2) {
		CheckParameter (parameters[2], _parameters[2]);
		pI::StringValue(parameters[2]).SetData (
		 (pI_str) _cf->GetClassifierDefaultParameters (_cl_index));
	}
} // /*virtual*/ void FLLLameClassifier::_Initialize (...)

/*virtual*/ void FLLLameClassifier::_Execute (Arguments& input_args, Arguments& output_args) {

	// validate input and output arguments
	CheckInputArgument (input_args[0], GetInputSignature()[0]);
	CheckArgumentType (input_args[1].get(), GetInputSignature()[1].get());
	CheckInputArgument (input_args[2], GetInputSignature()[2]);
	pI_bool do_predict(pI::BoolValue(input_args[2]).GetData());
	pI_str options(0);
	if (_trained == pI_FALSE) {
		if (do_predict == pI_TRUE)
			throw exception::ExecutionException("Cannot predict with un-trained classifier");
		if (input_args.size() > 3) {
			CheckArgumentType (input_args[3].get(), GetInputSignature()[3].get());
			if (pI::StringValue(input_args[3]).HasData())
				options = pI::StringValue(input_args[3]).GetData();
		}
		// create new classifier
		AbstractClassifier* cl(const_cast<AbstractClassifier*> (_cf->Create (_cl_index, options)));
		if (cl == 0)
			throw exception::InitializationException("Instantiation of FLLLame classifier failed.");
		if (_classifier != 0)
			_cf->Destroy (_classifier);
		_classifier = cl;
		// clear old labels
		_label_count = 0;
		_int_label_map.clear();
		_string_label_map.clear();
		_trained = pI_FALSE;
	}

	// prepare samples
	pI::DoubleTable dt(input_args[0]);
	boost::shared_array<pI_double> samples(new double[dt.GetRows() * dt.GetCols()]);
	pI_size index(0);
	for (pI_size r = 0; r < dt.GetRows(); ++r)
		for (pI_size c = 0; c < dt.GetCols(); ++c)
			samples[index++] = dt.GetData (r, c);

	if (_trained == pI_FALSE) {
		// prepare labels
		boost::shared_array<pI_int> labels(GetLabelsAsIntArray (input_args[1]));
		_classifier->BatchLearn (samples.get(), dt.GetRows(), dt.GetCols(), labels.get());
		_trained = pI_TRUE;
	} else if (do_predict == pI_FALSE) {
		// prepare labels
		boost::shared_array<pI_int> labels(GetLabelsAsIntArray (input_args[1]));
		_classifier->IncrementalAdapt (samples.get(), dt.GetRows(), dt.GetCols(), labels.get());
	} else {
		// prepare confidence matrix
		boost::shared_array<pI_double> confidences(new double[dt.GetRows() * dt.GetCols() * _label_count]);
		_classifier->Classify (samples.get(), dt.GetRows(), dt.GetCols(), confidences.get());
		// convert confidences to output labels
		ConfidencesToLabels (confidences, dt.GetRows(), input_args[1]);
	}
} // /*virtual*/ void FLLLameClassifier::_Execute (...)

pI_size FLLLameClassifier::GetLabelCount (ArgumentPtr arg) const {
	pI_size label_count(0);
	switch (arg->signature.type) {
		case T_pI_Argument_DoubleArray: label_count = DoubleArray(arg).GetCount(); break;
		case T_pI_Argument_IntArray: label_count = IntArray(arg).GetCount(); break;
		case T_pI_Argument_StringArray: label_count = StringArray(arg).GetCount(); break;
		case T_pI_Argument_BoolArray: label_count = BoolArray(arg).GetCount(); break;
	}
	return label_count;
} // pI_size FLLLameClassifier::GetLabelCount (ArgumentPtr arg) const

boost::shared_array<pI_int> FLLLameClassifier::GetLabelsAsIntArray (ArgumentPtr arg) {
	
	pI_size label_count(GetLabelCount (arg));
	boost::shared_array<pI_int> retval(new pI_int[label_count]);
	
	switch (arg->signature.type) {
		case T_pI_Argument_DoubleArray: {
			DoubleArray da(arg);
			if (!da.HasData())
				da.CreateData();
			for (pI_size lv = 0; lv < label_count; ++lv) {
				retval[lv] = static_cast<pI_int> (da.GetData (lv));
			}
			break;
		}
		case T_pI_Argument_IntArray: {
			IntArray ia(arg);
			if (!ia.HasData())
				ia.CreateData();
			for (pI_size lv = 0; lv < label_count; ++lv) {
				retval[lv] = ia.GetData (lv);
				_int_label_map.insert (ia.GetData (lv));
			}
			_label_count = _int_label_map.size();
			break;
		}
		case T_pI_Argument_StringArray: {
			StringArray sa(arg);
			if (!sa.HasData())
				sa.CreateData();
			for (pI_size lv = 0; lv < label_count; ++lv) {
				pI_str label_string(sa.GetData (lv));
				pI_int ilabel;
				t_slm::left_const_iterator li(_string_label_map.left.find (label_string));
				if (li == _string_label_map.left.end()) {
					_string_label_map.left.insert (
					 t_slm::left_value_type (std::string(label_string), _label_count));
					ilabel = _label_count++;
				} else {
					ilabel = li->second;
				}
				retval[lv] = ilabel;
			}
			break;
		}
		case T_pI_Argument_BoolArray: {
			BoolArray ba(arg);
			if (!ba.HasData())
				ba.CreateData();
			for (pI_size lv = 0; lv < label_count; ++lv) {
				retval[lv] = ba.GetData (lv);
			}
			break;
		}
	} // switch (_label_type)
	return retval;
} // boost::shared_array<pI_int> FLLLameClassifier::GetLabelsAsDoubleCvMat (...)

void FLLLameClassifier::ConfidencesToLabels (
 boost::shared_array<pI_double> confidences,
 pI_size rows,
 pI::ArgumentPtr& arg) const {

	switch (arg->signature.type) {
		case T_pI_Argument_DoubleArray: {
			DoubleArray da(arg);
			if (!da.HasData() || (da.GetCount() != rows)) {
				_runtime.GetCRuntime()->FreeArgumentData (
				 _runtime.GetCRuntime(),
				 arg.get());
				da.SetCount (rows);
				da.CreateData();
			}
			for (pI_size lv = 0; lv < rows; ++lv) {
				da.SetData (lv, confidences[lv]);
			}
			return;
		}
		case T_pI_Argument_IntArray: {
			IntArray ia(arg);
			if (!ia.HasData() || (ia.GetCount() != rows)) {
				_runtime.GetCRuntime()->FreeArgumentData (
				 _runtime.GetCRuntime(),
				 arg.get());
				ia.SetCount (rows);
				ia.CreateData();
			}
			for (pI_size lv = 0; lv < rows; ++lv) {
				pI_int max_index(0);
				pI_double max_confidence(confidences[lv * _label_count]);
				for (pI_size lv = 1; lv < _label_count; ++lv) {
					if (confidences[lv * _label_count + lv] > max_confidence) {
						max_confidence = confidences[lv * _label_count + lv];
						max_index = lv;
					}
					ia.SetData (lv, max_index);
				}
			}
			return;
		}
		case T_pI_Argument_StringArray: {
			StringArray sa(arg);
			if (!sa.HasData() || (sa.GetCount() != rows)) {
				_runtime.GetCRuntime()->FreeArgumentData (
				 _runtime.GetCRuntime(),
				 arg.get());
				sa.SetCount (rows);
				sa.CreateData();
			}
			for (pI_size lv = 0; lv < rows; ++lv) {
				pI_int max_index(0);
				pI_double max_confidence(confidences[lv * _label_count]);
				for (pI_size lv2 = 1; lv2 < _label_count; ++lv2) {
					if (confidences[lv * _label_count + lv2] > max_confidence) {
						max_confidence = confidences[lv * _label_count + lv2];
						max_index = lv2;
					}
				}
				t_slm::right_const_iterator ri(_string_label_map.right.find (static_cast<pI_int> (max_index)));
				sa.SetData (lv, const_cast<pI_str> (ri->second.c_str()));
			}
			return;
		}
		case T_pI_Argument_BoolArray: {
			BoolArray ba(arg);
			if (!ba.HasData() || (ba.GetCount() != rows)) {
				_runtime.GetCRuntime()->FreeArgumentData (
				 _runtime.GetCRuntime(),
				 arg.get());
				ba.SetCount (rows);
				ba.CreateData();
			}
			for (pI_size lv = 0; lv < rows * 2; lv += 2) {
				ba.SetData (lv, confidences[lv] > confidences[lv + 1] ? pI_FALSE : pI_TRUE);
			}
			return;
		}
	} // switch (_label_type)
} // void FLLLameClassifier::ConfidencesToLabels (...) const

} // namespace pIns
} // namespace pI
