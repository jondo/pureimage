/* ClassifierFactory.h
 * Copyright (C) Johannes Kepler Universitaet Linz,
 *               Institut f. Wissensbasierte Mathematische Systeme, 2006-2008.
 */

#ifndef CLASSIFIERFACTORY_H
#define CLASSIFIERFACTORY_H

#include <vector>
#include <Poco/SharedLibrary.h>

//#include "classifier.h"
#include "AbstractClassifier.hpp"

namespace ClassifierFactory {

/** ClassifierFactory tries to simplify the creation of classifiers.
    During construction, the factory searches either a given or a default 
    path for available classifier types and generates one default instance 
    for each type to allow faster instance creation. */
class ClassifierFactory {

    public:
    
        /** Default constructor.
            Searches given path (or default path) for classifier types
            and keeps one instance of each to allow fast creation.
            @param path_to_classifier_folder Absolute path to folder to search
                                             for useable classifier types. */
        ClassifierFactory(const char* path_to_classifier_folder = 0);
        
        /// Destructor. Frees internal handles to classifiers.
        ~ClassifierFactory();
        
        /** Method to spawn a classifier.
            @param index Index of classifier to load (< GetClassifierCount())
            @param param_set Parameterset to use for classifier.
            @return Classifier instance in case of creation success, 0 otherwise. */
        const AbstractClassifier* Create (const size_t index, const char* param_set = 0) const;
        
        /** Method to spawn a classifier.
            @param Id of classifier to spawn
            @param param_set Parameterset to use for classifier.
            @return Classifier instance in case of creation success, 0 otherwise. */
        const AbstractClassifier* Create (const char* id, const char* param_set = 0) const;
    
        /** Static method to destroy a classifier instance.
            @param cl The classifier to destroy. */
        static void Destroy (const AbstractClassifier* cl);
    
        /** Method to get the number of registered classifier types.
            @return Count of available classifier types. */
        const size_t GetClassifierCount() const;
        
		/** Method to retrieve the description of the classifier with given index.
			@param index index of classifier to retrieve description for.
			@return description of classifier instance with given index. */
		const char* GetClassifierDescription (const size_t index) const;

        /** Method to retrieve the id of a registered classifier type.
            @param index Index of classifier type to get id for 
                         (< GetClassifierCount())
            @return Id of classifier in case of success, 0 otherwise. */
        const char* GetClassifierId (const size_t index) const;
    
		/** Method to retrieve the default parameter grid of the 
			classifier with given index.
			@param index index of classifier to retrieve parameter grid for.
			@return default parameter grid of classifier instance 
					with given index as string. */
		const char* GetClassifierDefaultParameterGrid (const size_t index) const;
		
		/** Method to retrieve the default parameters of the 
			classifier with given index.
			@param index index of classifier to retrieve default parameters for.
			@return default parameters of classifier instance 
					with given index as string. */
		const char* GetClassifierDefaultParameters (const size_t index) const;

        /** Method to retrieve full path to classifier type dll.
            @param index Index of classifier type to get path for
                         (< GetClassifierCount())
            @return Path to classifier dll in case of success, 0 otherwise. */
        const char* GetClassifierPath (const size_t index) const;
        
        /** Method to retrieve full path to classifier type dll.
            @param Classifier id to get path for
            @return Path to classifier dll in case of success, 0 otherwise. */
        const char* GetClassifierPath (const char* id) const;

		/** Method to retrieve the version of the classifier with given index.
			@param index index of classifier to retrieve version for.
			@return version of classifier instance with given index as integer. */
		const size_t GetClassifierVersion (const size_t index) const;

        /** Method to check if a classifier type is available.
            @param id Id of classifier type to check availability.
            @return true if classifier type is available, false otherwise. */
        const bool IsAvailable (const char* id) const;
        
        /** Method to get an instance based on already learned 
            and saved classifier. The classifier file is tested against
            all available classifier types, only if file and type are
            compatible an instance is returned.
            @param saved_classifier Path to learned classifier.
            @return classifier instance in case of success, false otherwise. */
        const AbstractClassifier* Load (const char* saved_classifier) const;
    
    private:
    
        /// method to get a classifier instance per given id.
        const AbstractClassifier* FindClassifier (const char* id) const;
    
        struct ClassifierEntry {
        
            ClassifierEntry(): dll_handle(0), instance(0) {}
            
            Poco::SharedLibrary* dll_handle;
            AbstractClassifier* instance;
        };
    
        std::vector<ClassifierEntry> _classifiers;
		std::vector<ClassifierEntry> _other_dlls;
		std::string _classifier_path;
		std::vector<std::string> _classifier_paths;

}; // class ClassifierFactory

}; // namespaceClassifierFactory

#endif // CLASSIFIERFACTORY_H
