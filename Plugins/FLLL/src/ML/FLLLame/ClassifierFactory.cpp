/* ClassifierFactory.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <Poco/Path.h>
#include <Poco/File.h>
#include <Poco/DirectoryIterator.h>

#include <iostream>
#include <fstream>

#include "ClassifierFactory.hpp"


namespace ClassifierFactory {

typedef AbstractClassifier*(*Classifier_create_ptr)();

ClassifierFactory::ClassifierFactory(const char* path_to_classifier_folder):
_classifier_path(path_to_classifier_folder != 0 ? path_to_classifier_folder : "") {
    
	if (_classifier_path.size() == 0)
		_classifier_path = Poco::Path::current() + "//classifier";
	Poco::DirectoryIterator it(_classifier_path);
	Poco::DirectoryIterator end;
	std::string ext = Poco::SharedLibrary::suffix();
	ext = ext.substr(ext.find('.')+1);
	while (it != end) {
		if (it->isFile()) {
			Poco::Path p(it.path());
			if (p.getExtension() == ext) {
				// dll found
				_classifier_paths.push_back (p.toString (Poco::Path::PATH_GUESS));
			}
		}
		++it;
	}
    
    for (size_t lv = 0; lv < _classifier_paths.size(); lv++) {
		std::string& path(_classifier_paths[lv]);

		Poco::SharedLibrary* dll(0);
		try {
			dll = new Poco::SharedLibrary(path);
		} catch (...) {
			// must be already opened or sth. similar
			_classifier_paths.erase (_classifier_paths.begin() + lv);
			--lv;
			continue;
		}

        ClassifierEntry ce;
        ce.dll_handle = dll;
		bool is_classifier(true);
		if (dll->hasSymbol ("CreateClassifier")) {
			Classifier_create_ptr cptr((Classifier_create_ptr) dll->getSymbol ("CreateClassifier"));
			if (cptr) {
				ce.instance = cptr();
				_classifiers.push_back (ce);
			}
		} else {
			/*	obviously this dynamic lib does not hold a classifier;
				on the other hand it is there for some reason - with a 
				high probabilty it is needed by a classifier. In consequence,
				the dll must not be unloaded at this time, but on destruction -->
				book-keeping.
				Worst case scenario: the dll is not needed at all - in this case
				to load and store the dll will just consume memory. */
			_other_dlls.push_back (ce);
			_classifier_paths.erase (_classifier_paths.begin() + lv);
			--lv;
		}
    } // for (size_t lv = 0; lv < file_count; lv++)
}

ClassifierFactory::~ClassifierFactory() {
    
    size_t lv;
    for (lv = 0; lv < _classifiers.size(); lv++) {
        // first delete instance
        Destroy (_classifiers[lv].instance);
        // afterwards delete handle
        delete _classifiers[lv].dll_handle;
    }
	for (lv = 0; lv < _other_dlls.size(); ++lv) {
		delete _other_dlls[lv].dll_handle;
	}
}

const AbstractClassifier* ClassifierFactory::Create (const size_t index, const char* param_set) const {

    if (index >= _classifiers.size())
        return 0;
    const AbstractClassifier* preset(_classifiers[index].instance);
    if (preset == 0)
        return 0;
    return preset->Create (param_set);
}

const AbstractClassifier* ClassifierFactory::Create (const char* id, const char* param_set) const {
    
    const AbstractClassifier* preset(FindClassifier (id));
    if (preset == 0)
        return 0;
    return preset->Create (param_set);
}

void ClassifierFactory::Destroy (const AbstractClassifier* cl) {
    
    delete cl;
}

const size_t ClassifierFactory::GetClassifierCount() const {

    return _classifiers.size();
}

const char* ClassifierFactory::GetClassifierDescription (const size_t index) const {

	if (index >= _classifiers.size())
        return 0;
	return _classifiers[index].instance->About();
}

const char* ClassifierFactory::GetClassifierId (const size_t index) const {
    
    if (index >= _classifiers.size())
        return 0;
    const AbstractClassifier* preset(_classifiers[index].instance);
    if (preset == 0)
        return 0;
    return preset->GetId();
}

const char* ClassifierFactory::GetClassifierDefaultParameterGrid (const size_t index) const {

	if (index >= _classifiers.size())
        return 0;
	return _classifiers[index].instance->GetDefaultParameterGrid();
}

const char* ClassifierFactory::GetClassifierDefaultParameters (const size_t index) const {

	if (index >= _classifiers.size())
        return 0;
	return _classifiers[index].instance->GetParameters();
}

const char* ClassifierFactory::GetClassifierPath (const size_t index) const {
 
    if (index >= _classifier_paths.size())
        return 0;
    return _classifier_paths[index].c_str();
}

const size_t ClassifierFactory::GetClassifierVersion (const size_t index) const {
	
	if (index >= _classifier_paths.size())
        return 0;
	return static_cast<size_t> (_classifiers[index].instance->GetVersion());
}

const char* ClassifierFactory::GetClassifierPath (const char* id) const {

	const std::string given_id(id);
    for (size_t lv = 0; lv < _classifiers.size(); lv++) {
        AbstractClassifier* instance(_classifiers[lv].instance);
        if (instance) {
            const std::string classifier_id(instance->GetId());
            if (classifier_id == given_id) {
                return GetClassifierPath (lv);
            }
        }
    }
    return 0;
}

const bool ClassifierFactory::IsAvailable (const char* id) const {
    
    return (FindClassifier (id) != 0);
}

const AbstractClassifier* ClassifierFactory::Load (const char* saved_classifier) const {
    
    std::ifstream is(saved_classifier);
    if (is.is_open()) {
        char cl_id[1024];
        unsigned cl_version(0u);
        is >> cl_id;
        is >> cl_version;
        for (size_t lv = 0; lv < _classifiers.size(); lv++) {
            const AbstractClassifier* cl(_classifiers[lv].instance);
            if (cl) {
                if (std::string(cl->GetId()).compare(cl_id) == 0) {
                    if (cl->GetVersion() == cl_version) {
                        // saved classifier seems compatible with available classifier
                        AbstractClassifier* temp(cl->Create());
						if (temp) {
							if (temp->Load (saved_classifier) == AbstractClassifier::NoError) {
								AbstractClassifier* retval(cl->Create (temp->GetParameters()));
								Destroy (temp);
								temp = 0;
								if (retval->Load (saved_classifier) == AbstractClassifier::NoError)
									return retval;
								Destroy (retval);
							}
							// failed to load classifier, delete instance and continue search
							Destroy (temp);
						}
                    }
                }
            }
        }
    }
    return 0;
}

const AbstractClassifier* ClassifierFactory::FindClassifier (const char* id) const {
    
    const std::string given_id(id);
    for (size_t lv = 0; lv < _classifiers.size(); lv++) {
        AbstractClassifier* instance(_classifiers[lv].instance);
        if (instance) {
            const std::string classifier_id(instance->GetId());
            if (classifier_id == given_id)
                return instance;
        }
    }
    return 0;
}

}; // ClassifierFactory
