/* AbstractClassifier.h
 * Copyright (C) Johannes Kepler Universitaet Linz,
 *               Institut f. Wissensbasierte Mathematische Systeme, 2006-2008.
 */

#ifndef ABSTRACT_CLASSIFIER_H
#define ABSTRACT_CLASSIFIER_H

#include "BuildSharedLibrary.h"

template <typename T>
class DefaultDelete: public T
{
public:

    void operator delete(void* p)
    {
        ::operator delete(p);
    }
};


/** AbstractClassifier is another classifier interface to maximize binary compatibilty.
    Therefor, all datatypes in method-interfaces are simple c datatypes. */
class AbstractClassifier
{

public:

    enum ClassifierError
    {
        NoError                                             =  0,

        ArgumentError                                       =  8,       // group
        ArgErrorDifferentRowLengths                         =  9,       // bl, ia, cl
        ArgErrorLabelCountMismatch                          = 10,       // bl, ia
        ArgErrorUnreasonableTrainingData                    = 11,       // bl
        ArgErrorFeatureSizeMismatch                         = 12,       // cl
        ArgErrorConfidenceArrayMismatch                     = 13,       // cl
        ArgErrorEmptyFileName                               = 14,       // load, save

        OperationError                                      = 16,       // group
        OperationErrorUntrainedClassifier                   = 17,       // ia, cl, save
        OperationErrorNotSupported                          = 18,       // ia

        FileIOError                                         = 32,       // group
        FileIOErrorWriteFailed                              = 33,       // save
        FileIOErrorFileNotFound                             = 34,       // load
        FileIOErrorFileCorrupted                            = 35,       // load

        DeserializationError                                = 64,       // group
        DeserializationErrorVersionMismatch                 = 65,       // load
        DeserializationErrorTypeMismatch                    = 66,       // load

        UnknownError                                        = 65536
    };

    /** Default constructor.
        Assigns default values for the parameters in the specific deduced
        classifiers (stored as class variables). */
    AbstractClassifier() {}

    /** Constructor which initializes the classifier with specific parameter values.
        @param parameterset ParameterSet as string to initialize Classifier with. */
    AbstractClassifier(const char*) {}

    /** Method to create a new classifier with given parameterset.
        @param parameterset ParameterSet as string to initialize Classifier with.
        @return usable Classifier instance. Callee has ownership. */
    virtual AbstractClassifier* CALL Create(const char* parameterset = 0) const = 0;

#ifndef __GNUC__
    /// dtor. (maybe add some destroy method another time?)
    virtual ~AbstractClassifier() = 0 {};
#else
    virtual ~AbstractClassifier() {};
#endif /* __GNUC__ */

    /// Method to delete actual instance
    virtual void CALL Destroy() = 0;

    /** Method to get an id unique to classifier type.
        @return classifier id. */
    virtual const char* CALL GetId() const = 0;

    /** Method to retrieve the actual version of the classifier.
        @return version of classifier as unsigned integer. */
    virtual const unsigned CALL GetVersion() const = 0;

    /** Method to return parameter set of instance (as string).
        The parameter set will look like: "prunelevel = 1; depth=4.56;".
        @return parameter set of instance as string. */
    virtual const char* const CALL GetParameters() const = 0;

    /** Method to return a range of usable parameters
        for a new classifier instance as string.
        The default parameter grid will look like: "prunelevel = [0,6,4];".
        The bracketed numbers specify the lowest value (double),
        the highest value (double), and the number of values (int).
        @return default parameter grid as string. */
    virtual const char* CALL GetDefaultParameterGrid() const = 0;

    /** Performs batch (offline) training of this classifier.
        @note For an instance-based classifier, this method performs
              a selection of training instances.
        @param sample_matrix a matrix with double values containing all
                             features (=columns) from all training objects (=rows).
        @param cols column count of the sample matrix
        @param rows row count of the sample matrix
        @param label_vector an integer array whose entries are the different
                            class labels to which each row in the
                            training_matrix belongs. The label vector
                            is supposed to contain <em>rows</em> elements.
        @return NoError in case of success, error code otherwise. */
    virtual const ClassifierError CALL BatchLearn(
        const double* const sample_matrix,
        const unsigned rows,
        const unsigned cols,
        const int* label_vector) = 0;

    /** Performs incremental (online) adaptation of this classifier.
        @note For an instance-based classifier, this method
              performs an update of the selection of training instances.
        @param sample_matrix a matrix with double values containing all
                             features (=columns) from all training objects (=rows).
        @param cols column count of the sample matrix
        @param rows row count of the sample matrix
        @param label_vector an integer array whose entries are the different
                            class labels to which each row in the
                            training_matrix belongs. The label vector
                            is supposed to contain <em>rows</em> elements.
        @return NoError in case of success, error code otherwise. */
    virtual const ClassifierError CALL IncrementalAdapt(
        const double* const sample_matrix,
        const unsigned rows,
        const unsigned cols,
        const int* label_vector) = 0;

    /** Method to classifiy new samples.
        @param sample_matrix A matrix encoded in a row-wise array with double values
                             containing all features (=columns) from all new
                             objects (=rows) which should be classified.
        @param matrix_rows Row count of the sample matrix.
        @param matrix_cols Column count of the sample matrix.
        @param confidences OUT Pre-allocated array to hold calculated confidences.
                           Must hold matrix_rows * label_count elements (label_count
                           specifies the number of different labels).
                           Note: In case of labels 0, 1, 3 label_count must be 4.
        @return NoError in case of success, error code otherwise. */
    virtual const ClassifierError CALL Classify(
        const double* const sample_matrix,
        const unsigned matrix_rows,
        const unsigned matrix_cols,
        double* confidences) = 0;

    /** Prints useful information about the instanced classifier.
        @return a string containing information about the classifier. */
    virtual const char* const CALL About() const = 0;

    /** Saves the classifier to a file with the given name.
        @param filename the filename where the classifier should be stored.
        @return NoError in case of success, error code otherwise. */
    virtual const ClassifierError CALL Save(const char* file_name) const = 0;

    /** Loads this classifier from a file with the given name.
        @param filename the filename where the classifier
                        preset should be loaded from.
        @return NoError in case of success, error code otherwise. */
    virtual const ClassifierError CALL Load(const char* file_name) = 0;

    /** Method to register another feature name.
        @param name Feature name to register. */
    virtual void CALL AddFeatureName(const char* name) = 0;

    /** Method to clear all registered feature names of instance. */
    virtual void CALL ClearFeatureNames() = 0;

    /** Method to tell how many feature names are registered in classifier instance.
        @return Number of registered feature names. */
    virtual const size_t CALL GetFeatureCount() const = 0;

    /** Method to retrieve the name of the feature with given index.
        @param index Index of feature to get.
        @return Name of feature with given index, or 0 if out of boundary. */
    virtual const char* CALL GetFeatureName(const size_t index) const = 0;

    /** Method to get the number of classes classifier can distinguish between.
        @return Count of possible class labels. */
    virtual const size_t CALL GetNumberOfClasses() const = 0;

    void operator delete(void* p)
    {

        if (p)
        {
            AbstractClassifier* ac = static_cast<AbstractClassifier* >(p);
            ac->Destroy();
        }
    }


}; // class AbstractClassifier

#endif // ABSTRACT_CLASSIFIER_H
