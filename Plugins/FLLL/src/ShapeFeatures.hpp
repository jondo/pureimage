/* ShapeFeatures.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FLLL_SHAPEFEATURES_HPP
#define FLLL_SHAPEFEATURES_HPP

#include <PureImage.hpp>

namespace pI {
namespace pIns {

/** Computes features for objects in a colour image.
 *
 *  This plugin expects two images (or matrices) of the same size:
 *
 *  -# the image itself; right now, only RGB images are supported.
 *  -# an int matrix which indicates objects: 0 for background (no object),
 *     1 for the first object, ..., n for the n-th object
 */
class ShapeFeatures: public pIn {

    typedef pIn Base;

public:

    ShapeFeatures (Runtime& runtime);
    virtual ~ShapeFeatures();

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(ShapeFeatures)

    virtual const pI_int GetpInVersion() const {
        return 10000;
    }

    virtual const std::string GetAuthor() const {
        return "JKU Linz";
    }

    virtual const std::string GetDescription() const {
        return "Computes shape features for objects given as an integer matrix.";
    }

    virtual const std::string GetName() const {
        return "pIn/Features/ShapeFeatures";
    }

    virtual Arguments GetParameterSignature() const {
        return MEMBER_SIGNATURE (_parameters);
    }

    virtual Arguments GetInputSignature() const {
        return MEMBER_SIGNATURE (_input_args);
    }

    virtual Arguments GetOutputSignature() const {
        return MEMBER_SIGNATURE (_output_args);
    }

protected:

    virtual void _Initialize (const Arguments& parameters);

    virtual void _Execute (Arguments& input_args, Arguments& output_args);

    Arguments _parameters, _input_args, _output_args;

}; // class pInTemplate: public pIn

} // namespace pIns
} // namespace pI

#endif // FLLL_SHAPEFEATURES_HPP
