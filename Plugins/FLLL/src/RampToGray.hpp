/* RampToGray.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RAMP_TO_GRAY_HPP
#define RAMP_TO_GRAY_HPP

#include <PureImage.hpp>

namespace pI {
namespace pIns {

class RampToGray: public pIn {

    typedef pIn Base;

public:

	RampToGray(Runtime& runtime);
	virtual ~RampToGray();

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(RampToGray)

    virtual const pI_int GetpInVersion() const {
        return 10000;
    }

	virtual const std::string GetAuthor() const {
        return "JKU Linz";
    }

	virtual const std::string GetDescription() const {
        return "Converts an image with ~ramped colours to grayscale.";
    }

    virtual const std::string GetName() const {
        return "pIn/Color/RampToGray";
    }

    virtual Arguments GetParameterSignature() const {
        return MEMBER_SIGNATURE(_parameters);
    }

    virtual Arguments GetInputSignature() const {
        return MEMBER_SIGNATURE(_input_args);
    }

    virtual Arguments GetOutputSignature() const {
        return MEMBER_SIGNATURE(_output_args);
    }

protected:

    virtual void _Initialize (const Arguments& parameters);

    virtual void _Execute (Arguments& input_args, Arguments& output_args);

	template<class X>
	X Difference(X v1, X v2) const {
		return (v1 < v2) ? (v2 - v1) : (v1 - v2);
	}

	enum ColourModel {
		CM_Jet		= 0
	};

	struct t_cc {
		t_cc(pI_float r_val, pI_float g_val, pI_float b_val): r(r_val), g(g_val), b(b_val) {}
		pI_float r, g, b;
	};

	void InitColourModel (ColourModel model = CM_Jet);
	void GetBestCandidates (pI_float r, pI_float g, pI_float b, pI_size& index1, pI_size& index2);

	Arguments _parameters, _input_args, _output_args;
	Arguments _act_params;
	std::vector<t_cc> _colour_map;
}; // class RampToGray: public pIn

} // namespace pIns
} // namespace pI

#endif // RAMP_TO_GRAY_HPP
