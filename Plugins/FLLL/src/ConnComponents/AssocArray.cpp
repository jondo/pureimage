/* AssocArray.cpp
 *
 * Copyright 2006, 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */


#include <algorithm>

#include "AssocArray.h"


AssocArray::AssocArray(size_t theN)
        : n(theN), a(new size_t[theN + 1])
{
    for (size_t i = 0; i < n + 1; ++i)
        { a[i] = i; }
}


AssocArray::~AssocArray()
{}


void AssocArray::associate(size_t i, size_t j)
{
    if (i == 0 || j == 0 || i == j)
        return;

    size_t iSearch = a[j];
    while (iSearch != j && iSearch != i)
        { iSearch = a[iSearch]; }

    if (iSearch == j)
        { std::swap(a[i], a[j]); }
}


Relabeling AssocArray::relabel() const
{
    size_t compN = 0;
    boost::shared_array<size_t> aa(new size_t[n + 1]);
    std::copy(a.get(), a.get() + n + 1, aa.get());

    size_t iCurrent;
    for (size_t i = 1; i <= n; ++i)
    {
        if (i <= aa[i])
        {
            ++compN;
            iCurrent = i;
            while (aa[iCurrent] != i)
            {
                size_t iNext = aa[iCurrent];
                aa[iCurrent] = compN;
                iCurrent = iNext;
            }
            aa[iCurrent] = compN;
        }
    }

    return std::make_pair(compN, aa);
}
