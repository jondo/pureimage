/* AggHierClustering.cpp
 *
 * Copyright 2006, 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include "AggHierClustering.h"

#include <functional>
#include <limits>

#include <boost/random.hpp>

template<class T> T& accessLL( const std::vector< std::vector<T> >& v,
                               size_t i, size_t j )
{
  // BOOST_ASSERT( i != j );
  return( ( i > j ) ? v[i][j] : v[j][i] );
}


std::pair<Relabeling, std::vector<std::vector<double> > >
     aggHierClustering( const std::vector<std::vector<std::pair<int, int> > >& clusters,
                        double stopDistance,
                        size_t maxClusterN /*= 0 */ )
{
    size_t n = clusters.size();

    // In case of very large initial clusters, initializing the distance matrix
    // gets unacceptably slow. Hence, a random-sampled cluster is used.
    std::vector< std::vector< std::pair<int, int> > > sampled;
    const bool downsample = true;

    typedef boost::minstd_rand base_generator_type;
    base_generator_type generator( 42u );
    boost::uniform_real<> uniDist( 0, 1 );
    boost::variate_generator< base_generator_type&, boost::uniform_real<> > uni( generator, uniDist );

    for( size_t i = 0; i < n; ++i )
    {
        if( !downsample || clusters[i].size() <= 100 )
        { sampled.push_back( clusters[i] ); }
        else
        {
            double fraction = (100. * ( log10( (double)clusters[i].size() ) - 1 )) / (double)clusters[i].size();
            //sampled.push_back( std::vector<T>() );
            sampled.push_back( std::vector<std::pair<int, int> >() );
            for( size_t j = 0 ; j < clusters[i].size(); ++j )
            {
                if( uni() < fraction )
                { sampled[i].push_back( clusters[i][j] ); }
            }
        }
    }

    // Computes distance matrix which is used through the rest of the algorithm.

    // Speed: In the worst case, \c n-1 merges have to take place. After m steps,
    // the distance matrix is of size (n-m-1)^2 / 2, and each time we have to traverse
    // through each element to find the new minimum.
    // Hence we have   (1/2) * [ n^2 + (n-1)^2 + ... + 1^2 - n - (n-1) - ... - 1 ]
    //               = n*(n+1)*(n-1)/6
    // look up operations, which is still O(n^3 / 6).
    // TODO If speed turns out to be an issue, it might be interesting to maintain
    // the distance matrix both as a random access matrix and as a set sorted
    // w.r.t. distance. This makes updating after a merge more complicated;
    // but perhaps boost::multi_index is suitable to hold this structure.

    // internally use integer values 
    std::vector<std::vector<int> > dist; // Distance matrix
    for( size_t i = 0; i < n; ++i )          // Only lower left half is used; i.e. first index > second index.
    { dist.push_back( std::vector<int>( i, 0) ); }

    int minDist = std::numeric_limits<int>::max();
    
    std::pair<int, int> minPair;
    int x, y;
    // HERE This COSTS time!
    for( size_t i = 0; i < n; ++i )
    {
        for( size_t j = 0; j < i; ++j )
        {
            // Computes minimum distance between clusters i and j:
            int d = std::numeric_limits<int>::max();
            for( size_t k = 0; k < sampled[i].size(); ++k )
            {
                for( size_t l = 0; l < sampled[j].size(); ++l )
                {
                    x = sampled[i][k].first - sampled[j][l].first;
                    x *= x;
                    y = sampled[i][k].second - sampled[j][l].second;
                    y *= y;
                    d = std::min( d, x + y );
                    //d = std::min( d, distance( sampled[i][k], sampled[j][l] ) );
                }
            }

            dist[i][j] = d;

            if( dist[i][j] < minDist )
            { 
                minDist = dist[i][j]; 
                minPair = std::make_pair( static_cast<int> (i), static_cast<int> (j) );
            }
        }
    }

    std::vector<bool> deleted( n, false );
    size_t m = clusters.size();
    AssocArray aa( m );

    // Determines two clusters with minimum distance, and merges them together.
    // Runs as long as the minimum distance is smaller than \p stopDistance,
    // and as long as the number of clusters is not less than maximal cluster number (if any).
    while( m > 1 && ( ( static_cast<double> (minDist) < (stopDistance * stopDistance) ) || 
                    ( maxClusterN > 0 && m > maxClusterN ) ) )
    {
        // The indices to merge/delete. Keep in mind that mergeI < deleteI.
        int mergeI = minPair.second;
        int deleteI = minPair.first;

        // Merges the minimal pair clusters. 
        aa.associate( mergeI + 1, deleteI + 1);
        deleted[ deleteI ] = true;
        --m;

        double mergeSize = clusters[ mergeI ].size();
        double deleteSize = clusters[ deleteI ].size();

        // Updates the distance matrix in a loop.
        // Each entry of the lower left part of the matrix where one index equals \c mergeI has to be updated.
        // To work in the lower left part of the matrix only, the loop is splitted up into three (almsot identical) loops.
        // Entries which were already deleted, i.e. where \c deleted[k] is true, are no longer updated.
        // For efficiency, it also keeps track of the new minimum.

        int newMinDist = std::numeric_limits<int>::max();
        std::pair<int, int> newMinPair;
        
        for( int k = 0; k < mergeI; ++k )
        {
            if( !deleted[k] )
            {
                dist[ mergeI ][ k ] = 
                    std::min( dist[ mergeI ][ k ], dist[ deleteI ][ k ] );
                if( dist[ mergeI ][ k ] < newMinDist )
                {
                    newMinDist = dist[ mergeI ][ k ]; 
                    newMinPair = std::make_pair( mergeI, k );
                }
            }
        }

        for( int k = mergeI + 1; k < deleteI; ++k )
        {
            if( !deleted[k] )
            {
                dist[ k ][ mergeI ] = 
                    std::min( dist[ k ][ mergeI ], dist[ deleteI ][ k ] );

                if( dist[ k ][ mergeI ] < newMinDist )
                {
                    newMinDist = dist[ k ][ mergeI ]; 
                    newMinPair = std::make_pair( k, mergeI );
                }
            }
        }

        for( size_t k = deleteI + 1; k < n; ++k )
        {
            if( !deleted[k] )
            {
                dist[ k ][ mergeI ] = 
                    std::min( dist[ k ][ mergeI ], dist[ k ][ deleteI ] );
                    
                if( dist[ k ][ mergeI ] < newMinDist )
                {
                    newMinDist = dist[ k ][ mergeI ]; 
                    newMinPair = std::make_pair( static_cast<int> (k), mergeI );
                }

            }
        }

        if( !( newMinDist <= minDist ) )
        {
            // Determines new minimum distance.
            newMinDist = std::numeric_limits<int>::max();
            bool found = false;

            for( size_t i = 0; !found && i < n; ++i )
            {
                for( size_t j = 0; !found && j < i; ++j )
                {
                    if( !deleted[i] && !deleted[j] )
                    {
                        if( dist[i][j] < newMinDist )
                        { 
                            newMinDist = dist[i][j]; 
                            newMinPair = std::make_pair( static_cast<int> (i), static_cast<int> (j) );

                            if( newMinDist <= minDist )
                            { found = true; }
                        }
                    }
                }
            }
        }

        minDist = newMinDist;
        minPair = newMinPair;
    }

    //return aa.relabel();
    std::vector<std::vector<double> > dist_mat;
    if (dist.size() > 0) {
        dist_mat.resize (m);
        //int temp;
        size_t rows(0), cols(0);
        for (size_t lv = 0; lv < dist.size(); lv++) {
            if (!deleted[lv]) {
                dist_mat[cols].resize (m);
                for (size_t lv2 = 0; lv2 < dist[lv].size(); lv2++) {
                    if (!deleted[lv2]) {
                        dist_mat[cols][rows] = sqrt (static_cast<double> (dist[lv][lv2]));
                        /*temp = dist[lv][lv2];
                        if (temp > 0)
                            dist_mat[cols][rows] = sqrt (static_cast<double> (temp));
                        else
                            dist_mat[cols][rows] = 0.0;*/
                        ++rows;
                    }
                }
                ++cols;
                rows = 0;
            }
        }
    }
    
    return std::make_pair (aa.relabel(), dist_mat);
}
