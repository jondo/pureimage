/* ConnComponents.cpp
 *
 * Copyright 2006, 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ConnComponents.h"

#include <vector>
#include <algorithm>
#include <cmath>
#include <string.h>

#include <boost/scoped_array.hpp>

#include "AssocArray.h"
#include "AggHierClustering.h"


Boxes connComponents( const unsigned char* const image,
                              int width, int height, int bypp, int stride,
                              int* labels )
{
    const bool doAggHierClustering = false;

    int wxh = width * height;

    int x, y;
    int pix, idx;

    // Pass 1: assigns initial labels to each row.
    boost::scoped_array<unsigned int> cc( new unsigned int[wxh] );

    unsigned int compI = 0;
    bool found = false;

    for( y = 0, idx = 0; y < height; ++y )
    {
        pix = y * stride;
        for( x = 0; x < width; ++x, pix += bypp, ++idx )
        {
            if( image[pix] > 0 )
            {
                if( !found )
                {
                    found = true;
                    ++compI;
                }
                cc[idx] = compI;
            }
            else
            {
                found = false;
                cc[idx] = 0;
            }
        }
    }

    // If no component was found, it does just black-out \p labels and returns.
    if( compI == 0 )
    {
        memset( labels, 0, wxh );
        return Boxes();
    }

    // Pass 2: identifies labels which belong to the same component.
    AssocArray aa( compI );

    for( y = 0, idx = 0; y < height; ++y )
    {
        for( x = 0; x < width; ++x, ++idx )
        {
            unsigned int label = cc[idx];
            if( label > 0 )
            {
                if( y < height - 1 )
                    aa.associate( label, cc[idx + width] );
            }
        }
    }

    Relabeling relabel = aa.relabel();

    // Does hierarchical clustering to obtain yet another relabeling.

    // BUG This costs a lot of time, and seems obsolete to me, since 
    //     doing dilation beforehand is a much simpler (and faster way)
    //     to "link together" unconnected components. (RR, Sep. 2010)
    if( doAggHierClustering )
    {
        // Applies the relabeling to obtain clusters from the un-dilated image.

        std::vector< std::vector< std::pair<int, int> > > clusters;
        for( size_t i = 0; i < relabel.first; ++i )
            clusters.push_back( std::vector< std::pair<int, int> >() );

        for( y = 0, idx = 0; y < height; ++y )
        {
            pix = y * stride;
            for( x = 0; x < width; ++x, pix += bypp, ++idx )
            {
                if( image[pix] > 0 )
                {
                    size_t label = relabel.second[ cc[idx] ];

                    if( label > 0 )
                    {
                        cc[idx] = static_cast<unsigned int>( label );
                        // Don't put *all* pixels into \c clusters, but just the edge pixels (4-neighbours).
                        bool add( false );

                        // A pixel on the image border is an edge pixel for sure
                        if( x == 0 || y == 0 || x == width - 1 || y == height - 1 )
                        {
                            add = true;
                        }
                        else
                        {
                            const unsigned char left( image[( y * width + x - 1 ) * bypp] );
                            const unsigned char right( image[( y * width + x + 1 ) * bypp] );
                            const unsigned char top( image[(( y - 1 ) * width + x ) * bypp] );
                            const unsigned char bottom( image[(( y + 1 ) * width + x ) * bypp] );
                            if( left == 0 || right == 0 || top == 0 || bottom == 0 )
                                add = true;
                        }

                        if( add )
                            clusters[ label-1 ].push_back( std::make_pair( x, y ) );
                    }
                }
                else
                { 
                    cc[idx] = 0; 
                }
            }
        }

        std::pair<Relabeling, std::vector<std::vector<double> > > clustering_results =
            aggHierClustering( clusters, ( width + height ) / 30., 8 );

        Relabeling relabel = clustering_results.first;
    }

    Boxes bound;
    bound.n = relabel.first;
    bound.box.reset( new Box[ bound.n ] );

    // Finally, fills \p labels and computes the bounding boxes.
    for( y = 0, pix = 0, idx = 0; y < height; ++y )
    {
        for( x = 0; x < width; ++x, pix += bypp, ++idx )
        {
            int label = static_cast<int>( relabel.second[ cc[idx] ] );

            if( label > 0 )
                { bound.box[label-1].add( x, y ); }

            labels[pix] = label;
        }
    }

    // TODO Sort boxes w.r.t. their size, in descending order.

    return bound;
}
