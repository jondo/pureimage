/* AggHierClustering.h
 *
 * Copyright 2006, 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef AGGHIERCLUSTERING_H
#define AGGHIERCLUSTERING_H


#include <vector>
#include <utility>

#include "AssocArray.h"

/** Performs agglomerative hierarchical (non-overlapping) clustering on a set
 *  of initial clusters.
 * 
 *  This function takes a vector of clusters, and subsequentially merges clusters
 *  which are near each other until there are no more such clusters.
 *  Here, cluster simply means a \c vector<T> where no duplicate elements are allowed.
 *  The algorithm stops if there is just one cluster left which contains all elements,
 *  or if there are no more than \c maxClusterN clusters left, and the minimal distance 
 *  between two clusters becomes greater than \p stopDistance.
 *
 *  @param clusters       The set of initial clusters.
 *  @param stopDistance   The distance where to stop.
 *  @param maxClusterN    The maximum number of clusters. If non-positive, this is ignored.
 *
 *  @return The vector of merged clusters, as well as the distance matrix.
 */
std::pair<Relabeling, std::vector<std::vector<double> > >
     aggHierClustering( const std::vector<std::vector<std::pair<int, int> > >& clusters,
                        double stopDistance,
                        size_t maxClusterN = 0 );

#endif
