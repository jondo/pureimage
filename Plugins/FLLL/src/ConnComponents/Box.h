/* Box.h
 *
 * Copyright 2006, 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX_H
#define BOX_H

#include <limits>
#include <vector>
#include <boost/shared_array.hpp>

#ifdef __GNUC__
#define INT_MAX __INT_MAX__
#endif

class Box
{
public:
    Box()
        : area(0), sumX(0), sumY(0),
          minX(INT_MAX), minY(INT_MAX), maxX(-INT_MAX), maxY(-INT_MAX)
    { }

    void add(int newX, int newY)
    {
        ++area;

        sumX += newX;
        sumY += newY;

        if (newX < minX)      { minX = newX; }
        else if (newX > maxX) { maxX = newX; }

        if (newY < minY)      { minY = newY; }
        else if (newY > maxY) { maxY = newY; }
    }

    size_t getArea() const { return area; }

    double getMeanX() const { return (area <= 0 ? 0 : ((double)sumX/(double)area)); }
    double getMeanY() const { return (area <= 0 ? 0 : ((double)sumY/(double)area)); }

    int getMinX() const { return minX; }
    int getMinY() const { return minY; }

    int getMaxX() const { return maxX; }
    int getMaxY() const { return maxY; }

private:
    size_t area;
    int sumX, sumY;
    int minX, minY, maxX, maxY;
};


struct Boxes
{
    Boxes(): box(0), n(0)
    { }

    boost::shared_array<Box> box;
    size_t n;
};

#ifdef __GNUC__
#undef INT_MAX
#endif

#endif
