/* ConnComponents.h
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONNCOMPONENTS_H
#define CONNCOMPONENTS_H

#include "Box.h"


/** Performs connected component labeling with hierarchical clustering afterwards.
 *
 *  @param      image       Image data, either gray or color values. Size is assumed to be
 *                            \p bypp * \p width * \p height bytes.
 *  @param      width       The width of the image.
 *  @param      height      The height of the image.
 *  @param      bypp        Number of bytes per pixel in the input image, usually 1 or 3.
 *                            If \p bypp > 1, only the first pixel value (usually red)
 *                            is considered for detecting non-zero pixels.
 *  @param      stride      Distance (in bytes) between the starts of two consecutive lines
 *                            in the image
 *  @param[out] labels      Gray level image of size \p width * \p height, filled
 *                            with labels. 0 always means "no label".
 *
 *  @return                 Array of \c Boxes around the components found.
 */
Boxes connComponents(const unsigned char* const image, 
                             int width, int height, int bypp, int stride,
                             int* labels);

#endif
