/* AssocArray.h
 *
 * Copyright 2006, 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ASSOCARRAY_H
#define ASSOCARRAY_H

#include <utility>
#include <boost/shared_array.hpp>


typedef std::pair< size_t, boost::shared_array<size_t> > Relabeling;

/**
 *
 */
struct AssocArray
{
    AssocArray(size_t theN);
    ~AssocArray();

    /// Associates labels i and j so that they represent the same component.
    // Labels run from 1...N, 0 is always "no label".
    void associate(size_t i, size_t j);

    Relabeling relabel() const;

private:
    boost::shared_array<size_t> a;
    size_t n;
};


#endif
