/* ShapeFeatures.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ShapeFeatures.hpp"

#include <vector>

#include <ArgumentChecks.hpp>

#include <Arguments/IntMatrix.hpp>
#include <Arguments/DoubleTable.hpp>


namespace pI {
namespace pIns {


ShapeFeatures::ShapeFeatures (Runtime& runtime) : Base (runtime) {

    // create plugin parameters here...

    _input_args.push_back (
        IntMatrix (runtime).SetName ("Input matrix")
        .SetDescription ("Input matrix containing object labels (0 = background, 1 ... n = object labels)."));

    _output_args.push_back (
        DoubleTable (runtime).SetName ("Output table")
        .SetDescription ("Output table with calculated features."));
}

/*virtual*/ ShapeFeatures::~ShapeFeatures() {

    // clean up private memory
}

/*virtual*/ void ShapeFeatures::_Initialize (const Arguments& parameters) {

    CheckParameters (parameters, _parameters);
    // create I/O signature here...

}

/*virtual*/ void ShapeFeatures::_Execute (Arguments& input_args, Arguments& output_args) {

    // validate input arguments
    CheckInputArguments (input_args, GetInputSignature());
    CheckOutputArguments (output_args, GetOutputSignature());

    IntMatrix in (input_args[0]);
    std::vector<pI_size> areas;

    areas.push_back (0); // dummy entry for label 0

    pI_size width  = in.GetCols(),
            height = in.GetRows();

    for (pI_size y = 0; y < height; ++y) {
        for (pI_size x = 0; x < width; ++x) {

            pI_size label = static_cast<pI_size> (in.GetData (y, x));

            if (label >= areas.size()) {
                areas.resize (label + 1, 0);
            }

            ++areas[label];
        }
    }

    // Now that the number of objects - i.e. the number of rows - is known,
    // creates the resulting DoubleTable
    DoubleTable dt (output_args[0]);

    if (dt.HasData()) {
        _runtime.GetCRuntime()->FreeArgumentData (
            _runtime.GetCRuntime(), output_args[0].get());
    }

    dt.SetCols (1);
    dt.SetRows (areas.size() - 1);

    for (size_t k = 1; k < areas.size(); ++k) {
        dt.SetData (k - 1, 0, areas[k]);
    }
}

} // namespace pIns
} // namespace pI
