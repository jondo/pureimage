/* DiscrepancyNorm.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <ArgumentChecks.hpp>

#include <Arguments/DoubleValue.hpp>
#include <Arguments/BoolValue.hpp>
#include <Arguments/ByteImage.hpp>

#include <sstream>
#include <boost/current_function.hpp>
#include <boost/gil/gil_all.hpp>

#include "DiscrepancyNorm.hpp"

namespace pI {
namespace pIns {

struct discrepancy_norm {
    
	discrepancy_norm(double multiplier = 1.0): _multi(multiplier) {}

    boost::gil::gray8_pixel_t operator() (const boost::gil::gray8c_loc_t& loc) const {
        
		_a[1] = loc(-1, -1); _a[2] = loc(0, -1); _a[3] = loc(1, -1);
        _a[0] = loc(-1,  0);                     _a[4] = loc(1,  0);
        _a[7] = loc(-1,  1); _a[6] = loc(0,  1); _a[5] = loc(1,  1);

        int mean = (_a[0] + _a[1] + _a[2] + _a[3] + _a[4] + _a[5] + _a[6] + _a[7]) / 8;
        int minimal, maximal;
        minimal = maximal = _acc[0] = _a[0] - mean;

        for (int i = 1; i <= 7; ++i) {
            _acc[i] = _acc[i - 1] + ((_a[i]) - mean);
            minimal = std::min (minimal, _acc[i]);
            maximal = std::max (maximal, _acc[i]);
        }

        return boost::gil::gray8_pixel_t (std::min ((int) floor (_multi * (maximal - minimal)), 255));
    }

private:

    pI_double _multi;
    mutable pI_int _a[9];
    mutable pI_int _acc[9];
}; // struct discrepancy_norm


DiscrepancyNorm::DiscrepancyNorm(Runtime& runtime): Base(runtime) {

	_parameters.push_back (DoubleValue(runtime).SetName("Factor").SetDescription("Multiplication factor").SetData(1.0));
	_parameters.push_back (BoolValue(runtime).SetName("InPlace").SetDescription("Use input image for operation").SetData(pI_FALSE));
	_togray_pin = boost::shared_ptr<pIn>(runtime.SpawnPlugin ("pIn/Color/ToGrayscale"));
	_togray_pin->Initialize (_togray_pin->GetParameterSignature());
}

/*virtual*/ DiscrepancyNorm::~DiscrepancyNorm() {}

/*virtual*/ void DiscrepancyNorm::_Initialize (const Arguments& parameters) {
	
	CheckParameters (parameters, _parameters);

	_factor = DoubleValue(parameters[0]).GetData();
	_inplace = BoolValue(parameters[1]).GetData();
	_input_args.clear();
	_output_args.clear();
	_input_args.push_back (
	 ByteImage(GetRuntime()).SetName("Input image")
							.SetDescription("Grayscale input image")
							.SetChannels(1)
							.SetReadOnly(!_inplace));
	if (!_inplace) {
		_output_args.push_back (
		 ByteImage(GetRuntime()).SetName("Output image")
								.SetDescription("Discrepancy norm output image")
								.SetChannels(1));
	}
}

/*virtual*/ void DiscrepancyNorm::_Execute (Arguments& input_args, Arguments& output_args) {

	// validate input arguments
	CheckInputArguments (input_args, GetInputSignature());
	CheckOutputArguments (output_args, GetOutputSignature());
	
	// Calculates discrepancy norm for non-border pixels using a
    // discrepancy_norm function object and Boost.GIL's iteration mechanism.
    ByteImage in(input_args[0]);
	if (in.GetChannels() != 1) {
		Arguments temp = _togray_pin->GetOutputSignature();
		_togray_pin->Execute (input_args, temp);
		in = ByteImage(temp[0]);
	}
	boost::gil::gray8_ptr_t inbits =
	 reinterpret_cast<boost::gil::gray8_ptr_t> (&(in.GetCPtr()->data.ByteImage->data[0][0][0]));

	discrepancy_norm dnorm(_factor);
	if (_inplace == pI_FALSE) {
		ByteImage out(output_args[0]);
		out.SetWidth (in.GetWidth());
		out.SetHeight (in.GetHeight());
		out.SetChannels (1);
		try {
			out.CreateData();
			boost::gil::gray8_ptr_t outbits =
			 reinterpret_cast<boost::gil::gray8_ptr_t> (&(out.GetCPtr()->data.ByteImage->data[0][0][0]));
			boost::gil::transform_pixel_positions (
			 boost::gil::subimage_view(boost::gil::interleaved_view(in.GetWidth(), in.GetHeight(), inbits, in.GetPitch()),
									   1,
									   1,
									   in.GetWidth() - 2,
									   in.GetHeight() - 2),
			 boost::gil::subimage_view(boost::gil::interleaved_view(in.GetWidth(), in.GetHeight(), outbits, in.GetPitch()),
									   1,
									   1,
									   in.GetWidth() - 2,
									   in.GetHeight() - 2),
			 dnorm);
			// Note: currently, this is done in ByteImage::CreateData()
			// out.SetPitch ((((8 * in.GetWidth()) + 31) / 32) * 4);
		} catch (exception::MemoryException) {
			throw exception::ExecutionException ("Failed to create output image.");
		}
	} else {
		ArgumentPtr arg = _runtime.CreateArgument (T_pI_Argument_ByteImage);
		ByteImage out(arg);
		out.SetWidth (in.GetWidth());
		out.SetHeight (in.GetHeight());
		out.SetChannels (1);
		try {
			out.CreateData();
			boost::gil::gray8_ptr_t outbits =
			 reinterpret_cast<boost::gil::gray8_ptr_t> (&(out.GetCPtr()->data.ByteImage->data[0][0][0]));
			boost::gil::transform_pixel_positions (
			 boost::gil::subimage_view(boost::gil::interleaved_view(in.GetWidth(), in.GetHeight(), inbits, in.GetPitch()),
									   1,
									   1,
									   in.GetWidth() - 2,
									   in.GetHeight() - 2),
			 boost::gil::subimage_view(boost::gil::interleaved_view(in.GetWidth(), in.GetHeight(), outbits, in.GetPitch()),
									   1,
									   1,
									   in.GetWidth() - 2,
									   in.GetHeight() - 2),
			 dnorm);
			out.SetPath (in.GetPath());
			// Note: currently, this is done in ByteImage::CreateData()
			// out.SetPitch ((((8 * in.GetWidth()) + 31) / 32) * 4);
			input_args[0] = arg;
		} catch (exception::MemoryException) {
			throw exception::ExecutionException ("Failed to create output image.");
		}
	}
}

/*virtual*/ void DiscrepancyNorm::Serialize (std::string& serialization_data) const {

	// if plugin was not initialized return false
	if (_input_args.size() == 0)
		throw exception::SerializationException("DiscrepancyNorm cannot serialize when not initialized");
	serialization_data.clear();
	std::stringstream ss(serialization_data);
	ss << _factor << _inplace;
}

/*virtual*/ void DiscrepancyNorm::Deserialize (const std::string& serialization_data) {

	std::stringstream ss(serialization_data);

	try {
		ss >> _factor >> _inplace;
		Arguments params = GetParameterSignature();
		DoubleValue(params[0]).SetData (_factor);
		BoolValue(params[1]).SetData (_inplace);
		Initialize (params);
	} catch (...) {
		throw exception::DeserializationException("Edge/DiscrepancyNorm deserialization failed.");
	}
}

} // namespace pIns
} // namespace pI
