/* FLLL_PINs.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <pInSharedLibraryExport.hpp>

#include "ToGrayscale.hpp"
#include "Crop.hpp"
#include "DiscrepancyNorm.hpp"
#include "ApplyRGBPalette.hpp"
#include "ConnectedComponents.hpp"
#include "IntMatrix2ByteImage.hpp"
#include "ColorName.hpp"
#include "RampToGray.hpp"
#include "GetDominantColors.hpp"
#include "ShapeFeatures.hpp"
#include "DoubleTableCombiner.hpp"
#include "DoubleTableAdd.hpp"
#include "ML/FLLLameClassifier.hpp"


//                available
extern "C" pI_API pI_bool CreatepIns (pI_int index, struct _CRuntime* runtime, struct _CpIn** plugin) {

	if ((runtime == 0) || (index < 0))
        return pI_FALSE;

	// create a plugin if it has not been created already
    switch (index) {
		EXPORT_PIN_SC (0, runtime, pI::pIns::ToGrayscale);
		EXPORT_PIN_SC (1, runtime, pI::pIns::Crop);
        EXPORT_PIN_SC (2, runtime, pI::pIns::ApplyRGBPalette);
        EXPORT_PIN_SC (3, runtime, pI::pIns::ConnectedComponents);
        EXPORT_PIN_SC (4, runtime, pI::pIns::IntMatrix2ByteImage);
        EXPORT_PIN_SC (5, runtime, pI::pIns::ColorName);
    	EXPORT_PIN_SC (6, runtime, pI::pIns::RampToGray);
        EXPORT_PIN_SC (7, runtime, pI::pIns::GetDominantColors);
        EXPORT_PIN_SC (8, runtime, pI::pIns::ShapeFeatures);
		EXPORT_PIN_SC (9, runtime, pI::pIns::DoubleTableCombiner);
		EXPORT_PIN_SC (10, runtime, pI::pIns::DoubleTableAdd);
		EXPORT_PIN_SC (11, runtime, pI::pIns::FLLLameClassifier);
#ifdef ENABLE_DISCREPANCYNORM
        EXPORT_PIN_SC (12, runtime, pI::pIns::DiscrepancyNorm);
#endif
        default:
            return pI_FALSE;
    }
}; /* extern "C" pI_API pI_bool CreatepIns (pI_int index, struct _CRuntime* host, struct _CpIn** plugin) */

