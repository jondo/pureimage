/* DoubleTableCombiner.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <ArgumentChecks.hpp>

#include <Arguments.hpp>

#include "DoubleTableCombiner.hpp"

namespace pI {
namespace pIns {

DoubleTableCombiner::DoubleTableCombiner(Runtime& runtime): Base(runtime) {

	_parameters.push_back (
	 BoolArray(runtime).SetName("Columns table one").SetDescription("Column selection of first input table").SetCount(0));
	_parameters.push_back (
	 BoolArray(runtime).SetName("Columns table two").SetDescription("Column selection of second input table").SetCount(0));

	_input_args.push_back (
	 DoubleTable(runtime).SetName("Table one").SetDescription("First input table"));
	_input_args.push_back (
	 DoubleTable(runtime).SetName("Table two").SetDescription("Second input table"));

	_output_args.push_back (
	 DoubleTable(runtime).SetName("Combined tables").SetDescription("Combined input tables"));
} // DoubleTableCombiner::DoubleTableCombiner(Runtime& runtime)

/*virtual*/ DoubleTableCombiner::~DoubleTableCombiner() {}

/*virtual*/ void DoubleTableCombiner::_Initialize (const Arguments& parameters) {
	
	for (pI_size lv = 0; lv < parameters.size(); ++lv) {
		CheckArgumentType (parameters[lv].get(), _parameters[0].get(), pI_FALSE);
	}
	_given_parameters = _runtime.CopyArguments (parameters, pI_TRUE);
} // /*virtual*/ void DoubleTableCombiner::_Initialize (...)

/*virtual*/ void DoubleTableCombiner::_Execute (Arguments& input_args, Arguments& output_args) {

	if (input_args.size() < 1)
		throw exception::IncompleteArgumentException("At least one DoubleTable must provided in order to ");
	// validate input arguments
	pI_size col_count(0), row_count(0);
	for (pI_size lv = 0; lv < input_args.size(); ++lv) {
		CheckInputArgument (input_args[lv], _input_args[0]);
		DoubleTable dt(input_args[lv]);
		if (lv == 0)
			row_count = dt.GetRows();
		else if (dt.GetRows() < row_count) {
#ifdef PI_FORGIVING
			// use minimum rows
			row_count = dt.GetRows();
#else // #ifdef PI_FORGIVING
			throw pI::exception::ArgumentException("DoubleTableCombiner encountered different table geometries."); // draconic...
#endif // #ifdef PI_FORGIVING
		}
		if (_given_parameters.size() > lv) {
			BoolArray ba(_given_parameters[lv]);
			if ((ba.GetCount() > 0) && (ba.GetCount() != dt.GetCols()))
				throw exception::ArgumentException("Dimension mismatch in DoubleTableCombiner execute");
			for (pI_size c = 0; c < dt.GetCols(); ++c)
				if ((ba.GetCount() == 0) || (ba.GetData (c) == pI_TRUE))
					++col_count;
		} else
			col_count += dt.GetCols();
	}
	// validate output arguments
	CheckOutputArguments (output_args, _output_args);
	DoubleTable out(output_args[0]);
	if (out.HasData()) {
		if ((out.GetCols() != col_count) || (out.GetRows() != row_count))
			_runtime.GetCRuntime()->FreeArgumentData (_runtime.GetCRuntime(), output_args[0].get());
	}
	if (out.HasData() == pI_FALSE) {
		out.SetCols (col_count);
		out.SetRows (row_count);
		out.CreateData();
	}

	// start data assembling
	pI_size c = 0;
	for (pI_size lv = 0; lv < _given_parameters.size(); ++lv) {
		DoubleTable dt(input_args[lv]);
		BoolArray ba(_given_parameters[lv]);
		for (pI_size bac = 0; bac < dt.GetCols(); ++bac) {
			if ((ba.GetCount() == 0) || (ba.GetData (bac) == pI_TRUE)) {
				// adjust column labels
				out.SetHeader (lv, dt.GetHeader (bac));
				// copy table data
				for (pI_size r = 0; r < row_count; ++r) {
					out.SetData (r, c, dt.GetData (r, bac));
				}
				++c;
			}
		}
	}
} // /*virtual*/ void DoubleTableCombiner::_Execute (...)

} // namespace pIns
} // namespace pI
