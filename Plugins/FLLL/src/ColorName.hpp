/* ColorName.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FLLL_COLORNAME_HPP
#define FLLL_COLORNAME_HPP

#include <PureImage.hpp>

#include <map>
#include <string>
#include <boost/tuple/tuple.hpp>


namespace pI {
namespace pIns {

class ColorName: public pIn {

    typedef pIn Base;

public:

	ColorName(Runtime& runtime);
	virtual ~ColorName();

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(ColorName)

    virtual const pI_int GetpInVersion() const {
        return 10000;
    }

	virtual const std::string GetAuthor() const {
        return "JKU Linz";
    }

	virtual const std::string GetDescription() const {
        return "Looks up the name of an L*a*b* color triplet.";
    }

    virtual const std::string GetName() const {
        return "pIn/Color/ColorName";
    }

    virtual Arguments GetParameterSignature() const {
        return MEMBER_SIGNATURE(_parameters);
    }

    virtual Arguments GetInputSignature() const {
        return MEMBER_SIGNATURE(_input_args);
    }

    virtual Arguments GetOutputSignature() const {
        return MEMBER_SIGNATURE(_output_args);
    }

protected:

    virtual void _Initialize (const Arguments& parameters);

    virtual void _Execute (Arguments& input_args, Arguments& output_args);

	Arguments _parameters, _input_args, _output_args;

private:

	std::map< std::string, boost::tuples::tuple<double, double, double> > _color_map;

    typedef enum {X11 = 1, wxWidgets = 2, German = 3} ColorScheme;

    void InitColorMap(ColorScheme scheme);

}; // class ColorName: public pIn

} // namespace pIns
} // namespace pI

#endif // PIN_TEMPLATE_HPP
