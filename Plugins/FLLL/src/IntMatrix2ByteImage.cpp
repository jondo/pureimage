/* IntMatrix2ByteImage.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include "IntMatrix2ByteImage.hpp"

#include <cmath>

#include <ArgumentChecks.hpp>

#include <Arguments/ByteImage.hpp>
#include <Arguments/IntMatrix.hpp>


namespace pI {
namespace pIns {


IntMatrix2ByteImage::IntMatrix2ByteImage(Runtime& runtime): Base(runtime) {
}

/*virtual*/ IntMatrix2ByteImage::~IntMatrix2ByteImage() {
}

/*virtual*/ void IntMatrix2ByteImage::_Initialize (const Arguments& parameters) {

	CheckParameters (parameters, _parameters);

    _input_args.clear();
    _output_args.clear();


    _input_args.push_back (IntMatrix (GetRuntime())
                           .SetName ("Input matrix")
                           .SetDescription ("Arbitrary int matrix"));

    _output_args.push_back (ByteImage (GetRuntime())
                            .SetName ("Output image")
                            .SetDescription ("Gray-scale output image")
                            .SetChannels (1));
}

/*virtual*/ void IntMatrix2ByteImage::_Execute (Arguments& input_args, Arguments& output_args) {

	// validate input arguments
	CheckInputArguments (input_args, GetInputSignature());
	CheckOutputArguments (output_args, GetOutputSignature());

    IntMatrix in (input_args[0]);

    if (!in.HasData()) {
        return;
    }

    size_t width  = in.GetCols();
    size_t height = in.GetRows();

    ByteImage out (output_args[0]);
    pI_bool do_create_data (pI_TRUE);

    if (do_create_data == pI_TRUE) {
        if (out.HasData()) {
            _runtime.GetCRuntime()->FreeArgumentData (
                _runtime.GetCRuntime(),
                output_args[0].get());
        }

        out.SetWidth (width).SetHeight (height).SetChannels (1);
        
		try {
			out.CreateData();
		} catch (exception::MemoryException) {
            throw exception::ExecutionException ("Failed to create image.");
        }
    }

    // Note: currently, this is done in ByteImage::CreateData()
	// out.SetPitch ((((8 * width) + 31) / 32) * 4);

    int matrix_max = 0;

    for (size_t j = 0; j < height; ++j) {
        for (size_t i = 0; i < width; ++i) {
            matrix_max = std::max (matrix_max, in.GetData (j, i));
        }
    }

    double scale = (255. / (double)matrix_max);

    for (size_t j = 0; j < height; ++j) {
        for (size_t i = 0; i < width; ++i) {
            out.SetData(j, i, 0, (int)floor(scale * (double)in.GetData (j, i)));
        }
    }

}

} // namespace pIns
} // namespace pI
