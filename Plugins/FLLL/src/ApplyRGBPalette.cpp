/* ApplyRGBPalette.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ApplyRGBPalette.hpp"

#include <ArgumentChecks.hpp>
#include <Arguments/RGBPalette.hpp>
#include <Arguments/VariableArgument.hpp>
#include <Arguments/ByteImage.hpp>
#include <Arguments/IntMatrix.hpp>

namespace pI {
namespace pIns {


ApplyRGBPalette::ApplyRGBPalette (Runtime& runtime) : Base (runtime) {

    _parameters.push_back (RGBPalette (runtime)
                           .SetName ("RGBPalette")
                           .SetDescription ("The RGB palette.")
                           .SetCount (256)
                           .SetConstraints ("count >= 256"));
}


/*virtual*/ ApplyRGBPalette::~ApplyRGBPalette() {
}


/*virtual*/ void ApplyRGBPalette::_Initialize (const Arguments& parameters) {

    try {
        CheckParameters (parameters, _parameters);
    } catch (pI::exception::IncompatibleParameterException& xc) {
        std::string w (xc.what()); // Log it, or what
    }

    _parameters = _runtime.CopyArguments (parameters);

    _input_args.clear();
    _output_args.clear();

    VariableArgument arg (GetRuntime());
    arg.SetName ("Input image or matrix")
    .SetDescription ("Input gray-scale image or  matrix; accepted types are ByteImage, ShortMatrix or IntMatrix.");
    arg.SetCount (T_pI_Argument_VariableArgument);
    arg.CreateData();

    arg.SetTypes (T_pI_Argument_ByteImage, pI_TRUE);
    arg.SetTypes (T_pI_Argument_ShortMatrix, pI_TRUE);
    arg.SetTypes (T_pI_Argument_IntMatrix, pI_TRUE);
    _input_args.push_back (arg);

    _output_args.push_back (ByteImage (GetRuntime())
                            .SetName ("Output image")
                            .SetDescription ("RGB output image")
                            .SetChannels (3));
}


/*virtual*/ void ApplyRGBPalette::_Execute (Arguments& input_args, Arguments& output_args) {

    CheckInputArguments (input_args, GetInputSignature());
    CheckOutputArguments (output_args, GetOutputSignature());

    // Sets width, height, and pitch depending on the input type;
    // or throws if an incompatible type was passed. (Can this happen?)
    pI_size width, height, pitch;

    switch (input_args[0]->signature.type) {
        case T_pI_Argument_ByteImage: {
            ByteImage in (input_args[0]);
            width  = in.GetWidth();
            height = in.GetHeight();
            pitch  = in.GetPitch();
        }
        break;
        case T_pI_Argument_ShortMatrix: {
            ShortMatrix in (input_args[0]);
            width  = in.GetCols();
            height = in.GetRows();
            pitch  = in.GetCols();
        }
        break;
        case T_pI_Argument_IntMatrix: {
            IntMatrix in (input_args[0]);
            width  = in.GetCols();
            height = in.GetRows();
            pitch  = in.GetCols();
        }
        break;
        default:
            throw pI::exception::IncompatibleArgumentException ("Input argument 0 must be ByteImage or IntMatrix.");
    }

    // Re-creates output image if it has data, but wrong dimensions
    ByteImage out (output_args[0]);
    pI_bool do_create_data (pI_TRUE);

    if (out.HasData()) {
        if ( (out.GetChannels() == 3) && (out.GetWidth() == width) && (out.GetHeight() == height)) {
            do_create_data = pI_FALSE;
        }
    }

    if (do_create_data == pI_TRUE) {
        if (out.HasData()) {
            _runtime.GetCRuntime()->FreeArgumentData (
                _runtime.GetCRuntime(),
                output_args[0].get());
        }

        out.SetWidth (width).SetHeight (height).SetChannels (3);

		try {
			out.CreateData();
		} catch(exception::MemoryException) {
            throw exception::ExecutionException ("Failed to create image.");
        }
    }

    // Note: currently, this is done in ByteImage::CreateData()
	// Note2: it seems that this formula is wrong (see other implementations)
	// out.SetPitch (3 * ( ( ( (8 * width) + 31) / 32) * 4));

    RGBPalette rgbpal (_parameters[0]);

    switch (input_args[0]->signature.type) {
        case T_pI_Argument_ByteImage: {
            ByteImage in (input_args[0]);
            out.SetPath (in.GetPath());

            for (pI_size y = 0; y < height; ++y) {
                for (pI_size x = 0; x < width; ++x) {

                    pI_size idx = static_cast<pI_size> (in.GetData (y, x, 0));

                    if (idx >= 0 && idx < rgbpal.GetCount()) {
                        out.SetData (y, x, 0, rgbpal.GetR (idx));
                        out.SetData (y, x, 1, rgbpal.GetG (idx));
                        out.SetData (y, x, 2, rgbpal.GetB (idx));
                    }
                }
            }
        }
        break;
        case T_pI_Argument_ShortMatrix: {
            ShortMatrix in (input_args[0]);

            for (pI_size y = 0; y < height; ++y) {
                for (pI_size x = 0; x < width; ++x) {

                    pI_size idx = static_cast<pI_size> (in.GetData (y, x));

                    if (idx >= 0 && idx < rgbpal.GetCount()) {
                        out.SetData (y, x, 0, rgbpal.GetR (idx));
                        out.SetData (y, x, 1, rgbpal.GetG (idx));
                        out.SetData (y, x, 2, rgbpal.GetB (idx));
                    }
                }
            }
        }
        break;
        case T_pI_Argument_IntMatrix: {
            IntMatrix in (input_args[0]);

            for (pI_size y = 0; y < height; ++y) {
                for (pI_size x = 0; x < width; ++x) {

                    pI_size idx = static_cast<pI_size> (in.GetData (y, x));

                    if (idx >= 0 && idx < rgbpal.GetCount()) {
                        out.SetData (y, x, 0, rgbpal.GetR (idx));
                        out.SetData (y, x, 1, rgbpal.GetG (idx));
                        out.SetData (y, x, 2, rgbpal.GetB (idx));
                    }
                }
            }
        }
    }
}

} // namespace pIns
} // namespace pI
