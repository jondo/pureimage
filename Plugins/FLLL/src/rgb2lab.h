/* rgb2lab.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RGB2LAB_HPP
#define RGB2LAB_HPP

#include <boost/tuple/tuple.hpp>

boost::tuples::tuple<double, double, double> rgb2lab (unsigned char rgb_r, unsigned char rgb_g, unsigned char rgb_b);

boost::tuples::tuple<double, double, double> rgb2lab (const boost::tuples::tuple<unsigned char, unsigned char, unsigned char>& rgb);

boost::tuples::tuple<double, double, double> rgb2lab (int hex);

#endif
