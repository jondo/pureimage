/* pInTemplate.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <ArgumentChecks.hpp>

#include "pInTemplate.hpp"

namespace pI {
namespace pIns {


pInTemplate::pInTemplate(Runtime& runtime): Base(runtime) {

	// TODO create plugin parameters here...
}

/*virtual*/ pInTemplate::~pInTemplate() {

	// TODO clean up private memory here...
}

/*virtual*/ void pInTemplate::_Initialize (const Arguments& parameters) {

	CheckParameters (parameters, _parameters);
	// TODO create I/O signature here...
}

/*virtual*/ void pInTemplate::_Execute (Arguments& input_args, Arguments& output_args) {

	// validate input and output arguments
	CheckInputArguments (input_args, GetInputSignature());
	CheckOutputArguments (output_args, GetOutputSignature());

	// TODO implement plugin worker method here...

}

/*virtual*/ void pInTemplate::Serialize (std::stringstream& serialization_data) const {

	// TODO implement serialization method in case of stateful plugin
	Base::Serialize (serialization_data);
}

/*virtual*/ void pInTemplate::Deserialize (const std::string& serialization_data) {

	// TODO implement deserialization method in case of stateful plugin
	Base::Deserialize (serialization_data);
}

} // namespace pIns
} // namespace pI
