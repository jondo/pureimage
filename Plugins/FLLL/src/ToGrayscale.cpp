/* ToGrayscale.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <ArgumentChecks.hpp>
#include <Arguments/DoubleValue.hpp>
#include <Arguments/ByteImage.hpp>

#include "ToGrayscale.hpp"

namespace pI {
namespace pIns {


ToGrayscale::ToGrayscale(Runtime& runtime): Base(runtime), _red(0.3), _green(0.59), _blue(0.11) {

	_parameters.push_back (DoubleValue(runtime).SetName("Red").SetDescription("Red channel weight").SetData(0.3));
	_parameters.push_back (DoubleValue(runtime).SetName("Green").SetDescription("Green channel weight").SetData(0.59));
	_parameters.push_back (DoubleValue(runtime).SetName("Blue").SetDescription("Blue channel weight").SetData(0.11));
	_input_args.push_back (ByteImage(runtime).SetName("Input image").SetDescription("Arbitrary single-page input image").SetChannels(3));
	_output_args.push_back (ByteImage(runtime).SetName("Output image").SetDescription("Grayscale output image").SetChannels(1));
}

/*virtual*/ ToGrayscale::~ToGrayscale() {}

/*virtual*/ void ToGrayscale::_Initialize (const Arguments& parameters) {
	
	CheckParameters (parameters, _parameters);
	_red = DoubleValue(parameters[0]).GetData();
	_green = DoubleValue(parameters[1]).GetData();
	_blue = DoubleValue(parameters[2]).GetData();
	const pI_double sum(_red + _green + _blue);
	if (sum > 1.0) {
		_red /= sum;
		_green /= sum;
		_blue /= sum;
	}
}

/*virtual*/ void ToGrayscale::_Execute (Arguments& input_args, Arguments& output_args) {

	// validate input arguments
	CheckInputArguments (input_args, GetInputSignature());
	CheckOutputArguments (output_args, GetOutputSignature());
	
	ByteImage in(input_args[0]), out(output_args[0]);
	pI_bool do_create_data(pI_TRUE);
	if (out.HasData()) {
		if ((out.GetChannels() == 1) &&
			(out.GetWidth() == in.GetWidth()) &&
			(out.GetHeight() == in.GetHeight()))
			do_create_data = pI_FALSE;
	}
	if (do_create_data == pI_TRUE) {
		if (out.HasData()) {
			_runtime.GetCRuntime()->FreeArgumentData (
			 _runtime.GetCRuntime(),
			 output_args[0].get());
		}
		out.SetWidth (in.GetWidth());
		out.SetHeight (in.GetHeight());
		out.SetChannels (1);
		try {
			out.CreateData();
		} catch (exception::MemoryException) {
			throw exception::ExecutionException ("Failed to create grayscale image.");
		}
	}
	pI_byte bval;
	// Note: currently, this is done in ByteImage::CreateData()
	// out.SetPitch ((((8 * in.GetWidth()) + 31) / 32) * 4);
	out.SetPath (in.GetPath());
	for (pI_size h = 0; h < in.GetHeight(); ++h) {
		for (pI_size w = 0; w < in.GetWidth(); ++w) {
			bval = static_cast<pI_byte> (static_cast<pI_double> (in.GetData (h, w, 0)) * _red);
			bval += static_cast<pI_byte> (static_cast<pI_double> (in.GetData (h, w, 1)) * _green);
			bval += static_cast<pI_byte> (static_cast<pI_double> (in.GetData (h, w, 2)) * _blue);
			out.SetData (h, w, 0, bval);
		}
	}
}

} // namespace pIns
} // namespace pI
