/* Crop.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <ArgumentChecks.hpp>

#include <Arguments/ByteImage.hpp>
#include <Arguments/IntPolygon.hpp>

#include "Crop.hpp"

namespace pI {
namespace pIns {

Crop::Crop(Runtime& runtime): Base(runtime) {

	_input_args.push_back (
	 ByteImage(runtime).SetName("Input image")
					   .SetDescription("Arbitrary single-page input image"));
	_input_args.push_back (
	 IntPolygon(runtime).SetName("ROI")
						.SetDescription("Region to crop")
						.SetCount(2)
						.SetConstraints("count == 2"));
	_output_args.push_back (
	 ByteImage(runtime).SetName("Output image")
					   .SetDescription("Cropped output image"));
}

/*virtual*/ Crop::~Crop() {}

/*virtual*/ void Crop::_Initialize (const Arguments& parameters) {}

/*virtual*/ void Crop::_Execute (Arguments& input_args, Arguments& output_args) {

	// validate input arguments
	CheckInputArguments (input_args, GetInputSignature());
	CheckOutputArguments (output_args, GetOutputSignature());

	// crop the input image!
	ByteImage in(input_args[0]), out(output_args[0]);
	IntPolygon poly(input_args[1]);
	pI_int x0(std::min (poly.GetX (0), poly.GetX (1)));
	pI_int x1(std::max (poly.GetX (0), poly.GetX (1)));
	pI_int y0(std::min (poly.GetY (0), poly.GetY (1)));
	pI_int y1(std::max (poly.GetY (0), poly.GetY (1)));
	pI_int width = x1 - x0;
	pI_int height = y1 - y0;
	pI_int channels = in.GetChannels();

	out.SetWidth (width);
	out.SetHeight (height);
	out.SetChannels (in.GetChannels());
	
	out.CreateData();

	for (pI_int h = 0; h < height; ++h) {
		for (pI_int w = 0; w < width; ++w) {
			for (pI_int c = 0; c < channels; ++c) {
				out.SetData (h, w, c, in.GetData (h + y0, w + x0, c));
			}
		}
	}
	// Note: currently, this is done in ByteImage::CreateData()
	// out.SetPitch ((((channels * 8 * width) + 31) / 32) * 4);
}

} // namespace pIns
} // namespace pI
