/* FreenectVideoDepth.cpp
 *
 * Copyright 2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include "FreenectVideoDepth.hpp"

#include <ArgumentChecks.hpp>
#include <Arguments/ShortMatrix.hpp>
#include <Arguments/ByteImage.hpp>

#include <libfreenect_sync.h>

#include <cstring>

namespace pI {
namespace pIns {


FreenectVideoDepth::FreenectVideoDepth (Runtime& runtime) : Base (runtime) {
    _parameters.push_back (IntValue (GetRuntime()).SetName ("Index").SetDescription ("Device index (0 is the first)").SetData (0));
    _parameters.push_back (IntValue (GetRuntime()).SetName ("VideoFormat").SetDescription ("Video format to use").SetData ( (pI_int) FREENECT_VIDEO_RGB));
    _parameters.push_back (IntValue (GetRuntime()).SetName ("DepthFormat").SetDescription ("Depth format to use").SetData ( (pI_int) LED_GREEN));
}


/*virtual*/ FreenectVideoDepth::~FreenectVideoDepth() {
    freenect_sync_stop();
}


/*virtual*/ void FreenectVideoDepth::_Initialize (const Arguments& parameters) {

    CheckParameters (parameters, _parameters);
    _parameters = _runtime.CopyArguments (parameters);

    _input_args.push_back (IntValue (GetRuntime()).SetName ("Tilt").SetDescription ("New tilt angle in degrees (optional)"));
    _input_args.push_back (IntValue (GetRuntime()).SetName ("LEDStatus").SetDescription ("New LED status (optional)"));

    _output_args.push_back (ByteImage (GetRuntime()).SetName ("Image").SetDescription ("RGB output image").SetChannels (3));
    _output_args.push_back (ShortMatrix (GetRuntime()).SetName ("Depth").SetDescription ("Depth output matrix"));
    _output_args.push_back (IntValue (GetRuntime()).SetName ("TimeStamp").SetDescription ("Timestamp as returned by video function"));
}


/*virtual*/ void FreenectVideoDepth::_Execute (Arguments& input_args, Arguments& output_args) {

    // Validate input and output arguments

    // Since both input arguments are optional, only does necessary checks.
    switch (input_args.size()) {
        case 0:
            break;
        case 1:
            CheckInputArgument (input_args[0], this->GetInputSignature() [0]);
            break;
        default:
            CheckInputArguments (input_args, this->GetInputSignature());
    }

    CheckOutputArguments (output_args, GetOutputSignature());

    IntValue index (_parameters[0]);

    // First, sets the new tilt angle, and the LED status

    if (input_args.size() > 0) {
        freenect_raw_tilt_state* tiltState;
        int resTilt = freenect_sync_get_tilt_state (&tiltState, index.GetData());

        if (resTilt != 0) {
            throw exception::ExecutionException ("Failed to get tilt state. Is Kinect connected?");
        }

        IntValue newTiltAngle (input_args[0]);

        if (newTiltAngle.GetData() != tiltState->tilt_angle) {
            resTilt = freenect_sync_set_tilt_degs (newTiltAngle.GetData(), index.GetData());

            if (resTilt != 0) {
                throw exception::ExecutionException ("Failed to set tilt angle. Is Kinect connected?");
            }
        }
    }

    if (input_args.size() > 1) {
        IntValue newLEDStatus (input_args[1]);
        int resLED = freenect_sync_set_led ( (freenect_led_options) newLEDStatus.GetData(), index.GetData());

        if (resLED != 0) {
            throw exception::ExecutionException ("Failed to set LED status. Is Kinect connected?");
        }
    }

    // Second, gets video and depth maps
    char* dataRGB;
    short* dataDepth;
    unsigned int timestamp;

    IntValue intValueRGB (_parameters[1]);
    freenect_video_format fmtRGB = (freenect_video_format) intValueRGB.GetData();
    int resRGB = freenect_sync_get_video ( (void**) (&dataRGB), &timestamp, index.GetData(), fmtRGB);

    if (resRGB) {
        throw exception::ExecutionException ("Failed to get video. Kinect not connected?");
    }

    IntValue intValueDepth (_parameters[2]);
    freenect_depth_format fmtDepth = (freenect_depth_format) intValueDepth.GetData();
    int resDepth = freenect_sync_get_depth ( (void**) (&dataDepth), &timestamp, index.GetData(), fmtDepth);

    if (resDepth) {
        throw exception::ExecutionException ("Failed to get the depth information. Kinect not connected?");
    }

    // Get image dimensions, create image if necessary, and copy data
    freenect_frame_mode modeRGB = freenect_find_video_mode (FREENECT_RESOLUTION_MEDIUM, fmtRGB);

    pI_size outRGBChannels = (modeRGB.data_bits_per_pixel + modeRGB.padding_bits_per_pixel) / 8;
    pI_size outRGBWidth    = modeRGB.width;
    pI_size outRGBHeight   = modeRGB.height;

    ByteImage outRGB (output_args[0]);
    pI_bool do_create_data (pI_TRUE);

    if (outRGB.HasData()) {
        if ( (outRGB.GetChannels() == outRGBChannels)
                && (outRGB.GetWidth() == outRGBWidth) && (outRGB.GetHeight() == outRGBHeight)) {
            do_create_data = pI_FALSE;
        }
    }

    if (do_create_data == pI_TRUE) {
        if (outRGB.HasData()) {
            _runtime.GetCRuntime()->FreeArgumentData (_runtime.GetCRuntime(), output_args[0].get());
        }

        outRGB.SetWidth (outRGBWidth).SetHeight (outRGBHeight).SetChannels (outRGBChannels);

        try {
            outRGB.CreateData();
        }
        catch (exception::MemoryException) {
            throw exception::ExecutionException ("Failed to create RGB image.");
        }
    }

    memcpy (outRGB.GetDataBlock(), dataRGB, modeRGB.bytes);

    // Get matrix dimensions, create matrix if necessary, and copy data
    freenect_frame_mode modeDepth = freenect_find_depth_mode (FREENECT_RESOLUTION_MEDIUM, fmtDepth);

    pI_size outDepthWidth    = modeDepth.width;
    pI_size outDepthHeight   = modeDepth.height;

    ShortMatrix outDepth (output_args[1]);
    do_create_data = pI_TRUE;

    if (outDepth.HasData()) {
        if ( (outDepth.GetCols() == outDepthWidth) && (outDepth.GetRows() == outDepthHeight)) {
            do_create_data = pI_FALSE;
        }
    }

    if (do_create_data == pI_TRUE) {
        if (outDepth.HasData()) {
            _runtime.GetCRuntime()->FreeArgumentData (_runtime.GetCRuntime(), output_args[1].get());
        }

        outDepth.SetCols (outDepthWidth).SetRows (outDepthHeight);

        try {
            outDepth.CreateData();
        }
        catch (exception::MemoryException) {
            throw exception::ExecutionException ("Failed to create image.");
        }
    }

    memcpy (outDepth.GetDataBlock(), dataDepth, modeDepth.bytes);

    IntValue outTimeStep (output_args[2]);
    outTimeStep.SetData (timestamp);
}

} // namespace pIns
} // namespace pI
