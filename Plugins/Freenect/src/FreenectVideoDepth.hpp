/* FreenectVideoDepth.hpp
 *
 * Copyright 2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PI_PINS_FREENECTVIDEODEPTH_HPP
#define PI_PINS_FREENECTVIDEODEPTH_HPP

#include <PureImage.hpp>

namespace pI {
namespace pIns {

class FreenectVideoDepth: public pIn {

    typedef pIn Base;

public:

    FreenectVideoDepth (Runtime& runtime);
    virtual ~FreenectVideoDepth();

    DECLARE_VIRTUAL_COPY_CONSTRUCTOR (FreenectVideoDepth)

    virtual const pI_int GetpInVersion() const {
        return 20000;
    }

    virtual const std::string GetAuthor() const {
        return "JKU Linz";
    }

    virtual const std::string GetDescription() const {
        return "Gets RGB image and depth matrix of a Kinect device at a specified tilt angle.";
    }

    virtual const std::string GetName() const {
        return "Freenect/GetVideoAndDepth";
    }

    virtual Arguments GetParameterSignature() const {
        return MEMBER_SIGNATURE (_parameters);
    }

    virtual Arguments GetInputSignature() const {
        return MEMBER_SIGNATURE (_input_args);
    }

    virtual Arguments GetOutputSignature() const {
        return MEMBER_SIGNATURE (_output_args);
    }

protected:

    /** Initializes the FreenectVideoDepth plugin.
     *
     * @param   Index            Device index (0 is the first); default 0.
     * @param   VideoFormat      Video format to use; default FREENECT_VIDEO_RGB.
     * @param   DepthFormat      Depth format to use; default FREENECT_DEPTH_11BIT.
     */
    virtual void _Initialize (const Arguments& parameters);

    /** Executes the FreenectVideoDepth plugin.
     *
     * @param[in]   Tilt        New tilt angle in degrees (optional)
     * @param[in]   LEDStatus   New LED status (optional)
     * @param[out]  Image       RGB output image
     * @param[out]  Depth       Depth output matrix
     * @param[out]  TimeStamp   Timestamp as returned by video function
     */
    virtual void _Execute (Arguments& input_args, Arguments& output_args);

    Arguments _parameters, _input_args, _output_args;

}; // class FreenectVideoDepth: public pIn

} // namespace pIns
} // namespace pI

#endif // PI_PINS_FREENECTVIDEODEPTH_HPP
