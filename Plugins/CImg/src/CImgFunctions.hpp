/* CImgFunctions.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CIMG_FUNCTIONS_HPP
#define CIMG_FUNCTIONS_HPP

#include <vector>
#include <boost/shared_ptr.hpp>

#include <PureImage.hpp>

namespace pI {
namespace pIns {

class CImgFunctions: public pIn {

    typedef pIn Base;

public:

	CImgFunctions(Runtime& runtime);
	virtual ~CImgFunctions() {}

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(CImgFunctions)

    virtual const pI_int GetpInVersion() const {
        return 10100;
    }

	virtual const std::string GetAuthor() const {
        return "JKU Linz";
    }

	virtual const std::string GetDescription() const {
        return "pureImage wrapper for the CImg image processing library (version 1.4.6).";
    }

    virtual const std::string GetName() const {
        return "CImg/Functions";
    }

    virtual Arguments GetParameterSignature() const {
        return MEMBER_SIGNATURE(_parameters);
    }

    virtual Arguments GetInputSignature() const {
        return MEMBER_SIGNATURE(_input_args);
    }

    virtual Arguments GetOutputSignature() const {
        return MEMBER_SIGNATURE(_output_args);
    }

protected:

    virtual void _Initialize (const Arguments& parameters);

    virtual void _Execute (Arguments& input_args, Arguments& output_args);

	virtual void AdaptIOSignature();
	virtual void ExecuteCImg (Arguments& input_args, Arguments& output_args);


	// helper methods for argument signatures
	// generic images
	void InputImageSignature (
	 const std::string name,
	 const std::string description);

	void OutputImageSignature (
	 const std::string name,
	 const std::string description);

	// values
	void InputValueStringSignature (
	 const std::string name,
	 const std::string description,
	 const std::string value);
	void InputValueIntSignature (
	 const std::string name,
	 const std::string description,
	 const pI_int value);
	void InputValueDoubleSignature (
	 const std::string name,
	 const std::string description,
	 const pI_double value);
	void InputValueBoolSignature (
	 const std::string name,
	 const std::string description,
	 const pI_bool value);

	void OutputValueStringSignature (
	 const std::string name,
	 const std::string description,
	 const std::string value);
	void OutputValueIntSignature (
	 const std::string name,
	 const std::string description,
	 const pI_int value);
	void OutputValueDoubleSignature (
	 const std::string name,
	 const std::string description,
	 const pI_double value);
	void OutputValueBoolSignature (
	 const std::string name,
	 const std::string description,
	 const pI_bool value);

	Arguments _parameters, _input_args, _output_args;
	pI_int _method;

}; // class CImgPlugin: public pIn

} // namespace pIns
} // namespace pI

#endif // CIMG_FUNCTIONS_HPP
