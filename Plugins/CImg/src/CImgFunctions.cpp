/* CImgFunctions.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include "CImgFunctions.hpp"

#include <ArgumentChecks.hpp>

#include <Arguments/StringSelection.hpp>
#include <Arguments/ByteImage.hpp>
#include <Arguments/BoolValue.hpp>
#include <Arguments/DoubleValue.hpp>
#include <Arguments/IntValue.hpp>
#include <Arguments/StringValue.hpp>
#include <Arguments/IntPolygon.hpp>

// HACK: The following lines are, for some yet unknown (and definitely obscure) reason, necessary
//       so that MS VC10 can compile funcion ci.min() (index 17) and ci.max() (index 18).
// This is annoying, because there is no other reason why one would want to include <windows.h> here.
#ifdef _MSC_VER
#include <windows.h>
#endif

#undef min
#undef max
// End of HACK

#include <CImg.h>

using namespace cimg_library;

namespace pI {
namespace pIns {

CImg<unsigned char> CreateCImgFromImageArgument (ArgumentPtr arg) {

    // because of image argument padding, an image copy must be made
    ByteImage in_img (arg);
    pI_int width (in_img.GetWidth());
    pI_int height (in_img.GetHeight());
    pI_int channels (in_img.GetChannels());
    pI_int pitch (in_img.GetPitch());
    CImg<unsigned char> img (width, height, 1, channels);

    // Note: alternatively you can use the adapter
    //       to access the image data
    //       E.g.: rgb[r][c][0] -> in_img.GetData(r,c,0)
    pI_byte** * rgb = in_img.GetDataPtr();

    for (pI_int r = 0; r < height; ++r) {
        for (pI_int w = 0; w < width; ++w) {
            for (pI_int c = 0; c < channels; ++c) {
                img (w, r, 0, c) = rgb[r][w][c];
            }
        } // for (pI_int c = 0; c < width; ++c)
    } // for (pI_int r = 0; r < height; ++r)

    return img;
} // CImg<unsigned char> CreateCImgFromImageArgument(Argument& arg)


void WriteCImgToImgArg (CImg<unsigned char>& ci, ByteImage& img) {

    if (img.GetCPtr() == 0) {
        return;
    }

    pI_int width = ci.width();
    pI_int height = ci.height();
    pI_int channels = ci.spectrum();

    // Note: alternatively you can use the adapter
    //       to access the image data
    //       E.g.: rgb[h][w][c] = ci(w, h, 0, c)
    //              -> in_img.SetData(h,w,c, ci(w, h, 0, c))
    pI_byte** * rgb = img.GetDataPtr();

    for (pI_int h = 0; h < height; ++h) {
        for (pI_int w = 0; w < width; ++w) {
            if (channels == 1) {
                rgb[h][w][0] = ci (w, h, 0);
            }
            else
                for (pI_int c = 0; c < channels; ++c) {
                    rgb[h][w][c] = ci (w, h, 0, c);
                }
        } // for (pI_int w = 0; w < width; ++w)
    } // for (pI_int h = 0; h < height; ++h)

} // void WriteCImgToImgArg (CImg<unsigned char>& ci, ByteImage& img)

void ApplyCImgToImageArgument (CImg<unsigned char>& ci, ArgumentPtr arg, const pI_char* name = "", const pI_char* desc = "") {

    ByteImage outImg (arg);

    if (!outImg.HasData()) {
        outImg.SetWidth (ci.width())
              .SetHeight (ci.height())
              .SetChannels (ci.spectrum())
              .CreateData();
        // Now outImg.CreateData() throws...
        //if (outImg.CreateData() == pI_FALSE)
        //    throw pI::exception::ArgumentException("Could not create data for output image.");
        // Note: currently, this is done in ByteImage::CreateData()
        // outImg.SetPitch((((8 * ci.spectrum() * ci.width()) + 31) / 32) * 4);
    }

    //FIXME: dangerous!
    WriteCImgToImgArg (ci, outImg);
    outImg.SetName (name)
          .SetDescription (desc);

} // void ApplyFreeImageToArgument (FIBITMAP* bmp, Argument& arg)

CImgFunctions::CImgFunctions (Runtime& runtime) : Base (runtime), _method (-1) {

    StringSelection selector (runtime);
    selector.SetCount (79)
            .SetName ("Method")
            .SetDescription ("Method selector for CImg plugin.")
            // note: implicite create of data follows here
            .SetSymbols (0, "sqr")
            .SetSymbols (1, "exp")
            .SetSymbols (2, "log")
            .SetSymbols (3, "log10")
            .SetSymbols (4, "sin")
            .SetSymbols (5, "cos")
            .SetSymbols (6, "tan")
            .SetSymbols (7, "sinh")
            .SetSymbols (8, "cosh")
            .SetSymbols (9, "tanh")
            .SetSymbols (10, "asin")
            .SetSymbols (11, "acos")
            .SetSymbols (12, "atan")
            .SetSymbols (13, "atan2")
            .SetSymbols (14, "mul")
            .SetSymbols (15, "div")
            .SetSymbols (16, "pow")
            .SetSymbols (17, "min")
            .SetSymbols (18, "max")
            .SetSymbols (19, "kth_smallest")
            .SetSymbols (20, "median")
            .SetSymbols (21, "sum")
            .SetSymbols (22, "mean")
            .SetSymbols (23, "variance")
            .SetSymbols (24, "variance_noise")
            .SetSymbols (25, "MSE")
            .SetSymbols (26, "PSNR")
            .SetSymbols (27, "fill")
            .SetSymbols (28, "rand")
            .SetSymbols (29, "noise")
            .SetSymbols (30, "normalize") //?
            .SetSymbols (31, "norm") //?
            .SetSymbols (32, "cut")
            .SetSymbols (33, "quantize")
            .SetSymbols (34, "equalize")
            .SetSymbols (35, "resize") //!
            .SetSymbols (36, "mirror")
            .SetSymbols (37, "shift")
            .SetSymbols (38, "rotate")
            .SetSymbols (39, "warp")
            .SetSymbols (40, "crop")
            .SetSymbols (41, "autocrop")
            .SetSymbols (42, "channel")
            .SetSymbols (43, "append")
            .SetSymbols (44, "correlate")
            .SetSymbols (45, "convolve")
            .SetSymbols (46, "erode")
            .SetSymbols (47, "dilate")
            .SetSymbols (48, "blur")
            .SetSymbols (49, "blur_anisotropic")
            .SetSymbols (50, "blur_bilateral")
            .SetSymbols (51, "blur_patch")
            .SetSymbols (52, "blur_median")
            .SetSymbols (53, "sharpen")
            .SetSymbols (54, "laplacian")
            .SetSymbols (55, "structure_tensors")
            .SetSymbols (56, "edge_tensors")
            .SetSymbols (57, "displacement")
            .SetSymbols (58, "distance")
            .SetSymbols (59, "distance_eikonal")
            .SetSymbols (60, "haar")
            .SetSymbols (61, "FFT")
            .SetSymbols (62, "draw_point")
            .SetSymbols (63, "draw_line")
            .SetSymbols (64, "draw_arrow")
            .SetSymbols (65, "draw_spline")
            .SetSymbols (66, "draw_triangle")
            .SetSymbols (67, "draw_rectangle")
            .SetSymbols (68, "draw_polygon")
            .SetSymbols (69, "draw_circle")
            .SetSymbols (70, "draw_ellipse")
            .SetSymbols (71, "draw_image")
            .SetSymbols (72, "draw_text")
            .SetSymbols (73, "draw_quiver")
            .SetSymbols (74, "draw_axes")
            .SetSymbols (75, "draw_grid")
            .SetSymbols (76, "draw_fill")
            .SetSymbols (77, "draw_plasma")
            .SetSymbols (78, "draw_gaussian")
            .SetIndex (0);
    _parameters.push_back (selector);
}


/*virtual*/ void CImgFunctions::_Initialize (const Arguments& parameters) {

    CheckParameters (parameters, GetParameterSignature());

    _method = StringSelection (parameters[0]).GetIndex();
    AdaptIOSignature();
} // /*virtual*/ void CImgFunctions::_Initialize (...)


/*virtual*/ void CImgFunctions::_Execute (Arguments& input_args, Arguments& output_args) {

    if (_method < 0) {
        throw exception::NotInitializedException ("CImg plugin was not initialized.");
    }

    CheckInputArguments (input_args, GetInputSignature());
    CheckOutputArguments (output_args, GetOutputSignature());
    ExecuteCImg (input_args, output_args);
} // /*virtual*/ void CImgFunctions::_Execute (...)


/*virtual*/ void CImgFunctions::AdaptIOSignature() {

    _input_args.clear();
    _output_args.clear();

    switch (_method) {
        case 0: // sqr;
            InputImageSignature ("Sqr input image", "Input image to perform square root operation on");
            OutputImageSignature ("Sqr image", "Square root output image");
            break;
        case 1: // exp
            InputImageSignature ("Exp input image", "Input image to perform exponential operation on");
            OutputImageSignature ("Exp image", "Exponential output image");
            break;
        case 2: // log
            InputImageSignature ("Log input image", "Input image to perform logarithm operation on");
            OutputImageSignature ("Log image", "Logarithm output image");
            break;
        case 3: // log10
            InputImageSignature ("Log10 input image", "Input image to perform logarithm base 10 operation on");
            OutputImageSignature ("Log10 image", "Logarithm base 10 output image");
            break;
        case 4: // sin
            InputImageSignature ("Sin input image", "Input image to perform sinus operation on");
            OutputImageSignature ("Sin image", "Sinus output image");
            break;
        case 5: // cos
            InputImageSignature ("Cos input image", "Input image to perform cosinus operation on");
            OutputImageSignature ("Cos image", "Cosinus output image");
            break;
        case 6: // tan
            InputImageSignature ("Tan input image", "Input image to perform tangent operation on");
            OutputImageSignature ("Tan image", "Tangent output image");
            break;
        case 7: // sinh
            InputImageSignature ("Sinh input image", "Input image to perform hyperbolic sine operation on");
            OutputImageSignature ("Sinh image", "Hyperbolic sine output image");
            break;
        case 8: // cosh
            InputImageSignature ("Cosh input image", "Input image to perform hyperbolic cosine operation on");
            OutputImageSignature ("Cosh image", "Hyperbolic cosine output image");
            break;
        case 9: // tanh
            InputImageSignature ("Tanh input image", "Input image to perform hyperbolic tangent operation on");
            OutputImageSignature ("Tanh image", "Hyperbolic tangent output image");
            break;
        case 10: // asin
            InputImageSignature ("Asin input image", "Input image to perform arc-sine operation on");
            OutputImageSignature ("Asin image", "Arc-sine output image");
            break;
        case 11: // acos
            InputImageSignature ("Acos input image", "Input image to perform arc-cosine operation on");
            OutputImageSignature ("Acos image", "Arc-cosine output image");
            break;
        case 12: // atan
            InputImageSignature ("Atan input image", "Input image to perform arc-tangent operation on");
            OutputImageSignature ("Atan image", "Arc-tangent output image");
            break;
        case 13: // atan2
            InputImageSignature ("Atan2 first input image", "First input image to perform arc-tangent operation on");
            InputImageSignature ("Atan2 second input image", "Second input image to perform arc-tangent operation on");
            OutputImageSignature ("Atan2 image", "Arc-tangent output image");
            break;
        case 14: // mul
            InputImageSignature ("Mul first input image", "First input image to perform multiplication operation on");
            InputImageSignature ("Mul second input image", "Second input image to perform multiplication operation on");
            OutputImageSignature ("Mul image", "Multiplication output image");
            break;
        case 15: // div
            InputImageSignature ("Div first input image", "First input image to perform division operation on");
            InputImageSignature ("Div second input image", "Second input image to perform division operation on");
            OutputImageSignature ("Div image", "Division output image");
            break;
        case 16: // pow
            InputImageSignature ("Pow first input image", "First input image to perform power operation on");
            InputImageSignature ("Pow second input image", "Second input image to perform power operation on");
            OutputImageSignature ("Pow image", "Power output image");
            break;
        case 17: // min
            InputImageSignature ("Min first input image", "First input image to perform minimum operation on");
            InputImageSignature ("Min second input image", "Second input image to perform minimum operation on");
            OutputImageSignature ("Min image", "Minimum output image");
            break;
        case 18: // max
            InputImageSignature ("Max first input image", "First input image to perform maximum operation on");
            InputImageSignature ("Max second input image", "Second input image to perform maximum operation on");
            OutputImageSignature ("Max image", "Maximum output image");
            break;
        case 19: // kth_smallest
            InputImageSignature ("Input image", "Input image to retrieve k-th smallest value from");
            InputValueIntSignature ("k", "The k-th smallest value", 0);
            OutputValueIntSignature ("k-th smallest value", "The k-th smallest value of the image", 0);
            break;
        case 20: // median
            InputImageSignature ("Input image", "Input image to retrieve median value from");
            OutputValueIntSignature ("Median value", "Median value of the image", 0);
            break;
        case 21: // sum
            InputImageSignature ("Input image", "Input image to retrieve pixel sum from");
            OutputValueDoubleSignature ("Sum", "Pixel sum of the image", 0.0);
            break;
        case 22: // mean
            InputImageSignature ("Input image", "Input image to retrieve mean value from");
            OutputValueDoubleSignature ("Mean", "Mean pixel value of the image", 0.0);
            break;
        case 23: // variance
            InputImageSignature ("Input image", "Input image to retrieve variance value from");
            OutputValueDoubleSignature ("Variance", "Variance value of the image", 0.0);
            break;
        case 24: // variance_noise
            InputImageSignature ("Input image", "Input image to retrieve noise variance estimation from");
            OutputValueDoubleSignature ("Noise variance", "Noise variance value", 0.0);
            break;
        case 25: // MSE
            InputImageSignature ("First input image", "First input image for MSE calculation.");
            InputImageSignature ("Second input image", "Second input image for MSE calculation.");
            OutputValueDoubleSignature ("MSE", "Mean squared error between two images", 0.0);
            break;
        case 26: // PSNR
            InputImageSignature ("First input image", "First input image for PSNR calculation.");
            InputImageSignature ("Second input image", "Second input image for PSNR calculation.");
            OutputValueDoubleSignature ("MSE", "PSNR between two images", 0.0);
            break;
        case 27: // fill
            InputImageSignature ("Input image", "Input image to fill with constant value");
            InputValueIntSignature ("Graylevel", "Graylevel value to fill image with", 0);
            OutputValueIntSignature ("Filled image", "Graylevel value filled image", 0);
            break;
        case 28: // rand
            InputImageSignature ("Input image", "Input image to fill with random value");
            InputValueIntSignature ("Lower value", "Lower graylevel value to fill image randomly with", 0);
            InputValueIntSignature ("Upper value", "Upper graylevel value to fill image randomly with", 255);
            OutputValueIntSignature ("Filled image", "Randomly graylevel filled image", 0);
            break;
        case 29: // noise
            InputImageSignature ("Input image", "Input image to add noise to");
            InputValueDoubleSignature ("Noise level", "Amplitude of the random additive noise", 0.1);
            InputValueIntSignature ("Method", "Noise type: 0 adds gaussion, 1 uniform, 2 salt and pepper, 3 poisson and 4 rician noise", 0);
            OutputImageSignature ("Noisy image", "Image with noise");
            break;
        case 30: // normalize
            InputImageSignature ("Input image", "Input image to normalize");
            InputValueIntSignature ("Lower value", "Lower threshold for image normalization", 0);
            InputValueIntSignature ("Upper value", "Upper threshold for image normalization", 255);
            OutputImageSignature ("Normalized image", "Normalized image");
            break;
        case 31: // norm
            InputImageSignature ("Input image", "Input image to compute L2-norm of");
            InputValueIntSignature ("Norm type", "Norm type to apply: 0 = LInf, 1 = L1, 2 = L2", 2);
            OutputImageSignature ("Normalized image", "Normalized image");
            break;
        case 32: // cut
            InputImageSignature ("Input image", "Input image to cut");
            InputValueIntSignature ("Lower value", "Lower threshold for image pixel values", 0);
            InputValueIntSignature ("Upper value", "Upper threshold for image pixel values", 255);
            OutputImageSignature ("Cut image", "Cut image");
            break;
        case 33: // quantize
            InputImageSignature ("Input image", "Input image to quantize");
            InputValueIntSignature ("NB levels", "Quantization levels", 10);
            OutputImageSignature ("Quantized image", "Quantized image");
            break;
        case 34: // equalize
            InputImageSignature ("Input image", "Input image to equalize");
            InputValueIntSignature ("NB levels", "Number of histogram levels for equalization", 20);
            InputValueIntSignature ("Lower value", "Lower threshold for image pixel values", 0);
            InputValueIntSignature ("Upper value", "Upper threshold for image pixel values", 0);
            OutputImageSignature ("Equalized image", "Equalized image");
            break;
        case 35: // resize
            InputImageSignature ("Input image", "Input image to resize");
            InputValueIntSignature ("Width", "Desired width", 100);
            InputValueIntSignature ("Height", "Desired height", 100);
            InputValueIntSignature ("Interpolation type", "Interpolation method: 0 = no interpolation, 1 = bloc, 2 = moving average, 3 = linear, 4 = grid, 5 = bi-cubic", 1);
            OutputImageSignature ("Resized image", "Resized image");
            break;
        case 36: // mirror
            InputImageSignature ("Input image", "Input image to mirror");
            InputValueIntSignature ("Axis", "Mirror axis", 0);
            OutputImageSignature ("Mirrored image", "Mirrored image");
            break;
        case 37: // shift
            InputImageSignature ("Input image", "Input image to shift");
            InputValueIntSignature ("Delta X", "Delta X", 1);
            InputValueIntSignature ("Delty Y", "Delty Y", 1);
            OutputImageSignature ("Shifted image", "Shifted image");
            break;
        case 38: // rotate
            InputImageSignature ("Input image", "Input image to rotate");
            InputValueDoubleSignature ("Angle", "Angle to rotate image", 90.0);
            InputValueIntSignature ("Method", "Rotation type: 0 means zero values at borders, 1 nearest pixel, 2 cyclic", 0);
            OutputImageSignature ("Rotated image", "Rotated image");
            break;
        case 39: // warp
            InputImageSignature ("First input image", "First input image for warp");
            InputImageSignature ("Second input image", "Second input image for warp");
            InputValueBoolSignature ("Relative", "Relative warp", pI_FALSE);
            InputValueBoolSignature ("Interpolation", "Interpolation", pI_TRUE);
            OutputImageSignature ("Warped image", "Warped image");
            break;
        case 40: // crop
            InputImageSignature ("Input image", "Input image to crop");
            InputValueIntSignature ("X0", "X0 point", 0);
            InputValueIntSignature ("Y0", "Y0 point", 0);
            InputValueIntSignature ("X1", "X1 point", 10);
            InputValueIntSignature ("Y1", "Y1 point", 10);
            OutputImageSignature ("Cropped image", "Cropped image");
            break;
        case 41: // autocrop
            InputImageSignature ("Input image", "Input image to autocrop");
            InputValueIntSignature ("Background value", "Background value", 32);
            OutputImageSignature ("Cropped image", "Cropped image");
            break;
        case 42: // channel
            InputImageSignature ("Input image", "Input image to extract channel from");
            InputValueIntSignature ("Channel", "Channel to extract", 0);
            OutputImageSignature ("Single channel image", "Single channel image");
            break;
        case 43: // append
            InputImageSignature ("First input image", "First input image for append");
            InputImageSignature ("Second input image", "Second input image for append");
            InputValueIntSignature ("Axis", "Append axis", 0);
            OutputImageSignature ("Combined image", "Combined image");
            break;
        case 44: // correlate
            InputImageSignature ("First input image", "First input image for correlation");
            InputImageSignature ("Second input image", "Second input image for correlation");
            OutputImageSignature ("Correlated image", "Correlated image");
            break;
        case 45: // convolve
            InputImageSignature ("First input image", "First input image for convolution");
            InputImageSignature ("Second input image", "Second input image for convolution");
            OutputImageSignature ("Convolved image", "Convolved image");
            break;
        case 46: // erode
            InputImageSignature ("Input image", "Input image to erode");
            InputValueIntSignature ("Size", "Size of square structuring element", 3);
            OutputImageSignature ("Eroded image", "Eroded image");
            break;
        case 47: // dilate
            InputImageSignature ("Input image", "Input image to dilate");
            InputValueIntSignature ("Size", "Size of square structuring element", 3);
            OutputImageSignature ("Dilated image", "Dilated image");
            break;
        case 48: // blur
            InputImageSignature ("Blur input image", "Input image to perform blur operation on");
            InputValueDoubleSignature ("Sigma value", "Sigma value for blur operation", 1.0);
            InputValueBoolSignature ("Cond", "Cond flag for blur operation", pI_FALSE);
            OutputImageSignature ("Blurred image", "Blur output image");
            break;
        case 49: // blur_anisotropic
            InputImageSignature ("Blur input image", "Input image to perform blur operation on");
            InputValueDoubleSignature ("Amplitude", "Anisotropic blur amplitude", 0.5);
            InputValueDoubleSignature ("Sharpness", "Blur sharpness", 0.7);
            InputValueDoubleSignature ("Anisotropy", "Blur anisotropy", 0.6);
            InputValueDoubleSignature ("Alpha", "Blur alpha", 0.6);
            InputValueDoubleSignature ("Sigma", "Blur sigma", 1.1);
            InputValueDoubleSignature ("dl", "Blur dl", 0.8);
            InputValueDoubleSignature ("da", "Blur da", 30.0);
            OutputImageSignature ("Blurred image", "Blur output image");
            break;
        case 50: // blur_bilateral
            InputImageSignature ("Blur input image", "Input image to perform blur operation on");
            InputValueDoubleSignature ("Sigma s", "Blur sigma s", 0.1);
            InputValueDoubleSignature ("Sigma r", "Blur sigma r", 0.1);
            OutputImageSignature ("Blurred image", "Blur output image");
            break;
        case 51: // blur_patch
            InputImageSignature ("Blur input image", "Input image to perform blur operation on");
            InputValueDoubleSignature ("Sigma s", "Blur sigma s", 0.1);
            InputValueDoubleSignature ("Sigma p", "Blur sigma p", 0.1);
            InputValueIntSignature ("Size", "Patch size", 3);
            OutputImageSignature ("Blurred image", "Blur output image");
            break;
        case 52: // blur_median
            InputImageSignature ("Blur input image", "Input image to perform blur operation on");
            InputValueIntSignature ("n", "Median order", 3);
            OutputImageSignature ("Blurred image", "Blur output image");
            break;
        case 53: // sharpen
            InputImageSignature ("Sharpen input image", "Input image to perform sharpen operation on");
            InputValueDoubleSignature ("Amplitude", "Sharpen amplitude", 0.5);
            OutputImageSignature ("Sharpened image", "Sharpened output image");
            break;
        case 54: // laplacian
            InputImageSignature ("Laplacian input image", "Input image to perform laplacian operation on");
            OutputImageSignature ("Laplacian image", "Laplacian output image");
            break;
        case 55: // structure_tensors
            InputImageSignature ("Structure tensor input image", "Input image to perform structure tensor calculation on");
            InputValueIntSignature ("Scheme", "Tensor scheme", 3);
            OutputImageSignature ("Structure tensor output image", "Structure tensor output image");
            break;
        case 56: // edge_tensors
            InputImageSignature ("Edge tensor input image", "Input image to perform edge tensor calculation on");
            InputValueDoubleSignature ("Sharpness", "Blur sharpness", 0.7);
            InputValueDoubleSignature ("Anisotropy", "Blur anisotropy", 0.6);
            InputValueDoubleSignature ("Alpha", "Blur alpha", 0.6);
            InputValueDoubleSignature ("Sigma", "Blur sigma", 1.1);
            OutputImageSignature ("Edge tensor output image", "Edge tensor output image");
            break;
        case 57: // displacement
            InputImageSignature ("First input image", "First input image for displacement");
            InputImageSignature ("Second input image", "Second input image for displacement");
            InputValueDoubleSignature ("Smooth", "Displacement smooth", 0.1);
            InputValueDoubleSignature ("Precision", "Displacement precision", 0.1);
            OutputImageSignature ("Displacement output image", "Displacement output image");
            break;
        case 58: // distance
            InputImageSignature ("Input image", "Input image for Euclidian distance map");
            InputValueIntSignature ("Iso value", "Iso value of the resulting shape", 0);
            OutputImageSignature ("Output image", "Euclidian distance map output image");
            break;
        case 59: // distance_eikonal
            InputImageSignature ("Input image", "First input image for distance function");
            InputValueIntSignature ("Nb iterations", "Nb iterations", 10);
            InputValueDoubleSignature ("Band size", "Band size", 0.0);
            InputValueDoubleSignature ("Time step", "Time step", 0.5);
            OutputImageSignature ("Output image", "Eikonal distance output image");
            break;
        case 60: // haar
            InputImageSignature ("Input image", "Input image for Haar transform");
            InputValueBoolSignature ("Invert", "Set inverse of direct transform", pI_FALSE);
            InputValueIntSignature ("Nb scales", "Nb scales", 1);
            OutputImageSignature ("Output image", "Haar transformed output image");
            break;
        case 61: // FFT
            InputImageSignature ("Input image", "Input image for FFT transform");
            InputValueBoolSignature ("Invert", "Set inverse of direct transform", pI_FALSE);
            OutputImageSignature ("Output image", "FFT transformed output image");
            break;
        case 62: // draw_point
            InputImageSignature ("Input image", "Input image for drawing");
            InputValueIntSignature ("X", "Point x coordinate", 0);
            InputValueIntSignature ("Y", "Point y coordinate", 0);
            InputValueIntSignature ("Red", "Red colour component", 255);
            InputValueIntSignature ("Green", "Green colour component", 255);
            InputValueIntSignature ("Blue", "Blue colour component", 255);
            InputValueDoubleSignature ("Opacity", "Opacity", 1.0);
            OutputImageSignature ("Output image", "Drawed upon output image");
            break;
        case 63: // draw_line
            InputImageSignature ("Input image", "Input image for drawing");
            InputValueIntSignature ("X0", "Point 1 x coordinate", 0);
            InputValueIntSignature ("Y0", "Point 1 y coordinate", 0);
            InputValueIntSignature ("X1", "Point 2 x coordinate", 0);
            InputValueIntSignature ("Y1", "Point 2 y coordinate", 0);
            InputValueIntSignature ("Red", "Red colour component", 255);
            InputValueIntSignature ("Green", "Green colour component", 255);
            InputValueIntSignature ("Blue", "Blue colour component", 255);
            InputValueDoubleSignature ("Opacity", "Opacity", 1.0);
            OutputImageSignature ("Output image", "Drawed upon output image");
            break;
        case 64: // draw_arrow
            InputImageSignature ("Input image", "Input image for drawing");
            InputValueIntSignature ("X0", "Point 1 x coordinate (tail)", 0);
            InputValueIntSignature ("Y0", "Point 1 y coordinate (tail)", 0);
            InputValueIntSignature ("X1", "Point 2 x coordinate (head)", 0);
            InputValueIntSignature ("Y1", "Point 2 y coordinate (head)", 0);
            InputValueIntSignature ("Red", "Red colour component", 255);
            InputValueIntSignature ("Green", "Green colour component", 255);
            InputValueIntSignature ("Blue", "Blue colour component", 255);
            InputValueDoubleSignature ("Opacity", "Opacity", 1.0);
            InputValueDoubleSignature ("Head angle", "Arrow head angle", 30.0);
            InputValueDoubleSignature ("Head length", "Arrow head length", -10.0);
            OutputImageSignature ("Output image", "Drawed upon output image");
            break;
        case 65: // draw_spline
            InputImageSignature ("Input image", "Input image for drawing");
            InputValueIntSignature ("X0", "Point 1 x coordinate", 0);
            InputValueIntSignature ("Y0", "Point 1 y coordinate", 0);
            InputValueDoubleSignature ("U0", "Point 1 x starting velocity", 0.0);
            InputValueDoubleSignature ("V0", "Point 1 y starting velocity", 0.0);
            InputValueIntSignature ("X1", "Point 2 x coordinate", 0);
            InputValueIntSignature ("Y1", "Point 2 y coordinate", 0);
            InputValueDoubleSignature ("U1", "Point 2 x starting velocity", 0.0);
            InputValueDoubleSignature ("V1", "Point 2 y starting velocity", 0.0);
            InputValueIntSignature ("Red", "Red colour component", 255);
            InputValueIntSignature ("Green", "Green colour component", 255);
            InputValueIntSignature ("Blue", "Blue colour component", 255);
            InputValueDoubleSignature ("Opacity", "Opacity", 1.0);
            InputValueDoubleSignature ("Percision", "Precision", 1.0);
            OutputImageSignature ("Output image", "Drawed upon output image");
            break;
        case 66: // draw_triangle
            InputImageSignature ("Input image", "Input image for drawing");
            InputValueIntSignature ("X0", "Point 1 x coordinate", 0);
            InputValueIntSignature ("Y0", "Point 1 y coordinate", 0);
            InputValueIntSignature ("X1", "Point 2 x coordinate", 0);
            InputValueIntSignature ("Y1", "Point 2 y coordinate", 0);
            InputValueIntSignature ("X2", "Point 3 x coordinate", 0);
            InputValueIntSignature ("Y2", "Point 3 y coordinate", 0);
            InputValueIntSignature ("Red", "Red colour component", 255);
            InputValueIntSignature ("Green", "Green colour component", 255);
            InputValueIntSignature ("Blue", "Blue colour component", 255);
            InputValueDoubleSignature ("Opacity", "Opacity", 1.0);
            InputValueBoolSignature ("Filled", "If true, the triangle will be filled", pI_FALSE);
            OutputImageSignature ("Output image", "Drawed upon output image");
            break;
        case 67: // draw_rectangle
            InputImageSignature ("Input image", "Input image for drawing");
            InputValueIntSignature ("X0", "Point 1 x coordinate", 0);
            InputValueIntSignature ("Y0", "Point 1 y coordinate", 0);
            InputValueIntSignature ("X1", "Point 2 x coordinate", 0);
            InputValueIntSignature ("Y1", "Point 2 y coordinate", 0);
            InputValueIntSignature ("Red", "Red colour component", 255);
            InputValueIntSignature ("Green", "Green colour component", 255);
            InputValueIntSignature ("Blue", "Blue colour component", 255);
            InputValueDoubleSignature ("Opacity", "Opacity", 1.0);
            InputValueBoolSignature ("Filled", "If true, the rectangle will be filled", pI_FALSE);
            OutputImageSignature ("Output image", "Drawed upon output image");
            break;
        case 68: // draw_polygon
            InputImageSignature ("Input image", "Input image for drawing");
            _input_args.push_back (
                GetRuntime().CreateArgument (T_pI_Argument_IntPolygon, pI_TRUE, 0, "Polygon to draw", "Polygon to draw"));
            InputValueIntSignature ("Red", "Red colour component", 255);
            InputValueIntSignature ("Green", "Green colour component", 255);
            InputValueIntSignature ("Blue", "Blue colour component", 255);
            InputValueDoubleSignature ("Opacity", "Opacity", 1.0);
            InputValueBoolSignature ("Filled", "If true, the polygon will be filled", pI_FALSE);
            OutputImageSignature ("Output image", "Drawed upon output image");
            break;
        case 69: // draw_circle
            InputImageSignature ("Input image", "Input image for drawing");
            InputValueIntSignature ("X", "Point x coordinate", 0);
            InputValueIntSignature ("Y", "Point y coordinate", 0);
            InputValueIntSignature ("Radius", "Radius", 0);
            InputValueIntSignature ("Red", "Red colour component", 255);
            InputValueIntSignature ("Green", "Green colour component", 255);
            InputValueIntSignature ("Blue", "Blue colour component", 255);
            InputValueDoubleSignature ("Opacity", "Opacity", 1.0);
            InputValueBoolSignature ("Filled", "If true, the circle will be filled", pI_FALSE);
            OutputImageSignature ("Output image", "Drawed upon output image");
            break;
        case 70: // draw_ellipse
            InputImageSignature ("Input image", "Input image for drawing");
            InputValueIntSignature ("X", "Point x coordinate", 0);
            InputValueIntSignature ("Y", "Point y coordinate", 0);
            InputValueDoubleSignature ("R1", "Radius 1", 1.0);
            InputValueDoubleSignature ("R2", "Radius 2", 1.0);
            InputValueDoubleSignature ("Angle", "Angle of radius 1", 0.0);
            InputValueIntSignature ("Red", "Red colour component", 255);
            InputValueIntSignature ("Green", "Green colour component", 255);
            InputValueIntSignature ("Blue", "Blue colour component", 255);
            InputValueDoubleSignature ("Opacity", "Opacity", 1.0);
            InputValueBoolSignature ("Filled", "If true, the circle will be filled", pI_FALSE);
            OutputImageSignature ("Output image", "Drawed upon output image");
            break;
        case 71: // draw_image
            InputImageSignature ("Input image", "Input image for drawing");
            InputImageSignature ("Sprite image", "Sprite image to draw upon input image");
            InputValueIntSignature ("X", "Point x coordinate", 0);
            InputValueIntSignature ("Y", "Point y coordinate", 0);
            InputValueDoubleSignature ("Opacity", "Opacity", 1.0);
            OutputImageSignature ("Output image", "Drawed upon output image");
            break;
        case 72: // draw_text
            InputImageSignature ("Input image", "Input image for drawing");
            InputValueStringSignature ("Text", "Text to draw", "");
            InputValueIntSignature ("X", "Point x coordinate", 0);
            InputValueIntSignature ("Y", "Point y coordinate", 0);
            InputValueIntSignature ("Red", "Red colour component", 255);
            InputValueIntSignature ("Green", "Green colour component", 255);
            InputValueIntSignature ("Blue", "Blue colour component", 255);
            InputValueDoubleSignature ("Opacity", "Opacity", 1.0);
            InputValueIntSignature ("Size", "Font size", 11);
            OutputImageSignature ("Output image", "Drawed upon output image");
            break;
        case 73: // draw_quiver
            InputImageSignature ("Input image", "Input image for drawing");
            _input_args.push_back (
                GetRuntime().CreateArgument (T_pI_Argument_IntPolygon, pI_TRUE, 0, "Vector field", "Vector field to draw"));
            InputValueIntSignature ("Red", "Red colour component", 255);
            InputValueIntSignature ("Green", "Green colour component", 255);
            InputValueIntSignature ("Blue", "Blue colour component", 255);
            InputValueDoubleSignature ("Opacity", "Opacity", 1.0);
            InputValueIntSignature ("Sampling", "Length between each arrow", 25);
            InputValueDoubleSignature ("Factor", "Length factor of each arrow", -20.0);
            InputValueBoolSignature ("Arrows", "If true, the arrows will be drawn", pI_TRUE);
            OutputImageSignature ("Output image", "Drawed upon output image");
            break;
        case 74: // draw_axes
            InputImageSignature ("Input image", "Input image for drawing");
            InputValueIntSignature ("X0", "Point 1 x coordinate", 0);
            InputValueIntSignature ("Y0", "Point 1 y coordinate", 0);
            InputValueIntSignature ("X1", "Point 2 x coordinate", 0);
            InputValueIntSignature ("Y1", "Point 2 y coordinate", 0);
            InputValueIntSignature ("Red", "Red colour component", 255);
            InputValueIntSignature ("Green", "Green colour component", 255);
            InputValueIntSignature ("Blue", "Blue colour component", 255);
            InputValueDoubleSignature ("Opacity", "Opacity", 1.0);
            InputValueIntSignature ("SubX", "Subdivisions X", -60);
            InputValueIntSignature ("SubY", "Subdivisions Y", -60);
            InputValueDoubleSignature ("PrecX", "Precision X", 0);
            InputValueDoubleSignature ("PrecY", "Precision Y", 0);
            OutputImageSignature ("Output image", "Drawed upon output image");
            break;
        case 75: // draw_grid
            InputImageSignature ("Input image", "Input image for drawing");
            InputValueDoubleSignature ("DeltaX", "Delta X", 0.0);
            InputValueDoubleSignature ("DeltaY", "Delta Y", 0.0);
            InputValueDoubleSignature ("OffsetX", "Offset X", 0.0);
            InputValueDoubleSignature ("OffsetY", "Offset Y", 0.0);
            InputValueBoolSignature ("InvertX", "If true, X axis is inverted", pI_FALSE);
            InputValueBoolSignature ("InvertY", "If true, Y axis is inverted", pI_FALSE);
            InputValueIntSignature ("Red", "Red colour component", 255);
            InputValueIntSignature ("Green", "Green colour component", 255);
            InputValueIntSignature ("Blue", "Blue colour component", 255);
            InputValueDoubleSignature ("Opacity", "Opacity", 1.0);
            OutputImageSignature ("Output image", "Drawed upon output image");
            break;
        case 76: // draw_fill
            InputImageSignature ("Input image", "Input image for drawing");
            InputValueIntSignature ("X", "X coordinate of starting point", 0);
            InputValueIntSignature ("Y", "Y coordinate of starting point", 0);
            InputValueIntSignature ("Red", "Red colour component", 255);
            InputValueIntSignature ("Green", "Green colour component", 255);
            InputValueIntSignature ("Blue", "Blue colour component", 255);
            InputValueDoubleSignature ("Opacity", "Opacity", 1.0);
            InputValueDoubleSignature ("Sigma", "Sigma", 0.0);
            InputValueBoolSignature ("High connexity", "High connexity", pI_FALSE);
            OutputImageSignature ("Output image", "Drawed upon output image");
            break;
        case 77: // draw_plasma
            InputImageSignature ("Input image", "Input image for drawing");
            InputValueDoubleSignature ("Alpha", "Alpha", 1.0);
            InputValueDoubleSignature ("Beta", "Beta", 1.0);
            InputValueDoubleSignature ("Opacity", "Opacity", 1.0);
            OutputImageSignature ("Output image", "Drawed upon output image");
            break;
        case 78: // draw_gaussian
            InputImageSignature ("Input image", "Input image for drawing");
            InputValueDoubleSignature ("XC", "X-coordinate of gaussian center", 0.0);
            InputValueDoubleSignature ("YC", "Y-coordinate of gaussian center", 0.0);
            InputValueDoubleSignature ("Sigma", "Standard variation of distribution", 0.0);
            InputValueIntSignature ("Red", "Red colour component", 255);
            InputValueIntSignature ("Green", "Green colour component", 255);
            InputValueIntSignature ("Blue", "Blue colour component", 255);
            InputValueDoubleSignature ("Opacity", "Opacity", 1.0);
            OutputImageSignature ("Output image", "Drawed upon output image");
            break;
    } // switch (_method)
} // /*virtual*/ void CImgFunctions:AdaptIOSignature()


/*virtual*/ void CImgFunctions::ExecuteCImg (Arguments& input_args, Arguments& output_args) {

    CImg<unsigned char> ci (CreateCImgFromImageArgument (input_args[0]));

    try {
        switch (_method) {
            case 0: { // sqr
                ci.sqr();
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 1: { // exp
                ci.exp();
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 2: { // log
                ci.log();
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 3: { // log10
                ci.log10();
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 4: { // sin
                ci.sin();
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 5: { // cos
                ci.cos();
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 6: { // tan
                ci.tan();
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 7: { // sinh
                ci.sinh();
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 8: { // cosh
                ci.cosh();
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 9: { // tanh
                ci.tanh();
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 10: { // asin
                ci.asin();
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 11: { // acos
                ci.acos();
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 12: { // atan
                ci.atan();
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 13: { // atan2
                CImg<unsigned char> ci2 (CreateCImgFromImageArgument (input_args[1]));
                ci.atan2 (ci2);
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 14: { // mul
                CImg<unsigned char> ci2 (CreateCImgFromImageArgument (input_args[1]));
                ci.mul (ci2);
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 15: { // div
                CImg<unsigned char> ci2 (CreateCImgFromImageArgument (input_args[1]));
                ci.div (ci2);
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 16: { // pow
                CImg<unsigned char> ci2 (CreateCImgFromImageArgument (input_args[1]));
                ci.pow (ci2);
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 17: { // min
                CImg<unsigned char> ci2 (CreateCImgFromImageArgument (input_args[1]));
                ci.min (ci2);
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }

            case 18: { // max
                CImg<unsigned char> ci2 (CreateCImgFromImageArgument (input_args[1]));
                ci.max (ci2);
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 19: { // kth_smallest
                // !!!
                IntValue (output_args[0]).SetData (
                    ci.kth_smallest (IntValue (input_args[1]).GetData()));
                break;
            }
            case 20: { // median
                // !!!
                IntValue (output_args[0]).SetData (ci.median());
                break;
            }
            case 21: { // sum
                // !!!
                DoubleValue (output_args[0]).SetData (ci.sum());
                break;
            }
            case 22: { // mean
                // !!!
                DoubleValue (output_args[0]).SetData (ci.mean());
                break;
            }
            case 23: { // variance
                // !!!
                DoubleValue (output_args[0]).SetData (ci.variance());
                break;
            }
            case 24: { // variance_noise
                // !!!
                DoubleValue (output_args[0]).SetData (ci.variance_noise());
                break;
            }
            case 25: { // MSE
                CImg<unsigned char> ci2 (CreateCImgFromImageArgument (input_args[1]));
                DoubleValue (output_args[0]).SetData (ci.MSE (ci2));
                break;
            }
            case 26: { // PSNR
                CImg<unsigned char> ci2 (CreateCImgFromImageArgument (input_args[1]));
                DoubleValue (output_args[0]).SetData (ci.PSNR (ci2));
                break;
            }
            case 27: { // fill
                ci.fill (output_args[1].get()->data.IntValue->data);
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 28: { // rand
                ci.rand (
                    IntValue (input_args[1]).GetData(),
                    IntValue (input_args[2]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 29: { // noise
                ci.noise (
                    DoubleValue (input_args[1]).GetData(),
                    IntValue (input_args[2]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 30: { // normalize
                ci.normalize (
                    IntValue (input_args[1]).GetData(),
                    IntValue (input_args[2]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 31: { // norm
                ci.norm (IntValue (input_args[1]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 32: { // cut
                ci.cut (
                    IntValue (input_args[1]).GetData(),
                    IntValue (input_args[2]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 33: { // quantize
                ci.quantize (IntValue (input_args[1]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 34: { // equalize
                ci.equalize (
                    IntValue (input_args[1]).GetData(),
                    IntValue (input_args[2]).GetData(),
                    IntValue (input_args[3]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 35: { // resize
                ci.resize (
                    IntValue (input_args[1]).GetData(),
                    IntValue (input_args[2]).GetData(),
                    -100,
                    -100,
                    IntValue (input_args[3]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 36: { // mirror
                char axis = 'x';

                if (IntValue (input_args[1]).GetData() == 1) {
                    axis = 'y';
                }
                else if (IntValue (input_args[1]).GetData() == 2) {
                    axis = 'z';
                }

                ci.mirror (axis);
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 37: { // shift
                ci.shift (
                    IntValue (input_args[1]).GetData(),
                    IntValue (input_args[2]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 38: { // rotate
                ci.rotate (
                    static_cast<float> (DoubleValue (input_args[1]).GetData()),
                    0,
                    IntValue (input_args[2]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 39: { // warp
                CImg<unsigned char> ci2 (CreateCImgFromImageArgument (input_args[1]));
                ci.warp (
                    ci2,
                    BoolValue (input_args[2]).GetData() == pI_TRUE ? true : false,
                    BoolValue (input_args[3]).GetData() == pI_TRUE ? true : false);
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 40: { // crop
                ci.crop (
                    IntValue (input_args[1]).GetData(),
                    IntValue (input_args[2]).GetData(),
                    IntValue (input_args[3]).GetData(),
                    IntValue (input_args[4]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 41: { // autocrop
                ci.autocrop (
                    IntValue (input_args[1]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 42: { // channel
                ci.channel (
                    IntValue (input_args[1]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 43: { // append
                CImg<unsigned char> ci2 (CreateCImgFromImageArgument (input_args[1]));
                char axis = 'x';

                if (IntValue (input_args[2]).GetData() == 1) {
                    axis = 'y';
                }
                else if (IntValue (input_args[2]).GetData() == 2) {
                    axis = 'z';
                }

                ci.append (ci2, axis);
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 44: { // correlate
                CImg<unsigned char> ci2 (CreateCImgFromImageArgument (input_args[1]));
                ci.correlate (ci2);
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 45: { // convolve
                CImg<unsigned char> ci2 (CreateCImgFromImageArgument (input_args[1]));
                ci.convolve (ci2);
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 46: { // erode
                ci.erode (
                    IntValue (input_args[1]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 47: { // dilate
                ci.dilate (
                    IntValue (input_args[1]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 48: { // blur
                ci.blur (
                    (float) DoubleValue (input_args[1]).GetData(),
                    BoolValue (input_args[2]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 49: { // blur_anisotropic
                ci.blur_anisotropic (
                    (float) DoubleValue (input_args[1]).GetData(),
                    (float) DoubleValue (input_args[2]).GetData(),
                    (float) DoubleValue (input_args[3]).GetData(),
                    (float) DoubleValue (input_args[4]).GetData(),
                    (float) DoubleValue (input_args[5]).GetData(),
                    (float) DoubleValue (input_args[6]).GetData(),
                    (float) DoubleValue (input_args[7]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 50: { // blur_bilateral
                ci.blur_bilateral (
                    (float) DoubleValue (input_args[1]).GetData(),
                    (float) DoubleValue (input_args[2]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 51: { // blur_patch
                ci.blur_patch (
                    (float) DoubleValue (input_args[1]).GetData(),
                    (float) DoubleValue (input_args[2]).GetData(),
                    IntValue (input_args[3]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 52: { // blur_median
                ci.blur_median (IntValue (input_args[1]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 53: { // sharpen
                ci.sharpen ( (float) DoubleValue (input_args[1]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 54: { // laplacian
                ci.laplacian();
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 55: { // structure_tensors
                ci.structure_tensors (IntValue (input_args[1]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 56: { // edge_tensors
                ci.edge_tensors (
                    (float) DoubleValue (input_args[1]).GetData(),
                    (float) DoubleValue (input_args[2]).GetData(),
                    (float) DoubleValue (input_args[3]).GetData(),
                    (float) DoubleValue (input_args[4]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 57: { // displacement
                CImg<unsigned char> ci2 (CreateCImgFromImageArgument (input_args[1]));
                ci.displacement (
                    ci2,
                    (float) DoubleValue (input_args[2]).GetData(),
                    (float) DoubleValue (input_args[3]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 58: { // distance
                ci.distance (IntValue (input_args[1]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 59: { // distance_eikonal
                ci.distance_eikonal (
                    IntValue (input_args[1]).GetData(),
                    (float) DoubleValue (input_args[2]).GetData(),
                    (float) DoubleValue (input_args[3]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 60: { // haar
                ci.haar (
                    BoolValue (input_args[1]).GetData() == pI_TRUE ? true : false,
                    IntValue (input_args[2]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 61: { // FFT
                CImg<unsigned char> imag;
                ci.FFT (ci, imag, BoolValue (input_args[1]).GetData() == pI_TRUE ? true : false);
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 62: { // draw_point
                unsigned char colour[3] = {
                    (unsigned char) IntValue (input_args[3]).GetData(),
                    (unsigned char) IntValue (input_args[4]).GetData(),
                    (unsigned char) IntValue (input_args[5]).GetData()
                };
                ci.draw_point (
                    IntValue (input_args[1]).GetData(),
                    IntValue (input_args[2]).GetData(),
                    colour,
                    (float) DoubleValue (input_args[6]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 63: { // draw_line
                unsigned char colour[3] = {
                    (unsigned char) IntValue (input_args[5]).GetData(),
                    (unsigned char) IntValue (input_args[6]).GetData(),
                    (unsigned char) IntValue (input_args[7]).GetData()
                };
                ci.draw_line (
                    IntValue (input_args[1]).GetData(),
                    IntValue (input_args[2]).GetData(),
                    IntValue (input_args[3]).GetData(),
                    IntValue (input_args[4]).GetData(),
                    colour,
                    (float) DoubleValue (input_args[8]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 64: { // draw_arrow
                unsigned char colour[3] = {
                    (unsigned char) IntValue (input_args[5]).GetData(),
                    (unsigned char) IntValue (input_args[6]).GetData(),
                    (unsigned char) IntValue (input_args[7]).GetData()
                };
                ci.draw_arrow (
                    IntValue (input_args[1]).GetData(),
                    IntValue (input_args[2]).GetData(),
                    IntValue (input_args[3]).GetData(),
                    IntValue (input_args[4]).GetData(),
                    colour,
                    (float) DoubleValue (input_args[8]).GetData(),
                    (float) DoubleValue (input_args[9]).GetData(),
                    (float) DoubleValue (input_args[10]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 65: { // draw_spline
#if cimg_version==146
                throw exception::ExecutionException ("CImg draw_spline method does not compile in version 1.4.6 and VS2008");
#else
                unsigned char colour[3] = {
                    (unsigned char) IntValue (input_args[9]).GetData(),
                    (unsigned char) IntValue (input_args[10]).GetData(),
                    (unsigned char) IntValue (input_args[11]).GetData()
                };
                ci.draw_spline (
                    IntValue (input_args[1]).GetData(),
                    IntValue (input_args[2]).GetData(),
                    (float) DoubleValue (input_args[3]).GetData(),
                    (float) DoubleValue (input_args[4]).GetData(),
                    IntValue (input_args[5]).GetData(),
                    IntValue (input_args[6]).GetData(),
                    (float) DoubleValue (input_args[7]).GetData(),
                    (float) DoubleValue (input_args[8]).GetData(),
                    colour,
                    (float) DoubleValue (input_args[12]).GetData(),
                    (float) DoubleValue (input_args[13]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
#endif // #if cimg_version==146
                break;
            }
            case 66: { // draw_triangle
                unsigned char colour[3] = {
                    (unsigned char) IntValue (input_args[7]).GetData(),
                    (unsigned char) IntValue (input_args[8]).GetData(),
                    (unsigned char) IntValue (input_args[9]).GetData()
                };

                if (BoolValue (input_args[11]).GetData() == pI_TRUE) {
                    // filled
                    ci.draw_triangle (
                        IntValue (input_args[1]).GetData(),
                        IntValue (input_args[2]).GetData(),
                        IntValue (input_args[3]).GetData(),
                        IntValue (input_args[4]).GetData(),
                        IntValue (input_args[5]).GetData(),
                        IntValue (input_args[6]).GetData(),
                        colour,
                        (float) DoubleValue (input_args[10]).GetData());
                }
                else {
                    // outlined
                    ci.draw_triangle (
                        IntValue (input_args[1]).GetData(),
                        IntValue (input_args[2]).GetData(),
                        IntValue (input_args[3]).GetData(),
                        IntValue (input_args[4]).GetData(),
                        IntValue (input_args[5]).GetData(),
                        IntValue (input_args[6]).GetData(),
                        colour,
                        (float) DoubleValue (input_args[10]).GetData(),
                        0);
                }

                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 67: { // draw_rectangle
                unsigned char colour[3] = {
                    (unsigned char) IntValue (input_args[5]).GetData(),
                    (unsigned char) IntValue (input_args[6]).GetData(),
                    (unsigned char) IntValue (input_args[7]).GetData()
                };

                if (BoolValue (input_args[9]).GetData() == pI_TRUE) {
                    // filled
                    ci.draw_rectangle (
                        IntValue (input_args[1]).GetData(),
                        IntValue (input_args[2]).GetData(),
                        IntValue (input_args[3]).GetData(),
                        IntValue (input_args[4]).GetData(),
                        colour,
                        (float) DoubleValue (input_args[8]).GetData());
                }
                else {
                    // outlined
                    ci.draw_rectangle (
                        IntValue (input_args[1]).GetData(),
                        IntValue (input_args[2]).GetData(),
                        IntValue (input_args[3]).GetData(),
                        IntValue (input_args[4]).GetData(),
                        colour,
                        (float) DoubleValue (input_args[8]).GetData(),
                        0);
                }

                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 68: { // draw_polygon
                unsigned char colour[3] = {
                    (unsigned char) IntValue (input_args[2]).GetData(),
                    (unsigned char) IntValue (input_args[3]).GetData(),
                    (unsigned char) IntValue (input_args[4]).GetData()
                };
                IntPolygon poly (input_args[1]);
                CImg<int> points (poly.GetCount(), 2);

                for (size_t lv = 0; lv < poly.GetCount(); ++lv) {
                    points (lv, 0) = poly.GetX (lv);
                    points (lv, 1) = poly.GetY (lv);
                }

                if (BoolValue (input_args[6]).GetData() == pI_TRUE) {
                    // filled
                    ci.draw_polygon (
                        points,
                        colour,
                        (float) DoubleValue (input_args[5]).GetData());
                }
                else {
                    // outlined
                    // filled
                    ci.draw_polygon (
                        points,
                        colour,
                        (float) DoubleValue (input_args[5]).GetData(),
                        0);
                }

                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 69: { // draw_circle
                unsigned char colour[3] = {
                    (unsigned char) IntValue (input_args[4]).GetData(),
                    (unsigned char) IntValue (input_args[5]).GetData(),
                    (unsigned char) IntValue (input_args[6]).GetData()
                };

                if (BoolValue (input_args[8]).GetData() == pI_TRUE) {
                    // filled
                    ci.draw_circle (
                        IntValue (input_args[1]).GetData(),
                        IntValue (input_args[2]).GetData(),
                        IntValue (input_args[3]).GetData(),
                        colour,
                        (float) DoubleValue (input_args[7]).GetData());
                }
                else {
                    // outlined
                    ci.draw_circle (
                        IntValue (input_args[1]).GetData(),
                        IntValue (input_args[2]).GetData(),
                        IntValue (input_args[3]).GetData(),
                        colour,
                        (float) DoubleValue (input_args[7]).GetData(),
                        0);
                }

                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 70: { // draw_ellipse
                unsigned char colour[3] = {
                    (unsigned char) IntValue (input_args[6]).GetData(),
                    (unsigned char) IntValue (input_args[7]).GetData(),
                    (unsigned char) IntValue (input_args[8]).GetData()
                };

                if (BoolValue (input_args[10]).GetData() == pI_TRUE) {
                    // filled
                    ci.draw_ellipse (
                        IntValue (input_args[1]).GetData(),
                        IntValue (input_args[2]).GetData(),
                        (float) DoubleValue (input_args[3]).GetData(),
                        (float) DoubleValue (input_args[4]).GetData(),
                        (float) DoubleValue (input_args[5]).GetData(),
                        colour,
                        (float) DoubleValue (input_args[9]).GetData());
                }
                else {
                    // outlined
                    ci.draw_ellipse (
                        IntValue (input_args[1]).GetData(),
                        IntValue (input_args[2]).GetData(),
                        (float) DoubleValue (input_args[3]).GetData(),
                        (float) DoubleValue (input_args[4]).GetData(),
                        (float) DoubleValue (input_args[5]).GetData(),
                        colour,
                        (float) DoubleValue (input_args[9]).GetData(),
                        0);
                }

                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 71: { // draw_image
                CImg<unsigned char> ci2 (CreateCImgFromImageArgument (input_args[1]));
                ci.draw_image (
                    IntValue (input_args[2]).GetData(),
                    IntValue (input_args[3]).GetData(),
                    ci2,
                    (float) DoubleValue (input_args[4]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 72: { // draw_text
                unsigned char colour[3] = {
                    (unsigned char) IntValue (input_args[4]).GetData(),
                    (unsigned char) IntValue (input_args[5]).GetData(),
                    (unsigned char) IntValue (input_args[6]).GetData()
                };
                unsigned char bg_colour[3] = { 0, 0, 0};
                ci.draw_text (
                    IntValue (input_args[2]).GetData(),
                    IntValue (input_args[3]).GetData(),
                    StringValue (input_args[1]).GetData(),
                    colour,
                    bg_colour,
                    (float) DoubleValue (input_args[7]).GetData(),
                    IntValue (input_args[8]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 73: { // draw_quiver
                throw exception::ExecutionException ("Operation not (yet) available.");
                /*
                InputImageSignature ("Input image", "Input image for drawing");
                _input_args.push_back (
                 Getruntime()->CreatePolygonIntArgument (
                  IP_Input, "Vector field", "Vector field to draw"));
                InputValueIntSignature ("Red", "Red colour component", 255);
                InputValueIntSignature ("Green", "Green colour component", 255);
                InputValueIntSignature ("Blue", "Blue colour component", 255);
                InputValueDoubleSignature ("Opacity", "Opacity", 1.0);
                InputValueIntSignature ("Sampling", "Length between each arrow", 25);
                InputValueDoubleSignature ("Factor", "Length factor of each arrow", -20.0);
                InputValueBoolSignature ("Arrows", "If true, the arrows will be drawn", pI_TRUE);
                OutputImageSignature ("Output image", "Drawed upon output image");
                */
                break;
            }
            case 74: { // draw_axes
                // workaround: this method is not available with 1.3.2
#if cimg_version > 132
                unsigned char colour[3] = {
                    (unsigned char) pI::IntValue (input_args[5]).GetData(),
                    (unsigned char) pI::IntValue (input_args[6]).GetData(),
                    (unsigned char) pI::IntValue (input_args[6]).GetData()
                };
                ci.draw_axes (
                    (float) DoubleValue (input_args[1]).GetData(),
                    (float) DoubleValue (input_args[3]).GetData(),
                    (float) DoubleValue (input_args[2]).GetData(),
                    (float) DoubleValue (input_args[4]).GetData(),
                    colour,
                    (float) DoubleValue (input_args[8]).GetData(),
                    IntValue (input_args[9]).GetData(),
                    IntValue (input_args[10]).GetData(),
                    (float) DoubleValue (input_args[11]).GetData(),
                    (float) DoubleValue (input_args[12]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
#else
                throw pI::exception::ExecutionException ("Not implemented.");
#endif
            }
            case 75: { // draw_grid
                unsigned char colour[3] = {
                    (unsigned char) IntValue (input_args[7]).GetData(),
                    (unsigned char) IntValue (input_args[8]).GetData(),
                    (unsigned char) IntValue (input_args[9]).GetData()
                };
                ci.draw_grid (
                    (float) DoubleValue (input_args[1]).GetData(),
                    (float) DoubleValue (input_args[2]).GetData(),
                    (float) DoubleValue (input_args[3]).GetData(),
                    (float) DoubleValue (input_args[4]).GetData(),
                    BoolValue (input_args[5]).GetData() == pI_TRUE ? true : false,
                    BoolValue (input_args[6]).GetData() == pI_TRUE ? true : false,
                    colour,
                    (float) IntValue (input_args[10]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 76: { // draw_fill
                unsigned char colour[3] = {
                    (unsigned char) IntValue (input_args[3]).GetData(),
                    (unsigned char) IntValue (input_args[4]).GetData(),
                    (unsigned char) IntValue (input_args[5]).GetData()
                };
                ci.draw_fill (
                    IntValue (input_args[1]).GetData(),
                    IntValue (input_args[2]).GetData(),
                    colour,
                    (float) DoubleValue (input_args[6]).GetData(),
                    (float) DoubleValue (input_args[7]).GetData(),
                    BoolValue (input_args[8]).GetData() == pI_TRUE ? true : false);
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 77: { // draw_plasma
                ci.draw_plasma (
                    (float) DoubleValue (input_args[1]).GetData(),
                    (float) DoubleValue (input_args[2]).GetData(),
                    (float) DoubleValue (input_args[3]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            case 78: { // draw_gaussian
                unsigned char colour[3] = {
                    (unsigned char) IntValue (input_args[4]).GetData(),
                    (unsigned char) IntValue (input_args[5]).GetData(),
                    (unsigned char) IntValue (input_args[6]).GetData()
                };
                ci.draw_gaussian (
                    (float) DoubleValue (input_args[1]).GetData(),
                    (float) DoubleValue (input_args[2]).GetData(),
                    (float) DoubleValue (input_args[3]).GetData(),
                    colour,
                    (float) DoubleValue (input_args[7]).GetData());
                ApplyCImgToImageArgument (ci, output_args[0]);
                break;
            }
            default:
                throw exception::ExecutionException ("Operation not (yet) available.");
        } // switch (_method)
    }
    catch (cimg_library::CImgException& ex) {
        // workaround: with older CImg the method was called ex.message()

# if cimg_version <= 132
        throw exception::ExecutionException (ex.message());
#else
        throw exception::ExecutionException (ex.what());
#endif
    }
} // /*virtual*/ void CImgFunctions:ExecuteCImg()



void CImgFunctions::InputImageSignature (
    const std::string name,
    const std::string description) {

    ByteImage arg (GetRuntime());
    arg.SetReadOnly (pI_FALSE)
       .SetConstraints (0)
       .SetName ( (pI_str) name.c_str())
       .SetDescription ( (pI_str) description.c_str());

    _input_args.push_back (arg);
} // void CImgFunctions::InputImageSignature (const std::string name, const std::string description)

void CImgFunctions::OutputImageSignature (
    const std::string name,
    const std::string description) {

    ByteImage arg (GetRuntime());
    arg.SetReadOnly (pI_FALSE)
       .SetConstraints (0)
       .SetName ( (pI_str) name.c_str())
       .SetDescription ( (pI_str) description.c_str());

    _output_args.push_back (arg);
} // void CImgFunctions::OutputImageSignature (const std::string name, const std::string description)

void CImgFunctions::InputValueStringSignature (
    const std::string name,
    const std::string description,
    const std::string value) {

    StringValue arg (GetRuntime());
    arg.SetReadOnly (pI_FALSE)
       .SetConstraints (0)
       .SetName ( (pI_str) name.c_str())
       .SetDescription ( (pI_str) description.c_str())
       .SetData ( (pI_str) value.c_str());

    _input_args.push_back (arg);
} // void CImgFunctions::InputValueStringSignature (...)

void CImgFunctions::InputValueIntSignature (
    const std::string name,
    const std::string description,
    const pI_int value) {

    IntValue arg (GetRuntime());
    arg.SetReadOnly (pI_FALSE)
       .SetConstraints (0)
       .SetName ( (pI_str) name.c_str())
       .SetDescription ( (pI_str) description.c_str())
       .SetData (value);

    _input_args.push_back (arg);
} // void CImgFunctions::InputValueIntSignature (...)

void CImgFunctions::InputValueDoubleSignature (
    const std::string name,
    const std::string description,
    const pI_double value) {

    DoubleValue arg (GetRuntime());
    arg.SetReadOnly (pI_FALSE)
       .SetConstraints (0)
       .SetName ( (pI_str) name.c_str())
       .SetDescription ( (pI_str) description.c_str())
       .SetData (value);

    _input_args.push_back (arg);
} // void CImgFunctions::InputValueDoubleSignature (...)

void CImgFunctions::InputValueBoolSignature (
    const std::string name,
    const std::string description,
    const pI_bool value) {

    BoolValue arg (GetRuntime());
    arg.SetReadOnly (pI_FALSE)
       .SetConstraints (0)
       .SetName ( (pI_str) name.c_str())
       .SetDescription ( (pI_str) description.c_str())
       .SetData (value);

    _input_args.push_back (arg);
} // void CImgFunctions::InputValueBoolSignature (...)

void CImgFunctions::OutputValueStringSignature (
    const std::string name,
    const std::string description,
    const std::string value) {

    StringValue arg (GetRuntime());
    arg.SetReadOnly (pI_FALSE)
       .SetConstraints (0)
       .SetName ( (pI_str) name.c_str())
       .SetDescription ( (pI_str) description.c_str())
       .SetData ( (pI_str) value.c_str());

    _output_args.push_back (arg);
} // void CImgFunctions::OutputValueStringSignature (...)

void CImgFunctions::OutputValueIntSignature (
    const std::string name,
    const std::string description,
    const pI_int value) {

    IntValue arg (GetRuntime());
    arg.SetReadOnly (pI_FALSE)
       .SetConstraints (0)
       .SetName ( (pI_str) name.c_str())
       .SetDescription ( (pI_str) description.c_str())
       .SetData (value);

    _output_args.push_back (arg);
} // void CImgFunctions::OutputValueIntSignature (...)

void CImgFunctions::OutputValueDoubleSignature (
    const std::string name,
    const std::string description,
    const pI_double value) {

    DoubleValue arg (GetRuntime());
    arg.SetReadOnly (pI_FALSE)
       .SetConstraints (0)
       .SetName ( (pI_str) name.c_str())
       .SetDescription ( (pI_str) description.c_str())
       .SetData (value);

    _output_args.push_back (arg);
} // void CImgFunctions::OutputValueDoubleSignature (...)

void CImgFunctions::OutputValueBoolSignature (
    const std::string name,
    const std::string description,
    const pI_bool value) {

    BoolValue arg (GetRuntime());
    arg.SetReadOnly (pI_FALSE)
       .SetConstraints (0)
       .SetName ( (pI_str) name.c_str())
       .SetDescription ( (pI_str) description.c_str())
       .SetData (value);

    _output_args.push_back (arg);
} // void CImgFunctions::OutputValueBoolSignature (...)

} // namespace pIns
} // namespace pI
