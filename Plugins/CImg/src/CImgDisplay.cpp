/* CImgDisplay.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <Arguments.hpp>
#include <ArgumentChecks.hpp>

#include <CImg.h>

#include "CImgDisplay.hpp"

namespace pI {
namespace pIns {


cimg_library::CImg<unsigned char>* CreateCImgImgFromImageArgument (ArgumentPtr arg) {

    // because of image argument padding, an image copy must be made
    pI_int width (arg->signature.setup.ByteImage->width);
    pI_int height (arg->signature.setup.ByteImage->height);
    pI_int channels (arg->signature.setup.ByteImage->channels);
    pI_int pitch (arg->data.ByteImage->pitch);
    cimg_library::CImg<unsigned char>* img = new cimg_library::CImg<unsigned char> (width, height, 1, channels);
    pI_byte** * rgb = arg->data.ByteImage->data;

    for (pI_int r = 0; r < height; ++r) {
        for (pI_int w = 0; w < width; ++w) {
            for (pI_int c = 0; c < channels; ++c) {
                (*img) (w, r, 0, c) = rgb[r][w][c];
                (*img) (w, r, 0, c) = rgb[r][w][c];
                (*img) (w, r, 0, c) = rgb[r][w][c];
            }
        } // for (pI_int c = 0; c < width; ++c)
    } // for (pI_int r = 0; r < height; ++r)

    return img;
} // CImg<unsigned char> CreateCImgImgFromImageArgument(Argument& arg)


CImgDisplay::CImgDisplay (Runtime& runtime) : Base (runtime), _display (0), _img (0) {
    _parameters.push_back (StringValue (runtime)
                           .SetName ("name")
                           .SetDescription ("Window name.")
                           .SetData ("Display window"));
    _parameters.push_back (BoolValue (runtime).SetName ("enable_live")
                           .SetDescription ("If false (default), displays image together with on-screen info, and enables resizing and zooming."
                                            "If true, displays image as-is. Use this for subsequent calls of the plugin, i.e. for live images.")
                           .SetData (pI_FALSE));
    _parameters.push_back (BoolValue (runtime).SetName ("reserved")
                           .SetDescription ("Reserved for future use of swap_bytes.")
                           .SetData (pI_FALSE));
    _parameters.push_back (IntValue (runtime).SetName ("x")
                           .SetDescription ("Horizontal position of top-left corner.")
                           .SetData (0));
    _parameters.push_back (IntValue (runtime).SetName ("y")
                           .SetDescription ("Vertical position of top-left corner.")
                           .SetData (0));
}


/*virtual*/ CImgDisplay::~CImgDisplay() {

    if (_display != 0) {
        cimg_library::CImgDisplay* cidisp = reinterpret_cast<cimg_library::CImgDisplay* > (_display);
        delete cidisp;

        _display = 0;
    }

    if (_img != 0) {
        cimg_library::CImg<unsigned char>* ciimg =
            reinterpret_cast<cimg_library::CImg<unsigned char>* > (_img);
        delete ciimg;

        _img = 0;
    }
}


/*virtual*/ void CImgDisplay::_Initialize (const Arguments& parameters) {

    CheckParameters (parameters, GetParameterSignature());
    _parameters = _runtime.CopyArguments (parameters);

    // check if input descriptor was already created, if so do nothing but return
    if (_input_args.size() == 1) {
        return;
    }

    // input signature
    _input_args.push_back (
        GetRuntime().CreateArgument (T_pI_Argument_ByteImage, pI_FALSE, 0, "Image to display", ""));
}


/*virtual*/ void CImgDisplay::_Execute (Arguments& input_args, Arguments& output_args) {

    // validate input arguments
    CheckInputArguments (input_args, GetInputSignature());

    if (_img != 0) {
        delete reinterpret_cast<cimg_library::CImg<unsigned char>* > (_img);
        _img = 0;
    }

    _img = CreateCImgImgFromImageArgument (input_args[0]);
    cimg_library::CImg<unsigned char>* ciimg =
        reinterpret_cast<cimg_library::CImg<unsigned char>* > (_img);

    // Create display, if necessary
    if (_display == 0) {
        StringValue title (_parameters[0]);
        _display = new cimg_library::CImgDisplay (*ciimg, title.GetData());
        
        cimg_library::CImgDisplay* cidisp = reinterpret_cast<cimg_library::CImgDisplay* > (_display);
        int posx = IntValue (_parameters[3]).GetData();
        int posy = IntValue (_parameters[4]).GetData();

        if (posx > 0 && posy > 0) {
            cidisp->move (posx, posy);
        }
    }

    cimg_library::CImgDisplay* cidisp = reinterpret_cast<cimg_library::CImgDisplay* > (_display);

    // Show image in this display
    // Note: method display(CImgDisplay&, const bool) waits for key or mouse input,
    // and hence is not suitable for live display. This caused issue #40.
    bool enable_live = BoolValue (_parameters[1]).GetData();

    if (enable_live) {
        ciimg->display (*cidisp);
    }
    else {
        ciimg->display (*cidisp, true);
    }
}

} // namespace pIns
} // namespace pI

