/* CImgCreate.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universit�t Linz,
 * Institut f�r Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <Arguments.hpp>
#include <ArgumentChecks.hpp>

#include <CImg.h>

#include "CImgCreate.hpp"

namespace pI {
namespace pIns {

CImgCreate::CImgCreate (Runtime& runtime) : Base (runtime) {

	_input_args.push_back (StringValue(runtime).SetName("Formula").SetDescription("Image creation formula"));
	_input_args.push_back (IntValue(runtime).SetName("Width").SetDescription("Image width").SetData(640));
	_input_args.push_back (IntValue(runtime).SetName("Height").SetDescription("Image height").SetData(480));
	_input_args.push_back (IntValue(runtime).SetName("Channels").SetDescription("Image channels").SetData(3));
	_output_args.push_back (ByteImage(runtime).SetName("Created image").SetDescription("Formula-created output image"));
}

/*virtual*/ CImgCreate::~CImgCreate() {}

/*virtual*/ void CImgCreate::_Initialize (const Arguments& parameters) {}

/*virtual*/ void CImgCreate::_Execute (Arguments& input_args, Arguments& output_args) {

    // validate input arguments
    CheckInputArguments (input_args, GetInputSignature());

	cimg_library::CImg<unsigned char> ci(
	 IntValue(input_args[1]).GetData(),		// width
	 IntValue(input_args[2]).GetData(),		// height
	 1,										// depth
	 IntValue(input_args[3]).GetData());	// spectrum
	ci = StringValue(input_args[0]).GetData();

	ByteImage outImg (output_args[0]);
    outImg.SetWidth (ci.width())
          .SetHeight (ci.height())
          .SetChannels (ci.spectrum())
		  .SetName("Created image")
		  .SetDescription(StringValue(input_args[0]).GetData())
          .CreateData();

	// copy CImg to ByteImage
	pI_int width = ci.width();
    pI_int height = ci.height();
    pI_int channels = ci.spectrum();
    pI_byte*** rgb = outImg.GetDataPtr();
    for (pI_int h = 0; h < height; ++h) {
        for (pI_int w = 0; w < width; ++w) {
            if (channels == 1) {
                rgb[h][w][0] = ci (w, h, 0);
            }
            else
                for (pI_int c = 0; c < channels; ++c) {
                    rgb[h][w][c] = ci (w, h, 0, c);
                }
        } // for (pI_int w = 0; w < width; ++w)
    } // for (pI_int h = 0; h < height; ++h)
}

} // namespace pIns
} // namespace pI

