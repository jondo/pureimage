/* PCLCoordinates.hpp
 *
 * Copyright 2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PI_PINS_PCLCOORDINATES_HPP
#define PI_PINS_PCLCOORDINATES_HPP

#include <PureImage.hpp>

namespace pI {
namespace pIns {

class PCLDepthToMeters: public pIn {

    typedef pIn Base;

public:

	PCLDepthToMeters(Runtime& runtime);
	virtual ~PCLDepthToMeters();

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(PCLDepthToMeters)

    virtual const pI_int GetpInVersion() const {
        return 10000;
    }

	virtual const std::string GetAuthor() const {
        return "JKU Linz";
    }

	virtual const std::string GetDescription() const {
        return "Convert depth matrix to a matrix containing real world distance in meters.";
    }

    virtual const std::string GetName() const {
        return "PCL/DepthToMeters";
    }

    virtual Arguments GetParameterSignature() const {
        return MEMBER_SIGNATURE(_parameters);
    }

    virtual Arguments GetInputSignature() const {
        return MEMBER_SIGNATURE(_input_args);
    }

    virtual Arguments GetOutputSignature() const {
        return MEMBER_SIGNATURE(_output_args);
    }

protected:
    
    virtual void _Initialize (const Arguments& parameters);

    virtual void _Execute (Arguments& input_args, Arguments& output_args);

	Arguments _parameters, _input_args, _output_args;

}; // class PCLDepthToMeters: public pIn


class PCLDepthToRealWorld: public pIn {

    typedef pIn Base;

public:

	PCLDepthToRealWorld(Runtime& runtime);
	virtual ~PCLDepthToRealWorld();

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(PCLDepthToRealWorld)

    virtual const pI_int GetpInVersion() const {
        return 10000;
    }

	virtual const std::string GetAuthor() const {
        return "JKU Linz";
    }

	virtual const std::string GetDescription() const {
        return "Convert depth matrix to a (x,y,z)-array containing real world distance in meters.";
    }

    virtual const std::string GetName() const {
        return "PCL/DepthToXYZ";
    }

    virtual Arguments GetParameterSignature() const {
        return MEMBER_SIGNATURE(_parameters);
    }

    virtual Arguments GetInputSignature() const {
        return MEMBER_SIGNATURE(_input_args);
    }

    virtual Arguments GetOutputSignature() const {
        return MEMBER_SIGNATURE(_output_args);
    }

protected:

    virtual void _Initialize (const Arguments& parameters);

    virtual void _Execute (Arguments& input_args, Arguments& output_args);

	Arguments _parameters, _input_args, _output_args;

}; // class PCLDepthToRealWorld: public pIn


} // namespace pIns
} // namespace pI

#endif // PI_PINS_PCLCOORDINATES_HPP
