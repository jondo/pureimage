/* PCLIO.cpp
 *
 * Copyright 2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include "PCLIO.hpp"

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>

#include <ArgumentChecks.hpp>
#include <Arguments/XYZArray.hpp>
#include <Arguments/StringValue.hpp>


namespace pI {
namespace pIns {


PCLSaveXYZ::PCLSaveXYZ(Runtime& runtime): Base(runtime) {
}

/*virtual*/ PCLSaveXYZ::~PCLSaveXYZ() {
}

/*virtual*/ void PCLSaveXYZ::_Initialize (const Arguments& parameters) {

	CheckParameters (parameters, _parameters);
	_parameters = _runtime.CopyArguments (parameters);

    _input_args.push_back (StringValue (GetRuntime()).SetName ("Filename").SetDescription ("Name of the file"));
    _input_args.push_back (XYZArray (GetRuntime()).SetName ("PointCloud").SetDescription ("Point cloud as (x,y,z)-array"));
}

/*virtual*/ void PCLSaveXYZ::_Execute (Arguments& input_args, Arguments& output_args) {

	CheckInputArguments (input_args, GetInputSignature());
	CheckOutputArguments (output_args, GetOutputSignature());

	XYZArray xyz (input_args[1]);
    pI_size count = xyz.GetCount();

    pcl::PointCloud<pcl::PointXYZ> cloud;

    for (pI_size k = 0; k < count; ++k) {
        cloud.push_back (pcl::PointXYZ (xyz.GetX(k), xyz.GetY(k), xyz.GetZ(k)));
    }

    StringValue cloudName(input_args[0]);

    pcl::io::savePCDFileASCII (cloudName.GetData(), cloud);
}

} // namespace pIns
} // namespace pI
