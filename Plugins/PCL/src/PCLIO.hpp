/* PCLIO.hpp
 *
 * Copyright 2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PI_PINS_PCLIO_HPP
#define PI_PINS_PCLIO_HPP

#include <PureImage.hpp>

namespace pI {
namespace pIns {

class PCLSaveXYZ: public pIn {

    typedef pIn Base;

public:

	PCLSaveXYZ(Runtime& runtime);
	virtual ~PCLSaveXYZ();

	DECLARE_VIRTUAL_COPY_CONSTRUCTOR(PCLSaveXYZ)

    virtual const pI_int GetpInVersion() const {
        return 10000;
    }

	virtual const std::string GetAuthor() const {
        return "JKU Linz";
    }

	virtual const std::string GetDescription() const {
        return "Save (x,y,z)-array as point cloud in PCL format.";
    }

    virtual const std::string GetName() const {
        return "PCL/SaveXYZ";
    }

    virtual Arguments GetParameterSignature() const {
        return MEMBER_SIGNATURE(_parameters);
    }

    virtual Arguments GetInputSignature() const {
        return MEMBER_SIGNATURE(_input_args);
    }

    virtual Arguments GetOutputSignature() const {
        return MEMBER_SIGNATURE(_output_args);
    }

protected:
    
    virtual void _Initialize (const Arguments& parameters);

    virtual void _Execute (Arguments& input_args, Arguments& output_args);

	Arguments _parameters, _input_args, _output_args;

}; // class PCLSaveXYZ: public pIn

} // namespace pIns
} // namespace pI

#endif // PI_PLUGINS_PCL_PCLIO_HPP
