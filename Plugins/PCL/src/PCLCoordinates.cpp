/* PCLCoordinates.cpp
 *
 * Copyright 2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include "PCLCoordinates.hpp"

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

#include <ArgumentChecks.hpp>
#include <Arguments/ShortMatrix.hpp>
#include <Arguments/DoubleMatrix.hpp>
#include <Arguments/XYZArray.hpp>

#include <cmath>

namespace pI {
namespace pIns {

// Several methods how to convert Kinect depth to real world coordinates
// are suggested throughout the web.

// The most common method, mentioned for instance at
// http://nicolas.burrus.name/index.php/Research/KinectCalibration,
// is to take the inverse of a linear function of the depth.
double RawDepthToMeters_InvLinear (int depthValue) {
    const double k1 = 3.3309495161;
    const double k2 = -0.0030711016;

    if (depthValue < 2047) {
        return (1.0 / (k1 + k2 * (double) depthValue));
    }

    return 0.0;
}

// In http://tinyurl.com/6fxflsp, Stephane Magnenat mentions that using
// tan() is slightly better than the inverse.
double RawDepthToMeters_Tan1 (int depthValue) {
    const double k1 = 1.1863;
    const double k2 = 2842.5;
    const double k3 = 0.1236;

    if (depthValue < 2047) {
        return (k3 * tan (k1 + (double) depthValue / k2));
    }

    return 0.0;
}

//In http://vvvv.org/forum/the-kinect-thread, user marf reports this formula:
double RawDepthToMeters_Tan2 (int depthValue) {
    const double k1 = 0.5;
    const double k2 = 1024.;
    const double k3 = 33.825;
    const double k4 = 5.7;

    if (depthValue < 2047) {
        return 0.01 * (k3 * tan (k1 + (double) depthValue / k2) + k4);
    }

    return 0.0;
}

double RawDepthToMeters (int depthValue) {
    return RawDepthToMeters_Tan1 (depthValue);
}

//The following function is mentioned at http://graphics.stanford.edu/~mdfisher/Kinect.html
pcl::PointXYZ DepthToWorld (int x, int y, int depthValue) {
    const double fx_d = 1.0 / 5.9421434211923247e+02;
    const double fy_d = 1.0 / 5.9104053696870778e+02;
    const double cx_d = 3.3930780975300314e+02;
    const double cy_d = 2.4273913761751615e+02;

    pcl::PointXYZ result;
    double depth = RawDepthToMeters (depthValue);
    result.x = (float) ( (x - cx_d) * depth * fx_d);
    result.y = (float) ( (y - cy_d) * depth * fy_d);
    result.z = (float) (depth);
    return result;
}


PCLDepthToMeters::PCLDepthToMeters(Runtime& runtime): Base(runtime) {
}

/*virtual*/ PCLDepthToMeters::~PCLDepthToMeters() {
}

/*virtual*/ void PCLDepthToMeters::_Initialize (const Arguments& parameters) {

	CheckParameters (parameters, _parameters);
	_parameters = _runtime.CopyArguments (parameters);

    _input_args.push_back (ShortMatrix (GetRuntime()).SetName ("DepthMatrix").SetDescription ("Depth matrix"));
    _output_args.push_back (DoubleMatrix (GetRuntime()).SetName ("MetersMatrix").SetDescription ("Real world coordinates matrix in meters"));
}

/*virtual*/ void PCLDepthToMeters::_Execute (Arguments& input_args, Arguments& output_args) {

	CheckInputArguments (input_args, GetInputSignature());
	CheckOutputArguments (output_args, GetOutputSignature());

	ShortMatrix depth (input_args[0]);
    int width = depth.GetCols();
    int height = depth.GetRows();

    // Re-creates output matrix if it has data, but wrong dimensions
    DoubleMatrix out (output_args[0]);
    pI_bool do_create_data (pI_TRUE);

    if (out.HasData()) {
        if ( (out.GetCols() == width) && (out.GetRows() == height)) {
            do_create_data = pI_FALSE;
        }
    }

    if (do_create_data == pI_TRUE) {
        if (out.HasData()) {
            _runtime.GetCRuntime()->FreeArgumentData (
                _runtime.GetCRuntime(),
                output_args[0].get());
        }

        out.SetCols (width).SetRows (height);

		try {
			out.CreateData();
		} catch(exception::MemoryException) {
            throw exception::ExecutionException ("Failed to create matrix.");
        }
    }

    for (int j = 0; j < height; ++j) {
        for (int i = 0; i < width; ++i) {
            int dep = depth.GetData (j, i);
            out.SetData (j, i, RawDepthToMeters (dep));
        }
    }
}


PCLDepthToRealWorld::PCLDepthToRealWorld(Runtime& runtime): Base(runtime) {
}

/*virtual*/ PCLDepthToRealWorld::~PCLDepthToRealWorld() {
}

/*virtual*/ void PCLDepthToRealWorld::_Initialize (const Arguments& parameters) {

	CheckParameters (parameters, _parameters);
	_parameters = _runtime.CopyArguments (parameters);

    _input_args.push_back (ShortMatrix (GetRuntime()).SetName ("DepthMatrix").SetDescription ("Depth matrix"));
    _output_args.push_back (XYZArray (GetRuntime()).SetName ("XYZMeters").SetDescription ("(x,y,z)-array in real world coordinates in meters"));
}

/*virtual*/ void PCLDepthToRealWorld::_Execute (Arguments& input_args, Arguments& output_args) {

	CheckInputArguments (input_args, GetInputSignature());
	CheckOutputArguments (output_args, GetOutputSignature());

	ShortMatrix depth (input_args[0]);
    int width = depth.GetCols();
    int height = depth.GetRows();

    pcl::PointCloud<pcl::PointXYZ> cloud;

    for (int j = 0; j < height; ++j) {
        for (int i = 0; i < width; ++i) {
            int dep = depth.GetData (j, i);

            if (dep < 2048) {
                cloud.push_back (DepthToWorld (i, j, dep));
            }
        }
    }

    // Re-creates output array if it has data, but wrong dimensions
    XYZArray out (output_args[0]);
    pI_bool do_create_data (pI_TRUE);

    if (out.HasData()) {
        if (out.GetCount() == cloud.size()) {
            do_create_data = pI_FALSE;
        }
    }

    if (do_create_data == pI_TRUE) {
        if (out.HasData()) {
            _runtime.GetCRuntime()->FreeArgumentData (
                _runtime.GetCRuntime(),
                output_args[0].get());
        }

        out.SetCount (cloud.size());

		try {
			out.CreateData();
		} catch(exception::MemoryException) {
            throw exception::ExecutionException ("Failed to create array.");
        }
    }

    for (size_t k = 0; k < cloud.size(); ++k) {
        out.SetX (k, cloud.points[k].x);
        out.SetY (k, cloud.points[k].y);
        out.SetZ (k, cloud.points[k].z);
    }
}



} // namespace pIns
} // namespace pI

