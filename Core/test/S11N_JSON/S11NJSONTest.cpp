/* S11NJSONTest.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */ 

#include <PureImage.h>
#include <iostream>

#include <S11N_JSON/S11NJSONTest.hpp>


void SnJSONTest::SetUp()
{
    runtime = CreateCRuntime (pI_FALSE);
}

void SnJSONTest::TearDown()
{
    DestroyCRuntime (runtime, pI_FALSE);
}

TEST_F(SnJSONTest, TestArraySerialization) {

	Argument* arg;
	Argument* arg2;
	pI_size lv;

	arg = runtime->CreateArgument (runtime, T_pI_Argument_DoubleArray);
	arg->signature.setup.DoubleArray->count = 10;
	runtime->CreateArgumentData (runtime, arg);
	ASSERT_TRUE (arg->data.DoubleArray->data != 0);
	for (lv = 0; lv < 10; ++lv) {
		arg->data.DoubleArray->data[lv] = (pI_double) lv;
	}
	
	pI_int sn_len;
	pI_str sn = runtime->SerializeArgument (runtime, arg, &sn_len);
	arg2 = runtime->DeserializeArgument (runtime, sn, sn_len);
	for (lv = 0; lv < 10; ++lv) {
		ASSERT_TRUE (arg->data.DoubleArray->data[lv] == arg2->data.DoubleArray->data[lv]);
	}

	runtime->FreeArgument (runtime, arg, pI_TRUE);
	runtime->FreeArgument (runtime, arg2, pI_TRUE);
}

TEST_F(SnJSONTest, TestImageSerialization) {

	Argument* arg;
	Argument* arg2;
	pI_size w, h, c;

	arg = runtime->CreateArgument (runtime, T_pI_Argument_ByteImage);
	arg->signature.setup.ByteImage->height = 5;
	arg->signature.setup.ByteImage->width = 4;
	arg->signature.setup.ByteImage->channels = 3;
	arg->signature.constraints = runtime->CopyString (runtime, "rows == cols");
	runtime->CreateArgumentData (runtime, arg);
	arg->data.ByteImage->path = runtime->CopyString (runtime, "c:\\foo.gif");
	arg->data.ByteImage->pitch = 16;
	
	for (h = 0; h < arg->signature.setup.ByteImage->height; ++h) {
		for (w = 0; w < arg->signature.setup.ByteImage->width; ++w) {
			for (c = 0; c < arg->signature.setup.ByteImage->channels; ++c) {
				arg->data.ByteImage->data[h][w][c] = (pI_byte) c + 1;
			}
		}
	}

	pI_int sn_len;
	pI_str sn = runtime->SerializeArgument (runtime, arg, &sn_len);
	arg2 = runtime->DeserializeArgument (runtime, sn, sn_len);
	pI_str sn2 = runtime->SerializeArgument (runtime, arg2, &sn_len);
	
	/*arg2 = runtime->CopyArgument (runtime, arg, pI_TRUE);*/

	for (h = 0; h < arg->signature.setup.ByteImage->height; ++h) {
		for (w = 0; w < arg->signature.setup.ByteImage->width; ++w) {
			for (c = 0; c < arg->signature.setup.ByteImage->channels; ++c) {
				ASSERT_TRUE (arg->data.ByteImage->data[h][w][c] == arg2->data.ByteImage->data[h][w][c]);
			}
		}
	}

	ASSERT_TRUE (strcmp (sn, sn2) == 0);
	runtime->FreeString (runtime, sn);
	runtime->FreeString (runtime, sn2);

	runtime->FreeArgument (runtime, arg2, pI_TRUE);
	runtime->FreeArgument (runtime, arg, pI_TRUE);
}

TEST_F(SnJSONTest, TestComplexSerialization) {

	Argument* arg;
	Argument* arg_copy;
	Argument* arg_s11n;
	pI_size lv, lv2;

	arg = runtime->CreateArgument (runtime, T_pI_Argument_ComplexMatrix);
	arg->signature.setup.ComplexMatrix->rows = 2;
	arg->signature.setup.ComplexMatrix->cols = 3;

	runtime->CreateArgumentData (runtime, arg);
	ASSERT_TRUE(arg->data.ComplexMatrix->data != 0);
	
	for (lv = 0; lv < arg->signature.setup.ComplexMatrix->rows; ++lv) {
		for (lv2 = 0; lv2 < arg->signature.setup.ComplexMatrix->cols; ++lv2) {
			ASSERT_TRUE(&(arg->data.ComplexMatrix->data[lv][lv2]) != 0);
            arg->data.ComplexMatrix->data[lv][lv2].r = lv;
            arg->data.ComplexMatrix->data[lv][lv2].i = lv2;
		}
	}

	arg_copy = runtime->CopyArgument (runtime, arg, pI_TRUE);
	for (lv = 0; lv < arg->signature.setup.ComplexMatrix->rows; ++lv) {
		for (lv2 = 0; lv2 < arg->signature.setup.ComplexMatrix->cols; ++lv2) {
			ASSERT_TRUE(arg->data.ComplexMatrix->data[lv][lv2].r == arg_copy->data.ComplexMatrix->data[lv][lv2].r);
			ASSERT_TRUE(arg->data.ComplexMatrix->data[lv][lv2].i == arg_copy->data.ComplexMatrix->data[lv][lv2].i);
		}
	}

    pI_int str_len;
    pI_str sn = runtime->SerializeArgument (runtime, arg, &str_len);

	arg_s11n = runtime->DeserializeArgument (runtime, sn, strlen (sn));
	pI_str sn2 = runtime->SerializeArgument (runtime, arg_s11n, &str_len);

	for (lv = 0; lv < arg->signature.setup.ComplexMatrix->rows; ++lv) {
		for (lv2 = 0; lv2 < arg->signature.setup.ComplexMatrix->cols; ++lv2) {
			ASSERT_TRUE((pI_int) arg->data.ComplexMatrix->data[lv][lv2].r == (pI_int) arg_s11n->data.ComplexMatrix->data[lv][lv2].r);
			ASSERT_TRUE((pI_int) arg->data.ComplexMatrix->data[lv][lv2].i == (pI_int) arg_s11n->data.ComplexMatrix->data[lv][lv2].i);
		}
	}

	runtime->FreeArgument (runtime, arg, pI_TRUE);
	runtime->FreeArgument (runtime, arg_copy, pI_TRUE);
	runtime->FreeString (runtime, sn);
	runtime->FreeString (runtime, sn2);
}
