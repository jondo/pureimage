/* S11NJSONTest.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */ 

#ifndef SNJSONTEST_h__
#define SNJSONTEST_h__

#include <gtest/gtest.h>
#include <Runtime/Runtime.h>

class SnJSONTest : public testing::Test {
protected:  
    virtual void SetUp();
    virtual void TearDown();

    CRuntime* runtime;
};

#endif // SNJSONTEST_h__
