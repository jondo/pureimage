/* ConstraintParserTest.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */ 

#include <PureImage.h>
#include <iostream>

#include <ConstraintParser/ConstraintParserTest.hpp>


void ConstraintParserTest::SetUp()
{
    runtime = CreateCRuntime (pI_FALSE);

    Argument* arg;
    arg = runtime->CreateArgument (runtime, T_pI_Argument_DoubleArray);
    arg->signature.setup.DoubleArray->count = 10;
//    runtime->CreateArgumentData(runtime, arg);    
    array_arg = arg;

	/* removal of test argument breaks the following code:

	arg = runtime->CreateArgument (runtime, T_pI_Argument_Test);
    arg->signature.setup.Test->count_1 = 10;
    arg->signature.setup.Test->test_struct.value = 10;
    test_arg = arg;

	*/
}

void ConstraintParserTest::TearDown()
{
    runtime->FreeArgument (runtime, array_arg, pI_TRUE);
	//runtime->FreeArgument (runtime, test_arg, pI_TRUE);
    DestroyCRuntime (runtime, pI_FALSE);
}


TEST_F(ConstraintParserTest, array_arg_example) {

    std::string constraint = "count == 10";

	pI_int success = runtime->CheckArgumentConstraints (runtime, array_arg, (pI_str) constraint.c_str());
    ASSERT_TRUE(success == pI_TRUE);
}

TEST_F(ConstraintParserTest, should_fail_for_unknown_identifier) {

    std::string constraint = "cont == 10";

    pI_int success = runtime->CheckArgumentConstraints (runtime, array_arg, (pI_str) constraint.c_str());
    ASSERT_TRUE(success == pI_ERROR);
}

TEST_F(ConstraintParserTest, should_fail_for_string_to_int_implicite_cast) {

    std::string constraint = "count == '10'";

    pI_int success = runtime->CheckArgumentConstraints (runtime, array_arg, (pI_str) constraint.c_str());
    ASSERT_TRUE(success == pI_ERROR);
}

TEST_F(ConstraintParserTest, should_cast_double_val_for_int_var) {

    // this should result in an implicite cast as var count is of type integer
    // and dominates the type
    std::string constraint = "count == 10.5";

    pI_int success = runtime->CheckArgumentConstraints (runtime, array_arg, (pI_str) constraint.c_str());
    ASSERT_TRUE(success == pI_TRUE);
}

TEST_F(ConstraintParserTest, should_prefer_variable_type_before_value) {

    // The variable should be type dominant event if it is on the rightern side of expression
    // so it is expected that an implicite cast of the double value is done 
    // i.e., this resolves to (int)10.5  == 10
    std::string constraint = "10.5 == count";

    pI_int success = runtime->CheckArgumentConstraints (runtime, array_arg, (pI_str) constraint.c_str());
    ASSERT_TRUE(success == pI_TRUE);
}

TEST_F(ConstraintParserTest, should_evaluate_nested_expressions) {

    std::string constraint = "((10.5==10) || (10 ==count))";

	pI_int success = runtime->CheckArgumentConstraints (runtime, array_arg, (pI_str) constraint.c_str());
    ASSERT_TRUE(success == pI_TRUE);
}

TEST_F(ConstraintParserTest, should_accept_true_and_false_as_string) {

    std::string constraint = "(true && 'true') && !(false || 'false')";

    pI_int success = runtime->CheckArgumentConstraints (runtime, array_arg, (pI_str) constraint.c_str());
    ASSERT_TRUE(success == pI_TRUE);
}

/*
test case broken: no nested structure in argument available atm 
TEST_F(ConstraintParserTest, should_handle_nested_ids) {

	test_arg->signature.constraints = runtime->CopyString(runtime, "count_1 == test_struct.value");
    pI_int success = runtime->CheckArgumentConstraints (runtime, test_arg, test_arg->signature.constraints);
    ASSERT_TRUE(success == pI_TRUE);
    
    test_arg->signature.constraints = runtime->CopyString(runtime, "count_1 != test_struct.value");
    success = runtime->CheckArgumentConstraints (runtime, test_arg, test_arg->signature.constraints);
    ASSERT_FALSE(success == pI_TRUE);
    
}
*/
