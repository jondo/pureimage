/* CRuntimeTest.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */ 

#include <PureImage.h>
#include <Runtime/Internal/RuntimeHelperFunctions.h>
#include <iostream>

#include <CRuntime/CRuntimeTest.hpp>


void CRuntimeTest::SetUp()
{
    runtime = CreateCRuntime (pI_FALSE);
}

void CRuntimeTest::TearDown()
{
    DestroyCRuntime(runtime, pI_FALSE);
}

TEST_F(CRuntimeTest, TestIntValueArgumentCopy) {

	Argument* arg;
    Argument* arg_copy;

	arg = runtime->CreateArgument (runtime, T_pI_Argument_IntValue);
	runtime->CreateArgumentData (runtime, arg);
	ASSERT_TRUE(&(arg->data.IntValue->data) != 0);

    arg->data.IntValue->data = 4711;

	arg_copy = runtime->CopyArgument (runtime, arg, pI_TRUE);

	ASSERT_TRUE(arg_copy->data.IntValue->data == 4711);

	runtime->FreeArgument (runtime, arg, pI_TRUE);
	runtime->FreeArgument (runtime, arg_copy, pI_TRUE);
}

TEST_F(CRuntimeTest, TestArrayArgument) {

	Argument* arg;
	pI_size lv;

	arg = runtime->CreateArgument (runtime, T_pI_Argument_DoubleArray);
	arg->signature.setup.DoubleArray->count = 10;
	runtime->CreateArgumentData (runtime, arg);

	ASSERT_TRUE(arg->data.DoubleArray->data != 0);
	for (lv = 0; lv < 10; ++lv) {
		ASSERT_TRUE(&(arg->data.DoubleArray->data[lv]) != 0);
	}

	runtime->FreeArgument (runtime, arg, pI_TRUE);
}

TEST_F(CRuntimeTest, TestMatrixArgument) {

	Argument* arg;
	pI_size lv, lv2;

	arg = runtime->CreateArgument (runtime, T_pI_Argument_DoubleMatrix);
	arg->signature.setup.DoubleMatrix->rows = 4;
	arg->signature.setup.DoubleMatrix->cols = 5;
	runtime->CreateArgumentData (runtime, arg);
	ASSERT_TRUE(arg->data.DoubleMatrix->data != 0);

	for (lv = 0; lv < arg->signature.setup.DoubleMatrix->rows; ++lv) {
		for (lv2 = 0; lv2 < arg->signature.setup.DoubleMatrix->cols; ++lv2) {
			ASSERT_TRUE(&(arg->data.DoubleMatrix->data[lv][lv2]) != 0);
			arg->data.DoubleMatrix->data[lv][lv2] = (pI_double) lv * lv2;
		}
	}

	runtime->FreeArgument (runtime, arg, pI_TRUE);
}

TEST_F(CRuntimeTest, TestByteMatrixArgument) {
	Argument* arg;
	pI_size lv, lv2;

	arg = runtime->CreateArgument (runtime, T_pI_Argument_ByteMatrix);
	arg->signature.setup.ByteMatrix->rows = 3;
	arg->signature.setup.ByteMatrix->cols = 3;

	runtime->CreateArgumentData (runtime, arg);
	ASSERT_TRUE(arg->data.ByteMatrix->data != 0);
	
	for (lv = 0; lv < arg->signature.setup.ByteMatrix->rows; ++lv) {
		for (lv2 = 0; lv2 < arg->signature.setup.ByteMatrix->cols; ++lv2) {
			ASSERT_TRUE(&(arg->data.ByteMatrix->data[lv][lv2]) != 0);
		}
	}

	runtime->FreeArgument (runtime, arg, pI_TRUE);
}

TEST_F(CRuntimeTest, TestComplexMatrixArgument) {
	Argument* arg;
	Argument* arg_copy;
	pI_size lv, lv2;

	arg = runtime->CreateArgument (runtime, T_pI_Argument_ComplexMatrix);
	arg->signature.setup.ComplexMatrix->rows = 2;
	arg->signature.setup.ComplexMatrix->cols = 3;

	runtime->CreateArgumentData (runtime, arg);
	ASSERT_TRUE(arg->data.ComplexMatrix->data != 0);
	
	for (lv = 0; lv < arg->signature.setup.ComplexMatrix->rows; ++lv) {
		for (lv2 = 0; lv2 < arg->signature.setup.ComplexMatrix->cols; ++lv2) {
			ASSERT_TRUE(&(arg->data.ComplexMatrix->data[lv][lv2]) != 0);
            arg->data.ComplexMatrix->data[lv][lv2].r = lv;
            arg->data.ComplexMatrix->data[lv][lv2].i = lv2;
		}
	}

	arg_copy = runtime->CopyArgument (runtime, arg, pI_TRUE);
	for (lv = 0; lv < arg->signature.setup.ComplexMatrix->rows; ++lv) {
		for (lv2 = 0; lv2 < arg->signature.setup.ComplexMatrix->cols; ++lv2) {
			ASSERT_TRUE(arg->data.ComplexMatrix->data[lv][lv2].r == arg_copy->data.ComplexMatrix->data[lv][lv2].r);
			ASSERT_TRUE(arg->data.ComplexMatrix->data[lv][lv2].i == arg_copy->data.ComplexMatrix->data[lv][lv2].i);
		}
	}

	runtime->FreeArgument (runtime, arg, pI_TRUE);
	runtime->FreeArgument (runtime, arg_copy, pI_TRUE);
}

TEST_F(CRuntimeTest, TestStringSelectionArgumentCopy) {

	Argument* arg;
    Argument* arg_copy;
	pI_size lv;
    pI_int index;

	arg = runtime->CreateArgument (runtime, T_pI_Argument_StringSelection);
	arg->signature.setup.StringSelection->count = 2;

    runtime->CreateArgumentData (runtime, arg);
	ASSERT_TRUE(arg->data.StringSelection->symbols != 0);
	for (lv = 0; lv < 2; ++lv) {
		ASSERT_TRUE(&(arg->data.StringSelection->symbols[lv]) != 0);
	}

    arg->data.StringSelection->symbols[0] = runtime->CopyString(runtime, "foo");
    ASSERT_TRUE(strcmp(arg->data.StringSelection->symbols[0], "foo") == 0);
    arg->data.StringSelection->symbols[1] = runtime->CopyString(runtime, "bar");

    index = 1;
    arg->data.StringSelection->index = index;

	arg_copy = runtime->CopyArgument (runtime, arg, pI_TRUE);

	ASSERT_TRUE(arg_copy->data.StringSelection->index == index);
    ASSERT_TRUE(strcmp(arg_copy->data.StringSelection->symbols[1], "bar") == 0);

	runtime->FreeArgument (runtime, arg, pI_TRUE);
	runtime->FreeArgument (runtime, arg_copy, pI_TRUE);
}

TEST_F(CRuntimeTest, TestArrayArgumentCopy) {

	Argument* arg;
	Argument* arg_copy;
	pI_size lv;
	pI_double* field;

	arg = runtime->CreateArgument (runtime, T_pI_Argument_DoubleArray);
	arg->signature.setup.DoubleArray->count = 10;
	runtime->CreateArgumentData (runtime, arg);
	ASSERT_TRUE (arg->data.DoubleArray->data != 0);
	field = arg->data.DoubleArray->data;
	for (lv = 0; lv < arg->signature.setup.DoubleArray->count; ++lv) {
		field[lv] = (double) lv;
	}

	arg_copy = runtime->CopyArgument (runtime, arg, pI_TRUE);

	for (lv = 0; lv < arg->signature.setup.DoubleArray->count; ++lv) {
		ASSERT_TRUE (arg->data.DoubleArray->data[lv] == arg_copy->data.DoubleArray->data[lv]);
	}

	runtime->FreeArgument (runtime, arg_copy, pI_TRUE);
	runtime->FreeArgument (runtime, arg, pI_TRUE);
}

// removal of test argument breaks the following test case 
//TEST_F(CRuntimeTest, TestFetchSetupSymbol) {
//
//	Argument* arg;
//
//	enum _pIDataTypeClass rclass;
//	pI_int rindex;
//	pI_byte* rptr;
//
//	arg = runtime->CreateArgument (runtime, T_pI_Argument_Test);
//
//	rptr = CRuntime_FetchArgumentSetupSymbol (runtime, arg, "test_struct", &rclass, &rindex);
//	ASSERT_TRUE (rptr != 0);
//
//	rptr = CRuntime_FetchArgumentSetupSymbol (runtime, arg, "test_struct.value", &rclass, &rindex);
//	ASSERT_TRUE (rptr != 0);
//
//	runtime->FreeArgument (runtime, arg, pI_TRUE);
//}

TEST_F(CRuntimeTest, TestImageArgumentBasic) {
	Argument* arg;
	pI_size lv, lv2, lv3;

	arg = runtime->CreateArgument (runtime, T_pI_Argument_ByteImage);
	arg->signature.setup.ByteImage->width = 6;
	arg->signature.setup.ByteImage->height = 7;
	arg->signature.setup.ByteImage->channels = 3;

	runtime->CreateArgumentData (runtime, arg);
	ASSERT_TRUE(arg->data.ByteImage->data != 0);
	for (lv = 0; lv < arg->signature.setup.ByteImage->height; ++lv) {
		for (lv2 = 0; lv2 < arg->signature.setup.ByteImage->width; ++lv2) {
			for (lv3 = 0; lv3 < arg->signature.setup.ByteImage->channels; ++lv3) {
				arg->data.ByteImage->data[lv][lv2][lv3] = lv3 + 1;
			}
		}
	}
	
	runtime->FreeArgument (runtime, arg, pI_TRUE);
}

TEST_F(CRuntimeTest, TestImageArgumentCopy) {
	Argument* arg;
	Argument* arg_copy;
	pI_size lv, lv2, lv3;

	arg = runtime->CreateArgument (runtime, T_pI_Argument_ByteImage);
	arg->signature.setup.ByteImage->width = 6;
	arg->signature.setup.ByteImage->height = 7;
	arg->signature.setup.ByteImage->channels = 3;

	runtime->CreateArgumentData (runtime, arg);
	ASSERT_TRUE(arg->data.ByteImage->data != 0);
	for (lv = 0; lv < arg->signature.setup.ByteImage->height; ++lv) {
		for (lv2 = 0; lv2 < arg->signature.setup.ByteImage->width; ++lv2) {
			for (lv3 = 0; lv3 < arg->signature.setup.ByteImage->channels; ++lv3) {
				arg->data.ByteImage->data[lv][lv2][lv3] = lv3 + 1;
			}
		}
	}
	arg->data.ByteImage->path = runtime->CopyString (runtime, "c:\\foo.gif");
	arg->data.ByteImage->pitch = 20;
	
	arg_copy = runtime->CopyArgument (runtime, arg, pI_TRUE);
	for (lv = 0; lv < arg->signature.setup.ByteImage->height; ++lv) {
		for (lv2 = 0; lv2 < arg->signature.setup.ByteImage->width; ++lv2) {
			for (lv3 = 0; lv3 < arg->signature.setup.ByteImage->channels; ++lv3) {
				ASSERT_TRUE (arg->data.ByteImage->data[lv][lv2][lv3] == arg_copy->data.ByteImage->data[lv][lv2][lv3]);
			}
		}
	}
	ASSERT_TRUE (strcmp (arg->data.ByteImage->path, arg_copy->data.ByteImage->path) == 0);
	ASSERT_TRUE (arg->data.ByteImage->pitch == arg_copy->data.ByteImage->pitch);
	
	runtime->FreeArgument (runtime, arg, pI_TRUE);
	runtime->FreeArgument (runtime, arg_copy, pI_TRUE);
}

TEST_F(CRuntimeTest, TestPropertyMap) {

	Argument* arg1;
	Argument* arg2;
	Argument* arg3;
	pI_str s11n = 0;

	/* create and register three properties */
	arg1 = runtime->CreateArgument (runtime, T_pI_Argument_StringValue);
	runtime->CreateArgumentData (runtime, arg1);
	arg1->description.name = runtime->CopyString (runtime, "Test1");
	arg1->data.StringValue->data = runtime->CopyString (runtime, "Test1");
	runtime->StoreProperty (runtime, arg1, pI_TRUE);

	arg3 = runtime->CreateArgument (runtime, T_pI_Argument_StringValue);
	runtime->CreateArgumentData (runtime, arg3);
	arg3->description.name = runtime->CopyString (runtime, "Test3");
	arg3->data.StringValue->data = runtime->CopyString (runtime, "Test3");
	runtime->StoreProperty (runtime, arg3, pI_TRUE);

	arg2 = runtime->CreateArgument (runtime, T_pI_Argument_StringValue);
	runtime->CreateArgumentData (runtime, arg2);
	arg2->description.name = runtime->CopyString (runtime, "Test2");
	arg2->data.StringValue->data = runtime->CopyString (runtime, "Test2");
	runtime->StoreProperty (runtime, arg2, pI_TRUE);

	/* serialize and de-serialize properties */
	s11n = runtime->SerializeProperties (runtime);
	runtime->ClearProperties (runtime, pI_TRUE);
	runtime->DeserializeProperties (runtime, s11n);
	runtime->FreeString (runtime, s11n);

	/* retrieve a property */
	arg2 = runtime->GetProperty (runtime, "Test2", pI_FALSE);
	ASSERT_TRUE (strcmp (arg2->data.StringValue->data, "Test2") == 0);

	arg1 = runtime->GetProperty (runtime, "Test1", pI_TRUE);
	ASSERT_TRUE (strcmp (arg1->data.StringValue->data, "Test1") == 0);
	runtime->FreeArgument (runtime, arg1, pI_TRUE);
}

TEST_F(CRuntimeTest, TestPropertyMapPersistance) {

	CRuntime* crt;

	crt = CreateCRuntime (pI_TRUE);
	
	DestroyCRuntime (crt, pI_TRUE);
}
