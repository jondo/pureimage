/* CppAdapterTest.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */ 

#include <PureImage.h>
#include <Runtime/Internal/RuntimeHelperFunctions.h>
#include <iostream>
#include <boost/shared_ptr.hpp>

#include <Adapter/CppAdapterTest.hpp>
#include <Adapters/C++/AdapterFactory.hpp>
#include <Adapters/C++/Arguments/Arguments.hpp>
#include <Adapters/C++/Structures/Structures.hpp>

#include <typeinfo>

using namespace pI;

void CppAdapterTest::SetUp()
{
    runtime = CreateCRuntime (pI_FALSE);
}

void CppAdapterTest::TearDown()
{
    DestroyCRuntime (runtime, pI_FALSE);
}

TEST_F(CppAdapterTest, TestArgumentAdapterFactory) {

    ::Argument* arg;

	arg = runtime->CreateArgument (runtime, T_pI_Argument_ByteArray);
    boost::shared_ptr<pI::ArgumentAdapter> cppArg(pI::AdaptArgument(arg, pI_TRUE));

    // note use typeid on dereferenced poitners
    ASSERT_TRUE(typeid(*(cppArg.get())) == typeid(ByteArray));
}

TEST_F(CppAdapterTest, TestDoubleArray) {

    ::Argument* arg;

    arg = runtime->CreateArgument (runtime, T_pI_Argument_DoubleArray);
    boost::shared_ptr<DoubleArray> cppArg(
        (DoubleArray*) pI::AdaptArgument(arg, pI_TRUE));

    cppArg->SetCount(10);
    ASSERT_EQ(10, arg->signature.setup.DoubleArray->count);

    runtime->CreateArgumentData(runtime, arg);

    ASSERT_EQ(10, arg->signature.setup.DoubleArray->count);
    for(unsigned int idx = 0; idx < cppArg->GetCount(); idx++) {
        cppArg->SetData(idx, 1.0*idx);
    }
    
    for(unsigned int idx = 0; idx < cppArg->GetCount(); idx++) {
        ASSERT_DOUBLE_EQ(idx, cppArg->GetData(idx));
    }    
}

TEST_F(CppAdapterTest, TestByteImage) {

    Argument* arg;

    arg = runtime->CreateArgument (runtime, T_pI_Argument_ByteImage);
    boost::shared_ptr<ByteImage> cppArg(
        (ByteImage*) pI::AdaptArgument(arg, pI_TRUE));

    pI_size w = 60;
    pI_size h = 50;
    pI_size c = 3;
    
    cppArg->SetWidth(w);
    cppArg->SetHeight(h);
    cppArg->SetChannels(c);

    ASSERT_EQ(w, arg->signature.setup.ByteImage->width);
    ASSERT_EQ(h, arg->signature.setup.ByteImage->height);
    ASSERT_EQ(c, arg->signature.setup.ByteImage->channels);

    ASSERT_EQ(w, cppArg->GetWidth());
    ASSERT_EQ(h, cppArg->GetHeight());
    ASSERT_EQ(c, cppArg->GetChannels());


    runtime->CreateArgumentData(runtime, arg);

    unsigned int counter = 0;
    for(unsigned int row = 0; row < cppArg->GetHeight(); row++) {
        for(unsigned int col = 0; col < cppArg->GetWidth(); col++) {
            for(unsigned int channel = 0; channel < cppArg->GetChannels(); channel++) {
                cppArg->SetData(row, col, channel, (counter++)%256);
            }
        }
    }

    counter = 0;
    for(unsigned int row = 0; row < cppArg->GetHeight(); row++) {
        for(unsigned int col = 0; col < cppArg->GetWidth(); col++) {
            for(unsigned int channel = 0; channel < cppArg->GetChannels(); channel++) {
                ASSERT_DOUBLE_EQ((counter++)%256, cppArg->GetData(row, col, channel));
                ASSERT_DOUBLE_EQ(arg->data.ByteImage->data[row][col][channel], cppArg->GetData(row, col, channel));
            }
        }
    }
}


TEST_F(CppAdapterTest, TestComplexArray) {

	Argument* arg;
	arg = runtime->CreateArgument (runtime, T_pI_Argument_ComplexArray);
	boost::shared_ptr<ComplexArray> cppArg(
		(ComplexArray*) pI::AdaptArgument(arg, pI_TRUE));

	pI_size count = 3;

	cppArg->SetCount(count);

	ASSERT_EQ(count, arg->signature.setup.ComplexArray->count);
	ASSERT_EQ(count, cppArg->GetCount());

	cppArg->CreateData();

	Complex c1 = cppArg->GetData(0);
	Complex c2 = cppArg->GetData(1);
	Complex c3 = cppArg->GetData(2);
	c1.SetR(1.0);
	c1.SetI(0.1);
	c2.SetR(2.0);
	c2.SetI(0.2);
	c3.SetR(3.0);
	c3.SetI(0.3);

	ASSERT_DOUBLE_EQ(1.0, arg->data.ComplexArray->data[0].r);
	ASSERT_DOUBLE_EQ(0.1, arg->data.ComplexArray->data[0].i);
	ASSERT_DOUBLE_EQ(2.0, arg->data.ComplexArray->data[1].r);
	ASSERT_DOUBLE_EQ(0.2, arg->data.ComplexArray->data[1].i);
	ASSERT_DOUBLE_EQ(3.0, arg->data.ComplexArray->data[2].r);
	ASSERT_DOUBLE_EQ(0.3, arg->data.ComplexArray->data[2].i);
}
