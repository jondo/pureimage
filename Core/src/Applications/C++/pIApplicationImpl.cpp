/* pIApplicationImpl.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universit�t Linz,
 * Institut f�r Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <deque>
#include <Poco/Path.h>
#include <Poco/File.h>
#include <Poco/DirectoryIterator.h>
#include <boost/algorithm/string/predicate.hpp>
#include <Adapters/C++/pIApplicationImpl.h>
#include <SharedLibrary.h>

#include <iostream>

#define PI_PLUGIN_FACTORY_METHOD "CreatepIns"

/// dll export function definition
typedef /*pI_API*/ pI_bool (*CreatepIns) (
    pI_int index,
    struct _CRuntime* runtime,
    struct _CpIn** plugin);

pI_bool ValidatePluginNameConfiguration (std::string name) {

#if defined(_WIN32) || defined(_WIN64)
#if defined(DEBUG) || defined(_DEBUG)
	return boost::algorithm::ends_with (name, "_d.dll");
#else /* #if defined(DEBUG) || defined(_DEBUG) */
	return !boost::algorithm::ends_with (name, "_d.dll") &&
		   boost::algorithm::ends_with (name, ".dll");
#endif /* #if defined(DEBUG) || defined(_DEBUG) */
#elif defined(linux) || defined(__linux) || defined(__linux__) || defined(__TOS_LINUX__)
#if defined(DEBUG) || defined(_DEBUG)
	return boost::algorithm::ends_with (name, "_d.so") &&
		   boost::algorithm::starts_with (name, "lib");
#else /* #if defined(DEBUG) || defined(_DEBUG) */
	return !boost::algorithm::ends_with (name, "_d.so") &&
		   boost::algorithm::ends_with (name, ".so") &&
		   boost::algorithm::starts_with (name, "lib");
#endif /* #if defined(DEBUG) || defined(_DEBUG) */
#endif /* #elif defined(linux) || defined(__linux) || defined(__linux__) || defined(__TOS_LINUX__) */
} // pI_bool ValidatePluginNameConfiguration (std::string& name)

std::string CreatePluginFileName (std::string path, std::string name, pI_bool dbg) {

	std::string retval(path);
    // sanitize
    if (!boost::algorithm::ends_with (path, "/") && !boost::algorithm::ends_with (path, "\\")) {
        retval.append ("/");
    }
	if (ValidatePluginNameConfiguration (name) == pI_FALSE) {
#if defined(linux) || defined(__linux) || defined(__linux__) || defined(__TOS_LINUX__)
		retval.append ("lib");
#endif // #if defined(linux) || defined(__linux) || defined(__linux__) || defined(__TOS_LINUX__)
		retval.append (name);
#if defined(_WIN32) || defined(_WIN64)
		if (dbg == pI_TRUE)
			retval.append ("_d.dll");
		else
			retval.append (".dll");
#elif defined(linux) || defined(__linux) || defined(__linux__) || defined(__TOS_LINUX__)
		if (dbg == pI_TRUE)
			retval.append ("_d.so");
		else
			retval.append (".so");
#endif /* #elif defined(linux) || defined(__linux) || defined(__linux__) || defined(__TOS_LINUX__) */
	} else {
		retval.append (name);
	}
	if (!Poco::File (retval).exists())
		return std::string();
	return retval;
} // std::string CreatePluginFileName (std::string& path, std::string& name)

#ifdef __cplusplus
extern "C" {
#endif

/*extern*/ const pI_char* pIAppFindBaseDir (CRuntimePtr runtime) {

	std::string distFolder ("libraries");
    std::string current = Poco::Path::current();

    while (true) {
        Poco::Path p (current);
        p.pushDirectory (distFolder);

        if (Poco::File (p).exists()) {
            p.popDirectory();
			return runtime->CopyString (runtime, const_cast<pI_char* > (p.absolute().toString().c_str()));
        } else {
            p.popDirectory();
        }

        if (p.depth() == 0) {
            return "";
        }

        current = p.parent().toString();
    }
} // const pI_char* pIAppFindBaseDir (CRuntimePtr runtime)

/*extern*/ const pI_char* pIAppFindPluginDir (CRuntimePtr runtime) {

	std::string distFolder ("libraries");
    std::string current = Poco::Path::current();

    while (true) {
        Poco::Path p (current);
        p.pushDirectory (distFolder);

        if (Poco::File (p).exists()) {
			return runtime->CopyString (runtime, const_cast<pI_char* > (p.absolute().toString().c_str()));
        } else {
            p.popDirectory();
        }

        if (p.depth() == 0) {
            return "";
        }

        current = p.parent().toString();
    }
} // const pI_char* pIAppFindPluginDir (CRuntimePtr runtime)


/*extern*/ struct _SharedLibrary* pIAppLoadPluginLibrary (CRuntimePtr runtime, const pI_char* libName) {

	Poco::Path pluginDir;
	if (runtime->HasProperty (runtime, "PUREIMAGE_LIBRARY_PATH") == pI_TRUE) {
		pluginDir = runtime->GetProperty (runtime, "PUREIMAGE_LIBRARY_PATH", pI_FALSE)->data.StringValue->data;
	} else {
		pluginDir = pIAppFindBaseDir (runtime);
		pluginDir.pushDirectory ("libraries");
	}

    return pIAppLoadPluginLibraryWithPath (
	 runtime,
	 const_cast<pI_char* > (pluginDir.toString().c_str()), libName);
} // struct _SharedLibrary* pIAppLoadPluginLibrary (CRuntimePtr runtime, const pI_char* libName)


/*extern*/ struct _SharedLibrary* pIAppLoadPluginLibraryWithPath (
 CRuntimePtr runtime,
 const pI_char* path,
 const pI_char* libName) {

#if defined(DEBUG) || defined(_DEBUG)
	pI_bool dbg = pI_TRUE;
#else
	pI_bool dbg = pI_FALSE;
#endif
	std::string lib_path = CreatePluginFileName (path, libName, dbg);
	struct _SharedLibrary* sl = LoadSharedLibrary (const_cast<pI_char* > (lib_path.c_str()));
	// in case configuration-dependant lib was not found, fall back for alternate config
	if (sl == 0) {
		lib_path = CreatePluginFileName (path, libName, !dbg);
		sl = LoadSharedLibrary (const_cast<pI_char* > (lib_path.c_str()));
	}

	if (GetSharedLibraryFunction (sl, PI_PLUGIN_FACTORY_METHOD) != 0)
		return sl;
	else {
		UnloadSharedLibrary (sl);
		return 0;
	}
} // struct _SharedLibrary* pIAppLoadPluginLibraryWithPath (...)


/*extern*/ struct _pIAppPluginLibraryList* pIAppLoadPluginLibraries (
	CRuntimePtr runtime,
    const pI_char* path,
    const pI_bool recursive,
    const pI_bool auto_register) {

    std::string root;

    if (path != 0) {
        root = path;
    } else {
        // use current directory
        root = Poco::Path::current();
    }

    std::deque<Poco::DirectoryIterator> paths;
    paths.push_back (Poco::DirectoryIterator (root));
    Poco::DirectoryIterator end;

	std::vector<struct _SharedLibrary* > libs;

    while (!paths.empty()) {
        Poco::DirectoryIterator it (paths.front());
        paths.pop_front();

        while (it != end) {
            if (it->isFile()) {
                // got a file, try to load as plugin library
                try {
                    std::string fn (it.path().getFileName());

					// filter filenames for suitable, config-dependent naming

					if (ValidatePluginNameConfiguration (fn) == pI_TRUE) {
						std::string p (it.path().parent().toString (Poco::Path::PATH_GUESS));
						libs.push_back (
						 pIAppLoadPluginLibraryWithPath (
						  runtime,
						  const_cast<pI_char* > (p.c_str()), const_cast<pI_char* > (fn.c_str())));
					}
                } catch (...) {}
            } else {
                if (recursive == pI_TRUE) {
                    // note: because of the limitations of the Poco directory iterator,
                    //       it is necessary to create new instances instead of iterator
                    //       copies, which would share a file handle
                    paths.push_back (Poco::DirectoryIterator (it->path()));
                }
            }

            ++it;
        } // while (it != end)
    } // while (!paths.empty())

	struct _pIAppPluginLibraryList* list =
	 reinterpret_cast<struct _pIAppPluginLibraryList* > (
	  runtime->AllocateMemory (runtime, sizeof(pIAppPluginLibraryList)));
	list->count = libs.size(); list->_reserved = libs.size();
	list->shared_libs =
	 reinterpret_cast<struct _SharedLibrary**> (
	  runtime->AllocateMemory (runtime, sizeof(struct _SharedLibrary* ) * list->_reserved));

	for (pI_size lv = 0; lv < list->count; ++lv) {
		list->shared_libs[lv] = libs[lv];
	}
    if (auto_register == pI_TRUE) {
        pIAppRegisterPluginList (runtime, list);
    }
	return list;
} // struct _pIAppPluginLibraryList* pIAppLoadPluginLibraries (...)


/*extern*/ pI_size pIAppRegisterPluginList (
 CRuntimePtr runtime,
 struct _pIAppPluginLibraryList* list) {

	/*
     * This implementation takes care of simple dependencies
     * between plugins. I.e., a plugin that spawns another plugin
     * can not do this if this very plugin has not been loaded yet.
     *
     * If an error occurs during plugin creation it is supposed to come
     * from such a dependency and the creation is repeated in a later iteration.
     * If no plugins could be loaded in one iteration this iteration stops.
     *
     * Plugins that are loaded after all either have other problems (e.g. unresolved third party
     * libraries) or there are circular plugin dependencies.
     */

    size_t pIn_count (0);
	if (list == 0)
		return 0;
    std::vector<std::vector<struct _CpIn* > > plugin_vector;
	plugin_vector.resize (list->count);
    size_t last_fails (0);
    bool do_loop (true);

    while (do_loop) {
        size_t fails (0);
        for (size_t lv = 0; lv < list->count; ++lv) {
            struct _CpIn* plugin_ptr = 0;
            struct _CpIn** plugin_ptr_ptr = &plugin_ptr;
			SharedLibrary* sl = list->shared_libs[lv];

            if (sl == 0) {
                continue;
            }

			CreatepIns cpins = (CreatepIns) GetSharedLibraryFunction (sl, PI_PLUGIN_FACTORY_METHOD);

            if (cpins == 0) {
                continue;
            }

            if (plugin_vector[lv].size() == 0) {
                size_t lv2 (0);

                while (cpins (lv2++, runtime, plugin_ptr_ptr) != pI_FALSE) {
                    if (plugin_ptr == 0) {
                        ++fails;
                    } else {
                        if (runtime->RegisterPlugin (
                                    runtime,
                                    plugin_ptr) == pI_TRUE) {
                            ++pIn_count;
                        } else {
                            /* TODO: check for possible errors, including runtime mismatch */

                        }
                    }

                    plugin_vector[lv].push_back (plugin_ptr);
                } // while (cpins (lv2++, m_runtime.GetCRuntime(), plugin_ptr_ptr) != pI_FALSE)
            } else {
                for (size_t lv2 = 0; lv2 < plugin_vector[lv].size(); ++lv2) {
                    if (plugin_vector[lv][lv2] == 0) {
                        cpins (lv2, runtime, plugin_ptr_ptr);

                        if (plugin_ptr == 0) {
                            ++fails;
                        } else {
                            if (runtime->RegisterPlugin (
                                        runtime,
                                        *plugin_ptr_ptr) == pI_TRUE) {
                                ++pIn_count;
                                plugin_vector[lv][lv2] = *plugin_ptr_ptr;
                            }
                        }
                    } // if (plugin_vector[lv][lv2] == 0) {
                } // for (size_t lv2 = 0; lv2 < plugin_vector[lv].size(); ++lv2)
            }
        } // for (size_t lv = 0; lv < list->count; ++lv)

        if (fails == last_fails) {
            do_loop = false;
        }

        last_fails = fails;
    } // while (do_loop)

    return pIn_count;
} // pI_size pIAppRegisterPluginList (...)

/*extern*/ struct _pIAppPluginLibraryList* pIAppCreatePluginLibraryList (
 CRuntimePtr runtime,
 pI_size reserve) {

	if (runtime == 0)
		return 0;
	struct _pIAppPluginLibraryList* rv =
	 reinterpret_cast<struct _pIAppPluginLibraryList* > (
	  runtime->AllocateMemory (runtime, sizeof(pIAppPluginLibraryList)));
	if (rv == 0)
		return 0;
	rv->count = rv->_reserved = 0;
	if (reserve > 0) {
		rv->shared_libs =
		 reinterpret_cast<struct _SharedLibrary** > (
		  runtime->AllocateMemory (runtime, sizeof(struct _SharedLibrary* ) * reserve));
		if (rv->shared_libs != 0) {
			for (pI_size lv = 0; lv < reserve; ++lv)
				rv->shared_libs[lv] = 0;
			rv->_reserved = reserve;
		}
	} else
		rv->shared_libs = 0;
	return rv;
} // struct _pIAppPluginLibraryList* pIAppCreatePluginLibraryList (...)


/*extern*/ pI_bool pIAppAddToPluginLibraryList (
 CRuntimePtr runtime,
 struct _SharedLibrary* lib,
 struct _pIAppPluginLibraryList* list) {

	if (runtime == 0 || lib == 0 || list == 0)
		return pI_FALSE;

	if (list->shared_libs == 0 || list->_reserved == 0) {
		list->_reserved = 1; list->count = 1;
		list->shared_libs =
		 reinterpret_cast<struct _SharedLibrary** > (
		  runtime->AllocateMemory (runtime, sizeof(struct _SharedLibrary* ) * list->_reserved));
		if (list->shared_libs == 0)
			return pI_FALSE;
		list->shared_libs[0] = lib;
	} else if (list->count >= list->_reserved) {
		// upsize necessary; double array size
		list->_reserved = list->count * 2;

		struct _SharedLibrary** newlist =
		 reinterpret_cast<struct _SharedLibrary** > (
		  runtime->AllocateMemory (runtime, sizeof(struct _SharedLibrary* ) * list->_reserved));
		if (newlist == 0)
			return pI_FALSE;
		for (pI_size lv = 0; lv < list->count; ++lv) {
			newlist[lv] = list->shared_libs[lv];
		}
		for (pI_size lv = list->count; lv < list->_reserved; ++lv) {
			newlist[lv] = 0;
		}
		runtime->FreeMemory (runtime, list->shared_libs);
		newlist[list->count++] = lib;
		list->shared_libs = newlist;
	} else {
		list->shared_libs[list->count++] = lib;
	}
	return true;
} // pI_bool pIAppAddToPluginLibraryList (...)


/*extern*/ pI_bool pIAppMergePluginLibraryLists (
 CRuntimePtr runtime,
 struct _pIAppPluginLibraryList* srcdst,
 struct _pIAppPluginLibraryList* src2) {

	if (runtime == 0 || srcdst == 0 || src2 == 0)
		return pI_FALSE;
	pI_bool retval = pI_FALSE;
	if (src2->shared_libs != 0) {
		retval = pI_TRUE;
		for (pI_size lv = 0; lv < src2->count; ++lv) {
			retval &= pIAppAddToPluginLibraryList (runtime, src2->shared_libs[lv], srcdst);
		}
		runtime->FreeMemory (runtime, src2->shared_libs);
		runtime->FreeMemory (runtime, src2);
	}
	return retval;
} // pI_bool pIAppMergePluginLibraryLists (...)


/*extern*/ void pIAppDeletePluginLibraryList (
 CRuntimePtr runtime,
 struct _pIAppPluginLibraryList* list,
 pI_bool listOnly,
 pI_bool unloadSharedLibraries) {

	if (runtime == 0 || list == 0)
		return;
	if (list->shared_libs != 0) {
		if (listOnly == pI_FALSE) {
			for (pI_size lv = 0; lv < list->count; ++lv) {
				if (unloadSharedLibraries == pI_TRUE) {
					UnloadSharedLibrary (list->shared_libs[lv]);
				}
			}
		}
		runtime->FreeMemory (runtime, list->shared_libs);
		runtime->FreeMemory (runtime, list);
	}
} // void pIAppDeletePluginLibraryList (...)

#ifdef __cplusplus
}
#endif
