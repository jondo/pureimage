/* BaseApplication.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <deque>

#include <Poco/Path.h>
#include <Poco/File.h>
#include <Poco/DirectoryIterator.h>
#include <Adapters/C++/Arguments.hpp>
#include <Adapters/C++/Runtime.hpp>
#include <Adapters/C++/pIn.hpp>
#include <Adapters/C++/BaseApplication.hpp>

#define PI_PLUGIN_FACTORY_METHOD "CreatepIns"

namespace pI {

// dedicated unload method for poco dll, since
// poco shared library needs dedicated method call
void unload_dll (Poco::SharedLibrary* dll) {

    dll->unload();
    delete dll;
} // void unload_dll (Poco::SharedLibrary* dll)

BaseApplication::BaseApplication (const pI_bool load_properties /*= pI_FALSE*/) {

    CRuntime* c_runtime = CreateCRuntime (load_properties);

    if (load_properties == pI_FALSE) {
        _runtime = Runtime (c_runtime, pI_TRUE, load_properties);
    } else {
        _runtime = Runtime (c_runtime, pI_TRUE, load_properties);
    }
} // BaseApplication::BaseApplication(...)

BaseApplication::BaseApplication (
    const CRuntimePtr r,
    const pI_bool take_ownership /*= pI_FALSE*/,
    const pI_bool load_properties /*= pI_FALSE*/) {

    _runtime = Runtime (r, take_ownership, load_properties);
} // BaseApplication::BaseApplication(...)

BaseApplication::BaseApplication (Runtime& r) : _runtime (r) {}


void BaseApplication::LoadPluginLibrary (const pI_char* libName) {

    Poco::Path pluginDir (FindPIPluginDir());
    LoadPluginLibrary (pluginDir.toString().c_str(), libName);
}


void BaseApplication::LoadPluginLibrary (const pI_char* path, const pI_char* libName) {
    Poco::Path ppath (path);

    // TODO: currently we can not use the poco platform suffixes, as our
    // debug libraries do not have "d" in library name.
    std::string ext = Poco::SharedLibrary::suffix();
    ext = ext.substr (ext.find ('.') + 1);

#ifdef WIN32
    ppath.append (libName);
#else
    // Add a prefix for Linux/Unix libs
    ppath.append (std::string ("lib") + libName);
#endif
    ppath.setExtension (ext);
    std::string lib_path = ppath.toString (Poco::Path::PATH_GUESS);

    if (!Poco::File (lib_path).exists()) {
        throw pI::exception::PluginLibraryException (
            std::string ("Library does not exist: ").append (lib_path).c_str());
    }

    try {
        Poco::SharedLibrary* dll = new Poco::SharedLibrary (lib_path);

        if (dll->hasSymbol (PI_PLUGIN_FACTORY_METHOD)) {
            _dllHandles.push_back (boost::shared_ptr<Poco::SharedLibrary> (dll, unload_dll));
        } else {
            unload_dll (dll);
        }
    } catch (Poco::LibraryAlreadyLoadedException&) {
        // TODO: is this a problem?
        return;
    } catch (Poco::LibraryLoadException& e2) {
        throw pI::exception::PluginLibraryException (e2.what());
    }
} // void BaseApplication::LoadPluginLibrary (...)

void BaseApplication::LoadPluginLibraries (
    const pI_char* path /*= 0*/,
    const pI_bool recursive /*= pI_TRUE*/,
    const pI_bool auto_register /*= pI_TRUE*/) {

    std::string root;

    if (path != 0) {
        root = path;
    } else {
        // use current directory
        root = Poco::Path::current();
    }

    std::deque<Poco::DirectoryIterator> paths;
    paths.push_back (Poco::DirectoryIterator (root));
    Poco::DirectoryIterator end;

    while (!paths.empty()) {
        Poco::DirectoryIterator it (paths.front());
        paths.pop_front();

        while (it != end) {
            if (it->isFile()) {
                // got a file, try to load as plugin library
                try {
                    std::string p (it.path().parent().toString (Poco::Path::PATH_GUESS));
                    std::string fn (it.path().getFileName());
                    LoadPluginLibrary (p.c_str(), fn.c_str());
                } catch (...) {}
            } else {
                if (recursive == pI_TRUE) {
                    // note: because of the limitations of the Poco directory iterator,
                    //       it is necessary to create new instances instead of iterator
                    //       copies, which would share a file handle
                    paths.push_back (Poco::DirectoryIterator (it->path()));
                }
            }

            ++it;
        } // while (it != end)
    } // while (!paths.empty())

    if (auto_register == pI_TRUE) {
        RegisterPlugins();
    }
} // void BaseApplication::LoadPluginLibraries (...)

pI_size BaseApplication::RegisterPlugins() {
    /*
     * This implementation takes care of simple dependencies
     * between plugins. I.e., a plugin that spawns another plugin
     * can not do this if this very plugin has not been loaded yet.
     *
     * If an error occurs during plugin creation it is supposed to come
     * from such a dependency and the creation is repeated in a later iteration.
     * If no plugins could be loaded in one iteration this iteration stops.
     *
     * Plugins that are loaded after all either have other problems (e.g. unresolved third party
     * libraries) or there are circular plugin dependencies.
     */

    size_t pIn_count (0);
    std::vector<std::vector<struct _CpIn* > > plugin_vector;
    plugin_vector.resize (_dllHandles.size());
    size_t last_fails (0);
    bool do_loop (true);

    while (do_loop) {
        size_t fails (0);

        for (size_t lv = 0; lv < _dllHandles.size(); ++lv) {
            struct _CpIn* plugin_ptr = 0;
            struct _CpIn** plugin_ptr_ptr = &plugin_ptr;
            Poco::SharedLibrary* dll = _dllHandles[lv].get();

            if (dll == 0) {
                continue;
            }

            CreatepIns cpins = (CreatepIns) dll->getSymbol (PI_PLUGIN_FACTORY_METHOD);

            if (cpins == 0) {
                continue;
            }

            if (plugin_vector[lv].size() == 0) {
                size_t lv2 (0);

                while (cpins (lv2++, _runtime.GetCRuntime(), plugin_ptr_ptr) != pI_FALSE) {
                    if (plugin_ptr == 0) {
                        ++fails;
                    } else {
                        if (_runtime.GetCRuntime()->RegisterPlugin (
                                    _runtime.GetCRuntime(),
                                    plugin_ptr) == pI_TRUE) {
                            ++pIn_count;
                        } else {
                            /* TODO: check for possible errors, including runtime mismatch */

                        }
                    }

                    plugin_vector[lv].push_back (plugin_ptr);
                } // while (cpins (lv2++, m_runtime.GetCRuntime(), plugin_ptr_ptr) != pI_FALSE)
            } else {
                for (size_t lv2 = 0; lv2 < plugin_vector[lv].size(); ++lv2) {
                    if (plugin_vector[lv][lv2] == 0) {
                        cpins (lv2, _runtime.GetCRuntime(), plugin_ptr_ptr);

                        if (plugin_ptr == 0) {
                            ++fails;
                        } else {
                            if (_runtime.GetCRuntime()->RegisterPlugin (
                                        _runtime.GetCRuntime(),
                                        *plugin_ptr_ptr) == pI_TRUE) {
                                ++pIn_count;
                                plugin_vector[lv][lv2] = *plugin_ptr_ptr;
                            }
                        }
                    } // if (plugin_vector[lv][lv2] == 0) {
                } // for (size_t lv2 = 0; lv2 < plugin_vector[lv].size(); ++lv2)
            }
        } // for (size_t lv = 0; lv < dlls.size(); ++lv)

        if (fails == last_fails) {
            do_loop = false;
        }

        last_fails = fails;
    } // while (do_loop)

    return pIn_count;
} // pI_size BaseApplication::RegisterPlugins()

std::vector<std::string> BaseApplication::GetPluginNames() const {

    CRuntime* c_runtime = _runtime.GetCRuntime();
    pI_size plugin_count = c_runtime->plugin_count;
    std::vector<std::string> plugin_names;

    for (pI_size idx = 0; idx < plugin_count; idx++) {
        CpIn* cpIn = c_runtime->plugins[idx];
        plugin_names.push_back (cpIn->name);
    }

    return plugin_names;
} // std::vector<std::string> BaseApplication::GetPluginNames() const

std::vector<std::string> BaseApplication::GetPluginDescriptions() const {

    CRuntime* c_runtime = _runtime.GetCRuntime();
    pI_size plugin_count = c_runtime->plugin_count;
    std::vector<std::string> plugin_descs;

    for (pI_size idx = 0; idx < plugin_count; idx++) {
        CpIn* cpIn = c_runtime->plugins[idx];
        plugin_descs.push_back (cpIn->description != 0 ? cpIn->description : "");
    }

    return plugin_descs;
} // std::vector<std::string> BaseApplication::GetPluginDescriptions() const

std::vector<pI_unsigned> BaseApplication::GetPluginVersions() const {

    CRuntime* c_runtime = _runtime.GetCRuntime();
    pI_size plugin_count = c_runtime->plugin_count;
    std::vector<pI_unsigned> plugin_versions;

    for (pI_size idx = 0; idx < plugin_count; idx++) {
        CpIn* cpIn = c_runtime->plugins[idx];
        plugin_versions.push_back (cpIn->plugin_version);
    }

    return plugin_versions;
} // std::vector<pI_unsigned> BaseApplication::GetPluginVersions() const

std::vector<std::string> BaseApplication::GetPluginAuthors() const {

    CRuntime* c_runtime = _runtime.GetCRuntime();
    pI_size plugin_count = c_runtime->plugin_count;
    std::vector<std::string> plugin_authors;

    for (pI_size idx = 0; idx < plugin_count; idx++) {
        CpIn* cpIn = c_runtime->plugins[idx];
        plugin_authors.push_back (cpIn->author != 0 ? cpIn->author : "");
    }

    return plugin_authors;
} // std::vector<std::string> BaseApplication::GetPluginAuthors() const

std::vector<std::string> BaseApplication::GetPluginCopyrights() const {

    CRuntime* c_runtime = _runtime.GetCRuntime();
    pI_size plugin_count = c_runtime->plugin_count;
    std::vector<std::string> plugin_crs;

    for (pI_size idx = 0; idx < plugin_count; idx++) {
        CpIn* cpIn = c_runtime->plugins[idx];
        plugin_crs.push_back (cpIn->copyright != 0 ? cpIn->copyright : "");
    }

    return plugin_crs;
} // std::vector<std::string> BaseApplication::GetPluginCopyrights() const

/* static */
std::string BaseApplication::FindPIBaseDir() {

	std::string distFolder ("libraries");
    std::string current = Poco::Path::current();

    while (true) {
        Poco::Path p (current);
        p.pushDirectory (distFolder);

        if (Poco::File (p).exists()) {
            p.popDirectory();
            return p.absolute().toString();
        } else {
            p.popDirectory();
        }

        if (p.depth() == 0) {
            return "";
        }

        current = p.parent().toString();
    }
}

pInPtr BaseApplication::SpawnAndInitialize (
    pI::Runtime& runtime,
    const pI_char* plugin, ...) {

    pInPtr retval (runtime.SpawnPlugin (plugin));

    if (GetPInPtr (retval) == 0) {
        return retval;
    }

    pI::Arguments params (retval->GetParameterSignature());
    // now comes the evil and unsafe hack (not type safe, only support for limited types, serious problems when too few parameters are given)...
    va_list argptr;
    va_start (argptr, plugin);

    for (pI_size lv = 0; lv < params.size(); ++lv) {
        switch (params[lv]->signature.type) {
            case T_pI_Argument_IntValue: {
                pI::IntValue (params[lv]).SetData (va_arg (argptr, pI_int));
                break;
            }
            case T_pI_Argument_StringValue: {
                pI::StringValue (params[lv]).SetData (va_arg (argptr, pI_str));
                break;
            }
            case T_pI_Argument_BoolValue: {
                pI::BoolValue (params[lv]).SetData (va_arg (argptr, pI_bool));
                break;
            }
            case T_pI_Argument_DoubleValue: {
                pI::DoubleValue (params[lv]).SetData (va_arg (argptr, pI_double));
                break;
            }
            case T_pI_Argument_StringSelection: {
                pI::StringSelection (params[lv]).SetIndex (va_arg (argptr, pI_size));
                break;
            }
            default:
                throw pI::exception::IncompatibleParameterException ("Failed to fast-construct plugin with given parameters.");
        }
    }

    va_end (argptr);
    retval->Initialize (params);
    return retval;
} // boost::shared_ptr<pI::pIn> SpawnAndInitialize (pI_str plugin, ...)


} // namespace pI

#undef PI_PLUGIN_FACTORY_METHOD
