/* pI_exceptions.i
 *
 * Copyright 2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

// Core/include/Adapters/C++/ArgumentChecks.hpp
%catches(pI::exception::IncompatibleArgumentException) pI::CheckVariableArgumentType;
%catches(pI::exception::IncompatibleArgumentException, pI::exception::IncompatibleParameterException) pI::CheckArgumentType;
%catches(pI::exception::IncompatibleArgumentException, pI::exception::IncompatibleParameterException) pI::CheckArgumentConstraints;
%catches(pI::exception::IncompatibleArgumentException, pI::exception::IncompatibleParameterException) pI::CheckArgumentData;
%catches(pI::exception::IncompatibleParameterException) pI::CheckParameter;
%catches(pI::exception::IncompatibleParameterException, pI::exception::MissingParameterException) pI::CheckParameters;
%catches(pI::exception::IncompatibleArgumentException) pI::CheckInputArgument;
%catches(pI::exception::IncompatibleArgumentException, pI::exception::MissingArgumentException) pI::CheckInputArguments;
%catches(pI::exception::IncompatibleArgumentException) pI::CheckOutputArgument;
%catches(pI::exception::IncompatibleArgumentException, pI::exception::MissingArgumentException) pI::CheckOutputArguments;

// Core/include/Adapters/C++/Arguments/ArgumentAdapter.hpp
%catches(pI::exception::MemoryException) pI::ArgumentAdapter::CreateData;

// Core/include/Adapters/C++/Application.hpp
%catches(pI::exception::PluginLibraryException) pI::Application::LoadPluginLibrary;
%catches(pI::exception::PluginLibraryException) pI::Application::LoadPluginLibraries;

// Core/include/Adapters/C++/Runtime.hpp
%catches(pI::exception::MissingRuntimeException) pI::Runtime::Runtime;
%catches(pI::exception::MemoryException) pI::Runtime::AllocateArgument;
%catches(pI::exception::ArgumentException) pI::Runtime::AllocateArgumentData;
%catches(pI::exception::MemoryException) pI::Runtime::CopyString;
%catches(pI::exception::pInException, pI::exception::PluginRegistrationException) pI::Runtime::RegisterPlugin;
%catches(pI::exception::PluginCreationException) pI::Runtime::SpawnPlugin;
%catches(pI::exception::PropertyException) pI::Runtime::GetProperty;
%catches(pI::exception::PropertyException) pI::Runtime::StoreProperty;

// Core/include/Adapters/C++/pIn.hpp
%catches(pI::exception::MemoryException) pI::pIn::pIn;
%catches(pI::exception::MemoryException) pI::pIn::GetParameterSignature;
%catches(pI::exception::MemoryException, 
         pI::exception::MissingParameterException, 
         pI::exception::IncompatibleParameterException, 
         pI::exception::InitializationException) pI::pIn::Initialize;
%catches(pI::exception::MemoryException, pI::exception::NotInitializedException) pI::pIn::GetInputSignature;
%catches(pI::exception::MemoryException, pI::exception::NotInitializedException) pI::pIn::GetOutputSignature;
%catches(pI::exception::MemoryException,
         pI::exception::NotInitializedException, 
         pI::exception::MissingArgumentException, 
         pI::exception::IncompatibleArgumentException, 
         pI::exception::IncompleteArgumentException, 
         pI::exception::ExecutionException) pI::pIn::Execute ;
%catches(pI::exception::MemoryException,
         pI::exception::SerializationNotSupportedException, 
         pI::exception::SerializationException) pI::pIn::Serialize;
%catches(pI::exception::MemoryException,
         pI::exception::DeserializationNotSupportedException, 
         pI::exception::DeserializationException) pI::pIn::Deserialize;
