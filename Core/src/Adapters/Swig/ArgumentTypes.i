/**
 *  Typedefs for argument pointers
 */
%include <boost_shared_ptr.i>
%include "std_vector.i"

%shared_ptr(Argument)
 
/**
 * Adds support for std::vector<pI::ArgumentPtr>
 *
 * Note: it is very important to define this mapping before any c++ signatures 
 *       using pI::Arguments are processed.
 */
%typemap(javaimports) std::vector<pI::ArgumentPtr> %{
import java.util.ArrayList;
import java.util.Arrays;
%}

%javamethodmodifiers std::vector<pI::ArgumentPtr>::get "private";
%javamethodmodifiers std::vector<pI::ArgumentPtr>::set "private";

%typemap(javacode) std::vector<pI::ArgumentPtr> %{

    ArrayList<Argument> args = null;
    
    public ArgumentVector(Argument... _args) {
    	this();
    
        this.clear();
        if(args == null) {
            args = new ArrayList<Argument>();
        }
        for(Argument a: _args) {
            Add(a);
        }
    }
    
    private void fetchArgs() {
        args = new ArrayList<Argument>();
        for(int i=0; i<size(); i++) {
            args.add(pI.adaptArgument(get(i), true));
        }
    }
    
    public Argument Get(int i) {
        if(args == null) {
            fetchArgs();
        }
        return args.get(i);
    }

    public void Add(Argument arg) {
        if(args == null) {
            fetchArgs();
        }
        this.args.add(arg);
        this.add(arg.GetCPtr());
    }
    
    /**
     * Sets the argument at a given position.
     */
    public void Set(int index, Argument arg) {
        if(args == null) {
            fetchArgs();
        }
        this.args.set(index, arg);
        this.set(index, arg.GetCPtr());
    }
    
    public String toString() {
        if(args == null) {
            fetchArgs();
        }

        return Arrays.toString(this.args.toArray(new Argument[0]));
    }
%}
namespace std {
  %template(ArgumentVector) vector< pI::ArgumentPtr >;
}

%include "Adapters/C++/ArgumentTypedefs.hpp"

