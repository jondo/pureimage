from template_engine import *
from license_template import *

## Templates: #########################################################################################################

structures_template = Template(
"""
$HEADER

%include "enums.swg"

%rename(StructureType) pI_StructureType;

typedef enum _pI_StructureType {
$ENUMS
} pI_StructureType;

$INCLUDES
""")

enum_val = "    T_%s,"

include_stmt = Template(
"""
%include <Adapters/C++/Structures/${s.name}.hpp>
""")

## Template processing logic: #########################################################################################

def process(vars):

    HEADER = StringBuffer()
    HEADER << cpp_block_comment(unicode(license_text, errors='ignore')%("Structures.i")) << ENDL
    HEADER << cpp_block_comment(auto_generation_warning%(__file__)) << ENDL

    structures = vars.__root__.structures
    ENUMS = ",\n".join([enum_val%(s.c_type) for s in structures])
    INCLUDES = StringBuffer()
    for s in structures:
        INCLUDES << expand_template(include_stmt) << ENDL
    
    with open_file(vars.__outdir__ + "/Structures.i") as f:
        print("Writing to file %s"%(vars.__outdir__ + "/Structures.i"))
        f.write(expand_template(structures_template))
