from template_engine import *
from license_template import *

## Templates: #########################################################################################################

arguments_template = Template(
"""
$HEADER

%include "enums.swg"

%rename(ArgumentType) pI_ArgumentType;

typedef enum _pI_ArgumentType {
$ENUMS
} pI_ArgumentType;

$INCLUDES
""")

enum_val = "    T_%s,"

include_stmt = Template(
"""
%include <Adapters/C++/Arguments/${a.name}.hpp>
""")

## Template processing logic: #########################################################################################

def process(vars):

    HEADER = StringBuffer()
    HEADER << cpp_block_comment(unicode(license_text, errors='ignore')%("Arguments.i")) << ENDL
    HEADER << cpp_block_comment(auto_generation_warning%(__file__)) << ENDL

    arguments = vars.__root__.arguments
    ENUMS = ",\n".join([enum_val%(a.c_type) for a in arguments])
    INCLUDES = StringBuffer()
    for a in arguments:
        INCLUDES << expand_template(include_stmt) << ENDL
    
    with open_file(vars.__outdir__ + "/Arguments.i") as f:
        f.write(expand_template(arguments_template))
