/* runtime.i
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 *
 */

 // gives c type a nicer name
%rename (ArgumentType) pI_ArgumentType;

 // tells swig that the result of CreateArgument has to be managed
%newobject pI::Runtime::CreateArgument;
%newobject pI::Runtime::SpawnPlugin;
 
// hide some runtime methods
%ignore pI::Runtime::GetCRuntime;
%ignore pI::Runtime::DeleteArgument;
%ignore pI::Runtime::AllocateArgument;
%ignore pI::Runtime::AllocateArgumentData;
%ignore pI::Runtime::ObtainArgument;
%ignore pI::Runtime::CopyString;
%ignore pI::Runtime::FreeString;
%ignore pI::Runtime::CopyArguments;

%include "Adapters/C++/Runtime.hpp"
