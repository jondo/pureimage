/* ArgumentChecks.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <boost/format.hpp>
#include <Runtime/Internal/RuntimeHelperFunctions.h>
#include <Adapters/C++/pInException.hpp>

#ifdef PI_ARGUMENT_CHECKS_BINARY
#include <Adapters/C++/ArgumentChecks.hpp>
#endif // PI_ARGUMENT_CHECKS_BINARY

namespace pI {

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
void CheckVariableArgumentType (
    const CArgumentPtr given,
    const CArgumentPtr prototype)
{

    if (prototype->signature.setup.VariableArgument->count >= static_cast<pI_size> (given->signature.type)) {
        if (prototype->data.VariableArgument != 0) {
            if (prototype->data.VariableArgument->types[given->signature.type] == pI_TRUE) {
                // accept type
                return;
            }
        }
    }

    throw exception::IncompatibleArgumentException("Variable argument does not accept given type.");
} // void CheckVariableArgumentType (...)

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
void CheckVariableArgumentType (
    const ArgumentPtr& given,
    const ArgumentPtr& prototype)
{

    CheckVariableArgumentType (GetCArgument(given), GetCArgument(prototype));
} // void CheckVariableArgumentType (...)

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
void CheckArgumentPointers (
    const CArgumentPtr given,
    const CArgumentPtr prototype,
    const pI_bool is_argument /*= pI_TRUE*/)
{

    if ((given == 0) || (prototype == 0)) {
        if (is_argument == pI_TRUE) {
            throw exception::MissingArgumentException("Failed consistency check - missing argument");
        }

        throw exception::MissingParameterException("Failed consistency check - missing parameter");
    }
} // void CheckArgumentPointers (...)

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
void CheckArgumentType (
    const CArgumentPtr given,
    const CArgumentPtr prototype,
    const pI_bool is_argument /*= pI_TRUE*/)
{

    // valid pointer check
    CheckArgumentPointers (given, prototype, is_argument);

    // compatible type check
    if (given->signature.type != prototype->signature.type) {
        // special treatment for variable arguments
        if ((prototype->signature.type == T_pI_Argument_VariableArgument) ||
                (prototype->signature.type == T_pI_Argument_VariableArgumentList)) {
            CheckVariableArgumentType (given, prototype);
            return;
        }

        boost::format fmter("Failed consistency check - incompatible %1% types (given: %2%, expected: %3%)");

        if (is_argument == pI_TRUE)
            throw exception::IncompatibleArgumentException(
                (fmter % "argument"
                 % given->signature.type
                 % prototype->signature.type).str().c_str());

        throw exception::IncompatibleParameterException(
            (fmter % "parameter"
             % given->signature.type
             % prototype->signature.type).str().c_str());
    }
} // void CheckArgumentType (...)

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
void CheckArgumentConstraints (
    const CArgumentPtr given,
    const CArgumentPtr prototype,
    const pI_bool is_argument /*= pI_TRUE*/)
{

    // valid type check
    CheckArgumentType (given, prototype, is_argument);

    pI_int ceval = given->runtime->CheckArgumentConstraints (
                       given->runtime,
                       const_cast<CArgumentPtr > (given),
                       prototype->signature.constraints);

    if (ceval != pI_TRUE) {
        boost::format fmter("Failed consistency check - %1% constraint check failed (constraint: %2%, error: %3%)");

        if (is_argument == pI_TRUE)
            throw exception::IncompatibleArgumentException(
                (fmter % "argument"
                 % (prototype->signature.constraints != 0 ? prototype->signature.constraints : "")
                 % (given->runtime->last_constraint_error_msg != 0 ? given->runtime->last_constraint_error_msg : "")).str().c_str());

        throw exception::IncompatibleParameterException(
            (fmter % "parameter"
             % (prototype->signature.constraints != 0 ? prototype->signature.constraints : "")
             % (given->runtime->last_constraint_error_msg != 0 ? given->runtime->last_constraint_error_msg : "")).str().c_str());
    }
} // void CheckArgumentConstraints (

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
void CheckArgumentData (
    const CArgumentPtr given,
    const CArgumentPtr prototype,
    const pI_bool is_argument /*= pI_TRUE*/,
    const pI_bool do_constraint_check /*= pI_TRUE*/)
{

    if (do_constraint_check == pI_TRUE) {
        CheckArgumentConstraints (given, prototype, is_argument);
    } else {
        CheckArgumentType (given, prototype, is_argument);
    }

    // just see if the data union is valid; works, since the runtime cleans up incomplete arguments
    if (given->data.Empty == 0) {
        boost::format fmter("Failed data availability check - %1% of type %2% has no valid data)");

        if (is_argument == pI_TRUE)
            throw exception::IncompatibleArgumentException(
                (fmter % "argument"
                 % given->signature.type).str().c_str());

        throw exception::IncompatibleParameterException(
            (fmter % "parameter"
             % given->signature.type).str().c_str());
    }
} // void CheckArgumentData (...)

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
void CheckParameter (
    const CArgumentPtr given,
    const CArgumentPtr prototype)
{

    CheckArgumentData (given, prototype, pI_FALSE, pI_TRUE);
} // void CheckParameter (...)

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
void CheckParameter (
    const ArgumentPtr& given,
    const ArgumentPtr& prototype)
{

    CheckParameter (GetCArgument(given), GetCArgument(prototype));

} // void CheckParameter (...)

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
void CheckParameters (
    const Arguments& given,
    const Arguments& prototype,
    pI_int num /*= -1*/)
{
    if ((num > 0) && (static_cast<pI_int> (prototype.size()) < num)) {
        throw exception::IncompleteParameterException("CheckParameters failed due to insufficient prototype size.");
    }

    // check if the last argument is a variable argument list
    pI_bool has_val(
        (prototype.size() > 0) &&
        (GetCArgument(prototype[prototype.size() - 1])->signature.type == T_pI_Argument_VariableArgumentList));
    pI_size p_size(prototype.size() - (has_val == pI_TRUE ? 1 : 0));

    if ((given.size() < p_size) && ((num < 0) || (num > static_cast<pI_int> (given.size())))) {
        throw exception::MissingParameterException("Incomplete parameter set detected.");
    }

    size_t lv;

    for (lv = 0; (num < 0) ? (lv < p_size) : (static_cast<pI_int> (lv) < num); ++lv) {
        CheckParameter (given[lv], prototype[lv]);
    }

    // check for correct types in the filled out variable argument list
    if (has_val == pI_TRUE) {
        ArgumentPtr val_arg(prototype[prototype.size() - 1]);

        for ( ; (num < 0) ? (lv < given.size()) : (static_cast<pI_int> (lv) < num); ++lv) {
            CheckArgumentType (GetCArgument(given[lv]), GetCArgument(val_arg));
        }
    }
} // void CheckParameters (...)

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
void CheckInputArgument (
    const CArgumentPtr given,
    const CArgumentPtr prototype)
{

    CheckArgumentData (given, prototype, pI_TRUE, pI_TRUE);
} // void CheckInputArgument (...)

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
void CheckInputArgument (
    const ArgumentPtr& given,
    const ArgumentPtr& prototype)
{

    CheckInputArgument (GetCArgument(given), GetCArgument(prototype));

} // void CheckInputArgument (...)

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
void CheckInputArguments (
    const Arguments& given,
    const Arguments& prototype,
    pI_int num /*= -1*/)
{

    if ((num > 0) && (static_cast<pI_int> (prototype.size()) < num)) {
        throw exception::IncompleteArgumentException("CheckInputArguments failed due to insufficient prototype size.");
    }

    // check if the last argument is a variable argument list
    pI_bool has_val(
        (prototype.size() > 0) &&
        (GetCArgument(prototype[prototype.size() - 1])->signature.type == T_pI_Argument_VariableArgumentList));
    pI_size p_size(prototype.size() - (has_val == pI_TRUE ? 1 : 0));

    if ((given.size() < p_size) && ((num < 0) || (num > static_cast<pI_int> (given.size())))) {
        boost::format fmter("Incomplete intput argument set detected (given: %1%, expected: %2%)");
        throw exception::IncompatibleArgumentException(
            (fmter % (given.size())
             % (prototype.size())).str().c_str());
    }

    size_t lv;

    for (lv = 0; (num < 0) ? (lv < p_size) : (static_cast<pI_int> (lv) < num); ++lv) {
        CheckInputArgument (given[lv], prototype[lv]);
    }

    // check for correct types in the filled out variable argument list
    if (has_val == pI_TRUE) {
        ArgumentPtr val_arg(prototype[prototype.size() - 1]);

        for ( ; (num < 0) ? (lv < given.size()) : (static_cast<pI_int> (lv) < num); ++lv) {
            CheckArgumentType (GetCArgument(given[lv]), GetCArgument(val_arg));
        }
    }

} // void CheckInputArguments (...)

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
void CheckOutputArgument (
    const CArgumentPtr given,
    const CArgumentPtr prototype)
{

    CheckArgumentConstraints (given, prototype, pI_TRUE);
} // void CheckOutputArgument (...)

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
void CheckOutputArgument (
    const ArgumentPtr& given,
    const ArgumentPtr& prototype)
{
    CheckOutputArgument (pI::GetCArgument(given), pI::GetCArgument(prototype));
} // void CheckOutputArgument (...)

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
void CheckOutputArguments (
    const Arguments& given,
    const Arguments& prototype,
    pI_int num /*= -1*/)
{
    // check if the last argument is a variable argument list
    if ((num > 0) && (static_cast<pI_int> (prototype.size()) < num)) {
        throw exception::IncompleteArgumentException("CheckOutputArguments failed due to insufficient prototype size.");
    }

    pI_bool has_val(
        (prototype.size() > 0) &&
        (GetCArgument(prototype[prototype.size() - 1])->signature.type == T_pI_Argument_VariableArgumentList));
    pI_size p_size(prototype.size() - (has_val == pI_TRUE ? 1 : 0));

    if ((given.size() < p_size) && ((num < 0) || (num > static_cast<pI_int> (given.size())))) {
        boost::format fmter("Incomplete output argument set detected (given: %1%, expected: %2%)");
        throw exception::IncompatibleArgumentException(
            (fmter % (given.size())
             % (prototype.size())).str().c_str());
    }

    size_t lv;

    for (lv = 0; (num < 0) ? (static_cast<pI_int> (lv) < p_size) : (static_cast<pI_int> (lv) < num); ++lv) {
        CheckOutputArgument (given[lv], prototype[lv]);
    }

    // check for correct types in the filled out variable argument list
    if (has_val == pI_TRUE) {
        ArgumentPtr val_arg(prototype[prototype.size() - 1]);

        for ( ; (num < 0) ? (lv < given.size()) : (static_cast<pI_int> (lv) < num); ++lv) {
            CheckArgumentType (GetCArgument(given[lv]), GetCArgument(val_arg));
        }
    }
} // void CheckOutputArguments (...)

} // namespace pI
