/* CpInAdapter.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <Adapters/C++/Runtime.hpp>
#include <Adapters/C++/CpInAdapter.hpp>


#ifdef PI_ADAPT_ARGUMENTS
#include <Adapters/C++/AdapterFactory.hpp>
#endif

#define SET_PLUGIN_ERROR(plugin,error,text) \
    do { \
        plugin->last_error = error; \
        plugin->plugin_runtime->FreeString (plugin->plugin_runtime, plugin->last_error_text); \
        plugin->last_error_text = plugin->plugin_runtime->CopyString (plugin->plugin_runtime, (pI_str) text); \
    } while (0)

namespace pI {

class CpInAdapterBase::AdapterImpl {

public:

    AdapterImpl(
        const CpInAdapterBase* base,
        Runtime& runtime);

	AdapterImpl(
        const CpInAdapterBase* base,
        const CRuntimePtr runtime);

    virtual ~AdapterImpl();

    const CpInPtr GetCpIn();

protected:

    struct veto_delete {
        void operator() (const void*) const {}
    }; // struct veto_delete

    static ArgumentPtr ObtainArgument (CArgumentPtr arg) {

#ifndef PI_ADAPT_ARGUMENTS
        return ArgumentPtr(arg, veto_delete());
#else
        return AdaptArgument(arg, pI_TRUE);
#endif

    }

    void AdaptParameters (CpInPtr instance);

    void AdaptPluginArguments (
        CpInPtr instance,
        const pI_bool initialized = pI_TRUE);

    static struct _CpIn* CpIn_Clone (
        struct _CpIn* instance,
        pI_bool stateful);

    static pI_bool CpIn_Destroy (struct _CpIn* instance);

    static pI_bool CpIn_Initialize (
        struct _CpIn* instance,
        struct _ArgumentList* parameters);

    static pI_bool CpIn_Execute (
        struct _CpIn* instance,
        struct _ArgumentList* input_arguments,
        struct _ArgumentList* output_arguments);

    static pI_bool CpIn_Serialize (struct _CpIn* instance);
    static pI_bool CpIn_Deserialize (struct _CpIn* instance, pI_str data);

    CpInPtr _c_pin;
    CpInAdapterBase* _base;
    Runtime _runtime;

}; // class CpInAdapterBase::AdapterImpl

CpInAdapterBase::CpInAdapterBase(Runtime runtime):
	_runtime(runtime),
    _delete_impl(false)
{

	try {
        _impl = new AdapterImpl(this, _runtime);
    } catch (...) {
        throw exception::MemoryException("Creation of adapter instance failed");
    }
} // CpInAdapterBase::CpInAdapterBase(const Runtime& runtime)

CpInAdapterBase::CpInAdapterBase(const CRuntimePtr cruntime):
	_runtime(cruntime),
    _delete_impl(false)
{

	try {
        _impl = new AdapterImpl(this, _runtime);
    } catch (...) {
        throw exception::MemoryException("Creation of adapter instance failed");
    }
} // CpInAdapterBase::CpInAdapterBase(const CRuntimePtr cruntime)

/*virtual*/ CpInAdapterBase::~CpInAdapterBase()
{

    if (_delete_impl) {
        delete _impl;
    }
}

const CpInPtr CpInAdapterBase::GetCpIn()
{

    return _impl->GetCpIn();
} // const CpInPtr CpInAdapterBase::GetCpIn()

CpInAdapterBase::AdapterImpl::AdapterImpl(
    const CpInAdapterBase* base, Runtime& runtime):
    _base(const_cast<CpInAdapterBase* > (base)),
    _c_pin(0),
    _runtime(runtime)
{

} // CpInAdapterBase::AdapterImpl::AdapterImpl(...)

CpInAdapterBase::AdapterImpl::AdapterImpl(
    const CpInAdapterBase* base, const CRuntimePtr runtime):
    _base(const_cast<CpInAdapterBase* > (base)),
    _c_pin(0),
    _runtime(runtime)
{

} // CpInAdapterBase::AdapterImpl::AdapterImpl(...)

/*virtual*/ CpInAdapterBase::AdapterImpl::~AdapterImpl()
{

    if (_base->_delete_impl == false) {
        delete _base;
    }
} // /*virtual*/ CpInAdapterBase::AdapterImpl::~AdapterImpl()

const CpInPtr CpInAdapterBase::AdapterImpl::GetCpIn()
{

    // if cached return instance
    if (_c_pin != 0) {
        return _c_pin;
    }

    // otherwise create and initialize the instance
    pInPtr ip;

    try {
        ip = _base->GetpIn();
    } catch (...) {
        _base->_delete_impl = true;
        return 0;
    }

    CRuntimePtr cruntime(ip->GetRuntime().GetCRuntime());

    if (cruntime == 0) {
        throw exception::MissingRuntimeException("Runtime instance without CRuntime encountered.");
    }

    CpInPtr plugin =
        (CpInPtr) cruntime->AllocateMemory (cruntime, sizeof(CpIn));

    if (plugin == 0) {
        return 0;
    }

    // set last error to NoError
    plugin->last_error = CpIn_Error_NoError;
    // zero last occurred error text
    plugin->last_error_text = 0;

    // version information
    plugin->api_version = ip->GetAPIVersion();
    plugin->plugin_version = ip->GetpInVersion();
    // strings
    plugin->author = cruntime->CopyString (cruntime, (pI_str) ip->GetAuthor().c_str());

    if (plugin->author == 0) {
        SET_PLUGIN_ERROR (
            plugin,
            CpIn_Error_InsufficientMemory,
            "Insufficient memory for CpInAdapter (author)");
    }

    plugin->copyright = cruntime->CopyString (cruntime, (pI_str) ip->GetCopyright().c_str());

    if (plugin->copyright == 0) {
        SET_PLUGIN_ERROR (
            plugin,
            CpIn_Error_InsufficientMemory,
            "Insufficient memory for CpInAdapter (copyright)");
    }

    plugin->description = cruntime->CopyString (cruntime, (pI_str) ip->GetDescription().c_str());

    if (plugin->description == 0) {
        SET_PLUGIN_ERROR (
            plugin,
            CpIn_Error_InsufficientMemory,
            "Insufficient memory for CpInAdapter (description)");
    }

    plugin->name = cruntime->CopyString (cruntime, (pI_str) ip->GetName().c_str());

    if (plugin->name == 0) {
        SET_PLUGIN_ERROR (
            plugin,
            CpIn_Error_InsufficientMemory,
            "Insufficient memory for CpInAdapter (name)");
    }

    // plugin runtime
    plugin->plugin_runtime = cruntime;
    // parameters
    AdaptParameters (plugin); // called ONCE
    // arguments
    AdaptPluginArguments (plugin, pI_FALSE);

    {
        // copy serialization data
        std::stringstream ser_data;

        try {
            ip->Serialize (ser_data);
            plugin->serialization_data = cruntime->CopyString (cruntime, (pI_str) ser_data.str().c_str());

            if (plugin->serialization_data == 0) {
                SET_PLUGIN_ERROR (
                    plugin,
                    CpIn_Error_InsufficientMemory,
                    "Insufficient memory for CpInAdapter (serialization data)");
            }
        } catch (exception::SerializationNotSupportedException& ) {
            plugin->serialization_data = 0;
            SET_PLUGIN_ERROR (
                plugin,
                CpIn_Error_SerializationNotSupported,
                "Serialization not implemented error");
        } catch (exception::SerializationException& ) {
            plugin->serialization_data = 0;
            SET_PLUGIN_ERROR (
                plugin,
                CpIn_Error_Serialization,
                "Serialization error");
        }
    }

    // set adapter info
    plugin->adapter_info = this;

    // set function pointers
    plugin->Clone = CpIn_Clone;
    plugin->Destroy = CpIn_Destroy;
    plugin->Initialize = CpIn_Initialize;
    plugin->Execute = CpIn_Execute;
    plugin->Serialize = CpIn_Serialize;
    plugin->Deserialize = CpIn_Deserialize;

    // zero state
    plugin->state_buffer = 0;
    plugin->state_buffer_len = 0;

    _c_pin = plugin;
    return plugin;
} // const CpInPtr CpInAdapterBase::AdapterImpl::GetCpIn()

void CpInAdapterBase::AdapterImpl::AdaptParameters (CpInPtr instance)
{

    pInPtr ip;

    try {
        ip = _base->GetpIn();
    } catch (...) {
        _base->_delete_impl = true;
        return;
    }

    Arguments args(ip->GetParameterSignature());

    if (args.size() == 0) {
        // in case of empty argument list zero instance's param list
        instance->parameters = 0;
        return;
    }

    instance->parameters = instance->plugin_runtime->AllocateArgumentList (
                               instance->plugin_runtime,
                               args.size());

    if (instance->parameters == 0) {
        throw exception::MemoryException("CpInAdapter failed allocating argument memory");
    }

    for (pI_size lv = 0; lv < args.size(); ++lv) {
        instance->parameters->list[lv] =
            instance->plugin_runtime->CopyArgument (
                instance->plugin_runtime,
                GetCArgument(args[lv]),
                pI_TRUE);

        // do not throw an exception in case of out of memory - object is still valid
        if (instance->parameters->list[lv] == 0) {
            // BUT do set error code
            SET_PLUGIN_ERROR (instance, CpIn_Error_InsufficientMemory, "Insufficient memory for parameter adaption.");
        }
    }
} // void CpInAdapterBase::AdapterImpl::AdaptParameters()

void CpInAdapterBase::AdapterImpl::AdaptPluginArguments (
    CpInPtr instance,
    const pI_bool initialized)
{

    pInPtr ip;

    try {
        ip = _base->GetpIn();
    } catch (...) {
        _base->_delete_impl = true;
        return;
    }

    Arguments in_args(ip->GetInputSignature());
    Arguments out_args(ip->GetOutputSignature());

    // in case both input and output argument lists are empty (odd plugin) do nothing but return
    if ((in_args.size() == 0) && (out_args.size() == 0)) {
        if (initialized == pI_FALSE) {
            instance->input_arg_signature = 0;
            instance->output_arg_signature = 0;
        }

        return;
    }

    // if input arguments already set, delete and rebuild them
    if ((initialized == pI_TRUE) &&
            (instance->input_arg_signature != 0) &&
            (instance->output_arg_signature != 0)) {
        // delete whole available argument list
        instance->plugin_runtime->FreeArgumentList (
            instance->plugin_runtime,
            instance->input_arg_signature,
            pI_TRUE,   // free arguments
            pI_TRUE);  // free argument data
        instance->plugin_runtime->FreeArgumentList (
            instance->plugin_runtime,
            instance->output_arg_signature,
            pI_TRUE,   // free arguments
            pI_TRUE);  // free argument data
    }

    // create the argument lists
    instance->input_arg_signature = instance->plugin_runtime->AllocateArgumentList (
                                        instance->plugin_runtime,
                                        in_args.size());

    if (instance->input_arg_signature == 0) {
        throw exception::MemoryException("CpInAdapter failed allocating argument memory");
    }

    instance->output_arg_signature = instance->plugin_runtime->AllocateArgumentList (
                                         instance->plugin_runtime,
                                         out_args.size());

    if (instance->output_arg_signature == 0) {
        throw exception::MemoryException("CpInAdapter failed allocating argument memory");
    }


    // fill the argument list with copies from c++ descriptors
    for (pI_size lv = 0; lv < in_args.size(); ++lv) {
        instance->input_arg_signature->list[lv] = instance->plugin_runtime->CopyArgument (
                    instance->plugin_runtime,
                    GetCArgument(in_args[lv]),
                    pI_TRUE); // copy data (on the other hand, this should not be necessary
        // do not throw an exception in case of out of memory - object is still valid
        if (instance->input_arg_signature->list[lv] == 0) {
            // BUT do set error code
            SET_PLUGIN_ERROR (
                instance,
                CpIn_Error_InsufficientMemory,
                "CpInAdapter failed in input argument cloning");
        }
    }

    for (pI_size lv = 0; lv < out_args.size(); ++lv) {
        instance->output_arg_signature->list[lv] =
            instance->plugin_runtime->CopyArgument (
                instance->plugin_runtime,
                GetCArgument(out_args[lv]),
                pI_TRUE); // copy data (on the other hand, this should not be necessary
        // do not throw an exception in case of out of memory - object is still valid
        if (instance->output_arg_signature->list[lv] == 0) {
            // BUT do set error code
            SET_PLUGIN_ERROR (
                instance,
                CpIn_Error_InsufficientMemory,
                "CpInAdapter failed in output argument cloning");
        }
    }
} // void CpInAdapterBase::AdapterImpl::AdaptPluginArguments()

/*static*/ struct _CpIn* CpInAdapterBase::AdapterImpl::CpIn_Clone (
    struct _CpIn* instance,
    pI_bool stateful) {

    if (instance == 0) {
        return pI_FALSE;
    }

    CpInAdapterBase::AdapterImpl* adapter(
        reinterpret_cast<CpInAdapterBase::AdapterImpl* > (instance->adapter_info));
    pInPtr plugin(adapter->_base->GetpIn());
    CpInCloningAdapter* ca = 0;

    if (stateful == pI_TRUE) {
        ca = const_cast<CpInCloningAdapter* > (CpInCloningAdapter::Create (plugin->Clone()));
    } else {
        ca = const_cast<CpInCloningAdapter* > (CpInCloningAdapter::Create (plugin->Create()));
    }

    CpIn* retval = const_cast<CpIn*> (ca->GetCpIn());
    retval->adapter_info = ca->_impl;
    return retval;
} // /*static*/ struct _CpIn* CpInAdapterBase::AdapterImpl::CpIn_Clone (...)

/*static*/ pI_bool CpInAdapterBase::AdapterImpl::CpIn_Destroy (
    struct _CpIn* instance)
{

    if (instance != 0) {
        if (instance->adapter_info != 0) {
            delete (CpInAdapterBase::AdapterImpl*) instance->adapter_info;
        }

        /* free strings */
        CRuntime* cruntime(instance->plugin_runtime);
        cruntime->FreeString (cruntime, instance->author);
        cruntime->FreeString (cruntime, instance->copyright);
        cruntime->FreeString (cruntime, instance->description);
        cruntime->FreeString (cruntime, instance->name);
        /* free params */
        cruntime->FreeArgumentList (cruntime, instance->parameters, pI_TRUE, pI_TRUE);
        /* free args */
        cruntime->FreeArgumentList (cruntime, instance->input_arg_signature, pI_TRUE, pI_TRUE);
        cruntime->FreeArgumentList (cruntime, instance->output_arg_signature, pI_TRUE, pI_TRUE);
        /* free last error text */
        cruntime->FreeString (cruntime, instance->last_error_text);
        /* free serialization buffer */
        cruntime->FreeString (cruntime, instance->serialization_data);
        /* free state buffer */
        cruntime->FreeMemory (cruntime, instance->state_buffer);
        /* finally free plugin's memory */
        cruntime->FreeMemory (cruntime, instance);
        return pI_TRUE;
    }

    return pI_FALSE;
} // /*static*/ pI_bool CpInAdapterBase::AdapterImpl::CpIn_Destroy (...)

/*static*/ pI_bool CpInAdapterBase::AdapterImpl::CpIn_Initialize (
    struct _CpIn* instance,
    struct _ArgumentList* parameters)
{

    if ((instance == 0) || (parameters == 0)) {
        return pI_FALSE;
    }

    // 1. transform parameters struct to vector for c++ interface
    CpInAdapterBase::AdapterImpl* adapter(
        reinterpret_cast<CpInAdapterBase::AdapterImpl* > (instance->adapter_info));
    Arguments cpp_params;

    try {
        for (pI_size lv = 0; lv < parameters->count; ++lv) {
            cpp_params.push_back(ObtainArgument (parameters->list[lv]));
        }
    } catch (...) {
        SET_PLUGIN_ERROR (
            instance,
            CpIn_Error_InsufficientMemory,
            "CpInAdapter failed in obtaining parameter signature");
        return pI_FALSE;
    }

    // 2. actually call c++ Initialize method
    pInPtr plugin(adapter->_base->GetpIn());
    bool retval = pI_FALSE;

    try {
        plugin->Initialize (cpp_params);
        retval = pI_TRUE;
    } catch (exception::MissingParameterException& e) {
        SET_PLUGIN_ERROR (instance, CpIn_Error_MissingParameters, e.what());
    } catch (exception::IncompatibleParameterException& e) {
        SET_PLUGIN_ERROR (instance, CpIn_Error_IncompatibleParameters, e.what());
    } catch (exception::MemoryException& e) {
        SET_PLUGIN_ERROR (instance, CpIn_Error_InsufficientMemory, e.what());
    } catch (exception::InitializationException& e) {
        SET_PLUGIN_ERROR (instance, CpIn_Error_InitializationFailed, e.what());
    } catch (...) {
        SET_PLUGIN_ERROR (
            instance,
            CpIn_Error_InitializationFailed,
            "CpInAdapter - initialization of base plugin failed");
    }

    // 3. refresh input and output descriptors (may have changed during Initialize)
    try {
        if (retval != pI_FALSE) {
            adapter->AdaptPluginArguments (instance);
        }
    } catch (...) {
        SET_PLUGIN_ERROR (
            instance,
            CpIn_Error_InitializationFailed,
            "CpInAdapter - argument adaption failed");
        throw;
    }

    return retval;
} // /*static*/ pI_bool CpInAdapterBase::AdapterImpl::CpIn_Initialize (...)

/*static*/ pI_bool CpInAdapterBase::AdapterImpl::CpIn_Execute (
    struct _CpIn* instance,
    struct _ArgumentList* input_arguments,
    struct _ArgumentList* output_arguments)
{

    if (instance == 0) {
        return pI_FALSE;
    }

    // 1. transform input argument structs to vectors for c++ interface
    CpInAdapterBase::AdapterImpl* adapter(
        reinterpret_cast<CpInAdapterBase::AdapterImpl* > (instance->adapter_info));
    Arguments input_args, output_args;

    try {
        if (input_arguments != 0) {
            for (pI_size lv = 0; lv < input_arguments->count; ++lv) {
                input_args.push_back (ObtainArgument (input_arguments->list[lv]));
            }
        }

        if (output_arguments != 0) {
            for (pI_size lv = 0; lv < output_arguments->count; ++lv) {
                output_args.push_back (ObtainArgument (output_arguments->list[lv]));
            }
        }
    } catch (...) {
        SET_PLUGIN_ERROR (
            instance,
            CpIn_Error_InsufficientMemory,
            "CpInAdapter failed in obtaining argument signatures");
        return pI_FALSE;
    }

    // 2. call c++ interface Process method
    pInPtr plugin(adapter->_base->GetpIn());

    try {
        plugin->_Execute (input_args, output_args);
    } catch (exception::NotInitializedException& e) {
        SET_PLUGIN_ERROR (instance, CpIn_Error_NotInitialized, e.what());
        return pI_FALSE;
    } catch (exception::MissingArgumentException& e) {
        SET_PLUGIN_ERROR (instance, CpIn_Error_MissingArguments, e.what());
        return pI_FALSE;
    } catch (exception::IncompatibleArgumentException& e) {
        SET_PLUGIN_ERROR (instance, CpIn_Error_IncompatibleArguments, e.what());
        return pI_FALSE;
    } catch (exception::IncompleteArgumentException& e) {
        SET_PLUGIN_ERROR (instance, CpIn_Error_IncompleteArguments, e.what());
        return pI_FALSE;
    } catch (exception::MemoryException& e) {
        SET_PLUGIN_ERROR (instance, CpIn_Error_InsufficientMemory, e.what());
        return pI_FALSE;
    } catch (exception::ExecutionException& e) {
        SET_PLUGIN_ERROR (instance, CpIn_Error_ExecutionFailed, e.what());
        return pI_FALSE;
    } catch (...) {
        SET_PLUGIN_ERROR (
            instance,
            CpIn_Error_ExecutionFailed,
            "CpInAdapter - execution of base plugin failed");
        return pI_FALSE;
    }

    // 3. finally bend back any argument the plugin may have re-allocated
    if (input_arguments != 0) {
        for (pI_size lv = 0; lv < input_arguments->count; ++lv) {
            // for now, just swap every argument
            input_arguments->list[lv]->runtime->SwapArguments (
                input_arguments->list[lv]->runtime,
                input_arguments->list[lv],
                GetCArgument(input_args[lv]));
        }
    }

    if (output_arguments != 0) {
        for (pI_size lv = 0; lv < output_arguments->count; ++lv) {
            // for now, just swap every argument
            output_arguments->list[lv]->runtime->SwapArguments (
                output_arguments->list[lv]->runtime,
                output_arguments->list[lv],
                GetCArgument(output_args[lv]));
        }
    }

    return pI_TRUE;
} // /*static*/ pI_bool CpInAdapterBase::AdapterImpl::CpIn_Process (...)

/*static*/ pI_bool CpInAdapterBase::AdapterImpl::CpIn_Serialize (
    struct _CpIn* instance)
{

    if (instance == 0) {
        return pI_FALSE;
    }

    std::stringstream ser_data;
    CpInAdapterBase::AdapterImpl* adapter(
        reinterpret_cast<CpInAdapterBase::AdapterImpl* > (instance->adapter_info));
    pInPtr plugin(adapter->_base->GetpIn());

    try {
        plugin->Serialize (ser_data);
    } catch (exception::SerializationNotSupportedException& e) {
        SET_PLUGIN_ERROR (instance, CpIn_Error_SerializationNotSupported, e.what());
        return pI_FALSE;
    } catch (exception::SerializationException& e) {
        SET_PLUGIN_ERROR (instance, CpIn_Error_Serialization, e.what());
        return pI_FALSE;
    } catch (exception::MemoryException& e) {
        SET_PLUGIN_ERROR (instance, CpIn_Error_InsufficientMemory, e.what());
        return pI_FALSE;
    }

    if (instance->serialization_data) {
        instance->plugin_runtime->FreeString (
            instance->plugin_runtime,
            instance->serialization_data);
    }

    instance->serialization_data =
        instance->plugin_runtime->CopyString (
            instance->plugin_runtime,
            (pI_str) ser_data.str().c_str());

    if (instance->serialization_data == 0) {
        SET_PLUGIN_ERROR (
            instance,
            CpIn_Error_InsufficientMemory,
            "CpInAdapter - failed to copy serialization data");
    }

    return ((instance->serialization_data != 0) ? pI_TRUE : pI_FALSE);
} // /*static*/ pI_bool CpInAdapterBase::AdapterImpl::CpIn_Serialize (...)

/*static*/ pI_bool CpInAdapterBase::AdapterImpl::CpIn_Deserialize (
    struct _CpIn* instance, pI_str data)
{

    if (instance == 0) {
        return pI_FALSE;
    }

    std::string ser_data;
    CpInAdapterBase::AdapterImpl* adapter(
        reinterpret_cast<CpInAdapterBase::AdapterImpl* > (instance->adapter_info));
    pInPtr plugin(adapter->_base->GetpIn());

    try {
        plugin->Deserialize (ser_data);
    } catch (exception::DeserializationNotSupportedException& e) {
        SET_PLUGIN_ERROR (instance, CpIn_Error_DeserializationNotSupported, e.what());
        return pI_FALSE;
    } catch (exception::DeserializationException& e) {
        SET_PLUGIN_ERROR (instance, CpIn_Error_Deserialization, e.what());
        return pI_FALSE;
    } catch (exception::MemoryException& e) {
        SET_PLUGIN_ERROR (instance, CpIn_Error_InsufficientMemory, e.what());
        return pI_FALSE;
    }

    instance->serialization_data =
        instance->plugin_runtime->CopyString (
            instance->plugin_runtime,
            (pI_str) ser_data.c_str());

    if (instance->serialization_data == 0) {
        SET_PLUGIN_ERROR (
            instance,
            CpIn_Error_InsufficientMemory,
            "CpInAdapter - failed to copy deserialization data");
    }

    return ((instance->serialization_data != 0) ? pI_TRUE : pI_FALSE);
} // /*static*/ pI_bool CpInAdapterBase::AdapterImpl::CpIn_Deserialize (...)

} // namespace pI
