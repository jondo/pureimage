from template_engine import *
from license_template import *

## Templates: #########################################################################################################

arguments_hpp = Template(
"""
$HEADER

#ifndef PUREIMAGE_ADAPTERS_CPP_ARGUMENTS_HPP__
#define PUREIMAGE_ADAPTERS_CPP_ARGUMENTS_HPP__

$INCLUDES

#endif /* PUREIMAGE_ADAPTERS_CPP_ARGUMENTS_HPP__ */
""")

include_stmt = Template(
"""
#include <Adapters/C++/Arguments/${arg.name}.hpp>
""")

## Template processing logic: #########################################################################################

def process(vars):

    HEADER = StringBuffer()
    HEADER << cpp_block_comment(unicode(license_text, errors='ignore')%("Arguments.hpp")) << ENDL
    HEADER << cpp_block_comment(auto_generation_warning%(__file__)) << ENDL

    INCLUDES = StringBuffer().println()
    
    # create a Matlab proxy class for each argument specification 
    for arg in vars.__root__.arguments:
        INCLUDES << expand_template(include_stmt)
    
    with open_file(vars.__outdir__ + "/Arguments.hpp") as f:
        f.write(expand_template(arguments_hpp))
