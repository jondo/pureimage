from template_engine import *
from license_template import *

## Templates: #########################################################################################################

argument_proxy = Template(
"""
$HEADER

#ifndef PUREIMAGE_ADAPTERS_CPP_ARGUMENTS_${arg.name.upper()}_HPP__
#define PUREIMAGE_ADAPTERS_CPP_ARGUMENTS_${arg.name.upper()}_HPP__

#include <PureImage.h>
#include <Adapters/C++/ArgumentAdapter.hpp>
#include <Adapters/C++/pInException.hpp>
#include <Adapters/C++/Runtime.hpp>
#include <boost/weak_ptr.hpp>

#include <Adapters/C++/Arguments.hpp>
#include <Adapters/C++/Structures.hpp>

namespace pI {

    class ${arg.name}: public pI::ArgumentAdapter {

    private:
    	_pI_Argument_${arg.name}_Data** data;

    	_pI_Argument_${arg.name}_Setup** setup;

    public:

        /**
         * Creates a new argument and takes ownership.
         */
        ${arg.name}(pI::Runtime runtime);

        /**
         * Copy constructor. Creates a copy of the given Argument
         * and takes ownership over the new one.
         */
        ${arg.name}(const ${arg.name}& arg);

        /**
         * Creates an adapter without taking ownership of argument.
         *
         * @throws exception::IncompatibleArgumentException
         *
         */
        ${arg.name}(const CArgumentPtr c_ptr);

#ifndef SWIG
        /**
         * Creates an adapter without taking ownership of argument.
         *
         * @throws exception::IncompatibleArgumentException
         *
         */
        ${arg.name}(boost::weak_ptr<Argument> arg);
#endif

        /**
         * Creates an adapter for the given argument taking ownership when desired.
         *
         * @throws exception::IncompatibleArgumentException
         *
         */
        ${arg.name}(const CArgumentPtr c_ptr, pI_bool take_ownership);

        virtual ~${arg.name}();
        
#ifndef SWIG

        /**
         * Makes this adapter a copy of the given argument.
         */
        ${arg.name}& operator=(${arg.name} arg);

#endif        

        //
        // Derived methods of ArgumentAdapter
        // ----------------------------------

        /**
         * Sets the constraint expression for this argument.
         *
         * @return this instance for chaining
         */
        virtual ${arg.name}& SetConstraints( const pI_char* value );

        /**
         * Sets the name of this argument.
         *
         * @return this instance for chaining
         */
        virtual ${arg.name}& SetName( const pI_char* value );

        /**
         * Sets the description of this argument.
         *
         * @return this instance for chaining
         */
        virtual ${arg.name}& SetDescription( const pI_char* value );

        /**
         * Defines if this argument can be used for output or as input only (i.e., read-only).
         *
         * @return this instance for chaining
         */
        virtual ${arg.name}& SetReadOnly(pI_bool value);

        /**
         * Creates the data using the information given by the setup properties.
         *
         * Note: After this call it is not allowed to change the dependent setup data.
         *       Call FreeData() before resetting the data.
         *
         * @return this instance for chaining
         */
        virtual ${arg.name}& CreateData();

        /**
         * Frees the previously created data and makes this argument reconfigurable.
         *
         * @return this instance for chaining
         */
        virtual ${arg.name}& FreeData();

        //
        // Setup Accessor Functions
        // -------------------------
        $SETUP_ACCESSORS

        //
        // Data Accessor Functions
        // --------------------------
        $DATA_ACCESSORS

    private:
        /**
         * Retrieves pointers to the data and setup structs of the underlying C struct.
         */
        void SetStructPointers();

    }; // class ${arg.name}

} // namespace pI

#ifndef SWIG

#include <Runtime/Runtime.h>
#include <Runtime/Internal/DataTypes/Intrinsics/IntrinsicRuntimeFunctions.h>

inline
pI::${arg.name}::${arg.name}(pI::Runtime runtime)
: pI::ArgumentAdapter(runtime, T_${arg.c_type})
{
  SetStructPointers();
}

inline
pI::${arg.name}::${arg.name}(const pI::${arg.name}& arg)
: pI::ArgumentAdapter(
	arg.c_ptr->runtime->CopyArgument(
		arg.c_ptr->runtime,
		arg.c_ptr.get(),
		pI_TRUE),
	pI_TRUE)
{
  SetStructPointers();
}

inline
pI::${arg.name}::${arg.name}(const CArgumentPtr c_ptr)
: pI::ArgumentAdapter(c_ptr, pI_FALSE)
{
    if( c_ptr == 0 || (c_ptr->signature.type != T_${arg.c_type}) ) {
        throw exception::IncompatibleArgumentException("Expected argument of type ${arg.name}.");
    }

  SetStructPointers();
}

inline
pI::${arg.name}::${arg.name}(boost::weak_ptr<Argument> arg)
: pI::ArgumentAdapter(arg.lock().get(), pI_FALSE)
{
    if( c_ptr == 0 || (c_ptr->signature.type != T_${arg.c_type}) ) {
        throw exception::IncompatibleArgumentException("Expected argument of type ${arg.name}.");
    }

  SetStructPointers();
}


inline
pI::${arg.name}::${arg.name}(const CArgumentPtr c_ptr, pI_bool take_ownership)
: pI::ArgumentAdapter(c_ptr, take_ownership)
{
    if( c_ptr == 0 || (c_ptr->signature.type != T_${arg.c_type}) ) {
        throw exception::IncompatibleArgumentException("Expected argument of type ${arg.name}.");
    }

  SetStructPointers();
}

inline
pI::${arg.name}&
pI::${arg.name}::operator=(${arg.name} other)
{
	${arg.name} tmp(other);

	c_ptr = tmp.c_ptr;
	data = tmp.data;
	setup = tmp.setup;

	return *this;
}

inline
pI::${arg.name}::~${arg.name}()
{
}

//
// Derived Setters of ArgumentAdapter
// ------------------------------------

inline
pI::${arg.name}&
pI::${arg.name}::SetConstraints(const pI_char* value) {
    ArgumentAdapter::SetConstraints(value);

    return *this;
}

inline
pI::${arg.name}&
pI::${arg.name}::SetName(const pI_char* value) {
    ArgumentAdapter::SetName(value);

    return *this;
}

inline
pI::${arg.name}&
pI::${arg.name}::SetDescription(const pI_char* value) {
    ArgumentAdapter::SetDescription(value);

    return *this;
}

inline
pI::${arg.name}&
pI::${arg.name}::SetReadOnly(pI_bool value) {
    ArgumentAdapter::SetReadOnly(value);

    return *this;
}

inline
pI::${arg.name}&
pI::${arg.name}::CreateData() {
    ArgumentAdapter::CreateData();
    ${ByteImage_ImplicitSetPitch if arg.name== 'ByteImage' else ''}

    return *this;
}

inline
pI::${arg.name}&
pI::${arg.name}::FreeData() {
    ArgumentAdapter::FreeData();

    return *this;
}

//
// Definition of Setup Accessor Functions
// ----------------------------------------
$DEFINE_SETUP_ACCESSORS

//
// Definition of Data Accessor Functions
// ---------------------------------------
$DEFINE_DATA_ACCESSORS

inline
void pI::${arg.name}::SetStructPointers()
{
	data = &(c_ptr->data.${arg.name});
	setup = &(c_ptr->signature.setup.${arg.name});
}

#endif // SWIG

#endif /* PUREIMAGE_ADAPTERS_CPP_ARGUMENTS_${arg.name.upper()}_HPP__ */

""")

setup_getter = Template(
"""
        /**
         * Gets the value of <tt>${s.name}</tt> which is a setup property of this argument.
         *
         * @return the value
         */
        ${s.type} Get${s.name.capitalize()}() const;
""")

setup_setter = Template(
"""
        /**
         * Sets the value of <tt>${s.name}</tt> which is a setup property of this argument.
         *
         * ${mandatory_field_note if s.name in dependent_setup_fields[arg.name] else ''}
         *
         * @param the new value
         * @return this instance for chaining
         */
        ${arg.name}& Set${s.name.capitalize()}(const ${returntype(s)} value);
""")

#    Note: non-intrinsics are supposed to be structures (or arguments) and are
#          return as a wrapper without ownership.
data_getter = Template(
"""
        /**
         * Gets the value of <tt>${d.name}</tt> which is a data property of this argument.
         *
         * @throws exception::ArgumentException
         *
         * @return the value
         */
        ${returntype(d)} Get${d.name.capitalize()}(${indices(d)}) const;
""")

#
# A getter to access the data pointer field conveniently
#
data_getter_pointer = Template(
"""
#ifndef SWIG
        /**
         * Gets the pointer field for <tt>${d.name}</tt> which is a data property of this argument.
         *
         * The pointer field allows convenient access to the underlying data
         * by means of array access. E.g., you can traverse the byte data of ByteImage like this:
         * \code
         * pI_byte*** data = img.GetDataPtr();
         * for (unsinged int row; row < img.GetHeight(); ++row) {
         *   for (unsinged int col; col < img.GetWidth(); ++col) {
         *     for (unsinged int c; c < img.GetChannels(); ++c) {
         *       data[row][col][c] = 0;
         *     }
         *   }
         * }
         * \endcode
         *
         * @throws exception::ArgumentException
         *
         * @return the pointer field
         */
        ${d.type}${'*' * d.dim} Get${d.name.capitalize()}Ptr();
#endif
""")

#
# Special getter if block depth == dim
#
data_getter_block = Template(
"""
#ifndef SWIG_NO_GETTER_FOR_BLOCKDATA
        /**
         * Gets the a pointer to the first element of <tt>${d.name}</tt> which is a data property of this argument.
         *
         * Note: this data has block layout, i.e., the data is located in a coherent block of memory.
         *       This way it is possible to traverse the field in c-style using pointer arithmetic.
         *
         * Attention: block data is aligned. Padding has to be considered for this approach.
         *
         * E.g., you can traverse the byte data of ByteImage like this:
         * \code
         * int padding = img.GetPitch() - (img.GetWidth()*img.GetChannels()*8);
         * pI_byte* dataPtr = img.GetDataBlock();
         * for (unsinged int row; row < img.GetHeight(); ++row) {
         *   for (unsinged int col; col < img.GetWidth(); ++col) {
         *     for (unsinged int c; c < img.GetChannels(); ++c) {
         *       *dataPtr = 0;
         *       dataPtr++;
         *     }
         *   }
         *   dataPtr += padding;
         * }
         * \endcode
         *
         * @return the pointer to the first element
         */
         ${d.type}* Get${d.name.capitalize()}Block();
#endif /* SWIG_NO_GETTER_FOR_BLOCKDATA */
""")


# Basic setter.
#
#   Note: provided for intrinsic data types only
#       changing complex data can be achieved by getting
#       a reference and change its data
data_setter = Template(
"""
        /**
         * Sets the value of <tt>${d.name}</tt> which is a data property of this argument.
         * $EXTRA_COMMENT
         *
         * @param the new value
         * @return this instance for chaining
         */
        ${arg.name}& Set${d.name.capitalize()}(${indices(d)}${optional_comma(d)}const ${arg_type(d)} value);
""")

mandatory_field_note = """
         * Note: this setup property is mandatory for creating the argument's data.
         *       It cannot be changed after <tt>CreateData()</tt> has been called.
         *
         * @throws exception::ArgumentException if data has been created already
"""
        
# an extra comment for using ByteImage.SetPitch()
ByteImage_Extra_Comment_For_Pitch = """
         * This value is currently set automatically by <tt>CreateData()</tt> using a 4-byte alignment:
         * \code
         *     SetPitch ((((GetChannels() * GetWidth() * 8) + 31) / 32) * 4);
         * \endcode
         *
"""

#  HACK! ByteImage:
#    currently we have a redundant data field which typically causes problems when not set (properly).
#    This method could be applied to all BlockData arguments.
#    TODO: think about a generalisation. Maybe there is a solution on C-side?
ByteImage_ImplicitSetPitch = """
    /**
     * Formula:
     *  (( number_of_bits_in_row + (nalign*8-1) ) / nalign*8) * nalign
     *  Here: nalign = 4 (bytes)
     */
    SetPitch ((((GetChannels() * GetWidth() * 8) + 31) / 32) * 4);
"""

hasdata_check = """
    // block write access to setup configuration if the data has been constructed already
    if(HasData()) {
        throw pI::exception::ArgumentException("Data has been created already. Change of dependent meta data not allowed.");
    }
"""

define_setup_getter = Template("""
inline
${s.type} pI::${arg.name}::Get${s.name.capitalize()}() const
{

    return c_ptr->signature.setup.${arg.name}->${s.name};
}
""")

define_setup_setter = Template(
"""
inline
pI::${arg.name}& pI::${arg.name}::Set${s.name.capitalize()}(const ${arg_type(s)} value)
{
    ${hasdata_check if s.name in dependent_setup_fields[arg.name] else ''}

    c_ptr->runtime->FreeIntrinsic(c_ptr->runtime,
                                  (pI_byte*) &(c_ptr->signature.setup.${arg.name}->${s.name}),
                                  _${s.type});

    c_ptr->runtime->CopyIntrinsicInplace(c_ptr->runtime,
                                         (pI_byte*) &(value),
                                         (pI_byte*) &(c_ptr->signature.setup.${arg.name}->${s.name}),
                                         _${s.type});

    return *this;
}
""")

define_data_getter = Template(
"""
inline
${returntype(d)} pI::${arg.name}::Get${d.name.capitalize()}(${indices(d)}) const
{
    if(!HasData()) {
      throw pI::exception::ArgumentException("No data available. Create data before using it.");
    }

    ${accessor_return_stmt(arg, d)}
}
""")

# Note: expanded by accessor_return_stmt(arg, d) in case of an intrinsic field type
accessor_return_stmt_intrinsics = Template("""
    return c_ptr->data.${arg.name}->${d.name}${array_access(d)};
""")

# Note: expanded by accessor_return_stmt(arg, d) in case of an arg field type
accessor_return_stmt_arg = Template("""
    ${returntype(d)} ret(&c_ptr->data.${arg.name}->${d.name}${array_access(d)});
    return ret;
""")

# Note: expanded by accessor_return_stmt(arg, d) in case of an struct field type
accessor_return_stmt_struct = Template("""
    ${returntype(d)} ret(c_ptr->runtime, &c_ptr->data.${arg.name}->${d.name}${array_access(d)});
    return ret;
""")

define_data_setter = Template(
"""
inline
pI::${arg.name}& pI::${arg.name}::Set${d.name.capitalize()}(${indices(d)}${optional_comma(d)} const ${arg_type(d)} value)
{
    if(!HasData()) {
        // Note: the allocation of data is done implicitely. This reflects a strict life cycle of arguments.
        this->CreateData();
    } else {
        c_ptr->runtime->FreeIntrinsic (c_ptr->runtime,
                                       (pI_byte*) &(c_ptr->data.${arg.name}->${d.name}${array_access(d)}),
                                       _${d.type});
    }

    c_ptr->runtime->CopyIntrinsicInplace(c_ptr->runtime,
                                         (pI_byte*) &value,
                                         (pI_byte*) &(c_ptr->data.${arg.name}->${d.name}${array_access(d)}),
                                         _${d.type});
    return *this;

}
""")

#<#if d.layout == 'block'>
#<#if d.dim == d.block_depth>
define_data_getter_block = Template(
"""
inline
${d.type}* pI::${arg.name}::Get${d.name.capitalize()}Block()
{
    if(!HasData()) {
      throw pI::exception::ArgumentException("No data available. Create data before using it.");
    }

    return &(c_ptr->data.${arg.name}->${d.name}${"[0]"*len(d.sizes)});
}
""")

define_data_getter_pointer = Template(
"""
inline
${d.type}${'*' * d.dim} pI::${arg.name}::Get${d.name.capitalize()}Ptr()
{

	return c_ptr->data.${arg.name}->${d.name};
}
""")

def optional_comma(d):
    if d.dim == 0:
        return ''
    else:
        return ','

def arg_type(d):
    if d.type == "pI_str":
        return 'pI_char*'
    else:
        return d.type

def indices(d):
    if d.dim == 0:
        return ''
    else:
        indices = ["const pI_size %s"%(t) for t in d.sizes]
        return ', '.join(indices)

def array_access(d):
    if d.dim == 0:
        return ''
    else:
        indices = ["[%s]"%(id) for id in d.sizes]
        return ''.join(indices)

def returntype(d):
    if d.type.startswith("pI_Argument_"):
        return 'pI::' + d.type[len("pI_Argument_"):]
    if d.type.startswith("pI_Structure_"):
        return 'pI::' + d.type[len("pI_Structure_"):]
    return d.type

def accessor_return_stmt(arg, d):
    if d.type.startswith("pI_Argument_"):
        return expand_template(accessor_return_stmt_arg)
    if d.type.startswith("pI_Structure_"):
        return expand_template(accessor_return_stmt_struct)

    return expand_template(accessor_return_stmt_intrinsics)
    
#intrinsic_types = {}

## Template processing logic: #########################################################################################

def process(vars):

    #intrinsic_types = vars.intrinsic_types
    
    # create a Matlab proxy class for each argument specification 
    for arg in vars.__root__.arguments:
        HEADER = StringBuffer()
        HEADER << cpp_block_comment(unicode(license_text, errors='ignore')%(arg.name + ".hpp")) << ENDL
        HEADER << cpp_block_comment(auto_generation_warning%(__file__)) << ENDL


        SETUP_ACCESSORS = StringBuffer().println()
        DEFINE_SETUP_ACCESSORS = StringBuffer().println()
        for s in arg.setup:
            SETUP_ACCESSORS << expand_template(setup_getter) << ENDL
            SETUP_ACCESSORS << expand_template(setup_setter, userdata=vars) << ENDL
            DEFINE_SETUP_ACCESSORS << expand_template(define_setup_getter) << ENDL
            DEFINE_SETUP_ACCESSORS << expand_template(define_setup_setter, userdata=vars) << ENDL

        DATA_ACCESSORS = StringBuffer().println()
        DEFINE_DATA_ACCESSORS = StringBuffer().println()
        for d in arg.data:
            DATA_ACCESSORS << expand_template(data_getter) << ENDL
            DEFINE_DATA_ACCESSORS << expand_template(define_data_getter) << ENDL
            
            if (d.dim > 0):
                DATA_ACCESSORS << expand_template(data_getter_pointer) << ENDL
                DEFINE_DATA_ACCESSORS << expand_template(define_data_getter_pointer) << ENDL
                
            if (d.dim > 0) and (d.layout == "block") and (d.dim == d.block_depth):
                DATA_ACCESSORS << expand_template(data_getter_block) << ENDL
                DEFINE_DATA_ACCESSORS << expand_template(define_data_getter_block) << ENDL

            EXTRA_COMMENT = ''
            if(arg.name == "ByteImage" and d.name == "pitch"):
                EXTRA_COMMENT = ByteImage_Extra_Comment_For_Pitch

            if (d.type in vars.intrinsic_types):
                DATA_ACCESSORS << expand_template(data_setter) << ENDL
                DEFINE_DATA_ACCESSORS << expand_template(define_data_setter) << ENDL
            
            
        with open_file(vars.__outdir__ + "/Arguments/" + arg.name + ".hpp") as f:
            f.write(expand_template(argument_proxy))
