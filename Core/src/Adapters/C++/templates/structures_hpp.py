from template_engine import *
from license_template import *

## Templates: #########################################################################################################

structures_hpp = Template(
"""
$HEADER

#ifndef PUREIMAGE_ADAPTERS_CPP_STRUCTURES_HPP__
#define PUREIMAGE_ADAPTERS_CPP_STRUCTURES_HPP__

$INCLUDES

#endif /* PUREIMAGE_ADAPTERS_CPP_STRUCTURES_HPP__ */
""")

include_stmt = Template(
"""
#include <Adapters/C++/Structures/${s.name}.hpp>
""")

## Template processing logic: #########################################################################################

def process(vars):

    HEADER = StringBuffer()
    HEADER << cpp_block_comment(unicode(license_text, errors='ignore')%("Structures.hpp")) << ENDL
    HEADER << cpp_block_comment(auto_generation_warning%(__file__)) << ENDL

    INCLUDES = StringBuffer().println()
    
    # create a Matlab proxy class for each argument specification 
    for s in vars.__root__.structures:
        INCLUDES << expand_template(include_stmt)
    
    with open_file(vars.__outdir__ + "/Structures.hpp") as f:
        f.write(expand_template(structures_hpp))
