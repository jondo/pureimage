from template_engine import *
from license_template import *

## Templates: #########################################################################################################

factory_cpp = Template(
"""
$HEADER

#ifndef PUREIMAGE_ADAPTERS_CPP_ARGUMENTS_ADAPTER_FACTORY_HPP__
#define PUREIMAGE_ADAPTERS_CPP_ARGUMENTS_ADAPTER_FACTORY_HPP__

#include <Adapters/C++/Arguments.hpp>

namespace pI {

    inline
    ArgumentAdapter* AdaptArgument(const CArgumentPtr c_ptr, pI_bool take_ownership)
    {
        pI::ArgumentAdapter* arg;
        switch(c_ptr->signature.type) {
        $CASES
        default:
            return 0;
        }
        
        return arg;
    }

} /* namespace pI */

#endif /* PUREIMAGE_ADAPTERS_CPP_ARGUMENTS_ADAPTER_FACTORY_HPP__ */
""")

arg_switch_statement = Template(
"""
      case T_${arg.c_type}:
      {
        arg = new pI::${arg.name}(c_ptr, take_ownership);
        break;
      }    
""")

## Template processing logic: #########################################################################################

def process(vars):

    HEADER = StringBuffer()
    HEADER << cpp_block_comment(unicode(license_text, errors='ignore')%("AdapterFactory.hpp")) << ENDL
    HEADER << cpp_block_comment(auto_generation_warning%(__file__)) << ENDL

    CASES = StringBuffer().println()
    
    # create a Matlab proxy class for each argument specification 
    for arg in vars.__root__.arguments:
        CASES << expand_template(arg_switch_statement)
    
    with open_file(vars.__outdir__ + "/AdapterFactory.hpp") as f:
        f.write(expand_template(factory_cpp))
