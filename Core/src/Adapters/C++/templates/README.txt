Templates for creating a (light-weight) C++ adapter for PureImage
=================================================================

The templates use a home-grown Python templating engine which is located in Tools/template_engine.
The generator is run from a CMake target defined in the ../CMakeLists.txt.

To run the generator manually:

1. Set PYTHONPATH to the location of template_engine, e.g.:
        
        set PYTHONPATH=<path-to-pureimage>\Tools\template_engine
        
2. Run the main generator script:
        
        python create_cpp_adapter.py <path-to-pI_types.json> <out-dir>
