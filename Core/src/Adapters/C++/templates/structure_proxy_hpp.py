from template_engine import *
from license_template import *

## Templates: #########################################################################################################

structure_proxy = Template(
"""
$HEADER

#ifndef PUREIMAGE_ADAPTERS_CPP_STRUCTURES_${s.name.upper()}_HPP__
#define PUREIMAGE_ADAPTERS_CPP_STRUCTURES_${s.name.upper()}_HPP__

#include <PureImage.h>
#include <Adapters/C++/ArgumentAdapter.hpp>
#include <Adapters/C++/pInException.hpp>
#include <Adapters/C++/Runtime.hpp>

namespace pI {

    /**
     * This is an adapter for a ${s.c_type} type.
     *
     * Note: no ownership is taken of the underlying data structure.
     */
    class ${s.name} {
    
    private:
      ${s.c_type}* c_ptr;
      CRuntimePtr cruntime;
      
    public:
        
        /**
         * Creates a wrapper for the given ${s.c_type} without taking ownership.
         * 
         * Note: structure types are (currently) created only within argument types
         *       and there are no setter functions in the argument adapters.
         *       Therefor, only weak adapters (without ownership) are necessary to allow
         *       convenient access to the underlying c structure.
         */
        ${s.name}(CRuntimePtr cruntime, ${s.c_type}* c_ptr);

        ~${s.name}();

        $METHOD_DECLARATIONS
        
    };

} /* namespace pI */

#ifndef SWIG

inline
pI::${s.name}::${s.name}(CRuntimePtr cruntime, ${s.c_type}* c_ptr)
: cruntime(cruntime), c_ptr(c_ptr)
{
}

inline
pI::${s.name}::~${s.name}()
{
  // no ownership 
}

$METHOD_DEFINITIONS

#endif //SWIG


#endif /* PUREIMAGE_ADAPTERS_CPP_STRUCTURES_${s.name.upper()}_HPP__ */
""")

field_getter = Template(
"""
        /**
         * Gets the value of the field <tt>${field.name}</tt>.
         *
         * @return the value
         */
        ${field.type} Get${field.name.capitalize()}();
""")

field_setter = Template(
"""
        /**
         * Sets the value of the field <tt>${field.name}</tt>.
         * 
         * @param the new value
         * @return this instance (for chaining)
         */
        ${s.name}& Set${field.name.capitalize()}(${field.type} value);
""")

define_field_getter = Template(
"""
inline
${field.type} pI::${s.name}::Get${field.name.capitalize()}()
{
    return c_ptr->${field.name};
}
""")

define_field_setter = Template(
"""
inline
pI::${s.name}& pI::${s.name}::Set${field.name.capitalize()}(${field.type} value)
{


    cruntime->FreeIntrinsic(cruntime, 
                            (pI_byte*) &(c_ptr->${field.name}),
                            _${field.type});
                                   
    cruntime->CopyIntrinsicInplace(cruntime, 
                                   (pI_byte*) &(value), 
                                   (pI_byte*) &(c_ptr->${field.name}),
                                   _${field.type});
    return *this;
}
""")

## Template processing logic: #########################################################################################

def process(vars):

    for s in vars.__root__.structures:
        HEADER = StringBuffer()
        HEADER << cpp_block_comment(unicode(license_text, errors='ignore')%(s.name + ".hpp")) << ENDL
        HEADER << cpp_block_comment(auto_generation_warning%(__file__)) << ENDL

        METHOD_DECLARATIONS = StringBuffer().println()
        METHOD_DEFINITIONS = StringBuffer().println()
        for field in s.fields:
            METHOD_DECLARATIONS << expand_template(field_getter) << ENDL
            METHOD_DEFINITIONS << expand_template(define_field_getter) << ENDL
            
            if field.type in vars.intrinsic_types:
                METHOD_DECLARATIONS << expand_template(field_setter) << ENDL
                METHOD_DEFINITIONS << expand_template(define_field_setter) << ENDL
            
        with open_file(vars.__outdir__ + "/Structures/" + s.name + ".hpp") as f:
            f.write(expand_template(structure_proxy))
