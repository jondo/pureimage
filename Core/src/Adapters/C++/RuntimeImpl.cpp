/* RuntimeImpl.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universit�t Linz,
 * Institut f�r Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <Adapters/C++/pIn.hpp>
#include <Adapters/C++/pInAdapter.hpp>
#include <Adapters/C++/CpInAdapter.hpp>
#include <Adapters/C++/Runtime.hpp>

#ifdef __cplusplus
extern "C" {
#endif

/*extern*/ CRuntimeError CppRuntimeRegisterPlugin (CRuntimePtr runtime, void* ip) {

#if defined(PI_RAW_PIN_PTR)
	pI::pInPtr pinPtr(reinterpret_cast<pI::pIn* > (ip));
#else // #if defined(PI_RAW_PIN_PTR)
	pI::pInPtr pinPtr(reinterpret_cast<pI::pIn* > (ip), pI::runtime::VetoDelete);
#endif // #if defined(PI_RAW_PIN_PTR)

	if (pinPtr == 0) {
        return pI_CRuntime_Error_InvalidPlugin;
    }

    if (runtime->HasPlugin (runtime, (pI_str) pinPtr->GetDescription().c_str()) == pI_TRUE) {
		return pI_CRuntime_Error_PluginRegistrationFailed;
    }

    pI::CpInCloningAdapter* ca(
        const_cast<pI::CpInCloningAdapter* > (
            pI::CpInCloningAdapter::Create (pinPtr)));

    if (runtime->RegisterPlugin (
                runtime,
                const_cast<CpInPtr > (ca->GetCpIn())) == pI_FALSE) {
        return pI_CRuntime_Error_PluginRegistrationFailed;
    }
	return pI_CRuntime_Error_NoError;
}

/*extern*/ void* CppRuntimeSpawnPlugin (CRuntimePtr runtime, const pI_char* name) {

	CpInPtr cip(runtime->SpawnPluginByName (runtime, const_cast<pI_str> (name)));

    if (cip == 0) {
		return 0;
    }

    return new pI::pInAdapter(cip, pI_TRUE);
}

/*extern*/ void CppRuntimeErasePlugin (void* ip) {

	delete reinterpret_cast<pI::pInAdapter* > (ip);
}

#ifdef __cplusplus
}
#endif
