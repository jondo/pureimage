/* pInAdapter.cpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <Adapters/C++/pInAdapter.hpp>

namespace pI {

pInAdapter::pInAdapter(
    const CpInPtr c_pin,
    const pI_bool take_ownership):
    Base(CreateRuntime (c_pin->plugin_runtime)),
    _c_plugin(const_cast<CpInPtr > (c_pin)),
    _own_plugin(take_ownership)
{

} // pInAdapter::pInAdapter(...)

/*virtual*/ pInAdapter::~pInAdapter()
{

    if (_own_plugin == pI_TRUE) {
        _c_plugin->Destroy (_c_plugin);
    }
} // /*virtual*/ pInAdapter::~pInAdapter()

/*virtual*/ pInPtr pInAdapter::Create() const
{

    return pInPtr(new pInAdapter(_c_plugin->Clone (_c_plugin, pI_FALSE), pI_TRUE));
} // /*virtual*/ pInPtr pInAdapter::Create() const

/*virtual*/ pInPtr pInAdapter::Clone() const
{

    return pInPtr(new pInAdapter(_c_plugin->Clone (_c_plugin, pI_TRUE), pI_TRUE));
} // /*virtual*/ pInPtr pInAdapter::Clone() const

/*virtual*/ const pI_int pInAdapter::GetpInVersion() const
{
    return _c_plugin->plugin_version;
} // /*virtual*/ const pI_size pInAdapter::GetPluginVersion() const

/*virtual*/ const std::string pInAdapter::GetCopyright() const
{
    return std::string((_c_plugin->copyright != 0) ? _c_plugin->copyright : "");
} // /*virtual*/ const std::string pInAdapter::GetCopyright() const

/*virtual*/ const std::string pInAdapter::GetDescription() const
{
    return std::string((_c_plugin->description != 0) ?_c_plugin->description : "");
} // /*virtual*/ const std::string pInAdapter::GetDescription() const

/*virtual*/ const std::string pInAdapter::GetAuthor() const
{
    return std::string(_c_plugin->author != 0 ? _c_plugin->author : "");
} // /*virtual*/ const std::string pInAdapter::GetAuthor() const

/*virtual*/ const std::string pInAdapter::GetName() const
{
    return std::string(_c_plugin->name != 0 ? _c_plugin->name : "");
} // /*virtual*/ const std::string pInAdapter::GetName() const

/*virtual*/ Arguments pInAdapter::GetParameterSignature() const
{
    Arguments retval;

    try {
        if (_c_plugin->parameters != 0) {
            for (pI_size lv = 0; lv < _c_plugin->parameters->count; ++lv) {
				Argument* arg_copy = _c_plugin->plugin_runtime->CopyArgument (
                            _c_plugin->plugin_runtime,
                            _c_plugin->parameters->list[lv],
                            pI_TRUE); // copy data
                retval.push_back (ObtainArgument (arg_copy)); 
            }
        }
    } catch (...) {
        throw exception::MemoryException("pInAdapter - insufficient memory for parameter signature");
    }

    return retval;
} // /*virtual*/ Arguments pInAdapter::GetParameterSignature() const

/*virtual*/ void pInAdapter::_Initialize (const Arguments& parameters)
{

    // transform c++ interface vector to c struct
    _ArgumentList* params = _c_plugin->plugin_runtime->AllocateArgumentList (
                                _c_plugin->plugin_runtime,
                                parameters.size());

    if (params == 0) {
        throw exception::MemoryException("pInAdapter - failed to allocate argument memory");
        /*return pI_FALSE;*/
    }

    for (pI_size lv = 0; lv < parameters.size(); ++lv) {
        params->list[lv] = GetCArgument(parameters[lv]);
    }

    const pI_bool success(_c_plugin->Initialize (_c_plugin, params));

    // delete temporary parameter list
    _c_plugin->plugin_runtime->FreeArgumentList (
        _c_plugin->plugin_runtime,
        params,
        pI_FALSE,
        pI_FALSE);

    // throw exceptions on failed initialization
    if (success == pI_FALSE) {
        if (_c_plugin->last_error == CpIn_Error_MissingParameters) {
            throw exception::MissingParameterException(_c_plugin->last_error_text);
        } else if (_c_plugin->last_error == CpIn_Error_IncompatibleParameters) {
            throw exception::IncompatibleParameterException(_c_plugin->last_error_text);
        } else if (_c_plugin->last_error == CpIn_Error_IncompleteParameters) {
            throw exception::IncompatibleParameterException(_c_plugin->last_error_text);
        } else if (_c_plugin->last_error == CpIn_Error_InsufficientMemory) {
            throw exception::MemoryException(_c_plugin->last_error_text);
        } else {
            throw exception::InitializationException(_c_plugin->last_error_text);
        }
    }
} // /*virtual*/ void pInAdapter::_Initialize (...)

/*virtual*/ Arguments pInAdapter::GetInputSignature() const
{

    // transform struct to c++ interface vector
    try {
        Arguments retval;

        if (_c_plugin->input_arg_signature != 0) {
            for (pI_size lv = 0; lv < _c_plugin->input_arg_signature->count; ++lv) {
				Argument* arg_copy = _c_plugin->plugin_runtime->CopyArgument (
                            _c_plugin->plugin_runtime,
                            _c_plugin->input_arg_signature->list[lv],
                            pI_TRUE);// copy data
                retval.push_back (ObtainArgument (arg_copy)); 
            }
        }

        return retval;
    } catch (...) {
        throw exception::MemoryException("pInAdapter - insufficient memory for input signature");
    }
} // /*virtual*/ Arguments pInAdapter::GetInputSignature() const

/*virtual*/ Arguments pInAdapter::GetOutputSignature() const
{

    try {
        Arguments retval;

        if (_c_plugin->output_arg_signature != 0) {
            for (pI_size lv = 0; lv < _c_plugin->output_arg_signature->count; ++lv) {
				Argument* arg_copy = _c_plugin->plugin_runtime->CopyArgument (
                            _c_plugin->plugin_runtime,
                            _c_plugin->output_arg_signature->list[lv],
                            pI_TRUE); // copy data
                retval.push_back (ObtainArgument (arg_copy)); 
            }
        }

        return retval;
    } catch (...) {
        throw exception::MemoryException("pInAdapter - insufficient memory for output signature");
    }
} // /*virtual*/ Arguments pInAdapter::GetOutputSignature() const

/*virtual*/ void pInAdapter::_Execute (Arguments& input_args, Arguments& output_args)
{

    // 1. transform arguments: c++ interface vectors to c struct
    ArgumentList* c_input_args = _c_plugin->plugin_runtime->AllocateArgumentList (
                                     _c_plugin->plugin_runtime,
                                     input_args.size());

    if (c_input_args == 0) {
        throw exception::MemoryException("pInAdapter - insufficient memory for execution");
    }

    ArgumentList* c_output_args = _c_plugin->plugin_runtime->AllocateArgumentList (
                                      _c_plugin->plugin_runtime,
                                      output_args.size());

    if (c_output_args == 0) {
        _c_plugin->plugin_runtime->FreeArgumentList (_c_plugin->plugin_runtime, c_input_args, pI_TRUE, pI_TRUE);
        throw exception::MemoryException("pInAdapter - insufficient memory for execution");
    }

    pI_size lv;

    for (lv = 0; lv < input_args.size(); ++lv) {
        c_input_args->list[lv] = GetCArgument (input_args[lv]);
    }

    for (lv = 0; lv < output_args.size(); ++lv) {
        c_output_args->list[lv] = GetCArgument (output_args[lv]);

    }

    // 2. call c-plugin's Execute
    const pI_bool success(_c_plugin->Execute (_c_plugin, c_input_args, c_output_args));

    // 3. delete argument list (list ONLY)
    _c_plugin->plugin_runtime->FreeArgumentList (_c_plugin->plugin_runtime, c_input_args, pI_FALSE, pI_FALSE);
    _c_plugin->plugin_runtime->FreeArgumentList (_c_plugin->plugin_runtime, c_output_args, pI_FALSE, pI_FALSE);

    // throw exceptions on failed execution
    if (success == pI_FALSE) {
        if (_c_plugin->last_error == CpIn_Error_MissingArguments) {
            throw exception::MissingArgumentException(_c_plugin->last_error_text);
        } else if (_c_plugin->last_error == CpIn_Error_IncompatibleArguments) {
            throw exception::IncompatibleArgumentException(_c_plugin->last_error_text);
        } else if (_c_plugin->last_error == CpIn_Error_IncompleteArguments) {
            throw exception::IncompleteArgumentException(_c_plugin->last_error_text);
        } else if (_c_plugin->last_error == CpIn_Error_InsufficientMemory) {
            throw exception::MemoryException(_c_plugin->last_error_text);
        } else {
            throw exception::ExecutionException(_c_plugin->last_error_text);
        }
    }
} // /*virtual*/ Arguments pInAdapter::_Execute (Arguments& input_args)

/*virtual*/ void pInAdapter::Serialize (
    std::string& serialization_data) const
{

    _c_plugin->last_error = CpIn_Error_NoError;

    if (_c_plugin->Serialize (_c_plugin) == pI_FALSE) {
        switch (_c_plugin->last_error) {
            case CpIn_Error_NoError:
                break;
            case CpIn_Error_InsufficientMemory:
                throw exception::MemoryException (_c_plugin->last_error_text);
            case CpIn_Error_SerializationNotSupported:
                throw exception::SerializationNotSupportedException (_c_plugin->last_error_text);
            case CpIn_Error_Serialization:
            default:
                throw exception::SerializationException (_c_plugin->last_error_text);
        }
    }

    serialization_data =
        _c_plugin->serialization_data != 0 ? _c_plugin->serialization_data : "";
} // /*virtual*/ void pInAdapter::Serialize (...) const

/*virtual*/ void pInAdapter::Deserialize (
    const std::string& serialization_data)
{

    _c_plugin->last_error = CpIn_Error_NoError;

    if (_c_plugin->Deserialize (_c_plugin, (pI_str) serialization_data.c_str()) == pI_FALSE) {
        switch (_c_plugin->last_error) {
            case CpIn_Error_NoError:
                break;
            case CpIn_Error_InsufficientMemory:
                throw exception::MemoryException (_c_plugin->last_error_text);
            case CpIn_Error_DeserializationNotSupported:
                throw exception::DeserializationNotSupportedException (_c_plugin->last_error_text);
            case CpIn_Error_Deserialization:
            default:
                throw exception::DeserializationException (_c_plugin->last_error_text);
        }
    }
} // /*virtual*/ void pInAdapter::Deserialize (...)

/*static*/ Runtime pInAdapter::CreateRuntime (const CRuntimePtr cruntime)
{

    return Runtime(cruntime);
} // /*static*/ Runtime pInAdapter::CreateRuntime (const CRuntime* cruntime)

} // namespace pI
