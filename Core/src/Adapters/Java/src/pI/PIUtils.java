/* pIUtils.java
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

package pI;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

public class PIUtils {

    public static final String ID_PUREIMAGE_PATH = "PUREIMAGE_PATH";
    public static final String ID_PI_PLUGIN_DIR = "PI_DIST_DIR";

    public static final String PI_JNI_LOAD_FROM_CLASSPATH = "PI_JNI_LOAD_FROM_CLASSPATH";
    public static final String PI_JNI_LIB_PATH = "PI_JNI_LIB_PATH";

    public static boolean lib_is_loaded = false;
    public static boolean lib_is_debug = false;

    public static boolean load_jni_lib() {
    	return load_jni_lib("pI_java_jni");
    }
    
    private static String getLibFileName(String libname, boolean debugSuffix) {
    	String libfile;
    	String osname = System.getProperty("os.name");

		// Adapt this to correctly support another OS.
		// See http://lopica.sourceforge.net/os.html for a list of platform names.
		if (osname.toLowerCase().startsWith("windows")) {
			if(debugSuffix) {
				libfile = String.format("%s_d.dll", libname);
			} else {
				libfile = String.format("%s.dll", libname);
			}
		} else {
			if(debugSuffix) {
				libfile = String.format("lib%s_d.so", libname);
			} else {
				libfile = String.format("lib%s.so", libname);
			}
		}
		
		return libfile;
    }
    
	public static boolean load_jni_lib(String libname) {
		if (lib_is_loaded == true) {
			return true;
		}

		String pi_jni_lib_path = System.getProperty(PI_JNI_LIB_PATH);
		String pi_jni_load_from_classpath = System.getProperty(PI_JNI_LOAD_FROM_CLASSPATH);

		if (pi_jni_lib_path != null || pi_jni_load_from_classpath != null) {
			// Manual loading of the JNI library allows to control the location of the DLL.
			// This loop tries to load the release version of the library; if that fails,
			// tries to load the debug version.
			for(int trial = 0; !lib_is_loaded && trial <= 1; ++trial) {
				
				String libfile = getLibFileName(libname, (trial == 1));
				
				String lib_path;
				if (pi_jni_lib_path != null) {
					lib_path = pi_jni_lib_path + File.separator + libfile;
					
					System.err.print("Loading JNI library from lib path " + lib_path);
				} else {
					// try to find the library in the class path
					ClassLoader cl = pI.class.getClassLoader();
					URL libUrl = cl.getResource(libfile);
					lib_path = libUrl.getFile();
	
					System.err.print("Loading JNI library from class path " + lib_path);
				}
	
				try {
					System.load(lib_path);
					System.err.println(".");
					lib_is_loaded = true;
					lib_is_debug = (trial == 1);
				} catch (UnsatisfiedLinkError e) {
					System.err.println(" failed.");
					if(trial == 1) {
						System.err.println("Native code library failed to load. \n" + e);
						return false;
					}
				}
			}

		} else {
			System.err.println("using automatic lib loader.");

			try {
				System.loadLibrary(libname);
				lib_is_loaded = true;
			} catch (UnsatisfiedLinkError e) {
				System.err.println("Native code library failed to load. \n" + e);
				return false;
			}
		}

		// This shutdown hook makes 'sure' that gc is called on System.exit
		// Though, there is not even a guaranty that this is called.
		java.lang.Runtime.getRuntime().addShutdownHook(
				new Thread(new Runnable() {
					@Override
					public void run() {
						System.gc();
						System.runFinalization();
					}
				}));

		return true;
	}

    /**
     * Extracts all plugin classes from a given jarFile
     *
     * @param jarPath
     *            path to a jar file containing Java pIns
     *
     * @return a list of pIn classes
     */
    public static List<String> getPluginClasses(String jarPath) {

        List<String> pluginClasses = new LinkedList<String>();

        // Skip known built in jars that may lie aside of plugin jars
        if (jarPath.endsWith("pI_java.jar")
                || jarPath.endsWith("pI_cpp_arguments.jar")) {
            return pluginClasses;
        }

        try {
            JarInputStream jarFile = new JarInputStream(new FileInputStream(
                    jarPath));
            JarEntry jarEntry;
            while (true) {
                jarEntry = jarFile.getNextJarEntry();
                if (jarEntry == null) {
                    break;
                }
                if (jarEntry.getName().endsWith(".class")) {
                    String classPath = jarEntry.getName();
                    // System.err.print("Processing class :" +
                    // jarEntry.getName());

                    classPath = classPath.replace('/', '.').replace(".class",
                            "");
                    try {
                        Class<?> clazz = PIUtils.class.getClassLoader()
                                .loadClass(classPath);
                        if (DirectorPlugin.class.isAssignableFrom(clazz)) {
                            pluginClasses.add(jarEntry.getName().replace(
                                    ".class", ""));
                        } else {
                        }
                    } catch (java.lang.Exception e) {
                        //e.printStackTrace();
                        System.err.println(e.getMessage());
                    }
                }
            }
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
        return pluginClasses;
    }
    
    public static void loadJNIFromPluginLibrary() {
    	load_jni_lib("Java_pIns");
    }

    public static void setSystemVariables(String PUREIMAGE_PATH,
            String jniLibDirectory, String pluginDir) {
        System.setProperty(ID_PUREIMAGE_PATH, PUREIMAGE_PATH);
        System.setProperty(PI_JNI_LIB_PATH, jniLibDirectory);
        System.setProperty(ID_PI_PLUGIN_DIR, pluginDir);
    }

    public static void setSystemVariables(String pIBaseDir) {
        String pluginDir = pIBaseDir + "/" + "libraries";
        String jniLibDir = pluginDir;
        setSystemVariables(pIBaseDir, jniLibDir, pluginDir);

    }

    public static String getPluginDir() {
        String distDir = System.getProperty(PIUtils.ID_PI_PLUGIN_DIR);
        if (distDir == null) {
            throw new RuntimeException(
                    "No variable found for distribution directory. Call PIUtils.setSystemVariables() first.");
        } else if (!new File(distDir).exists()) {
            throw new RuntimeException(
                    "Could not find distribution directory: " + distDir);
        }
        return distDir;
    }

    public static String getPIBaseDir() {
        String pIBaseDir = System.getProperty(ID_PUREIMAGE_PATH);
        if (pIBaseDir == null || pIBaseDir.isEmpty()) {
            pIBaseDir = System.getenv().get(ID_PUREIMAGE_PATH);
        }

        if (pIBaseDir == null || pIBaseDir.isEmpty()) {
            throw new java.lang.RuntimeException(
                    "No variable PUREIMAGE_PATH set. Use System.setProperty or use an environment variable.");
        }

        return pIBaseDir;
    }

    /**
     * Initialize the PureImage JNI connector using a specified pureImage base
     * directory.
     *
     * @throws java.lang.RuntimeException
     *             if path to PureImage installation is invalid.
     *
     * @param pIBaseDir
     *            path to pureImage installation.
     * @param DEBUG
     *            indicates whether to load debug versions of plugins.
     */
    public static void initJNI(String pIBaseDir) {
        setSystemVariables(pIBaseDir);

        if (pIBaseDir == null) {
            throw new java.lang.RuntimeException(
                    "Variable PUREIMAGE_PATH must be set.");
        } else if (!new File(pIBaseDir).exists()) {
            throw new java.lang.RuntimeException(
                    "Could not find pureImage installation directory: "
                            + pIBaseDir);
        }

        load_jni_lib();
    }

    /**
     * Initialize the PureImage JNI connector.
     *
     * @throws java.lang.RuntimeException
     *             if variable PUREIMAGE_PATH has not been set.
     *
     * @param DEBUG
     *            indicates whether to load debug versions of plugins.
     */
    public static void initJNI() {
        String pIBaseDir = getPIBaseDir();
        initJNI(pIBaseDir);
    }

    public static void setRuntimeProperty(Runtime runtime, String key,
            String value) {
        StringValue prop = new StringValue(runtime).SetName(key)
                .SetData(value);
        // Note: the argument is going to be copied, so no ownership problem.
        runtime.StoreProperty(prop.GetCPtr());
    }

    public static void setRuntimeProperty(Runtime runtime, String key,
            boolean value) {
        BoolValue prop = new BoolValue(runtime).SetName(key)
                .SetData(value);
        // Note: the argument is going to be copied, so no ownership problem.
        runtime.StoreProperty(prop.GetCPtr());
    }

    public static void setSwigOwnership(Argument arg, boolean val) {
        arg.swigCMemOwn = val;
    }

    public static long getSwigCPtr(Argument proxy) {
        return Argument.getCPtr(proxy);
    }

    public static long getSwigCPtr(SWIGTYPE_p_unsigned_char proxy) {
        return SWIGTYPE_p_unsigned_char.getCPtr(proxy);
    }

    public static String create3rdPartyClasspath(String...jars) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < jars.length; i++) {
            String jar = getPIBaseDir() + File.separator + "3rdParty" + File.separator + jars[i];
            if(! new File(jar).exists()) {
                System.err.println("Missing 3rdParty archive: "+ jar);
                continue;
            }

            if(i>0) {
                sb.append(File.pathSeparator);
            }
            sb.append(jar);
        }
        return sb.toString();
    }

    public static String[] toArray(StringVector vec) {
        String[] res = new String[(int) vec.size()];

        for (int i = 0; i < res.length; i++) {
            res[i] = vec.get(i);
        }

        return res;
    }

    public static String readFile(String file) throws IOException {
        int len = (int) (new File(file)).length();
        FileInputStream fis = new FileInputStream(file);
        byte buf[] = new byte[len];
        fis.read(buf);
        fis.close();

        return new String(buf);
    }

	// / method to combine plugin spawning and initialization
	public static pIn SpawnAndInitialize(Runtime runtime, String pluginUri,
			Object... args) {
	
		pIn retval = runtime.SpawnPlugin(pluginUri);
	
		ArgumentVector params = retval.GetParameterSignature();
	
		// now comes the evil and unsafe hack (not type safe, only support for
		// limited types, serious problems when too few parameters are given)...
		for (int lv = 0; lv < params.size(); ++lv) {
			switch (params.Get(lv).GetType()) {
			case T_pI_Argument_IntValue: {
				((IntValue) params.Get(lv)).SetData((Integer) args[lv]);
				break;
			}
			case T_pI_Argument_StringValue: {
				((StringValue) params.Get(lv)).SetData((String) args[lv]);
				break;
			}
			case T_pI_Argument_BoolValue: {
				((BoolValue) params.Get(lv)).SetData((Boolean) args[lv]);
				break;
			}
			case T_pI_Argument_DoubleValue: {
				((DoubleValue) params.Get(lv)).SetData((Double) args[lv]);
				break;
			}
			case T_pI_Argument_StringSelection: {
				((StringSelection) params.Get(lv)).SetIndex((Integer) args[lv]);
				break;
			}
			default:
				throw new IncompatibleParameterException(
						"Failed to fast-construct plugin with given parameters.");
			}
		}
	
		retval.Initialize(params);
	
		return retval;
	} // pIn SpawnAndInitialize (pI_str plugin, ...)

} // end class
