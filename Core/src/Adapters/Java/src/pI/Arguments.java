/* Arguments.java
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

package pI;

public class Arguments {

	public static Argument[] toArray(ArgumentVector args, boolean own) {
		Argument[] result = new Argument[(int)args.size()];
		for (int i = 0; i < args.size(); i++) {
			result[i] = args.Get(i);
		}
		return result;
	}

	public static ArgumentVector toVector(Argument... args) {
		// ATTENTION: the ArgumentVector will have weak refs only
		//	TODO: is this ok? or will crash?
		ArgumentVector result = new ArgumentVector();
		for (Argument arg : args) {
			result.add(arg.GetCPtr());
		}
		return result;
	}

	
	public static Argument copyArgument(Runtime runtime, Argument arg) {
		Argument copy = pI.adaptArgument(runtime.CopyArgument(arg.GetCPtr(), true),true);

		return copy;
	}
	
	public static long getCPtr(Argument a) {
		return SWIGTYPE_p_Argument.getCPtr(a.GetCPtr());
	}
}
