/* Runtime.c
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

package pI;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;

public class LauncherUtil {

	public static String findPIBaseDir() {
		File currentDir = new File(".").getAbsoluteFile();
		String distFolder;
		distFolder = File.separator + "libraries";
		
		while(true) {
			String dist = currentDir.getAbsolutePath() + distFolder;
			if(new File(dist).exists()) {
				return currentDir.getAbsolutePath();
			}
			currentDir = currentDir.getParentFile();
			
			if(currentDir == null) {
				throw new java.lang.RuntimeException("Could not find parent folder containing " + distFolder);
			}
		}
	}

	public static ClassLoader createClassLoader(ClassLoader parent, String...paths) {
		if(parent == null) {
			parent = ClassLoader.getSystemClassLoader();
		}
		ArrayList<URL> urls = new ArrayList<URL>();
		try {
			for (String p : paths) {
				File file = new File(p);
				if(!file.exists() && !file.isAbsolute()) {
					// this way we can start the JVM 
					// specifying a location for jar libraries
					// and create a ClassLoader using relative paths
					URL resource = parent.getResource(p);
					if(resource != null) {
						urls.add(resource);
					}
				} else {
					urls.add(file.toURI().toURL());
				}
			}
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
			throw new java.lang.RuntimeException(e1);
		}
//		URLClassLoader loader = new URLClassLoader(urls.toArray(new URL[0]), URLClassLoader.getSystemClassLoader());
		URLClassLoader loader = new URLClassLoader(urls.toArray(new URL[0]), null);
	
		return loader;
	}

	public static void launchClass(ClassLoader loader, String clazzName, String[] args) throws ClassNotFoundException {
		Class<?> clazz;
		clazz = loader.loadClass(clazzName);
				
		try {
			Method _main;
			_main = clazz.getMethod("main", String[].class);
			_main.invoke(null, new Object[]{ args });
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}		
	}

	public static String libraryDirname() {
		return "libraries";
	}

	public static String thirdPartyDirname() {
		return "libraries";
	}

}
