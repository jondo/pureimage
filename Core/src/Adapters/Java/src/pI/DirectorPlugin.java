/* DirectorPlugin.java
*
* Copyright 2010-2011 Johannes Kepler Universität Linz,
* Institut für wissensbasierte Mathematische Systeme.
*
* This file is part of pureImage.
*
* pureImage is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License, version 3,
* as published by the Free Software Foundation.
*
* pureImage is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty
* of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with pureImage. If not, see <http://www.gnu.org/licenses/>.
*/
package pI;

import java.lang.reflect.Constructor;

/**
 * This class can be used for bi-directional adaptation with C++ runtime.
 *
 */
public abstract class DirectorPlugin extends PluginAdapter {

    protected Runtime runtime;


    public DirectorPlugin(Runtime runtime) {
        super(runtime);
        this.runtime = runtime;
    }

    @Override
    public Runtime GetRuntime() {
        return runtime;
    }

/*    @Override
    public int GetAPIVersion() {
        return runtime.GetAPIVersion();
    }
*/

    @Override
    public void Deserialize(String s) {
        System.err.println("Warning: Deserialize() has not been implemented by sub-class");
    }

    @Override
    public void Serialize(stringstream sstream) {
        System.err.println("Warning: Serialize() has not been implemented by sub-class");
    }

    @Override
    public pIn Create() {
        Constructor<? extends pIn> ctor;
        try {
            ctor = this.getClass().getConstructor(Runtime.class);
            return ctor.newInstance(this.runtime);
        } catch (java.lang.Exception e) {
            throw new java.lang.RuntimeException(e);
        }
    }

    @Override
    public pIn Clone() {
        pIn copy = this.Create();
        stringstream sb = new stringstream();
        this.Serialize(sb);
        copy.Deserialize(sb.toString());
        return copy;
    }
}
