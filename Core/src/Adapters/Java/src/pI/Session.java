/* Session.java
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */


package pI;

import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.LinkedHashMap;

public class Session {


	// NOTE: formerly, I used ThreadLocal for this session, to allow at least multiple instances by using multiple Threads
	//		 draw back: java gc runs in its own thread and can not access this scope.
	//		 So I returned to classical singleton
	//		 Alternatively, one could store the Session reference in the managed instances instead and use ThreadLocal
//  private static ThreadLocal<Session> _instance = new ThreadLocal<Session>();
	private static Session _instance = null;

    private final LinkedHashMap<Long, WeakReference<pIn>> plugins;

    private final LinkedHashMap<Long, WeakReference<Argument>> arguments;

    private Session() {
        plugins = new  LinkedHashMap<Long, WeakReference<pIn>>();
        arguments = new  LinkedHashMap<Long, WeakReference<Argument>>();
    }

    public static Session instance() {
      if(_instance == null) {
          _instance = new Session();
      }
      return _instance;
    }

    public synchronized boolean hasPlugins() {
        return plugins.size()>0;
    }

    public synchronized pIn popPlugin() {
    	Iterator<Long> it = plugins.keySet().iterator();
    	if(it.hasNext()) {
    		Long key = it.next();
    		WeakReference<pIn> ref = plugins.get(key);
    		plugins.remove(key);
            return ref.get();    		
    	} else {
    		return null;
    	}
    }

    public synchronized void addPlugin(pIn p) {
    	long key = pIn.getCPtr(p);
    	if(plugins.containsKey(key)) {
    		System.err.println("Warning: already registered a ref for plugin. Might cause deletion hazards");
    	}
        plugins.put(key, new WeakReference<pIn>(p));
    }

    public synchronized void removePlugin(pIn p) {
        plugins.remove(p);
    }

    public synchronized boolean hasArguments() {
        return arguments.size()>0;
    }

    public synchronized Argument popArgument() {
    	Iterator<Long> it = arguments.keySet().iterator();
    	if(it.hasNext()) {
    		Long key = it.next();
    		WeakReference<Argument> ref = arguments.get(key);
    		arguments.remove(key);
            return ref.get();    		
    	} else {
    		return null;
    	}    	
    }

    public synchronized void addArgument(Argument a) {
    	long key = SWIGTYPE_p_Argument.getCPtr(a.GetCPtr());
    	if(arguments.containsKey(key)) {
    		System.err.println("Warning: already registered a ref for argument. Might cause deletion hazards");
    	}    	
        arguments.put(key, new WeakReference<Argument>(a));
    }

    public synchronized void removeArgument(Argument a) {
    	long key = SWIGTYPE_p_Argument.getCPtr(a.GetCPtr());
    	if(!arguments.containsKey(key)) {
    		System.err.println("Warning: argument is not registered.");
    	}
        arguments.remove(key);
    }

}
