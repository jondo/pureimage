/* Exceptions.i
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * This file maps c++ exceptions to java exceptions.
 * 
 * If c++ interfaces declare thrown exceptions swig will generate try-catching code which
 * throws a generic java RuntimeException containing the name of the c++ exception.
 * 
 * But, to be completely equivalent the corresponding java exception proxies should be thrown.
 * For that we first have to establish inheritance of pI::exception::Exception to java.lang.RuntimeException.
 * Then we have to override the java throws typemap for every each exception.
 */
 
%include "exception.i"
%include "std_except.i"


%typemap(javabase) pI::exception::Exception "java.lang.RuntimeException";

%include "pIException.hpp"
%include "pInException.hpp"
%include "RuntimeException.hpp"
%include "ApplicationException.hpp"

%{
void SWIG_pI_JavaThrowException(JNIEnv* jenv, const char* class_name, const char* msg)
{
  jenv->ExceptionClear();
  jclass excep = jenv->FindClass(class_name);

  if (excep) {
    jenv->ThrowNew(excep, msg);
  } else {
    // should not happen
    throw "Could not find exception class.";
  }
  
}

%}

namespace pI {
namespace exception {
%extend Exception {
 const char* toString() {
    return self->what();
 } 
}
}

%typemap(throws) exception::MemoryException %{
  SWIG_pI_JavaThrowException(jenv, "pI/MemoryException", $1.what());
  return $null;
%}


%typemap(throws) exception::PluginCreationException %{
  SWIG_pI_JavaThrowException(jenv, "pI/PluginCreationException", $1.what());
  return $null;
%}

%typemap(throws) exception::PluginLibraryException %{
  SWIG_pI_JavaThrowException(jenv, "pI/PluginLibraryException", $1.what());
  return $null;
%}

%typemap(throws) exception::MissingRuntimeException %{
  SWIG_pI_JavaThrowException(jenv, "pI/MissingRuntimeException", $1.what());
  return $null;
%}

%typemap(throws) exception::ParameterException %{
  SWIG_pI_JavaThrowException(jenv, "pI/ParameterException", $1.what());
  return $null;
%}

%typemap(throws) exception::MissingParameterException %{
  SWIG_pI_JavaThrowException(jenv, "pI/MissingParameterException", $1.what());
  return $null;
%}

%typemap(throws) exception::IncompatibleParameterException %{
  SWIG_pI_JavaThrowException(jenv, "pI/IncompatibleParameterException", $1.what());
  return $null;
%}

%typemap(throws) exception::IncompleteParameterException %{
  SWIG_pI_JavaThrowException(jenv, "pI/IncompleteParameterException", $1.what());
  return $null;
%}

%typemap(throws) exception::ArgumentException %{
  SWIG_pI_JavaThrowException(jenv, "pI/ArgumentException", $1.what());
  return $null;
%}

%typemap(throws) exception::IncompatibleArgumentException %{
  SWIG_pI_JavaThrowException(jenv, "pI/IncompatibleArgumentException", $1.what());
  return $null;
%}

%typemap(throws) exception::MissingArgumentException %{
  SWIG_pI_JavaThrowException(jenv, "pI/MissingArgumentException", $1.what());
  return $null;
%}

%typemap(throws) exception::IncompleteArgumentException %{
  SWIG_pI_JavaThrowException(jenv, "pI/IncompleteArgumentException", $1.what());
  return $null;
%}


%typemap(throws) exception::NotInitializedException %{
  SWIG_pI_JavaThrowException(jenv, "pI/NotInitializedException", $1.what());
  return $null;
%}

%typemap(throws) exception::InitializationException %{
  SWIG_pI_JavaThrowException(jenv, "pI/InitializationException", $1.what());
  return $null;
%}

%typemap(throws) exception::ExecutionException %{
  SWIG_pI_JavaThrowException(jenv, "pI/ExecutionException", $1.what());
  return $null;
%}

%typemap(throws) exception::SerializationException %{
  SWIG_pI_JavaThrowException(jenv, "pI/SerializationException", $1.what());
  return $null;
%}

%typemap(throws) exception::DeserializationException %{
  SWIG_pI_JavaThrowException(jenv, "pI/DeserializationException", $1.what());
  return $null;
%}



}
