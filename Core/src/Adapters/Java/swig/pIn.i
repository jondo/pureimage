/* pIn.i
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */
 
%include "Adapters/C++/pInTypedefs.hpp"
 
/**
Built-in Data Type for Argument Lists
=========================================

For Java, List (ArrayList) shall be used as container for Arguments.
Of course, this is only kind of sugar, but greatly simplifies usage of
plugin methods.

Though, this is a bit more difficult part of porting PureImage with Swig.
It was not possible (for me) to map a c++ type to an exisiting complex data 
type and insert adequate transformation code: i.e., map std::vector to
java.util.ArrayList and insert some lines of code that converts between these
two data structures. 

All tries resulted in erroneously generated jni calls. Particularly,
it was not possible to have a multi-expression replacement
but only inline -- but, for conversion multiple statements
are necessary. 

The solution followed here is to make the originial proxy methods private
and manually create public methods with the desired interface that 
delegate to the private proxy methods.

*/

%include <sstream.i>

// Add interface
// ---------------

// Definition of Helpers and Delegators
// ---------------------------------------

// tells swig that ownership must be taken for returned instances 
%newobject pI::pIn::Create;
%newobject pI::pIn::Clone;


// Processing of Original Declaration
// -------------------------------------
%include "Adapters/C++/pIn.hpp"


// Adding director PluginAdapter
// -----------------------------

%feature("director") pI::PluginAdapter; 

%include "Adapters/C++/PluginAdapter.hpp"

//%feature("nodirector") pI::PluginAdapter::Create;
