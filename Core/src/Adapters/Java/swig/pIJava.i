/* pIJava.i
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
 /**
  * This is the root swig file for the java target.
  */
 
%module(directors="1") pI

/*
 * Preprocessor defines 
 * ======================
 * Note: these are necessary to get rid e.g. of shared_ptr managed arguments.
 *       This is necessary, as Java itself has a memory management.
 */
//%define PI_ADAPT_ARGUMENTS
//%enddef
%define PI_RAW_PIN_PTR
%enddef

/*
 * Includes for the C++ wrapper implementation
 * ============================================== 
 */
%{
#include <PureImage.h>
#include <Adapters/C++/ArgumentTypedefs.hpp>
#include <Adapters/C++/AdapterFactory.hpp>
#include <Adapters/C++/pInException.hpp>
#include <Adapters/C++/Runtime.hpp>
#include <Adapters/C++/pIn.hpp>
#include <Adapters/C++/PluginAdapter.hpp>
#include <Adapters/C++/Application.hpp>
#include <Adapters/C++/ArgumentAdapter.hpp>
#include <Adapters/C++/Arguments.hpp>
#include <Adapters/C++/Structures.hpp>
#include <Adapters/C++/AdapterFactory.hpp>
#include <Adapters/C++/ArgumentChecks.hpp>

using namespace pI;
%}

/*
 * Imports for Java module class
 * ===============================
 */
%pragma(java) moduleimports=%{
import java.util.List;
import java.util.ArrayList;
import java.io.File;
import java.net.URL;
%}


/*
 * SWIG dll loading mechanism (Java)
 * ============================
 */
//%include "LoadLibrary.i"

/*
 * Exception mapping (Java)
 * ===================
 */
%include "Exceptions_java.i"
%include "Exceptions.i"

/*
 * Mapping of Primitives
 * =======================
 */
/* TODO: define types in an extra typedef swig-file */
typedef unsigned char pI_byte;
typedef int pI_int;
typedef unsigned int pI_unsigned;
typedef unsigned int pI_size;
typedef short pI_short;
typedef unsigned short pI_ushort;
typedef float pI_float;
typedef double pI_double;
typedef bool pI_bool;
typedef char pI_char;
typedef char* pI_str;

/*
 * Mapping for complex types
 * ===========================
 */
%include <std_string.i>
%include <boost_shared_ptr.i>

/*
 * Chaining mechanism for setters
 * ================================
 * Note: otherwise SWIG creates a new proxy for each returned instance.
 *		 This would not lead to an incorrect behaviour, though, it is more reasonable to see the same instances
 *		 in Java as well.
 *  Attention: This changes all setter methods that return a reference
*              to an instance of the defining class.
 */
namespace pI {
  %typemap(javaout) SWIGTYPE& (SWIGTYPE::Set*) {
      $jnicall;
      return this;
  }
  %typemap(javaout) SWIGTYPE& (SWIGTYPE::CreateData) {
      $jnicall;
      return this;
  }  
}

/**
 * Structure adapters
 * ====================
 */
%include "Structures.i"

/*
 * Argument adapters
 * ====================
 */

 // Note: important to have 'Java/ArgumentCheck.i' first
//       as it defines mappings for argument checkers
%include "ArgumentTypes.i"

// make all Arguments be managed by session
%include "ManagedArgument.i"

// shorter class name in java
%rename(Argument) pI::ArgumentAdapter;

%include "Adapters/C++/ArgumentAdapter.hpp"
// the main SWIG stuff for argument adaptation (platform independent)
%include "Arguments.i"
// a Java factory for generation and down casting of ArgumentAdapter proxies
%include "AdapterFactory.i"

%include "ArgumentCheck.i"
%include "ArgumentUtils.i"

/*
 * Plugin adapter
 * =================
 */
// make all pIns managed by session
%include "ManagedPlugin.i"
%include "pIn.i"

/*
 * Runtime adapter
 * =================
 */
%include "Runtime.i"
CRuntimePtr ::CreateCRuntime(pI_bool loadProperties);
pI_int ::GetpIAPIVersion(CRuntimePtr crt);

/*
 * This method is necessary when delivering Arguments out of Java.
 * Java must not have ownership after that.
 */
%pragma(java) modulecode=%{
  public static void setSwigOwnership(Argument arg, boolean val) {
	  arg.swigCMemOwn = val;
  }
%}

/*
 * Application adapter
 * =================
 */
%typemap(javaimports) pI::Application %{
import pI.Session;
%}

%typemap(javacode) pI::Application %{
  public void dispose() {
	freeArguments();
	freePlugins();
	delete();
  }
  
  protected void freePlugins() {
	Session session = Session.instance();
	while(session.hasPlugins()) {
		pIn p = session.popPlugin();
        if(p!=null) {
            p.delete();
        }
	}
  }  

  protected void freeArguments() {
	Session session = Session.instance();
	while(session.hasArguments()) {
		Argument a = session.popArgument();
        if(a!=null) {
            a.delete();
        }
	}
  }  
%}
%include "Application.i"
