/* ManagedPlugin.i
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

%typemap(javaimports) pI::pIn %{
import pI.Session;
%}

%typemap(javabody) pI::pIn %{
  private long swigCPtr;
  protected boolean swigCMemOwn;

  public $javaclassname(long cPtr, boolean cMemoryOwn) {  
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
	
	// register this argument to the managed session.
	if(swigCMemOwn) {
		Session.instance().addPlugin(this);
	}
  }

  public static long getCPtr($javaclassname obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }
%}


%typemap(javafinalize) pI::pIn %{
  protected void finalize() {
	Session.instance().removePlugin(this);
	delete();
  }
%}

%typemap(javadestruct, methodname="delete", methodmodifiers="public synchronized") pI::pIn %{ {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        pIJNI.delete_pIn(swigCPtr);
      }
      swigCPtr = 0;
    }
} %}
