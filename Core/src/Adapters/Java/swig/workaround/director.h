/* director.h
 *
 * Copyright 2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SWIG_DIRECTOR_H
#define SWIG_DIRECTOR_H

#if defined(DEBUG_DIRECTOR_OWNED)
#include <iostream>
#endif

namespace Swig {

    class JObjectWrapper {
    public:
        JObjectWrapper();

        ~JObjectWrapper();

        bool set(JNIEnv *jenv, jobject jobj, bool mem_own, bool weak_global);

        jobject get(JNIEnv *jenv) const;

        void release(JNIEnv *jenv);

        jobject peek();

        /* Java proxy releases ownership of C++ object, C++ object is now
        responsible for destruction (creates NewGlobalRef to pin Java
        proxy) */
        void java_change_ownership(JNIEnv *jenv, jobject jself, bool take_or_release);

    private:
        /* pointer to Java object */
        jobject jthis_;
        /* Local or global reference flag */
        bool weak_global_;
    };

    class Director;

    /* Utility class for managing the JNI environment */
    class JNIEnvWrapper {
        const Director *director_;
        JNIEnv *jenv_;
    
    public:
      JNIEnvWrapper(const Director *director);

      ~JNIEnvWrapper();
      
      JNIEnv *getJNIEnv() const;
    };

    class Director {

        JavaVM *swig_jvm_;

        friend class JNIEnvWrapper;

    protected:

        /* Java object wrapper */
        JObjectWrapper swig_self_;
    
        /* Disconnect director from Java object */
        void swig_disconnect_director_self(const char *disconn_method);
    
      public:
        Director(JNIEnv *jenv);
    
        virtual ~Director();
    
        bool swig_set_self(JNIEnv *jenv, jobject jself, bool mem_own, bool weak_global);
    
        jobject swig_get_self(JNIEnv *jenv) const;
    
        // Change C++ object's ownership, relative to Java
        void swig_java_change_ownership(JNIEnv *jenv, jobject jself, bool take_or_release);

    }; // class Director

} // namespace Swig

#endif // SWIG_DIRECTOR_H
