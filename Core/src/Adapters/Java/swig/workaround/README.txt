These files represent a workaround that splits the definitions used by java swig director feature into a separate header file.
I need this to have access to the SwigDirector adapter class which enables bi-directional calls between java and c++.
