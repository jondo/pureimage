%{
#include <sstream>
%}

namespace std {
    class stringstream {
    };
}

%extend std::stringstream {
    const std::string toString() {
        return self->str();
    }
    
    void append(const std::string& s) {
        *self << s;
    }

    void append(unsigned int i) {
        *self << i;
    }

    void append(bool b) {
        *self << b;
    }
    
    void append(int i) {
        *self << i;
    }

    void append(double d) {
        *self << d;
    }

}
