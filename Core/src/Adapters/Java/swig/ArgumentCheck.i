/* ArgumentCheck.i
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This provides a nicer interface to the convenience methods for
 * checking argument and parameter signatures.
 * The reason for the great effort is, that it would be nice to use
 * built in types (List) to compose Arguments.
 * Another issue is debugging: A proxy data type is not possible to analyse directly.
 */
 
 /**
  * Note: another idea would be to provide interfaces with arrays.
  * Argument[] params = pIn1.GetParameterSignature();
  * Argument out = pIn1.Execute(arg1, arg2)[0];
  */
 
namespace pI {

// rename the original methods as they have ArgumentVector as argument types
// and are difficult to use
//%javamethodmodifiers CheckParameters "private";
//%rename (CheckParameters0) CheckParameters;

//%javamethodmodifiers CheckInputArguments "private";
//%rename (CheckInputArguments0) CheckInputArguments;

//%javamethodmodifiers CheckOutputArguments "private";
//%rename (CheckOutputArguments0) CheckOutputArguments;

}

/*
%pragma(java) modulecode=%{

  public static void CheckParameters(Argument[] theirs, Argument[] mine) {
        ArgumentVector _theirs = Arguments.toVector(theirs);
        ArgumentVector _mine = Arguments.toVector(mine);
        CheckParameters0(_theirs, _mine);
  }
  
  public static void CheckInputArguments(Argument[] theirs, Argument[] mine) {
        ArgumentVector _theirs = Arguments.toVector(theirs);
        ArgumentVector _mine = Arguments.toVector(mine);
        CheckInputArguments0(_theirs, _mine);
  }

  public static void CheckOutputArguments(Argument[] theirs, Argument[] mine) {
        ArgumentVector _theirs = Arguments.toVector(theirs);
        ArgumentVector _mine = Arguments.toVector(mine);
        CheckOutputArguments0(_theirs, _mine);
  }
  
%}
*/