/* string_buffer.i
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
 
/*
 * This file establishes a mapping from arguments of type std::string& that are delivered as an output buffer
 * to java.lang.StringBuffer.
 */
 
%{
#include <string>
%}

namespace std {

%naturalvar string;

class string;


// string &
%typemap(jni) string & "jobject"
%typemap(jtype) string & "StringBuffer"
%typemap(jstype) string & "StringBuffer"
%typemap(javadirectorin) string & "$jniinput"
%typemap(javadirectorout) string & "$javacall"


%typemap(in) string &
%{ if(!$input) {
     SWIG_JavaThrowException(jenv, SWIG_JavaNullPointerException, "null std::string");
     return $null;
    } 
    $1 = new std::string();
%}

%typemap(argout) int *out {
   // TODO: Append output value $1 to $result
}


%typemap(freearg) string &
%{ if($1) {
   // TODO: append the string to the result stream
      $1; $input;
      delete $1;
    } 
%}

%typemap(directorout,warning=SWIGWARN_TYPEMAP_THREAD_UNSAFE_MSG) string &
%{ if(!$input) {
     SWIG_JavaThrowException(jenv, SWIG_JavaNullPointerException, "null std::string");
     return $null;
   }
   const char *$1_pstr = (const char *)jenv->GetStringUTFChars($input, 0); 
   if (!$1_pstr) return $null;
   /* possible thread/reentrant code problem */
   static std::string $1_str;
   $1_str = $1_pstr;
   $result = &$1_str;
   jenv->ReleaseStringUTFChars($input, $1_pstr); %}

%typemap(directorin,descriptor="Ljava/lang/String;") string &
%{ $input = jenv->NewStringUTF($1.c_str()); %}

%typemap(javain) string & "$javainput"

%typemap(javaout) string & {
    return $jnicall;
  }

%typemap(typecheck) string & = char *;

%typemap(throws) string &
%{ SWIG_JavaThrowException(jenv, SWIG_JavaRuntimeException, $1.c_str());
   return $null; %}


}
