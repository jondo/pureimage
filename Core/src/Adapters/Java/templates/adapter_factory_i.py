from template_engine import *
from license_template import *

## Templates: #########################################################################################################

factory_template = Template(
"""
$HEADER

%pragma(java) modulecode=%{

/**
 * Performs a downcast to a specialized ArgumentAdapter. 
 */
public static Argument adaptArgument(SWIGTYPE_p_Argument _c_ptr, boolean own) {
  // create a weak local argument adapter for retrieving type info
  long c_ptr = SWIGTYPE_p_Argument.getCPtr(_c_ptr);
  Argument arg = new Argument(pIJNI.new_Argument__SWIG_2(c_ptr, false), false);
  switch(arg.GetType()) {
      $CASES
	  default:
	   throw new IllegalArgumentException();
  }
}
%}
""")

case_stmt = Template(
"""
	  case T_${arg.c_type}:
	    return new ${arg.name}(pIJNI.new_${arg.name}__SWIG_3(c_ptr, own),own);
""")

## Template processing logic: #########################################################################################

def process(vars):

    HEADER = StringBuffer()
    HEADER << cpp_block_comment(unicode(license_text, errors='ignore')%("AdapterFactory.i")) << ENDL
    HEADER << cpp_block_comment(auto_generation_warning%(__file__)) << ENDL

    CASES = StringBuffer().println()
    
    # create a Matlab proxy class for each argument specification 
    for arg in vars.__root__.arguments:
        CASES << expand_template(case_stmt)

    with open_file(vars.__outdir__ + "/AdapterFactory.i") as f:
        f.write(expand_template(factory_template))
