/* Runtime.c
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef __cplusplus
extern "C" {
#endif

#define PI_BUILD_RUNTIME

#include <malloc.h>
#include <string.h>
#include <stdio.h>

#include <PureImage.h>

#include <Runtime/Internal/RuntimeHelperFunctions.h>

    /* Constraint parser includes */
#include <Runtime/Internal/ConstraintParser/SymbolStack.h>
#include <Runtime/Internal/ConstraintParser/PicParser.h>
#include <Runtime/Internal/ConstraintParser/StackEval.h>

    /* Type includes */
#include <Runtime/Internal/DataTypes/Intrinsics/IntrinsicRuntimeFunctions.h>
#include <Runtime/Internal/DataTypes/Intrinsics/IntrinsicSizes.h>
#include <Runtime/Internal/DataTypes/Structures/StructureSizes.h>
#include <Runtime/Internal/DataTypes/Arguments/ArgumentSizes.h>

    /* S11N includes */
#include <Runtime/Internal/S11N/JSON/ArgumentSerializationJSON.h>


#ifndef __GNUC__
#define STRDUP(s) _strdup(s)
#define STRNCPY(out, outlen, src, srclen) strncpy_s(out, outlen, src, srclen)
#else
#define STRDUP(s) strdup(s)
#define STRNCPY(out, outlen, src, srclen) strncpy(out, src, srclen)
#endif

    /* constants */
    pI_int c_data_ptr_size = sizeof(pI_byte* );
    pI_str c_properties_file_name = "pI_properties.json";

    /* macros */
#define RUNTIME_SET_ERROR(RUNTIME,ERROR) \
    do { \
        RUNTIME->last_error = ERROR; \
    } while (0)

    /** Calls free on given pointer and sets it to zero. */
#define RUNTIME_SAFE_FREE(RUNTIME, X) \
    do { \
        RUNTIME->FreeMemory (RUNTIME, X); \
        X = 0; \
    } while (0)

    /** Calls strdup only if in argment is valid. */
#define RUNTIME_SAFE_CSTR_DUP(out, in) \
    do { \
        if (in != 0) \
            out = (pI_str) STRDUP ((char* ) in); \
        else \
            out = 0; \
    } while (0)

    /* ------ local helper function fwd. decls */
    enum _DataType GetArgumentDataTypeBySymbol (
        struct _CRuntime* runtime,
        Argument* arg,
        pI_str symbol,
        pI_bool is_structure_symbol);

    pI_bool CRuntime_FreeArgument (
        struct _CRuntime* runtime,
        struct _Argument* argument,
        pI_bool free_data);

    void CRuntime_ResetArgument (
        struct _CRuntime* runtime,
        struct _Argument* argument);

    pI_bool CRuntime_CreateFlatStructure (
        struct _CRuntime* runtime,
        pI_byte* data_ptr,
        pI_int symbol_sizes[],
        pI_char** * symbols,
        pI_int index,
        pI_int field_index);

    pI_bool CRuntime_FreeFlatStructure (
        struct _CRuntime* runtime,
        pI_byte* data_ptr,
        pI_int symbol_sizes[],
        pI_char** * symbols,
        pI_int index,
        pI_int field_index);

    pI_bool CRuntime_CopyFlatStructure (
        struct _CRuntime* runtime,
        pI_byte* src_data_ptr,
        pI_byte* dst_data_ptr,
        pI_int symbol_sizes[],
        pI_char** * symbols,
        pI_int index,
        pI_int field_index);

    pI_bool CRuntime_RecursiveArgumentDataFree (
        struct _CRuntime* runtime,
        pI_int dimensions,
        pI_int max_dimensions,
        pI_byte* data_ptr,
        pIDataTypeClass type_class,
        pI_int type_index,
        enum _pI_ArgumentType arg_type,
        pI_int index,
        struct _Argument* argument,
        pI_int* data_dimension,
        pI_int* data_step);

    pI_bool CRuntime_RecursiveArgumentDataAllocate (
        struct _CRuntime* runtime,
        pI_int dimensions,
        pI_int max_dimensions,
        pI_byte* data_ptr,
        pI_byte* data_block_ptr,
        pIDataTypeClass type_class,
        pI_int type_index,
        enum _pI_ArgumentType arg_type,
        pI_int index,
        struct _Argument* argument,
        pI_int* data_dimension,
        pI_int* data_step);

    pI_bool CRuntime_RecursiveArgumentDataCopy (
        struct _CRuntime* runtime,
        pI_int dimensions,
        pI_int max_dimensions,
        pI_byte* in_data_ptr,
        pI_byte* out_data_ptr,
        pI_byte* out_data_block_ptr,
        pIDataTypeClass type_class,
        pI_int type_index,
        enum _pI_ArgumentType arg_type,
        pI_int index,
        struct _Argument* argument,
        pI_int* data_dimension,
        pI_int* data_step);

    enum _pIDataTypeClass CRuntime_GetDataTypeClass (
        struct _CRuntime* runtime,
        pI_str data_type_name,
        pI_int* index);

    struct _Argument* CRuntime_CopyArgument (
        struct _CRuntime* runtime,
        struct _Argument* argument,
        pI_bool copy_data);

    enum _pI_IntrinsicType CRuntime_IntrinsicNameToType (pI_char* name);

    enum _pI_StructureType CRuntime_StructureNameToType (pI_char* name);

    pI_byte* CRuntime_FetchArgumentSetupStructureSymbol (
        struct _CRuntime* runtime,
        pI_byte* data,
        enum _pI_StructureType struct_type,
        pI_str symbol,
        enum _pIDataTypeClass* kind,
        pI_int* index);

    pI_bool CRuntime_HasProperty (
        struct _CRuntime* runtime,
        pI_str name);

    struct _Argument* CRuntime_GetProperty (
        struct _CRuntime* runtime,
        pI_str name,
        pI_bool get_copy);

    pI_bool CRuntime_StoreProperty (
        struct _CRuntime* runtime,
        struct _Argument* prop,
        pI_bool take_ownership);

    pI_bool CRuntime_RemoveProperty (
        struct _CRuntime* runtime,
        pI_str name);

    void CRuntime_ClearProperties (
        struct _CRuntime* runtime,
        pI_bool free_memory);

    pI_str CRuntime_SerializeProperties (
        struct _CRuntime* runtime);

    pI_bool CRuntime_DeserializeProperties (
        struct _CRuntime* runtime,
        pI_str sprops);


    void FetchArgumentDataCountPerDimension (
        struct _CRuntime* runtime,
        struct _Argument* argument,
        pI_int field_index,
        pI_int* retval,
        pI_int max_depth);

    void CalculateArgumentDataStepPerDimension (
        pI_int* dimensions,
        pI_int* retval,
        pI_int elem_size,
        pI_int max_depth,
        pI_int padding_dim);

    pI_int GetTopLevelElemSize (
        enum _pIDataTypeClass type_class,
        pI_int type_index,
        pI_int data_dimensions);

    /* Note: the following runtime implementation can be enhanced in multiple ways -
       the registered plugins could be sorted, a memory pool for memory allocation could
       be used, .... */

    static pI_int CRuntime_GetRuntimeVersion()
    {
        return PI_API_VERSION;
    } /* static pI_int CRuntime_GetRuntimeVersion() */

    /* ------ initialization and destruction */

    static void CRuntime_Initialize (struct _CRuntime* runtime)
    {

        if (runtime != 0) {
            runtime->plugin_count = 0;
            runtime->plugins = 0;
            RUNTIME_SET_ERROR (runtime, pI_CRuntime_Error_NoError);
            runtime->last_constraint_error = pI_Constraint_Error_NoError;
            runtime->last_constraint_error_msg = 0;
            runtime->property_map = 0;
            runtime->property_count = 0;
            runtime->property_capacity = 0;
			runtime->shared_library_data = 0;
        } /* if (runtime != 0) */
    } /* static void CRuntime_Initialize (struct _CRuntime* runtime) */

    static void CRuntime_Destroy (struct _CRuntime* runtime)
    {

        if (runtime != 0) {
            if (runtime->plugins != 0) {
                runtime->DestroyRegisteredPlugins (runtime);
            }

            /* destroy property map, free its memory */
            runtime->ClearProperties (runtime, pI_TRUE);
            runtime->FreeString (runtime, runtime->last_constraint_error_msg);
            free (runtime);
        } /* if (runtime != 0) */
    } /* static void CRuntime_Destroy (struct _CRuntime* runtime) */

    /* ------ plugin registration */

    static void CRuntime_DestroyRegisteredPlugins (struct _CRuntime* runtime)
    {

        pI_size lv;

        for (lv = 0; lv < runtime->plugin_count; ++lv) {
            runtime->DestroyPlugin (runtime, runtime->plugins[lv]);
        } /* for (lv = 0; lv < runtime->plugin_count; ++lv) */

        runtime->plugin_count = 0;
        RUNTIME_SAFE_FREE (runtime, runtime->plugins);
    } /* static void CRuntime_DestroyRegisteredPlugins (struct _CRuntime* runtime) */

    static pI_bool CRuntime_RegisterPlugin (struct _CRuntime* runtime, struct _CpIn* plugin)
    {

        struct _CpIn** plugins;
        pI_size lv;

        if ((runtime != 0) && (plugin != 0)) {
			/* check for correct runtime version */
			if (plugin->api_version != runtime->GetRuntimeVersion (runtime)) {
				/* runtime version mismatch */
				RUNTIME_SET_ERROR (runtime, pI_CRuntime_Error_RuntimeVersionMismatch);
				return pI_FALSE;
			}
            for (lv = 0; lv < runtime->plugin_count; ++lv) {
                if (strcmp (runtime->plugins[lv]->name, plugin->name) == 0) {
                    /* plugin already registered */
                    return pI_FALSE;
                }
            } /* for (lv = 0; lv < runtime->plugin_count; ++lv) */

            plugins = (struct _CpIn** ) runtime->AllocateMemory (runtime, (sizeof(struct _CpIn* ) * (runtime->plugin_count + 1)));

            if (plugins != 0) {
                for (lv = 0; lv < runtime->plugin_count; ++lv) {
                    plugins[lv] = runtime->plugins[lv];
                }

                plugins[runtime->plugin_count] = plugin;
                runtime->FreeMemory (runtime, runtime->plugins);
                runtime->plugins = plugins;
                ++runtime->plugin_count;
                return pI_TRUE;
            } /* if (plugins != 0) */

            /* memory allocation failed */
            RUNTIME_SET_ERROR (runtime, pI_CRuntime_Error_InsufficientMemory);
        } /* if ((runtime != 0) && (plugin != 0)) */

        return pI_FALSE;
    } /* static pI_bool CRuntime_RegisterPlugin (...) */

    static pI_bool CRuntime_UnregisterPlugin (struct _CRuntime* runtime, struct _CpIn* plugin)
    {

        pI_size lv;

        if ((runtime != 0) && (plugin != 0)) {
            for (lv = 0; lv < runtime->plugin_count; ++lv) {
                if (runtime->plugins[lv] == plugin) {
                    if (runtime->plugin_count > 1) {
                        runtime->plugins[lv] = runtime->plugins[runtime->plugin_count - 1];
                    }

                    --runtime->plugin_count;

                    if (runtime->plugin_count == 0) {
                        RUNTIME_SAFE_FREE (runtime, runtime->plugins);
                    }

                    return pI_TRUE;
                } /* if (runtime->plugins[lv] == plugin) */
            } /* for (lv = 0; lv < runtime->plugin_count; ++lv) */

            /* plugin lookup failed */
            RUNTIME_SET_ERROR (runtime, pI_CRuntime_Error_PluginLookupFailed);
        } /* if ((runtime != 0) && (plugin != 0)) */

        return pI_FALSE;
    } /* static pI_bool CRuntime_UnregisterPlugin (...) */

    static pI_bool CRuntime_UnregisterPluginByName (struct _CRuntime* runtime, pI_str name)
    {

        pI_size lv;

        if ((runtime != 0) && (name != 0)) {
            for (lv = 0; lv < runtime->plugin_count; ++lv) {
                if (strcmp (runtime->plugins[lv]->name, name) == 0) {
                    if (runtime->plugin_count > 1) {
                        runtime->plugins[lv] = runtime->plugins[runtime->plugin_count - 1];
                    }

                    --runtime->plugin_count;

                    if (runtime->plugin_count == 0) {
                        RUNTIME_SAFE_FREE (runtime, runtime->plugins);
                    }

                    return pI_TRUE;
                } /* if (strcmp (runtime->plugins[lv]->name, name) == 0) */
            } /* for (lv = 0; lv < runtime->plugin_count; ++lv) */

            /* plugin lookup failed */
            RUNTIME_SET_ERROR (runtime, pI_CRuntime_Error_PluginLookupFailed);
        } /* if ((runtime != 0) && (name != 0)) */

        return pI_FALSE;
    } /* static pI_bool CRuntime_UnregisterPluginByName (...) */

    static pI_bool CRuntime_HasPlugin (struct _CRuntime* runtime, pI_str name)
    {

        pI_size lv;

        if ((runtime != 0) && (name != 0)) {
            for (lv = 0; lv < runtime->plugin_count; ++lv) {
                if (strcmp (runtime->plugins[lv]->name, name) == 0) {
                    return pI_TRUE;
                }
            } /* for (lv = 0; lv < runtime->plugin_count; ++lv) */
        } /* if ((runtime != 0) && (name != 0)) */

        return pI_FALSE;
    } /* static pI_bool CRuntime_HasPlugin (...) */

    /* ------ plugin creation and deletion */

    static struct _CpIn* CRuntime_SpawnPlugin (struct _CRuntime* runtime, struct _CpIn* plugin) {

        if ((runtime != 0) && (plugin != 0)) {
            return plugin->Clone (plugin, pI_FALSE);
        }

        return 0;
    } /* static struct _CpIn* CRuntime_SpawnPlugin (...) */

    static struct _CpIn* CRuntime_SpawnPluginByName (struct _CRuntime* runtime, pI_str name) {

        /* implementation: "clone" runtime prototype plugin with given name */
        pI_size lv;

        if ((runtime != 0) && (name != 0)) {
            for (lv = 0; lv < runtime->plugin_count; ++lv) {
                if (strcmp (runtime->plugins[lv]->name, name) == 0) {
                    return runtime->SpawnPlugin (runtime, runtime->plugins[lv]);
                }
            } /* for (lv = 0; lv < runtime->plugin_count; ++lv) */

            /* plugin lookup failed */
            RUNTIME_SET_ERROR (runtime, pI_CRuntime_Error_PluginLookupFailed);
        } /* if ((runtime != 0) && (name != 0)) */

        return 0;
    } /* static struct _CpIn* CRuntime_SpawnPluginByName (...) */

    static struct _CpIn* CRuntime_ClonePlugin (
        struct _CRuntime* runtime,
        struct _CpIn* plugin) {

        /* clones a plugin with current state */
        if ((runtime != 0) && (plugin != 0)) {
            return plugin->Clone (plugin, pI_TRUE);
        }

        return 0;
    } /* static struct _CpIn* CRuntime_ClonePlugin (...) */

    static pI_bool CRuntime_DestroyPlugin (struct _CRuntime* runtime, struct _CpIn* plugin)
    {

        /* frees plugin's memory */
        if ((runtime != 0) && (plugin != 0)) {
            return plugin->Destroy (plugin);
        }

        return pI_FALSE;
    } /* static pI_bool CRuntime_DestroyPlugin (...) */

    /* ------ memory allocation and deletion */
    static void* CRuntime_AllocateMemory (
        struct _CRuntime* runtime,
        pI_size buffer_size)
    {

        void* retval;

        if ((runtime != 0) && (buffer_size > 0)) {
            retval = malloc (buffer_size);
#ifdef RUNTIME_LOG_ALLOCATION
            printf ("Allocated memory: %x, size: %d\n", (int) retval, buffer_size);
#endif

            if (retval != 0) {
                return retval;
            }

            /* memory allocation failed */
            RUNTIME_SET_ERROR (runtime, pI_CRuntime_Error_InsufficientMemory);
        } /* if ((runtime != 0) && (buffer_size > 0)) */

        return 0;
    } /* static  void* CRuntime_AllocateMemory (...) */

    static pI_bool CRuntime_FreeMemory (struct _CRuntime* runtime, void* buf)
    {

        if (buf != 0) {
#ifdef RUNTIME_LOG_ALLOCATION
            printf ("Freeing memory: %x\n", (int) buf);
#endif
            free (buf);
            return pI_TRUE;
        } /* if (buf != 0) */

        return pI_FALSE;
    } /* static pI_bool CRuntime_FreeMemory (...) */

    /* ------ string methods */

    static pI_str CRuntime_AllocateString (struct _CRuntime* runtime, pI_size buffer_size)
    {

        pI_str retval;

        if ((runtime != 0) && (buffer_size > 0)) {
            retval = (pI_str) runtime->AllocateMemory (runtime, sizeof(pI_char) * (buffer_size + 1)); /* add one extra for zero termination */

            if (retval != 0) {
                /* zero termination at right-most buffer position -> empty string */
                retval[0] = 0;
                /* zero termination at left-most buffer position */
                retval[buffer_size] = 0;
                return retval;
            }

            /* memory allocation failed */
            RUNTIME_SET_ERROR (runtime, pI_CRuntime_Error_InsufficientMemory);
        } /* if ((runtime != 0) && (buffer_size > 0)) */

        return 0;
    } /* static pI_str CRuntime_AllocateString (...) */

    static pI_str CRuntime_CopyString (struct _CRuntime* runtime, pI_str src)
    {

        pI_str out = 0;

        RUNTIME_SAFE_CSTR_DUP (out, src);

        if (out == 0) {
            /* memory allocation failed */
            RUNTIME_SET_ERROR (runtime, pI_CRuntime_Error_InsufficientMemory);
        }

        return out;
    } /* static pI_str CRuntime_AllocateSerializationBuffer (...) */

    static pI_str CRuntime_CopyStringN (struct _CRuntime* runtime, pI_str src, pI_size len)
    {

        pI_str out = 0;
        pI_size str_len;

        if ((runtime == 0) || (src == 0) || (len == 0)) {
            return 0;
        }

        str_len = strlen (src);

        if (len < str_len) {
            str_len = len;
        }

        out = (pI_str) runtime->AllocateMemory (runtime, sizeof(pI_char) * (str_len + 1));
        STRNCPY (out, str_len + 1, src, str_len);
        return out;
    } /* static pI_str CRuntime_CopyStringN (struct _CRuntime* runtime, pI_str src, pI_size len) */

    static pI_bool CRuntime_FreeString (struct _CRuntime* runtime, pI_str buffer)
    {

        if (buffer != 0) {
            runtime->FreeMemory (runtime, buffer);
            return pI_TRUE;
        }

        return pI_FALSE;
    } /* static pI_bool CRuntime_FreeSerializationBuffer (...) */


    /* ------ intrinsic methods */

    static pI_byte* CRuntime_CreateIntrinsic (
        struct _CRuntime* runtime,
        enum _pI_IntrinsicType type)
    {

        pI_byte* retval = 0;
        pI_bool success;

        if (runtime == 0) {
            return 0;
        }

        retval = runtime->AllocateMemory (runtime, pI_IntrinsicSizes[type]);

        if (retval != 0) {
            success = CRuntime_CreateIntrinsicDataType (runtime, retval, type);

            if (success != pI_TRUE) {
                runtime->FreeMemory (runtime, retval);
                retval = 0;
            }
        }

        return retval;
    } /* static pI_byte* CRuntime_CreateIntrinsic (...) */

    static pI_byte* CRuntime_CopyIntrinsic (
        struct _CRuntime* runtime,
        pI_byte* source,
        enum _pI_IntrinsicType type)
    {

        pI_byte* retval = 0;
        pI_bool success;

        if ((runtime == 0) || (source == 0)) {
            return 0;
        }

        retval = runtime->AllocateMemory (runtime, pI_IntrinsicSizes[type]);

        if (retval != 0) {
            success = CRuntime_CopyIntrinsicDataType (runtime, source, retval, type);

            if (success != pI_TRUE) {
                runtime->FreeMemory (runtime, retval);
                retval = 0;
            }
        }

        return retval;
    } /* static pI_byte* CRuntime_CopyIntrinsic (...) */

    static pI_bool CRuntime_CopyIntrinsicInplace (
        struct _CRuntime* runtime,
        pI_byte* source,
		pI_byte* dst,
        enum _pI_IntrinsicType type) {

		return CRuntime_CopyIntrinsicDataType (runtime, source, dst, type);
	} /* static pI_bool CRuntime_CopyIntrinsicInplace (...) */

    static pI_bool CRuntime_FreeIntrinsic (
        struct _CRuntime* runtime,
        pI_byte* intrinsic,
        enum _pI_IntrinsicType type)
    {

        if ((runtime == 0) || (intrinsic == 0)) {
            return pI_FALSE;
        }

        return CRuntime_FreeIntrinsicDataType (runtime, intrinsic, type);
    } /* static pI_bool CRuntime_FreeIntrinsic (...) */

    static pI_byte* CRuntime_DeserializeIntrinsic (
        struct _CRuntime* runtime,
        enum _pI_IntrinsicType type,
        pI_str args11n)
    {

        pI_byte* retval = 0;
        pI_bool success;

        if ((runtime == 0) || (args11n == 0)) {
            return 0;
        }

        retval = runtime->AllocateMemory (runtime, pI_IntrinsicSizes[type]);

        if (retval != 0) {
            success =
                CRuntime_DeserializeIntrinsicDataType (
                    runtime,
                    retval,
                    type,
                    args11n);

            if (success == pI_FALSE) {
                runtime->FreeMemory (runtime, retval);
                retval = 0;
            }
        }

        return retval;
    } /* static pI_byte* CRuntime_DeserializeIntrinsic (...) */

    /* ------ structure methods */

    static void* CRuntime_CreateStructure (
        struct _CRuntime* runtime,
        enum _pI_StructureType type)
    {

        void* retval = 0;
        pI_bool success;

        if (runtime == 0) {
            return 0;
        }

        retval = runtime->AllocateMemory (runtime, pI_StructureSizes[type]);

        if (retval != 0) {
            success =
                CRuntime_CreateFlatStructure (
                    runtime,
                    retval,
                    pI_StructureSymbolSizes,
                    pI_StructureSymbolTypes,
                    type,
                    -1);

            if (success != pI_TRUE) {
                runtime->FreeMemory (runtime, retval);
                retval = 0;
            }
        }

        return retval;
    } /* static void* CRuntime_CreateStructure (...) */

    static pI_bool CRuntime_FreeStructure (
        struct _CRuntime* runtime,
        void* structure,
        enum _pI_StructureType type)
    {

        if ((runtime == 0) || (structure == 0)) {
            return pI_FALSE;
        }

        return CRuntime_FreeFlatStructure (
                   runtime,
                   structure,
                   pI_StructureSymbolSizes,
                   pI_StructureSymbolTypes,
                   type,
                   -1);
    } /* static pI_bool CRuntime_FreeStructure (...) */

    static void* CRuntime_CopyStructure (
        struct _CRuntime* runtime,
        void* source,
        enum _pI_StructureType type)
    {

        void* retval = 0;
        pI_bool success;

        if ((runtime == 0) || (source == 0)) {
            return 0;
        }

        retval = runtime->AllocateMemory (runtime, pI_StructureSizes[type]);

        if (retval != 0) {
            success = CRuntime_CopyFlatStructure (
                          runtime,
                          source,
                          retval,
                          pI_StructureSymbolSizes,
                          pI_StructureSymbolTypes,
                          type,
                          -1);

            if (success != pI_TRUE) {
                runtime->FreeMemory (runtime, retval);
                retval = 0;
            }
        }

        return retval;
    } /* static void* CRuntime_CopyStructure (...) */

    /* ------ argument methods */

    static struct _Argument* CRuntime_AllocateArgument (
        struct _CRuntime* runtime) {

        struct _Argument* retval =
            (struct _Argument* ) runtime->AllocateMemory (runtime, sizeof(struct _Argument));

        if (retval == 0) {
            /* memory allocation failed */
            RUNTIME_SET_ERROR (runtime, pI_CRuntime_Error_InsufficientMemory);
            return 0;
        }

        /* argument struct must be properly initialized */
        CRuntime_ResetArgument (runtime, retval);
        return retval;
    } /* struct _Argument* CRuntime_AllocateArgument (...) */

    static struct _ArgumentList* CRuntime_AllocateArgumentList (
        struct _CRuntime* runtime,
        pI_size num_arguments) {

        struct _ArgumentList* retval =
            (struct _ArgumentList* ) runtime->AllocateMemory (runtime, sizeof(struct _ArgumentList));
        pI_size lv;

        if (retval != 0) {
            retval->count = num_arguments;

            if (retval->count > 0) {
                retval->list =
                    (struct _Argument** ) runtime->AllocateMemory (
                        runtime,
                        sizeof(struct _Argument* ) * retval->count);

                if (retval->list != 0) {
                    for (lv = 0; lv < num_arguments; ++lv) {
                        retval->list[lv] = 0;
                    } /* for (lv = 0; lv < num_arguments; ++lv) */
                } else { /* if (retval->list != 0) */
                    /* memory allocation failed */
                    RUNTIME_SET_ERROR (runtime, pI_CRuntime_Error_InsufficientMemory);
                    retval->list = 0;
                } /* else - if (retval->list != 0) */
            } else { /* if (retval->count > 0) */
                retval->list = 0;
            }
        } else { /* if (retval != 0) */
            /* memory allocation failed */
            RUNTIME_SET_ERROR (runtime, pI_CRuntime_Error_InsufficientMemory);
        } /* else - if (retval != 0) */

        return retval;
    } /* struct _ArgumentList* CRuntime_AllocateArgumentList (...) */

    static struct _Argument* CRuntime_CreateArgument (
        struct _CRuntime* runtime,
        enum _pI_ArgumentType type) {

        struct _Argument* arg = 0;

        if (runtime == 0) {
            return 0;
        }

        arg = runtime->AllocateArgument (runtime);

        if (arg != 0) {
            arg->signature.type = type;

            if (runtime->CreateArgumentSetup (runtime, arg) == pI_FALSE) {
                /* argument structure creation failed, so the
                   stub is deleted and 0 is returned. */
                runtime->FreeArgument (runtime, arg, pI_FALSE);
                return 0;
            }
        }

        return arg;
    } /* static struct _Argument* CRuntime_CreateArgument (...) */

    static pI_bool CRuntime_CreateArgumentData (
        struct _CRuntime* runtime,
        struct _Argument* argument)
    {

        pI_int** arg_data_dimensions = pI_ArgumentDataDimensions;
        pI_int* arg_data_symbol_sizes = pI_ArgumentDataSymbolSizes;
        enum _pI_ArgumentType arg_type;
        pI_byte* data;
        pI_int lv, dims, elem_size;
        pI_int type_index;
        enum _pIDataTypeClass type_class;
        int _arg_type;
        pI_int data_dims[5] = {0};
        pI_int data_steps[6] = {0};

        if ((runtime == 0) || (argument == 0)) {
            return pI_FALSE;
        }

        /* free argument data in case function is called twice
           on same argument or s.l.t. */
        runtime->FreeArgumentData (runtime, argument);

        arg_type = argument->signature.type;
        _arg_type = arg_type;

        /* allocate memory for data struct */
        data = runtime->AllocateMemory (runtime, pI_ArgumentDataStructSizes[arg_type]);

        if (data == 0) {
            return pI_FALSE;
        }

        /* zero out data struct */
        memset (data, 0, pI_ArgumentDataStructSizes[arg_type]);
        argument->data.Empty = (struct _pI_Argument_Empty_Data* ) data;

        /* iterate data fields */
        for (lv = 0; lv < arg_data_symbol_sizes[arg_type]; ++lv) {
            dims = arg_data_dimensions[arg_type][lv];
            type_class =
                CRuntime_GetDataTypeClass (
                    runtime,
                    pI_ArgumentDataSymbolTypes[arg_type][lv],
                    &type_index);
            elem_size = GetTopLevelElemSize (type_class, type_index, 0);

            FetchArgumentDataCountPerDimension (
                runtime,
                argument,
                lv,
                data_dims,
                5);

            CalculateArgumentDataStepPerDimension (
                data_dims,
                data_steps,
                elem_size,
                dims,
                pI_ArgumentDataPaddingDimensions[arg_type][lv]);

            if (CRuntime_RecursiveArgumentDataAllocate (
                        runtime,
                        dims,
                        dims,
                        data,
                        0,
                        type_class,
                        type_index,
                        arg_type,
                        lv,
                        argument,
                        data_dims,
                        data_steps) == pI_FALSE) {
                /* recursive allocation failed, clean up and return false */
                runtime->FreeArgumentData (runtime, argument);
                return pI_FALSE;
            }

            data += elem_size;

            /* enforce proper alignment */
            if ((elem_size % PI_STRUCTURE_ALIGNMENT) != 0) {
                data += PI_STRUCTURE_ALIGNMENT - elem_size % PI_STRUCTURE_ALIGNMENT;
            }
        } /* for (lv = 0; lv < arg_data_symbol_sizes[type]; ++lv) */

        return pI_TRUE;
    } /* pI_bool CRuntime_CreateArgumentData (...) */

    static pI_bool CRuntime_CreateArgumentSetup (
        struct _CRuntime* runtime,
        struct _Argument* argument)
    {

        pI_int* arg_setup_symbol_sizes = pI_ArgumentSetupSymbolSizes;
        pI_char** * arg_setup_symbol_types = pI_ArgumentSetupSymbolTypes;
        enum _pI_ArgumentType type;
        pI_byte* structure;

        if ((runtime == 0) || (argument == 0)) {
            return pI_FALSE;
        }

        /* free argument structure in case function is called twice
           on same argument or s.l.t. */
        runtime->FreeArgumentSetup (runtime, argument);
        type = argument->signature.type;

        /* in case argument does not have a structure, return with success */
        if (pI_ArgumentSetupStructSizes[type] == 0) {
            return pI_TRUE;
        }

        /* allocate argument Setup. */
        structure = (pI_byte* ) runtime->AllocateMemory (runtime, pI_ArgumentSetupStructSizes[type]);

        if (structure == 0) {
            return pI_FALSE;
        }

        /* zero out data struct */
        memset (structure, 0, pI_ArgumentSetupStructSizes[type]);
        argument->signature.setup.Empty = (struct _pI_Argument_Empty_Setup* ) structure;

        return CRuntime_CreateFlatStructure (
                   runtime,
                   structure,
                   arg_setup_symbol_sizes,
                   arg_setup_symbol_types,
                   type,
                   -1);
    } /* static pI_bool CRuntime_CreateArgumentSetup (...) */

    static pI_bool CRuntime_FreeArgumentData (
        struct _CRuntime* runtime,
        struct _Argument* argument)
    {

        pI_int** arg_data_dimensions = pI_ArgumentDataDimensions;
        pI_int* arg_data_symbol_sizes = pI_ArgumentDataSymbolSizes;
        enum _pI_ArgumentType arg_type;
        pI_byte* data;
        pI_int lv, dims;
        enum _pIDataTypeClass type_class;
        pI_int type_index, elem_size;
        pI_int data_dims[5] = {0};
        pI_int data_steps[6] = {0};

        if ((argument == 0) || (argument == 0)) {
            return pI_FALSE;
        }

        arg_type = argument->signature.type;
        data = (pI_byte* ) argument->data.Empty; /* since a pointer union is contained, this works safely. */

        if (data == 0) {
            /* argument data has not yet been allocated or already been freed.
               do nothing but return true, since argument is already deleted.*/
            return pI_TRUE;
        }

        /* iterate data fields */
        for (lv = 0; lv < arg_data_symbol_sizes[arg_type]; ++lv) {
            type_class = CRuntime_GetDataTypeClass (runtime, pI_ArgumentDataSymbolTypes[arg_type][lv], &type_index);

            FetchArgumentDataCountPerDimension (
                runtime,
                argument,
                lv,
                data_dims,
                5);

            dims = arg_data_dimensions[arg_type][lv];

            elem_size = GetTopLevelElemSize (type_class, type_index, 0);

            CalculateArgumentDataStepPerDimension (
                data_dims,
                data_steps,
                elem_size,
                dims,
                pI_ArgumentDataPaddingDimensions[arg_type][lv]);

            CRuntime_RecursiveArgumentDataFree (
                runtime,
                dims,
                dims,
                data,
                type_class,
                type_index,
                arg_type,
                lv,
                argument,
                data_dims,
                data_steps);

            data += elem_size;

            /* enforce proper alignment */
            if ((elem_size % PI_STRUCTURE_ALIGNMENT) != 0) {
                data += PI_STRUCTURE_ALIGNMENT - elem_size % PI_STRUCTURE_ALIGNMENT;
            }

        } /* for (lv = 0; lv < arg_data_symbol_sizes[type]; ++lv) */

        RUNTIME_SAFE_FREE (runtime, argument->data.Empty);
        argument->data.Empty = 0;
        return pI_TRUE;
    } /* pI_bool CRuntime_FreeArgumentData (...) */

    static pI_bool CRuntime_FreeArgumentSetup (
        struct _CRuntime* runtime,
        struct _Argument* argument)
    {

        pI_bool retval;

        if ((runtime == 0) || (argument == 0)) {
            return pI_FALSE;
        }

        if (argument->signature.setup.Empty == 0) {
            return pI_TRUE;
        }

        retval = CRuntime_FreeFlatStructure (
                     runtime,
                     (pI_byte* ) argument->signature.setup.Empty,
                     &pI_ArgumentSetupSymbolSizes[argument->signature.type],
                     &pI_ArgumentSetupSymbolTypes[argument->signature.type],
                     0,
                     -1);

        RUNTIME_SAFE_FREE (runtime, argument->signature.setup.Empty);
        return retval;
    } /* static pI_bool CRuntime_FreeArgumentSetup (...) */

    pI_bool CRuntime_FreeArgument (
        struct _CRuntime* runtime,
        struct _Argument* argument,
        pI_bool free_data)
    {

        pI_bool retval = pI_TRUE;

        if ((runtime == 0) || (argument == 0)) {
            return pI_FALSE;
        }

        if (free_data == pI_TRUE) {
            retval &= runtime->FreeArgumentData (runtime, argument);
        }

        retval &= runtime->FreeArgumentSetup (runtime, argument);
        retval &= runtime->FreeString (runtime, argument->signature.constraints);
        retval &= runtime->FreeString (runtime, argument->description.name);
        retval &= runtime->FreeString (runtime, argument->description.description);
        runtime->FreeMemory (runtime, argument);

        return retval;
    } /* void CRuntime_FreeArgument (...) */

    static pI_bool CRuntime_FreeArgumentList (
        struct _CRuntime* runtime,
        struct _ArgumentList* argument_list,
        pI_bool free_arguments,
        pI_bool free_argument_data)
    {

        pI_bool retval = pI_FALSE;
        pI_size lv;

        if ((runtime != 0) && (argument_list != 0)) {
            retval = pI_TRUE;

            if (free_arguments == pI_TRUE) {
                for (lv = 0; lv < argument_list->count; ++lv) {
                    retval &= runtime->FreeArgument (runtime, argument_list->list[lv], free_argument_data);
                }
            } /* if (free_arguments == pI_TRUE) */

            RUNTIME_SAFE_FREE (runtime, argument_list->list);
            RUNTIME_SAFE_FREE (runtime, argument_list);
        } /* if ((runtime != 0) && (argument_list != 0)) */

        return retval;
    } /* static void CRuntime_FreeArgumentList (...) */

    struct _Argument* CRuntime_CopyArgument (
        struct _CRuntime* runtime,
        struct _Argument* argument,
        pI_bool copy_data) {

        pI_int* arg_setup_symbol_sizes = pI_ArgumentSetupSymbolSizes;
        pI_char** * arg_setup_symbol_types = pI_ArgumentSetupSymbolTypes;
        pI_int** arg_data_dimensions = pI_ArgumentDataDimensions;
        pI_int* arg_data_symbol_sizes = pI_ArgumentDataSymbolSizes;
        pI_char** * arg_data_symbols = pI_ArgumentDataSymbolTypes;
        struct _Argument* copy;
        pI_byte* in_data;
        pI_byte* out_data;
        pI_int lv, dims;
        enum _pI_ArgumentType type;
        enum _pIDataTypeClass type_class;
        pI_int type_index, elem_size;
        pI_int data_dims[5] = {0};
        pI_int data_steps[6] = {0};
        pI_int temp;

        if ((runtime == 0) || (argument == 0)) {
            return 0;
        }

        type = argument->signature.type;
        temp = (pI_int) type;

        /* 1. create empty argument */
        copy = runtime->AllocateArgument (runtime);

        if (copy == 0) {
            return 0;
        }

        /* 2. copy meta information */
        copy->signature.type = type;
        copy->signature.readonly = argument->signature.readonly;
        copy->signature.constraints = runtime->CopyString (runtime, argument->signature.constraints);
        copy->description.name = runtime->CopyString (runtime, argument->description.name);
        copy->description.description = runtime->CopyString (runtime, argument->description.description);
        copy->runtime = runtime;

        /* zero out setup and data fields */
        copy->signature.setup.Empty = 0;
        copy->data.Empty = 0;

        /* 3. copy setup information */
        if (runtime->CreateArgumentSetup (runtime, copy) == pI_FALSE) {
            runtime->FreeArgument (runtime, copy, pI_FALSE);
            return 0;
        }

        CRuntime_CopyFlatStructure (
            runtime,
            (pI_byte* ) argument->signature.setup.Empty,
            (pI_byte* ) copy->signature.setup.Empty,
            arg_setup_symbol_sizes,
            arg_setup_symbol_types,
            type,
            -1);

        /* 4. copy argument data */
        if ((copy_data == pI_TRUE) /*&& (argument->signature.setup.Empty != 0)*/) {

            /* check if given argument has valid data */
            in_data = (pI_byte* ) argument->data.Empty; /* since a pointer union is contained, this works safely. */

            if (in_data == 0) {
                return copy;    /* if no data available, return argument copy as is */
            }

            /* 4.1 allocate data struct */
            /* argument data has not yet been allocated. */
            out_data = (pI_byte* ) runtime->AllocateMemory (runtime, pI_ArgumentDataStructSizes[type]);

            if (out_data == 0) {
                /* data creation failed, deallocate and return with failure. */
                runtime->FreeArgument (runtime, copy, pI_TRUE);
                return pI_FALSE;
            }

            /* zero out data struct */
            memset (out_data, 0, pI_ArgumentDataStructSizes[type]);
            copy->data.Empty = (struct _pI_Argument_Empty_Data* ) out_data;

            /* 4.2 recursively fill data structures */
            for (lv = 0; lv < arg_data_symbol_sizes[type]; ++lv) {
                /* fetch type of actual member */
                dims = arg_data_dimensions[type][lv];
                type_class =
                    CRuntime_GetDataTypeClass (
                        runtime,
                        pI_ArgumentDataSymbolTypes[type][lv],
                        &type_index);

                FetchArgumentDataCountPerDimension (
                    runtime,
                    argument,
                    lv,
                    data_dims,
                    5);

                elem_size = GetTopLevelElemSize (type_class, type_index, 0);

                CalculateArgumentDataStepPerDimension (
                    data_dims,
                    data_steps,
                    elem_size,
                    dims,
                    pI_ArgumentDataPaddingDimensions[type][lv]);

                CRuntime_RecursiveArgumentDataCopy (
                    runtime,
                    dims,
                    dims,
                    in_data,
                    out_data,
                    0,
                    type_class,
                    type_index,
                    type,
                    lv,
                    argument,
                    data_dims,
                    data_steps);

                in_data += elem_size;
                out_data += elem_size;

                /* enforce proper alignment */
                if ((elem_size % PI_STRUCTURE_ALIGNMENT) != 0) {
                    in_data += PI_STRUCTURE_ALIGNMENT - elem_size % PI_STRUCTURE_ALIGNMENT;
                    out_data += PI_STRUCTURE_ALIGNMENT - elem_size % PI_STRUCTURE_ALIGNMENT;
                }

            } /* for (lv = 0; lv < arg_data_symbol_sizes[type]; ++lv) */
        } /* if ((copy_data == pI_TRUE) && (argument->signature.setup.Empty != 0)) */

        return copy;
    } /* static struct _Argument* CRuntime_CopyArgument (...) */

    static struct _ArgumentList* CRuntime_CopyArgumentList (
        struct _CRuntime* runtime,
        struct _ArgumentList* list,
        pI_bool copy_data) {

        struct _ArgumentList* retval = 0;
        pI_size lv;

        if ((list != 0) && (runtime != 0)) {
            retval = (struct _ArgumentList* ) runtime->AllocateMemory (runtime, sizeof(struct _ArgumentList));

            if (retval != 0) {
                retval->count = list->count;
                retval->list =
                    (struct _Argument** ) runtime->AllocateMemory (runtime, sizeof(struct _Argument* ) * list->count);

                if (retval->list != 0) {
                    for (lv = 0; lv < list->count; ++lv) {
                        retval->list[lv] = runtime->CopyArgument (runtime, list->list[lv], copy_data);

                        if (retval->list[lv] == 0) {
                            /* memory allocation failed */
                            RUNTIME_SET_ERROR (runtime, pI_CRuntime_Error_InsufficientMemory);
                        }
                    } /* for (lv = 0; lv < list->count; ++lv) */
                } else {
                    /* memory allocation failed */
                    RUNTIME_SET_ERROR (runtime, pI_CRuntime_Error_InsufficientMemory);
                }
            } else { /* if (retval != 0) */
                /* memory allocation failed */
                RUNTIME_SET_ERROR (runtime, pI_CRuntime_Error_InsufficientMemory);
            } /* else - if (retval != 0) */
        } /* if ((list != 0) && (runtime != 0)) */

        return retval;
    } /* static struct _ArgumentList* CRuntime_CopyArgumentList (...) */

    static pI_bool CRuntime_SwapArguments (
        struct _CRuntime* runtime,
        struct _Argument* arg1,
        struct _Argument* arg2)
    {

        struct _Argument temp;

        if ((runtime == 0) || (arg1 == 0) || (arg2 == 0)) {
            return pI_FALSE;
        }

        /* simple, yet efficient memcpy-based implementation */
        memcpy (&temp, arg1, sizeof(struct _Argument));
        memcpy (arg1, arg2, sizeof(struct _Argument));
        memcpy (arg2, &temp, sizeof(struct _Argument));
        return pI_TRUE;
    } /* static pI_bool CRuntime_SwapArguments (...) */

    static pI_int CRuntime_CheckArgumentConstraints (
        struct _CRuntime* runtime,
        struct _Argument* arg,
        pI_str constraints)
    {

        struct _pI_CP_SymbolStack symbol_stack;
        struct _pI_CP_ParserArg parser_arg;
        pI_int res, parse_err;

        if ((runtime == 0) || (arg == 0)) {
            return pI_ERROR;
        }

        /* in case of unset constraints, return true */
        if (constraints == 0) {
            return pI_TRUE;
        }

        pI_CP_InitStack (&symbol_stack);
        /* parse constraint string, transform it into symbol stack */
        yy_set_string (constraints);
        parser_arg.runtime = runtime;
        parser_arg.symbol_stack = &symbol_stack;
        parse_err = yyparse (&parser_arg);

        if (parse_err != 0) {
            pI_CP_FreeSymbolStack (runtime, &symbol_stack);
            return pI_ERROR;
        }

        res = pI_CP_evaluate_symbol_stack (&symbol_stack, arg);
        pI_CP_FreeSymbolStack (runtime, &symbol_stack);

        if (pI_CP_EvalStack_getLastError() != pI_Constraint_Error_NoError) {
            runtime->last_constraint_error = pI_CP_EvalStack_getLastError();
            runtime->FreeString (runtime, runtime->last_constraint_error_msg);
            runtime->last_constraint_error_msg =
                runtime->CopyString (runtime, pI_CP_EvalStack_getLastErrorMsg());
            return pI_ERROR;
        }

        return res;
    } /* static pI_bool CRuntime_CheckArgumentConstraints (...) */


    /* ------ helper methods */

    pI_byte* CRuntime_FetchArgumentSetupSymbol (
        struct _CRuntime* runtime,
        struct _Argument* arg,
        pI_str symbol,
        enum _pIDataTypeClass* kind,
        pI_int* index)
    {

        enum _pI_ArgumentType arg_type;
        pI_int lv;
        pI_bool found = pI_FALSE;
        pI_str type_string = 0;
        pI_int dt_index, type_index, type_size;
        enum _pIDataTypeClass dt_class;
        pI_byte* retval;
        pI_str subsymbol, temp = 0;
        pI_char c_url_separator_char = '.';
        pI_str c_url_separator_str = ".";

        if ((runtime == 0) || (arg == 0) || (symbol == 0)) {
            return 0;
        }

        subsymbol = strstr (symbol, c_url_separator_str);

        if (subsymbol != 0) {
            temp = runtime->CopyStringN (runtime, symbol, subsymbol - symbol);
        } else {
            temp = runtime->CopyString (runtime, symbol);
        }

        arg_type = arg->signature.type;
        retval = &(*((pI_byte* ) arg->signature.setup.Empty));

        for (lv = 0; lv < pI_ArgumentSetupSymbolSizes[arg_type]; ++lv) {
            if (strcmp (temp, pI_ArgumentSetupSymbols[arg_type][lv]) == 0) {
                found = pI_TRUE;
                break;
            }

            /* fetch type of actual member */
            dt_class = CRuntime_GetDataTypeClass (runtime, pI_ArgumentSetupSymbolTypes[arg->signature.type][lv], &type_index);

            /* create the object in place, depending on type */
            if (dt_class == INTRINSIC_CLASS) {
                type_size = pI_IntrinsicSizes[type_index];
            } else if (dt_class == ARGUMENT_CLASS) {
                type_size = sizeof(struct _Argument);
            } else if (dt_class == STRUCTURE_CLASS) {
                type_size = pI_StructureSizes[type_index];
            } else {
                runtime->FreeString (runtime, temp);
                return 0; /* should never happen */
            }

            retval += type_size;

            /* enforce proper alignment */
            if ((type_size % PI_STRUCTURE_ALIGNMENT) != 0) {
                retval += PI_STRUCTURE_ALIGNMENT - type_size % PI_STRUCTURE_ALIGNMENT;
            }
        } /* for (lv = 0; lv < pI_ArgumentSetupSymbolSizes[arg_type]; ++lv) */

        runtime->FreeString (runtime, temp);

        if (found == pI_FALSE) {
            return 0;
        }

        type_string = pI_ArgumentSetupSymbolTypes[arg_type][lv];
        dt_class = CRuntime_GetDataTypeClass (runtime, type_string, &dt_index);

        if (kind != 0) {
            *kind = dt_class;
        }

        if (index != 0) {
            *index = dt_index;
        }

        switch (dt_class) {
            case INTRINSIC_CLASS: {
                if (subsymbol != 0) {
                    /* this should NEVER happen */
                }

                return retval;
            } /* case INTRINSIC_CLASS: */
            case ARGUMENT_CLASS: {
                if (subsymbol == 0) {
                    return retval;
                }

                return CRuntime_FetchArgumentSetupSymbol (runtime, arg, subsymbol + 1, kind, index);
            }
            case STRUCTURE_CLASS: {
                if (subsymbol == 0) {
                    return retval;
                }

                *index = pI_StructureSymbolSizes[dt_index];
                return CRuntime_FetchArgumentSetupStructureSymbol (runtime, retval, dt_index, subsymbol + 1, kind, index);
            }
        } /* switch (dt_class) */

        return 0;
    } /* pI_byte* CRuntime_FetchArgumentSetupSymbol (...) */

    enum _pIDataTypeClass CRuntime_GetDataTypeClass (
        struct _CRuntime* runtime,
        pI_str data_type_name,
        pI_int* index)
    {

        pI_size lv;
        pI_size intrinsic_count = sizeof(pI_IntrinsicNames) / sizeof(*pI_IntrinsicNames);
        pI_size structure_count = sizeof(pI_StructureNames) / sizeof(*pI_StructureNames);

        if ((runtime == 0) || (data_type_name == 0)) {
            return UNKNOWN_CLASS;
        }

        /* check if the data type to query is an intrinsic */
        for (lv = 0; lv < intrinsic_count; ++lv) {
            if (strcmp (data_type_name, pI_IntrinsicNames[lv]) == 0) {
                if (index != 0) {
                    *index = lv;
                }

                return INTRINSIC_CLASS;
            }
        }

        /* check if the data type to query is a structure */
        for (lv = 0; lv < structure_count; ++lv) {
            if (strcmp (data_type_name, pI_StructureNames[lv]) == 0) {
                if (index != 0) {
                    *index = lv;
                }

                return STRUCTURE_CLASS;
            }
        }

        /* check if the data type to query is an argument */
        if ((strcmp (data_type_name, "Argument") == 0) ||
                (strcmp (data_type_name, "struct _Argument") == 0)) {
            if (index != 0) {
                *index = 0;
            }

            return ARGUMENT_CLASS;
        }

        /* name was not found, return unknown class */
        if (index != 0) {
            *index = -1;
        }

        return UNKNOWN_CLASS;
    } /* static enum _pIDataTypeClass CRuntime_GetDataTypeClass (...) */

    /* ------ export methods */

    /*extern*/ struct _CRuntime* CreateCRuntime(pI_bool load_property_file) {

        struct _CRuntime* runtime = (struct _CRuntime* ) malloc (sizeof(struct _CRuntime));
        FILE* pfile = 0;
        pI_size file_len;
        pI_str text_file, tf_ptr;

        if (runtime != 0 ) {
            /* apply proper functions to struct */
            runtime->GetRuntimeVersion =            CRuntime_GetRuntimeVersion;
            runtime->Initialize =                   CRuntime_Initialize;
            runtime->Destroy =                      CRuntime_Destroy;
            runtime->DestroyRegisteredPlugins =     CRuntime_DestroyRegisteredPlugins;
            runtime->RegisterPlugin =               CRuntime_RegisterPlugin;
            runtime->UnregisterPlugin =             CRuntime_UnregisterPlugin;
            runtime->UnregisterPluginByName =       CRuntime_UnregisterPluginByName;
            runtime->HasPlugin =                    CRuntime_HasPlugin;
            runtime->SpawnPlugin =                  CRuntime_SpawnPlugin;
            runtime->SpawnPluginByName =            CRuntime_SpawnPluginByName;
            runtime->ClonePlugin =                  CRuntime_ClonePlugin;
            runtime->DestroyPlugin =                CRuntime_DestroyPlugin;
            runtime->AllocateMemory =               CRuntime_AllocateMemory;
            runtime->FreeMemory =                   CRuntime_FreeMemory;
            runtime->AllocateString =               CRuntime_AllocateString;
            runtime->CopyString =                   CRuntime_CopyString;
            runtime->CopyStringN =                  CRuntime_CopyStringN;
            runtime->FreeString =                   CRuntime_FreeString;

            runtime->CreateIntrinsic =              CRuntime_CreateIntrinsic;
            runtime->CopyIntrinsic =                CRuntime_CopyIntrinsic;
			runtime->CopyIntrinsicInplace =         CRuntime_CopyIntrinsicInplace;
            runtime->FreeIntrinsic =                CRuntime_FreeIntrinsic;
            runtime->SerializeIntrinsic =           CRuntime_SerializeIntrinsicDataType;
            runtime->DeserializeIntrinsic =         CRuntime_DeserializeIntrinsic;

            runtime->CreateStructure =              CRuntime_CreateStructure;
            runtime->FreeStructure =                CRuntime_FreeStructure;
            runtime->CopyStructure =                CRuntime_CopyStructure;

            runtime->AllocateArgument =             CRuntime_AllocateArgument;
            runtime->AllocateArgumentList =         CRuntime_AllocateArgumentList;
            runtime->CreateArgument =               CRuntime_CreateArgument;
            runtime->CreateArgumentData =           CRuntime_CreateArgumentData;
            runtime->CreateArgumentSetup =          CRuntime_CreateArgumentSetup;
            runtime->FreeArgumentData =             CRuntime_FreeArgumentData;
            runtime->FreeArgumentSetup =            CRuntime_FreeArgumentSetup;
            runtime->FreeArgument =                 CRuntime_FreeArgument;
            runtime->FreeArgumentList =             CRuntime_FreeArgumentList;
            runtime->CopyArgument =                 CRuntime_CopyArgument;
            runtime->CopyArgumentList =             CRuntime_CopyArgumentList;
            runtime->SwapArguments =                CRuntime_SwapArguments;
            runtime->CheckArgumentConstraints =     CRuntime_CheckArgumentConstraints;

            runtime->SerializeArgument =            pI_JSON_SerializeArgument;
            runtime->DeserializeArgument =          pI_JSON_DeserializeArgument;
            runtime->SerializeArguments =           pI_JSON_SerializeArgumentList;
            runtime->DeserializeArguments =         pI_JSON_DeserializeArgumentList;

            runtime->HasProperty =                  CRuntime_HasProperty;
            runtime->GetProperty =                  CRuntime_GetProperty;
            runtime->StoreProperty =                CRuntime_StoreProperty;
            runtime->RemoveProperty =               CRuntime_RemoveProperty;
            runtime->ClearProperties =              CRuntime_ClearProperties;
            runtime->SerializeProperties =          CRuntime_SerializeProperties;
            runtime->DeserializeProperties =        CRuntime_DeserializeProperties;

            /* initialize runtime */
            runtime->Initialize (runtime);

            if (load_property_file == pI_TRUE) {
                /* check if property file exists, and if so, load it from hdd */
#if _MSC_VER >= 1400
                fopen_s (&pfile, c_properties_file_name, "r");
#else /* #if _MSC_VER >= 1400 */
                pfile = fopen (c_properties_file_name, "r");
#endif /* #if _MSC_VER >= 1400 */

                if (pfile != 0) {
                    /* retrieve file len and set up buffer accordingly */
                    fseek (pfile, 0L, SEEK_END);
                    file_len = ftell (pfile);
                    fseek (pfile, 0L, SEEK_SET);
                    text_file = runtime->AllocateString (runtime, file_len);

                    if (text_file == 0) {
                        /* memory allocation failed */
                        RUNTIME_SET_ERROR (runtime, pI_CRuntime_Error_InsufficientMemory);
                    } else {
                        /* read file char by char */
                        tf_ptr = text_file;

                        while ((*tf_ptr = fgetc (pfile)) != EOF) {
                            ++tf_ptr;
                        }

                        *tf_ptr = '\0';

                        /* deserialize text file */
                        if (runtime->DeserializeProperties (runtime, text_file) != pI_TRUE) {
                            RUNTIME_SET_ERROR (runtime, pI_CRuntime_Error_Deserialization);
                        }

                        /* deallocate text file memory */
                        runtime->FreeString (runtime, text_file);
                    }

                    fclose (pfile);
                } /* if (pfile != 0) */
            } /* if (load_property_file == pI_TRUE) */
        } /* if (runtime != 0) */

        return runtime;
    } /* struct _CRuntime* CreateCRuntime() */

    /*extern*/  void DestroyCRuntime (struct _CRuntime* runtime, pI_bool write_property_file)
    {

        FILE* pfile = 0;
        pI_str properties = 0;

        if (runtime != 0) {
            /* write property file if desired */
            if (write_property_file == pI_TRUE) {
#if _MSC_VER >= 1400
                fopen_s (&pfile, c_properties_file_name, "w+"); /* if file exists, overwrite it */
#else /* #if _MSC_VER >= 1400 */
                pfile = fopen (c_properties_file_name, "w+"); /* if file exists, overwrite it */
#endif /* #if _MSC_VER >= 1400 */

                if (pfile != 0) {
                    properties = runtime->SerializeProperties (runtime);

                    if (properties == 0) {
                        RUNTIME_SET_ERROR (runtime, pI_CRuntime_Error_Serialization);
                    } else {
                        fputs (properties, pfile);
                        runtime->FreeString (runtime, properties);
                    }

                    fclose (pfile);
                }
            } /* if (write_property_file == pI_TRUE) */

            /* destroy the runtime instance */
            runtime->Destroy (runtime);
        }
    } /* void DestroyCRuntime (struct _CRuntime* runtime) */

    /* extern */ pI_int GetpIAPIVersion()
    {

        return PI_API_VERSION;
    } /* pI_int GetpIAPIVersion() */

    /* ------ local helper functions */

    pI_byte* CRuntime_GetArgumentDataBySymbol (
        struct _CRuntime* runtime,
        Argument* arg,
        pI_str symbol,
        pI_bool is_structure_symbol)
    {

        pI_int lv;
        pI_int* symbol_sizes;
        pI_str** symbols;
        pI_str** symbol_types;
        pI_byte* retval;
        enum _pIDataTypeClass type_class;
        pI_int type_index, type_size;

        if ((arg == 0) || (symbol == 0)) {
            return 0;
        }

        if (is_structure_symbol == pI_TRUE) {
            symbol_sizes = pI_ArgumentSetupSymbolSizes;
            symbols = pI_ArgumentSetupSymbols;
            symbol_types = pI_ArgumentSetupSymbolTypes;
            retval = &(*((pI_byte* ) arg->signature.setup.Empty));
        } else {
            symbol_sizes = pI_ArgumentDataSymbolSizes;
            symbols = pI_ArgumentDataSymbols;
            symbol_types = pI_ArgumentDataSymbolTypes;
            retval = &(*((pI_byte* ) arg->data.Empty));
        }

        /* Note: the following code relies on the assumption that the address
                 of the first structure member equals the address of the
                 structure itself. */
        for (lv = 0; lv < symbol_sizes[arg->signature.type]; ++lv) {
            if (strcmp (symbol, symbols[arg->signature.type][lv]) == 0) {
                return retval;
            }

            /* fetch type of actual member */
            type_class = CRuntime_GetDataTypeClass (runtime, symbol_types[arg->signature.type][lv], &type_index);
            /* create the object in place, depending on type */
            type_size = GetTopLevelElemSize (type_class, type_index, 0);

            retval += type_size;

            /* enforce proper alignment */
            if ((type_size % PI_STRUCTURE_ALIGNMENT) != 0) {
                retval += PI_STRUCTURE_ALIGNMENT - type_size % PI_STRUCTURE_ALIGNMENT;
            }
        } /* for (lv = 0; lv < symbol_sizes[arg->signature.type]; ++lv) */

        return 0;
    } /* static pI_byte* GetArgumentDataBySymbol (...) */

    pI_str* CRuntime_ArgumentSizeDependenciesAsString (
        struct _CRuntime* runtime,
        Argument* arg,
        pI_str symbol)
    {

        pI_int lv, index = 0;
        pI_str** symbols = pI_ArgumentDataSymbols;
        pI_str** dep_symbols = pI_ArgumentDependencySymbols;
        pI_int* dependencies = pI_ArgumentDependencySymbolsizes;

        if ((arg == 0) || (symbol == 0)) {
            return 0;
        }

        for (lv = 0; lv < dependencies[arg->signature.type]; ++lv) {
            if (strcmp (symbol, symbols[arg->signature.type][index]) == 0) {
                return &(dep_symbols[arg->signature.type][lv + 1]);
            }  else {
                /* advance to next possible dependency */
                while (lv < dependencies[arg->signature.type]) {
                    ++lv;

                    if (dep_symbols[arg->signature.type][lv] == 0) {
                        break;
                    }
                } /* while (lv < dependencies[arg->signature.type]) */

                ++index;
            } /* if (strcmp (symbol, symbols[arg->signature.type][lv]) == 0) */
        } /* for (lv = 0; lv < dependencies[arg->signature.type]; ++lv) */

        return 0;
    } /* static pI_str* ArgumentSizeDependenciesAsString (...) */

    void CRuntime_ResetArgument (
        struct _CRuntime* runtime,
        struct _Argument* argument)
    {

        /* note: use for initialization - it does NOT free any memory */
        if (argument != 0) {
            argument->signature.type = T_pI_Argument_Empty;
            argument->signature.readonly = pI_FALSE;
            argument->signature.constraints = 0;
            argument->signature.setup.Empty = 0;
            argument->data.Empty = 0;
            argument->description.name = 0;
            argument->description.description = 0;
            argument->runtime = runtime;
        } /* if (argument != 0) */
    } /* void CRuntime_ResetArgument (...) */

    pI_bool CRuntime_CreateFlatStructure (
        struct _CRuntime* runtime,
        pI_byte* data_ptr,
        pI_int symbol_sizes[],
        pI_char** * symbols,
        pI_int index,
        pI_int field_index)
    {

        pI_int lv, type_index, type_size;
        enum _pIDataTypeClass type_class;
        pI_bool retval = pI_TRUE;

        for (lv = field_index >= 0 ? field_index : 0; field_index >= 0 ? lv == field_index : lv < symbol_sizes[index]; ++lv) {
            /* fetch type of actual member */
            type_class = CRuntime_GetDataTypeClass (runtime, symbols[index][lv], &type_index);

            /* create the object in place, depending on type */
            if (type_class == INTRINSIC_CLASS) {
                type_size = pI_IntrinsicSizes[type_index];
                retval &=
                    CRuntime_CreateIntrinsicDataType (
                        runtime,
                        data_ptr,
                        (enum _pI_IntrinsicType) type_index);
            } else if (type_class == ARGUMENT_CLASS) {
                type_size = sizeof(struct _Argument);
                CRuntime_ResetArgument (runtime, (struct _Argument* ) data_ptr);
            } else if (type_class == STRUCTURE_CLASS) {
                type_size = pI_StructureSizes[type_index];
                retval &=
                    CRuntime_CreateFlatStructure (
                        runtime,
                        data_ptr,
                        pI_StructureSymbolSizes,
                        pI_StructureSymbolTypes,
                        type_index,
                        -1);
            } else {
                /* unknown type; should never happen; for now, leave by returning false */
                return pI_FALSE;
            }

            data_ptr += type_size;

            /* enforce proper alignment */
            if ((type_size % PI_STRUCTURE_ALIGNMENT) != 0) {
                data_ptr += PI_STRUCTURE_ALIGNMENT - type_size % PI_STRUCTURE_ALIGNMENT;
            }
        } /* for (lv = 0; lv < arg_structure_symbol_sizes[type]; ++lv) */

        return retval;
    } /* static pI_bool CRuntime_CreateFlatStructure (...) */

    pI_bool CRuntime_FreeFlatStructure (
        struct _CRuntime* runtime,
        pI_byte* data_ptr,
        pI_int symbol_sizes[],
        pI_char** * symbols,
        pI_int index,
        pI_int field_index)
    {

        pI_int lv, type_index, type_size;
        enum _pIDataTypeClass type_class;
        pI_bool retval = pI_TRUE;

        for (lv = field_index >= 0 ? field_index : 0; field_index >= 0 ? lv == field_index : lv < symbol_sizes[index]; ++lv) {
            /* fetch type of actual member */
            type_class = CRuntime_GetDataTypeClass (runtime, symbols[index][lv], &type_index);

            /* create the object in place, depending on type */
            if (type_class == INTRINSIC_CLASS) {
                type_size = pI_IntrinsicSizes[type_index];
                retval &=
                    CRuntime_FreeIntrinsicDataType (
                        runtime,
                        data_ptr,
                        (enum _pI_IntrinsicType) type_index);
            } else if (type_class == ARGUMENT_CLASS) {
                type_size = sizeof(struct _Argument);
                retval &= CRuntime_FreeArgument (runtime, (struct _Argument* ) data_ptr, pI_TRUE);
            } else if (type_class == STRUCTURE_CLASS) {
                type_size = pI_StructureSizes[type_index];
                retval &=
                    CRuntime_FreeFlatStructure (
                        runtime,
                        data_ptr,
                        pI_StructureSymbolSizes,
                        pI_StructureSymbols,
                        type_index,
                        -1);
            } else {
                /* unknown type; should never happen; for now, leave by returning false */
                return pI_FALSE;
            }

            data_ptr += type_size;

            /* enforce proper alignment */
            if ((type_size % PI_STRUCTURE_ALIGNMENT) != 0) {
                data_ptr += PI_STRUCTURE_ALIGNMENT - type_size % PI_STRUCTURE_ALIGNMENT;
            }
        } /* for (lv = 0; lv < arg_structure_symbol_sizes[type]; ++lv) */

        return retval;
    } /* static pI_bool CRuntime_FreeFlatStructure (...) */

    pI_bool CRuntime_CopyFlatStructure (
        struct _CRuntime* runtime,
        pI_byte* src_data_ptr,
        pI_byte* dst_data_ptr,
        pI_int symbol_sizes[],
        pI_char** * symbols,
        pI_int index,
        pI_int field_index)
    {

        pI_int lv, type_index, type_size;
        enum _pIDataTypeClass type_class;
        pI_bool retval = pI_TRUE;
        pI_int* ptr;

        for (lv = field_index >= 0 ? field_index : 0; field_index >= 0 ? lv == field_index : lv < symbol_sizes[index]; ++lv) {
            /* fetch type of actual member */
            type_class = CRuntime_GetDataTypeClass (runtime, symbols[index][lv], &type_index);

            /* create the object in place, depending on type */
            if (type_class == INTRINSIC_CLASS) {
                type_size = pI_IntrinsicSizes[type_index];
                retval &=
                    CRuntime_CopyIntrinsicDataType (
                        runtime,
                        src_data_ptr,
                        dst_data_ptr,
                        (enum _pI_IntrinsicType) type_index);
            } else if (type_class == ARGUMENT_CLASS) {
                type_size = sizeof(struct _Argument);
                /* TODO: TEST next two lines! */
                ptr = (pI_int* ) dst_data_ptr;
                *ptr = (int) CRuntime_CopyArgument (runtime, (struct _Argument* ) src_data_ptr, pI_TRUE);
            } else if (type_class == STRUCTURE_CLASS) {
                type_size = pI_StructureSizes[type_index];
                retval &=
                    CRuntime_CopyFlatStructure (
                        runtime,
                        src_data_ptr,
                        dst_data_ptr,
                        pI_StructureSymbolSizes,
                        pI_StructureSymbolTypes,
                        type_index,
                        -1);
            } else {
                /* unknown type; should never happen; for now, leave by returning false */
                return pI_FALSE;
            }

            src_data_ptr += type_size;
            dst_data_ptr += type_size;

            /* enforce proper alignment */
            if ((type_size % PI_STRUCTURE_ALIGNMENT) != 0) {
                src_data_ptr += PI_STRUCTURE_ALIGNMENT - type_size % PI_STRUCTURE_ALIGNMENT;
                dst_data_ptr += PI_STRUCTURE_ALIGNMENT - type_size % PI_STRUCTURE_ALIGNMENT;
            }
        } /* for (lv = 0; lv < arg_structure_symbol_sizes[type]; ++lv) */

        return retval;
    } /* static pI_bool CRuntime_CopyFlatStructure (...) */

    /* post-order free */
    pI_bool CRuntime_RecursiveArgumentDataFree (
        struct _CRuntime* runtime,
        pI_int dimensions,
        pI_int max_dimensions,
        pI_byte* data_ptr,
        pIDataTypeClass type_class,
        pI_int type_index,
        enum _pI_ArgumentType arg_type,
        pI_int index,
        struct _Argument* argument,
        pI_int* data_dimension,
        pI_int* data_step)
    {

        pI_int count, lv, elem_size;
        pI_bool retval = pI_TRUE;
        pI_byte* data;
        pI_byte* next_data_ptr;
        pI_byte* block_ptr = 0;

        if (dimensions == 0) {
            /* Remark: at this level data_ptr points to a flat structure */
            /* free data. */
            return CRuntime_FreeFlatStructure (
                       runtime,
                       data_ptr,
                       pI_ArgumentDataSymbolSizes,
                       pI_ArgumentDataSymbolTypes,
                       arg_type,
                       index);
        } /* if (dimensions == 0) */

        /* get dimension of current scope */
        count = data_dimension[max_dimensions - dimensions];

        if (dimensions == pI_ArgumentDataBlockDimensions[arg_type][index]) {
            /* store data pointer to decunstruct data field AFTER recursion */
            block_ptr = (pI_byte* ) * ((pI_int* ) data_ptr);

            /* fetch the pointer field on lowest level */
            for (lv = dimensions; (lv > 1) && (block_ptr != 0); --lv) {
                block_ptr = (pI_byte* ) * ((pI_int* ) block_ptr);
            }
        }

        /* retrieve element size */
        if (type_class == INTRINSIC_CLASS) {
            elem_size = pI_IntrinsicSizes[type_index];
        } else if (type_class == ARGUMENT_CLASS) {
            elem_size = sizeof(struct _Argument);
        } else if (type_class == STRUCTURE_CLASS) {
            elem_size = pI_StructureSizes[type_index];
        } else {
            return pI_FALSE;
        }

        /* get proper data pointer */
        data = (pI_byte* ) * ((pI_int* ) data_ptr);

        if (data != 0) {
            for (lv = 0; lv < count; ++lv) {
                if (dimensions > 1) {
                    next_data_ptr = data + lv * c_data_ptr_size;
                } else {
                    next_data_ptr = data + lv * elem_size;
                }

                retval &=
                    CRuntime_RecursiveArgumentDataFree (
                        runtime,
                        dimensions - 1,
                        max_dimensions,
                        next_data_ptr,
                        type_class,
                        type_index,
                        arg_type,
                        index,
                        argument,
                        data_dimension,
                        data_step);
            } /* for (lv = 0; lv < count; ++lv) */

            /* clean up pointer field */
            if (dimensions > 1) {
                runtime->FreeMemory (runtime, data);
            }
        } /* if (data != 0) */

        /* deconstruct data field if necessary */
        runtime->FreeMemory (runtime, block_ptr);

        return retval;
    } /* static pI_bool CRuntime_RecursiveArgumentDataFree (...) */

    pI_bool CRuntime_RecursiveArgumentDataAllocate (
        struct _CRuntime* runtime,
        pI_int dimensions,
        pI_int max_dimensions,
        pI_byte* data_ptr,
        pI_byte* data_block_ptr,
        pIDataTypeClass type_class,
        pI_int type_index,
        enum _pI_ArgumentType arg_type,
        pI_int index,
        struct _Argument* argument,
        pI_int* data_dimension,
        pI_int* data_step)
    {

        pI_int count, lv, block_alloc_size = 0;
        pI_bool retval = pI_TRUE;
        pI_byte* data_ptr_field = 0;
        pI_byte* next_data_ptr_field_ptr = 0;
        pI_byte* next_data_block_ptr = 0;

        if (dimensions == 0) {
            /* create data in place */
            return CRuntime_CreateFlatStructure (
                       runtime,
                       data_block_ptr != 0 ? data_block_ptr : data_ptr,
                       pI_ArgumentDataSymbolSizes,
                       pI_ArgumentDataSymbolTypes,
                       arg_type,
                       index);
        } /* if (dimensions == 0) */

        /* get dimension of current scope */
        count = data_dimension[max_dimensions - dimensions];

        if (count == 0) {
            /* zero count means nothing to do */
            *((pI_int* ) data_ptr) = 0;
            return pI_TRUE;
        }

        /* check if block allocation is necessary */
        if (dimensions == pI_ArgumentDataBlockDimensions[arg_type][index]) {
            /* calculate block allocation size */
            block_alloc_size = data_step[max_dimensions - dimensions];
            /* allocate memory block, set it to zero to allow proper deallocation in all cases */
            data_block_ptr = runtime->AllocateMemory (runtime, block_alloc_size);

            if (data_block_ptr == 0) {
                return pI_FALSE;
            }

            memset (data_block_ptr, 0, block_alloc_size);
        } /* if (dimensions == pI_ArgumentDataBlockDimensions[arg_type][index]) */

        /* allocate pointer field if necessary */
        if (dimensions > 1) {
            data_ptr_field = runtime->AllocateMemory (runtime, count * c_data_ptr_size);

            if (data_ptr_field == 0) {
                runtime->FreeMemory (runtime, data_block_ptr);
                return pI_FALSE;
            }
        }

        /* recursively allocate lower dimensions */
        for (lv = 0; lv < count; ++lv) {
            if (dimensions > 1) {
                next_data_ptr_field_ptr = data_ptr_field + lv * c_data_ptr_size;
            } else {
                next_data_ptr_field_ptr = data_ptr;
            }

            if (data_block_ptr != 0) {
                next_data_block_ptr = data_block_ptr + lv * data_step[(max_dimensions - dimensions) + 1];
            } else {
                next_data_block_ptr = 0;
            }

            retval &= CRuntime_RecursiveArgumentDataAllocate (
                          runtime,
                          dimensions - 1,
                          max_dimensions,
                          next_data_ptr_field_ptr,
                          next_data_block_ptr,
                          type_class,
                          type_index,
                          arg_type,
                          index,
                          argument,
                          data_dimension,
                          data_step);
        } /* for (lv = 0; lv < count; ++lv) */

        /* bend outside pointers */
        if (dimensions == 1) {
            *((pI_int* ) data_ptr) = (pI_int) data_block_ptr;
        } else {
            *((pI_int* ) data_ptr) = (pI_int) data_ptr_field;
        }

        return retval;
    } /* pI_bool CRuntime_RecursiveArgumentDataAllocate (...) */

    pI_bool CRuntime_RecursiveArgumentDataCopy (
        struct _CRuntime* runtime,
        pI_int dimensions,
        pI_int max_dimensions,
        pI_byte* in_data_ptr,
        pI_byte* out_data_ptr,
        pI_byte* out_data_block_ptr,
        pIDataTypeClass type_class,
        pI_int type_index,
        enum _pI_ArgumentType arg_type,
        pI_int index,
        struct _Argument* argument,
        pI_int* data_dimension,
        pI_int* data_step)
    {

        pI_int count, lv, block_alloc_size, elem_size;
        pI_bool retval = pI_TRUE;
        pI_byte* src_data_ptr = 0;
        pI_byte* next_src_data_ptr = 0;
        pI_byte* dst_data_ptr_field = 0;
        pI_byte* next_dst_data_ptr_field_ptr = 0;
        pI_byte* next_dst_data_block_ptr = 0;

        if (dimensions == 0) {
            /* create data in place. */
            return CRuntime_CopyFlatStructure (
                       runtime,
                       in_data_ptr,
                       out_data_block_ptr != 0 ? out_data_block_ptr : out_data_ptr,
                       pI_ArgumentDataSymbolSizes,
                       pI_ArgumentDataSymbolTypes,
                       arg_type,
                       index);
        }

        /* create data field */
        count = data_dimension[max_dimensions - dimensions];

        /* check if block allocation is necessary */
        if (dimensions == pI_ArgumentDataBlockDimensions[arg_type][index]) {
            /* calculate block allocation size */
            block_alloc_size = data_step[max_dimensions - dimensions];
            /* allocate memory block, set it to zero to allow proper deallocation in all cases */
            out_data_block_ptr = runtime->AllocateMemory (runtime, block_alloc_size);

            if (out_data_block_ptr == 0) {
                return pI_FALSE;
            }

            memset (out_data_block_ptr, 0, block_alloc_size);
        } /* if (dimensions == pI_ArgumentDataBlockDimensions[arg_type][index]) */

        /* allocate pointer field if necessary*/
        if (dimensions > 1) {
            dst_data_ptr_field = runtime->AllocateMemory (runtime, count * c_data_ptr_size);

            if (dst_data_ptr_field == 0) {
                runtime->FreeMemory (runtime, out_data_block_ptr);
                return pI_FALSE;
            }
        }

        /* retrieve element size */
        if (type_class == INTRINSIC_CLASS) {
            elem_size = pI_IntrinsicSizes[type_index];
        } else if (type_class == ARGUMENT_CLASS) {
            elem_size = sizeof(struct _Argument);
        } else if (type_class == STRUCTURE_CLASS) {
            elem_size = pI_StructureSizes[type_index];
        } else {
            return pI_FALSE;
        }

        /* get proper src data pointer */
        src_data_ptr = (pI_byte* ) * ((pI_int* ) in_data_ptr);

        /* recursively allocate lower dimensions */
        for (lv = 0; lv < count; ++lv) {
            if (dimensions > 1) {
                next_dst_data_ptr_field_ptr = dst_data_ptr_field + lv * c_data_ptr_size;
                next_src_data_ptr = src_data_ptr + lv * c_data_ptr_size;
            } else {
                next_dst_data_ptr_field_ptr = out_data_ptr;
                next_src_data_ptr = src_data_ptr  + lv * elem_size;
            }

            if (out_data_block_ptr != 0) {
                next_dst_data_block_ptr = out_data_block_ptr + lv * data_step[(max_dimensions - dimensions) + 1];
            } else {
                next_dst_data_block_ptr = 0;
            }

            retval &=
                CRuntime_RecursiveArgumentDataCopy (
                    runtime,
                    dimensions - 1,
                    max_dimensions,
                    next_src_data_ptr,
                    next_dst_data_ptr_field_ptr,
                    next_dst_data_block_ptr,
                    type_class,
                    type_index,
                    arg_type,
                    index,
                    argument,
                    data_dimension,
                    data_step);
        } /* for (lv = 0; lv < count; ++lv) */

        /* bend outside pointers */
        if (dimensions == 1) {
            *((pI_int* ) out_data_ptr) = (pI_int) out_data_block_ptr;
        } else {
            *((pI_int* ) out_data_ptr) = (pI_int) dst_data_ptr_field;
        }

        return retval;
    } /* static pI_bool CRuntime_RecursiveArgumentDataCopy (...) */

    enum _pI_IntrinsicType CRuntime_IntrinsicNameToType (pI_char* name)
    {

        pI_size lv, ub = sizeof(pI_IntrinsicNames) / sizeof(*pI_IntrinsicNames);

        for (lv = 0; lv < ub; ++lv) {
            if (strcmp (pI_IntrinsicNames[lv], name) == 0) {
                return lv;
            }
        }

        return (enum _pI_IntrinsicType) - 1;
    } /* enum _pI_IntrinsicType CRuntime_IntrinsicNameToType (pI_char* name) */

    enum _pI_StructureType CRuntime_StructureNameToType (pI_char* name)
    {

        pI_size lv, ub = sizeof(pI_StructureNames) / sizeof(*pI_StructureNames);

        for (lv = 0; lv < ub; ++lv) {
            if (strcmp (pI_StructureNames[lv], name) == 0) {
                return lv;
            }
        }

        return (enum _pI_StructureType) - 1;
    } /* enum _pI_StructureType CRuntime_StructureNameToType (pI_char* name) */

    pI_byte* CRuntime_FetchArgumentSetupStructureSymbol (
        struct _CRuntime* runtime,
        pI_byte* data,
        enum _pI_StructureType struct_type,
        pI_str symbol,
        enum _pIDataTypeClass* kind,
        pI_int* index)
    {

        pI_int lv;
        pI_str type_string = 0;
        pI_int dt_index, type_index, type_size;
        enum _pIDataTypeClass dt_class;
        pI_str subsymbol, temp;
        pI_char c_url_separator_char = '.';
        pI_str c_url_separator_str = ".";

        if ((runtime == 0) || (data == 0) || (symbol == 0)) {
            return 0;
        }

        subsymbol = strstr (symbol, c_url_separator_str);

        if (subsymbol != 0) {
            temp = runtime->CopyStringN (runtime, symbol, subsymbol - symbol);
        } else {
            temp = runtime->CopyString (runtime, symbol);
        }

        for (lv = 0; lv < *index; ++lv) {
            if (strcmp (symbol, pI_StructureSymbols[struct_type][lv]) == 0) {
                break;
            }

            /* fetch type of actual member */
            dt_class = CRuntime_GetDataTypeClass (runtime, pI_StructureSymbolTypes[struct_type][lv], &type_index);

            /* create the object in place, depending on type */
            if (dt_class == INTRINSIC_CLASS) {
                type_size = pI_IntrinsicSizes[type_index];
            } else if (dt_class == ARGUMENT_CLASS) {
                type_size = sizeof(struct _Argument);
            } else if (dt_class == STRUCTURE_CLASS) {
                type_size = pI_StructureSizes[type_index];
            } else {
                runtime->FreeString (runtime, temp);
                return 0; /* should never happen */
            }

            data += type_size;

            /* enforce proper alignment */
            if ((type_size % PI_STRUCTURE_ALIGNMENT) != 0) {
                data += PI_STRUCTURE_ALIGNMENT - type_size % PI_STRUCTURE_ALIGNMENT;
            }
        } /* for (lv = 0; lv < pI_ArgumentSetupSymbolSizes[arg_type]; ++lv) */

        runtime->FreeString (runtime, temp);

        type_string = pI_StructureSymbolTypes[struct_type][lv];
        dt_class = CRuntime_GetDataTypeClass (runtime, type_string, &dt_index);

        if (kind != 0) {
            *kind = dt_class;
        }

        if (index != 0) {
            *index = dt_index;
        }

        switch (dt_class) {
            case INTRINSIC_CLASS: {
                if (subsymbol != 0) {
                    /* this should NEVER happen */
                }

                return data;
            } /* case INTRINSIC_CLASS: */
            case ARGUMENT_CLASS: {
                if (subsymbol == 0) {
                    return data;
                }

                return CRuntime_FetchArgumentSetupSymbol (runtime, (struct _Argument* ) data, subsymbol + 1, kind, index);
            }
            case STRUCTURE_CLASS: {
                if (subsymbol == 0) {
                    return data;
                }

                return CRuntime_FetchArgumentSetupStructureSymbol (runtime, data, dt_index, subsymbol + 1, kind, index);
            }
        } /* switch (dt_class) */

        return 0;
    } /* pI_byte* CRuntime_FetchArgumentSetupStructureSymbol (...) */

    pI_bool CRuntime_HasProperty (
        struct _CRuntime* runtime,
        pI_str name)
    {

        pI_int low = 0, mid, high, comp_result;

        if ((runtime == 0) || (name == 0) || (runtime->property_count == 0)) {
            return pI_FALSE;
        }

        low = 0, high = runtime->property_count;

        while (low < high) {
            mid = low + (high - low) / 2;
            comp_result = strcmp (runtime->property_map[mid]->description.name, name);

            if (comp_result < 0) {
                low = mid + 1;
            } else {
                high = mid;
            }
        }

        if ((low < runtime->property_count) &&
                (strcmp (runtime->property_map[low]->description.name, name) == 0)) {
            return pI_TRUE;
        }

        return pI_FALSE;
    } /* pI_bool CRuntime_HasProperty (...) */

    struct _Argument* CRuntime_GetProperty (
        struct _CRuntime* runtime,
        pI_str name,
        pI_bool get_copy) {

        pI_int low = 0, mid, high, comp_result;

        if ((runtime == 0) || (name == 0) || (runtime->property_count == 0)) {
            return 0;
        }

        low = 0, high = runtime->property_count;

        while (low < high) {
            mid = low + (high - low) / 2;
            comp_result = strcmp (runtime->property_map[mid]->description.name, name);

            if (comp_result < 0) {
                low = mid + 1;
            } else {
                high = mid;
            }
        }

        if ((low < runtime->property_count) &&
                (strcmp (runtime->property_map[low]->description.name, name) == 0)) {
            if (get_copy == pI_FALSE) {
                return runtime->property_map[low];
            } else {
                return runtime->CopyArgument (runtime, runtime->property_map[low], pI_TRUE);
            }
        }

        return 0;
    } /* struct _Argument* CRuntime_GetProperty (...) */

    pI_bool CRuntime_StoreProperty (
        struct _CRuntime* runtime,
        struct _Argument* prop,
        pI_bool take_ownership)
    {

        struct _Argument** temp_arg_map;
        pI_int lv, capacity, low, mid = 0, high, comp_result = 0;

        /* check precondidtions */
        if ((runtime == 0) || (prop == 0) || (prop->description.name == 0)) {
            return pI_FALSE;
        }

        /* 1. check if property is already available */
        if (runtime->GetProperty (runtime, prop->description.name, pI_FALSE) != 0) {
            return pI_FALSE;
        }

        /* 2. check if the property map needs an upsize operation */
        if (runtime->property_capacity == runtime->property_count) {
            /* upsize is necessary */
            if (runtime->property_capacity < 8) {
                capacity = 8;
            } else {
                capacity = runtime->property_capacity * 2;
            }

            temp_arg_map = runtime->AllocateMemory (runtime, sizeof(struct _Argument*) * capacity);

            if (temp_arg_map == 0) {
                return pI_FALSE;
            }

            for (lv = 0; lv < runtime->property_count; ++lv) {
                temp_arg_map[lv] = runtime->property_map[lv];
            }

            for (; lv < capacity; ++lv) {
                temp_arg_map[lv] = 0;
            }

            runtime->FreeMemory (runtime, runtime->property_map);
            runtime->property_map = temp_arg_map;
            runtime->property_capacity = capacity;
        }

        /* 3. add the new property at the correct position */
        /* 3.a find correct spot using binary search */
        low = 0, high = runtime->property_count;

        while (low < high) {
            mid = low + (high - low) / 2;
            comp_result = strcmp (runtime->property_map[mid]->description.name, prop->description.name);

            if (comp_result < 0) {
                low = mid + 1;
            } else {
                high = mid;
            }
        }

        /* 3.b left-shift entries from found spot */
        for (lv = runtime->property_count - 1; lv >= low; --lv) {
            runtime->property_map[lv + 1] = runtime->property_map[lv];
        }

        /* 3.c insert new entry */
        ++runtime->property_count;

        if (take_ownership == pI_TRUE) {
            runtime->property_map[low] = prop;
        } else {
            runtime->property_map[low] = runtime->CopyArgument (runtime, prop, pI_TRUE);
            return (runtime->property_map[low] != 0);
        }

        return pI_TRUE;
    } /* pI_bool CRuntime_StoreProperty (...) */

    pI_bool CRuntime_RemoveProperty (
        struct _CRuntime* runtime,
        pI_str name)
    {

        pI_int low = 0, mid, high, comp_result, lv;

        if ((runtime == 0) || (name == 0) || (runtime->property_count == 0)) {
            return pI_FALSE;
        }

        low = 0, high = runtime->property_count;

        while (low < high) {
            mid = low + (high - low) / 2;
            comp_result = strcmp (runtime->property_map[mid]->description.name, name);

            if (comp_result < 0) {
                low = mid + 1;
            } else {
                high = mid;
            }
        }

        if ((low < runtime->property_count) &&
                (strcmp (runtime->property_map[low]->description.name, name) == 0)) {
            runtime->FreeArgument (runtime, runtime->property_map[mid], pI_TRUE);

            for (lv = mid; lv < runtime->property_count; ++lv) {
                runtime->property_map[lv] = runtime->property_map[lv + 1];
            }

            --runtime->property_count;
            return pI_TRUE;
        }

        return pI_FALSE;
    } /* pI_bool CRuntime_RemoveProperty (...) */

    void CRuntime_ClearProperties (
        struct _CRuntime* runtime,
        pI_bool free_memory)
    {

        pI_int lv;

        for (lv = 0; lv < runtime->property_count; ++lv) {
            runtime->FreeArgument (runtime, runtime->property_map[lv], pI_TRUE);
            runtime->property_map[lv] = 0;
        } /* for (lv = 0; lv < runtime->property_count; ++lv) */

        runtime->property_count = 0;

        if (free_memory == pI_TRUE) {
            runtime->FreeMemory (runtime, runtime->property_map);
            runtime->property_map = 0;
            runtime->property_capacity = 0;
        } /* if (free_memory == pI_TRUE) */
    } /* void CRuntime_ClearProperties (...) */

    pI_str CRuntime_SerializeProperties (
        struct _CRuntime* runtime)
    {

        struct _ArgumentList arg_list;
        pI_int snlen;

        arg_list.count = runtime->property_count;
        arg_list.list = runtime->property_map;
        return pI_JSON_SerializeArgumentList (runtime, &arg_list, &snlen);
    } /* pI_str CRuntime_SerializeProperties (...) */

    pI_bool CRuntime_DeserializeProperties (
        struct _CRuntime* runtime,
        pI_str sprops)
    {

        struct _ArgumentList* arg_list;
        arg_list = pI_JSON_DeserializeArgumentList (runtime, sprops, 0);

        if ((arg_list != 0) && (arg_list->count != 0) && (arg_list->list != 0)) {
            /* NOTE: next line clears any property in memory; this is perhaps not the best thing to do ?!? */
            runtime->ClearProperties (runtime, pI_TRUE);
            runtime->property_capacity = arg_list->count;
            runtime->property_count = arg_list->count;
            runtime->property_map = arg_list->list;
            runtime->FreeMemory (runtime, arg_list);
            return pI_TRUE;
        }

        return pI_FALSE;
    } /* pI_bool CRuntime_DeserializeProperties (...) */



    void FetchArgumentDataCountPerDimension (
        struct _CRuntime* runtime,
        struct _Argument* argument,
        pI_int field_index,
        pI_int* retval,
        pI_int max_depth)
    {

        pI_int dims, lv;
        pI_int temp = argument->signature.type;
        pI_int* arg_data = 0;
        pI_str* dep_str = 0;
        dims = pI_ArgumentDataDimensions[argument->signature.type][field_index];

        dep_str =
            CRuntime_ArgumentSizeDependenciesAsString (
                runtime,
                argument,
                pI_ArgumentDataSymbols[argument->signature.type][field_index]);

        for (lv = 0; lv < max_depth; ++lv) {
            if (lv < dims) {
                if (dep_str != 0) {
                    arg_data = (pI_int* ) CRuntime_GetArgumentDataBySymbol (
                                   runtime,
                                   argument,
                                   dep_str[lv],
                                   pI_TRUE);
                    retval[lv] = arg_data != 0 ? *arg_data : 0;
                } else {
                    retval[lv] = 0;
                }
            } else {
                retval[lv] = 0;
            }
        }
    } /* void FetchArgumentDataCountPerDimension (...) */

    void CalculateArgumentDataStepPerDimension (
        pI_int* dimensions,
        pI_int* retval,
        pI_int elem_size,
        pI_int max_depth,
        pI_int padding_dim)
    {

        pI_int lv;

        padding_dim = max_depth - padding_dim;
        retval[max_depth] = elem_size;

        if (padding_dim == max_depth) {
            if (PI_FIELD_PADDING > 0) {
                if ((retval[max_depth] % PI_FIELD_PADDING) != 0) {
                    retval[max_depth] += PI_FIELD_PADDING - retval[max_depth] % PI_FIELD_PADDING;
                }
            }
        }

        for (lv = max_depth - 1; lv >= 0; --lv) {
            retval[lv] = dimensions[lv] * retval[lv + 1];

            if (padding_dim == lv) {
                if (PI_FIELD_PADDING > 0) {
                    if ((retval[lv] % PI_FIELD_PADDING) != 0) {
                        retval[lv] += PI_FIELD_PADDING - retval[lv] % PI_FIELD_PADDING;
                    }
                }
            }
        }
    } /* void CalculateArgumentDataStepPerDimension (...) */

    pI_int GetTopLevelElemSize (
        enum _pIDataTypeClass type_class,
        pI_int type_index,
        pI_int data_dimensions)
    {

        if (data_dimensions > 0) {
            return c_data_ptr_size;
        }

        if (type_class == INTRINSIC_CLASS) {
            return pI_IntrinsicSizes[type_index];
        } else if (type_class == ARGUMENT_CLASS) {
            return sizeof(struct _Argument);
        } else if (type_class == STRUCTURE_CLASS) {
            return pI_StructureSizes[type_index];
        }

        return 0;
    } /* pI_int GetTopLevelElemSize (...) */


#undef RUNTIME_SAFE_CSTR_DUP
#undef RUNTIME_SAFE_FREE
#undef RUNTIME_SET_ERROR

#undef PI_BUILD_RUNTIME

#ifdef __cplusplus
}
#endif
