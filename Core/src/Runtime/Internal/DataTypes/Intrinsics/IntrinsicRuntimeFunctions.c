/* IntrinsicRuntimeFunctions.c
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdio.h>

#include <PureImage.h>
#include <Runtime/Internal/DataTypes/Intrinsics/IntrinsicSizes.h>

#include <Runtime/Internal/DataTypes/Intrinsics/IntrinsicRuntimeFunctions.h>

#ifdef __GNUC__
#define SPRINTF(buf, maxlen, pat, data) snprintf(buf, maxlen, pat, data)
#define SSCANF(str, f, data) sscanf(str, f, data)
#else
#define SPRINTF(buf, maxlen, pat, data) sprintf_s(buf, maxlen, pat, data)
#define SSCANF(str, f, data) sscanf_s(str, f, data)
#endif

#ifdef __cplusplus
extern "C" {
#endif

    pI_bool CRuntime_CreateIntrinsicDataType (
        struct _CRuntime* runtime,
        pI_byte* data_ptr,
        enum _pI_IntrinsicType type)
    {

        if ((data_ptr == 0) || (runtime == 0)) {
            return pI_FALSE;
        }

        /* create data in place. */
        switch (type) {
            case _pI_str: {
                data_ptr = runtime->AllocateMemory (runtime, sizeof(pI_str));
                break;
            }
            default:
                memset (data_ptr, 0, pI_IntrinsicSizes[type]);
        }

        return pI_TRUE;
    } /* pI_bool CRuntime_CreateIntrinsicDataType (...) */

    pI_bool CRuntime_CopyIntrinsicDataType (
        struct _CRuntime* runtime,
        pI_byte* in_data_ptr,
        pI_byte* out_data_ptr,
        enum _pI_IntrinsicType type)
    {

        if ((in_data_ptr == 0) || (out_data_ptr == 0)) {
            return pI_FALSE;
        }

        /* copy data from src to dst. */
        switch (type) {
            case _pI_str: {
                pI_int* ptr = (pI_int* ) out_data_ptr;
				pI_str* str_ptr = (pI_str*) ptr;
                *str_ptr = runtime->CopyString (runtime, (pI_str) * (pI_int* ) in_data_ptr);
                break;
            }
            default:
                memcpy (out_data_ptr, in_data_ptr, pI_IntrinsicSizes[type]);
        }

        return pI_TRUE;
    } /* pI_bool CRuntime_CopyIntrinsicDataType (...) */

    pI_bool CRuntime_FreeIntrinsicDataType (
        struct _CRuntime* runtime,
        pI_byte* data_ptr,
        enum _pI_IntrinsicType type)
    {

        if ((data_ptr == 0) || (runtime == 0)) {
            return pI_FALSE;
        }

        /* free data in place. */
        switch (type) {
            case _pI_str: {
                runtime->FreeString (runtime, (pI_str) *(pI_int* ) data_ptr);
                data_ptr = 0;
                break;
            }
            default:
                memset (data_ptr, 0, pI_IntrinsicSizes[type]);
        }

        return pI_TRUE;
    } /* pI_bool CRuntime_FreeIntrinsicDataType (...) */

    pI_str CRuntime_SerializeIntrinsicDataType (
        struct _CRuntime* runtime,
        pI_byte* data_ptr,
        enum _pI_IntrinsicType type,
        pI_size* str_len)
    {

        pI_char buf[64];
        pI_size len;

        if (data_ptr == 0) {
            return 0;
        }

        switch (type) {
            case _pI_byte:
                SPRINTF (buf, 64, "%d", (pI_int) *data_ptr);
                len = strlen (buf);

                if (str_len != 0) {
                    *str_len = len;
                }

                return runtime->CopyStringN (runtime, buf, len);
            case _pI_int:
            case _pI_unsigned:
            case _pI_size:
                SPRINTF (buf, 64, "%d", *(pI_int* ) data_ptr);
                len = strlen (buf);

                if (str_len != 0) {
                    *str_len = len;
                }

                return runtime->CopyStringN (runtime, buf, len);

            case _pI_short:
            case _pI_ushort:
                SPRINTF (buf, 64, "%hd", *(pI_short* ) data_ptr);
                len = strlen (buf);

                if (str_len != 0) {
                    *str_len = len;
                }

                return runtime->CopyStringN (runtime, buf, len);
            case _pI_float:
                SPRINTF (buf, 64, "%f", *(pI_float* ) data_ptr);
                len = strlen (buf);

                if (str_len != 0) {
                    *str_len = len;
                }

                return runtime->CopyStringN (runtime, buf, len);
            case _pI_double:
                SPRINTF (buf, 64, "%lf", *(pI_double* ) data_ptr);
                len = strlen (buf);

                if (str_len != 0) {
                    *str_len = len;
                }

                return runtime->CopyStringN (runtime, buf, len);
            case _pI_bool:
                SPRINTF (buf, 64, "%s", *(pI_bool* ) data_ptr == pI_TRUE ? "true" : "false");
                len = strlen (buf);

                if (str_len != 0) {
                    *str_len = len;
                }

                return runtime->CopyStringN (runtime, buf, len);
            case _pI_char:
                SPRINTF (buf, 64, "%c", *data_ptr);

                if (str_len != 0) {
                    *str_len = 1;
                }

                return runtime->CopyStringN (runtime, buf, 1);
            case _pI_str:

                if ((str_len != 0) && (data_ptr != 0)) {
					if (* (pI_int* ) data_ptr != 0)
						*str_len = strlen ((pI_str) * (pI_int* ) data_ptr);
					else
						*str_len = 0;
                }

                return runtime->CopyString (runtime, (pI_str) * (pI_int* ) data_ptr);
        }

        return 0;
    } /* pI_str CRuntime_SerializeIntrinsicDataType (...) */

    pI_bool CRuntime_DeserializeIntrinsicDataType (
        struct _CRuntime* runtime,
        pI_byte* data_ptr,
        enum _pI_IntrinsicType type,
        pI_str args11n)
    {

        pI_char buf[64];
        pI_int beauty_byte;
        pI_str as_string;

        switch (type) {
            case _pI_byte:
                SSCANF (args11n, "%d", &beauty_byte);
                *data_ptr = (pI_byte) beauty_byte;
                return pI_TRUE;
            case _pI_int:
            case _pI_unsigned:
            case _pI_size:
                SSCANF (args11n, "%d", (pI_size*) data_ptr);
                return pI_TRUE;
            case _pI_short:
            case _pI_ushort:
                SSCANF (args11n, "%hd", (pI_short*) data_ptr);
                return pI_TRUE;
            case _pI_float:
                SSCANF (args11n, "%f", (pI_float*) data_ptr);
                return pI_TRUE;
            case _pI_double:
                SSCANF (args11n, "%lf", (pI_double*) data_ptr);
                return pI_TRUE;
            case _pI_bool:
                SSCANF (args11n, "%64s", buf);

                if (strcmp (buf, "true") == 0) {
                    *(pI_bool* ) data_ptr = pI_TRUE;
                } else {
                    *(pI_bool* ) data_ptr = pI_FALSE;
                }

                return pI_TRUE;
            case _pI_char:
                *(pI_char* ) data_ptr = *args11n;
                return pI_TRUE;
            case _pI_str:
                as_string = runtime->CopyString (runtime, args11n);
                data_ptr = as_string;
                return pI_TRUE;
        }

        return pI_FALSE;
    } /* pI_bool CRuntime_DeserializeIntrinsicDataType (...) */

#ifdef __cplusplus
}
#endif
