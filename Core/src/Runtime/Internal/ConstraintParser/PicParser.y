/* PicParser.y
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

%require "2.4.1"
%defines "../../../../include/Runtime/Internal/ConstraintParser/PicParser.h"
%debug
%error-verbose
%locations

%code requires {

/**
* ATTENTION:
* Remark about thread-safety:
*   Setting the string and calling the parser is not atomic.
*   I.e., parsing is not thread-safe at all.
*
*   So, in multi-threaded environments make sure to secure this cal path
*   (i.e., setting string and call yyparse) by means of a mutex.
*/

#include <PureImage.h>
#include <Runtime/Internal/ConstraintParser/ParserSymbols.h>
#include <Runtime/Internal/ConstraintParser/SymbolStack.h>

/* These defines make accessing the stack and symbol table easier */
#define PIC_PUSH( SYMBOL ) \
do {\
    struct _pI_CP_SymbolStack* symbol_stack;\
    symbol_stack = ((struct _pI_CP_ParserArg*) parser_arg)->symbol_stack;\
    symbol_stack->push(symbol_stack, SYMBOL);\
} while(0)

#define RUNTIME ((struct _pI_CP_ParserArg*) parser_arg)->runtime

/* Defines the signature of the lexer function */
#define YYLEX_PARAM &yylval, &yylloc, RUNTIME

typedef struct _pI_CP_ParserArg
{
  struct _CRuntime* runtime;
  struct _pI_CP_SymbolStack* symbol_stack;
} pI_CP_ParserArg;

/* lexer errors are propagated by means of this (global!) variable */
int pI_CP_lex_error;

/* This defines that the parser has an argument */
#define YYPARSE_PARAM parser_arg


/* Some declarations that build the interface to the parser */

/* Change this variable to control debug messages (use like error levels) */
extern int yydebug;

/**
* Sets the string the parser has to process.
*
* Remark: see comments about thread-safety.
*
* @param str The string to be parsed.
*/
extern void yy_set_string(char* str);

/**
* Executes the parser using a given parser argument structure which contains a pointer to
* the resulting stack and a pointer to the symbol table to be used.
*
* Remark: unfortunately bison generates a void* signature
*
*/
extern int yyparse (void* parse_arg);


/**
* This method is called by yyparse when errors occur.
* So, implement this to receive these parser errors.
*/
extern int yyerror (char* msg);

}
/* This code block will be part of the pic_parser.c */
%code {

/* Defines the signature of the lexer function */
extern int yylex (YYSTYPE* yylval, YYLTYPE* yylloc, struct _CRuntime* runtime);

}


%initial-action
{
    /* Reset the lexer error */
    /* TODO: maybe this should better be done in lexer implementation */
    pI_CP_lex_error = 0;
};

/* The types of the data produced by the scanner */
%union {
   pI_double double_val;
   pI_int int_val;
   pI_bool bool_val;
   pI_str str_val;
}

%token                  END

%left <symbol>          AND
%left <symbol>          OR
 /* Important: define NOT as binding right and with greater priority as logical ops (e.g. AND), */
 /* otherwise there is a S/R conflict against log op rules. */
%right <symbol>         NOT

%token <symbol>         EQ
%token <symbol>         LT
%token <symbol>         LEQ
%token <symbol>         GT
%token <symbol>         GEQ
%token <symbol>         NEQ

%token <str_val>        IDENTIFIER
%token <int_val>        INTEGER
%token <double_val>     DOUBLE
%token <str_val>        STRING
%token <bool_val>       TRUE
%token <bool_val>       FALSE
%token OPEN_PAREN
%token CLOSE_PAREN
%token ERROR

%start bool_exp

%%

bool_exp
:
    TRUE    {   PIC_PUSH(pI_CP_CreateBoolSymbol(RUNTIME, 1));   }
|
    FALSE   {   PIC_PUSH(pI_CP_CreateBoolSymbol(RUNTIME, 0));   }
|
    INTEGER {   PIC_PUSH(pI_CP_CreateIntSymbol(RUNTIME, $1 ) ); }
|
    OPEN_PAREN bool_exp CLOSE_PAREN
|
    NOT bool_exp {  PIC_PUSH(pI_CP_CreateOperatorSymbol(RUNTIME, ps_NOT, 1)); }
|
    bool_exp OR bool_exp {  PIC_PUSH(pI_CP_CreateOperatorSymbol(RUNTIME, ps_OR, 2)); }
|
    bool_exp AND bool_exp { PIC_PUSH(pI_CP_CreateOperatorSymbol(RUNTIME, ps_AND, 2)); }
|
    eval_exp EQ eval_exp { PIC_PUSH(pI_CP_CreateOperatorSymbol(RUNTIME, ps_EQ, 2)); }
|
    eval_exp NEQ eval_exp { PIC_PUSH(pI_CP_CreateOperatorSymbol(RUNTIME, ps_NEQ, 2)); }
|
    eval_exp LT eval_exp { PIC_PUSH(pI_CP_CreateOperatorSymbol(RUNTIME, ps_LT, 2)); }
|
    eval_exp LEQ eval_exp { PIC_PUSH(pI_CP_CreateOperatorSymbol(RUNTIME, ps_LEQ, 2)); }
|
    eval_exp GT eval_exp { PIC_PUSH(pI_CP_CreateOperatorSymbol(RUNTIME, ps_GT, 2)); }
|
    eval_exp GEQ eval_exp { PIC_PUSH(pI_CP_CreateOperatorSymbol(RUNTIME, ps_GEQ, 2)); }
;

eval_exp
:
    INTEGER     { PIC_PUSH( pI_CP_CreateIntSymbol(RUNTIME, $1 ) ); }
|
    DOUBLE      { PIC_PUSH( pI_CP_CreateDoubleSymbol(RUNTIME, $1 ) ); }
|
    STRING      { PIC_PUSH( pI_CP_CreateStringSymbol(RUNTIME, $1 ) ); }
|
    IDENTIFIER  { PIC_PUSH( pI_CP_CreateVariableSymbol(RUNTIME, $1 ) ); }
;

%%

/* This declaration is necessary to import the lexer function into the parser. */
extern int yy_scan_string(char* str);

/**
* Sets the string the parser has to process.
*
* Remark: setting the string and calling the parser is not atomic.
*   I.e., parsing is not thread-safe at all.
*   So, in multi-threaded environments make sure to secure this cal path
*   (i.e., setting string and call yyparse) by means of a mutex.
*
* @param str The string to be parsed.
*/
void yy_set_string(char* str) {
    yy_scan_string(str);
}
