/* StackEval.c
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <string.h>
#include <stdio.h> /* for sprintf_s */

#ifdef __GNUC__
#define SPRINTF(buf, maxlen, pat, data) snprintf(buf, maxlen, pat, data)
#else
#define SPRINTF(buf, maxlen, pat, data) sprintf_s(buf, maxlen, pat, data)
#endif

#include <ctype.h>

#include <Runtime/Internal/ConstraintParser/StackEval.h>
#include <Runtime/Internal/ConstraintParser/ParserSymbols.h>
#include <Runtime/Internal/RuntimeHelperFunctions.h>

#ifdef __cplusplus
extern "C" {
#endif

#define PI_STACK_EVAL_DOUBLE_COMPARE_EPSILON 1E-20
#define PI_STACK_EVAL_ERRMSG_BUF_SIZE 512

    pI_int evalStack_err;
    pI_char evalStack_errMsg[512];

    pI_int retrieveValue(struct _pI_CP_SymbolStack* stack, struct _pI_CP_Symbol** * tos, struct _Argument* arg, struct _pI_CP_Symbol* result);

    pI_bool retrieveBoolean(struct _pI_CP_SymbolStack* stack, struct _pI_CP_Symbol** * tos, struct _Argument* arg);

    pI_int op_double(enum _pI_CP_SymbolCode op_code, pI_double left, pI_double right)
    {

        switch (op_code) {
            case ps_EQ:
                /* double equals: using */
                return fabs(left - right) < PI_STACK_EVAL_DOUBLE_COMPARE_EPSILON;
            case ps_NEQ:
                return !op_double( ps_EQ, left, right );
            case ps_GT:
                return left > right;
            case ps_GEQ:
                return ( op_double( ps_EQ, left, right ) || left > right ) ;
            case ps_LT:
                return left < right;
            case ps_LEQ:
                return ( op_double( ps_EQ, left, right ) || left < right ) ;
            default:
                /* never reached */
                SPRINTF(evalStack_errMsg, PI_STACK_EVAL_ERRMSG_BUF_SIZE, "Unknown operator code: %d", op_code);
                evalStack_err = -42;
                return 0;
        }

    }

    pI_int op_string(enum _pI_CP_SymbolCode op_code, pI_str left, pI_str right)
    {

        switch (op_code) {
            case ps_EQ:
                /* double equals: using */
                return (strcmp(left, right) == 0);
            case ps_NEQ:
                return !op_string( ps_EQ, left, right );
            case ps_GT:
                return (strcmp(left, right) > 0);
            case ps_GEQ:
                return ( op_string( ps_EQ, left, right ) || op_string( ps_GT, left, right ) ) ;
            case ps_LT:
                return (strcmp(left, right) < 0);
            case ps_LEQ:
                return ( op_string( ps_EQ, left, right ) || op_string( ps_LT, left, right ) ) ;
            default:
                /* never reached */
                SPRINTF(evalStack_errMsg, PI_STACK_EVAL_ERRMSG_BUF_SIZE, "Unknown operator code: %d", op_code);
                evalStack_err = -42;
                return 0;
        }
    }

    pI_int op_int(enum _pI_CP_SymbolCode op_code, pI_int left, pI_int right)
    {

        switch (op_code) {
            case ps_EQ:
                /* double equals: using */
                return left == right;
            case ps_NEQ:
                return left != right;
            case ps_GT:
                return left > right;
            case ps_GEQ:
                return left >= right;
            case ps_LT:
                return left < right;
            case ps_LEQ:
                return left <= right;
            default:
                /* never reached */
                SPRINTF(evalStack_errMsg, PI_STACK_EVAL_ERRMSG_BUF_SIZE, "Unknown operator code: %d", op_code);
                evalStack_err = -42;
                return 0;
        }
    }

    pI_int pI_CP_EvalStack_getLastError()
    {
        return evalStack_err;
    }

    pI_str pI_CP_EvalStack_getLastErrorMsg()
    {
        return evalStack_errMsg;
    }

    pI_int pI_CP_evaluate_symbol_stack(struct _pI_CP_SymbolStack* stack, struct _Argument* arg)
    {
        struct _pI_CP_Symbol** tos;

        evalStack_err = 0;

        tos = stack->top;

        return retrieveBoolean(stack, &tos, arg);
    }

    pI_double get_double_from_symbol(struct _pI_CP_Symbol* symbol)
    {
        switch (symbol->value_type) {
            case _pI_int:
                return (pI_double) symbol->data.int_val;
            case _pI_bool:
                return (pI_double) symbol->data.bool_val;
            case _pI_double:
                return symbol->data.double_val;
            case _pI_str:
                /* don't allow numbers as string */
            default:
                SPRINTF(evalStack_errMsg, PI_STACK_EVAL_ERRMSG_BUF_SIZE, "Implicit cast to double not supported for type %d.",
                          symbol->value_type);
                evalStack_err = pI_Constraint_Error_IllegalType;
                return 0;
        }
    }

    pI_int get_int_from_symbol(struct _pI_CP_Symbol* symbol)
    {
        switch (symbol->value_type) {
            case _pI_int:
                return (pI_int) symbol->data.int_val;
            case _pI_bool:
                return (pI_int) symbol->data.bool_val;
            case _pI_double:
                return (pI_int) symbol->data.double_val;
            case _pI_str:
                /* don't allow numbers as string */
            default:
                SPRINTF(evalStack_errMsg, PI_STACK_EVAL_ERRMSG_BUF_SIZE, "Implicit cast to int not supported for type %d.",
                          symbol->value_type);
                evalStack_err = pI_Constraint_Error_IllegalType;
                return 0;
        }
    }

    pI_bool get_bool_from_symbol(struct _pI_CP_Symbol* symbol)
    {
        switch (symbol->value_type) {
            case _pI_int:
                return symbol->data.int_val != 0;
            case _pI_bool:
                return symbol->data.bool_val;
            case _pI_double:
                return symbol->data.double_val != 0.0;
            case _pI_str: {
                char lower_str[255];
                int idx;
                int slen = strlen(symbol->data.str_val);

                for (idx = 0; idx < slen; idx++) {
                    lower_str[idx] = tolower(symbol->data.str_val[idx]);
                }

                if (strcmp(lower_str, "false")) {
                    return 0;
                } else if (strcmp(lower_str, "true")) {
                    return 1;
                } else {
                    SPRINTF(evalStack_errMsg, PI_STACK_EVAL_ERRMSG_BUF_SIZE, "Implicit cast to bool not supported for string %s.",
                              symbol->data.str_val);
                    evalStack_err = pI_Constraint_Error_IllegalType;
                    return 0;
                }
            }
            default:
                SPRINTF(evalStack_errMsg, PI_STACK_EVAL_ERRMSG_BUF_SIZE, "Implicit cast to bool not supported for type %d.",
                          symbol->value_type);
                evalStack_err = pI_Constraint_Error_IllegalType;
                return 0;
        }
    }

    pI_str get_string_from_symbol(struct _pI_CP_Symbol* symbol)
    {
        switch (symbol->value_type) {
            case _pI_str:
                return symbol->data.str_val;

                /* don't want numbers being auto-converted into strings. */
            case _pI_int:
            case _pI_bool:
            case _pI_double:
            default:
                SPRINTF(evalStack_errMsg, PI_STACK_EVAL_ERRMSG_BUF_SIZE, "Implicit cast to string not supported for type %d.",
                          symbol->value_type);
                evalStack_err = pI_Constraint_Error_IllegalType;
                return 0;
        }
    }


    pI_int op_on_implicit_casted_symbols(enum _pI_CP_SymbolCode op_code,
                                         struct _pI_CP_Symbol* left,
                                         struct _pI_CP_Symbol* right)
    {
        enum _pI_IntrinsicType type_code;

        /* the only case when to use the right operand's type is
           when left is primitive and right is var */
        if ( (left->code != ps_VAR) && (right->code == ps_VAR)) {
            type_code = right->value_type;
        } else {
            type_code = left->value_type;
        }

        switch (type_code) {
            case _pI_int:
            case _pI_bool:
                return op_int(op_code, get_int_from_symbol(left), get_int_from_symbol(right));
            case _pI_double:
                return op_double(op_code, get_double_from_symbol(left), get_double_from_symbol(right));
            case _pI_str:
                return op_string(op_code, get_string_from_symbol(left), get_string_from_symbol(right));
        }

        /* if you come here then a symbol is processed which is not a primitiv
           should not happen */
        SPRINTF(evalStack_errMsg, PI_STACK_EVAL_ERRMSG_BUF_SIZE, "Illegal type %d", type_code);
        evalStack_err = pI_Constraint_Error_IllegalType;
        return 0;
    }

    pI_int retrieveBoolean(struct _pI_CP_SymbolStack* stack, struct _pI_CP_Symbol** * tos, struct _Argument* arg)
    {

        struct _pI_CP_Symbol* symbol;
        struct _pI_CP_Symbol right, left;

        if (*tos == 0) {
            SPRINTF(evalStack_errMsg, PI_STACK_EVAL_ERRMSG_BUF_SIZE, "%s", "Stack underflow");
            evalStack_err = pI_Constraint_Error_StackUnderflow;
            return 0;
        }

        symbol = stack->pop_traverse(stack, tos);

        switch (symbol->code) {
            case ps_VAL:
                return symbol->data.bool_val;
            case ps_EQ:
            case ps_NEQ:
            case ps_GT:
            case ps_GEQ:
            case ps_LT:
            case ps_LEQ: {
                int op_res;

                /* get two numeric vals */
                retrieveValue(stack, tos, arg, &right);

                if (evalStack_err) {
                    return 0;
                }

                retrieveValue(stack, tos, arg, &left);

                if (evalStack_err) {
                    return 0;
                }


                op_res = op_on_implicit_casted_symbols(symbol->code, &left, &right);

                if (evalStack_err) {
                    return 0;
                }

                return op_res;
            }
            case ps_AND:
            case ps_OR: {
                int right, left;

                right = retrieveBoolean(stack, tos, arg);

                if (evalStack_err) {
                    return 0;
                }

                left = retrieveBoolean(stack, tos, arg);

                if (evalStack_err) {
                    return 0;
                }

                switch (symbol->code) {
                    case ps_AND:
                        return left && right;
                    case ps_OR:
                        return left || right;
                    default:
                        /* never reached */
                        SPRINTF(evalStack_errMsg, PI_STACK_EVAL_ERRMSG_BUF_SIZE, "Illegal symbol code: %d", symbol->code);
                        evalStack_err = -42;
                        return 0;
                }
            }
            case ps_NOT: {
                int expr;

                expr = retrieveBoolean(stack, tos, arg);

                if (evalStack_err) {
                    return 0;
                }

                return !expr;
            }
            default:
                SPRINTF(evalStack_errMsg, PI_STACK_EVAL_ERRMSG_BUF_SIZE, "%s", "Expression expected.");
                evalStack_err = pI_Constraint_Error_ExpectedExpression;
                return 0;
        }
    }

    pI_int retrieveValue(struct _pI_CP_SymbolStack* stack,
                         struct _pI_CP_Symbol** * tos,
                         struct _Argument* arg,
                         struct _pI_CP_Symbol* result)
    {

        struct _pI_CP_Symbol* symbol;

        if (*tos == 0) {
            SPRINTF(evalStack_errMsg, PI_STACK_EVAL_ERRMSG_BUF_SIZE, "%s", "Stack underflow");
            evalStack_err = pI_Constraint_Error_StackUnderflow;
            return 0;
        }

        /* gets the next symbol from the stack */
        symbol = stack->pop_traverse(stack, tos);


        /* if the symbol is a primitive type we transfer the symbol into the result */
        if (symbol->code == ps_VAL) {
            result->code = symbol->code;
            result->value_type = symbol->value_type;

            switch (symbol->value_type) {
                case _pI_int:
                    result->data.int_val = symbol->data.int_val;
                    break;
                case _pI_bool:
                    result->data.bool_val = symbol->data.bool_val;
                    break;
                case _pI_double:
                    result->data.double_val = symbol->data.double_val;
                    break;
                case _pI_str:
                    result->data.str_val = symbol->data.str_val;
                    break;
                default:
                    SPRINTF(evalStack_errMsg, PI_STACK_EVAL_ERRMSG_BUF_SIZE, "Unknown value type: %d", symbol->value_type);
                    evalStack_err = pI_Constraint_Error_IllegalType;
                    return 0;
            }
        } else if (symbol->code == ps_VAR) {
            /* this block handles the lookup of a variable
               and produces a typed result depending on the id's type
               ATTENTION: only intrinsic types are supported */
            enum _pIDataTypeClass type_class;
            pI_int type_index;
            pI_byte* symbol_data;
            char* type_name;

            symbol_data = CRuntime_FetchArgumentSetupSymbol(arg->runtime, arg,
                          symbol->data.str_val, &type_class, &type_index);

            if (symbol_data == 0) {
                SPRINTF(evalStack_errMsg, PI_STACK_EVAL_ERRMSG_BUF_SIZE, "Unknown variable: %s",  symbol->data.str_val);
                evalStack_err = pI_Constraint_Error_UnknownVariable;
                return 0;
            }

            /* only intrinsic types are supported */
            if (type_class != INTRINSIC_CLASS) {
                SPRINTF(evalStack_errMsg, PI_STACK_EVAL_ERRMSG_BUF_SIZE, "%s", "Given type is not intrinsic.");
                evalStack_err = pI_Constraint_Error_IllegalType;
                return 0;
            }

            type_name = (char*) pI_IntrinsicNames[type_index];

            /* extract a typed var type symbol including the value */
            result->code = ps_VAR;

            if (strcmp(type_name, "pI_double") == 0) {
                result->value_type = _pI_double;
                result->data.double_val = (pI_double) * symbol_data;
            } else if (strcmp(type_name, "pI_int") == 0) {
                result->value_type = _pI_int;
                result->data.int_val = (pI_int) * symbol_data;
            } else if (strcmp(type_name, "pI_bool") == 0) {
                result->value_type = _pI_bool;
                result->data.int_val = (pI_int) * symbol_data;
            } else if (strcmp(type_name, "pI_char") == 0) {
                result->value_type = _pI_int;
                result->data.int_val = (pI_int) * symbol_data;
            } else if (strcmp(type_name, "pI_str") == 0) {
                result->value_type = _pI_str;
                result->data.str_val = (pI_str) * symbol_data;
            } else if (strcmp(type_name, "pI_byte") == 0) {
                result->value_type = _pI_int;
                result->data.int_val = (pI_int) * symbol_data;
                /* TODO: support unsigned int format */
            } else if (strcmp(type_name, "pI_unsigned") == 0) {
                result->value_type = _pI_int;
                result->data.int_val = (pI_int) * symbol_data;
            } else if (strcmp(type_name, "pI_size") == 0) {
                result->value_type = _pI_int;
                result->data.int_val = (pI_int) * symbol_data;
            } else {
                SPRINTF(evalStack_errMsg, PI_STACK_EVAL_ERRMSG_BUF_SIZE, "Unknown pI type name: %s", type_name);
                evalStack_err = pI_Constraint_Error_IllegalType;
                return 0;
            }
        } else {
            SPRINTF(evalStack_errMsg, PI_STACK_EVAL_ERRMSG_BUF_SIZE, "%s", "Value or Variable symbol expected");
            evalStack_err = pI_Constraint_Error_ExpectedValue;
            return pI_Constraint_Error_ExpectedValue;
        }

        return 0;

    }

#undef PI_STACK_EVAL_DOUBLE_COMPARE_EPSILON
#undef PI_STACK_EVAL_ERRMSG_BUF_SIZE

#ifdef __cplusplus
}
#endif
