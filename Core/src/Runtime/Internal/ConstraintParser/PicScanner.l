/* PicScanner.l
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

%{
# include <stdlib.h>
# include <string.h>
# include <PureImage.h>
# include <Runtime/Internal/ConstraintParser/ParserSymbols.h>
# include <Runtime/Internal/ConstraintParser/PicParser.h>

/* For automatic EOF tokens when end of string is reached; */
/* important: otherwise impl of yywrap missing */


/* Always work on buffer not using std::cin; */
/* important: if not defined there is a call to missing function isatty (?) */
#define YY_NEVER_INTERACTIVE 1

/* not necessary... */
/* #define yyterminate() return token::END */

/* Defines the signature of the yylex method */
/* Remark: yylval and yylloc are bison-default */
/*         &driver is configured by "%lex-param" in pic_parser.yy */
# define YY_DECL                                        \
    int yylex (YYSTYPE* yylval,      \
                YYLTYPE* yylloc, struct _CRuntime* runtime)

#ifdef __GNUC__
#define SPRINTF(buf, maxlen, pat, a, b) snprintf(buf, maxlen, pat, a, b)
#else
#define SPRINTF(buf, maxlen, pat, a, b) sprintf_s(buf, maxlen, pat, a, b)
#endif

%}

/* Some definitions for scanner that can be used within rules */
ID    [a-zA-Z][a-zA-Z_0-9]*
DIGIT   [0-9]
BLANK [ \t]

/* THE RULES: */
%%

{BLANK}+   yylloc->first_column++;
[\n]+      yylloc->first_line++;

"||"    yylloc->first_column+= 2; return OR;
"&&"    yylloc->first_column+= 2; return AND;
"=="    yylloc->first_column+= 2; return EQ;
"!="    yylloc->first_column+= 2; return NEQ;
"<="    yylloc->first_column+= 2; return LEQ;
"<"     yylloc->first_column+= 1; return LT;
">="    yylloc->first_column+= 2; return GEQ;
">"     yylloc->first_column+= 1; return GT;
"!"     yylloc->first_column+= 1; return NOT;
"("     yylloc->first_column+= 1; return OPEN_PAREN;
")"     yylloc->first_column+= 1; return CLOSE_PAREN;

([1-9]{DIGIT}*"."{DIGIT}+)|([1-9]{DIGIT}*("."{DIGIT}+)?E("+"|"-")[1-9]{DIGIT}*) {
            yylval->double_val = atof(yytext);
            yylloc->first_column += strlen(yytext);
            return DOUBLE;
}

[1-9]{DIGIT}* {
            yylval->int_val = atoi(yytext);
            yylloc->first_column += strlen(yytext);
            return INTEGER;
}

([Ff]alse|FALSE) {
        yylval->bool_val = 0;
        yylloc->first_column += strlen(yytext);
        return FALSE;
}

['"]([Ff]alse|FALSE)['"] {
        yylval->bool_val = 0;
        yylloc->first_column += strlen(yytext);
        return FALSE;
}

([Tt]rue|TRUE) {
        yylval->bool_val = 1;
        yylloc->first_column += strlen(yytext);
        return TRUE;
}

['"]([Tt]rue|TRUE)['"] {
        yylval->bool_val = 1;
        yylloc->first_column += strlen(yytext);
        return TRUE;
}

['][^']*[']|["][^"]*["] {
            /* Strip the leading and trailing quotes */
            int len = strlen(yytext);
            yylval->str_val = runtime->CopyStringN(runtime, yytext+1,len-2);
            yylloc->first_column += strlen(yytext);
            return STRING;
}

{ID}("."{ID})*  {
            yylval->str_val = runtime->CopyString(runtime, yytext);
            yylloc->first_column += strlen(yytext);
            return IDENTIFIER;
}

.          {
        char buf[250];
        SPRINTF(buf, 250, "Unknown token: %s at pos %d", yytext, yylloc->first_column);
        yyerror( buf );

        yylloc->first_column++;

        return ERROR;
    }

%%

int yywrap() {
    return 1;
}
