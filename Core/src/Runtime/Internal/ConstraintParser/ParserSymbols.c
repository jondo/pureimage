/* ParserSymbols.c
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <PureImage.h>

#include <Runtime/Internal/ConstraintParser/ParserSymbols.h>


struct _pI_CP_Symbol* pI_CP_CreateSymbol(struct _CRuntime* runtime) {
    return (struct _pI_CP_Symbol*) runtime->AllocateMemory(runtime, sizeof(struct _pI_CP_Symbol));
}

struct _pI_CP_Symbol* pI_CP_CreateBoolSymbol(struct _CRuntime* runtime, pI_bool bool_val) {
    struct _pI_CP_Symbol* s;
    s = pI_CP_CreateSymbol(runtime);
    s->code = ps_VAL;
    s->value_type = _pI_bool;
    s->data.bool_val = bool_val;
    return s;
}

struct _pI_CP_Symbol* pI_CP_CreateIntSymbol(struct _CRuntime* runtime, pI_int int_val) {
    struct _pI_CP_Symbol* s;
    s = pI_CP_CreateSymbol(runtime);
    s->code = ps_VAL;
    s->value_type = _pI_int;
    s->data.int_val = int_val;
    return s;
}

struct _pI_CP_Symbol* pI_CP_CreateDoubleSymbol(struct _CRuntime* runtime, pI_double double_val) {
    struct _pI_CP_Symbol* s;
    s = pI_CP_CreateSymbol(runtime);
    s->code = ps_VAL;
    s->value_type = _pI_double;
    s->data.double_val = double_val;
    return s;
}

struct _pI_CP_Symbol* pI_CP_CreateStringSymbol(struct _CRuntime* runtime, pI_str str_val) {
    struct _pI_CP_Symbol* s;
    s = pI_CP_CreateSymbol(runtime);
    s->code = ps_VAL;
    s->value_type = _pI_str;
    s->data.str_val = str_val;
    return s;
}

struct _pI_CP_Symbol* pI_CP_CreateVariableSymbol(struct _CRuntime* runtime, pI_str id) {
    struct _pI_CP_Symbol* s;
    s = pI_CP_CreateSymbol(runtime);
    s->code = ps_VAR;
    s->data.str_val = id;
    return s;
}

struct _pI_CP_Symbol* pI_CP_CreateOperatorSymbol(struct _CRuntime* runtime, enum _pI_CP_SymbolCode opCode, pI_int arg_count) {
    struct _pI_CP_Symbol* s;
    s = pI_CP_CreateSymbol(runtime);
    s->code = opCode;
    s->value_type = _pI_int;
    s->data.int_val = arg_count;
    return s;
}

struct _pI_CP_Symbol* pI_CP_CreateErrorSymbol(struct _CRuntime* runtime, pI_str msg) {
    struct _pI_CP_Symbol* s;
    s = pI_CP_CreateSymbol(runtime);
    s->code = ps_ERROR;
    s->value_type = _pI_str;
    s->data.str_val = msg;
    return s;
}

void pI_CP_FreeParserSymbol(struct _CRuntime* runtime, struct _pI_CP_Symbol* symbol)
{

    if (symbol->code == ps_VAR) {
        runtime->FreeString(runtime, symbol->data.str_val);
    }

    free (symbol);
}
