/* SymbolStack.c
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <memory.h>

#include <Runtime/Internal/ConstraintParser/SymbolStack.h>

pI_int SymbolStack_push(struct _pI_CP_SymbolStack* stack, struct _pI_CP_Symbol* symbol)
{

    if (stack->top == stack->stack + PIC_STACK_MAXSIZE) {
        return -1;
    }

    if (stack->top == 0) {
        stack->top = stack->stack;
    } else {
        stack->top++;
    }

    *(stack->top) = symbol;

    return 0;
}

struct _pI_CP_Symbol* SymbolStack_pop_traverse(struct _pI_CP_SymbolStack* stack, struct _pI_CP_Symbol** * top) {

    struct _pI_CP_Symbol* symbol;
    symbol = **top;

    if (stack->stack == *top) {
        /* stack is empty now; set top to zero */
        *top = 0;
    } else {
        (*top)--;
    }

    return symbol;
}

struct _pI_CP_Symbol* SymbolStack_pop(struct _pI_CP_SymbolStack* stack) {
    return SymbolStack_pop_traverse(stack, &stack->top);
}


void pI_CP_InitStack(struct _pI_CP_SymbolStack* stack)
{
    memset(stack->stack, 0, PIC_STACK_MAXSIZE);

    stack->top = 0;
    stack->push = SymbolStack_push;
    stack->pop_traverse = SymbolStack_pop_traverse;
}

void pI_CP_FreeSymbolStack(struct _CRuntime* runtime, struct _pI_CP_SymbolStack* symbol_stack)
{
    struct _pI_CP_Symbol** next;
    pI_unsigned idx;

    next = symbol_stack->top;

    for (idx = 0; idx < PIC_STACK_MAXSIZE; idx++) {
        pI_CP_FreeParserSymbol(runtime, *next);

        if ( next == symbol_stack->stack  ) {
            break;
        }

        next--;
    }
}
