/* ArgumentSerializationJSON.c
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <string.h>
#include <stdio.h>
#include <json.h>

#include <Runtime/Internal/S11N/JSON/ArgumentSerializationJSON.h>
#include <Runtime/Internal/RuntimeHelperFunctions.h>

#include <Runtime/Internal/DataTypes/Intrinsics/IntrinsicRuntimeFunctions.h>
#include <Runtime/Internal/DataTypes/Intrinsics/IntrinsicSizes.h>
#include <Runtime/Internal/DataTypes/Structures/StructureSizes.h>
#include <Runtime/Internal/DataTypes/Arguments/ArgumentSizes.h>

#define PI_SN_JSON_API_VERSION_STR                  "pIVersion"

#define PI_SN_JSON_TYPE_STR                         "type"
#define PI_SN_JSON_CLASS                            "typeClass"
#define PI_SN_JSON_CLASS_INDEX                      "subClassId"
#define PI_SN_JSON_CLASS_INDEX_NAME                 "subClassName"
#define PI_SN_JSON_INTRINSIC_TYPE_STR               "intrinsic"
#define PI_SN_JSON_STRUCTURE_TYPE_STR               "structure"
#define PI_SN_JSON_ARGUMENT_TYPE_STR                "argument"

#define PI_SN_JSON_ARGUMENT_DESCRIPTION_STR         "argumentDescription"
#define PI_SN_JSON_ARGUMENT_DESCRIPTION_NAME        "name"
#define PI_SN_JSON_ARGUMENT_DESCRIPTION_DESC        "description"

#define PI_SN_JSON_ARGUMENT_SIGNATURE_STR           "argumentSignature"
#define PI_SN_JSON_ARGUMENT_SIGNATURE_TYPE_ID       "typeId"
#define PI_SN_JSON_ARGUMENT_SIGNATURE_TYPE_STR      "type"
#define PI_SN_JSON_ARGUMENT_SIGNATURE_RO            "readOnly"
#define PI_SN_JSON_ARGUMENT_SIGNATURE_CONSTRAINTS   "constraints"

#define PI_SN_JSON_ARGUMENT_SETUP_STR               "setup"
#define PI_SN_JSON_ARGUMENT_DATA_STR                "values"

#define PI_SN_JSON_ARGUMENT_VALUE_STR               "value"

#define PI_SN_JSON_EMPTY_STRING                     ""

#define PI_SN_JSON_ARGUMENT_COUNT                   "arguments"
#define PI_SN_JSON_ARGUMENT_LIST                    "list"

    /* fwd. declarations */
    pI_bool RecursiveBuildJSONObjWithArgument (
        struct _CRuntime* runtime,
        struct _Argument* arg,
        struct json_object* jobj);

    pI_bool RecursiveBuildArgumentWithJSONObj (
        struct _CRuntime* runtime,
        struct _Argument* arg,
        struct json_object* jobj);

    pI_bool AddTypeInfo (
        struct json_object* jobj,
        enum _pIDataTypeClass type,
        pI_int index)
    {

        struct json_object* datatype;

        if ((jobj == 0) || (type == UNKNOWN_CLASS)) {
            return pI_FALSE;
        }

        datatype = json_object_new_object();

        if (datatype == 0) {
            return pI_FALSE;
        }

        switch (type) {
            case INTRINSIC_CLASS:
                json_object_object_add (
                    datatype,
                    PI_SN_JSON_CLASS,
                    json_object_new_string (PI_SN_JSON_INTRINSIC_TYPE_STR));
                json_object_object_add (
                    datatype,
                    PI_SN_JSON_CLASS_INDEX,
                    json_object_new_int ((int) index));
                json_object_object_add (
                    datatype,
                    PI_SN_JSON_CLASS_INDEX_NAME,
                    json_object_new_string ((char* ) pI_IntrinsicNames[index]));
                break;
            case ARGUMENT_CLASS:
                json_object_object_add (
                    datatype,
                    PI_SN_JSON_CLASS,
                    json_object_new_string (PI_SN_JSON_ARGUMENT_TYPE_STR));
                json_object_object_add (
                    datatype,
                    PI_SN_JSON_CLASS_INDEX,
                    json_object_new_int ((int) index));
                json_object_object_add (
                    datatype,
                    PI_SN_JSON_CLASS_INDEX_NAME,
                    json_object_new_string ((char* ) pI_ArgumentNames[index]));
                break;
            case STRUCTURE_CLASS:
                json_object_object_add (
                    datatype,
                    PI_SN_JSON_CLASS,
                    json_object_new_string (PI_SN_JSON_STRUCTURE_TYPE_STR));
                json_object_object_add (
                    datatype,
                    PI_SN_JSON_CLASS_INDEX,
                    json_object_new_int ((int) index));
                json_object_object_add (
                    datatype,
                    PI_SN_JSON_CLASS_INDEX_NAME,
                    json_object_new_string ((char* ) pI_StructureNames[index]));
                break;
            default:
                json_object_put (datatype);
                return pI_FALSE;
        } /* switch (type) */

        json_object_object_add (jobj, PI_SN_JSON_TYPE_STR, datatype);
        return pI_TRUE;
    } /* pI_bool AddTypeInfo (...) */

    pI_bool GetTypeInfo (
        struct json_object* jobj,
        enum _pIDataTypeClass* type,
        pI_int* index)
    {

        struct json_object* datatype;
        struct json_object* subnode;
        char* str;

        if (jobj == 0) {
            return pI_FALSE;
        }

        datatype = json_object_object_get (jobj, PI_SN_JSON_TYPE_STR);

        if (datatype == 0) {
            return pI_FALSE;
        }

        subnode = json_object_object_get (datatype, PI_SN_JSON_CLASS);

        if ((subnode != 0) && (type != 0)) {
            str = (char* ) json_object_get_string (subnode);

            if (strcmp (str, PI_SN_JSON_INTRINSIC_TYPE_STR) == 0) {
                *type = INTRINSIC_CLASS;
            } else if (strcmp (str, PI_SN_JSON_ARGUMENT_TYPE_STR) == 0) {
                *type = ARGUMENT_CLASS;
            } else if (strcmp (str, PI_SN_JSON_STRUCTURE_TYPE_STR) == 0) {
                *type = STRUCTURE_CLASS;
            } else {
                *type = UNKNOWN_CLASS;
            }
        } /* if ((subnode != 0) && (type != 0)) */

        subnode = json_object_object_get (datatype, PI_SN_JSON_CLASS_INDEX);

        if ((subnode != 0) && (index != 0)) {
            *index = json_object_get_int (subnode);
        }

        return pI_TRUE;
    } /* pI_bool GetTypeInfo (...) */

    pI_bool AddArgumentMetaInfo (struct json_object* jobj, struct _Argument* arg)
    {

        struct json_object* description;
        struct json_object* signature;

        if ((jobj == 0) || (arg == 0)) {
            return pI_FALSE;
        }

        description = json_object_new_object();

        json_object_object_add (
            description,
            PI_SN_JSON_ARGUMENT_DESCRIPTION_NAME,
            json_object_new_string (
                (char* ) (arg->description.name != 0 ? arg->description.name : PI_SN_JSON_EMPTY_STRING)));
        json_object_object_add (
            description,
            PI_SN_JSON_ARGUMENT_DESCRIPTION_DESC,
            json_object_new_string (
                (char* ) (arg->description.description != 0 ? arg->description.description : PI_SN_JSON_EMPTY_STRING)));
        json_object_object_add (jobj, PI_SN_JSON_ARGUMENT_DESCRIPTION_STR, description);

        signature = json_object_new_object();
        json_object_object_add (
            signature,
            PI_SN_JSON_ARGUMENT_SIGNATURE_TYPE_ID,
            json_object_new_int ((int) arg->signature.type));
        json_object_object_add (
            signature,
            PI_SN_JSON_ARGUMENT_SIGNATURE_TYPE_STR,
            json_object_new_string (pI_ArgumentNames[arg->signature.type]));
        json_object_object_add (
            signature,
            PI_SN_JSON_ARGUMENT_SIGNATURE_RO,
            json_object_new_boolean ((boolean) arg->signature.readonly));
        json_object_object_add (
            signature,
            PI_SN_JSON_ARGUMENT_SIGNATURE_CONSTRAINTS,
            json_object_new_string ((char* ) (arg->signature.constraints != 0 ? arg->signature.constraints : PI_SN_JSON_EMPTY_STRING)));
        json_object_object_add (jobj, PI_SN_JSON_ARGUMENT_SIGNATURE_STR, signature);

        return pI_TRUE;
    } /* pI_bool AddArgumentMetaInfo (struct json_object* jobj, struct _Argument* arg) */

    pI_bool GetArgumentMetaInfo (struct json_object* jobj, struct _Argument* arg)
    {

        struct json_object* datatype;
        struct json_object* subnode;
        pI_str str;

        if (jobj == 0) {
            return pI_FALSE;
        }

        datatype = json_object_object_get (jobj, PI_SN_JSON_ARGUMENT_DESCRIPTION_STR);

        if (datatype == 0) {
            return pI_FALSE;
        }

        subnode = json_object_object_get (datatype, PI_SN_JSON_ARGUMENT_DESCRIPTION_NAME);

        if (subnode != 0) {
            str = (pI_str) json_object_get_string (subnode);
            arg->runtime->FreeString (arg->runtime, arg->description.name);
            arg->description.name = arg->runtime->CopyString (arg->runtime, (pI_str) str);
        }

        subnode = json_object_object_get (datatype, PI_SN_JSON_ARGUMENT_DESCRIPTION_DESC);

        if (subnode != 0) {
            str = (pI_str) json_object_get_string (subnode);
            arg->runtime->FreeString (arg->runtime, arg->description.description);
            arg->description.description = arg->runtime->CopyString (arg->runtime, (pI_str) str);
        }

        datatype = json_object_object_get (jobj, PI_SN_JSON_ARGUMENT_SIGNATURE_STR);

        if (datatype == 0) {
            return pI_FALSE;
        }

        subnode = json_object_object_get (datatype, PI_SN_JSON_ARGUMENT_SIGNATURE_TYPE_ID);

        if (subnode != 0) {
            arg->signature.type = (enum _pI_ArgumentType) json_object_get_int (subnode);
        }

        subnode = json_object_object_get (datatype, PI_SN_JSON_ARGUMENT_SIGNATURE_RO);

        if (subnode != 0) {
            arg->signature.readonly = (pI_bool) json_object_get_boolean (subnode);
        }

        subnode = json_object_object_get (datatype, PI_SN_JSON_ARGUMENT_SIGNATURE_CONSTRAINTS);

        if (subnode != 0) {
            str = (pI_str) json_object_get_string (subnode);
            arg->runtime->FreeString (arg->runtime, arg->signature.constraints);
            arg->signature.constraints = arg->runtime->CopyString (arg->runtime, (pI_str) str);
        }

        return pI_TRUE;
    } /* pI_bool GetArgumentMetaInfo (struct json_object* jobj, struct _Argument* arg) */

    pI_bool AddFlatStructure (
        struct json_object* jobj,
        pI_str node_name,
        struct _CRuntime* runtime,
        pI_byte* data_ptr,
        pI_int symbol_sizes[],
        pI_char** * symbol_types,
        pI_char** * symbol_names,
        pI_int index,
        struct _Argument* argument,
        pI_bool add_to_array,
        pI_int field_index,
        pI_bool add_to_structure)
    {

        pI_int lv, type_index, type_size;
        enum _pIDataTypeClass type_class;
        pI_bool retval = pI_TRUE;
        struct json_object* jnode = 0;
        struct json_object* subnode = 0;
        struct json_object* typenode = 0;
        pI_str str;
        pI_size str_len;

        if ((jobj == 0) || (argument == 0)) {
            return pI_FALSE;
        }

        if (add_to_array == pI_FALSE) {
            jnode = json_object_new_object();
        }

        for (lv = field_index >= 0 ? field_index : 0; field_index >= 0 ? lv == field_index : lv < symbol_sizes[index]; ++lv) {
            /* fetch type of actual member */
            type_class = CRuntime_GetDataTypeClass (runtime, symbol_types[index][lv], &type_index);

            if (add_to_array == pI_FALSE) {
                subnode = json_object_new_object();
                AddTypeInfo (subnode, type_class, type_index);
            }

            /* add json object, depending on type */
            if (type_class == INTRINSIC_CLASS) {
                type_size = pI_IntrinsicSizes[type_index];
                str =
                    CRuntime_SerializeIntrinsicDataType (
                        runtime,
                        data_ptr,
                        type_index,
                        &str_len);
                typenode = json_object_new_string ((char* ) (str != 0 ? str : PI_SN_JSON_EMPTY_STRING));
                runtime->FreeString (runtime, str);

                if (add_to_structure == pI_TRUE) {
                    json_object_object_add (jobj, symbol_names[index][lv], typenode);
                } else if (add_to_array == pI_FALSE) {
                    json_object_object_add (subnode, PI_SN_JSON_ARGUMENT_VALUE_STR, typenode);
                } else {
                    json_object_array_add (jobj, typenode);
                }
            } else if (type_class == ARGUMENT_CLASS) {
                type_size = sizeof(struct _Argument);
                typenode = json_object_new_object();
                RecursiveBuildJSONObjWithArgument (runtime, (struct _Argument* ) data_ptr, typenode);

                if (add_to_structure == pI_TRUE) {
                    json_object_object_add (jobj, symbol_names[index][lv], typenode);
                } else if (add_to_array == pI_FALSE) {
                    json_object_object_add (subnode, (char* ) symbol_types[index][lv], typenode);
                } else {
                    json_object_array_add (jobj, typenode);
                }

            } else if (type_class == STRUCTURE_CLASS) {
                type_size = pI_StructureSizes[type_index];
                typenode = json_object_new_object();
                AddFlatStructure (
                    typenode,
                    symbol_types[index][lv],
                    runtime,
                    data_ptr,
                    pI_StructureSymbolSizes,
                    pI_StructureSymbolTypes,
                    pI_StructureSymbols,
                    type_index,
                    argument,
                    add_to_array,
                    -1,
                    pI_TRUE);

                if (add_to_structure == pI_TRUE) {
                    json_object_object_add (jobj, symbol_names[index][lv], typenode);
                } else if (add_to_array == pI_FALSE) {
                    json_object_object_add (subnode, (char* ) symbol_types[index][lv], typenode);
                } else {
                    json_object_array_add (jobj, typenode);
                }
            } else {
                /* unknown type; should never happen; for now, leave by returning false */
                return pI_FALSE;
            }

            if (add_to_array == pI_FALSE) {
                json_object_object_add (jnode, (char* ) symbol_names[index][lv], subnode);
            }

            data_ptr += type_size;

            /* enforce proper alignment */
            if ((type_size % PI_STRUCTURE_ALIGNMENT) != 0) {
                data_ptr += PI_STRUCTURE_ALIGNMENT - type_size % PI_STRUCTURE_ALIGNMENT;
            }
        } /* for (lv = 0; lv < arg_structure_symbol_sizes[type]; ++lv) */

        if (add_to_array == pI_FALSE) {
            json_object_object_add (jobj, (char* ) node_name, jnode);
        }

        return retval;
    } /* pI_bool AddFlatStructure (...) */

    pI_bool GetFlatStructure_1 (
        struct json_object* jobj,
        pI_str node_name,
        struct _CRuntime* runtime,
        pI_byte* data_ptr,
        pI_int symbol_sizes[],
        pI_char** * symbol_types,
        pI_char** * symbol_names,
        pI_int index,
        struct _Argument* argument,
        pI_int field_index,
        pI_bool from_array,
        pI_bool from_structure)
    {

        pI_int lv, type_index, type_size;
        enum _pIDataTypeClass type_class;
        pI_bool retval = pI_TRUE;
        struct json_object* jnode = 0;
        struct json_object* subnode = 0;
        struct json_object* typenode = 0;
        pI_str str;

        if ((jobj == 0) || (argument == 0)) {
            return pI_FALSE;
        }

        if (from_array == pI_FALSE) {
            jnode = json_object_object_get (jobj, node_name);
        }

        for (lv = field_index >= 0 ? field_index : 0; field_index >= 0 ? lv == field_index : lv < symbol_sizes[index]; ++lv) {
            if (from_array == pI_TRUE) {
                type_class = CRuntime_GetDataTypeClass (runtime, symbol_types[index][lv], &type_index);
            } else {
                subnode = json_object_object_get (jnode, (char* ) symbol_names[index][lv]);
                GetTypeInfo (subnode, &type_class, &type_index);
            }

            /* add json object, depending on type */
            if (type_class == INTRINSIC_CLASS) {
                type_size = pI_IntrinsicSizes[type_index];

                if (from_structure == pI_TRUE) {
                    typenode = json_object_object_get (jobj, symbol_names[index][lv]);
                    str = (pI_str) json_object_get_string (typenode);
                } else if (from_array == pI_FALSE) {
                    typenode = json_object_object_get (subnode, PI_SN_JSON_ARGUMENT_VALUE_STR);
                    str = (pI_str) json_object_get_string (typenode);
                } else {
                    str = (pI_str) json_object_get_string (jobj);
                }

                CRuntime_DeserializeIntrinsicDataType (
                    runtime,
                    data_ptr,
                    type_index,
                    str);
            } else if (type_class == ARGUMENT_CLASS) {
                type_size = sizeof(struct _Argument);

                if (from_structure == pI_TRUE) {
                    typenode = json_object_object_get (jobj, symbol_names[index][lv]);
                    str = (pI_str) json_object_get_string (typenode);
                } else if (from_array == pI_FALSE) {
                    typenode = json_object_object_get (subnode, (char* ) symbol_types[index][lv]);
                }

                RecursiveBuildArgumentWithJSONObj (runtime, (struct _Argument* ) data_ptr, typenode != 0 ? typenode : jobj);
            } else if (type_class == STRUCTURE_CLASS) {
                type_size = pI_StructureSizes[type_index];

                if (from_structure == pI_TRUE) {
                    typenode = json_object_object_get (jobj, symbol_names[index][lv]);
                    GetFlatStructure_1 (
                        typenode,
                        symbol_types[index][lv],
                        runtime,
                        data_ptr,
                        pI_StructureSymbolSizes,
                        pI_StructureSymbolTypes,
                        pI_ArgumentSetupSymbols,
                        type_index,
                        argument,
                        -1,
                        from_array,
                        pI_FALSE);
                } else
                    GetFlatStructure_1 (
                        subnode,
                        symbol_types[index][lv],
                        runtime,
                        data_ptr,
                        pI_StructureSymbolSizes,
                        pI_StructureSymbolTypes,
                        pI_ArgumentSetupSymbols,
                        type_index,
                        argument,
                        -1,
                        from_array,
                        pI_FALSE);
            } else {
                /* unknown type; should never happen; for now, leave by returning false */
                return pI_FALSE;
            }

            data_ptr += type_size;

            /* enforce proper alignment */
            if ((type_size % PI_STRUCTURE_ALIGNMENT) != 0) {
                data_ptr += PI_STRUCTURE_ALIGNMENT - type_size % PI_STRUCTURE_ALIGNMENT;
            }
        } /* for (lv = 0; lv < arg_structure_symbol_sizes[type]; ++lv) */

        return retval;
    } /* pI_bool GetFlatStructure_1 (...) */

    pI_bool GetFlatStructure_N (
        struct json_object* jobj,
        pI_str node_name,
        struct _CRuntime* runtime,
        pI_byte* data_ptr,
        pI_int symbol_sizes[],
        pI_char** * symbol_types,
        pI_char** * symbol_names,
        pI_int index,
        struct _Argument* argument,
        enum _pIDataTypeClass type_class,
        pI_int type_index,
        pI_int field_index)
    {

        pI_int lv, type_size;
        pI_bool retval = pI_TRUE;
        struct json_object* jnode = 0;
        pI_str str;

        if ((jobj == 0) || (argument == 0)) {
            return pI_FALSE;
        }

        for (lv = field_index >= 0 ? field_index : 0; field_index >= 0 ? lv == field_index : lv < symbol_sizes[index]; ++lv) {

            /* add json object, depending on type */
            if (type_class == INTRINSIC_CLASS) {
                type_size = pI_IntrinsicSizes[type_index];
                str = (pI_str) json_object_get_string (jobj);
                CRuntime_DeserializeIntrinsicDataType (
                    runtime,
                    data_ptr,
                    type_index,
                    str);
            } else if (type_class == ARGUMENT_CLASS) {
                type_size = sizeof(struct _Argument);
                jnode = json_object_object_get (jobj, (char* ) symbol_types[index][lv]);
                RecursiveBuildArgumentWithJSONObj (runtime, (struct _Argument* ) data_ptr, jnode);
            } else if (type_class == STRUCTURE_CLASS) {
                type_size = pI_StructureSizes[type_index];
                GetFlatStructure_1 (
                    jobj,
                    symbol_types[index][lv],
                    runtime,
                    data_ptr,
                    pI_StructureSymbolSizes,
                    pI_StructureSymbolTypes,
                    pI_StructureSymbols,
                    type_index,
                    argument,
                    -1,
                    pI_TRUE,
                    pI_TRUE);
            } else {
                /* unknown type; should never happen; for now, leave by returning false */
                return pI_FALSE;
            }

            data_ptr += type_size;

            /* enforce proper alignment */
            if ((type_size % PI_STRUCTURE_ALIGNMENT) != 0) {
                data_ptr += PI_STRUCTURE_ALIGNMENT - type_size % PI_STRUCTURE_ALIGNMENT;
            }
        } /* for (lv = 0; lv < arg_structure_symbol_sizes[type]; ++lv) */

        return retval;
    } /* pI_bool GetFlatStructure_N (...) */

    pI_bool AddDataRecursive (
        struct json_object* jobj,
        pI_str node_name,
        struct _CRuntime* runtime,
        pI_int dimensions,
        pI_int max_dimensions,
        pI_byte* data_ptr,
        pIDataTypeClass type_class,
        pI_int type_index,
        enum _pI_ArgumentType arg_type,
        pI_int index,
        struct _Argument* argument,
        pI_bool is_array,
        pI_int field_index)
    {

        pI_int count, ptr_size = sizeof(pI_byte* ), lv, elem_size;
        pI_bool retval = pI_TRUE;
        pI_byte* next_data_ptr;
        pI_byte* data;
        pI_byte* arg_data;
        struct json_object* jnode = 0;
        struct json_object* arrnode = 0;
        pI_str* data_deps = 0;

        if (type_class == INTRINSIC_CLASS) {
            elem_size = pI_IntrinsicSizes[type_index];
        } else if (type_class == ARGUMENT_CLASS) {
            elem_size = sizeof(struct _Argument);
        } else if (type_class == STRUCTURE_CLASS) {
            elem_size = pI_StructureSizes[type_index];
        } else {
            return pI_FALSE;
        }

        if ((dimensions == max_dimensions) && (dimensions > 0)) {
            jnode = json_object_new_object();
            AddTypeInfo (jnode, type_class, type_index);
        }

        if (dimensions != 0) {
            arrnode = json_object_new_array();
        }

        if (dimensions == 0) {
            /* create data in place. */
            retval = AddFlatStructure (
                         jnode == 0 ? jobj : jnode,
                         max_dimensions > 0 ? 0 : node_name,
                         runtime,
                         data_ptr,
                         pI_ArgumentDataSymbolSizes,
                         pI_ArgumentDataSymbolTypes,
                         pI_ArgumentDataSymbols,
                         arg_type,
                         argument,
                         max_dimensions > 0 ? pI_TRUE : pI_FALSE,
                         index,
                         pI_FALSE);

            if (jnode != 0) {
                json_object_object_add (jobj, node_name, jnode);
            }

            return retval;
        } else { /* if (dimensions == 0) */
            /* create data field */
            data_deps = CRuntime_ArgumentSizeDependenciesAsString (
                            runtime,
                            argument,
                            pI_ArgumentDataSymbols[arg_type][index]);
            arg_data = CRuntime_GetArgumentDataBySymbol (
                           runtime,
                           argument,
                           data_deps[max_dimensions - dimensions],
                           pI_TRUE);

            count = arg_data != 0 ? *arg_data : 0;

            data = (pI_byte* ) * ((pI_int* ) data_ptr);

            for (lv = 0; lv < count; ++lv) {
                /* Remark:  all layers with dimension > 1 store pointers only.
                            At dimension == 1 flat structures are stored side by side
                            each having a size of elem_size bytes */
                if (dimensions == 1) {
                    next_data_ptr = data + lv * elem_size;
                } else {
                    next_data_ptr = data + lv * ptr_size;
                }

                retval &= AddDataRecursive (
                              arrnode == 0 ? jobj : arrnode,
                              node_name,
                              runtime,
                              dimensions - 1,
                              max_dimensions,
                              next_data_ptr,
                              type_class,
                              type_index,
                              arg_type,
                              index,
                              argument,
                              arrnode == 0 ? pI_FALSE : pI_TRUE,
                              lv);
            } /* for (lv = 0; lv < count; ++lv) */

            if (arrnode != 0) {
                if (jnode != 0) {
                    json_object_object_add (jnode, node_name, arrnode);
                }

                if (is_array == pI_TRUE) {
                    json_object_array_add (jobj, jnode == 0 ? arrnode : jnode);
                } else {
                    json_object_object_add (jobj, node_name, jnode == 0 ? arrnode : jnode);
                }
            }

            return retval;
        } /* else - if (dimensions == 0) */
    } /* static pI_bool AddDataRecursive (...) */

    pI_bool GetDataRecursive (
        struct json_object* jobj,
        pI_str node_name,
        struct _CRuntime* runtime,
        pI_int dimensions,
        pI_int max_dimensions,
        pI_byte* data_ptr,
        pIDataTypeClass type_class,
        pI_int type_index,
        enum _pI_ArgumentType arg_type,
        pI_int index,
        pI_int arr_index,
        struct _Argument* argument)
    {

        pI_int count, ptr_size = sizeof(pI_byte* ), lv, elem_size;
        pI_bool retval = pI_TRUE;
        pI_byte* next_data_ptr;
        pI_byte* data;
        struct json_object* jnode = 0;
        struct json_object* subnode = 0;
        struct json_object* arrnode = 0;

        if (type_class == INTRINSIC_CLASS) {
            elem_size = pI_IntrinsicSizes[type_index];
        } else if (type_class == ARGUMENT_CLASS) {
            elem_size = sizeof(struct _Argument);
        } else if (type_class == STRUCTURE_CLASS) {
            elem_size = pI_StructureSizes[type_index];
        } else {
            return pI_FALSE;
        }

        if ((dimensions == max_dimensions) && (dimensions > 0)) {
            jnode = json_object_object_get (jobj, node_name);
            GetTypeInfo (jnode, &type_class, &type_index);
        }

        if (dimensions == 0) {
            /* fetch single structure. */
            if (max_dimensions == 0) {
                return GetFlatStructure_1 (
                           jnode == 0 ? jobj : jnode,
                           node_name,
                           runtime,
                           data_ptr,
                           pI_ArgumentDataSymbolSizes,
                           pI_ArgumentDataSymbolTypes,
                           pI_ArgumentDataSymbols,
                           arg_type,
                           argument,
                           index,
                           pI_FALSE,
                           pI_FALSE);
            }

            /* fetch structure out of array compound */
            GetTypeInfo (jnode, &type_class, &type_index);
            retval = GetFlatStructure_N (
                         jnode == 0 ? jobj : jnode,
                         node_name,
                         runtime,
                         data_ptr,
                         pI_ArgumentDataSymbolSizes,
                         pI_ArgumentDataSymbolTypes,
                         pI_ArgumentDataSymbols,
                         arg_type,
                         argument,
                         type_class,
                         type_index,
                         index);
            return retval;
        } else { /* if (dimensions == 0) */
            /* create data field */
            count =
                *CRuntime_GetArgumentDataBySymbol (
                    runtime,
                    argument,
                    CRuntime_ArgumentSizeDependenciesAsString (
                        runtime,
                        argument,
                        pI_ArgumentDataSymbols[arg_type][index])[max_dimensions - dimensions],
                    pI_TRUE);

            data = (pI_byte* ) * ((pI_int* ) data_ptr);

            if (dimensions == max_dimensions) {
                subnode = json_object_object_get (jnode == 0 ? jobj : jnode, node_name);
            } else {
                subnode = jnode == 0 ? jobj : jnode;
            }

            for (lv = 0; lv < count; ++lv) {
                arrnode = json_object_array_get_idx (subnode, lv);

                /* Remark:  all layers with dimension > 1 store pointers only.
                            At dimension == 1 flat structures are stored side by side
                            each having a size of elem_size bytes */
                if (dimensions == 1) {
                    next_data_ptr = data + lv * elem_size;
                } else {
                    next_data_ptr = data + lv * ptr_size;
                }

                retval &= GetDataRecursive (
                              arrnode == 0 ? jobj : arrnode,
                              node_name,
                              runtime,
                              dimensions - 1,
                              max_dimensions,
                              next_data_ptr,
                              type_class,
                              type_index,
                              arg_type,
                              index,
                              lv,
                              argument);
            } /* for (lv = 0; lv < count; ++lv) */

            return retval;
        } /* else - if (dimensions == 0) */
    } /* static pI_bool GetDataRecursive (...) */

    pI_bool RecursiveBuildJSONObjWithArgument (
        struct _CRuntime* runtime,
        struct _Argument* arg,
        struct json_object* jobj)
    {

        pI_int lv, dims;
        pI_bool retval = pI_TRUE;
        enum _pIDataTypeClass type_class;
        pI_int type_index, elem_size;
        enum _pI_ArgumentType type;
        pI_byte* data;

        if ((runtime == 0) || (arg == 0) || (jobj == 0)) {
            return pI_FALSE;
        }

        type = arg->signature.type;
        retval &= AddTypeInfo (jobj, ARGUMENT_CLASS, type);
        retval &= AddArgumentMetaInfo (jobj, arg);
        retval &= AddFlatStructure (
                      jobj,
                      PI_SN_JSON_ARGUMENT_SETUP_STR,
                      runtime,
                      (pI_byte* ) (arg->signature.setup.Empty),
                      pI_ArgumentSetupSymbolSizes,
                      pI_ArgumentSetupSymbolTypes,
                      pI_ArgumentSetupSymbols,
                      type,
                      arg,
                      pI_FALSE,
                      -1,
                      pI_FALSE);

        data = (pI_byte* ) arg->data.Empty;

        /* in case argument has no valid data, return */
        if (data == 0) {
            return retval;
        }

        for (lv = 0; lv < pI_ArgumentDataSymbolSizes[type]; ++lv) {
            dims = pI_ArgumentDataDimensions[type][lv];
            type_class =
                CRuntime_GetDataTypeClass (
                    runtime,
                    pI_ArgumentDataSymbolTypes[type][lv],
                    &type_index);

            retval &= AddDataRecursive (
                          jobj,
                          pI_ArgumentDataSymbols[type][lv],
                          runtime,
                          dims,
                          dims,
                          data,
                          type_class,
                          type_index,
                          type,
                          lv,
                          arg,
                          pI_FALSE,
                          lv);

            if (type_class == INTRINSIC_CLASS) {
                elem_size = pI_IntrinsicSizes[type_index];
            } else if (type_class == ARGUMENT_CLASS) {
                elem_size = sizeof(struct _Argument);
            } else if (type_class == STRUCTURE_CLASS) {
                elem_size = pI_StructureSizes[type_index];
            } else {
                return pI_FALSE;
            }

            data += elem_size;

            /* enforce proper alignment */
            if ((elem_size % PI_STRUCTURE_ALIGNMENT) != 0) {
                data += PI_STRUCTURE_ALIGNMENT - elem_size % PI_STRUCTURE_ALIGNMENT;
            }
        } /* for (lv = 0; lv < pI_ArgumentDataSymbolSizes[type]; ++lv) */

        return retval;
    } /* pI_bool RecursiveBuildJSONObjWithArgument (...) */

    pI_bool RecursiveBuildArgumentWithJSONObj (
        struct _CRuntime* runtime,
        struct _Argument* arg,
        struct json_object* jobj)
    {

        pI_int lv, dims;
        pI_bool retval = pI_TRUE;
        enum _pIDataTypeClass type_class;
        pI_int type_index, data_index;
        enum _pI_ArgumentType type;
        pI_byte* data;
        pI_int elem_size;

        if ((runtime == 0) || (arg == 0) || (jobj == 0)) {
            return pI_FALSE;
        }

        /* 1. retrieve type information */
        retval &= GetTypeInfo (jobj, &type_class, &type_index);
        type = arg->signature.type;

        /* 2. retrieve meta information */
        retval &= GetArgumentMetaInfo (jobj, arg);

        /* 3. retrieve argument signature */
        retval &= runtime->CreateArgumentSetup (runtime, arg);
        retval &= GetFlatStructure_1 (
                      jobj,
                      PI_SN_JSON_ARGUMENT_SETUP_STR,
                      runtime,
                      (pI_byte* ) (arg->signature.setup.Empty),
                      pI_ArgumentSetupSymbolSizes,
                      pI_ArgumentSetupSymbolTypes,
                      pI_ArgumentSetupSymbols,
                      type_index,
                      arg,
                      -1,
                      pI_FALSE,
                      pI_FALSE);

        /* 4. prepare memory for data struct:
               let the runtime allocate the memory; could be opt out (initialization)! */
        runtime->CreateArgumentData (runtime, arg);

        data = (pI_byte* ) arg->data.Empty; /* since a pointer union is contained, this works safely. */
        /* 5. retrieve argument data */
        for (lv = 0; lv < pI_ArgumentDataSymbolSizes[type_index]; ++lv) {
            dims = pI_ArgumentDataDimensions[type_index][lv];
            type_class =
                CRuntime_GetDataTypeClass (
                    runtime,
                    pI_ArgumentDataSymbolTypes[type_index][lv],
                    &data_index);

            if (type_class == INTRINSIC_CLASS) {
                elem_size = pI_IntrinsicSizes[data_index];
            } else if (type_class == ARGUMENT_CLASS) {
                elem_size = sizeof(struct _Argument);
            } else if (type_class == STRUCTURE_CLASS) {
                elem_size = pI_StructureSizes[data_index];
            } else {
                return pI_FALSE;
            }

            retval &= GetDataRecursive (
                          jobj,
                          pI_ArgumentDataSymbols[type_index][lv],
                          runtime,
                          dims,
                          dims,
                          data,
                          type_class,
                          data_index,
                          type_index,
                          lv,
                          0,
                          arg);

            data += elem_size;

            /* enforce proper alignment */
            if ((elem_size % PI_STRUCTURE_ALIGNMENT) != 0) {
                data += PI_STRUCTURE_ALIGNMENT - elem_size % PI_STRUCTURE_ALIGNMENT;
            }
        } /* for (lv = 0; lv < pI_ArgumentDataSymbolSizes[type]; ++lv) */

        return retval;
    } /* pI_bool RecursiveBuildArgumentWithJSONObj (...) */

    pI_str pI_JSON_SerializeArgument (
        struct _CRuntime* runtime,
        struct _Argument* arg,
        pI_int* snlen)
    {

        struct json_object* jobj;
        struct json_object* api_version_obj;
        pI_str retval;

        if ((runtime == 0) || (arg == 0)) {
            return 0;
        }

        jobj = json_object_new_object();
        api_version_obj = json_object_new_int (PI_API_VERSION);
        json_object_object_add (jobj, PI_SN_JSON_API_VERSION_STR, api_version_obj);

        if (RecursiveBuildJSONObjWithArgument (runtime, arg, jobj) == pI_TRUE) {
            retval = runtime->CopyString (runtime, (pI_str) json_object_get_string (jobj));
            *snlen = strlen (retval);
            return retval;
        }

        return 0;
    } /* pI_str pI_JSON_SerializeArgument (...) */

    pI_str pI_JSON_SerializeArgumentList (
        struct _CRuntime* runtime,
        struct _ArgumentList* arg_list,
        pI_int* snlen)
    {

        struct json_object* jobj;
        struct json_object* arg;
        struct json_object* arg_arr;
        struct json_object* api_version_obj;
        struct json_object* arg_count_obj;
        pI_str retval;
        pI_size lv;

        if ((runtime == 0) || (arg_list == 0)) {
            return 0;
        }

        jobj = json_object_new_object();
        api_version_obj = json_object_new_int (PI_API_VERSION);
        json_object_object_add (jobj, PI_SN_JSON_API_VERSION_STR, api_version_obj);
        arg_count_obj = json_object_new_int (arg_list->count);
        json_object_object_add (jobj, PI_SN_JSON_ARGUMENT_COUNT, arg_count_obj);
        arg_arr = json_object_new_array();

        for (lv = 0; lv < arg_list->count; ++lv) {
            arg = json_object_new_object();

            if (RecursiveBuildJSONObjWithArgument (runtime, arg_list->list[lv], arg) == pI_FALSE) {
                return 0;
            }

            json_object_array_add (arg_arr, arg);
        }

        json_object_object_add (jobj, PI_SN_JSON_ARGUMENT_LIST, arg_arr);
        retval = runtime->CopyString (runtime, (pI_str) json_object_get_string (jobj));
        *snlen = strlen (retval);
        return retval;
    }

    struct _Argument* pI_JSON_DeserializeArgument (
        struct _CRuntime* runtime,
        pI_str arg_str,
        pI_int snlen) {

        json_object* jobj;
        json_object* api_version_obj;
        struct _Argument* retval;
        int version;

        if ((runtime == 0) || (arg_str == 0)) {
            return 0;
        }

        jobj = json_tokener_parse (arg_str);

        if (is_error (jobj)) {
            /* NOTE: unsure if next line is necessary or harmful! */
            json_object_put (jobj);
            return 0;
        }

        retval = runtime->AllocateArgument (runtime);

        api_version_obj = json_object_object_get (jobj, PI_SN_JSON_API_VERSION_STR);
        version = json_object_get_int (api_version_obj);

        if ((pI_int) version != PI_API_VERSION) {
            /* NOTE: unsure if next line is necessary or harmful! */
            json_object_put (jobj);
            runtime->FreeArgument (runtime, retval, pI_FALSE);
			runtime->last_error = pI_CRuntime_Error_RuntimeVersionMismatch;
            return 0;
        }

        retval->runtime = runtime;

        if (RecursiveBuildArgumentWithJSONObj (runtime, retval, jobj) != pI_TRUE) {
            /* clean up */
            runtime->FreeArgument (runtime, retval, pI_TRUE);
            return 0;
        }

        return retval;
    } /* struct _Argument* pI_JSON_DeserializeArgument (...) */

    struct _ArgumentList* pI_JSON_DeserializeArgumentList (
        struct _CRuntime* runtime,
        pI_str arg_str,
        pI_int snlen) {

        struct json_object* jobj;
        struct json_object* api_version_obj;
        struct json_object* arg_count_obj;
        struct json_object* arg;
        struct json_object* arg_arr;
        struct _ArgumentList* retval;
        int version, arg_count, lv;

        if ((runtime == 0) || (arg_str == 0)) {
            return 0;
        }

        jobj = json_tokener_parse (arg_str);

        if (is_error (jobj)) {
            /* NOTE: unsure if next line is necessary or harmful! */
            json_object_put (jobj);
            return 0;
        }

        api_version_obj = json_object_object_get (jobj, PI_SN_JSON_API_VERSION_STR);
        version = json_object_get_int (api_version_obj);
        arg_count_obj = json_object_object_get (jobj, PI_SN_JSON_ARGUMENT_COUNT);
        arg_count = json_object_get_int (arg_count_obj);

        if ((pI_int) version != PI_API_VERSION) {
            /* NOTE: unsure if next line is necessary or harmful! */
            json_object_put (jobj);
			runtime->last_error = pI_CRuntime_Error_RuntimeVersionMismatch;
            return 0;
        }

        arg_arr = json_object_object_get (jobj, PI_SN_JSON_ARGUMENT_LIST);
        retval = runtime->AllocateArgumentList (runtime, arg_count);

        for (lv = 0; lv < arg_count; ++lv) {
            retval->list[lv] = runtime->AllocateArgument (runtime);
            retval->list[lv]->runtime = runtime;
            arg = json_object_array_get_idx (arg_arr, lv);
            RecursiveBuildArgumentWithJSONObj (runtime, retval->list[lv], arg);
        }

        return retval;
    } /* struct _Argument* pI_JSON_DeserializeArgument (...) */

#undef PI_SN_JSON_ARGUMENT_LIST
#undef PI_SN_JSON_ARGUMENT_COUNT
#undef PI_SN_JSON_EMPTY_STRING
#undef PI_SN_JSON_ARGUMENT_VALUE_STR
#undef PI_SN_JSON_ARGUMENT_DATA_STR
#undef PI_SN_JSON_ARGUMENT_SETUP_STR
#undef PI_SN_JSON_ARGUMENT_SIGNATURE_CONSTRAINTS
#undef PI_SN_JSON_ARGUMENT_SIGNATURE_RO
#undef PI_SN_JSON_ARGUMENT_SIGNATURE_TYPE_STR
#undef PI_SN_JSON_ARGUMENT_SIGNATURE_TYPE_ID
#undef PI_SN_JSON_ARGUMENT_SIGNATURE_STR
#undef PI_SN_JSON_ARGUMENT_DESCRIPTION_DESC
#undef PI_SN_JSON_ARGUMENT_DESCRIPTION_NAME
#undef PI_SN_JSON_ARGUMENT_DESCRIPTION_STR
#undef PI_SN_JSON_ARGUMENT_TYPE_STR
#undef PI_SN_JSON_STRUCTURE_TYPE_STR
#undef PI_SN_JSON_INTRINSIC_TYPE_STR
#undef PI_SN_JSON_CLASS_INDEX_NAME
#undef PI_SN_JSON_CLASS_INDEX
#undef PI_SN_JSON_CLASS
#undef PI_SN_JSON_TYPE_STR
#undef PI_SN_JSON_API_VERSION_STR

#ifdef __cplusplus
}
#endif
