/* BuildSharedLibrary.h
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_BUILD_SHARED_LIBRARY_H
#define PUREIMAGE_BUILD_SHARED_LIBRARY_H

#ifdef WIN32
#if defined (pI_BUILD_SHARED_LIBRARY)
#define pI_API __declspec(dllexport)
#elif defined (pI_USE_SHARED_LIBRARY)
#define pI_API __declspec(dllimport)
#else /* assume pI_USE_SHARED_LIBRARY */
/*#pragma message( "Warning: neither pI_BUILD_SHARED_LIBRARY nor pI_USE_SHARED_LIBRARY are defined." )
#pragma message( "         Assuming pI_USE_SHARED_LIBRARY." )*/
#define pI_API __declspec(dllimport)
#endif // #if defined (pI_BUILD_SHARED_LIBRARY)
#define CALL __stdcall
#else // #ifdef WIN32 /* not WIN32 */
#if __GNUC__ >= 4
#define pI_API __attribute__ ((visibility ("default")))
#else
#define pI_API
#endif
#define CALL
#endif // #ifdef WIN32

#endif // PUREIMAGE_BUILD_SHARED_LIBRARY_H
