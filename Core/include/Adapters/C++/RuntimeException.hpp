/* RuntimeException.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_ADAPTERS_CPP_RUNTIME_EXCEPTION_HPP
#define PUREIMAGE_ADAPTERS_CPP_RUNTIME_EXCEPTION_HPP

#include "pIException.hpp"

namespace pI {
/** The namespace containing all exceptions thrown by pureImage.
*/
namespace exception {

/*  Short overview:

    pI::exception::Exception
     |
     +--- RuntimeException
		   +--- RuntimeVersionMismatch
           +--- PluginRegistrationException
           +--- PropertyException
*/

/** general runtime exception.
    General runtime exception class. */
class RuntimeException: public Exception {
    typedef Exception Base;
public:
    RuntimeException(const char* message):
        Base(message) {
        ;
    }
}; // class RuntimeException: public Exception

/** Runtime version mismatch exception.
    This exception is typically thrown when an argument or
	plugin with incompatible API version is loaded. */
class RuntimeVersionException: public RuntimeException {
    typedef RuntimeException Base;
public:
    RuntimeVersionException(const char* message):
        Base(message) {
        ;
    }
}; // class RuntimeVersionException: public RuntimeException

/** pIn registration exception.
    This exception is typically thrown by the c++ runtime when
    a plugin could not be registered (most likely because its
    already available). */
class PluginRegistrationException: public RuntimeException {
    typedef RuntimeException Base;
public:
    PluginRegistrationException(const char* message):
        Base(message) {
        ;
    }
}; // class PluginRegistrationException: public RuntimeException

/** Property exception.
    General property exception class. */
class PropertyException: public RuntimeException {
    typedef RuntimeException Base;
public:
    PropertyException(const char* message):
        Base(message) {
        ;
    }
}; // class PropertyException: public RuntimeException

} // namespace exception
} // namespace pI

#endif // PUREIMAGE_ADAPTERS_CPP_RUNTIME_EXCEPTION_HPP
