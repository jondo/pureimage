/* pIApplicationImpl.h
 *
 * Copyright 2010-2011 Johannes Kepler Universit�t Linz,
 * Institut f�r Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_ADAPTERS_CPP_APPLICATION_IMPL_H
#define PUREIMAGE_ADAPTERS_CPP_APPLICATION_IMPL_H

#include <PureImage.h>

#ifdef __cplusplus
extern "C" {
#endif

struct _SharedLibrary;

/** Container for multiple shared libraries. */
struct _pIAppPluginLibraryList {

	pI_size count, _reserved;
	struct _SharedLibrary** shared_libs;
} pIAppPluginLibraryList;

#ifndef pI_STATIC_LIBRARY

#include <BuildSharedLibrary.h>

/* Function type definitions for convenient usage. */
typedef /*pI_API*/ const pI_char* (*pIAppFindBaseDirFunc) (CRuntimePtr);
typedef /*pI_API*/ const pI_char* (*pIAppFindPluginDirFunc) (CRuntimePtr);
typedef /*pI_API*/ struct _SharedLibrary* (*pIAppLoadPluginLibraryFunc) (CRuntimePtr, const pI_char* );
typedef /*pI_API*/ struct _SharedLibrary* (*pIAppLoadPluginLibraryWithPathFunc) (CRuntimePtr, const pI_char* , const pI_char* );
typedef /*pI_API*/ struct _pIAppPluginLibraryList* (*pIAppLoadPluginLibrariesFunc) (
 CRuntimePtr ,
 const pI_char* ,
 const pI_bool,
 const pI_bool);
typedef /*pI_API*/ pI_size (*pIAppRegisterPluginListFunc) (CRuntimePtr, struct _pIAppPluginLibraryList* );
typedef /*pI_API*/ struct _pIAppPluginLibraryList* (*pIAppCreatePluginLibraryListFunc) (CRuntimePtr, pI_size);
typedef /*pI_API*/ pI_bool (*pIAppAddToPluginLibraryListFunc) (
 CRuntimePtr,
 struct _SharedLibrary* ,
 struct _pIAppPluginLibraryList* );
typedef /*pI_API*/ pI_bool (*pIAppMergePluginLibraryListsFunc) (
 CRuntimePtr,
 struct _pIAppPluginLibraryList* ,
 struct _pIAppPluginLibraryList* );
typedef /*pI_API*/ void (*pIAppDeletePluginLibraryListFunc) (CRuntimePtr, struct _pIAppPluginLibraryList*, pI_bool, pI_bool);

/** Retrieves pureImage base directory.
	@param runtime CRuntime instance.
	@return base directory. */
pI_API const pI_char* pIAppFindBaseDir (CRuntimePtr runtime);

/** Retrieves pureImage plugin directory (configuration dependent).
	@param runtime CRuntime instance.
	@return base directory. */
pI_API const pI_char* pIAppFindPluginDir (CRuntimePtr runtime);

/** Loads a pureImage plugin library.
	@param runtime CRuntime instance.
	@param libName name of plugin library, which resides in default directory.
	@return shared library instance in case of success, zero otherwise. */
pI_API struct _SharedLibrary* pIAppLoadPluginLibrary (CRuntimePtr runtime, const pI_char* libName);

/** Loads a pureImage plugin library.
	@param runtime CRuntime instance.
	@param libName name of plugin library.
	@param libName library path.
	@return shared library instance in case of success, zero otherwise. */
pI_API struct _SharedLibrary* pIAppLoadPluginLibraryWithPath (
 CRuntimePtr runtime,
 const pI_char* path,
 const pI_char* libName);

/** Loads multiple pureImage plugin libraries.
	@param runtime CRuntime instance.
	@param libName library path.
	@param recursive if true, the path is searched recursively.
	@param auto_register if true, successfully loaded plugins
						 are registered automatically to the runtime instance.
	@return list of shared library instances in case of success, zero otherwise. */
pI_API struct _pIAppPluginLibraryList* pIAppLoadPluginLibraries (
	CRuntimePtr runtime,
    const pI_char* path = 0,
    const pI_bool recursive = pI_TRUE,
    const pI_bool auto_register = pI_TRUE);

/** Registers a list of pureImage plugin libraries.
	@param runtime CRuntime instance.
	@param list list of plugin libraries.
	@return number of successfully registered plugins. */
pI_API pI_size pIAppRegisterPluginList (CRuntimePtr runtime, struct _pIAppPluginLibraryList* list);

/** Creates an empty pureImage plugin library list.
	@param runtime CRuntime instance.
	@param reserve pre-allocated array size
	@return newly created plugin library list.*/
pI_API struct _pIAppPluginLibraryList* pIAppCreatePluginLibraryList (CRuntimePtr runtime, pI_size reserve);

/** Adds a plugin library to a plugin library list.
	@param runtime CRuntime instance.
	@param lib plugin library to add.
	@param list library list to add to.
	@return pI_TRUE in case of success, pI_FALSE otherwise. */
pI_API pI_bool pIAppAddToPluginLibraryList (
 CRuntimePtr runtime,
 struct _SharedLibrary* lib,
 struct _pIAppPluginLibraryList* list);

/** Adds plugins from one plugin library list to another.
	After function execution, the source list is no longer valid.
	@param runtime CRuntime instance.
	@param srcdst library list to add to.
	@param src2 library list to merge into srcdst.
	@return pI_TRUE in case of success, pI_FALSE otherwise. */
pI_API pI_bool pIAppMergePluginLibraryLists (
 CRuntimePtr runtime,
 struct _pIAppPluginLibraryList* srcdst,
 struct _pIAppPluginLibraryList* src2);

/** Deletes a plugin library list instance.
	@param runtime CRuntime instance.
	@param list plugin library list to delete.
	@param unloadSharedLibraries if pI_TRUE, all shared libraries in list are also unloaded. */
pI_API void pIAppDeletePluginLibraryList (
 CRuntimePtr runtime,
 struct _pIAppPluginLibraryList* list,
 pI_bool listOnly,
 pI_bool unloadSharedLibraries);

#else /* #ifndef pI_STATIC_LIBRARY */

/** Retrieves pureImage base directory.
	@param runtime CRuntime instance.
	@return base directory. */
extern pI_char* pIAppFindBaseDir (CRuntimePtr runtime);

/** Retrieves pureImage plugin directory (configuration dependent).
	@param runtime CRuntime instance.
	@return base directory. */
extern pI_char* pIAppFindPluginDir (CRuntimePtr runtime);

/** Loads a pureImage plugin library.
	@param runtime CRuntime instance.
	@param libName name of plugin library, which resides in default directory.
	@return shared library instance in case of success, zero otherwise. */
extern struct _SharedLibrary* pIAppLoadPluginLibrary (CRuntimePtr runtime, pI_char* libName);

/** Loads a pureImage plugin library.
	@param runtime CRuntime instance.
	@param libName name of plugin library.
	@param libName library path.
	@return shared library instance in case of success, zero otherwise. */
extern struct _SharedLibrary* pIAppLoadPluginLibraryWithPath (
 CRuntimePtr runtime,
 pI_char* path,
 pI_char* libName);

/** Loads multiple pureImage plugin libraries.
	@param runtime CRuntime instance.
	@param libName library path.
	@param recursive if true, the path is searched recursively.
	@param auto_register if true, successfully loaded plugins
						 are registered automatically to the runtime instance.
	@return list of shared library instances in case of success, zero otherwise. */
extern struct _pIAppPluginLibraryList* pIAppLoadPluginLibraries (
	CRuntimePtr runtime,
    const pI_char* path = 0,
    const pI_bool recursive = pI_TRUE,
    const pI_bool auto_register = pI_TRUE);

/** Registers a list of pureImage plugin libraries.
	@param runtime CRuntime instance.
	@param list list of plugin libraries.
	@return number of successfully registered plugins. */
extern pI_size pIAppRegisterPluginList (CRuntimePtr runtime, struct _pIAppPluginLibraryList* list);

/** Creates an empty pureImage plugin library list.
	@param runtime CRuntime instance.
	@param reserve pre-allocated array size
	@return newly created plugin library list.*/
extern struct _pIAppPluginLibraryList* pIAppCreatePluginLibraryList (CRuntimePtr runtime, pI_size reserve);

/** Adds a plugin library to a pre-allocated plugin library list.
	@param runtime CRuntime instance.
	@param lib plugin library to add.
	@param list library list to add to.
	@return pI_TRUE in case of success, pI_FALSE otherwise. */
extern pI_bool pIAppAddToPluginLibraryList (
 CRuntimePtr runtime,
 struct _SharedLibrary* lib,
 struct _pIAppPluginLibraryList* list);

/** Adds plugins from one plugin library list to another.
	After function execution, the source list is no longer valid.
	@param runtime CRuntime instance.
	@param srcdst library list to add to.
	@param src2 library list to merge into srcdst.
	@return pI_TRUE in case of success, pI_FALSE otherwise. */
extern pI_bool pIAppMergePluginLibraryLists (
 CRuntimePtr runtime,
 struct _pIAppPluginLibraryList* srcdst,
 struct _pIAppPluginLibraryList* src2);

/** Deletes a plugin library list instance.
	@param runtime CRuntime instance.
	@param list plugin library list to delete.
	@param unloadSharedLibraries if pI_TRUE, all shared libraries in list are also unloaded. */
extern void pIAppDeletePluginLibraryList (
 CRuntimePtr runtime,
 struct _pIAppPluginLibraryList* list,
 pI_bool listOnly,
 pI_bool unloadSharedLibraries);

#endif /* #ifndef pI_STATIC_LIBRARY */

#ifdef __cplusplus
}
#endif

#endif // PUREIMAGE_ADAPTERS_CPP_APPLICATION_IMPL_H
