/* ApplicationGlobalFunctions.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universit�t Linz,
 * Institut f�r Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <boost/shared_ptr.hpp>
#include <boost/bind.hpp>

#include <PureImage.h>
#include <Adapters/C++/pIApplicationImpl.h>

#ifndef pI_STATIC_LIBRARY
#include <BuildSharedLibrary.h>
#include <SharedLibrary.h>
#endif // pI_STATIC_LIBRARY

namespace pI {

namespace application {

typedef boost::shared_ptr<struct _SharedLibrary> SharedLibrarySharedPtr;
typedef boost::shared_ptr<struct _pIAppPluginLibraryList> SharedLibraryListSharedPtr;

#ifndef pI_STATIC_LIBRARY

void DeleteLibraryList (SharedLibrarySharedPtr impl, struct _pIAppPluginLibraryList* sll, struct _CRuntime* rt) {

	pIAppDeletePluginLibraryListFunc delfunc =
	 (pIAppDeletePluginLibraryListFunc) GetSharedLibraryFunction (impl.get(), "pIAppDeletePluginLibraryList");
	delfunc (rt, sll, pI_FALSE, pI_TRUE);
}

SharedLibraryListSharedPtr CreateLibraryList (struct _CRuntime* rt, SharedLibrarySharedPtr impl) {

	pIAppCreatePluginLibraryListFunc clfunc =
	 (pIAppCreatePluginLibraryListFunc) GetSharedLibraryFunction (impl.get(), "pIAppCreatePluginLibraryList");
	struct _pIAppPluginLibraryList* sll(clfunc (rt, 2));
	return SharedLibraryListSharedPtr (sll, boost::bind (DeleteLibraryList, impl, sll, rt));
}

#else // #ifndef pI_STATIC_LIBRARY

void DeleteLibraryList (struct _pIAppPluginLibraryList* sll, struct _CRuntime* rt) {

	pIAppDeletePluginLibraryList (rt, sll, pI_FALSE, pI_TRUE);
}

SharedLibraryListSharedPtr CreateLibraryList (struct _CRuntime* rt) {

	struct _pIAppPluginLibraryList* sll(pIAppCreatePluginLibraryList (rt, 2));
	return SharedLibraryListSharedPtr (sll, boost::bind (DeleteLibraryList, impl, sll, rt));
}

#endif // #ifndef pI_STATIC_LIBRARY

} // namespace application

} // namespace pI
