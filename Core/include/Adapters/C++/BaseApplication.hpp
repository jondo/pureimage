/* BaseApplication.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_ADAPTERS_CPP_BASEAPPLICATION_HPP
#define PUREIMAGE_ADAPTERS_CPP_BASEAPPLICATION_HPP

#include <string>
#include <stdarg.h>
#include <boost/shared_ptr.hpp>
#include <Poco/SharedLibrary.h>
#include <Adapters/C++/Runtime.hpp>

namespace pI {

/**
 * This class provides a simple way to create an application
 * for pureImage. It mainly provides facilities to manage plugin libraries
 * and takes care of plugin registration.
 */
class BaseApplication {

public:

    /// dll export function definition
    typedef pI_bool (*CreatepIns) (
        pI_int index,
        struct _CRuntime* runtime,
        struct _CpIn** plugin);

    /** Default constructor.
        Creates and manages its own runtime instance.
        @param load_properties if pI_TRUE, the application property
               file is loaded and parsed. */
    BaseApplication (const pI_bool load_properties = pI_FALSE);

#ifndef SWIG
    /** Creates an application using a given runtime.
        @param runtime pI runtime to use for application
        @param take_ownership if pI_TRUE, the application will
               delete the runtime on destruction.
        @param load_properties if true and take_ownership is true,
               the runtime will write a properties file on exit. */
    BaseApplication (
        const CRuntimePtr runtime,
        const pI_bool take_ownership = pI_FALSE,
        const pI_bool load_properties = pI_FALSE);
#endif

    /** Creates an application for a given runtime without taking ownership. */
    BaseApplication (Runtime& runtime);

    ~BaseApplication() {
        _runtime.GetCRuntime()->DestroyRegisteredPlugins (_runtime.GetCRuntime());
        _dllHandles.clear();
    }

    /// Returns a runtime instance.
    Runtime GetRuntime() const {
        return _runtime;
    }

    /** Loads a library with specified name from the default path.
        This method just loads the library. Actual plugins are not loaded until
        method RegisterPlugins is called. The default path is equal to
        @c FindPIBaseDir() + /Dist[Debug|Release].
        For instance, in Windows, a plugin library @c my_plugin.dll is loaded with
        @code LoadPluginLibrary("my_plugin"); @endcode

        @exception PluginLibraryException
        @param path directory of the plugin library
        @param libName file name of the plugin library */
    void LoadPluginLibrary (const pI_char* libName);

    /** Loads a library with specified name from a given path.
        This method just loads the library. Actual plugins are not loaded until
        method RegisterPlugins is called. For instance, in Windows, a plugin
        library @c some_path\my_plugin.dll is loaded with
            @code LoadPluginLibrary("some_path", "my_plugin"); @endcode

        @exception PluginLibraryException
        @param path directory of the plugin library
        @param libName file name of the plugin library */
    void LoadPluginLibrary (const pI_char* path, const pI_char* libName);

    /** Scans a folder for plugin libraries, and loads all which are found.
        @exception PluginLibraryException
        @param path root plugin library folder.
                    If not specified, the working directory is used instead.
        @param recursive if pI_TRUE, sub directories are scanned as well.
        @param auto_register if pI_TRUE, the loaded plugin libraries are
                             registered to the system. */
    void LoadPluginLibraries (
        const pI_char* path = 0,
        const pI_bool recursive = pI_TRUE,
        const pI_bool auto_register = pI_TRUE);

    /** Loads all plugins created by all previously loaded plugin libraries.
        Simple plugin dependencies are handled by a looping approach.
        Attention: circular dependencies can not be resolved.
        @return number of actually added (not totally registered) plugins */
    pI_size RegisterPlugins();

    /** Provides the list of names of all loaded plugins. */
    std::vector<std::string> GetPluginNames() const;

    /** Provides the list of descriptions of all loaded plugins. */
    std::vector<std::string> GetPluginDescriptions() const;

    /** Provides the list of version numbers of all loaded plugins. */
    std::vector<pI_unsigned> GetPluginVersions() const;

    /** Provides the list of authors of all loaded plugins. */
    std::vector<std::string> GetPluginAuthors() const;

    /** Provides the list of copyright notices of all loaded plugins. */
    std::vector<std::string> GetPluginCopyrights() const;

    /**
     * Tries to find the pureImage installation root dir by looking for "libraries" folder.
     */
    static std::string FindPIBaseDir();

    /// method to combine plugin spawning and initialization
    pInPtr SpawnAndInitialize (pI::Runtime& runtime, const pI_char* plugin, ...);

private:

    Runtime _runtime;

    /*
     * All loaded DLLs. Unload them on program termination.
     */
    std::vector<boost::shared_ptr<Poco::SharedLibrary> > _dllHandles;

};
}

#endif // PUREIMAGE_ADAPTERS_CPP_BASEAPPLICATION_HPP
