/* pInAdapter.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_ADAPTERS_CPP_PINADAPTER_HPP
#define PUREIMAGE_ADAPTERS_CPP_PINADAPTER_HPP

#include <Adapters/C++/PureImage.hpp>

#ifdef PI_ADAPT_ARGUMENTS
#include "AdapterFactory.hpp"
#endif

namespace pI {

/** pInAdapter is a wrapper which transforms a given
    CpIn (C) instance into an pIn (C++) instance.
    Using this adapter it is possible to use a CpIn
    retrieved by runtime-query as more comfortable pIn. */
class pInAdapter: public pIn {

    typedef pIn Base;

public:

    /** Constructor.
        This constructor takes, in difference to the typical pIn constructor,
        an CpIn pointer, which also holds a pointer to a valid runtime.

        @param c_pin          CpIn instance to wrap into C++ pIn.

        @param take_ownership If pI_TRUE, the adapter takes the ownership of the
                              given CpIn instance and deletes it on destruction.
    */
    pInAdapter(
        const CpInPtr c_pin,
        const pI_bool take_ownership = pI_FALSE);

    virtual ~pInAdapter();

    virtual pInPtr Create() const;

    virtual pInPtr Clone() const;

    virtual const pI_int GetpInVersion() const;

    virtual const std::string GetCopyright() const;

    virtual const std::string GetDescription() const;

    virtual const std::string GetAuthor() const;

    virtual const std::string GetName() const;

    virtual Arguments GetParameterSignature() const;

    virtual Arguments GetInputSignature() const;

    virtual Arguments GetOutputSignature() const;

    virtual void Serialize (std::string& serialization_data) const;

    virtual void Deserialize (const std::string& serialization_data);

protected:

	virtual void _Initialize (const Arguments& parameters);

	virtual void _Execute (Arguments& input_args, Arguments& output_args);

    struct veto_delete {
        void operator() (const void*) const {}
    }; // struct veto_delete

    struct argument_delete {
        void operator() (const void* c_arg) const {
            Argument* arg = reinterpret_cast<Argument*>(const_cast<void*>(c_arg));

            if(arg != 0) {
                arg->runtime->FreeArgument(arg->runtime, arg, pI_TRUE);
            }
        }
    }; // struct veto_delete


    static ArgumentPtr ObtainArgument(CArgumentPtr arg, pI_bool own = pI_TRUE) {

        if(!own)
            return ArgumentPtr(arg, veto_delete());
        else
            return ArgumentPtr(arg, argument_delete());

    }

    CpInPtr _c_plugin;
    const pI_bool _own_plugin;

private:

    static Runtime CreateRuntime (const CRuntimePtr cruntime);

}; // class pInAdapter

} // namespace pI

#endif // PUREIMAGE_ADAPTERS_CPP_PINADAPTER_HPP
