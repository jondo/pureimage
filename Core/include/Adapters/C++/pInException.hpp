/* pInException.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_ADAPTERS_CPP_PIN_EXCEPTION_HPP
#define PUREIMAGE_ADAPTERS_CPP_PIN_EXCEPTION_HPP

#include "pIException.hpp"

namespace pI {
/** The namespace containing all exceptions thrown by pureImage.
*/
namespace exception {

/*  Short overview:

    pI::exception::Exception
     |
     +--- pInException
           |
           +--- PluginCreationException
           +--- NotInitializedException
           +--- InitializationException
           +--- ParameterException
           |     |
           |     +--- MissingParameterException
           |     +--- IncompatibleParameterException
           |     +--- IncompleteParameterException
           |
           +--- ArgumentException
           |     |
           |     +--- MissingArgumentException
           |     +--- IncompatibleArgumentException
           |     +--- IncompleteArgumentException
           |
           +--- ExecutionException
           +--- SerializationException
           |     |
           |     +--- SerializationNotSupportedException
           |
           +--- DeserializationException
           |     |
           |     +--- DeserializationNotSupportedException
           |
           +--- PluginQueryException
*/

/** pIn general runtime exception.
    General runtime exception class. */
class pInException: public Exception {
    typedef Exception Base;
public:
    pInException(const char* message):
        Base(message) {
        ;
    }
}; // class pInException: public Exception

/** pIn creation exception.
    This exception is typically thrown by a plugin when its construction fails.
*/
class PluginCreationException: public pInException {
    typedef pInException Base;
public:
    PluginCreationException(const char* message):
        Base(message) {
        ;
    }
}; // class PluginCreationException: public pInException

/** pIn not initialized exception.
    This exception is typically thrown by a plugin when its Execute
    method is called without prior initialization. */
class NotInitializedException: public pInException {
    typedef pInException Base;
public:
    NotInitializedException(const char* message):
        Base(message) {
        ;
    }
}; // class NotInitializedException: public pInException

/** pIn initialization exception.
    This exception is typically thrown by a plugin when its Initialization
    method fails. */
class InitializationException: public pInException {
    typedef pInException Base;
public:
    InitializationException(const char* message):
        Base(message) {
        ;
    }
}; // class InitializationException: public pInException

/** pIn general parameter exception.
    General parameter exception class. */
class ParameterException: public pInException {
    typedef pInException Base;
public:
    ParameterException(const char* message):
        Base(message) {
        ;
    }
}; // class ParameterException: public pInException

/** pIn missing parameter exception.
    This exception is typically thrown by a plugin when its Initialize
    method is called and not all required parameters are passed, which
	means that not all mandatory parameters are given. */
class MissingParameterException: public ParameterException {
    typedef ParameterException Base;
public:
    MissingParameterException(const char* message):
        Base(message) {
        ;
    }
}; // class MissingParameterException: public ParameterException

/** pIn incompatible parameter exception.
    This exception is typically thrown by a plugin when its Initialize
    method is called with an incompatible parameter set, which means
	that a parameter of another type as expected is given. */
class IncompatibleParameterException: public ParameterException {
    typedef ParameterException Base;
public:
    IncompatibleParameterException(const char* message):
        Base(message) {
        ;
    }
}; // class IncompatibleParameterException: public ParameterException

/** pIn incomplete parameter exception.
    This exception is typically thrown by a plugin when its Initialize
    method is called with a parameter set with partially incomplete data,
	which means that the data field of a mandatory parameter is empty. */
class IncompleteParameterException: public ParameterException {
    typedef ParameterException Base;
public:
    IncompleteParameterException(const char* message):
        Base(message) {
        ;
    }
}; // class IncompleteParameterException: public ParameterException

/** pIn general argument exception.
    General argument exception class. */
class ArgumentException: public pInException {
    typedef pInException Base;
public:
    ArgumentException(const char* message):
        Base(message) {
        ;
    }
}; // class ArgumentException: public pInException

/** pIn missing argument exception.
    This exception is typically thrown by a plugin when its Execute
    method is called and not all required parameters are passed,
	which means that not all mandatory arguments are given. */
class MissingArgumentException: public ArgumentException {
    typedef ArgumentException Base;
public:
    MissingArgumentException(const char* message):
        Base(message) {
        ;
    }
}; // class MissingArgumentException: public ArgumentException

/** pIn incompatible argument exception.
    This exception is typically thrown by a plugin when its Execute
    method is called with an incompatible parameter set, which means
	that an argument of another type as expected is given. */
class IncompatibleArgumentException: public ArgumentException {
    typedef ArgumentException Base;
public:
    IncompatibleArgumentException(const char* message):
        Base(message) {
        ;
    }
}; // class IncompatibleArgumentException: public ArgumentException

/** pIn incomplete argument exception.
    This exception is typically thrown by a plugin when its Execute
    method is called with a parameter set with partially incomplete data,
	which means that the data field of a mandatory argument is empty. */
class IncompleteArgumentException: public ArgumentException {
    typedef ArgumentException Base;
public:
    IncompleteArgumentException(const char* message):
        Base(message) {
        ;
    }
}; // class IncompleteArgumentException: public ArgumentException

/** pIn execution exception.
    This exception is typically thrown by a plugin when its Execute
    method fails proper execution. */
class ExecutionException: public pInException {
    typedef pInException Base;
public:
    ExecutionException(const char* message):
        Base(message) {
        ;
    }
}; // class ExecutionException: public pInException

/** pIn serialization exception.
    This exception is typically thrown by a plugin when its Serialization
    method failed. */
class SerializationException: public pInException {
    typedef pInException Base;
public:
    SerializationException(const char* message):
        Base(message) {
        ;
    }
}; // class SerializationException: public pInException

/** pIn serialization not supported exception.
    This exception is typically thrown by a plugin when its Serialization
    method is not implemented. */
class SerializationNotSupportedException: public SerializationException {
    typedef SerializationException Base;
public:
    SerializationNotSupportedException(const char* message):
        Base(message) {
        ;
    }
}; // class SerializationNotSupportedException: public SerializationException

/** pIn deserialization exception.
    This exception is typically thrown by a plugin when its Deserialization
    method failed. */
class DeserializationException: public pInException {
    typedef pInException Base;
public:
    DeserializationException(const char* message):
        Base(message) {
        ;
    }
}; // class DeserializationException: public pInException

/** pIn deserialization exception.
    This exception is typically thrown by a plugin when its Deserialization
    method failed. */
class DeserializationNotSupportedException: public DeserializationException {
    typedef DeserializationException Base;
public:
    DeserializationNotSupportedException(const char* message):
        Base(message) {
        ;
    }
}; // class DeserializationException: public DeserializationException

/** pIn query exception.
    This exception is typically thrown by a plugin when its Initialize
    or Execute method tried to query another plugin without success. */
class PluginQueryException: public pInException {
    typedef pInException Base;
public:
    PluginQueryException(const char* message):
        Base(message) {
        ;
    }
}; // class PluginQueryException: public pInException

} // namespace exception
} // namespace pI

#endif // PUREIMAGE_ADAPTERS_CPP_PIN_EXCEPTION_HPP
