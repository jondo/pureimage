/* ArgumentChecks.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_ADAPTERS_CPP_ARGUMENTCHECKS_HPP
#define PUREIMAGE_ADAPTERS_CPP_ARGUMENTCHECKS_HPP

#include <Adapters/C++/Runtime.hpp>

namespace pI {

// Note: only top-level check methods get SWIGged
#ifndef SWIG

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
/// @throws exception::IncompatibleArgumentException
void CheckVariableArgumentType (
    const CArgumentPtr given,
    const CArgumentPtr prototype);

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
/// @throw exception::IncompatibleArgumentException
void CheckVariableArgumentType (
    const ArgumentPtr& given,
    const ArgumentPtr& prototype);

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
/** @throws exception::IncompatibleArgumentException
  * @throws exception::IncompatibleParameterException
**/
void CheckArgumentType (
    const CArgumentPtr given,
    const CArgumentPtr prototype,
    const pI_bool is_argument = pI_TRUE);

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
/** @throws exception::IncompatibleArgumentException
  * @throws exception::IncompatibleParameterException
**/
void CheckArgumentConstraints (
    const CArgumentPtr given,
    const CArgumentPtr prototype,
    const pI_bool is_argument = pI_TRUE);

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
/** @throws exception::IncompatibleArgumentException
  * @throws exception::IncompatibleParameterException
**/
void CheckArgumentData (
    const CArgumentPtr given,
    const CArgumentPtr prototype,
    const pI_bool is_argument = pI_TRUE,
    const pI_bool do_constraint_check = pI_TRUE);

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
/// @throws exception::IncompatibleParameterException
void CheckParameter (
    const CArgumentPtr given,
    const CArgumentPtr prototype);

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
/// @throws exception::IncompatibleParameterException
void CheckParameter (
    const ArgumentPtr& given,
    const ArgumentPtr& prototype);

#endif // #ifndef SWIG

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
/** @throws exception::IncompatibleParameterException
  * @throws exception::MissingParameterException
**/
void CheckParameters (
    const Arguments& given,
    const Arguments& prototype,
	pI_int num = -1);

#ifndef SWIG

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
/// @throws exception::IncompatibleArgumentException
void CheckInputArgument (
    const CArgumentPtr given,
    const CArgumentPtr prototype);

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
/// @throws exception::IncompatibleArgumentException
void CheckInputArgument (
    const ArgumentPtr& given,
    const ArgumentPtr& prototype);

#endif // #ifndef SWIG

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
/** @throws exception::IncompatibleArgumentException
  * @throws exception::MissingArgumentException
**/
void CheckInputArguments (
    const Arguments& given,
    const Arguments& prototype,
	pI_int num = -1);

#ifndef SWIG

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
/// @throws exception::IncompatibleArgumentException
void CheckOutputArgument (
    const CArgumentPtr given,
    const CArgumentPtr prototype);

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
/// @throws exception::IncompatibleArgumentException
void CheckOutputArgument (
    const ArgumentPtr& given,
    const ArgumentPtr& prototype);

#endif // #ifndef SWIG

#ifndef PI_ARGUMENT_CHECKS_BINARY
static inline
#endif // PI_ARGUMENT_CHECKS_BINARY
/** @throws exception::IncompatibleArgumentException
  * @throws exception::MissingArgumentException
**/
void CheckOutputArguments (
    const Arguments& given,
    const Arguments& prototype,
	pI_int num = -1);


} // namespace pI

#ifndef PI_ARGUMENT_CHECKS_BINARY
#include <../src/Adapters/C++/ArgumentChecks.cpp>
#endif // PI_ARGUMENT_CHECKS_BINARY

#endif // PUREIMAGE_ADAPTERS_CPP_ARGUMENTCHECKS_HPP
