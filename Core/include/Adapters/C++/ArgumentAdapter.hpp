/* ArgumentAdapter.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_ADAPTERS_CPP_ARGUMENTS_ARGUMENTADAPTER_HPP
#define PUREIMAGE_ADAPTERS_CPP_ARGUMENTS_ARGUMENTADAPTER_HPP

#include <PureImage.h>
#include <Adapters/C++/pIExceptions.hpp>

// ATTENTION: do not include this (circular dependency)
//#include "Adapters/C++/ArgumentTypedefs.hpp"
#include <boost/shared_ptr.hpp>

namespace pI {

typedef Argument* CArgumentPtr;

class Runtime;

/**
 * A base class for all argument adapters.
 *
 * It provides access to common members of a pureImage argument.
 */
class ArgumentAdapter {

#ifndef SWIG
protected:
    boost::shared_ptr<Argument> c_ptr;
#endif

public:

    /**
     * Creates and manages an argument instance of given type.
     */
    ArgumentAdapter(Runtime& runtime, pI_ArgumentType argType);

    ArgumentAdapter(boost::shared_ptr<Argument> c_ptr);

    /**
     * Creates an adapter for a given argument and takes ownership if
     * specified.
     */
	ArgumentAdapter(const CArgumentPtr c_ptr, pI_bool take_ownership);

    virtual ~ArgumentAdapter();

    /**
     * Provides access to the underlying C argument instance.
     */
    Argument* GetCPtr();

    pI_char* GetConstraints() const;

    ArgumentAdapter& SetConstraints( const pI_char* value );

    pI_char* GetName() const;

    ArgumentAdapter& SetName( const pI_char* value );

    pI_char* GetDescription() const;

    ArgumentAdapter& SetDescription( const pI_char* value );

    pI_ArgumentType GetType() const;

    pI_bool IsReadOnly() const;

    ArgumentAdapter& SetReadOnly(pI_bool value);

    pI_bool HasData() const;

    /**
     * Allocates the data part of the argument.
     *
     * After calling this method it is not allowed to
     * change signature information that defines the layout
     * of the data (e.g., length of an array).
     *
     * @throws exception::MemoryException
     */
	// this is virtual to have dynamic sub-types in sub-classes
    pI::ArgumentAdapter& CreateData();

	/**
	 * Frees the data allocated previously.
	 * This resets this argument's state. Changes to its data signature are allowed afterwards.
	 */
	pI::ArgumentAdapter& FreeData();

#ifndef SWIG
    /**
     * Delivers the contained C argument as a shared_ptr.
     */
    boost::shared_ptr<Argument> shared_ptr();

    /**
     * A cast operator that allows assignment of this
     * adapter to a shared_ptr.
     *
     * E.g., this is useful when dealing with argument lists (pI::Arguments).
     */
    operator boost::shared_ptr<Argument>();

#endif

}; // class ArgumentAdapter

} // namespace pI


#ifndef SWIG

#include <Runtime/Runtime.h>
#include <Adapters/C++/Runtime.hpp>

#define SET_STRING_SAFE(R, STR, NEW_STR)\
    {\
        if(STR) { \
            R->FreeString(R, const_cast<pI_char*>(STR));\
        }\
        STR = R->CopyString(R, const_cast<pI_char*>(NEW_STR));\
    }

// HACK: preliminarily control definition of helpers by defines
#ifndef DEFINE_NO_DELETER
#define DEFINE_NO_DELETER
static void do_not_delete_argument(Argument* arg) {};
#endif

inline
pI::ArgumentAdapter::ArgumentAdapter(Runtime& runtime, pI_ArgumentType argType)
{
    c_ptr = boost::shared_ptr<Argument>(runtime.GetCRuntime()->CreateArgument(runtime.GetCRuntime(), argType), Runtime::DeleteArgument);
}

inline
pI::ArgumentAdapter::ArgumentAdapter(boost::shared_ptr<Argument> c_arg)
{
    c_ptr = c_arg;
}

inline
pI::ArgumentAdapter::ArgumentAdapter(const CArgumentPtr c_arg, pI_bool take_ownership)
{
    if (take_ownership) {
        c_ptr = boost::shared_ptr<Argument>(const_cast<CArgumentPtr>(c_arg), Runtime::DeleteArgument);
    } else {
        /*
        Note:   this is necessary as the default management of the C argument instance
                is done using a boost::shared_ptr.
                In the case that the adapter is used without ownership the auto deletion
                has to be circumvented.
        */
        c_ptr = boost::shared_ptr<Argument>(const_cast<CArgumentPtr>(c_arg), do_not_delete_argument);
    }
}

inline
pI::ArgumentAdapter::~ArgumentAdapter()
{
}

inline
Argument* pI::ArgumentAdapter::GetCPtr()
{
    return c_ptr.get();
}

inline
pI_char* pI::ArgumentAdapter::GetConstraints() const
{
    return c_ptr->signature.constraints;
}

inline
pI::ArgumentAdapter& pI::ArgumentAdapter::SetConstraints( const pI_char* value )
{
    SET_STRING_SAFE(c_ptr->runtime, c_ptr->signature.constraints, value);
    return *this;
}

inline
pI_char* pI::ArgumentAdapter::GetName() const
{
    return c_ptr->description.name;
}

inline
pI::ArgumentAdapter& pI::ArgumentAdapter::SetName(const pI_char* value)
{
    SET_STRING_SAFE(c_ptr->runtime, c_ptr->description.name, value);
    return *this;
}

inline
pI_char* pI::ArgumentAdapter::GetDescription() const
{
    return c_ptr->description.description;
}

inline
pI::ArgumentAdapter& pI::ArgumentAdapter::SetDescription(const pI_char* value)
{
    SET_STRING_SAFE(c_ptr->runtime, c_ptr->description.description, value);
    return *this;
}

inline
pI_ArgumentType pI::ArgumentAdapter::GetType() const
{
    return c_ptr->signature.type;
}

inline
pI_bool pI::ArgumentAdapter::IsReadOnly() const
{
    return c_ptr->signature.readonly;
}

inline
pI::ArgumentAdapter& pI::ArgumentAdapter::SetReadOnly(pI_bool value)
{
    c_ptr->signature.readonly = value;
    return *this;
}

inline
pI_bool pI::ArgumentAdapter::HasData() const
{
    return (c_ptr->data.Empty != 0);
}

inline
pI::ArgumentAdapter& pI::ArgumentAdapter::CreateData()
{
	if(!c_ptr->runtime->CreateArgumentData(c_ptr->runtime, GetCPtr())) {
		throw exception::MemoryException("Failed to allocated argument data.");
	}

	return *this;
}

inline
pI::ArgumentAdapter& pI::ArgumentAdapter::FreeData()
{
	c_ptr->runtime->FreeArgumentData(c_ptr->runtime, c_ptr.get());
	c_ptr->data.Empty = 0;

	return *this;
}

inline
pI::ArgumentAdapter::operator boost::shared_ptr<Argument>()
{
    return c_ptr;
}

#undef SET_STRING_SAFE

#endif //SWIG

#endif // PUREIMAGE_ADAPTERS_CPP_ARGUMENTS_ARGUMENTADAPTER_HPP
