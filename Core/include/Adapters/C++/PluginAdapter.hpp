/* PluginAdapter.hpp
 *
 * Copyright 2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PLUGIN_ADAPTER_HPP
#define PLUGIN_ADAPTER_HPP

#include <Adapters/C++/pIn.hpp>

namespace pI {

    class PluginAdapter: public pIn {

    public:

        PluginAdapter(Runtime& runtime): pIn(runtime) {};

        virtual ~PluginAdapter() {};

        virtual pInPtr Create() const {
            return pInPtr();
        }

        virtual const pI_int GetpInVersion() const {
            return 0;
        }

        virtual const std::string GetDescription() const {
            return "";
        }

        virtual const std::string GetName() const {
            return "";
        }

        virtual Arguments GetParameterSignature() const {
            return Arguments();
        }

        virtual Arguments GetInputSignature() const {
            return Arguments();
        };

        virtual Arguments GetOutputSignature() const {
            return Arguments();
        }

        virtual pInPtr Clone() const { return pInPtr(0); }

        virtual void Serialize (std::stringstream& serialization_data) const {}

		virtual void Deserialize (const std::string& serialization_data) {}

	protected:

		virtual void _Initialize (const Arguments& parameters) {};

        virtual void _Execute (Arguments& input_args, Arguments& output_args) {}

    };
}

#endif // PLUGIN_ADAPTER_HPP
