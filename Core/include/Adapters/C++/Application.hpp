/* pIApplication.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universit�t Linz,
 * Institut f�r Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_ADAPTERS_CPP_APPLICATION_HPP
#define PUREIMAGE_ADAPTERS_CPP_APPLICATION_HPP

#include <string>
#include <stdarg.h>
#include <boost/shared_ptr.hpp>

#include <Adapters/C++/Runtime.hpp>
#include <Adapters/C++/ApplicationGlobalFunctions.hpp>

#include <Adapters/C++/Arguments.hpp>

namespace pI {

class Application {

public:

	Application (const pI_bool load_properties = pI_FALSE);

#ifndef SWIG
	Application(
     const CRuntimePtr runtime,
     const pI_bool take_ownership = pI_FALSE,
     const pI_bool load_properties = pI_FALSE);
#endif

	Application(Runtime& runtime);

	Application(Application& app);

	virtual ~Application();

	/// Returns a runtime instance.
    Runtime GetRuntime() const {
        return _runtime;
    }

	inline void LoadPluginLibrary (const pI_char* libName);
	inline void LoadPluginLibrary (const pI_char* path, const pI_char* libName);
	inline void LoadPluginLibraries (
     const pI_char* path = 0,
     const pI_bool recursive = pI_TRUE,
     const pI_bool auto_register = pI_TRUE);

	inline pI_size RegisterPlugins();

	///** Provides the list of names of all loaded plugins. */
 //   std::vector<std::string> GetPluginNames() const;
 //   /** Provides the list of descriptions of all loaded plugins. */
 //   std::vector<std::string> GetPluginDescriptions() const;
 //   /** Provides the list of version numbers of all loaded plugins. */
 //   std::vector<pI_unsigned> GetPluginVersions() const;
 //   /** Provides the list of authors of all loaded plugins. */
 //   std::vector<std::string> GetPluginAuthors() const;
 //   /** Provides the list of copyright notices of all loaded plugins. */
 //   std::vector<std::string> GetPluginCopyrights() const;

    /**
     * Tries to find the pureImage installation root dir by looking for libraries folder.
     */
    inline std::string FindPIBaseDir() const;

    /**
     * Tries to find the pureImage plugin dir by looking for libraries folder.
     */
    inline std::string FindPIPluginDir() const;


    /// method to combine plugin spawning and initialization
	inline pInPtr SpawnAndInitialize (const pI_char* plugin, ...);

private:

	inline void AddDefaultProperties();


	Runtime _runtime;

#ifndef pI_STATIC_LIBRARY
	application::SharedLibrarySharedPtr _impl;
#endif // #ifndef pI_STATIC_LIBRARY
	application::SharedLibraryListSharedPtr _sharedLibs;

}; // class Application


Application::Application(const pI_bool load_properties /*= pI_FALSE*/): _runtime(load_properties) {
#ifndef pI_STATIC_LIBRARY
	_impl = application::SharedLibrarySharedPtr(LoadSharedLibrary (pI_RUNTIME_MAKE_SHARED_LIB_NAME("pIApplicationImpl")), UnloadSharedLibrary);
	_sharedLibs = application::CreateLibraryList (_runtime.GetCRuntime(), _impl);
#else // #ifndef pI_STATIC_LIBRARY
	_sharedLibs = SharedLibraryListSharedPtr (pIAppCreatePluginLibraryList (_runtime.GetCRuntime(), 2));
	_sharedLibs = application::CreateLibraryList (_runtime.GetCRuntime());
#endif // #ifndef pI_STATIC_LIBRARY
	AddDefaultProperties();
}


#ifndef SWIG
	Application::Application(
     const CRuntimePtr runtime,
     const pI_bool take_ownership /*= pI_FALSE*/,
     const pI_bool load_properties /*= pI_FALSE*/):
	  _runtime(runtime, take_ownership, load_properties) {
#ifndef pI_STATIC_LIBRARY
	_impl = application::SharedLibrarySharedPtr(LoadSharedLibrary (pI_RUNTIME_MAKE_SHARED_LIB_NAME("pIApplicationImpl")), UnloadSharedLibrary);
	_sharedLibs = application::CreateLibraryList (_runtime.GetCRuntime(), _impl);
#else // #ifndef pI_STATIC_LIBRARY
	_sharedLibs = application::CreateLibraryList (_runtime.GetCRuntime());
#endif // #ifndef pI_STATIC_LIBRARY
	AddDefaultProperties();
}
#endif // #ifndef SWIG


Application::Application (Runtime& runtime): _runtime(runtime) {
#ifndef pI_STATIC_LIBRARY
	_impl = application::SharedLibrarySharedPtr(LoadSharedLibrary (pI_RUNTIME_MAKE_SHARED_LIB_NAME("pIApplicationImpl")), UnloadSharedLibrary);
	_sharedLibs = application::CreateLibraryList (_runtime.GetCRuntime(), _impl);
#else // #ifndef pI_STATIC_LIBRARY
	_sharedLibs = application::CreateLibraryList (_runtime.GetCRuntime());
#endif // #ifndef pI_STATIC_LIBRARY
	AddDefaultProperties();
}


Application::Application(Application& app): _impl(app._impl), _runtime(app._runtime), _sharedLibs(app._sharedLibs) {}


/*virtual*/ Application::~Application() {
	_runtime.GetCRuntime()->DestroyRegisteredPlugins (_runtime.GetCRuntime());
}


/*inline*/ void Application::LoadPluginLibrary (const pI_char* libName) {

#ifndef pI_STATIC_LIBRARY
	pIAppLoadPluginLibraryFunc dfunc =
	 (pIAppLoadPluginLibraryFunc) GetSharedLibraryFunction (_impl.get(), "pIAppLoadPluginLibrary");
	pIAppAddToPluginLibraryListFunc afunc =
	 (pIAppAddToPluginLibraryListFunc) GetSharedLibraryFunction (_impl.get(), "pIAppAddToPluginLibraryList");
	if (dfunc == 0 || afunc == 0) {
	    throw pI::exception::PluginLibraryException ("pureImage application shared library was not found.");
    }
	struct _SharedLibrary* sl =
	 reinterpret_cast<struct _SharedLibrary* > (dfunc (_runtime.GetCRuntime(), const_cast<pI_char* > (libName)));
    pI_bool ok = afunc (_runtime.GetCRuntime(), sl, _sharedLibs.get());
#else // #ifndef pI_STATIC_LIBRARY
	struct _SharedLibrary* sl =
	 reinterpret_cast<struct _SharedLibrary* > (
	  pIAppLoadPluginLibrary (_runtime.GetCRuntime(), const_cast<pI_char* > (libName)));
	pI_bool ok = pIAppAddToPluginLibraryList (_runtime.GetCRuntime(), sl, _sharedLibs.get());
#endif // #ifndef pI_STATIC_LIBRARY

    if (ok == pI_FALSE) {
        throw pI::exception::PluginLibraryException (
                std::string ("Failed to load plugin library ").append (libName).c_str());
    }
} // const pI_bool Application::LoadPluginLibrary (const pI_char* libName)


/*inline*/ void Application::LoadPluginLibrary (const pI_char* path, const pI_char* libName) {

#ifndef pI_STATIC_LIBRARY
	pIAppLoadPluginLibraryWithPathFunc dfunc =
	 (pIAppLoadPluginLibraryWithPathFunc) GetSharedLibraryFunction (_impl.get(), "pIAppLoadPluginLibraryWithPath");
	pIAppAddToPluginLibraryListFunc afunc =
	 (pIAppAddToPluginLibraryListFunc) GetSharedLibraryFunction (_impl.get(), "pIAppAddToPluginLibraryList");
	if (dfunc == 0 || afunc == 0) {
	    throw pI::exception::PluginLibraryException ("pureImage application shared library was not found.");
    }
	struct _SharedLibrary* sl =
	 reinterpret_cast<struct _SharedLibrary* > (
	  dfunc (_runtime.GetCRuntime(), const_cast<pI_char* > (path), const_cast<pI_char* > (libName)));
	pI_bool ok = afunc (_runtime.GetCRuntime(), sl, _sharedLibs.get());
#else // #ifndef pI_STATIC_LIBRARY
	struct _SharedLibrary* sl =
	 reinterpret_cast<struct _SharedLibrary* > (
	  pIAppLoadPluginLibrary (_runtime.GetCRuntime(), const_cast<pI_char* > (path), const_cast<pI_char* > (libName)));
	pI_bool ok = pIAppAddToPluginLibraryList (_runtime.GetCRuntime(), sl, _sharedLibs.get());
#endif // #ifndef pI_STATIC_LIBRARY

    if (ok == pI_FALSE) {
        throw pI::exception::PluginLibraryException (
                std::string ("Failed to load plugin library ").append (libName).c_str());
    }
} // const pI_bool Application::LoadPluginLibrary (const pI_char* path, const pI_char* libName)


/*inline*/ void Application::LoadPluginLibraries (
 const pI_char* path /*= 0*/,
 const pI_bool recursive /*= pI_TRUE*/,
 const pI_bool auto_register /*= pI_TRUE*/) {

#ifndef pI_STATIC_LIBRARY
	pIAppLoadPluginLibrariesFunc lfunc =
	 (pIAppLoadPluginLibrariesFunc) GetSharedLibraryFunction (_impl.get(), "pIAppLoadPluginLibraries");
	pIAppDeletePluginLibraryListFunc dfunc =
	 (pIAppDeletePluginLibraryListFunc) GetSharedLibraryFunction (_impl.get(), "pIAppDeletePluginLibraryList");
	pIAppMergePluginLibraryListsFunc mfunc =
	 (pIAppMergePluginLibraryListsFunc) GetSharedLibraryFunction (_impl.get(), "pIAppMergePluginLibraryLists");
	if (lfunc == 0 || dfunc == 0 || mfunc == 0) {
	    throw pI::exception::PluginLibraryException ("pureImage application shared library was not found.");
    }

    pI_bool ok = pI_FALSE;
	pI_char* p = const_cast<pI_char* >(path);
	if ((p == 0) && _runtime.HasProperty ("PUREIMAGE_LIBRARY_PATH")) {
		p = _runtime.GetCRuntime()->GetProperty (_runtime.GetCRuntime(), "PUREIMAGE_LIBRARY_PATH", pI_FALSE)->data.StringValue->data;
	}
	struct _pIAppPluginLibraryList* list = 
	 lfunc (_runtime.GetCRuntime(), p != 0 ? p : FindPIPluginDir().c_str(), recursive, auto_register);

	if (auto_register == pI_TRUE) {
		if (list != 0) {
			dfunc (_runtime.GetCRuntime(), list, pI_TRUE, pI_FALSE);
			ok = pI_TRUE;
		}
	} else {
		if (list != 0)
			ok = mfunc (_runtime.GetCRuntime(), _sharedLibs.get(), list);
	}

#else // #ifndef pI_STATIC_LIBRARY
	if (auto_register == pI_TRUE) {
		struct _pIAppPluginLibraryList* list = pIAppLoadPluginLibraries (_runtime.GetCRuntime(), path, recursive, pI_TRUE);
		if (list != 0)
			pIAppDeletePluginLibraryList (_runtime.GetCRuntime(), list, pI_TRUE, pI_FALSE);
            ok = pI_TRUE;
	} else {
		struct _pIAppPluginLibraryList* list = pIAppLoadPluginLibraries (_runtime.GetCRuntime(), path, recursive, pI_FALSE);
		if (list != 0)
			ok = pIAppMergePluginLibraryLists (_runtime.GetCRuntime(), _sharedLibs.get(), list);
	}
#endif // #ifndef pI_STATIC_LIBRARY
    if (ok == pI_FALSE) {
        throw pI::exception::PluginLibraryException (
                std::string ("Failed to load plugin libraries from ").append (path).c_str());
    }
} // const pI_bool Application::LoadPluginLibraries (...)


/*inline*/ pI_size Application::RegisterPlugins() {

#ifndef pI_STATIC_LIBRARY
	pIAppRegisterPluginListFunc rfunc =
	 (pIAppRegisterPluginListFunc) GetSharedLibraryFunction (_impl.get(), "pIAppRegisterPluginList");
	if (rfunc == 0)
		return 0;
	return rfunc (_runtime.GetCRuntime(), _sharedLibs.get());
#else // #ifndef pI_STATIC_LIBRARY
	return pIAppRegisterPluginList (_runtime.GetCRuntime(), _sharedLibs.get());
#endif // #ifndef pI_STATIC_LIBRARY
} // pI_size Application::RegisterPlugins()


/*inline*/ std::string Application::FindPIBaseDir() const {

#ifndef pI_STATIC_LIBRARY
	pIAppFindBaseDirFunc dfunc =
	 (pIAppFindBaseDirFunc) GetSharedLibraryFunction (_impl.get(), "pIAppFindBaseDir");
	if (dfunc == 0)
		return 0;
	char* bd = const_cast<char* > (dfunc (_runtime.GetCRuntime()));
#else // #ifndef pI_STATIC_LIBRARY
	char* bd = const_cast<char* > (pIAppFindBaseDir (_runtime.GetCRuntime()));
#endif // #ifndef pI_STATIC_LIBRARY
	std::string rv(bd);
	_runtime.FreeString (bd);
	return rv;
} // std::string Application::FindPIBaseDir() const

/*inline*/ std::string Application::FindPIPluginDir() const {

#ifndef pI_STATIC_LIBRARY
	pIAppFindBaseDirFunc dfunc =
	 (pIAppFindBaseDirFunc) GetSharedLibraryFunction (_impl.get(), "pIAppFindPluginDir");
	if (dfunc == 0)
		return 0;
	char* bd = const_cast<char* >(dfunc (_runtime.GetCRuntime()));
#else // #ifndef pI_STATIC_LIBRARY
	char* bd = const_cast<char* > (pIAppFindPluginDir (_runtime.GetCRuntime()));
#endif // #ifndef pI_STATIC_LIBRARY
	std::string rv(bd);
	_runtime.FreeString (bd);
	return rv;
} // std::string Application::FindPIPluginDir() const


pInPtr Application::SpawnAndInitialize (const pI_char* pluginName, ...) {

    pInPtr retval (_runtime.SpawnPlugin (pluginName));

    if (GetPInPtr (retval) == 0) {
        return retval;
    }

    pI::Arguments params (retval->GetParameterSignature());
    // now comes the evil and unsafe hack (not type safe, only support for limited types, serious problems when too few parameters are given)...
    va_list argptr;
    va_start (argptr, pluginName);

    for (pI_size lv = 0; lv < params.size(); ++lv) {
        switch (params[lv]->signature.type) {
            case T_pI_Argument_IntValue: {
                pI::IntValue (params[lv]).SetData (va_arg (argptr, pI_int));
                break;
            }
            case T_pI_Argument_StringValue: {
                pI::StringValue (params[lv]).SetData (va_arg (argptr, pI_str));
                break;
            }
            case T_pI_Argument_BoolValue: {
                pI::BoolValue (params[lv]).SetData (va_arg (argptr, pI_bool));
                break;
            }
            case T_pI_Argument_DoubleValue: {
                pI::DoubleValue (params[lv]).SetData (va_arg (argptr, pI_double));
                break;
            }
            case T_pI_Argument_StringSelection: {
                pI::StringSelection (params[lv]).SetIndex (va_arg (argptr, pI_size));
                break;
            }
            default:
                throw pI::exception::IncompatibleParameterException ("Failed to fast-construct plugin with given parameters.");
        }
    }

    va_end (argptr);
    retval->Initialize (params);
    return retval;
} // pInPtr Application::SpawnAndInitialize (const pI_char* plugin, ...)


/*inline*/ void Application::AddDefaultProperties() {

	if (!_runtime.HasProperty ("PUREIMAGE_PATH")) {
		_runtime.StoreProperty (StringValue(_runtime).SetName("PUREIMAGE_PATH").SetData(FindPIBaseDir().c_str()));
	}
	if (!_runtime.HasProperty ("PUREIMAGE_LIBRARY_PATH")) {
		_runtime.StoreProperty (StringValue(_runtime).SetName("PUREIMAGE_LIBRARY_PATH").SetData(FindPIPluginDir().c_str()));
	}
} // /*inline*/ void Application::AddDefaultProperties()


} // namespace pI

#endif // PUREIMAGE_ADAPTERS_CPP_APPLICATION_HPP
