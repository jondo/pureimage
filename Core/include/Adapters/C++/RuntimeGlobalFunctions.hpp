/* RuntimeGlobalFunctions.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universit�t Linz,
 * Institut f�r Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#include <PureImage.h>
#include <Adapters/C++/RuntimeImpl.h>

#ifndef pI_STATIC_LIBRARY
#include <BuildSharedLibrary.h>
#include <SharedLibrary.h>
#endif // pI_STATIC_LIBRARY

namespace pI {

namespace runtime {

/* _Global_ functions */

static inline void VetoDelete (const void*) {}

#ifndef pI_STATIC_LIBRARY

static inline void CleanUpCRuntime (struct _CRuntime* rt) {

	SharedLibrary** injectptr = 0;
	DestroyCRuntimeFunc dfunc = 0;

	if (rt == 0)
		return;
	injectptr = (SharedLibrary**) rt->shared_library_data;

	dfunc = (DestroyCRuntimeFunc) GetSharedLibraryFunction (injectptr[0], "DestroyCRuntime");
	if (dfunc == 0)
		return;

	dfunc (rt, pI_FALSE);
	UnloadSharedLibrary (injectptr[1]);
	UnloadSharedLibrary (injectptr[0]);
} // static inline void CleanUpCRuntime (struct _CRuntime* rt)

static inline void CleanUpCRuntimeWriteProperties (struct _CRuntime* rt) {

	SharedLibrary** injectptr = 0;
	DestroyCRuntimeFunc dfunc = 0;

	if (rt == 0)
		return;
	injectptr = (SharedLibrary**) rt->shared_library_data;

	dfunc = (DestroyCRuntimeFunc) GetSharedLibraryFunction (injectptr[0], "DestroyCRuntime");
	if (dfunc == 0)
		return;

	dfunc (rt, pI_TRUE);
	UnloadSharedLibrary (injectptr[1]);
	UnloadSharedLibrary (injectptr[0]);
} // static inline void CleanUpCRuntimeWriteProperties (struct _CRuntime* rt)

static inline CRuntimePtr CreateCRuntime (const pI_bool load_properties = pI_FALSE) {

	SharedLibrary* crtsl = 0;
	SharedLibrary* cppimplsl = 0;
	CreateCRuntimeFunc cfunc = 0;
	CRuntimePtr crt = 0;
	SharedLibrary** injectptr = 0;

	// load functionality from shared library
	crtsl = LoadSharedLibrary (pI_RUNTIME_MAKE_SHARED_LIB_NAME("pIRuntime"));
	if (crtsl == 0)
		return 0;

	cppimplsl = LoadSharedLibrary (pI_RUNTIME_MAKE_SHARED_LIB_NAME("pICppRuntimeImpl"));
	if (cppimplsl == 0) {
		UnloadSharedLibrary (crtsl);
		return 0;
	}

	// fetch runtime creation function from shared library
	cfunc = (CreateCRuntimeFunc) GetSharedLibraryFunction (crtsl, "CreateCRuntime");
	if (cfunc == 0) {
		UnloadSharedLibrary (crtsl);
		UnloadSharedLibrary (cppimplsl);
		return 0;
	}

	// instantiate runtime
	crt = cfunc (load_properties);
	if (crt == 0) {
		UnloadSharedLibrary (crtsl);
		UnloadSharedLibrary (cppimplsl);
		return 0;
	}

	// inject shared libraries into runtime
	injectptr = (SharedLibrary**) crt->AllocateMemory (crt, sizeof(SharedLibrary*) * 2);
	injectptr[0] = crtsl;
	injectptr[1] = cppimplsl;
	crt->shared_library_data = injectptr;
	return crt;
} // static inline CRuntimePtr CreateCRuntime (const pI_bool load_properties = pI_FALSE)

static inline pI_int GetAPIVersion (struct _CRuntime* rt) {

	SharedLibrary** injectptr = 0;
	GetpIAPIVersionFunc vfunc = 0;

	if (rt == 0)
		return -1;
	injectptr = (SharedLibrary**) rt->shared_library_data;

	vfunc = (GetpIAPIVersionFunc) GetSharedLibraryFunction (injectptr[0], "GetpIAPIVersion");
	if (vfunc == 0)
		return -1;

	return vfunc();
} // static inline void CleanUpCRuntime (struct _CRuntime* rt)

static inline CRuntimeError CppRuntimeRegisterPlugin (CRuntimePtr crt, void* ip) {

	SharedLibrary** injectptr = 0;
	CppRuntimeRegisterPluginFunc regfunc = 0;

	if (crt == 0)
		return pI_CRuntime_Error_InvalidInstance;
	injectptr = (SharedLibrary**) crt->shared_library_data;

	regfunc = (CppRuntimeRegisterPluginFunc) GetSharedLibraryFunction (injectptr[1], "CppRuntimeRegisterPlugin");
	if (regfunc == 0)
		return pI_CRuntime_Error_InvalidInstance;

	return regfunc (crt, ip);
} // static inline CRuntimeError CppRuntimeRegisterPlugin (CRuntimePtr runtime, void* ip)

static inline void* CppRuntimeSpawnPlugin (CRuntimePtr crt, const pI_char* name) {

	SharedLibrary** injectptr = 0;
	CppRuntimeSpawnPluginFunc sfunc = 0;

	if (crt == 0)
		return 0;
	injectptr = (SharedLibrary**) crt->shared_library_data;

	sfunc = (CppRuntimeSpawnPluginFunc) GetSharedLibraryFunction (injectptr[1], "CppRuntimeSpawnPlugin");
	if (sfunc == 0)
		return 0;

	return sfunc (crt, name);
} // static inline void* CppRuntimeSpawnPlugin (CRuntimePtr crt, , const pI_char* name)

static inline CRuntimeError CppRuntimeErasePlugin (CRuntimePtr crt, void* ip) {

	SharedLibrary** injectptr = 0;
	CppRuntimeErasePluginFunc efunc = 0;

	if (crt == 0)
		return pI_CRuntime_Error_InvalidInstance;
	injectptr = (SharedLibrary**) crt->shared_library_data;

	efunc = (CppRuntimeErasePluginFunc) GetSharedLibraryFunction (injectptr[1], "CppRuntimeErasePlugin");
	if (efunc == 0)
		return pI_CRuntime_Error_InvalidInstance;

	efunc (ip);
	return pI_CRuntime_Error_NoError;
} // static inline CRuntimeError CppRuntimeErasePlugin (void* ip)

#else // #ifndef pI_STATIC_LIBRARY

static inline void CleanUpCRuntime (struct _CRuntime* rt) {
	DestroyCRuntime (rt, pI_FALSE);
}

static inline void CleanUpCRuntimeWriteProperties (struct _CRuntime* rt) {
	DestroyCRuntime (rt, pI_TRUE);
}

static inline CRuntimePtr CreateCRuntime (const pI_bool load_properties = pI_FALSE) {
	return CreateCRuntime (load_properties);
}

static inline pI_int GetAPIVersion (struct _CRuntime* rt) {
	return GetpIAPIVersion();
}

static inline CRuntimeError CppRuntimeRegisterPlugin (CRuntimePtr crt, void* ip) {
	return CppRuntimeRegisterPlugin (crt, ip);
}

static inline void* CppRuntimeSpawnPlugin (CRuntimePtr crt, const pI_char* name) {
	return CppRuntimeSpawnPlugin (crt, name);
}

static inline CRuntimeError CppRuntimeErasePlugin (CRuntimePtr, void* ip) {
	CppRuntimeErasePlugin (ip);
	return pI_CRuntime_Error_NoError;
}

#endif // #ifndef pI_STATIC_LIBRARY

} // namespace runtime

} // namespace pI
