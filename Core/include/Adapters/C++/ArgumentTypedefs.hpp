/* ArgumentTypedefs.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_ADAPTERS_CPP_ARGUMENTTYPEDEFS_HPP
#define PUREIMAGE_ADAPTERS_CPP_ARGUMENTTYPEDEFS_HPP

#ifndef PI_ADAPT_ARGUMENTS
#include <boost/shared_ptr.hpp>
#else
#include <Adapters/C++/Arguments/ArgumentAdapter.hpp>
#endif // PI_ADAPT_ARGUMENTS

#include <vector>

namespace pI {

/**
      Defines an abstraction of pointers to arguments.

      In C++-pure applications it is desired to have an automatic memory management
      using shared pointers and access the C arguments directly.

      On other platforms (e.g. Java) the C++ argument adapters shall be used
      (ported by Swig) and memory management is done by the target environment.
*/

typedef Argument* CArgumentPtr;

// The Argument pointer type
// Argument uses a boost shared_ptr for automated resource deallocation.
typedef boost::shared_ptr<Argument> ArgumentPtr;

/// Arguments = Argument-vector
typedef std::vector<ArgumentPtr> Arguments;

/**
    This (inline) function is used to have a uniform access to the
    C argument referenced by the argument pointer.
*/
inline
CArgumentPtr GetCArgument (ArgumentPtr arg)
{
    return arg.get();
} // CArgumentPtr GetCArgument (ArgumentPtr arg)

} // namespace pI

#endif // PUREIMAGE_ADAPTERS_CPP_ARGUMENTTYPEDEFS_HPP
