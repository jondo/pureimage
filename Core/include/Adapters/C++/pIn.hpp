/* pIn.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_ADAPTERS_CPP_PIN_HPP
#define PUREIMAGE_ADAPTERS_CPP_PIN_HPP

#include <string>
#include <sstream>
#include <PureImage.h>
#include <Adapters/C++/Runtime.hpp>
#include <Adapters/C++/ArgumentTypedefs.hpp>
#include <Adapters/C++/pInException.hpp>

#ifndef MEMBER_SIGNATURE
#ifdef pI_BUILD_SHARED_LIBRARY
#define MEMBER_SIGNATURE(x) x
#else /* #ifdef pI_BUILD_SHARED_LIBRARY */
#define MEMBER_SIGNATURE(x) _runtime.CopyArguments (x)
#endif /* #ifdef pI_BUILD_SHARED_LIBRARY */
#endif /* #ifndef MEMBER_SIGNATURE */

#ifndef DECLARE_VIRTUAL_COPY_CONSTRUCTOR
#define DECLARE_VIRTUAL_COPY_CONSTRUCTOR(class_name) \
	virtual pInPtr Create() const { \
		return pInPtr(new class_name(_runtime)); \
	}
#endif /* DECLARE_VIRTUAL_COPY_CONSTRUCTOR */

/** The pureImage namespace.
*/
namespace pI {

/** Interface for image plugins.
    Every image plugin must derive from pIn. A typical workflow includes these steps:

    -# Create or acquire an image plugin.
    -# Optionally, retrieve and process meta information, such as GetAPIVersion(),
                 GetpInVersion(), GetName(), GetAuthor(), GetCopyright(), and GetDescription().
    -# Ask plugin for its parameters using GetParameterSignature().
    -# Initialize the plugin using the Initialize() method with the desired parameter set.
    -# Ask plugin for its input and output signature, using GetInputSignature()
       and GetOutputSignature().
    -# Finally, call Execute() using properly setup input and output signatures.

    @note Although parameters and arguments share the same type internally,
          parameters are handled by the Initialize() method, whereas
          arguments are handled by Execute().
*/
class pIn {

	friend class CpInAdapterBase;

public:

    /** Constructor.
        Expects a runtime instance to be used by the plugin
        for memory allocation purposes or plugin queries.
        @exception MemoryException */
	pIn(Runtime runtime): _runtime(runtime) {}

    /// Destructor (virtual).
    virtual ~pIn() {}

    /// Method to retrieve the runtime of the plugin.
    Runtime GetRuntime() const {
        return _runtime;
    }

    /** Create function to supply virtual copy constructor functionality.
		In order to simplify the implementation of this function, a special
		macro, DECLARE_VIRTUAL_COPY_CONSTRUCTOR, can be used.
        @return New heap-constructed, uninitialized and stateless instance
                of the same type. */
    virtual pInPtr Create() const = 0;

    /** Clone function to retrieve a stateful copy of the instance.
        The default implementation relies on the Serialize and
        Deserialize methods for stateful copying, therefore stateful
        plugins must provide an implementation of these.
        @return Stateful copy of the actual instance. */
    virtual pInPtr Clone() const {

		pInPtr instance(Create());
		std::stringstream ser_data;

		try {
			Serialize (ser_data);
			instance->Deserialize (ser_data.str());
		} catch (...) {}

		return instance;
	} // virtual const pIn* pIn::Clone() const

    /** Method to retrieve the API version.
        The API version may be used to enforce binary compatibility
        between runtime and plugin or even between multiple plugins.
        @return API version as unsigned integer. */
    const pI_int GetAPIVersion() const {
        return PI_API_VERSION;
    } // const pI_size GetAPIVersion() const

    /** Method to retrieve the version of the plugin.
        The plugin version can freely be set, although the following
        encoding is encouraged: xxyyzz, where xx specifies the major
        revision, yy the minor and zz the patch revision.
        Note: instead of 00003 for major 0 and minor 0 and patch 3,
              one should just write 3, because otherwise the number
              is interpreted as octal (leading zero). A value of 00008
              would lead to a syntax error on compilation.
        GetpInVersion allows a plugin to enforce the proper version
        of any plugin it might depend on.
        @return Plugin version as unsigned integer. */
    virtual const pI_int GetpInVersion() const = 0;

    /** Returns the author of the plugin.
        @return The author, or empty string if not set. */
    virtual const std::string GetAuthor() const {
        return std::string();
    } // virtual const std::string GetAuthor() const

    /** Returns the copyright notice of the plugin.
        @return The copyright of the plugin, or empty string if not set. */
    virtual const std::string GetCopyright() const {
        return std::string();
    } // virtual const std::string GetCopyright() const

    /** Returns the description of the plugin.
        @return The plugin description. */
    virtual const std::string GetDescription() const = 0;

    /** Returns the unique name of the plugin.
        The name should contain also its category, the individual
        fields are seperated by '/'. Example:
        "Fingerprint/NBIS/DetectMinutiae".
        @return The name (human readable id) of the plugin. */
    virtual const std::string GetName() const = 0;

    /** Method to retrieve the parameter signature of the plugin.
        A parameter setting using the same types in the same order
        as returned by this function must be used to Initialize
        the plugin.
        @exception MemoryException
        @return The parameter signature Initialize expects. */
    virtual Arguments GetParameterSignature() const = 0;

    /** Method to initialize the plugin using given parameters.
        The parameters must be perfectly compatible to the parameter signature
        the plugin expects, i.e. with the return value of GetParameterSignature().
        After Initialize() was called, both input and output signatures
        may be queried via GetInputSignature() and GetOutputSignature().
        @exception MissingParameterException
        @exception IncompatibleParameterException
        @exception MemoryException
        @exception InitializationException
        @param parameters Parameters to initialize plugin with */
    inline void Initialize (const Arguments& parameters) {

		_Initialize (parameters);
	} // void pIn::Initialize (const Arguments& parameters)

    /** Method to query the input argument signature of the plugin.
        The signature returned by this function is guaranteed to be
        valid only after Initialize was called successfully.
        Note: The input signature may contain arguments which are
              flagged both input and output arguments.
        @exception MemoryException
        @exception NotInitializedException
        @return Input signature of the plugin. */
    virtual Arguments GetInputSignature() const = 0;

    /** Method to query the output argument signature of the plugin.
        The signature returned by this function is guaranteed to be
        valid only after Initialize was called successfully.
        Note: The output signature must never contain arguments which
              are flagged both input and output arguments.
        @exception MemoryException
        @exception NotInitializedException
        @return Output signature of the plugin. */
    virtual Arguments GetOutputSignature() const = 0;

    /** Worker method of the plugin.
        This overload is the primary form and must be implemented by
        each plugin. It relies on pre-allocated arguments and therefore
        is the fastest form - it may even re-use the memory from previous
        iterations.
        @exception NotInitializedException
        @exception MissingArgumentException
        @exception IncompatibleArgumentException
        @exception IncompleteArgumentException
        @exception ExecutionException
        @exception MemoryException
        @param input_args Input arguments to base calculations on.
        @param output_args Output arguments. */
    void Execute (Arguments& input_args, Arguments& output_args) {

		Arguments inputargs(input_args), outputargs(output_args);
		_Execute (inputargs, outputargs);
		_runtime.SwapArguments (input_args, inputargs);
		_runtime.SwapArguments (output_args, outputargs);
	} // void pIn::Execute (Arguments& input_args, Arguments& output_args)

    /** Worker method of the plugin.
        This overload returns the resulting output arguments as copy,
        therefore, depending on the actual algorithm, it may be
        slower than the other version.
        @exception NotInitializedException
        @exception MissingArgumentException
        @exception IncompatibleArgumentException
        @exception IncompleteArgumentException
        @exception ExecutionException
        @exception MemoryException
        @param input_args Input arguments to base calculations on.
        @return Resulting output arguments. */
    Arguments Execute (Arguments& input_args) {

		Arguments output_args(GetOutputSignature());
		_Execute (input_args, output_args);
		return output_args;
	} // Arguments pIn::Execute (Arguments& input_args)

    /** Method to serialize the plugin state.
        Each plugin may provide its own serialization mechanism, it
        only must be capable of deserialize itself using the serialized
        data.
        Note: If Serialize is called on an uninitialized pIn, it
              must return false.
        @exception SerializationException
        @exception SerializationNotSupportedException
        @exception MemoryException
        @param serialization_data string to serialize plugin into */
    virtual void Serialize (std::stringstream& serialization_data) const {
        throw exception::SerializationNotSupportedException("Plugin serialization not implemented.");
    } // virtual const pI_bool Serialize

    /** Method to deserialize the plugin state.
        Each plugin may provide its own serialization mechanism, it only
        must be capable of deserialize itself.
        @exception DeserializationException
        @exception DeserializationNotSupportedException
        @exception MemoryException
        @param serialization_data string to construct plugin with */
    virtual void Deserialize (const std::string& serialization_data) {
        throw exception::DeserializationNotSupportedException("Plugin deserialization not implemented.");
    } // virtual const pI_bool Deserialize

protected:

	/** Method to initialize the plugin using given parameters.
        The parameters must be perfectly compatible to the parameter signature
        the plugin expects, i.e. with the return value of GetParameterSignature().
        After Initialize() was called, both input and output signatures
        may be queried via GetInputSignature() and GetOutputSignature().
        @exception MissingParameterException
        @exception IncompatibleParameterException
        @exception MemoryException
        @exception InitializationException
        @param parameters Parameters to initialize plugin with */
    virtual void _Initialize (const Arguments& parameters) = 0;

	/** Worker method of the plugin.
        This overload is the primary form and must be implemented by
        each plugin. It relies on pre-allocated arguments and therefore
        is the fastest form - it may even re-use the memory from previous
        iterations.
        @exception NotInitializedException
        @exception MissingArgumentException
        @exception IncompatibleArgumentException
        @exception IncompleteArgumentException
        @exception ExecutionException
        @exception MemoryException
        @param input_args Input arguments to base calculations on.
        @param output_args Output arguments. */
    virtual void _Execute (Arguments& input_args, Arguments& output_args) = 0;

    mutable Runtime _runtime;

}; // class pIn

} // namespace pI

#endif // PUREIMAGE_ADAPTERS_CPP_PIN_HPP
