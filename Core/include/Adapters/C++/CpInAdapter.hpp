/* CpInAdapter.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_ADAPTERS_CPP_CPINADAPTER_HPP
#define PUREIMAGE_ADAPTERS_CPP_CPINADAPTER_HPP

#include <PureImage.h>
#include <Adapters/C++/pIn.hpp>

namespace pI {

/** Adapter base class, which cannot be instantiated.
    CpInAdapterBase provides the @see GetCpIn method,
    which returns a valid CpIn instance. */
class CpInAdapterBase {

    class AdapterImpl;
    friend class CloningAdapter;

public:

    /// Destructor (virtual).
    virtual ~CpInAdapterBase();

    /// Method to actually retrieve the wrapped @see CpIn instance.
    const CpInPtr GetCpIn();

protected:

    CpInAdapterBase(Runtime runtime);
    CpInAdapterBase(const CRuntimePtr cruntime);

    virtual pInPtr GetpIn() = 0;

	virtual void _Execute (Arguments& input_args, Arguments& output_args) {

		GetpIn()->_Execute (input_args, output_args);
	}

    AdapterImpl* _impl;
    Runtime _runtime;
    bool _delete_impl;
}; // class CpInAdapterBase

/** Adapter class, which takes the pIn to wrap as template parameter. */
template<class T>
class CpInAdapter: public CpInAdapterBase {

public:

    // use named constructor ideom to enforce heap construction
    /** Named constructor to actually construct a new adapter instance.
        @param runtime Runtime instance.
        @return heap-constructed adapter instance. */
    static const CpInAdapter* Create(Runtime& runtime) {
        return new CpInAdapter(runtime);
    }

    static const CpInAdapter* Create(const CRuntimePtr runtime) {
        return new CpInAdapter(runtime);
    }

protected:

    CpInAdapter(Runtime runtime):
        CpInAdapterBase(runtime) {}


    CpInAdapter(const CRuntimePtr runtime):
        CpInAdapterBase(runtime) {}


    virtual pInPtr GetpIn() {
        if (_plugin.get() == 0) {
            _plugin = boost::shared_ptr<T>(new T(_runtime));
        }

        return _plugin;
    }

	boost::shared_ptr<T> _plugin;

}; // class CpInAdapter

/** Adapter class which takes an existing pIn instance. */
class CpInCloningAdapter: public CpInAdapterBase {

public:

    // use named constructor ideom to enforce heap construction
    /** Named constructor to actually construct a new adapter instance.
        @param instance pIn instance for adapter.
        @param own_plugin If pI_TRUE, the instance will be deleted automatically.
        @return heap-constructed adapter instance. */
    static const CpInCloningAdapter* Create(pInPtr instance) {
        return new CpInCloningAdapter(instance);
    }

protected:

    CpInCloningAdapter(pInPtr instance):
        CpInAdapterBase(instance->GetRuntime()),
        _instance(instance) {}

    virtual pInPtr GetpIn() {
        return _instance;
    }

private:

    pInPtr _instance;
}; // class CloningAdapter: public CpInAdapterBase

} // namespace pI

#endif // PUREIMAGE_ADAPTERS_CPP_CPINADAPTER_HPP
