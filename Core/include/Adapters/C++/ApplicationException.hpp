/* ApplicationException.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_ADAPTERS_CPP_APPLICATION_EXCEPTION_HPP
#define PUREIMAGE_ADAPTERS_CPP_APPLICATION_EXCEPTION_HPP

#include "pIException.hpp"

namespace pI {
/** The namespace containing all exceptions thrown by pureImage.
*/
namespace exception {

/*  Short overview:

    pI::exception::Exception
     |
     +--- ApplicationException
           +--- PluginLibraryException

*/

/** pIn general runtime exception.
    General runtime exception class. */
class ApplicationException: public Exception {
    typedef Exception Base;
public:
    ApplicationException(const char* message):
        Base(message) {
        ;
    }
}; // class ApplicationException: public Exception

/**
 * A general plugin library exception.
 *
 * This exception is thrown when a plugin library could not be loaded
 * (e.g., missing library or missing factory method).
 */
class PluginLibraryException: public ApplicationException {
    typedef ApplicationException Base;
public:
    PluginLibraryException(const char* message):
        Base(message) {
        ;
    }
}; // class PluginLibraryException(const char* message): public ApplicationException

} // namespace exception
} // namespace pI

#endif // PUREIMAGE_ADAPTERS_CPP_APPLICATION_EXCEPTION_HPP
