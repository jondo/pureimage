/* RuntimeImpl.h
 *
 * Copyright 2010-2011 Johannes Kepler Universit�t Linz,
 * Institut f�r Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_ADAPTERS_CPP_RUNTIME_IMPL_H
#define PUREIMAGE_ADAPTERS_CPP_RUNTIME_IMPL_H

#include <PureImage.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef pI_STATIC_LIBRARY

#include <BuildSharedLibrary.h>

	typedef /*pI_API*/ CRuntimeError (*CppRuntimeRegisterPluginFunc) (CRuntimePtr runtime, void* ip);
	typedef /*pI_API*/ void* (*CppRuntimeSpawnPluginFunc) (CRuntimePtr runtime, const pI_char* name);
	typedef /*pI_API*/ void (*CppRuntimeErasePluginFunc) (void* ip);

	pI_API CRuntimeError CppRuntimeRegisterPlugin (CRuntimePtr runtime, void* ip);
	pI_API void* CppRuntimeSpawnPlugin (CRuntimePtr runtime, const pI_char* name);
	pI_API void CppRuntimeErasePlugin (void* ip);

#else /* #ifndef pI_STATIC_LIBRARY */

	extern CRuntimeError CppRuntimeRegisterPlugin (CRuntimePtr runtime, void* ip);
	extern void* CppRuntimeSpawnPlugin (CRuntimePtr runtime, const pI_char* name);
	extern void CppRuntimeErasePlugin (void* ip);

#endif /* #ifndef pI_STATIC_LIBRARY */

#ifdef __cplusplus
}
#endif

#endif // PUREIMAGE_ADAPTERS_CPP_RUNTIME_IMPL_H
