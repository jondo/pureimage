/* pIException.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_ADAPTERS_CPP_PI_EXCEPTION_HPP
#define PUREIMAGE_ADAPTERS_CPP_PI_EXCEPTION_HPP

#include <exception>
#include <string>

/* NOTE: it would be nice to have throw declarations in functions.
          Unfortunately, MSVC does currently not support typed throw declarations (such as throw(std::exception))
          and yields annoying warnings.
          Primarily, this comes in handy for using Swig, as it then does the adaptation
          of exception handling automatically. Secondly, this is also kind of documentation.

          As these declarations should currently not influence the program behaviour
          it seems to be safe to disable the warning C4290.
*/
#if defined _MSC_VER
#pragma warning( disable : 4290 )
#endif

namespace pI {
/** The namespace containing all exceptions thrown by pureImage.
*/
namespace exception {

/*  Short overview:

    pI::exception::Exception
     |
     +--- MemoryException
     +--- MissingRuntimeException

*/

// HACK: SWIG has, for whatever reasons, problems with this definition
#ifndef SWIG

/** pIn exception base class. */
class Exception: public std::exception {
public:

    /// Default constructor
    Exception(): exception() {}

    /** Constructor.
        @param message Exception message to show. */
    Exception(const char* message): exception(), _msg(message) {}

    /// Destructor (virtual)
    virtual ~Exception() throw() = 0;

    /// exception what overload
    virtual const char* what() const throw() {

        return _msg.c_str();
    }

protected:

    std::string _msg;

}; // class Exception: public std::exception

/*virtual*/ inline Exception::~Exception() throw() {}

#else

class Exception: public std::exception {
public:
    //Exception(const char* message): exception(message);
    virtual ~Exception() = 0;
    virtual const char* what();
};

#endif


/** pIn memory exception.
    This exception is typically thrown by a plugin when a dynamic memory
    allocation failed. */
class MemoryException: public Exception {
    typedef Exception Base;
public:
    MemoryException(const char* message):
        Base(message) {
        ;
    }
}; // class MemoryException: public Exception


/** pIn missing runtime exception.
    This exception is typically thrown by a plugin upon construction when an
    invalid runtime pointer is given. */
class MissingRuntimeException: public Exception {
    typedef Exception Base;
public:
    MissingRuntimeException(const char* message):
        Base(message) {
        ;
    }
}; // class MissingRuntimeException: public Exception

} // namespace exception
} // namespace pI

#endif // PUREIMAGE_ADAPTERS_CPP_PI_EXCEPTION_HPP
