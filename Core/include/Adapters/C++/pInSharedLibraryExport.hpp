/* pInSharedLibraryExport.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_ADAPTERS_CPP_PIN_SHARED_LIBRARY_EXPORT_HPP
#define PUREIMAGE_ADAPTERS_CPP_PIN_SHARED_LIBRARY_EXPORT_HPP

#include <PureImage.hpp>
#include <BuildSharedLibrary.h>
#include <CpInAdapter.hpp>

#define EXPORT_PIN(runtime, type) \
    { \
        pI::CpInAdapter<type>* foo = \
                                     const_cast<pI::CpInAdapter<type>* > ( \
                                             pI::CpInAdapter<type>::Create (runtime)); \
        *plugin = const_cast<struct _CpIn* > (foo->GetCpIn()); \
        if (*plugin == 0) \
            delete foo; \
        return pI_TRUE; \
    } \

#define EXPORT_PIN_SC(number, runtime, type) \
    case number: EXPORT_PIN (runtime, type);

typedef pI_bool (*pInCreateFunction) (pI_int index, struct _CRuntime* runtime, struct _CpIn** plugin);

#endif /* #define PUREIMAGE_ADAPTERS_CPP_PIN_SHARED_LIBRARY_EXPORT_HPP */
