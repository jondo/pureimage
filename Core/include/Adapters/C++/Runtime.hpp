/* Runtime.hpp
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_ADAPTERS_CPP_RUNTIME_HPP
#define PUREIMAGE_ADAPTERS_CPP_RUNTIME_HPP

#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/bind.hpp>

#include <PureImage.h>
#include <Adapters/C++/ArgumentTypedefs.hpp>
#include <Adapters/C++/pIExceptions.hpp>
#include <Adapters/C++/pInTypedefs.hpp>
#include <Adapters/C++/RuntimeGlobalFunctions.hpp>

namespace pI {

/** The Runtime class is a convenience class which aims at
    the simplification of both plugin and application development.
    It wraps a CRuntime instance and therefore supplies
    similar functionality, especially argument creation and
    plugin registration methods. */
class Runtime {

	typedef boost::shared_ptr<struct _CRuntime> CRuntimeSharedPtr;
	typedef boost::shared_ptr<struct _SharedLibrary> SharedLibrarySharedPtr;

public:

    /** Runtime constructor which creates and wraps a
        managed CRuntime instance.
        @param load_properties if true, the runtime will try
               to open and parse a properties file.
        @throw exception::MissingRuntimeException
    */
    Runtime(const pI_bool load_properties = pI_FALSE) {
		_c_runtime =
		 CRuntimeSharedPtr (
			runtime::CreateCRuntime (load_properties),
			load_properties == pI_FALSE ? runtime::CleanUpCRuntime : runtime::CleanUpCRuntimeWriteProperties);

		if (_c_runtime == 0) {
			throw exception::MissingRuntimeException("Failed to create CRuntime during Runtime construction.");
		}
	}

    /** Runtime constructor which takes a valid CRuntime instance.
        @param cruntime Valid CRuntime instance.
		@param take_ownership if true, the CRuntime will
			   be deleted on destruction
		@param write_properties if true and take_ownership is true,
			   the runtime will write a properties file on exit.
        @throw exception::MissingRuntimeException
    */
    Runtime(const CRuntimePtr cruntime,
			const pI_bool take_ownership = pI_FALSE,
			const pI_bool write_properties = pI_FALSE) {

		if (cruntime == 0) {
			throw exception::MissingRuntimeException("Runtime must receive valid CRuntime instance on construction.");
		}

		if (take_ownership == pI_TRUE) {
			_c_runtime =
			 CRuntimeSharedPtr (
				cruntime,
				write_properties == pI_FALSE ? runtime::CleanUpCRuntime : runtime::CleanUpCRuntimeWriteProperties);
		} else {
			_c_runtime = CRuntimeSharedPtr(cruntime, runtime::VetoDelete);
		}
	}

    /// Copy constructor.
	Runtime(const Runtime& runtime): 
	 _c_runtime(runtime._c_runtime) {}

    /// virtual destructor.
    virtual ~Runtime() {}

    // method to retrieve the runtime version of this Runtime
    inline pI_int GetRuntimeVersion() const;

	// method to retrieve the API version of this Runtime
    inline pI_int GetAPIVersion() const;

    /// method to retrieve the underlying CRuntime instance
    inline CRuntimePtr GetCRuntime() const;

    /// method to delete an Argument.
    static inline void DeleteArgument (CArgumentPtr arg);

    /** Method to create an argument on heap memory using the runtime.
        The argument created is a default argument lacking any data.
        @throw exception::MemoryException
        @return The allocated argument. */
    inline ArgumentPtr AllocateArgument() const;

    //pI_bool AllocateArgumentData (ArgumentPtr arg) const;
    /// @throws exception::ArgumentException
    inline void AllocateArgumentData (ArgumentPtr arg) const;

    /** Method to copy any argument, using the runtime.
        @param src Argument to copy.
        @param copy_data If pI_TRUE, argument data is duplicated as well.
        @return Argument copy. */
    inline ArgumentPtr CopyArgument (
        const ArgumentPtr& src,
        const pI_bool copy_data = pI_TRUE) const;

    /** Method to copy multiple arguments, using the runtime.
        @param src Arguments to copy.
        @param copy_data If pI_TRUE, argument data is duplicated as well.
        @return Copy of given arguments. */
    inline Arguments CopyArguments (
        const Arguments& src,
        const pI_bool copy_data = pI_TRUE) const;

	/** Method to swap two argument vectors.
		Both vectors must have equal size, otherwise an exception is thrown.
		@param l first argument vector.
		@param r second argument vector. */
	inline void SwapArguments (Arguments& l, Arguments& r) const;

    /** Method to copy a C-string, using the runtime.
        @param str String to copy.
        @throws exception::MemoryException
        @return Copy of the string. */
    inline pI_char* CopyString (const pI_char* str) const;

    /** Method to free a C-string, using the runtime.
        @param str String to free. */
    inline void FreeString (const pI_char* str) const;

    /** Method to create a new argument of given type.
        @param type Type of argument to create.
        @param read_only specifies if argument is intended as read-only.
        @param constraints constraing string of the new argument.
        @param name argument name.
        @param description argument description.
        @return newly created argument, without data. */
    inline ArgumentPtr CreateArgument (
        const pI_ArgumentType type,
        const pI_bool read_only = pI_FALSE,
        const pI_char* constraints = 0,
        const pI_char* name = 0,
        const pI_char* description = 0) const;

    /** Method to create an argument out of its serialized form.
        The mechanism relies on the argument_deserializer of the
        CRuntime instance.
        @param str Serialized form of argument.
        @return Argument according to serialized form. */
    inline ArgumentPtr DeserializeArgument (const pI_char* str) const;

    /** Method to serialize a given argument into a string.
        @param arg The argument to serialize.
        @return The argument as string. */
    inline std::string SerializeArgument (ArgumentPtr arg) const;

    /** Method to create an argument list out of its serialized form.
        The mechanism relies on the argument_deserializer of the
        CRuntime instance.
        @param str Serialized form of the argument list.
        @return Argument list according to serialized form. */
    inline Arguments DeserializeArguments (const pI_char* str) const;

    /** Method to serialize multiple arguments into a string.
        @param list The arguments to serialize.
        @return The arguments as string. */
    inline std::string SerializeArguments (Arguments list) const;

    /** Method to enforce proper destruction of a C Argument
        by using a custom deletion function for the boost::shared_ptr.
        @param arg Argument instance.
        @return The argument as reference-countung shared_ptr
                with custom deleter. */
    inline ArgumentPtr ObtainArgument (const CArgumentPtr arg) const;

    /** Method to check the availability of a specific plugin.
        @param name Name of the plugin to query.
        @return pI_TRUE if plugin is available. */
    inline pI_bool HasPlugin (const pI_char* name) const;

    /** Method to register an exisiting plugin to the runtime.
        @throws exception::pInException
        @throws exception::PluginRegistrationException
        @param ip pIn instance to register. */
	inline void RegisterPlugin (pInPtr ip) const;

    /** Method to spawn an instance of a registered plugin.
        @param name Name of the plugin to create.
        @throws exception::PluginCreationException
        @return New plugin instance in case of success, zero otherwise. */
    inline pInPtr SpawnPlugin (const pI_char* name) const;

    /** Method to unregister a specific plugin.
        @param name Name of the plugin to unregister. */
    inline void UnregisterPlugin (const pI_char* name) const;

    /** Method to check if a specific property is available.
        @param name Name of the property to query.
        @return pI_TRUE if property is available, pI_FALSE otherwise. */
    inline pI_bool HasProperty (const pI_char* name) const;

    /** Method to retrieve a runtime property.
        Note: the property is returned as copy.
        @param name Name of the property to retrieve.
        @throws exception::PropertyException
        @return a copy of the stored Argument instance. */
    inline ArgumentPtr GetProperty (const pI_char* name) const;

    /** Method to delete a runtime property.
        @param name Name of the property to delete. */
    inline void RemoveProperty (const pI_char* name);

    /** Method to store a runtime property.
        Note: the property is stored as copy.
        @throws exception::PropertyException
        @param arg property to store.*/
    inline void StoreProperty (ArgumentPtr arg);

private:

    mutable CRuntimeSharedPtr _c_runtime;

}; // class Runtime


/*inline*/ pI_int Runtime::GetRuntimeVersion() const {
    return _c_runtime->GetRuntimeVersion();
}


/*inline*/ pI_int Runtime::GetAPIVersion() const {
	return runtime::GetAPIVersion(_c_runtime.get());
}


/// method to retrieve the underlying CRuntime instance
/*inline*/ CRuntimePtr Runtime::GetCRuntime() const {
    return _c_runtime.get();
}


/*static inline*/ void Runtime::DeleteArgument (CArgumentPtr arg)
{

    if ((arg != 0) && (arg->runtime != 0)) {
        arg->runtime->FreeArgument (arg->runtime, arg, pI_TRUE);
    }
} // /*static*/ void Runtime::DeleteArgument (pInArgument* arg)


/*inline*/ ArgumentPtr Runtime::AllocateArgument() const
{

    CArgumentPtr arg(GetCRuntime()->AllocateArgument (GetCRuntime()));

    if (arg == 0) {
        throw exception::MemoryException("Failed to allocate argument memory");
    }

    return ObtainArgument(arg);
} // Argument Runtime::AllocateArgument (const pI_bool initialize /*= pI_TRUE*/) const


/*inline*/ void Runtime::AllocateArgumentData (ArgumentPtr arg) const
{

    if (GetCRuntime()->CreateArgumentData (GetCRuntime(), GetCArgument(arg)) == pI_FALSE) {
        throw exception::ArgumentException("Failed to allocate argument data.");
    }
} // pI_bool Runtime::AllocateArgumentData (ArgumentPtr arg) const


/*inline*/ ArgumentPtr Runtime::CopyArgument (
    const ArgumentPtr& src,
    const pI_bool copy_data /*= pI_TRUE*/) const
{

    CArgumentPtr arg(GetCRuntime()->CopyArgument (GetCRuntime(), GetCArgument(src), copy_data));
    return ObtainArgument(arg);
} // ArgumentPtr Runtime::CopyArgument (...) const


/*inline*/ Arguments Runtime::CopyArguments (
    const Arguments& src,
    const pI_bool copy_data /*= pI_TRUE*/) const
{

    Arguments retval;

    for (pI_size lv = 0; lv < src.size(); ++lv) {
        retval.push_back (CopyArgument (src[lv], copy_data));
    }

    return retval;
} // Arguments Runtime::CopyArguments (...) const


/*inline*/ void Runtime::SwapArguments (Arguments& l, Arguments& r) const
{
	if (l.size() != r.size())
		throw exception::MissingArgumentException("Swap arguments operation called with two vectors of different size.");
	for (pI_size lv = 0; lv < l.size(); ++lv) {
        l[lv]->runtime->SwapArguments (
            l[lv]->runtime,
            GetCArgument(r[lv]),
            GetCArgument(l[lv]));
    }
} // void Runtime::SwapArguments (...) const


/*inline*/ pI_char* Runtime::CopyString (const pI_char* str) const
{

    pI_char* retval(0);

    if (str != 0) {
        retval = GetCRuntime()->CopyString (GetCRuntime(), const_cast<pI_char* >(str));

        if (retval == 0) {
            throw exception::MemoryException("Failed to allocate string memory");
        }
    }

    return retval;
} // pI_char* Runtime::CopyString (const pI_char* str) const


/*inline*/ void Runtime::FreeString (const pI_char* str) const
{

    GetCRuntime()->FreeString (GetCRuntime(), const_cast<pI_char* >(str));
} // void Runtime::FreeString (const pI_char* str) const


/*inline*/ ArgumentPtr Runtime::CreateArgument (
    const pI_ArgumentType type,
    const pI_bool read_only /* = pI_FALSE */,
    const pI_char* constraints /*= 0*/,
    const pI_char* name /*= 0*/,
    const pI_char* description /*= 0*/) const
{

    CArgumentPtr arg(GetCRuntime()->CreateArgument (GetCRuntime(), type));

    if (arg != 0) {
        arg->signature.readonly = read_only;
        arg->signature.constraints = GetCRuntime()->CopyString (GetCRuntime(), const_cast<pI_char* >(constraints));
        arg->description.name = GetCRuntime()->CopyString (GetCRuntime(), const_cast<pI_char* >(name));
        arg->description.description = GetCRuntime()->CopyString (GetCRuntime(), const_cast<pI_char* >(description));
    }

    return ObtainArgument (arg);
} // ArgumentPtr Runtime::CreateArgument (...)


/*inline*/ ArgumentPtr Runtime::DeserializeArgument (const pI_char* str) const
{

    CArgumentPtr arg(GetCRuntime()->DeserializeArgument (GetCRuntime(), const_cast<pI_char* >(str), std::string(str).size()));
	if (arg == 0) {
		if (GetCRuntime()->last_error == pI_CRuntime_Error_RuntimeVersionMismatch) {
			GetCRuntime()->last_error = pI_CRuntime_Error_NoError;
			throw exception::RuntimeVersionException("Deserialization of argument with old runtime failed.");
		}
	}
    return ObtainArgument (arg);
} // Argument Runtime::DeserializeArgument (pI_char* str) const


/*inline*/ std::string Runtime::SerializeArgument (ArgumentPtr arg) const
{

    pI_int str_len;
    return std::string(GetCRuntime()->SerializeArgument (GetCRuntime(), GetCArgument(arg), &str_len));
} // std::string Runtime::SerializeArgument (ArgumentPtr arg) const


/*inline*/ Arguments Runtime::DeserializeArguments (const pI_char* str) const
{

    Arguments retval;
    struct _ArgumentList* arg_list;
    arg_list = GetCRuntime()->DeserializeArguments (GetCRuntime(), const_cast<pI_char* >(str), 0);

    if ((arg_list != 0) && (arg_list->count != 0) && (arg_list->list != 0)) {
        for (pI_size lv = 0; lv < arg_list->count; ++lv) {
			if (arg_list->list[lv] == 0) {
				if (GetCRuntime()->last_error == pI_CRuntime_Error_RuntimeVersionMismatch) {
					GetCRuntime()->last_error = pI_CRuntime_Error_NoError;
					GetCRuntime()->FreeArgumentList (GetCRuntime(), arg_list, pI_TRUE, pI_TRUE);
					throw exception::RuntimeVersionException("Deserialization of argument with old runtime failed.");
				}
			}
            retval.push_back (ObtainArgument (arg_list->list[lv]));
        }

        GetCRuntime()->FreeMemory (GetCRuntime(), arg_list);
    }

    return retval;
} // Arguments Runtime::DeserializeArguments (pI_char* str) const


/*inline*/ std::string Runtime::SerializeArguments (Arguments list) const
{

    if (list.size() == 0) {
        return std::string();
    }

    pI_int snlen;
    std::string retval;
    pI_str sn_str;
    struct _ArgumentList* arg_list = GetCRuntime()->AllocateArgumentList (GetCRuntime(), list.size());

    for (pI_size lv = 0; lv < list.size(); ++lv) {
        arg_list->list[lv] = GetCArgument(list[0]);
    }

    sn_str = GetCRuntime()->SerializeArguments (GetCRuntime(), arg_list, &snlen);
    retval = sn_str;
    GetCRuntime()->FreeArgumentList (GetCRuntime(), arg_list, pI_FALSE, pI_FALSE);
    GetCRuntime()->FreeString (GetCRuntime(), sn_str);
    return retval;
} // std::string Runtime::SerializeArguments (Arguments list) const


/*inline*/ ArgumentPtr Runtime::ObtainArgument (const CArgumentPtr arg) const
{

    return ArgumentPtr(arg, DeleteArgument);

} // Argument Runtime::ObtainArgument (CArgumentPtr arg) const


/*inline*/ pI_bool Runtime::HasPlugin (const pI_char* name) const
{

    return GetCRuntime()->HasPlugin (GetCRuntime(), const_cast<pI_char* >(name));
} // pI_bool Runtime::HasPlugin (const pI_char* name) const


/*inline*/ void Runtime::RegisterPlugin (pInPtr ip) const
{

	CRuntimeError err = runtime::CppRuntimeRegisterPlugin (GetCRuntime(), GetPInPtr (ip));
	if (err == pI_CRuntime_Error_InvalidPlugin) {
        throw exception::pInException("Cannot register invalid plugin.");
    } else if (err == pI_CRuntime_Error_PluginRegistrationFailed) {
        throw exception::PluginRegistrationException("Failed to register plugin.");
    }
} // void Runtime::RegisterPlugin (pInPtr ip) const


/*inline*/ pInPtr Runtime::SpawnPlugin (const pI_char* name) const
{

	pI::pIn* instance = reinterpret_cast<pI::pIn* > (runtime::CppRuntimeSpawnPlugin (GetCRuntime(), name));
#if defined(PI_RAW_PIN_PTR)
	return instance;
#else // #if defined(PI_RAW_PIN_PTR)
	return pInPtr(instance, boost::bind(runtime::CppRuntimeErasePlugin, GetCRuntime(), instance));
#endif // #if defined(PI_RAW_PIN_PTR)
} // pInPtr Runtime::SpawnPlugin (const pI_char* name) const


/*inline*/ void Runtime::UnregisterPlugin (const pI_char* name) const
{

    GetCRuntime()->UnregisterPluginByName (GetCRuntime(), const_cast<pI_str> (name));
} // void Runtime::UnregisterPlugin (const pI_char* name) const


/*inline*/ pI_bool Runtime::HasProperty (const pI_char* name) const
{

    return GetCRuntime()->HasProperty (GetCRuntime(), const_cast<pI_char* >(name));
} // pI_bool Runtime::HasProperty (const pI_char* name) const


/*inline*/ ArgumentPtr Runtime::GetProperty (const pI_char* name) const
{

    CArgumentPtr carg = GetCRuntime()->GetProperty (GetCRuntime(), const_cast<pI_char* >(name), pI_TRUE);

    if (carg != 0) {
        return ObtainArgument (carg);
    } else {
        throw exception::PropertyException("Failed to retrieve the property.");
    }
} // void Runtime::GetProperty (const pI_char* name, ArgumentPtr arg) const


/*inline*/ void Runtime::RemoveProperty (const pI_char* name)
{

    GetCRuntime()->RemoveProperty (GetCRuntime(), const_cast<pI_char* >(name));
} // void Runtime::RemoveProperty (const pI_char* name)


void Runtime::StoreProperty (ArgumentPtr arg)
{

    if (GetCRuntime()->StoreProperty (GetCRuntime(), GetCArgument(arg), pI_FALSE) == pI_FALSE) {
        throw exception::PropertyException("Failed to store the property (most likely because its already available).");
    }
} // void Runtime::StoreProperty (ArgumentPtr arg)

} // namespace pI

#endif // PUREIMAGE_ADAPTERS_CPP_RUNTIME_HPP
