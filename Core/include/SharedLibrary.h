/* SharedLibrary.h
 *
 * Copyright 2010-2011 Johannes Kepler Universit�t Linz,
 * Institut f�r Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_SHARED_LIBRARY_H
#define PUREIMAGE_SHARED_LIBRARY_H

#ifdef __cplusplus
extern "C" {
#endif

#if defined(_WIN32) || defined(_WIN64)
#include <windows.h>
#undef min
#undef max
#elif defined(linux) || defined(__linux) || defined(__linux__) || defined(__TOS_LINUX__)
#include <dlfcn.h>
#else
#error "pureImage does not support your platform regarding shared libraries."
#endif

#ifndef pI_RUNTIME_MAKE_SHARED_LIB_NAME
#if defined(_WIN32) || defined(_WIN64)
#if defined(DEBUG) || defined(_DEBUG)
#define pI_RUNTIME_MAKE_SHARED_LIB_NAME(name) \
	(name "_d.dll")
#else /* #if defined(DEBUG) || defined(_DEBUG) */
#define pI_RUNTIME_MAKE_SHARED_LIB_NAME(name) \
	(name ".dll")
#endif /* #if defined(DEBUG) || defined(_DEBUG) */
#elif defined(linux) || defined(__linux) || defined(__linux__) || defined(__TOS_LINUX__)
#if defined(DEBUG) || defined(_DEBUG)
#define pI_RUNTIME_MAKE_SHARED_LIB_NAME(name) \
	("lib" name "_d.so")
#else /* #if defined(DEBUG) || defined(_DEBUG) */
#define pI_RUNTIME_MAKE_SHARED_LIB_NAME(name) \
	("lib" name ".so")
#endif /* #if defined(DEBUG) || defined(_DEBUG) */
#endif /* #elif defined(linux) || defined(__linux) || defined(__linux__) || defined(__TOS_LINUX__) */
#endif /* #ifndef pI_RUNTIME_MAKE_SHARED_LIB_NAME */

#include <malloc.h>
#include <string.h>

/* set new alignment */
#ifdef __GNUC__
#define DO_PRAGMA_EXPAND(x) _Pragma (#x)
#define DO_PRAGMA(x) DO_PRAGMA_EXPAND(x)
DO_PRAGMA (pack (push,PI_STRUCTURE_ALIGNMENT))
#else
#pragma pack(push)                          /* push current alignment to stack */
#pragma pack(PI_STRUCTURE_ALIGNMENT)
#endif


typedef struct _SharedLibrary {

	void* slPtr;

} SharedLibrary;


/* note: because of header-only implementations, the simple portable model
		 for using header-based inline functions is adopted.
		 @see http://www.greenend.org.uk/rjk/2003/03/inline.html */

static inline
const char* AppendSharedLibraryFileExtension (const char* path, const bool appendConfig) {

	char* p = 0;
	int p_len = 0;
#if defined(_WIN32) || defined(_WIN64)
#if defined(DEBUG) || defined(_DEBUG)
	int ext_len = appendConfig ? 6 : 4;
	const char* ext = appendConfig ? "_d.dll" : ".dll";
#else /* #if defined(DEBUG) || defined(_DEBUG) */
	int ext_len = 4;
	const char* ext = ".dll";
#endif /* #if defined(DEBUG) || defined(_DEBUG) */
#elif defined(linux) || defined(__linux) || defined(__linux__) || defined(__TOS_LINUX__)
#if defined(DEBUG) || defined(_DEBUG)
	int ext_len = appendConfig ? 5 : 3;
	const char* ext = appendConfig ? "_d.so" : ".so";
#else /* #if defined(DEBUG) || defined(_DEBUG) */
	int ext_len = 3;
	const char* ext = ".so";
#endif /* #if defined(DEBUG) || defined(_DEBUG) */
#endif
	if (path == 0)
		return 0;
	p_len = strlen (path);
	if ((p_len <= ext_len) || (strncmp (path + p_len - ext_len, ext, ext_len) != 0)) {
		p = (char*) malloc (sizeof(char) * (p_len + ext_len + 1));
#ifndef __GNUC__
		strcpy_s (p, p_len + 1, path);
		strcpy_s (p + p_len, ext_len + 1, ext);
#else
		strcpy (p, path);
		strcpy (p + p_len, ext);
#endif
		p[p_len + ext_len] = 0;
		return p;
	} else {
		p = (char*) malloc (sizeof(char) * (p_len + 1));
#ifndef __GNUC__
		strcpy_s (p, p_len + 1, path);
#else
		strcpy (p, path);
#endif
		p[p_len] = 0;
		return p;
	}
}


static inline
struct _SharedLibrary* LoadPureImageSharedLibrary (const char* path) {

	SharedLibrary* lptr = 0;
	char* p = (char*) AppendSharedLibraryFileExtension (path, true);
	if (p == 0)
		return 0;
	lptr = (struct _SharedLibrary*) malloc (sizeof(struct _SharedLibrary));
	if (lptr == 0) {
		free (p);
		return 0;
	}

#if defined(_WIN32) || defined(_WIN64)
	lptr->slPtr = LoadLibraryExA (p, 0, 0);
#elif defined(linux) || defined(__linux) || defined(__linux__) || defined(__TOS_LINUX__)
	lptr->slPtr = dlopen (p, RTLD_LAZY | RTLD_GLOBAL);
#endif
	free (p);
	if (lptr->slPtr == 0) {
		free (lptr);
		return 0;
	}
	return lptr;
}


static inline
struct _SharedLibrary* LoadSharedLibrary (const char* path) {

	SharedLibrary* lptr = 0;
	char* p = (char*) AppendSharedLibraryFileExtension (path, false);
	if (p == 0)
		return 0;
	lptr = (struct _SharedLibrary*) malloc (sizeof(struct _SharedLibrary));
	if (lptr == 0) {
		free (p);
		return 0;
	}

#if defined(_WIN32) || defined(_WIN64)
	lptr->slPtr = LoadLibraryExA (p, 0, 0);
#elif defined(linux) || defined(__linux) || defined(__linux__) || defined(__TOS_LINUX__)
	lptr->slPtr = dlopen (p, RTLD_LAZY | RTLD_GLOBAL);
#endif
	free (p);
	if (lptr->slPtr == 0) {
		free (lptr);
		return 0;
	}
	return lptr;
}


static inline
void UnloadSharedLibrary (struct _SharedLibrary* ptr) {

    if (ptr == 0)
        return;
#if defined(_WIN32) || defined(_WIN64)
	FreeLibrary ((HMODULE) ptr->slPtr);
#elif defined(linux) || defined(__linux) || defined(__linux__) || defined(__TOS_LINUX__)
	dlclose (ptr->slPtr);
#endif
	free (ptr);
}


static inline
void* GetSharedLibraryFunction (const struct _SharedLibrary* sl, const char* name) {
	if ((sl == 0) || (name == 0))
		return 0;
#if defined(_WIN32) || defined(_WIN64)
	return (void*) GetProcAddress ((HMODULE) sl->slPtr, name);
#elif defined(linux) || defined(__linux) || defined(__linux__) || defined(__TOS_LINUX__)
	return dlsym (sl->slPtr, name);
#endif
}


static inline
const char* GetSharedLibraryFileExtension() {

#if defined(_WIN32) || defined(_WIN64)
	return ".dll";
#elif defined(linux) || defined(__linux) || defined(__linux__) || defined(__TOS_LINUX__)
	return ".so";
#endif
}


#pragma pack(pop)                           /* restore original alignment from stack */

#ifdef __cplusplus
}
#endif

#endif /* PUREIMAGE_SHARED_LIBRARY_H */
