/* PureImage.h
 *
 * Copyright 2010-2012 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_PUREIMAGE_H
#define PUREIMAGE_PUREIMAGE_H

/** Include header for pureImage image processing library. */

#ifndef PI_STRUCTURE_ALIGNMENT
#define PI_STRUCTURE_ALIGNMENT 4
#endif /* #ifndef PI_STRUCTURE_ALIGNMENT */

#ifndef PI_FIELD_PADDING
#define PI_FIELD_PADDING PI_STRUCTURE_ALIGNMENT
#endif /* #ifndef PI_FIELD_PADDING */

#ifndef PI_API_VERSION
#define PI_API_VERSION 10300
#endif /* PI_API_VERSION */

#ifndef USE_EXPANDED_DATATYPES
    #include <DataTypes/Intrinsics.h>
    #include <DataTypes/Structures.h>
    #include <DataTypes/Arguments.h>
#else
    #include <DataTypes/DataTypes.h>
#endif

#include <Runtime/Runtime.h>
#include <pIn/pIn.h>

/** @mainpage pureImage

    @section overview Overview

    pureImage (or pI for short) is a plug-in development framework that provides a common plug-in interface,
    generic data structures, and a uniform workflow managed by a runtime environment.

    pureImage is specialized in image processing under C++. However, it can be used in different areas
    (due to its generic data types, such as vectors, matrices, etc.), and in any environment which
    provides C bindings, such as Java, Matlab, or Python.

    Please see the project page https://pureimage.flll.jku.at/ for more information about the
    software, to check for newer releases, to post questions, to report bugs or to contact the authors.

    \if offline
        For a description how to start using pureImage, see the <a href="../../ReadMe.html">ReadMe file</a>.
        For more detailed informations on the current release of pureImage, consult the <a href="../../ReleaseNotes.html">Release Notes</a>.

        For a comprehensive tutorial on pureImage, see the <a href="../pI_Tutorial/Tutorial.pdf">Tutorial</a>.

        For a listing of available plugins, see the <a href="../plugins/plugins.html">plugins</a> listing.
    \elseif online
        For a description how to start using pureImage, see the <a href="external/ReadMe.html">ReadMe file</a>.
        For more detailed informations on the current release of pureImage, consult the <a href="external/ReleaseNotes.html">Release Notes</a>.
    \endif

    @section license License

    pureImage is copyright by <a href="http://www.flll.jku.at/">Institut f&uuml;r
    Wissensbasierte Mathematische Systeme</a> at <a href="http://www.jku.at/">Johannes
    Kepler Universit&auml;t Linz</a>, and is released under the terms of
    <a href="http://www.gnu.org/licenses/gpl.html">GPL v3</a>.

    pureImage documentation is licensed under the <a href="http://creativecommons.org/licenses/by-nc-sa/3.0/">
    Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License</a>.
*/

#endif /* #ifndef PUREIMAGE_PUREIMAGE_H */
