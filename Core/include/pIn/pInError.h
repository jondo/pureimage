/* pInError.h
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_PIN_PINERROR_H
#define PUREIMAGE_PIN_PINERROR_H

#ifdef __cplusplus
extern "C" {
#endif

    /** Enumeration of possible CpIn errors. */
    typedef enum _CpInError {
        CpIn_Error_NoError = 0,                 /*!< No error occurred.*/
        CpIn_Error_NotInitialized,              /*!< Image plugin was not initialized.*/
        CpIn_Error_MissingParameters,           /*!< Missing parameters for Initialize method.*/
        CpIn_Error_MissingArguments,            /*!< Missing arguments for Execute method.*/
        CpIn_Error_IncompatibleParameters,      /*!< Provided parameters not compatible with Initialize method.*/
        CpIn_Error_IncompleteParameters,        /*!< Initialize method misses parameters.*/
        CpIn_Error_IncompatibleArguments,       /*!< Provided parameters not compatible with Execute method.*/
        CpIn_Error_IncompleteArguments,         /*!< Execute method misses parameters.*/
        CpIn_Error_InsufficientMemory,          /*!< Memory allocation failed.*/
        CpIn_Error_PluginLookupFailed,          /*!< Plugin query failed.*/
        CpIn_Error_Serialization,               /*!< Plugin serialization failed.*/
        CpIn_Error_SerializationNotSupported,   /*!< Plugin serialization not implemented.*/
        CpIn_Error_Deserialization,             /*!< Plugin deserialization failed.*/
        CpIn_Error_DeserializationNotSupported, /*!< Plugin deserialization not implemented.*/
        CpIn_Error_MissingCRuntime,             /*!< No valid runtime provided.*/
        CpIn_Error_InitializationFailed,        /*!< Plugin initialization failed.*/
        CpIn_Error_ExecutionFailed,             /*!< Plugin execution failed.*/
		CpIn_Error_CreationFailed,				/*!< Plugin creation failed.*/
    }
    CpInError;

#ifdef __cplusplus
}
#endif

#endif /* PUREIMAGE_PIN_PINERROR_H */
