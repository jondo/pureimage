/* pIn.h
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_PIN_PIN_H
#define PUREIMAGE_PIN_PIN_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef USE_EXPANDED_DATATYPES
    #include <DataTypes/Arguments.h>
#else
    #include <DataTypes/DataTypes.h>
#endif

#include <Runtime/RuntimeError.h>

    /* fwd decl for CpInCRuntime */
    struct _CpInCRuntime;

    /** CpIn struct definition.
        A CpIn structure is supposed to be initialized by a function:
        There is no user-defined constructor, which would affect binary compatibility,
        and no implemented function defined; for maximal binary compatibility, there are
        only simple types, pointers and function pointers, which have to be set from
        outside. The use of function pointers also allows the quick exchange of
        implementation details without touching the interface. */
    typedef struct _CpIn {

        /** Pointer to clone method.
            This method is responsible for instance creation, it provides
            simple stateless plugin creation, and must also provide a
            stateful clone in case the plugin needs an internal state.
            @param instance instance to create / clone.
            @param stateful If pI_FALSE, the plugin will be created from
                            scratch, otherwise the state of the given plugin
                            is used for the copy.
            @return new instance in case of success, zero otherwise. */
        struct _CpIn* (*Clone) (struct _CpIn* instance, pI_bool stateful);

        /** Pointer to destroy method.
            This method destroys the plugin and cleans any user data.
            @param instance The instance to destroy.
            @return pI_TRUE in case of success, pI_FALSE otherwise. */
        pI_bool (*Destroy) (struct _CpIn* instance);

        /** Pointer to initialization method.
            This method initializes the plugin with the given parameter set. The
            given parameter set must be compatible with the parameters of the struct.
            When Initialize was executed successfully, the arguments field of the struct
            contains the arguments the @see Process method expects. Upon failed
            initialization, one can examine the last_error member of the struct for the
            reason.
            @param instance CpIn instance to execute Initialize upon.
            @param parameters Parameterset to initialize instance with.
            @return pI_TRUE on success. */
        pI_bool (*Initialize) (struct _CpIn* instance, struct _ArgumentList* parameters);

        /** Pointer to execute method.
            This method is the actual worker method. The given argument set, which
            may contain both input and output arguments, must be compatible with
            the arguments of the struct after @see Initialize was called.
            @param instance CpIn instance to execute Initialize upon.
            @param arguments The input and output arguments to execute the instance with.
            @return pI_TRUE on success. */
        pI_bool (*Execute) (struct _CpIn* instance,
                            struct _ArgumentList* input_arguments,
                            struct _ArgumentList* output_arguments);

        /** Pointer to serialize method.
            This method serialized the state of the plugin into the member string
            @see serialization_data.
            @param instance CpIn instance.
            @return pI_TRUE on success. */
        pI_bool (*Serialize) (struct _CpIn* instance);

        /** Pointer to deserialize method.
            This method deserialized the the plugin using the given string.
            @param instance CpIn instance.
            @param data Serialization data.
            @return pI_TRUE on success. */
        pI_bool (*Deserialize) (struct _CpIn* instance, pI_str data);


        /** pIn API version. */
        pI_unsigned api_version;

        /** pIn plugin version. */
        pI_unsigned plugin_version;

        /** The author of the plugin. */
        pI_str author;

        /** The copyright notice of the plugin. */
        pI_str copyright;

        /** The description of the plugin. */
        pI_str description;

        /** The name (human-readable id) of the plugin.
            The name should contain also its category, the individual
            fields are seperated by '/'. Example:
            "Fingerprint/NBIS/DetectMinutiae". */
        pI_str name;

        /** The plugin runtime. */
        struct _CRuntime* plugin_runtime;

        /** Actual parameters of the plugin. */
        struct _ArgumentList* parameters;

        /** Input and output argument signatures, valid only
            after successful initialization. */
        struct _ArgumentList* input_arg_signature;
        struct _ArgumentList* output_arg_signature;

        /** Last occurred error. */
        enum _CpInError last_error;

        /** Textual description of last error. */
        pI_str last_error_text;

        /** Serialization data. */
        pI_str serialization_data;

        /** The state buffer of the plugin. For stateful C-Plugins. */
        void* state_buffer;

        /** The size of the state buffer. */
        pI_size state_buffer_len;

        /** Adapter info for CpInAdapter; not needed otherwise. */
        void* adapter_info;

    } CpIn;

    typedef struct _CpIn* CpInPtr;

#ifdef __cplusplus
}
#endif

#endif /* PUREIMAGE_PIN_PIN_H */
