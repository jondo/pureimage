/* Runtime.h
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_RUNTIME_RUNTIME_H
#define PUREIMAGE_RUNTIME_RUNTIME_H

#ifdef __cplusplus
extern "C" {
#endif

/* set new alignment */
#ifdef __GNUC__
#define DO_PRAGMA_EXPAND(x) _Pragma (#x)
#define DO_PRAGMA(x) DO_PRAGMA_EXPAND(x)
DO_PRAGMA (pack (push,PI_STRUCTURE_ALIGNMENT))
#else
#pragma pack(push)                          /* push current alignment to stack */
#pragma pack(PI_STRUCTURE_ALIGNMENT)
#endif

#ifndef USE_EXPANDED_DATATYPES
    #include <DataTypes/Arguments.h>
#else
    #include <DataTypes/DataTypes.h>
#endif

struct _CpIn;

#include "RuntimeError.h"

    /** Enumeration of possible data type classes. */
    typedef enum _pIDataTypeClass {
        UNKNOWN_CLASS = 0,          /*!< Unknown data type class.*/
        INTRINSIC_CLASS,            /*!< Intrinsic data type class.*/
        ARGUMENT_CLASS,             /*!< Argument data type class.*/
        STRUCTURE_CLASS             /*!< Structure data type class.*/
    }
    pIDataTypeClass;

    /** CRuntime struct definition.
        A runtime is supposed to be shared between different image
        plugins and arguments. It is implemented as
        low-level C POD type, therefore no user-defined constructor and
        no method implementation is provided. The use of function pointers
        allows the exchange of implementation detail without the need for
        recompilation of any client. */
    typedef struct _CRuntime {

        /** Pointer to Runtime version method. */
        pI_int (*GetRuntimeVersion) ();

        /* ------ initialization and destruction */

        /** Pointer to Initialize method.
            Intialized the given instance (sets registered plugins to zero).
            @param runtime runtime instance. */
        void (*Initialize) (
            struct _CRuntime* runtime);

        /** Pointer to Destroy method.
            Destroys any registered plugin.
            @param runtime runtime instance. */
        void (*Destroy) (
            struct _CRuntime* runtime);

        /* ------ plugin registration */

        /** Pointer to DestroyPlugin method
            Destroys only all registered plugins - but lets the runtime alive.
            @param runtime runtime instance. */
        void (*DestroyRegisteredPlugins) (
            struct _CRuntime* runtime);

        /** Pointer to RegisterPlugin method.
            Once registered, the runtime takes ownership of given plugin and
            is able to correctly answer a query request.
            Note: A plugin may only be registered once (name is used as key).
                  If registered twice, no second registration is done and
                  pI_FALSE is returned.
            @param runtime runtime instance.
            @param plugin Plugin to register.
            @return pI_TRUE if registration was successful, pI_FALSE
                    otherwise. */
        pI_bool (*RegisterPlugin) (
            struct _CRuntime* runtime,
            struct _CpIn* plugin);

        /** Pointer to UnregisterPlugin method.
            This method removes the registration of a plugin, after
            unregistration any query to the plugin will fail. This is
            especially useful when un- or re-loading shared libraries holding
            image plugins.
            Note: This function directly compares the pointer, not the name.
            @param runtime runtime instance.
            @param plugin Plugin to cancel registration for.
            @return pI_TRUE in case of unregistration success, pI_FALSE
                    otherwise. */
        pI_bool (*UnregisterPlugin) (
            struct _CRuntime* runtime,
            struct _CpIn* plugin);

        /** Pointer to UnregisterPluginByName method.
            This method removes the registration of a plugin, after
            unregistration any query to the plugin will fail. This is especially
            useful when un- or re-loading shared libraries holding image plugins.
            Note: This function cancels registration using the name of a plugin.
            @param runtime runtime instance.
            @param plugin Plugin to cancel registration for.
            @return pI_TRUE in case of unregistration success, pI_FALSE
                    otherwise. */
        pI_bool (*UnregisterPluginByName) (
            struct _CRuntime* runtime,
            pI_str name);

        /** Pointer to HasPlugin method.
            This method tries to find a plugin using its name.
            @param runtime runtime instance.
            @param name Name of plugin to look up.
            @return pI_TRUE if plugin is available, pI_FALSE otherwise. */
        pI_bool (*HasPlugin) (
            struct _CRuntime* runtime,
            pI_str name);

        /* ------ plugin creation and deletion */

        /** Pointer to SpawnPlugin method.
            This method creates a new plugin instance out of an existing one:
            The new plugin will not be initialized and therefor be possibly in
            different state as the given one.
            @param runtime runtime instance.
            @param plugin Plugin to create new plugin of.
            @return Pointer to newly created instance, or zero in case of
                    failure. */
        struct _CpIn* (*SpawnPlugin) (
            struct _CRuntime* runtime,
            struct _CpIn* plugin);

        /** Pointer to SpawnPluginByName method.
            This method creates a new plugin instance with given name. Therefore,
            a plugin query precedes any possible creation.
            @param runtime runtime instance.
            @param plugin Plugin to create new plugin of.
            @return Pointer to newly created instance, or zero in case of
                    failure. */
        struct _CpIn* (*SpawnPluginByName) (
            struct _CRuntime* runtime,
            pI_str name);

        /** Pointer to ClonePlugin method.
            This method clones a given plugin instance - an exact copy will be
            returned with the same type and even state as the original.
            @param runtime runtime instance.
            @param plugin Plugin to clone.
            @return Pointer to copy in case of success, 0 otherwise. */
        struct _CpIn* (*ClonePlugin) (
            struct _CRuntime* runtime,
            struct _CpIn* plugin);

        /** Pointer to DestroyPlugin method.
            This method destroys the given plugin and frees its memory.
            @param runtime runtime instance.
            @param plugin Plugin-instance to delete.
            @return pI_TRUE in case of success. */
        pI_bool (*DestroyPlugin) (
            struct _CRuntime* runtime,
            struct _CpIn* plugin);

        /* ------ memory allocation and deletion */

        /** Pointer to AllocateMemory method.
            This method triggers the allocation of a raw memory buffer on runtime-side.
            @param runtime runtime instance.
            @param buffer_size Number of bytes to allocate.
            @return pointer to raw buffer, or 0 if allocation failed. */
        void* (*AllocateMemory) (
            struct _CRuntime* runtime,
            pI_size buffer_size);

        /** Pointer to FreeMemory method.
            Method to delete a buffer which was allocated on runtime-side.
            @param runtime runtime instance.
            @param buffer memory buffer to delete.
            @return pI_TRUE in case of success. */
        pI_bool (*FreeMemory) (
            struct _CRuntime* runtime,
            void* buffer);

        /* ------ string methods */

        /** Pointer to AllocateString method.
            This method triggers the allocation of a string buffer on runtime-side.
            @param runtime runtime instance.
            @param buffer_size Number of characters to allocate.
            @return pointer to string buffer, or 0 if allocation failed. */
        pI_str (*AllocateString) (
            struct _CRuntime* runtime,
            pI_size buffer_size);

        /** Pointer to CopyString method.
            Method to copy a string on runtime-side.
            @param runtime runtime instance.
            @param src String to copy.
            @return Copy of given string, or zero if allocation failed. */
        pI_str (*CopyString) (
            struct _CRuntime* runtime,
            pI_str src);

        /** Pointer to CopyStringN method.
            Method to copy the first n characters of a string on runtime-side.
            @param runtime runtime instance.
            @param src String to copy.
            @param len Number of characters to copy.
            @return Copy of given string, or 0 if allocation failed. */
        pI_str (*CopyStringN) (
            struct _CRuntime* runtime,
            pI_str src,
            pI_size len);

        /** Pointer to FreeString method.
            Method to delete a string which was allocated on runtime-side.
            @param runtime runtime instance.
            @param buffer String to delete.
            @return pI_TRUE in case of success. */
        pI_bool (*FreeString) (
            struct _CRuntime* runtime,
            pI_str buffer);

        /* ------ intrinsic methods */

        /** Pointer to CreateIntrinsic method.
            It creates an intrinsic data type on the heap.
            @param runtime runtime instance.
            @param type intrinsic type to create.
            @return pointer to intrinsic data type in case of success, 0 otherwise. */
        pI_byte* (*CreateIntrinsic) (
            struct _CRuntime* runtime,
            enum _pI_IntrinsicType type);

        /** Pointer to CopyIntrinsic method.
            It creates an intrinsic data type copy on the heap.
            @param runtime runtime instance.
            @param source pointer to intrinsic to copy.
            @param type intrinsic type to create.
            @return pointer to intrinsic data type in case of success, 0 otherwise. */
        pI_byte* (*CopyIntrinsic) (
            struct _CRuntime* runtime,
            pI_byte* source,
            enum _pI_IntrinsicType type);

        /** Pointer to CopyIntrinsicInplace method.
            Copies an intrinsic data type to given pre-allocated memory.
            @param runtime runtime instance.
            @param source pointer to intrinsic to copy.
			@param dst pointer to copy-to area
            @param type intrinsic type to create.
            @return pI_TRUE in case of success. */
        pI_bool (*CopyIntrinsicInplace) (
            struct _CRuntime* runtime,
            pI_byte* source,
			pI_byte* dst,
            enum _pI_IntrinsicType type);

        /** Pointer to FreeIntrinsic method.
            It deletes an intrinsic data type which was created on the heap.
            @param runtime runtime instance.
            @param intrinsic pointer to intrinsic to delete.
            @param type intrinsic type to create.
            @return pI_TRUE in case of success. */
        pI_bool (*FreeIntrinsic) (
            struct _CRuntime* runtime,
            pI_byte* intrinsic,
            enum _pI_IntrinsicType type);

        /** Pointer to SerializeIntrinsic method.
            It serializes an intrinsic data type.
            @param runtime runtime instance.
            @param intrinsic pointer to intrinsic to serialize.
            @param type intrinsic type to create.
            @param str_len out-param to receive the len of the serialized form.
            @return intrinsic as string in case of success, 0 otherwise. */
        pI_str (*SerializeIntrinsic) (
            struct _CRuntime* runtime,
            pI_byte* intrinsic,
            enum _pI_IntrinsicType type,
            pI_size* str_len);

        /** Pointer to DeserializeIntrinsic method.
            It deserializes an intrinsic data type.
            @param runtime runtime instance.
            @param type intrinsic type to create.
            @param args11n serialized form of intrinsic.
            @return deserialized intrinsic created on the heap in case of success, 0 otherwise. */
        pI_byte* (*DeserializeIntrinsic) (
            struct _CRuntime* runtime,
            enum _pI_IntrinsicType type,
            pI_str args11n);

        /* ------ structure methods */

        /** Pointer to CreateStructure method.
            It creates a structure on the heap.
            @param runtime runtime instance.
            @param type structure type to create.
            @return structure created on the heap in case of success, 0 otherwise. */
        void* (*CreateStructure) (
            struct _CRuntime* runtime,
            enum _pI_StructureType type);

        /** Pointer to FreeStructure method.
            It deletes a structure on the heap.
            @param runtime runtime instance.
            @param structure structure to delete.
            @param type structure type to delete.
            @return pI_TRUE in case of success. */
        pI_bool (*FreeStructure) (
            struct _CRuntime* runtime,
            void* structure,
            enum _pI_StructureType type);

        /** Pointer to CopyStructure method.
            It creates a structure copy on the heap.
            @param runtime runtime instance.
            @param source structure to copy.
            @param type structure type to copy.
            @return copy of given structure in case of success, 0 otherwise. */
        void* (*CopyStructure) (
            struct _CRuntime* runtime,
            void* source,
            enum _pI_StructureType type);

        /* ------ argument methods */

        /** Pointer to AllocateArgument method.
            This method creates a new argument on the heap.
            @param runtime runtime instance.
            @return newly created argument, or zero in case of
                    failed memory allocation. */
        struct _Argument* (*AllocateArgument) (
            struct _CRuntime* runtime);

        /** Pointer to AllocateArgumentList method.
            This method creates an argument list (ONLY the list) on the heap.
            @param runtime runtime instance.
            @param num_arguments Number of arguments to create.
            @return newly created argument list, or zero in case of failed
                    memory allocation. */
        struct _ArgumentList* (*AllocateArgumentList) (
            struct _CRuntime* runtime,
            pI_size num_arguments);

        /** Pointer to CreateArgument method.
            In addition to AllocateArgument, also type-dependent structure
            information is allocated and initialized.
            @param runtime runtime instance.
            @param type Type of argument to create.
            @return newly created argument, or zero in case of
                    failed memory allocation. */
        struct _Argument* (*CreateArgument) (
            struct _CRuntime* runtime,
            enum _pI_ArgumentType type);

        /** Pointer to CreateArgumentData method.
            This method allocates argument data
            according to the set argument signature data. For instance,
            if the meta-data of a Polygon-argument says it should hold
            20 tuples, this method actually allocates the memory for these
            tuples. Atm., any data is initialized.
            @param runtime runtime instance.
            @param argument Argument to allocate memory for.
            @return pI_TRUE in case of allocation success. */
        pI_bool (*CreateArgumentData) (
            struct _CRuntime* runtime,
            struct _Argument* argument);

        /** Pointer to CreateArgumentSetup method.
            This method allocates the argument setup structure, dependant
            on the argument type.
            @param runtime runtime instance.
            @param argument Argument to allocate setup memory for.
            @return pI_TRUE in case of allocation success. */
        pI_bool (*CreateArgumentSetup) (
            struct _CRuntime* runtime,
            struct _Argument* argument);

        /** Pointer to FreeArgumentData method.
            This method frees any argument data, but not the argument itself.
            @param runtime runtime instance.
            @param argument Argument to free data of.
            @return pI_TRUE in case of success. */
        pI_bool (*FreeArgumentData) (
            struct _CRuntime* runtime,
            struct _Argument* argument);

        /** Pointer to FreeArgumentSetup method.
            This method frees any argument structure, but not the argument itself.
            @param runtime runtime instance.
            @param argument Argument to free structure of.
            @return pI_TRUE in case of success. */
        pI_bool (*FreeArgumentSetup) (
            struct _CRuntime* runtime,
            struct _Argument* argument);

        /** Pointer to FreeArgument method.
            This method deletes an argument, and depending on set flag, its
            data.
            @param runtime runtime instance.
            @param argument Argument to delete.
            @param free_data If pI_TRUE, the argument data is deleted too,
                             otherwise it will be perserved.
            @return pI_TRUE in case of success. */
        pI_bool (*FreeArgument) (
            struct _CRuntime* runtime,
            struct _Argument* argument,
            pI_bool free_data);

        /** Pointer to FreeArgumentList method.
            This method deletes an entire argument list.
            @param runtime runtime instance.
            @param argument_list List to delete.
            @param free_arguments If pI_TRUE, the contained arguments are
                                  deleted too, otherwise only the list is
                                  deleted.
            @param free_argument_data If free_arguments is pI_TRUE and
                                      free_argument_data is pI_TRUE too,
                                      the arguments data will be deleted.
            @return pI_TRUE in case of success. */
        pI_bool (*FreeArgumentList) (
            struct _CRuntime* runtime,
            struct _ArgumentList* argument_list,
            pI_bool free_arguments,
            pI_bool free_argument_data);

        /** Pointer to CopyArgument method.
            This method copies an argument.
            @param runtime runtime instance.
            @param argument Argument to copy.
            @param copy_data If pI_TRUE, the argument data will be copied
                             as well.
            @return Pointer to copy in case of success, 0 otherwise. */
        struct _Argument* (*CopyArgument) (
            struct _CRuntime* runtime,
            struct _Argument* argument,
            pI_bool copy_data);

        /** Pointer to CopyArgumentList method.
            This method copies an entire argument list, including its
            arguments.
            @param runtime runtime instance.
            @param list Argument list to copy.
            @param copy_data If pI_TRUE, the arguments data is copied as well.
            @return Copy of argument list in case of success, 0 otherwise. */
        struct _ArgumentList* (*CopyArgumentList) (
            struct _CRuntime* runtime,
            struct _ArgumentList* list,
            pI_bool copy_data);

        /** Pointer to SwapArgument method.
            This method swaps the contents of two valid arguments.
            @param runtime runtime instance.
            @param arg1 Pointer to first argument.
            @param arg2 Pointer to second argument.
            @return pI_TRUE on success, pI_FALSE otherwise. */
        pI_bool (*SwapArguments) (
            struct _CRuntime* runtime,
            struct _Argument* arg1,
            struct _Argument* arg2);

        /** Pointer to CheckArgumentConstraints method.
            This method validates an argument using a given constraint string.
            @param runtime runtime instance.
            @param arg Argument to validate.
            @param constraints Constraint string to validate argument with.
            @return pI_TRUE if argument fulfills constraints,
                    pI_FALSE in case the constraints are not fulfilled,
                    pI_ERROR if an error occurred during evaluation
                             (@see last_constraint_error and
                              last_constraint_error_msg for error details). */
        pI_int (*CheckArgumentConstraints) (
            struct _CRuntime* runtime,
            struct _Argument* arg,
            pI_str constraints);

        /* ------ serialization methods */

        /** Pointer to SerializeArgument method.
            This method serializes a given argument using the system serialization
            (JSON per default).
            @param runtime runtime instance.
            @param arg Argument to serialize.
            @param strlen OUT-parameter to receive the length of the serialization string.
            @return Serialized argument in case of success, zero otherwise. */
        pI_str (*SerializeArgument) (
            struct _CRuntime* runtime,
            struct _Argument* arg,
            pI_int* strlen);

        /** Pointer to DeserializeArgument method.
            This method deserializes a given argument string using the system
            deserialization (JSON per default).
            @param runtime runtime instance.
            @param arg_str Serialized argument.
            @param strlen length of the serialisation string.
            @return Deserialized argument in case of success, zero otherwise. */
        struct _Argument* (*DeserializeArgument) (
            struct _CRuntime* runtime,
            pI_str arg_str,
            pI_int strlen);

        /** Pointer to SerializeArguments method.
            This method serializes a given argument list using the system serialization
            (JSON per default).
            @param runtime runtime instance.
            @param list Arguments to serialize.
            @param strlen OUT-parameter to receive the length of the serialization string.
            @return Serialized argument in case of success, zero otherwise. */
        pI_str (*SerializeArguments) (
            struct _CRuntime* runtime,
            struct _ArgumentList* list,
            pI_int* strlen);

        /** Pointer to DeserializeArguments method.
           This method deserializes a given argument string using the system
           deserialization (JSON per default).
           @param runtime runtime instance.
           @param arg_str Serialized arguments.
           @param snlen length of the serialisation string.
           @return Deserialized argument list in case of success, zero otherwise. */
        struct _ArgumentList* (*DeserializeArguments) (
            struct _CRuntime* runtime,
            pI_str arg_str,
            pI_int snlen);

        /* ------ properties methods */

        /** Pointer to HasProperty method.
            This method test for the availability of a property with specific name.
            @param runtime runtime instance.
            @param name of the property to retrieve.
            @return pI_TRUE in case theproperty was found, otherwise pI_FALSE. */
        pI_bool (*HasProperty) (
            struct _CRuntime* runtime,
            pI_str name);

        /** Pointer to GetProperty method.
            This method retrieves stored properties.
            @param runtime runtime instance.
            @param name of the property to retrieve.
            @param get_copy if true, a copy is returned, the original otherwise.
            @return the property, 0 in case it was not found. */
        struct _Argument* (*GetProperty) (
            struct _CRuntime* runtime,
            pI_str name,
            pI_bool get_copy);

        /** Pointer to StoreProperty method.
            This method adds an argument to the property map of the runtime.
            Note: properties are sorted using the name field of the argument.
                  it is not possible to register two arguments with the same name.
            @param runtime runtime instance.
            @param prop the argument to add as property.
            @param take_ownership if true, the runtime will manage the given
                                  argument, otherwise it creates a copy.
            @return true if the property was stored as intended. */
        pI_bool (*StoreProperty) (
            struct _CRuntime* runtime,
            struct _Argument* prop,
            pI_bool take_ownership /* otherwise copies argument! */);

        /** Pointer to RemoveProperty method.
            This method removes an argument from the property map of the runtime.
            @param runtime runtime instance.
            @param name of the property to remove.
            @return true if the property was removes as intended. */
        pI_bool (*RemoveProperty) (
            struct _CRuntime* runtime,
            pI_str name);

        /** Pointer to ClearProperties method.
            This method deletes all stored properties.
            @param runtime runtime instance.
            @param free_memory if true, the dynamic array is also deleted. */
        void (*ClearProperties) (
            struct _CRuntime* runtime,
            pI_bool free_memory);

        /** Pointer to SerializeProperties method.
            This method serializes all stored properties.
            @param runtime runtime instance.
            @return the property map in serialized form. */
        pI_str (*SerializeProperties) (
            struct _CRuntime* runtime);

        /** Pointer to DeserializeProperties method.
            This method de-serializes properties.
            Note: the current implementation discards any properties stored so far,
                  the property state from the serialized form is directly applied.
            @param runtime runtime instance.
            @param sprops the serialized property map
            @return true if de-serialization attempts was successful. */
        pI_bool (*DeserializeProperties) (
            struct _CRuntime* runtime,
            pI_str sprops);

        /* ------ "private" member variables */

        /** Last occurred error. */
        enum _CRuntimeError last_error;

        /** Last occurred constraint parser error. Will be reset when
            CheckArgumentConstraints is executed. */
        enum _ArgumentConstraintError last_constraint_error;

        /** More detailed information on last occurred constraint
            parser error. Will be reset when CheckArgumentConstraints
            is executed. */
        pI_str last_constraint_error_msg;

        /** The number of registered plugins. */
        pI_size plugin_count;

        /** Registered plugin list. */
        struct _CpIn** plugins;

        /** Property map. */
        struct _Argument** property_map;

        /** Number of arguments in property map. */
        pI_int property_count;

        /** Actual size of dynamic property map. */
        pI_int property_capacity;

		/** Field for shared library instances, managed by high-level adapters. */
		void* shared_library_data;

    } CRuntime;

    typedef struct _CRuntime* CRuntimePtr;

#ifndef pI_STATIC_LIBRARY

#include <BuildSharedLibrary.h>

	/** method to create valid runtime struct. */
    typedef /*pI_API*/ struct _CRuntime* (*CreateCRuntimeFunc) (pI_bool load_property_file);
    /** method to destroy valid CRuntime struct. */
    typedef /*pI_API*/ void (*DestroyCRuntimeFunc) (struct _CRuntime* runtime, pI_bool write_property_file);
    /** method to retrieve the version of this API. */
    typedef /*pI_API*/ pI_int (*GetpIAPIVersionFunc)();

	/** method to create valid runtime struct. */
    pI_API struct _CRuntime* CreateCRuntime (pI_bool load_property_file);
    /** method to destroy valid CRuntime struct. */
    pI_API void DestroyCRuntime (struct _CRuntime* runtime, pI_bool write_property_file);
    /** method to retrieve the version of this API. */
    pI_API pI_int GetpIAPIVersion();

#else /* defined pI_STATIC_LIBRARY */

    /** method to create valid runtime struct. */
    extern struct _CRuntime* CreateCRuntime (pI_bool load_property_file);
    /** method to destroy valid CRuntime struct. */
    extern void DestroyCRuntime (struct _CRuntime* runtime, pI_bool write_property_file);
    /** method to retrieve the version of this API. */
    extern pI_int GetpIAPIVersion();

#endif /* pI_STATIC_LIBRARY */

#pragma pack(pop)                           /* restore original alignment from stack */

#ifdef __cplusplus
}
#endif

#endif /* PUREIMAGE_RUNTIME_RUNTIME_H */
