/* RuntimeError.h
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_RUNTIME_RUNTIMEERROR_H
#define PUREIMAGE_RUNTIME_RUNTIMEERROR_H

#ifdef __cplusplus
extern "C" {
#endif

#include <pIn/pInError.h>

    /** Enumeration of possible CpInCRuntime errors. */
    typedef enum _CRuntimeError {
        pI_CRuntime_Error_NoError =					CpIn_Error_NoError,             /*!< No error occurred.*/
        pI_CRuntime_Error_InsufficientMemory =		CpIn_Error_InsufficientMemory,  /*!< Memory allocation failed.*/
        pI_CRuntime_Error_PluginLookupFailed =		CpIn_Error_PluginLookupFailed,  /*!< Plugin query failed.*/
        pI_CRuntime_Error_Serialization =			CpIn_Error_Serialization,       /*!< Serialization failed.*/
        pI_CRuntime_Error_Deserialization =			CpIn_Error_Deserialization,     /*!< Deserialization failed.*/
        pI_CRuntime_Error_RuntimeVersionMismatch,		                            /*!< Runtime version mismatch.*/
		pI_CRuntime_Error_PluginVersionMismatch,			                        /*!< Plugin version mismatch.*/
		pI_CRuntime_Error_InvalidPlugin,											/*!< Operation on invalid plugin instance.*/
		pI_CRuntime_Error_PluginRegistrationFailed,									/*!< Plugin registration failed.*/
		pI_CRuntime_Error_PluginCreationFailed = CpIn_Error_CreationFailed,			/*!< Plugin creation failed.*/
		pI_CRuntime_Error_InvalidInstance,											/*!< Invalid CRuntime instance.*/
    }
    CRuntimeError;

    typedef enum _ArgumentConstraintError {
        pI_Constraint_Error_NoError = 0,        /*!< No error occurred.*/
        pI_Constraint_Error_ExpectedExpression, /*!< Expected expression error.*/
        pI_Constraint_Error_ExpectedValue,      /*!< Expected value error.*/
        pI_Constraint_Error_UnknownVariable,    /*!< Received unknown variable.*/
        pI_Constraint_Error_IllegalType,        /*!< Received unknown type.*/
        pI_Constraint_Error_StackUnderflow      /*!< Stack underflow error.*/
    } ArgumentConstraintError;

#ifdef __cplusplus
}
#endif

#endif /* PUREIMAGE_RUNTIME_RUNTIMEERROR_H */
