
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton interface for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* "%code requires" blocks.  */



/**
* ATTENTION:
* Remark about thread-safety:
*   Setting the string and calling the parser is not atomic.
*   I.e., parsing is not thread-safe at all.
*
*   So, in multi-threaded environments make sure to secure this cal path
*   (i.e., setting string and call yyparse) by means of a mutex.
*/

#include <PureImage.h>
#include <Runtime/Internal/ConstraintParser/ParserSymbols.h>
#include <Runtime/Internal/ConstraintParser/SymbolStack.h>

/* These defines make accessing the stack and symbol table easier */
#define PIC_PUSH( SYMBOL ) \
do {\
    struct _pI_CP_SymbolStack* symbol_stack;\
    symbol_stack = ((struct _pI_CP_ParserArg*) parser_arg)->symbol_stack;\
    symbol_stack->push(symbol_stack, SYMBOL);\
} while(0)

#define RUNTIME ((struct _pI_CP_ParserArg*) parser_arg)->runtime

/* Defines the signature of the lexer function */
#define YYLEX_PARAM &yylval, &yylloc, RUNTIME

typedef struct _pI_CP_ParserArg
{
  struct _CRuntime* runtime;
  struct _pI_CP_SymbolStack* symbol_stack;
} pI_CP_ParserArg;

/* lexer errors are propagated by means of this (global!) variable */
int pI_CP_lex_error;

/* This defines that the parser has an argument */
#define YYPARSE_PARAM parser_arg


/* Some declarations that build the interface to the parser */

/* Change this variable to control debug messages (use like error levels) */
extern int yydebug;

/**
* Sets the string the parser has to process.
*
* Remark: see comments about thread-safety.
*
* @param str The string to be parsed.
*/
extern void yy_set_string(char* str);

/**
* Executes the parser using a given parser argument structure which contains a pointer to
* the resulting stack and a pointer to the symbol table to be used.
*
* Remark: unfortunately bison generates a void* signature
*
*/
extern int yyparse (void* parse_arg);


/**
* This method is called by yyparse when errors occur.
* So, implement this to receive these parser errors.
*/
extern int yyerror (char* msg);





/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     END = 258,
     AND = 259,
     OR = 260,
     NOT = 261,
     EQ = 262,
     LT = 263,
     LEQ = 264,
     GT = 265,
     GEQ = 266,
     NEQ = 267,
     IDENTIFIER = 268,
     INTEGER = 269,
     DOUBLE = 270,
     STRING = 271,
     TRUE = 272,
     FALSE = 273,
     OPEN_PAREN = 274,
     CLOSE_PAREN = 275,
     ERROR = 276
   };
#endif



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{


   pI_double double_val;
   pI_int int_val;
   pI_bool bool_val;
   pI_str str_val;



} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;

#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} YYLTYPE;
# define yyltype YYLTYPE /* obsolescent; will be withdrawn */
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif

extern YYLTYPE yylloc;

