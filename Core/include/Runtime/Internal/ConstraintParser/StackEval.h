/* StackEval.h
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_RUNTIME_INTERNAL_CONSTRAINTPARSER_STACKEVAL_H
#define PUREIMAGE_RUNTIME_INTERNAL_CONSTRAINTPARSER_STACKEVAL_H

#ifdef __cplusplus
extern "C" {
#endif

#include <PureImage.h>

#include <Runtime/Internal/ConstraintParser/SymbolStack.h>

    /*
        Evaluates a given symbol stack applying a given symbol table (for accessing variable values).

        @param a symbol stack created by the parser.
        @param a symbol table managing variable symbols.
        @return the evaluated expression (true or false)
    */
    extern pI_int pI_CP_evaluate_symbol_stack(struct _pI_CP_SymbolStack* stack, struct _Argument* arg);

    /*
        Gives the current error code.
        Check this whenever EvalStack has been called.
    */
    extern pI_int pI_CP_EvalStack_getLastError();

    extern pI_str pI_CP_EvalStack_getLastErrorMsg();

#ifdef __cplusplus
}
#endif

#endif /* PUREIMAGE_RUNTIME_INTERNAL_CONSTRAINTPARSER_STACKEVAL_H */
