/* ParserSymbols.h
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_RUNTIME_INTERNAL_CONSTRAINTPARSER_PARSERSYMBOLS_H
#define PUREIMAGE_RUNTIME_INTERNAL_CONSTRAINTPARSER_PARSERSYMBOLS_H

#ifdef __cplusplus
extern "C" {
#endif

#include <PureImage.h>

    typedef enum _pI_CP_SymbolCode {
        ps_VAL,
        ps_VAR,
        ps_EQ,
        ps_NEQ,
        ps_LT,
        ps_LEQ,
        ps_GT,
        ps_GEQ,
        ps_AND,
        ps_OR,
        ps_NOT,
        ps_ERROR
    } pI_CP_SymbolCode;

    typedef union _pI_CP_SymbolData {
        pI_int int_val;
        pI_bool bool_val;
        pI_double double_val;
        pI_str str_val;
    } pI_CP_SymbolData;

    typedef struct _pI_CP_Symbol {

        enum _pI_CP_SymbolCode code;

        enum _pI_IntrinsicType value_type;

        union _pI_CP_SymbolData data;

    } pI_CP_Symbol;

    /*
        A symbol containing a constant boolean expression (= value).
    */
    extern struct _pI_CP_Symbol* pI_CP_CreateBoolSymbol(struct _CRuntime* runtime, pI_bool bool_val);

    /*
        A symbol containing a constant numerical value.
    */
    extern struct _pI_CP_Symbol* pI_CP_CreateIntSymbol(struct _CRuntime* runtime, pI_int int_val);

    /*
        A symbol containing a constant numerical value.
    */
    extern struct _pI_CP_Symbol* pI_CP_CreateDoubleSymbol(struct _CRuntime* runtime, pI_double double_val);

    /*
        A symbol containing a constant numerical value.
    */
    extern struct _pI_CP_Symbol* pI_CP_CreateStringSymbol(struct _CRuntime* runtime, pI_str str_val);

    /*
        Keeps information about a variable which represents a numeric value.
    */
    extern struct _pI_CP_Symbol* pI_CP_CreateVariableSymbol(struct _CRuntime* runtime, pI_str id);

    /*
        Used for all operators.
    */
    extern struct _pI_CP_Symbol* pI_CP_CreateOperatorSymbol(struct _CRuntime* runtime, enum _pI_CP_SymbolCode opCode, pI_int arg_count);

    /*
        Used to proceed with parsing if a semantic problem (e.g., unknown variable) occurs.
    */
    extern struct _pI_CP_Symbol* pI_CP_CreateErrorSymbol(struct _CRuntime* runtime, pI_str msg);

    extern void pI_CP_FreeParserSymbol(struct _CRuntime* runtime, struct _pI_CP_Symbol* symbol);

#ifdef __cplusplus
}
#endif

#endif /* PUREIMAGE_RUNTIME_INTERNAL_CONSTRAINTPARSER_PARSERSYMBOLS_H */
