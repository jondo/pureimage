/* SymbolStack.h
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_RUNTIME_INTERNAL_CONSTRAINTPARSER_SYMBOLSTACK_H
#define PUREIMAGE_RUNTIME_INTERNAL_CONSTRAINTPARSER_SYMBOLSTACK_H

#ifdef __cplusplus
extern "C" {
#endif

#include <Runtime/Internal/ConstraintParser/ParserSymbols.h>

#define PIC_STACK_MAXSIZE 50

    /**
          A simple stack for ParserSymbols.

          Attention: there is a constant defining the maximum stack height (currently = 50).
    */
    typedef struct _pI_CP_SymbolStack {

        /* Adapt the constant when running into problems due to size of this stack. */
        struct _pI_CP_Symbol* stack[ PIC_STACK_MAXSIZE ];

        /*
            a pointer to the top of the stack.
        */
        struct _pI_CP_Symbol** top;

        /**
              Stores a given ParserSymbol at the top.
        */
        pI_int (*push) (struct _pI_CP_SymbolStack* stack, struct _pI_CP_Symbol* symbol);

        /*
            Retrieves the top element and decrements the stack's top pointer.
            When the stack is empty top is set to zero.

            Attention: popping a symbol draws the ownership. So it is the caller to clean up
            using FreeSymbol.
        */
        struct _pI_CP_Symbol* (*pop) (struct _pI_CP_SymbolStack* stack);

        /**
              Retrieves the top symbol and decrements the given top pointer.

              Remark: this methods does not (necessarily) change the stacks content.
              It is offered as a means to traverse the stack by "popping".
              Make a local pointer variable pointing to the top and deliver its reference.

              The function will set the pointer to zero if the stack is empty.
        */
        struct _pI_CP_Symbol* (*pop_traverse) (struct _pI_CP_SymbolStack* stack, struct _pI_CP_Symbol** * top);

    } pI_CP_SymbolStack;

    extern void pI_CP_CreateStack(struct _pI_CP_SymbolStack* stack);

    extern void pI_CP_InitStack(struct _pI_CP_SymbolStack* stack);

    extern void pI_CP_FreeSymbolStack(struct _CRuntime* runtime, struct _pI_CP_SymbolStack* symbol_stack);

#ifdef __cplusplus
}
#endif

#endif /* PUREIMAGE_RUNTIME_INTERNAL_CONSTRAINTPARSER_SYMBOLSTACK_H */
