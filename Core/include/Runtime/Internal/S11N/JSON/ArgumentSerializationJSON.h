/* ArgumentSerializationJSON.h
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_RUNTIME_INTERNAL_S11N_JSON_ARGUMENTSERIALIZATIONJSON_H
#define PUREIMAGE_RUNTIME_INTERNAL_S11N_JSON_ARGUMENTSERIALIZATIONJSON_H

#ifdef __cplusplus
extern "C" {
#endif

#include <PureImage.h>

    extern pI_str pI_JSON_SerializeArgument (
        struct _CRuntime* runtime,
        struct _Argument* arg,
        pI_int* snlen);

	extern pI_str pI_JSON_SerializeArgumentList (
        struct _CRuntime* runtime,
        struct _ArgumentList* arg_list,
        pI_int* snlen);

    extern struct _Argument* pI_JSON_DeserializeArgument (
        struct _CRuntime* runtime,
        pI_str arg_str,
        pI_int snlen);

	extern struct _ArgumentList* pI_JSON_DeserializeArgumentList (
        struct _CRuntime* runtime,
        pI_str arg_str,
        pI_int snlen);

#ifdef __cplusplus
}
#endif

#endif /* PUREIMAGE_RUNTIME_INTERNAL_S11N_JSON_ARGUMENTSERIALIZATIONJSON_H */
