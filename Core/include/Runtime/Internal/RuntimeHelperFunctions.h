/* RuntimeHelperFunctions.h
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_RUNTIME_INTERNAL_RUNTIMEHELPERFUNCTIONS_H
#define PUREIMAGE_RUNTIME_INTERNAL_RUNTIMEHELPERFUNCTIONS_H

#ifdef __cplusplus
extern "C" {
#endif

#include <PureImage.h>

/* set new alignment */
#ifdef __GNUC__
#define DO_PRAGMA_EXPAND(x) _Pragma (#x)
#define DO_PRAGMA(x) DO_PRAGMA_EXPAND(x)
DO_PRAGMA (pack (push,PI_STRUCTURE_ALIGNMENT))
#else
#pragma pack(push)                          /* push current alignment to stack */
#pragma pack(PI_STRUCTURE_ALIGNMENT)
#endif

    /* Note: the functions declared in this header are implemented in "Runtime.c",
             due to dependency on local functions and types. */

    extern pI_str* CRuntime_ArgumentSizeDependenciesAsString (
        struct _CRuntime* runtime,
        Argument* arg,
        pI_str symbol);

    extern pI_byte* CRuntime_FetchArgumentSetupSymbol (
        struct _CRuntime* runtime,
        struct _Argument* arg,
        pI_str symbol,
        enum _pIDataTypeClass* kind,
        pI_int* index);

    extern pI_byte* CRuntime_GetArgumentDataBySymbol (
        struct _CRuntime* runtime,
        Argument* arg,
        pI_str symbol,
        pI_bool is_structure_symbol);

    extern enum _pIDataTypeClass CRuntime_GetDataTypeClass (
        struct _CRuntime* runtime,
        pI_str data_type_name,
        pI_int* index);


#pragma pack(pop)                           /* restore original alignment from stack */

#ifdef __cplusplus
}
#endif


#endif /* PUREIMAGE_RUNTIME_INTERNAL_RUNTIMEHELPERFUNCTIONS_H */
