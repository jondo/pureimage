/* IntrinsicRuntimeFunctions.h
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_RUNTIME_INTERNAL_DATATYPES_INTRINSICS_INTRINSICRUNTIMEFUNCTIONS_H
#define PUREIMAGE_RUNTIME_INTERNAL_DATATYPES_INTRINSICS_INTRINSICRUNTIMEFUNCTIONS_H

#ifdef __cplusplus
extern "C" {
#endif

    pI_bool CRuntime_CreateIntrinsicDataType (
        struct _CRuntime* runtime,
        pI_byte* data_ptr,
        enum _pI_IntrinsicType type);

    pI_bool CRuntime_CopyIntrinsicDataType (
        struct _CRuntime* runtime,
        pI_byte* in_data_ptr,
        pI_byte* out_data_ptr,
        enum _pI_IntrinsicType type);

    pI_bool CRuntime_FreeIntrinsicDataType (
        struct _CRuntime* runtime,
        pI_byte* data_ptr,
        enum _pI_IntrinsicType type);

    pI_str CRuntime_SerializeIntrinsicDataType (
        struct _CRuntime* runtime,
        pI_byte* data_ptr,
        enum _pI_IntrinsicType type,
        pI_size* str_len);

    pI_bool CRuntime_DeserializeIntrinsicDataType (
        struct _CRuntime* runtime,
        pI_byte* data_ptr,
        enum _pI_IntrinsicType type,
        pI_str args11n);

#ifdef __cplusplus
}
#endif

#endif /* PUREIMAGE_RUNTIME_INTERNAL_DATATYPES_INTRINSICS_INTRINSICRUNTIMEFUNCTIONS_H */
