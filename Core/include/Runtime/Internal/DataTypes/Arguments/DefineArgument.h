/* DefineArgument.h
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

/* ******************************************************************
    First block enforces X-macro arguments.
   ****************************************************************** */

#ifndef ARGUMENT_NAME
#error "ARGUMENT_NAME must be set before including DefineArgument.h"
#endif /* ARGUMENT_NAME */

#ifndef ARGUMENT_INCLUDE
#error "ARGUMENT_INCLUDE must be set before including DefineArgument.h"
#endif /* ARGUMENT_INCLUDE */

/* ******************************************************************
    Next block validates include preconditions.
   ****************************************************************** */

#ifndef ARGUMENT_DECLARE
#ifndef ARGUMENT_CREATE_ENUM
#ifndef ARGUMENT_CREATE_NAME
#ifndef ARGUMENT_CREATE_SETUP_UNION
#ifndef ARGUMENT_CREATE_DATA_UNION
#ifndef ARGUMENT_CREATE_SETUP_STRUCT_SIZES
#ifndef ARGUMENT_CREATE_DATA_STRUCT_SIZES
#ifndef ARGUMENT_CREATE_SETUP_SYMBOLS
#ifndef ARGUMENT_CREATE_DATA_SYMBOLS
#ifndef ARGUMENT_CREATE_SETUP_SYMBOL_TYPES
#ifndef ARGUMENT_CREATE_DATA_SYMBOL_TYPES
#ifndef ARGUMENT_CREATE_SETUP_SYMBOL_SIZES
#ifndef ARGUMENT_CREATE_DATA_SYMBOL_SIZES
#ifndef ARGUMENT_DEPENDENCY_SYMBOLS
#ifndef ARGUMENT_DEPENDENCY_SYMBOL_SIZES
#ifndef ARGUMENT_CREATE_DATA_DIMENSIONS
#ifndef ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS
#ifndef ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS */
#endif /* ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS */
#endif /* ARGUMENT_CREATE_DATA_DIMENSIONS */
#endif /* ARGUMENT_DEPENDENCY_SYMBOL_SIZES */
#endif /* ARGUMENT_DEPENDENCY_SYMBOLS */
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_SIZES */
#endif /* ARGUMENT_CREATE_SETUP_SYMBOL_SIZES */
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_TYPES */
#endif /* ARGUMENT_CREATE_SETUP_SYMBOL_TYPES */
#endif /* ARGUMENT_CREATE_DATA_SYMBOLS */
#endif /* ARGUMENT_CREATE_SETUP_SYMBOLS */
#endif /* ARGUMENT_CREATE_DATA_STRUCT_SIZES */
#endif /* ARGUMENT_CREATE_SETUP_STRUCT_SIZES */
#endif /* ARGUMENT_CREATE_DATA_UNION */
#endif /* ARGUMENT_CREATE_SETUP_UNION */
#endif /* ARGUMENT_CREATE_NAME */
#endif /* ARGUMENT_CREATE_ENUM */
#endif /* ARGUMENT_DECLARE */

#ifdef ARGUMENT_DECLARE
#ifdef ARGUMENT_CREATE_ENUM
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_UNION */
#ifdef ARGUMENT_CREATE_NAME
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_UNION */
#ifdef ARGUMENT_CREATE_SETUP_UNION
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_UNION */
#ifdef ARGUMENT_CREATE_DATA_UNION
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_UNION */
#ifdef ARGUMENT_CREATE_SETUP_STRUCT_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_STRUCT_SIZES */
#ifdef ARGUMENT_CREATE_DATA_STRUCT_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_STRUCT_SIZES */
#ifdef ARGUMENT_CREATE_SETUP_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOLS */
#ifdef ARGUMENT_CREATE_DATA_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOLS */
#ifdef ARGUMENT_CREATE_SETUP_SYMBOL_TYPES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOL_TYPES */
#ifdef ARGUMENT_CREATE_DATA_SYMBOL_TYPES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_TYPES */
#ifdef ARGUMENT_CREATE_SETUP_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOL_SIZES */
#ifdef ARGUMENT_CREATE_DATA_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_SIZES */
#ifdef ARGUMENT_DEPENDENCY_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOLS */
#ifdef ARGUMENT_DEPENDENCY_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOL_SIZES */
#ifdef ARGUMENT_CREATE_DATA_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS */
#endif /* ARGUMENT_DECLARE */

#ifdef ARGUMENT_CREATE_ENUM
#ifdef ARGUMENT_CREATE_NAME
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_UNION */
#ifdef ARGUMENT_CREATE_SETUP_UNION
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_UNION */
#ifdef ARGUMENT_CREATE_DATA_UNION
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_UNION */
#ifdef ARGUMENT_CREATE_SETUP_STRUCT_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_STRUCT_SIZES */
#ifdef ARGUMENT_CREATE_DATA_STRUCT_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_STRUCT_SIZES */
#ifdef ARGUMENT_CREATE_SETUP_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOLS */
#ifdef ARGUMENT_CREATE_DATA_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOLS */
#ifdef ARGUMENT_CREATE_SETUP_SYMBOL_TYPES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOL_TYPES */
#ifdef ARGUMENT_CREATE_DATA_SYMBOL_TYPES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_TYPES */
#ifdef ARGUMENT_CREATE_SETUP_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOL_SIZES */
#ifdef ARGUMENT_CREATE_DATA_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_SIZES */
#ifdef ARGUMENT_DEPENDENCY_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOLS */
#ifdef ARGUMENT_DEPENDENCY_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOL_SIZES */
#ifdef ARGUMENT_CREATE_DATA_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS */
#endif /* ARGUMENT_CREATE_ENUM */

#ifdef ARGUMENT_CREATE_NAME
#ifdef ARGUMENT_CREATE_SETUP_UNION
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_UNION */
#ifdef ARGUMENT_CREATE_DATA_UNION
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_UNION */
#ifdef ARGUMENT_CREATE_SETUP_STRUCT_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_STRUCT_SIZES */
#ifdef ARGUMENT_CREATE_DATA_STRUCT_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_STRUCT_SIZES */
#ifdef ARGUMENT_CREATE_SETUP_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOLS */
#ifdef ARGUMENT_CREATE_DATA_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOLS */
#ifdef ARGUMENT_CREATE_SETUP_SYMBOL_TYPES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOL_TYPES */
#ifdef ARGUMENT_CREATE_DATA_SYMBOL_TYPES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_TYPES */
#ifdef ARGUMENT_CREATE_SETUP_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOL_SIZES */
#ifdef ARGUMENT_CREATE_DATA_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_SIZES */
#ifdef ARGUMENT_DEPENDENCY_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOLS */
#ifdef ARGUMENT_DEPENDENCY_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOL_SIZES */
#ifdef ARGUMENT_CREATE_DATA_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS */
#endif /* ARGUMENT_CREATE_NAME */

#ifdef ARGUMENT_CREATE_SETUP_UNION
#ifdef ARGUMENT_CREATE_DATA_UNION
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_UNION */
#ifdef ARGUMENT_CREATE_SETUP_STRUCT_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_STRUCT_SIZES */
#ifdef ARGUMENT_CREATE_DATA_STRUCT_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_STRUCT_SIZES */
#ifdef ARGUMENT_CREATE_SETUP_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOLS */
#ifdef ARGUMENT_CREATE_DATA_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOLS */
#ifdef ARGUMENT_CREATE_SETUP_SYMBOL_TYPES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOL_TYPES */
#ifdef ARGUMENT_CREATE_DATA_SYMBOL_TYPES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_TYPES */
#ifdef ARGUMENT_CREATE_SETUP_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOL_SIZES */
#ifdef ARGUMENT_CREATE_DATA_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_SIZES */
#ifdef ARGUMENT_DEPENDENCY_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOLS */
#ifdef ARGUMENT_DEPENDENCY_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOL_SIZES */
#ifdef ARGUMENT_CREATE_DATA_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS */
#endif /* ARGUMENT_CREATE_SETUP_UNION */

#ifdef ARGUMENT_CREATE_DATA_UNION
#ifdef ARGUMENT_CREATE_SETUP_STRUCT_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_STRUCT_SIZES */
#ifdef ARGUMENT_CREATE_DATA_STRUCT_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_STRUCT_SIZES */
#ifdef ARGUMENT_CREATE_SETUP_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOLS */
#ifdef ARGUMENT_CREATE_DATA_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOLS */
#ifdef ARGUMENT_CREATE_SETUP_SYMBOL_TYPES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOL_TYPES */
#ifdef ARGUMENT_CREATE_DATA_SYMBOL_TYPES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_TYPES */
#ifdef ARGUMENT_CREATE_SETUP_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOL_SIZES */
#ifdef ARGUMENT_CREATE_DATA_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_SIZES */
#ifdef ARGUMENT_DEPENDENCY_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOLS */
#ifdef ARGUMENT_DEPENDENCY_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOL_SIZES */
#ifdef ARGUMENT_CREATE_DATA_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS */
#endif /* ARGUMENT_CREATE_DATA_UNION */

#ifdef ARGUMENT_CREATE_SETUP_STRUCT_SIZES
#ifdef ARGUMENT_CREATE_DATA_STRUCT_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_STRUCT_SIZES */
#ifdef ARGUMENT_CREATE_SETUP_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOLS */
#ifdef ARGUMENT_CREATE_DATA_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOLS */
#ifdef ARGUMENT_CREATE_SETUP_SYMBOL_TYPES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOL_TYPES */
#ifdef ARGUMENT_CREATE_DATA_SYMBOL_TYPES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_TYPES */
#ifdef ARGUMENT_CREATE_SETUP_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOL_SIZES */
#ifdef ARGUMENT_CREATE_DATA_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_SIZES */
#ifdef ARGUMENT_DEPENDENCY_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOLS */
#ifdef ARGUMENT_DEPENDENCY_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOL_SIZES */
#ifdef ARGUMENT_CREATE_DATA_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS */
#endif /* ARGUMENT_CREATE_SETUP_STRUCT_SIZES */

#ifdef ARGUMENT_CREATE_DATA_STRUCT_SIZES
#ifdef ARGUMENT_CREATE_SETUP_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOLS */
#ifdef ARGUMENT_CREATE_DATA_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOLS */
#ifdef ARGUMENT_CREATE_SETUP_SYMBOL_TYPES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOL_TYPES */
#ifdef ARGUMENT_CREATE_DATA_SYMBOL_TYPES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_TYPES */
#ifdef ARGUMENT_CREATE_SETUP_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOL_SIZES */
#ifdef ARGUMENT_CREATE_DATA_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_SIZES */
#ifdef ARGUMENT_DEPENDENCY_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOLS */
#ifdef ARGUMENT_DEPENDENCY_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOL_SIZES */
#ifdef ARGUMENT_CREATE_DATA_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS */
#endif /* ARGUMENT_CREATE_DATA_STRUCT_SIZES */

#ifdef ARGUMENT_CREATE_SETUP_SYMBOLS
#ifdef ARGUMENT_CREATE_DATA_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOLS */
#ifdef ARGUMENT_CREATE_SETUP_SYMBOL_TYPES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOL_TYPES */
#ifdef ARGUMENT_CREATE_DATA_SYMBOL_TYPES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_TYPES */
#ifdef ARGUMENT_CREATE_SETUP_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOL_SIZES */
#ifdef ARGUMENT_CREATE_DATA_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_SIZES */
#ifdef ARGUMENT_DEPENDENCY_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOLS */
#ifdef ARGUMENT_DEPENDENCY_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOL_SIZES */
#ifdef ARGUMENT_CREATE_DATA_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS */
#endif /* ARGUMENT_CREATE_SETUP_SYMBOLS */

#ifdef ARGUMENT_CREATE_DATA_SYMBOLS
#ifdef ARGUMENT_CREATE_SETUP_SYMBOL_TYPES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOL_TYPES */
#ifdef ARGUMENT_CREATE_DATA_SYMBOL_TYPES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_TYPES */
#ifdef ARGUMENT_CREATE_SETUP_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOL_SIZES */
#ifdef ARGUMENT_CREATE_DATA_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_SIZES */
#ifdef ARGUMENT_DEPENDENCY_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOLS */
#ifdef ARGUMENT_DEPENDENCY_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOL_SIZES */
#ifdef ARGUMENT_CREATE_DATA_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS */
#endif /* ARGUMENT_CREATE_DATA_SYMBOLS */

#ifdef ARGUMENT_CREATE_SETUP_SYMBOL_TYPES
#ifdef ARGUMENT_CREATE_DATA_SYMBOL_TYPES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_TYPES */
#ifdef ARGUMENT_CREATE_SETUP_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOL_SIZES */
#ifdef ARGUMENT_CREATE_DATA_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_SIZES */
#ifdef ARGUMENT_DEPENDENCY_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOLS */
#ifdef ARGUMENT_DEPENDENCY_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOL_SIZES */
#ifdef ARGUMENT_CREATE_DATA_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS */
#endif /* ARGUMENT_CREATE_SETUP_SYMBOL_TYPES */

#ifdef ARGUMENT_CREATE_DATA_SYMBOL_TYPES
#ifdef ARGUMENT_CREATE_SETUP_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_SETUP_SYMBOL_SIZES */
#ifdef ARGUMENT_CREATE_DATA_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_SIZES */
#ifdef ARGUMENT_DEPENDENCY_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOLS */
#ifdef ARGUMENT_DEPENDENCY_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOL_SIZES */
#ifdef ARGUMENT_CREATE_DATA_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS */
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_TYPES */

#ifdef ARGUMENT_CREATE_SETUP_SYMBOL_SIZES
#ifdef ARGUMENT_CREATE_DATA_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_SIZES */
#ifdef ARGUMENT_DEPENDENCY_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOLS */
#ifdef ARGUMENT_DEPENDENCY_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOL_SIZES */
#ifdef ARGUMENT_CREATE_DATA_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS */
#endif /* ARGUMENT_CREATE_SETUP_SYMBOL_SIZES */

#ifdef ARGUMENT_CREATE_DATA_SYMBOL_SIZES
#ifdef ARGUMENT_DEPENDENCY_SYMBOLS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOLS */
#ifdef ARGUMENT_DEPENDENCY_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOL_SIZES */
#ifdef ARGUMENT_CREATE_DATA_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS */
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_SIZES */

#ifdef ARGUMENT_DEPENDENCY_SYMBOLS
#ifdef ARGUMENT_DEPENDENCY_SYMBOL_SIZES
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_DEPENDENCY_SYMBOL_SIZES */
#ifdef ARGUMENT_CREATE_DATA_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS */
#endif /* ARGUMENT_DEPENDENCY_SYMBOLS */

#ifdef ARGUMENT_DEPENDENCY_SYMBOL_SIZES
#ifdef ARGUMENT_CREATE_DATA_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS */
#endif /* ARGUMENT_DEPENDENCY_SYMBOL_SIZES */

#ifdef ARGUMENT_CREATE_DATA_DIMENSIONS
#ifdef ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS */
#ifdef ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS */
#endif /* ARGUMENT_CREATE_DATA_DIMENSIONS */

#ifdef ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS
#ifdef ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS
#error "Valid defines must be set before including DefineArgument.h"
#endif /* ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS */
#endif /* ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS */

/* ******************************************************************
    Next block creates the names for the argument SETUPs.
   ****************************************************************** */

#define ARGUMENT_PREFIX                     pI_Argument_
#define ARGUMENT_PREFIX_STRUCT              _pI_Argument_
#define ARGUMENT_PREFIX_ENUM                T_pI_Argument_

#define S2_(x, y)                           x##y
#define S2(x, y)                            S2_(x, y)
#define S3_(x, y, z)                        x##y##z
#define S3(x, y, z)                         S3_(x, y, z)
#define S4_(w, x, y, z)                     w##x##y##z
#define S4(w, x, y, z)                      S4_(w, x, y, z)
/* FIX: with gnu cpp concatenating '*' with ##* does not work */
/* #define DATA_PTR_DECL_(u, v, w, x, y, z)  u v##w##xy z;*/
#define DATA_PTR_DECL_(u, v, w, x, y, z)    u v##w##x* z;
#define DATA_PTR_DECL(u, v, w, x, y, z)     DATA_PTR_DECL_(u, v, w, x, y, z)

#define QUOTEME_(x)                         #x
#define QUOTEME(x)                          QUOTEME_(x)
#define ARGUMENT_ENUM__(x, y)               x##y
#define ARGUMENT_ENUM_(x, y)                ARGUMENT_ENUM__(x, y)
#define ARGUMENT_ENUM(x)                    ARGUMENT_ENUM_(ARGUMENT_PREFIX_ENUM, x)
#define _ARGUMENT_SETUP                     S3(ARGUMENT_PREFIX_STRUCT, ARGUMENT_NAME, _Setup)
#define ARGUMENT_SETUP                      S3(ARGUMENT_PREFIX, ARGUMENT_NAME, _Setup)
#define ARGUMENT_SETUP_SYMBOLS              S3(ARGUMENT_PREFIX, ARGUMENT_NAME, _Setup_Symbols)
#define ARGUMENT_SETUP_SYMBOL_TYPES         S3(ARGUMENT_PREFIX, ARGUMENT_NAME, _Setup_Symbol_Types)
#define ARGUMENT_SETUP_DEPENDENCIES         S3(ARGUMENT_PREFIX, ARGUMENT_NAME, _Setup_Dependencies)
#define _ARGUMENT_DATA                      S3(ARGUMENT_PREFIX_STRUCT, ARGUMENT_NAME, _Data)
#define ARGUMENT_DATA                       S3(ARGUMENT_PREFIX, ARGUMENT_NAME, _Data)
#define ARGUMENT_DATA_PTR                   DATA_PTR_DECL(struct, ARGUMENT_PREFIX_STRUCT, ARGUMENT_NAME, _Data, *, ARGUMENT_NAME)
#define ARGUMENT_DATA_DIMENSIONS            S3(ARGUMENT_PREFIX, ARGUMENT_NAME, _Data_Dimensions)
#define ARGUMENT_DATA_BLOCK_DIMENSIONS      S3(ARGUMENT_PREFIX, ARGUMENT_NAME, _Data_Block_Dimensions)
#define ARGUMENT_DATA_PADDING_DIMENSION     S3(ARGUMENT_PREFIX, ARGUMENT_NAME, _Data_Padding_Dimension)
#define ARGUMENT_SETUP_PTR                  DATA_PTR_DECL(struct, ARGUMENT_PREFIX_STRUCT, ARGUMENT_NAME, _Setup, *, ARGUMENT_NAME)
#define ARGUMENT_DATA_SYMBOLS               S3(ARGUMENT_PREFIX, ARGUMENT_NAME, _Data_Symbols)
#define ARGUMENT_DATA_SYMBOL_TYPES          S3(ARGUMENT_PREFIX, ARGUMENT_NAME, _Data_Symbol_Types)

/* ******************************************************************
    Next block checks wheter argument definition lacks either:
     SETUP definition
     DATA definition
   ****************************************************************** */

#define SETUP(a, b)
#define DATA(a, b)
#define DATA_1D(a, b, c)
#define DATA_2D(a, b, c, d)
#define DATA_3D(a, b, c, d, e)
#define DATA_4D(a, b, c, d, e, f)
#define DATA_5D(a, b, c, d, e, f, g)
#define DATA_2D_BLOCK(a, b, c, d, n, p)
#define DATA_3D_BLOCK(a, b, c, d, e, n, p)
#define DATA_4D_BLOCK(a, b, c, d, e, f, n, p)
#define DATA_5D_BLOCK(a, b, c, d, e, f, g, n, p)

#include ARGUMENT_INCLUDE
#ifdef NO_ARGUMENT_SETUP
#define ARGUMENT_NO_SETUP
#endif /* NO_ARGUMENT_SETUP */
#ifdef NO_ARGUMENT_DATA
#define ARGUMENT_NO_DATA
#endif /* NO_ARGUMENT_DATA */
#include "UndoArgumentDefines.h"

#undef DATA_5D_BLOCK
#undef DATA_4D_BLOCK
#undef DATA_3D_BLOCK
#undef DATA_2D_BLOCK

#undef DATA_5D
#undef DATA_4D
#undef DATA_3D
#undef DATA_2D
#undef DATA_1D

#undef DATA
#undef SETUP

/* ******************************************************************
    Next block creates the argument SETUPs.
   ****************************************************************** */

#ifdef ARGUMENT_DECLARE

#define DATA(a, b)
#define DATA_1D(x, a, b)
#define DATA_2D(x, a, b, c)
#define DATA_3D(x, a, b, c, d)
#define DATA_4D(x, a, b, c, d, e)
#define DATA_5D(x, a, b, c, d, e, f)
#define DATA_2D_BLOCK(x, a, b, c, n, p)
#define DATA_3D_BLOCK(x, a, b, c, d, n, p)
#define DATA_4D_BLOCK(x, a, b, c, d, e, n, p)
#define DATA_5D_BLOCK(x, a, b, c, d, e, f, n, p)

/* create argument SETUP struct */
typedef struct _ARGUMENT_SETUP {
#ifdef ARGUMENT_NO_SETUP
    void* none;
#else /* #ifdef ARGUMENT_NO_SETUP */
#define SETUP(a, b) a b;
#include ARGUMENT_INCLUDE
#include "UndoArgumentDefines.h"
#undef SETUP
#endif /* ARGUMENT_NO_SETUP */
} ARGUMENT_SETUP;


#ifdef PI_BUILD_RUNTIME
/* create argument SETUP string symbols */
#ifdef ARGUMENT_NO_SETUP
pI_char** ARGUMENT_SETUP_SYMBOLS = 0;
#else /* #ifdef ARGUMENT_NO_SETUP */
pI_char* ARGUMENT_SETUP_SYMBOLS[] = {
#define SETUP(a, b) #b,
#include ARGUMENT_INCLUDE
#include "UndoArgumentDefines.h"
#undef SETUP
};
#endif /* ARGUMENT_NO_SETUP */
#else /* #ifdef PI_BUILD_RUNTIME */
#ifdef ARGUMENT_NO_SETUP
extern pI_char** ARGUMENT_SETUP_SYMBOLS;
#else /* #ifdef ARGUMENT_NO_SETUP */
extern pI_char* ARGUMENT_SETUP_SYMBOLS[];
#endif /* ARGUMENT_NO_SETUP */
#endif /* #ifdef PI_BUILD_RUNTIME */

#ifdef PI_BUILD_RUNTIME
/* create argument SETUP string symbols */
#ifdef ARGUMENT_NO_SETUP
pI_char** ARGUMENT_SETUP_SYMBOL_TYPES = 0;
#else /* #ifdef ARGUMENT_NO_SETUP */
pI_char* ARGUMENT_SETUP_SYMBOL_TYPES[] = {
#define SETUP(a, b) #a,
#include ARGUMENT_INCLUDE
#include "UndoArgumentDefines.h"
#undef SETUP
};
#endif /* ARGUMENT_NO_SETUP */
#else /* #ifdef PI_BUILD_RUNTIME */
#ifdef ARGUMENT_NO_SETUP
extern pI_char** ARGUMENT_SETUP_SYMBOL_TYPES;
#else /* #ifdef ARGUMENT_NO_SETUP */
extern pI_char* ARGUMENT_SETUP_SYMBOL_TYPES[];
#endif /* ARGUMENT_NO_SETUP */
#endif /* #ifdef PI_BUILD_RUNTIME */

#undef DATA_5D_BLOCK
#undef DATA_4D_BLOCK
#undef DATA_3D_BLOCK
#undef DATA_2D_BLOCK
#undef DATA_5D
#undef DATA_4D
#undef DATA_3D
#undef DATA_2D
#undef DATA_1D
#define SETUP(a, b)

#undef DATA

/* create argument size dependency as string-array description */
#ifdef PI_BUILD_RUNTIME
#ifdef ARGUMENT_NO_DATA
pI_char** ARGUMENT_SETUP_DEPENDENCIES = 0;
#else /* #ifdef ARGUMENT_NO_DATA */
pI_char* ARGUMENT_SETUP_DEPENDENCIES[] = {
#define DATA(x, a)                                  #a, 0,
#define DATA_1D(x, a, b)                            #a, #b, 0,
#define DATA_2D(x, a, b, c)                         #a, #b, #c, 0,
#define DATA_3D(x, a, b, c, d)                      #a, #b, #c, #d, 0,
#define DATA_4D(x, a, b, c, d, e)                   #a, #b, #c, #d, #e, 0,
#define DATA_5D(x, a, b, c, d, e, f)                #a, #b, #c, #d, #e, #f, 0,
#define DATA_2D_BLOCK(x, a, b, c, n, p)             #a, #b, #c, 0,
#define DATA_3D_BLOCK(x, a, b, c, d, n, p)          #a, #b, #c, #d, 0,
#define DATA_4D_BLOCK(x, a, b, c, d, e, n, p)       #a, #b, #c, #d, #e, 0,
#define DATA_5D_BLOCK(x, a, b, c, d, e, f, n, p)    #a, #b, #c, #d, #e, #f, 0,
#include ARGUMENT_INCLUDE
#include "UndoArgumentDefines.h"
#undef DATA_5D_BLOCK
#undef DATA_4D_BLOCK
#undef DATA_3D_BLOCK
#undef DATA_2D_BLOCK
#undef DATA_5D
#undef DATA_4D
#undef DATA_3D
#undef DATA_2D
#undef DATA_1D
#undef DATA
};
#endif /* #ifdef ARGUMENT_NO_DATA */
#else /* #ifdef PI_BUILD_RUNTIME */
#ifdef ARGUMENT_NO_DATA
extern pI_char** ARGUMENT_SETUP_DEPENDENCIES;
#else /* #ifdef ARGUMENT_NO_DATA */
extern pI_char* ARGUMENT_SETUP_DEPENDENCIES[];
#endif /* ARGUMENT_NO_DATA */
#endif /* #ifdef PI_BUILD_RUNTIME */

#undef SETUP

#define SETUP(a, b)

/* create argument data struct */
typedef struct _ARGUMENT_DATA {
#ifdef ARGUMENT_NO_DATA
    void* none;
#else /* #ifdef ARGUMENT_NO_DATA */
#define DATA(a, b)                                  a b;
#define DATA_1D(x, a, b)                            x* a;
#define DATA_2D(x, a, b, c)                         x** a;
#define DATA_3D(x, a, b, c, d)                      x*** a;
#define DATA_4D(x, a, b, c, d, e)                   x**** a;
#define DATA_5D(x, a, b, c, d, e, f)                x***** a;
#define DATA_2D_BLOCK(x, a, b, c, n, p)             x** a;
#define DATA_3D_BLOCK(x, a, b, c, d, n, p)          x*** a;
#define DATA_4D_BLOCK(x, a, b, c, d, e, n, p)       x**** a;
#define DATA_5D_BLOCK(x, a, b, c, d, e, f, n, p)    x***** a;
#include ARGUMENT_INCLUDE
#include "UndoArgumentDefines.h"
#undef DATA_5D_BLOCK
#undef DATA_4D_BLOCK
#undef DATA_3D_BLOCK
#undef DATA_2D_BLOCK
#undef DATA_5D
#undef DATA_4D
#undef DATA_3D
#undef DATA_2D
#undef DATA_1D
#undef DATA
#endif /* #ifdef ARGUMENT_NO_DATA */
} ARGUMENT_DATA;

/* creates size field of data dimensions */
#ifdef PI_BUILD_RUNTIME
#ifdef ARGUMENT_NO_DATA
pI_int* ARGUMENT_DATA_DIMENSIONS = 0;
#else /* #ifdef ARGUMENT_NO_DATA */
pI_int ARGUMENT_DATA_DIMENSIONS[] = {
#define DATA(a, b)                                  0,
#define DATA_1D(x, a, b)                            1,
#define DATA_2D(x, a, b, c)                         2,
#define DATA_3D(x, a, b, c, d)                      3,
#define DATA_4D(x, a, b, c, d, e)                   4,
#define DATA_5D(x, a, b, c, d, e, f)                5,
#define DATA_2D_BLOCK(x, a, b, c, n, p)             2,
#define DATA_3D_BLOCK(x, a, b, c, d, n, p)          3,
#define DATA_4D_BLOCK(x, a, b, c, d, e, n, p)       4,
#define DATA_5D_BLOCK(x, a, b, c, d, e, f, n, p)    5,
#include ARGUMENT_INCLUDE
#include "UndoArgumentDefines.h"
#undef DATA_5D_BLOCK
#undef DATA_4D_BLOCK
#undef DATA_3D_BLOCK
#undef DATA_2D_BLOCK
#undef DATA_5D
#undef DATA_4D
#undef DATA_3D
#undef DATA_2D
#undef DATA_1D
#undef DATA
};
#endif /* #ifdef ARGUMENT_NO_DATA */
#else /* #ifdef PI_BUILD_RUNTIME */
#ifdef ARGUMENT_NO_DATA
extern pI_int* ARGUMENT_DATA_DIMENSIONS;
#else /* #ifdef ARGUMENT_NO_DATA */
extern pI_int ARGUMENT_DATA_DIMENSIONS[];
#endif /* ARGUMENT_NO_DATA */
#endif /* #ifdef PI_BUILD_RUNTIME */

/* creates size field of data block dimensions */
#ifdef PI_BUILD_RUNTIME
#ifdef ARGUMENT_NO_DATA
pI_int* ARGUMENT_DATA_BLOCK_DIMENSIONS = 0;
#else /* #ifdef ARGUMENT_NO_DATA */
pI_int ARGUMENT_DATA_BLOCK_DIMENSIONS[] = {
#define DATA(a, b)                                  0,
#define DATA_1D(x, a, b)                            1,
#define DATA_2D(x, a, b, c)                         1,
#define DATA_3D(x, a, b, c, d)                      1,
#define DATA_4D(x, a, b, c, d, e)                   1,
#define DATA_5D(x, a, b, c, d, e, f)                1,
#define DATA_2D_BLOCK(x, a, b, c, n, p)             n,
#define DATA_3D_BLOCK(x, a, b, c, d, n, p)          n,
#define DATA_4D_BLOCK(x, a, b, c, d, e, n, p)       n,
#define DATA_5D_BLOCK(x, a, b, c, d, e, f, n, p)    n,
#include ARGUMENT_INCLUDE
#include "UndoArgumentDefines.h"
#undef DATA_5D_BLOCK
#undef DATA_4D_BLOCK
#undef DATA_3D_BLOCK
#undef DATA_2D_BLOCK
#undef DATA_5D
#undef DATA_4D
#undef DATA_3D
#undef DATA_2D
#undef DATA_1D
#undef DATA
};
#endif /* #ifdef ARGUMENT_NO_DATA */
#else /* #ifdef PI_BUILD_RUNTIME */
#ifdef ARGUMENT_NO_DATA
extern pI_int* ARGUMENT_DATA_BLOCK_DIMENSIONS;
#else /* #ifdef ARGUMENT_NO_DATA */
extern pI_int ARGUMENT_DATA_BLOCK_DIMENSIONS[];
#endif /* ARGUMENT_NO_DATA */
#endif /* #ifdef PI_BUILD_RUNTIME */

/* creates size field of data padding dimension */
#ifdef PI_BUILD_RUNTIME
#ifdef ARGUMENT_NO_DATA
pI_int* ARGUMENT_DATA_PADDING_DIMENSION = 0;
#else /* #ifdef ARGUMENT_NO_DATA */
pI_int ARGUMENT_DATA_PADDING_DIMENSION[] = {
#define DATA(a, b)                                  0,
#define DATA_1D(x, a, b)                            1,
#define DATA_2D(x, a, b, c)                         1,
#define DATA_3D(x, a, b, c, d)                      1,
#define DATA_4D(x, a, b, c, d, e)                   1,
#define DATA_5D(x, a, b, c, d, e, f)                1,
#define DATA_2D_BLOCK(x, a, b, c, n, p)             p,
#define DATA_3D_BLOCK(x, a, b, c, d, n, p)          p,
#define DATA_4D_BLOCK(x, a, b, c, d, e, n, p)       p,
#define DATA_5D_BLOCK(x, a, b, c, d, e, f, n, p)    p,
#include ARGUMENT_INCLUDE
#include "UndoArgumentDefines.h"
#undef DATA_5D_BLOCK
#undef DATA_4D_BLOCK
#undef DATA_3D_BLOCK
#undef DATA_2D_BLOCK
#undef DATA_5D
#undef DATA_4D
#undef DATA_3D
#undef DATA_2D
#undef DATA_1D
#undef DATA
};
#endif /* #ifdef ARGUMENT_NO_DATA */
#else /* #ifdef PI_BUILD_RUNTIME */
#ifdef ARGUMENT_NO_DATA
extern pI_int* ARGUMENT_DATA_PADDING_DIMENSION;
#else /* #ifdef ARGUMENT_NO_DATA */
extern pI_int ARGUMENT_DATA_PADDING_DIMENSION[];
#endif /* ARGUMENT_NO_DATA */
#endif /* #ifdef PI_BUILD_RUNTIME */

/* create argument data string symbols */
#ifdef PI_BUILD_RUNTIME
#ifdef ARGUMENT_NO_DATA
pI_char** ARGUMENT_DATA_SYMBOLS = 0;
#else /* #ifdef ARGUMENT_NO_DATA */
pI_char* ARGUMENT_DATA_SYMBOLS[] = {
#define DATA(a, b)                                  #b,
#define DATA_1D(x, a, b)                            #a,
#define DATA_2D(x, a, b, c)                         #a,
#define DATA_3D(x, a, b, c, d)                      #a,
#define DATA_4D(x, a, b, c, d, e)                   #a,
#define DATA_5D(x, a, b, c, d, e, f)                #a,
#define DATA_2D_BLOCK(x, a, b, c, n, p)             #a,
#define DATA_3D_BLOCK(x, a, b, c, d, n, p)          #a,
#define DATA_4D_BLOCK(x, a, b, c, d, e, n, p)       #a,
#define DATA_5D_BLOCK(x, a, b, c, d, e, f, n, p)    #a,
#include ARGUMENT_INCLUDE
#include "UndoArgumentDefines.h"
#undef DATA_5D_BLOCK
#undef DATA_4D_BLOCK
#undef DATA_3D_BLOCK
#undef DATA_2D_BLOCK
#undef DATA_5D
#undef DATA_4D
#undef DATA_3D
#undef DATA_2D
#undef DATA_1D
#undef DATA
};
#endif /* #ifdef ARGUMENT_NO_DATA */
#else /* #ifdef PI_BUILD_RUNTIME */
#ifdef ARGUMENT_NO_DATA
extern pI_char** ARGUMENT_DATA_SYMBOLS;
#else /* #ifdef ARGUMENT_NO_DATA */
extern pI_char* ARGUMENT_DATA_SYMBOLS[];
#endif /* ARGUMENT_NO_DATA */
#endif /* #ifdef PI_BUILD_RUNTIME */

#ifdef PI_BUILD_RUNTIME
#ifdef ARGUMENT_NO_DATA
pI_char** ARGUMENT_DATA_SYMBOL_TYPES = 0;
#else /* #ifdef ARGUMENT_NO_DATA */
pI_char* ARGUMENT_DATA_SYMBOL_TYPES[] = {
#define DATA(a, b)                                  #a,
#define DATA_1D(x, a, b)                            #x,
#define DATA_2D(x, a, b, c)                         #x,
#define DATA_3D(x, a, b, c, d)                      #x,
#define DATA_4D(x, a, b, c, d, e)                   #x,
#define DATA_5D(x, a, b, c, d, e, f)                #x,
#define DATA_2D_BLOCK(x, a, b, c, n, p)             #x,
#define DATA_3D_BLOCK(x, a, b, c, d, n, p)          #x,
#define DATA_4D_BLOCK(x, a, b, c, d, e, n, p)       #x,
#define DATA_5D_BLOCK(x, a, b, c, d, e, f, n, p)    #x,
#include ARGUMENT_INCLUDE
#include "UndoArgumentDefines.h"
#undef DATA_5D_BLOCK
#undef DATA_4D_BLOCK
#undef DATA_3D_BLOCK
#undef DATA_2D_BLOCK
#undef DATA_5D
#undef DATA_4D
#undef DATA_3D
#undef DATA_2D
#undef DATA_1D
#undef DATA
};
#endif /* #ifdef ARGUMENT_NO_DATA */
#else /* #ifdef PI_BUILD_RUNTIME */
#ifdef ARGUMENT_NO_DATA
extern pI_char** ARGUMENT_DATA_SYMBOL_TYPES;
#else /* #ifdef ARGUMENT_NO_DATA */
extern pI_char* ARGUMENT_DATA_SYMBOL_TYPES[];
#endif /* ARGUMENT_NO_DATA */
#endif /* #ifdef PI_BUILD_RUNTIME */

#undef SETUP

#endif /* ARGUMENT_DECLARE */

/* ******************************************************************
    Next block creates global argument information accessors.
   ****************************************************************** */

#ifdef ARGUMENT_CREATE_ENUM
ARGUMENT_ENUM (ARGUMENT_NAME),
#endif /* ARGUMENT_CREATE_ENUM */

#ifdef ARGUMENT_CREATE_NAME
              QUOTEME (S2 (ARGUMENT_PREFIX, ARGUMENT_NAME)),
#endif /* ARGUMENT_CREATE_NAME */

#ifdef ARGUMENT_CREATE_SETUP_UNION
              ARGUMENT_SETUP_PTR
#endif /* ARGUMENT_CREATE_SETUP_UNION */

#ifdef ARGUMENT_CREATE_DATA_UNION
              ARGUMENT_DATA_PTR
#endif /* ARGUMENT_CREATE_DATA_UNION */

#ifdef ARGUMENT_CREATE_SETUP_STRUCT_SIZES
#ifdef ARGUMENT_NO_SETUP
              0,
#else /* #ifdef ARGUMENT_NO_SETUP */
              sizeof(struct _ARGUMENT_SETUP),
#endif /* #ifdef ARGUMENT_NO_SETUP */
#endif /* ARGUMENT_CREATE_SETUP_STRUCT_SIZES */

#ifdef ARGUMENT_CREATE_DATA_STRUCT_SIZES
#ifdef ARGUMENT_NO_DATA
              0,
#else /* #ifdef ARGUMENT_NO_DATA */
              sizeof(struct _ARGUMENT_DATA),
#endif /* #ifdef ARGUMENT_NO_DATA */
#endif /* ARGUMENT_CREATE_DATA_STRUCT_SIZES */

#ifdef ARGUMENT_CREATE_SETUP_SYMBOLS
#ifdef ARGUMENT_NO_SETUP
              0,
#else /* #ifdef ARGUMENT_NO_SETUP */
              ARGUMENT_SETUP_SYMBOLS,
#endif /* ARGUMENT_NO_SETUP */
#endif /* ARGUMENT_CREATE_SETUP_SYMBOLS */

#ifdef ARGUMENT_CREATE_DATA_SYMBOLS
#ifdef ARGUMENT_NO_DATA
              0,
#else /* #ifdef ARGUMENT_NO_DATA */
              ARGUMENT_DATA_SYMBOLS,
#endif /* ARGUMENT_NO_DATA */
#endif /* ARGUMENT_CREATE_DATA_SYMBOLS */

#ifdef ARGUMENT_CREATE_SETUP_SYMBOL_TYPES
#ifdef ARGUMENT_NO_SETUP
              0,
#else /* #ifdef ARGUMENT_NO_SETUP */
              ARGUMENT_SETUP_SYMBOL_TYPES,
#endif /* ARGUMENT_NO_SETUP */
#endif /* ARGUMENT_CREATE_SETUP_SYMBOL_TYPES */

#ifdef ARGUMENT_CREATE_DATA_SYMBOL_TYPES
#ifdef ARGUMENT_NO_DATA
              0,
#else /* #ifdef ARGUMENT_NO_SETUP */
              ARGUMENT_DATA_SYMBOL_TYPES,
#endif /* ARGUMENT_NO_SETUP */
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_TYPES */

#ifdef ARGUMENT_CREATE_SETUP_SYMBOL_SIZES
#ifdef ARGUMENT_NO_SETUP
              0,
#else /* #ifdef ARGUMENT_NO_SETUP */
              sizeof(ARGUMENT_SETUP_SYMBOLS) / sizeof(pI_char* ),
#endif /* ARGUMENT_NO_SETUP */
#endif /* ARGUMENT_CREATE_SETUP_SYMBOL_SIZES */

#ifdef ARGUMENT_CREATE_DATA_SYMBOL_SIZES
#ifdef ARGUMENT_NO_DATA
              0,
#else /* #ifdef ARGUMENT_NO_DATA  */
              sizeof(ARGUMENT_DATA_SYMBOLS) / sizeof(pI_char* ),
#endif /* ARGUMENT_NO_DATA */
#endif /* ARGUMENT_CREATE_DATA_SYMBOL_SIZES */

#ifdef ARGUMENT_CREATE_DATA_DIMENSIONS
#ifdef ARGUMENT_NO_DATA
              0,
#else /* #ifdef ARGUMENT_NO_DATA */
              ARGUMENT_DATA_DIMENSIONS,
#endif /* ARGUMENT_NO_DATA */
#endif /* ARGUMENT_CREATE_DATA_DIMENSIONS */

#ifdef ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS
#ifdef ARGUMENT_NO_DATA
              0,
#else /* #ifdef ARGUMENT_NO_DATA */
              ARGUMENT_DATA_BLOCK_DIMENSIONS,
#endif /* ARGUMENT_NO_DATA */
#endif /* ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS */

#ifdef ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS
#ifdef ARGUMENT_NO_DATA
              0,
#else /* #ifdef ARGUMENT_NO_DATA */
              ARGUMENT_DATA_PADDING_DIMENSION,
#endif /* ARGUMENT_NO_DATA */
#endif /* ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS */

#ifdef ARGUMENT_DEPENDENCY_SYMBOLS
#ifdef ARGUMENT_NO_DATA
              0,
#else /* #ifdef ARGUMENT_NO_DATA */
              ARGUMENT_SETUP_DEPENDENCIES,
#endif /* ARGUMENT_NO_DATA */
#endif /* ARGUMENT_DEPENDENCY_SYMBOLS */

#ifdef ARGUMENT_DEPENDENCY_SYMBOL_SIZES
#ifdef ARGUMENT_NO_DATA
              0,
#else /* #ifdef ARGUMENT_NO_DATA */
              sizeof(ARGUMENT_SETUP_DEPENDENCIES) / sizeof(pI_char* ),
#endif /* ARGUMENT_NO_DATA */
#endif /* ARGUMENT_DEPENDENCY_SYMBOL_SIZES */

#undef ARGUMENT_NO_SETUP
#undef ARGUMENT_NO_DATA

#undef ARGUMENT_DATA_SYMBOL_TYPES
#undef ARGUMENT_DATA_SYMBOLS
#undef ARGUMENT_SETUP_PTR
#undef ARGUMENT_DATA_PADDING_DIMENSION
#undef ARGUMENT_DATA_BLOCK_DIMENSIONS
#undef ARGUMENT_DATA_DIMENSIONS
#undef ARGUMENT_DATA_PTR
#undef ARGUMENT_DATA
#undef _ARGUMENT_DATA
#undef ARGUMENT_SETUP_DEPENDENCIES
#undef ARGUMENT_SETUP_SYMBOL_TYPES
#undef ARGUMENT_SETUP_SYMBOLS
#undef ARGUMENT_SETUP
#undef _ARGUMENT_SETUP
#undef ARGUMENT_ENUM__
#undef ARGUMENT_ENUM_
#undef ARGUMENT_ENUM
#undef QUOTEME
#undef QUOTEME_
#undef DATA_PTR_DECL
#undef DATA_PTR_DECL_
#undef S4
#undef S4_
#undef S3
#undef S3_
#undef S2
#undef S2_

#undef ARGUMENT_PREFIX_ENUM
#undef ARGUMENT_PREFIX_STRUCT
#undef ARGUMENT_PREFIX

#undef ARGUMENT_INCLUDE
#undef ARGUMENT_NAME
