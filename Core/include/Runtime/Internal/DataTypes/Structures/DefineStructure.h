/* DefineStructure.h
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

/* ******************************************************************
    First block enforces X-macro arguments.
   ****************************************************************** */

#ifndef STRUCTURE_NAME
#error "STRUCTURE_NAME must be set before including DefineStructure.h"
#endif /* STRUCTURE_NAME */

#ifndef STRUCTURE_INCLUDE
#error "STRUCTURE_INCLUDE must be set before including DefineStructure.h"
#endif /* STRUCTURE_INCLUDE */


/* ******************************************************************
    Next block validates include preconditions.
   ****************************************************************** */

#ifndef STRUCTURE_DECLARE
#ifndef STRUCTURE_CREATE_ENUM
#ifndef STRUCTURE_CREATE_NAME
#ifndef STRUCTURE_CREATE_SIZES
#ifndef STRUCTURE_CREATE_SYMBOLS
#ifndef STRUCTURE_CREATE_SYMBOL_SIZES
#ifndef STRUCTURE_CREATE_SYMBOL_TYPES
#error "Valid defines must be set before including DefineStructure.h"
#endif /* STRUCTURE_CREATE_SYMBOL_TYPES */
#endif /* #ifndef STRUCTURE_CREATE_SYMBOL_SIZES */
#endif /* #ifndef STRUCTURE_CREATE_SYMBOLS */
#endif /* #ifndef STRUCTURE_CREATE_SIZES */
#endif /* #ifndef STRUCTURE_CREATE_NAME */
#endif /* #ifndef STRUCTURE_CREATE_ENUM */
#endif /* #ifndef STRUCTURE_DECLARE */

#ifdef STRUCTURE_DECLARE
#ifdef STRUCTURE_CREATE_ENUM
#error "Valid defines must be set before including DefineStructure.h"
#endif /* #ifdef STRUCTURE_CREATE_ENUM */
#ifdef STRUCTURE_CREATE_NAME
#error "Valid defines must be set before including DefineStructure.h"
#endif /* #ifdef STRUCTURE_CREATE_NAME */
#ifdef STRUCTURE_CREATE_SIZES
#error "Valid defines must be set before including DefineStructure.h"
#endif /* #ifdef STRUCTURE_CREATE_SIZES */
#ifdef STRUCTURE_CREATE_SYMBOLS
#error "Valid defines must be set before including DefineStructure.h"
#endif /* #ifdef STRUCTURE_CREATE_SYMBOLS */
#ifdef STRUCTURE_CREATE_SYMBOL_SIZES
#error "Valid defines must be set before including DefineStructure.h"
#endif /* #ifdef STRUCTURE_CREATE_SYMBOL_SIZES */
#ifdef STRUCTURE_CREATE_SYMBOL_TYPES
#error "Valid defines must be set before including DefineStructure.h"
#endif /* #ifdef STRUCTURE_CREATE_SYMBOL_TYPES */
#endif /* #ifdef STRUCTURE_DECLARE */

#ifdef STRUCTURE_CREATE_ENUM
#ifdef STRUCTURE_CREATE_NAME
#error "Valid defines must be set before including DefineStructure.h"
#endif /* #indef STRUCTURE_CREATE_NAME */
#ifdef STRUCTURE_CREATE_SIZES
#error "Valid defines must be set before including DefineStructure.h"
#endif /* #indef STRUCTURE_CREATE_SIZES */
#ifdef STRUCTURE_CREATE_SYMBOLS
#error "Valid defines must be set before including DefineStructure.h"
#endif /* #indef STRUCTURE_CREATE_SYMBOLS */
#ifdef STRUCTURE_CREATE_SYMBOL_SIZES
#error "Valid defines must be set before including DefineStructure.h"
#endif /* #indef STRUCTURE_CREATE_SYMBOL_SIZES */
#ifdef STRUCTURE_CREATE_SYMBOL_TYPES
#error "Valid defines must be set before including DefineStructure.h"
#endif /* #ifdef STRUCTURE_CREATE_SYMBOL_TYPES */
#endif /* #ifdef STRUCTURE_CREATE_ENUM */

#ifdef STRUCTURE_CREATE_NAME
#ifdef STRUCTURE_CREATE_SIZES
#error "Valid defines must be set before including DefineStructure.h"
#endif /* #indef STRUCTURE_CREATE_SIZES */
#ifdef STRUCTURE_CREATE_SYMBOLS
#error "Valid defines must be set before including DefineStructure.h"
#endif /* #indef STRUCTURE_CREATE_SYMBOLS */
#ifdef STRUCTURE_CREATE_SYMBOL_SIZES
#error "Valid defines must be set before including DefineStructure.h"
#endif /* #indef STRUCTURE_CREATE_SYMBOL_SIZES */
#ifdef STRUCTURE_CREATE_SYMBOL_TYPES
#error "Valid defines must be set before including DefineStructure.h"
#endif /* #ifdef STRUCTURE_CREATE_SYMBOL_TYPES */
#endif /* #ifdef STRUCTURE_CREATE_NAME */

#ifdef STRUCTURE_CREATE_SIZES
#ifdef STRUCTURE_CREATE_SYMBOLS
#error "Valid defines must be set before including DefineStructure.h"
#endif /* #indef STRUCTURE_CREATE_SYMBOLS */
#ifdef STRUCTURE_CREATE_SYMBOL_SIZES
#error "Valid defines must be set before including DefineStructure.h"
#endif /* #indef STRUCTURE_CREATE_SYMBOL_SIZES */
#ifdef STRUCTURE_CREATE_SYMBOL_TYPES
#error "Valid defines must be set before including DefineStructure.h"
#endif /* #ifdef STRUCTURE_CREATE_SYMBOL_TYPES */
#endif /* #ifdef STRUCTURE_CREATE_SIZES */

#ifdef STRUCTURE_CREATE_SYMBOLS
#ifdef STRUCTURE_CREATE_SYMBOL_SIZES
#error "Valid defines must be set before including DefineStructure.h"
#endif /* #ifdef STRUCTURE_CREATE_SYMBOL_SIZES */
#ifdef STRUCTURE_CREATE_SYMBOL_TYPES
#error "Valid defines must be set before including DefineStructure.h"
#endif /* #ifdef STRUCTURE_CREATE_SYMBOL_TYPES */
#endif /* #ifdef STRUCTURE_CREATE_SYMBOLS */

#ifdef STRUCTURE_CREATE_SYMBOL_SIZES
#ifdef STRUCTURE_CREATE_SYMBOL_TYPES
#error "Valid defines must be set before including DefineStructure.h"
#endif /* #ifdef STRUCTURE_CREATE_SYMBOL_TYPES */
#endif /* #ifdef STRUCTURE_CREATE_SYMBOL_SIZES */


/* ******************************************************************
    Next block creates the names for the structures.
   ****************************************************************** */

#define STRUCTURE_PREFIX            pI_Structure_
#define STRUCTURE_PREFIX_STRUCT     _pI_Structure_
#define STRUCTURE_PREFIX_ENUM       T_pI_Structure_

#define S2_(x, y)                   x##y
#define S2(x, y)                    S2_(x, y)
#define S3_(x, y, z)                x##y##z
#define S3(x, y, z)                 S3_(x, y, z)
#define S4_(w, x, y, z)             w##x##y##z
#define S4(w, x, y, z)              S4_(w, x, y, z)

#define QUOTEME_(x)                 #x
#define QUOTEME(x)                  QUOTEME_(x)
#define STRUCTURE_ENUM__(x, y)      x##y
#define STRUCTURE_ENUM_(x, y)       STRUCTURE_ENUM__(x, y)
#define STRUCTURE_ENUM(x)           STRUCTURE_ENUM_(STRUCTURE_PREFIX_ENUM, x)

#define _STRUCTURE                  S2(STRUCTURE_PREFIX_STRUCT, STRUCTURE_NAME)
#define STRUCTURE                   S2(STRUCTURE_PREFIX, STRUCTURE_NAME)
#define STRUCTURE_SYMBOLS           S3(STRUCTURE_PREFIX, STRUCTURE_NAME, _Symbols)

#define STRUCTURE_SYMBOL_TYPES      S3(STRUCTURE_PREFIX, STRUCTURE_NAME, _Symbol_Types)


/* ******************************************************************
    Next block creates the structures.
   ****************************************************************** */

#ifdef STRUCTURE_DECLARE

#define FIELD(a, b) a b;
/* create structure struct */
typedef struct _STRUCTURE {
#include STRUCTURE_INCLUDE
} STRUCTURE;
#undef FIELD

#ifdef PI_BUILD_RUNTIME
/* create structure string symbols */
pI_char* STRUCTURE_SYMBOLS[] = {
#define FIELD(a, b) #b,
#include STRUCTURE_INCLUDE
#undef FIELD
};
#else /* #ifdef PI_BUILD_RUNTIME */
extern pI_char* STRUCTURE_SYMBOLS[];
#endif /* #ifdef PI_BUILD_RUNTIME */

#ifdef PI_BUILD_RUNTIME
/* create structure string symbols */
pI_char* STRUCTURE_SYMBOL_TYPES[] = {
#define FIELD(a, b) #a,
#include STRUCTURE_INCLUDE
#undef FIELD
};
#else /* #ifdef PI_BUILD_RUNTIME */
extern pI_char* STRUCTURE_SYMBOL_TYPES[];
#endif /* #ifdef PI_BUILD_RUNTIME */

#endif /* STRUCTURE_DECLARE */


/* ******************************************************************
    Next block creates global structure information accessors.
   ****************************************************************** */

#ifdef STRUCTURE_CREATE_ENUM
STRUCTURE_ENUM (STRUCTURE_NAME),
#endif /* STRUCTURE_CREATE_ENUM */

#ifdef STRUCTURE_CREATE_NAME
               QUOTEME (S2 (STRUCTURE_PREFIX, STRUCTURE_NAME)),
#endif /* STRUCTURE_CREATE_NAME */

#ifdef STRUCTURE_CREATE_SIZES
               sizeof(struct _STRUCTURE),
#endif /* STRUCTURE_CREATE_SIZES */

#ifdef STRUCTURE_CREATE_SYMBOLS
               STRUCTURE_SYMBOLS,
#endif /* STRUCTURE_CREATE_SYMBOLS */

#ifdef STRUCTURE_CREATE_SYMBOL_SIZES
               sizeof(STRUCTURE_SYMBOLS) / sizeof(pI_char* ),
#endif /* STRUCTURE_CREATE_SYMBOL_SIZES */

#ifdef STRUCTURE_CREATE_SYMBOL_TYPES
               STRUCTURE_SYMBOL_TYPES,
#endif /* STRUCTURE_CREATE_SYMBOL_TYPES */

               /* ******************************************************************
                   Final block cleans up.
                  ****************************************************************** */

#undef STRUCTURE_SYMBOL_TYPES
#undef STRUCTURE_SYMBOLS
#undef STRUCTURE
#undef _STRUCTURE
#undef STRUCTURE_ENUM
#undef STRUCTURE_ENUM_
#undef STRUCTURE_ENUM__
#undef QUOTEME
#undef QUOTEME_
#undef S4
#undef S4_
#undef S3
#undef S3_
#undef S2
#undef S2_
#undef STRUCTURE_PREFIX_ENUM
#undef STRUCTURE_PREFIX_STRUCT
#undef STRUCTURE_PREFIX
#undef STRUCTURE_INCLUDE
#undef STRUCTURE_NAME
