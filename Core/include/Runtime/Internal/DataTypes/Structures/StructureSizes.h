/* StructureSizes.h
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_RUNTIME_INTERNAL_DATATYPES_STRUCTURES_STRUCTURESIZES_H
#define PUREIMAGE_RUNTIME_INTERNAL_DATATYPES_STRUCTURES_STRUCTURESIZES_H

#ifdef PI_BUILD_RUNTIME

pI_int pI_StructureSizes[] = {
#define STRUCTURE_CREATE_SIZES
#include <DataTypes/Structures/StructureList.h>
#undef STRUCTURE_CREATE_SIZES
}; /* pI_int pI_StructureSizes[] */

#else /* #ifdef PI_BUILD_RUNTIME */

extern pI_int pI_StructureSizes[];

#endif /* #ifdef PI_BUILD_RUNTIME */

#endif /* #ifndef PUREIMAGE_RUNTIME_INTERNAL_DATATYPES_STRUCTURES_STRUCTURESIZES_H */
