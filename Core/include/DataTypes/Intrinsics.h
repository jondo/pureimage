/* Intrinsics.h
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_DATATYPES_INTRINSICS_H
#define PUREIMAGE_DATATYPES_INTRINSICS_H

#ifdef __cplusplus
extern "C" {
#endif

/* set new alignment */
#ifdef __GNUC__
#define DO_PRAGMA_EXPAND(x) _Pragma (#x)
#define DO_PRAGMA(x) DO_PRAGMA_EXPAND(x)
DO_PRAGMA (pack (push,PI_STRUCTURE_ALIGNMENT))
#else
#pragma pack(push)                          /* push current alignment to stack */
#pragma pack(PI_STRUCTURE_ALIGNMENT)
#endif

#if defined _MSC_VER && _MSC_VER <= 1500 /* Visual C++ 2008 (9.0) */
#include <Runtime/Internal/DataTypes/msinttypes/stdint.h> /* msvc wrapped C99 integral types */
#else
#include <stdint.h> /* C99 integer types */
#endif /* _MSC_VER */

    /* true and false constants*/
#define pI_FALSE        0
#define pI_TRUE         1
    /* error constant */
#define pI_ERROR        -1

	typedef char pI_char_type;
    typedef pI_char_type* pI_string_type;

    /* declare data type enumeration */
#define TYPE(a, b) _##b,
    typedef enum _pI_IntrinsicType {
#include <DataTypes/Intrinsics/Intrinsics.def>
    } pI_IntrinsicType;
#undef TYPE

    /* data type definitions */
#define TYPE(a, b) typedef a b;
#include <DataTypes/Intrinsics/Intrinsics.def>
#undef TYPE

#ifdef PI_BUILD_RUNTIME

    /* intrinsic type names */
#define TYPE(a, b) #b,
    pI_char* pI_IntrinsicNames[] = {
#include <DataTypes/Intrinsics/Intrinsics.def>
    }; /* pI_char* pI_IntrinsicNames[] */
#undef TYPE

#else /* #ifdef PI_BUILD_RUNTIME */

    /* intrinsic type names */
    extern pI_char* pI_IntrinsicNames[];

#endif /* #ifdef PI_BUILD_RUNTIME */

#pragma pack(pop)                           /* restore original alignment from stack */

#ifdef __cplusplus
}
#endif

#endif /* PUREIMAGE_DATATYPES_INTRINSICS_H */
