/* ByteImageArgument.def
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */ 

/** The following argument specifies an image type.
	It contains a "height", a "width" and a "channels" field 
	in the setup part, which specify the image layout.
	The data part of the argument declaration consists of three fields:
	 - the "pitch", which is supposed to hold the number of bytes 
		which form one padding stripe.
	 - the "path", which is supposed to hold the path to the image if 
		loaded or imported from hard-disk.
	 - the "data", which specifies a three-dimensional data field, where 
		the dimensions are dependent on "height", "width" and "channels" 
		of the setup part. The data field must be allocated as whole block,
		the padding dimension is set to two.

	The memory layout of this image type is similar to the FreeImage bitmap.
	Using the following definition, it is possible to represent arbitrarily 
	sized images with any number of channels. */
 
SETUP(pI_size, height)
SETUP(pI_size, width)
SETUP(pI_size, channels)

DATA(pI_size, pitch)
DATA(pI_str, path)
DATA_3D_BLOCK(pI_byte, data, height, width, channels, 3, 2)
