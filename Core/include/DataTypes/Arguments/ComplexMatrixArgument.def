/* ComplexMatrixArgument.def
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

/** The following argument specifies a complex value matrix.
	It contains a "rows" and a "cols" field in the setup part, which
	determine the size of the matrix, as well as a pointer to a
	two-dimensional complex value field, "data" in the data part, which
	is dependent on the "rows" and "cols". The complex value matrix is
	allocated as a continuous 2-dimensional memory block, the padding is
	done on the "cols" dimension.
	Note: in this case the padding is unlikely to have any effect because
		  of the size of the complex type. */

SETUP(pI_size, rows)
SETUP(pI_size, cols)

DATA_2D_BLOCK(pI_Structure_ComplexNumber, data, rows, cols, 2, 1)
