/* ArgumentList.h
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

/* single value arguments */
#define ARGUMENT_NAME Empty
#define ARGUMENT_INCLUDE <DataTypes/Arguments/EmptyArgument.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>

#define ARGUMENT_NAME BoolValue
#define ARGUMENT_INCLUDE <DataTypes/Arguments/BoolValueArgument.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>

#define ARGUMENT_NAME DoubleValue
#define ARGUMENT_INCLUDE <DataTypes/Arguments/DoubleValueArgument.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>

#define ARGUMENT_NAME IntValue
#define ARGUMENT_INCLUDE <DataTypes/Arguments/IntValueArgument.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>

#define ARGUMENT_NAME ShortValue
#define ARGUMENT_INCLUDE <DataTypes/Arguments/ShortValueArgument.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>

#define ARGUMENT_NAME StringValue
#define ARGUMENT_INCLUDE <DataTypes/Arguments/StringValueArgument.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>

/* array arguments */
#define ARGUMENT_NAME BoolArray
#define ARGUMENT_INCLUDE <DataTypes/Arguments/BoolArrayArgument.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>

#define ARGUMENT_NAME ByteArray
#define ARGUMENT_INCLUDE <DataTypes/Arguments/ByteArrayArgument.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>

#define ARGUMENT_NAME ComplexArray
#define ARGUMENT_INCLUDE <DataTypes/Arguments/ComplexArrayArgument.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>

#define ARGUMENT_NAME DoubleArray
#define ARGUMENT_INCLUDE <DataTypes/Arguments/DoubleArrayArgument.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>

#define ARGUMENT_NAME IntArray
#define ARGUMENT_INCLUDE <DataTypes/Arguments/IntArrayArgument.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>

#define ARGUMENT_NAME ShortArray
#define ARGUMENT_INCLUDE <DataTypes/Arguments/ShortArrayArgument.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>

#define ARGUMENT_NAME StringArray
#define ARGUMENT_INCLUDE <DataTypes/Arguments/StringArrayArgument.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>

/* matrix arguments */
#define ARGUMENT_NAME ByteMatrix
#define ARGUMENT_INCLUDE <DataTypes/Arguments/ByteMatrixArgument.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>

#define ARGUMENT_NAME ComplexMatrix
#define ARGUMENT_INCLUDE <DataTypes/Arguments/ComplexMatrixArgument.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>

#define ARGUMENT_NAME DoubleMatrix
#define ARGUMENT_INCLUDE <DataTypes/Arguments/DoubleMatrixArgument.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>

#define ARGUMENT_NAME IntMatrix
#define ARGUMENT_INCLUDE <DataTypes/Arguments/IntMatrixArgument.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>

#define ARGUMENT_NAME ShortMatrix
#define ARGUMENT_INCLUDE <DataTypes/Arguments/ShortMatrixArgument.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>

/* misc arguments */
#define ARGUMENT_NAME Range
#define ARGUMENT_INCLUDE <DataTypes/Arguments/RangeArgument.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>

#define ARGUMENT_NAME ByteImage
#define ARGUMENT_INCLUDE <DataTypes/Arguments/ByteImageArgument.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>

#define ARGUMENT_NAME StringSelection
#define ARGUMENT_INCLUDE <DataTypes/Arguments/StringSelectionArgument.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>

#define ARGUMENT_NAME IntPolygon
#define ARGUMENT_INCLUDE <DataTypes/Arguments/IntPolygonArgument.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>

#define ARGUMENT_NAME DoubleTable
#define ARGUMENT_INCLUDE <DataTypes/Arguments/DoubleTableArgument.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>

#define ARGUMENT_NAME RGBPalette
#define ARGUMENT_INCLUDE <DataTypes/Arguments/RGBPaletteArgument.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>

#define ARGUMENT_NAME XYZArray
#define ARGUMENT_INCLUDE <DataTypes/Arguments/XYZArrayArgument.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>

/* Note: do not insert new arguments after this line! */
#define ARGUMENT_NAME VariableArgument
#define ARGUMENT_INCLUDE <DataTypes/Arguments/VariableArgument.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>

#define ARGUMENT_NAME VariableArgumentList
#define ARGUMENT_INCLUDE <DataTypes/Arguments/VariableArgumentList.def>
#include <Runtime/Internal/DataTypes/Arguments/DefineArgument.h>
