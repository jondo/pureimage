/* DataTypes.h
 *
 * Copyright 2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_DATATYPES_H
#define PUREIMAGE_DATATYPES_H

#ifdef __cplusplus
extern "C" {
#endif

#define pI_FALSE        0
#define pI_TRUE         1
    /* error constant */
#define pI_ERROR        -1

#ifdef PI_BUILD_RUNTIME
#include <DataTypes/DataTypesDefine.h>
#else
#include <DataTypes/DataTypesDeclare.h>
#endif

#ifdef __cplusplus
} /* extern "C" { */
#endif

#endif /* PUREIMAGE_DATATYPES_H */
