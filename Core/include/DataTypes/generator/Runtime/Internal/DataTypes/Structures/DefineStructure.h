/* DefineStructure.h
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

/* ******************************************************************
    First block enforces X-macro arguments.
   ****************************************************************** */

#ifndef STRUCTURE_NAME
#error "STRUCTURE_NAME must be set before including DefineStructure.h"
#endif /* STRUCTURE_NAME */

#ifndef STRUCTURE_INCLUDE
#error "STRUCTURE_INCLUDE must be set before including DefineStructure.h"
#endif /* STRUCTURE_INCLUDE */

#define STRUCTURE_PREFIX        pI_Structure_
#define C_STRUCTURE_(a,b) a ## b
#define C_STRUCTURE(a,b) C_STRUCTURE_(a,b)

{
"name":
    xstr(STRUCTURE_NAME),
"c_type":
    xstr(C_STRUCTURE(STRUCTURE_PREFIX, STRUCTURE_NAME)),
#define FIELD(TYPE, NAME) {"type": xstr(TYPE), "name": xstr(NAME)},
"fields":
    [
#include STRUCTURE_INCLUDE
    ]
#undef FIELD
},

#undef STRUCTURE_PTR
#undef STRUCTURE_PTR_
#undef STRUCTURE_PREFIX_STRUCT

#undef STRUCTURE_INCLUDE
#undef STRUCTURE_NAME
