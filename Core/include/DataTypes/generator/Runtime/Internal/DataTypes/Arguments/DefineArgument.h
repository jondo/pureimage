/* DefineArgument.h
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

/* ******************************************************************
    First block enforces X-macro arguments.
   ****************************************************************** */

#ifndef ARGUMENT_NAME
#error "ARGUMENT_NAME must be set before including DefineArgument.h"
#endif /* ARGUMENT_NAME */

#ifndef ARGUMENT_INCLUDE
#error "ARGUMENT_INCLUDE must be set before including DefineArgument.h"
#endif /* ARGUMENT_INCLUDE */


#define ARGUMENT_PREFIX     pI_Argument_
#define C_ARGUMENT_(a,b) a ## b
#define C_ARGUMENT(a,b) C_ARGUMENT_(a,b)
{
"name":
    xstr(ARGUMENT_NAME),
"c_type":
    xstr(C_ARGUMENT(ARGUMENT_PREFIX, ARGUMENT_NAME)),
"def":
    xstr(ARGUMENT_INCLUDE),
#define SETUP(TYPE, NAME) {"type": xstr(TYPE), "name": xstr(NAME)},
#define DATA(a, b)
#define DATA_1D(x, a, b)
#define DATA_2D(x, a, b, c)
#define DATA_3D(x, a, b, c, d)
#define DATA_4D(x, a, b, c, d, e)
#define DATA_5D(x, a, b, c, d, e, f)
#define DATA_2D_BLOCK(x, a, b, c, n, p)
#define DATA_3D_BLOCK(x, a, b, c, d, n, p)
#define DATA_4D_BLOCK(x, a, b, c, d, e, n, p)
#define DATA_5D_BLOCK(x, a, b, c, d, e, f, n, p)
"setup":    [
#include ARGUMENT_INCLUDE
    ],
#undef DATA_5D_BLOCK
#undef DATA_4D_BLOCK
#undef DATA_3D_BLOCK
#undef DATA_2D_BLOCK
#undef DATA_5D
#undef DATA_4D
#undef DATA_3D
#undef DATA_2D
#undef DATA_1D
#undef DATA
#undef SETUP

#define SETUP(TYPE, NAME)
#define DATA(a, b) {"type": xstr(a), "name": xstr(b), "dim": 0, "layout": "simple", "padding": 0},
#define DATA_1D(x, a, b) {"type": xstr(x), "name": xstr(a), "dim": 1, "sizes": [xstr(b)], "layout": "simple", "padding": 1},
#define DATA_2D(x, a, b, c) {"type": xstr(x), "name": xstr(a), "dim": 2, "sizes": [xstr(b), xstr(c)], "layout": "simple", "padding": 1},
#define DATA_3D(x, a, b, c, d) {"type": xstr(x), "name": xstr(a), "dim": 3, "sizes": [xstr(b), xstr(c), xstr(d)], "layout": "simple", "padding": 1},
#define DATA_4D(x, a, b, c, d, e) {"type": xstr(x), "name": xstr(a), "dim": 4, "sizes": [xstr(b), xstr(c), xstr(d), xstr(e)], "layout": "simple", "padding": 1},
#define DATA_5D(x, a, b, c, d, e, f) {"type": xstr(x), "name": xstr(a), "dim": 5, "sizes": [xstr(b), xstr(c), xstr(d), xstr(e), xstr(f)], "layout": "simple", "padding": 1},
#define DATA_2D_BLOCK(x, a, b, c, n, p) {"type": xstr(x), "name": xstr(a), "dim": 2, "sizes": [xstr(b), xstr(c)], "layout": "block", "padding": p, "block_depth": n},
#define DATA_3D_BLOCK(x, a, b, c, d, n, p){"type": xstr(x), "name": xstr(a), "dim": 3, "sizes": [xstr(b), xstr(c), xstr(d)], "layout": "block", "padding": p, "block_depth": n},
#define DATA_4D_BLOCK(x, a, b, c, d, e, n, p){"type": xstr(x), "name": xstr(a), "dim": 4, "sizes": [xstr(b), xstr(c), xstr(d), xstr(e)], "layout": "block", "padding": p, "block_depth": n},
#define DATA_5D_BLOCK(x, a, b, c, d, e, f, n, p) {"type": xstr(x), "name": xstr(a), "dim": 5, "sizes": [xstr(b), xstr(c), xstr(d), xstr(e), xstr(f)], "layout": "block", "padding": p, "block_depth": n},
"data": [
#include ARGUMENT_INCLUDE
    ],  
#undef DATA_5D_BLOCK
#undef DATA_4D_BLOCK
#undef DATA_3D_BLOCK
#undef DATA_2D_BLOCK
#undef DATA_5D
#undef DATA_4D
#undef DATA_3D
#undef DATA_2D
#undef DATA_1D
#undef DATA
#undef SETUP
},

#undef STRUCTURE_PTR
#undef STRUCTURE_PTR_
#undef STRUCTURE_PREFIX_STRUCT

#undef STRUCTURE_INCLUDE
#undef STRUCTURE_NAME

#undef ARGUMENT_INCLUDE
#undef ARGUMENT_NAME
