/* Structures.h
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_DATATYPES_STRUCTURES_H
#define PUREIMAGE_DATATYPES_STRUCTURES_H

#ifdef __cplusplus
extern "C" {
#endif

/* set new alignment */
#ifdef __GNUC__
#define DO_PRAGMA_EXPAND(x) _Pragma (#x)
#define DO_PRAGMA(x) DO_PRAGMA_EXPAND(x)
DO_PRAGMA (pack (push,PI_STRUCTURE_ALIGNMENT))
#else
#pragma pack(push)                          /* push current alignment to stack */
#pragma pack(PI_STRUCTURE_ALIGNMENT)
#endif

#include <DataTypes/Intrinsics.h>

    /* fwd. declaration for arguments in structures */
    struct _Argument;

    /*  In this header file, any structure registered to pureImage gets
        expanded using X-macros. */

    /* the inclusion of Structures.h will create the following data structures:

    -- for each argument:

        + a name-dependent structure, containing the desired fields.
        + name dependent structure layout information.
        + name dependent array for the names of the fields.
        + name dependent array containg the type of the fields.


    -- GLOBAL:

        enum _pI_StructureType:
            global structure id;
            allows the creation of a structure with given id.

        pI_char* pI_StructureNames[]:
            global names of the individual structures;
            allows the lookup of the structure name with given id.

        pI_int pI_StructureSizes[]:
            global sizes of the individual structures;
            allows the lookup of the structure size with given id.

        pI_char** pI_StructureSymbols[]:
            global names of the data fields within the individual structures;
            enables the parsing of structure members.

        pI_int pI_StructureSymbolSizes[]:
            lengths of the global names of the data fields within the individual structures;

        pI_int* pI_StructureTypes[];
            global data types of the fields within the individual structures;
            enables the runtime to perform type-dependent operations on the individual fields.

    */

#define STRUCTURE_DECLARE
#include <DataTypes/Structures/StructureList.h>
#undef STRUCTURE_DECLARE

    typedef enum _pI_StructureType {
#define STRUCTURE_CREATE_ENUM
#include <DataTypes/Structures/StructureList.h>
#undef STRUCTURE_CREATE_ENUM
    } pI_StructureType;

#ifdef PI_BUILD_RUNTIME

    pI_char* pI_StructureNames[] = {
#define STRUCTURE_CREATE_NAME
#include <DataTypes/Structures/StructureList.h>
#undef STRUCTURE_CREATE_NAME
    }; /* pI_char* pI_StructureNames[] */

    pI_char** pI_StructureSymbols[] = {
#define STRUCTURE_CREATE_SYMBOLS
#include <DataTypes/Structures/StructureList.h>
#undef STRUCTURE_CREATE_SYMBOLS
    }; /* pI_char** pI_StructureSymbols[] */

    pI_int pI_StructureSymbolSizes[] = {
#define STRUCTURE_CREATE_SYMBOL_SIZES
#include <DataTypes/Structures/StructureList.h>
#undef STRUCTURE_CREATE_SYMBOL_SIZES
    }; /* pI_int pI_StructureSymbolSizes[] */

    pI_char** pI_StructureSymbolTypes[] = {
#define STRUCTURE_CREATE_SYMBOL_TYPES
#include <DataTypes/Structures/StructureList.h>
#undef STRUCTURE_CREATE_SYMBOL_TYPES
    }; /* pI_char** pI_StructureSymbolTypes[] */

#else /* #ifdef PI_BUILD_RUNTIME */

    extern pI_char* pI_StructureNames[];
    extern pI_char** pI_StructureSymbols[];
    extern pI_int pI_StructureSymbolSizes[];
    extern pI_char** pI_StructureSymbolTypes[];

#endif /* #ifdef PI_BUILD_RUNTIME */

#pragma pack(pop)                           /* restore original alignment from stack */

#ifdef __cplusplus
}
#endif

#endif /* PUREIMAGE_DATATYPES_STRUCTURES_H */
