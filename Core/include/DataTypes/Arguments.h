/* Arguments.h
 *
 * Copyright 2010-2011 Johannes Kepler Universität Linz,
 * Institut für Wissensbasierte Mathematische Systeme.
 *
 * This file is part of pureImage.
 *
 * pureImage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * pureImage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pureImage. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUREIMAGE_DATATYPES_ARGUMENTS_H
#define PUREIMAGE_DATATYPES_ARGUMENTS_H

#ifdef __cplusplus
extern "C" {
#endif

/* set new alignment */
#ifdef __GNUC__
#define DO_PRAGMA_EXPAND(x) _Pragma (#x)
#define DO_PRAGMA(x) DO_PRAGMA_EXPAND(x)
DO_PRAGMA (pack (push,PI_STRUCTURE_ALIGNMENT))
#else
#pragma pack(push)                          /* push current alignment to stack */
#pragma pack(PI_STRUCTURE_ALIGNMENT)
#endif

#include <DataTypes/Intrinsics.h>
#include <DataTypes/Structures.h>

    /*  In this header file, any argument registered to pureImage gets
        expanded using X-macros. Also, the definition of any
        argument structure happens here. */


    /* the inclusion of Arguments.h will create the following data structures:

    -- for each structure:

        + a name-dependent argument setup structure
        + name dependent string list with setup name fields
        + name dependent string list with setup type fields

        + a name-dependent array of data-field dependencies on setup fields

        + a name-dependent argument data structure
        + a name-dependent field which contains data field dimensions
        + a name-dependent field which contains data block dimensions
        + a name-dependent field which contains data padding dimensions
        + name dependent string list with data name fields
        + name dependent string list with data type fields

    -- GLOBAL:

        enum _pI_ArgumentType:
            global argument id;
            allows the creation of an argument with given id.

        union _pI_ArgumentSetup:
            global union which contains pointers to type-dependent setup structures;
            can be accessed using the type of an arbitrary argument.

        union _pI_ArgumentData:
            global union which contains pointers to type-dependent data structures;
            can be accessed using the type of an arbitrary argument.

        pI_char* pI_ArgumentNames[]:
            global names of the individual arguments;
            allows the lookup of the argument name with given id.

        pI_char** pI_ArgumentSetupSymbols[]:
            global string array with setup name fields.

        pI_char** pI_ArgumentDataSymbols[]:
            global string array with data name fields.

        pI_char** pI_ArgumentSetupSymbolTypes[]:
            global string array with setup types.

        pI_char** pI_ArgumentDataSymbolTypes[]:
            global string array with data types.

        pI_int pI_ArgumentSetupSymbolSizes[]:
            global array which contains the number of setup symbols of an argument.

        pI_int pI_ArgumentDataSymbolSizes[]:
            global array which contains the number of data symbols of an argument.

        pI_char** pI_ArgumentDependencySymbols[]:
            global string array which contains the dependency symbols of a data field.

        pI_int pI_ArgumentDependencySymbolsizes[]:
            global string array which contains the number of dependency symbols of a data field.

        pI_int* pI_ArgumentDataDimensions[]:
            global array which contains the dimensions of a data field.

        pI_int* pI_ArgumentDataBlockDimensions[]:
            global array which contains the block dimensions of a data field.

        pI_int* pI_ArgumentDataPaddingDimensions[]:
            global array which contains the padding dimensions of a data field.

    */

#define ARGUMENT_DECLARE
#include <DataTypes/Arguments/ArgumentList.h>
#undef ARGUMENT_DECLARE

    typedef enum _pI_ArgumentType {
#define ARGUMENT_CREATE_ENUM
#include <DataTypes/Arguments/ArgumentList.h>
#undef ARGUMENT_CREATE_ENUM
    }
    pI_ArgumentType;

    typedef union _pI_ArgumentSetup {
#define ARGUMENT_CREATE_SETUP_UNION
#include <DataTypes/Arguments/ArgumentList.h>
#undef ARGUMENT_CREATE_SETUP_UNION
    } pI_ArgumentSetup;

    typedef union _pI_ArgumentData {
#define ARGUMENT_CREATE_DATA_UNION
#include <DataTypes/Arguments/ArgumentList.h>
#undef ARGUMENT_CREATE_DATA_UNION
    } pI_ArgumentData;

#ifdef PI_BUILD_RUNTIME

    pI_char* pI_ArgumentNames[] = {
#define ARGUMENT_CREATE_NAME
#include <DataTypes/Arguments/ArgumentList.h>
#undef ARGUMENT_CREATE_NAME
    }; /* pI_char* pI_ArgumentNames[] */

    pI_char** pI_ArgumentSetupSymbols[] = {
#define ARGUMENT_CREATE_SETUP_SYMBOLS
#include <DataTypes/Arguments/ArgumentList.h>
#undef ARGUMENT_CREATE_SETUP_SYMBOLS
    }; /* pI_char** pI_ArgumentSetupSymbols[] */

    pI_char** pI_ArgumentDataSymbols[] = {
#define ARGUMENT_CREATE_DATA_SYMBOLS
#include <DataTypes/Arguments/ArgumentList.h>
#undef ARGUMENT_CREATE_DATA_SYMBOLS
    }; /* pI_char** pI_ArgumentDataSymbols[] */

    pI_char** pI_ArgumentSetupSymbolTypes[] = {
#define ARGUMENT_CREATE_SETUP_SYMBOL_TYPES
#include <DataTypes/Arguments/ArgumentList.h>
#undef ARGUMENT_CREATE_SETUP_SYMBOL_TYPES
    }; /* pI_char** pI_ArgumentSetupSymbolTypes[] */

    pI_char** pI_ArgumentDataSymbolTypes[] = {
#define ARGUMENT_CREATE_DATA_SYMBOL_TYPES
#include <DataTypes/Arguments/ArgumentList.h>
#undef ARGUMENT_CREATE_DATA_SYMBOL_TYPES
    }; /* pI_char** pI_ArgumentDataSymbolTypes[] */

    pI_int pI_ArgumentSetupSymbolSizes[] = {
#define ARGUMENT_CREATE_SETUP_SYMBOL_SIZES
#include <DataTypes/Arguments/ArgumentList.h>
#undef ARGUMENT_CREATE_SETUP_SYMBOL_SIZES
    }; /* pI_int pI_ArgumentSetupSymbolSizes[] */

    pI_int pI_ArgumentDataSymbolSizes[] = {
#define ARGUMENT_CREATE_DATA_SYMBOL_SIZES
#include <DataTypes/Arguments/ArgumentList.h>
#undef ARGUMENT_CREATE_DATA_SYMBOL_SIZES
    }; /* pI_int pI_ArgumentDataSymbolSizes[] */

    pI_char** pI_ArgumentDependencySymbols[] = {
#define ARGUMENT_DEPENDENCY_SYMBOLS
#include <DataTypes/Arguments/ArgumentList.h>
#undef ARGUMENT_DEPENDENCY_SYMBOLS
    }; /* pI_char** pI_ArgumentDependencySymbols[] */

    pI_int pI_ArgumentDependencySymbolsizes[] = {
#define ARGUMENT_DEPENDENCY_SYMBOL_SIZES
#include <DataTypes/Arguments/ArgumentList.h>
#undef ARGUMENT_DEPENDENCY_SYMBOL_SIZES
    }; /* pI_int pI_ArgumentDependencySymbolsizes[] */

    pI_int* pI_ArgumentDataDimensions[] = {
#define ARGUMENT_CREATE_DATA_DIMENSIONS
#include <DataTypes/Arguments/ArgumentList.h>
#undef ARGUMENT_CREATE_DATA_DIMENSIONS
    }; /* pI_int pI_ArgumentDataDimensions[] */

    pI_int* pI_ArgumentDataBlockDimensions[] = {
#define ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS
#include <DataTypes/Arguments/ArgumentList.h>
#undef ARGUMENT_CREATE_DATA_BLOCK_DIMENSIONS
    }; /* pI_int pI_ArgumentDataBlockDimensions[] */

    pI_int* pI_ArgumentDataPaddingDimensions[] = {
#define ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS
#include <DataTypes/Arguments/ArgumentList.h>
#undef ARGUMENT_CREATE_DATA_PADDING_DIMENSIONS
    }; /* pI_int pI_ArgumentDataPaddingDimensions[] */

#else /* #ifdef PI_BUILD_RUNTIME */

    extern pI_char*     pI_ArgumentNames[];
    extern pI_char**    pI_ArgumentSetupSymbols[];
    extern pI_char**    pI_ArgumentDataSymbols[];
    extern pI_char**    pI_ArgumentSetupSymbolTypes[];
    extern pI_char**    pI_ArgumentDataSymbolTypes[];
    extern pI_int       pI_ArgumentSetupSymbolSizes[];
    extern pI_int       pI_ArgumentDataSymbolSizes[];
    extern pI_char**    pI_ArgumentDependencySymbols[];
    extern pI_int       pI_ArgumentDependencySymbolsizes[];
    extern pI_int*      pI_ArgumentDataDimensions[];
    extern pI_int*      pI_ArgumentDataBlockDimensions[];
    extern pI_int*      pI_ArgumentDataPaddingDimensions[];

#endif /* #ifdef PI_BUILD_RUNTIME */

    /** Argument description structure. */
    typedef struct _ArgumentDescription {

        /** Human-readable name of the argument. */
        pI_str name;
        /** Human-readable description of the argument. */
        pI_str description;
    } ArgumentDescription;

    /** Argument signature structure. */
    typedef struct _ArgumentSignature {

        /** Type of the argument. */
        enum _pI_ArgumentType type;
        /** read only flag */
        pI_bool readonly;
        /** Argument Setup description. */
        union _pI_ArgumentSetup setup;
        /** constraints */
        pI_str constraints;
    } ArgumentSignature;

    /** Argument struct definition.
        Argument is supposed to be the only datatype to be
        exchanged between different image plugins. It is used semantically for
        parameters (given to Initialize method) and arguments (given
        to Execute). It is implemented as low-level C POD type, therefore
        no user-defined constructor and  no method implementation is provided.
        The use of function pointers allows the exchange of implementation
        detail without the need for recompilation of any client. */
    typedef struct _Argument {

        /** Argument signature */
        struct _ArgumentSignature signature;
        /** Argument data. */
        union _pI_ArgumentData data;
        /** Argument description */
        struct _ArgumentDescription description;
        /** Pointer to the runtime which allocated the argument.
            This allows one-side data allocation and deletion. Additionaly,
            on C++ side, the runtime pointer can be used for automatic resource
            deletion using a smart pointer. */
        struct _CRuntime* runtime;

    } Argument;

    /** Setup to hold multiple arguments. */
    typedef struct _ArgumentList {
        /** The number of arguments managed. */
        pI_size count;
        /** The arguments themselves as pointer-list. */
        struct _Argument** list;
    } ArgumentList;

#pragma pack(pop)                           /* restore original alignment from stack */

#ifdef __cplusplus
}
#endif

#endif /* PUREIMAGE_DATATYPES_ARGUMENTS_H */
