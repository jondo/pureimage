macro(ADD_PANDOC_COMMAND FILENAME)

    file(TO_NATIVE_PATH ${CMAKE_BINARY_DIR}/doc/${FILENAME}.html ${FILENAME}_HTML)
    file(TO_NATIVE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/${FILENAME}.txt INPUT)

    set(_OUTPUT ${${FILENAME}_HTML})

    add_custom_command(
        OUTPUT ${_OUTPUT}
        COMMAND ${PANDOC_EXECUTABLE} -s -o ${_OUTPUT} ${INPUT}
        DEPENDS ${INPUT}
        COMMENT "${PANDOC_EXECUTABLE} -s -o ${_OUTPUT} ${INPUT}"
    )
endmacro()

ADD_PANDOC_COMMAND(ReadMe)
ADD_PANDOC_COMMAND(ReleaseNotes)

add_custom_target(ReadMe ALL DEPENDS ${ReadMe_HTML} ${ReleaseNotes_HTML})


# #############################################################################


configure_file(Doxyfile.in ${CMAKE_BINARY_DIR}/doc/Doxyfile)              # A reconfigure is needed after changing Doxyfile.in
configure_file(DoxyfileOnline.in ${CMAKE_BINARY_DIR}/doc/DoxyfileOnline)  # Alternative configuration for online documentation

add_custom_target(doxygen ALL
    COMMAND ${DOXYGEN_EXECUTABLE} ${CMAKE_BINARY_DIR}/doc/Doxyfile
)
