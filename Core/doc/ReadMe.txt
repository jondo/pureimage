﻿pureImage
=========

pureImage is a plug-in development framework that provides a common plug-in interface,
generic data structures, and a uniform workflow managed by a runtime environment.

pureImage is specialized in image processing under C++. However, it can be used in
different areas (due to its generic data types, such as vectors, matrices, etc.),
and in any environment which provides C bindings, such as Java, Matlab, or Python.

Please see the project page [https://pureimage.flll.jku.at/] for more information
about the software, to check for new releases, to post questions, to report bugs,
or to contact the authors.


Getting started
---------------

For developers new to pureImage we recommend the study of the tutorial, the
source code of plugins and the unit test code.

In order to make use of the pureImage library, the first step necessary is to
build pureImage as explained subsequently.


Using pureImage plugins
-----------------------

The usage of pureImage plugins should be straightforward. The only thing which
must be considered is the dependency of certain plugins on third party libraries.

For instance, in order to use the `OpenCV` pureImage plugin, it is necessary
that the application environment finds the dynamic OpenCV libraries. To ensure this
under Windows, one could add the corresponding library path to the global search path
(as the pureImage installer offers), or copy the dynamic libraries to the application folder.


Directory tree
--------------

`/Core` - the pureImage Core:

- the library runtime for various environments
- different source code generators
- documentation
- unit tests

`/Plugins` - Plugins and wrappers:

- `Plugins/CImg`:        wrapper around [CImg]
- `Plugins/FLLL`:        a collection of image processing algorithms from [FLLL]
- `Plugins/Freenect`:    wrapper based on [libfreenect]
- `Plugins/JavaAdapter`: enables plugin development under Java, contains algorithms implemented in Java
- `Plugins/OpenCV`:      wrapper around [OpenCV]
- `Plugins/Template`:    template code for your own plugin
- `Plugins/Tesseract:    wrapper around [Tesseract]

`/Examples` - Applications, demos and examples:

- `Examples/Basics/C++/00.HelloWorld`:     shows the necessary steps for a simple pureImage application
- `Examples/Basics/C++/01.PluginLoading`:  shows how to load, register, instantiate and use plugins
- `Examples/Basics/C++/02.PluginWorkflow`: summarizes the necessary steps for plugin usage
- `Examples/Basics/C++/03.SrcPlugins`:     shows how source-code plugins can be used with pureImage
- `Examples/Basics/C++/04.ForeignPlugins`: shows how plugins written in other programming languages can be used from a C++ pureImage application
- `Examples/Basics/C++/05.ByteImage`:      demonstrates the different ways how to use the ByteImage data type, the different levels of pixel access, and some best practices
- `Examples/Demos/<C++,Java>/Cookies`:     a C++ example for real time image acquisition and classification
- `Examples/Demos/<C++,Java>/OCR`:         demonstrates optical character recognition via Tesseract
- `Examples/Tools/C++/PluginLister`:       a C++ application to generate information about plugins in JSON format
- `Examples/Viewer/C++/2D`:                application to view image files, video files, or images from a live camera
- `Examples/Viewer/C++/Kinect`:            application to view images and depth maps acquired by a Microsoft Kinect device


Building pureImage
------------------

In April 2011, pureImage development completely switched to the cross-platform build system `CMake`.
Right now, we support 32-bit builds with

* gcc 4.6.1 under Ubuntu 11.10, and on
* Microsoft Visual Studio 2010 Express (= `vc10`) under Windows.

We also provide configuration files for vc9 and MinGW builds, but these are not well tested.
Note that 64-bit builds are not supported yet.

In order to build pureImage, get the source by cloning our Git repository via
`git clone https://pureimage.flll.jku.at/pureImage.git`.

On Ubuntu 11.10, simply run `Tools/install-dependencies-ubuntu11.10.sh` to
install all necessary 3rd party libraries and tools. Then generate a build environment
by running one of the provided configure scripts, e.g. for Makefiles or the
Code::Blocks IDE. Finally use the generated build environment to build pureImage.

For a vc10 build of the whole release (the core system, plugins, and examples),
a lot of 3rd party libraries and tools have to be installed or built manually.
See the files `cmake-win32-libs-vc10.conf` and `cmake-win32-tools.conf`, where all these
dependencies have to be configured. The vc10 build environment can then be created by
`configure-win32-vc10.bat`. The vc10 build can be packaged into an Inno-Setup installer
by compiling installer-win32.iss.

Note that even the code generation and building the Java build targets happens from
within the CMake-generated build environment, e.g. vc10.

External dependencies:

- Build system:
    - [CMake] (some files embedded)
- pureImage C/C++ core:
    - [Boost]
    - [JSON-C]
- pureImage C/C++ application core:
    - [Poco]
- pureImage Java core:
    - [Java SDK]
- pureImage code generation:
    - [Bison]
    - [Flex]
    - [SWIG]
    - [Python]
    - [msinttypes] (embedded)
- Unit tests:
    - [Google test]
- C++ Plugin development
    - [Boost]
- Java Plugin development
    - [Java SDK]
- Plugins and wrappers
    - [CImg]
    - [FLLL]
    - [OpenCV]
    - [libfreenect]
    - [Tesseract]

Additionally, binaries built with VC10 depend on the [Microsoft Visual C++ 2010 Redistributable Package].


Authors and contributors
------------------------

 - Ulrich Brandstaetter, JKU Linz, ulrich.brandstaetter@jku.at
 - Oliver Buchtala, JKU Linz, oliver.buchtala@jku.at
 - Roland Richter, JKU Linz, roland.richter@jku.at
 - Robert Pollak, JKU Linz, robert.pollak@jku.at
 - Xiaoyang Chen, ht_cxy009@hotmail.com


License
-------

pureImage is copyright © by Institut für Wissensbasierte Mathematische Systeme at
Johannes Kepler Universität Linz, and is released under the terms of the GPL v3.

pureImage documentation is licensed under a Creative Commons
Attribution-NonCommercial-ShareAlike 3.0 Unported License.


The pureImage Windows installer provides GPL-v3-licensed binaries:

- libraries that contain the following 3rd party libraries or parts of them, and
- applications that use the following 3rd party libraries:

[Boost], [CImg], [JSON-C], [OpenCV], [Poco], [msinttypes], [Tesseract]

The libraries' websites provide the corresponding source code.


[CMake]:        http://cmake.org/
[CImg]:         http://cimg.sourceforge.net/
[FLLL]:         http://www.flll.jku.at/
[OpenCV]:       http://opencv.willowgarage.com/wiki/
[libfreenect]:  http://openkinect.org/wiki/
[Tesseract]:    http://code.google.com/p/tesseract-ocr/
[Boost]:        http://www.boost.org/
[Python]:       http://www.python.org/
[JSON-C]:       http://oss.metaparadigm.com/json-c/
[Microsoft Visual C++ 2010 Redistributable Package]: http://www.microsoft.com/download/en/details.aspx?id=5555
[Poco]:         http://pocoproject.org/
[Java SDK]:     http://www.oracle.com/technetwork/java/javase/
[Bison]:        http://www.gnu.org/software/bison/
[Flex]:         http://www.cs.princeton.edu/~appel/modern/c/software/flex/flex_toc.html
[SWIG]:         http://www.swig.org/
[Google test]:  http://code.google.com/p/googletest/
[msinttypes]:   http://code.google.com/p/msinttypes/

[https://pureimage.flll.jku.at/]: https://pureimage.flll.jku.at/
