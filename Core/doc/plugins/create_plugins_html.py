from template_engine import load_json
import json
import plugins_html_template
import sys

if __name__ == "__main__":

    infile = sys.argv[1]
    outfile = sys.argv[2]
    
    try:
        plugins = load_json(infile)
    except ValueError as e:
        print("Could not parse json:\n%s"%(str));
        raise e

    vars = {
            "__root__": plugins,
            "__outfile__": outfile
    }

    plugins_html_template.process(vars)
