from template_engine import *

## Templates: #########################################################################################################

html_template = Template(
"""
<html>
<head>
<meta charset="utf-8">
<style>
.plugin {
  position: relative;
  margin-left:20px;
  margin-bottom:20px;
  border-top:3px solid #365F91;  
}

.backref {
  position: absolute;
  right:10%; top:10px;
}

.arguments {
  margin-left:50px;
}

</style>
</head>
<body>
<div id="index" class="pluginIndex">
  <ul>
$INDEX
  </ul>
</div> <!-- index -->

$PLUGINS
</body>
</html>
""")


index_entry_template = Template(
"""
    <li><a href="#${name}">${name}</a></li>
""")


plugin_template = Template(
"""
<div class="plugin" id="${name}">
  <h1>${name}</h1>

  ${plugin.description}

  <div class="arguments">
    <h2>Parameters</h2>
$PARAMETERS
  </div> <!-- parameters -->

  <div class="arguments">
    <h2>Input Signature</h2>
$INPUTSIGNATURE
  </div> <!-- input signature -->

  <div class="arguments">
    <h2>Output Signature</h2>
$OUTPUTSIGNATURE
  </div> <!-- output signature -->

  <div class="backref"><a href="#index">Index</a></div>
</div> <!-- plugin -->

""")


parameter_template = Template(
"""
      <li>
        <p>${replace_prefix(param["type"]["subClassName"], "pI_Argument_", "pI::")} <b>${param["argumentDescription"]["name"]}</b>
        <p>${param["argumentDescription"]["description"]}</p>
$STRINGSELECTION
      </li>
""")


string_selection_template = Template(
"""
        <p class="string_selection">Values:</p>
        <p>${', '.join(param["symbols"]["symbols"])}</p>
""")


signature_template = Template(
"""
      <li>
        <p>${replace_prefix(arg["type"]["subClassName"], "pI_Argument_", "pI::")} <b>${arg["argumentDescription"]["name"]}</b></p>
        <p>${arg["argumentDescription"]["description"]}</p>
      </li>
""")


cImgFunction_string_selection_template = Template(
"""
        <p class="string_selection">Value: ${cImgFunction.name}</p>
""")


## Helper methods: ####################################################################################################

def replace_prefix(string, old, new):
    return new + string[len(old):] if string.startswith(old) else string


## Template processing logic: #########################################################################################

def process(vars):
    # this allows using access per key (~dict) and also per attribute (~struct)
    vars = AttrDict(vars)

    INDEX = StringBuffer()
    PLUGINS = StringBuffer()
    for plugin in vars.__root__.plugins:
        if plugin.name == 'CImg/Functions':
            for cImgFunction in plugin["CImg functions"]:
                name = 'CImg/Functions::' + cImgFunction.name
                INDEX << expand_template(index_entry_template)
                
                PARAMETERS = StringBuffer()
                if len(plugin.parameters) > 0:
                    PARAMETERS << "    <ul>\n"
                    for param in plugin.parameters:
                        STRINGSELECTION = StringBuffer()
                        if param["type"]["subClassName"] == "pI_Argument_StringSelection":
                            STRINGSELECTION << expand_template(cImgFunction_string_selection_template)
                        PARAMETERS << expand_template(parameter_template)
                    PARAMETERS << "    </ul>\n"
                else:
                    PARAMETERS << "      None\n"
                
                INPUTSIGNATURE = StringBuffer()
                OUTPUTSIGNATURE = StringBuffer()
                if plugin.optionalInit == "true" or plugin.defaultInit == "true":
                    if len(cImgFunction.inputSignature) > 0:
                        INPUTSIGNATURE << "    <ul>\n"
                        for arg in cImgFunction.inputSignature:
                            INPUTSIGNATURE << expand_template(signature_template)
                        INPUTSIGNATURE << "    </ul>"
                    else:
                        INPUTSIGNATURE << "      None\n"

                    if len(cImgFunction.outputSignature) > 0:
                        OUTPUTSIGNATURE << "    <ul>\n"
                        for arg in cImgFunction.outputSignature:
                            OUTPUTSIGNATURE << expand_template(signature_template)
                        OUTPUTSIGNATURE << "    </ul>"
                    else:
                        OUTPUTSIGNATURE << "      None\n"
                
                PLUGINS << expand_template(plugin_template)

        else:
            name = plugin.name;
            INDEX << expand_template(index_entry_template)
            
            PARAMETERS = StringBuffer()
            if len(plugin.parameters) > 0:
                PARAMETERS << "    <ul>\n"
                for param in plugin.parameters:
                    STRINGSELECTION = StringBuffer()
                    if param["type"]["subClassName"] == "pI_Argument_StringSelection":
                        STRINGSELECTION << expand_template(string_selection_template)
                    PARAMETERS << expand_template(parameter_template)
                PARAMETERS << "    </ul>"
            else:
                PARAMETERS << "      None\n"
            
            INPUTSIGNATURE = StringBuffer()
            OUTPUTSIGNATURE = StringBuffer()
            if plugin.optionalInit == "true" or plugin.defaultInit == "true":
                if len(plugin.inputSignature) > 0:
                    INPUTSIGNATURE << "    <ul>\n"
                    for arg in plugin.inputSignature:
                        INPUTSIGNATURE << expand_template(signature_template)
                    INPUTSIGNATURE << "    </ul>"
                else:
                    INPUTSIGNATURE << "      None\n"

                if len(plugin.outputSignature) > 0:
                    OUTPUTSIGNATURE << "    <ul>\n"
                    for arg in plugin.outputSignature:
                        OUTPUTSIGNATURE << expand_template(signature_template)
                    OUTPUTSIGNATURE << "    </ul>"
                else:
                    OUTPUTSIGNATURE << "      None\n"
            else:
                INPUTSIGNATURE << "    <p>Dependent on parameters.</p>\n"
                OUTPUTSIGNATURE << "    <p>Dependent on parameters.</p>\n"
            
            PLUGINS << expand_template(plugin_template)


    with open_file(vars.__outfile__) as f:
        f.write(expand_template(html_template))
