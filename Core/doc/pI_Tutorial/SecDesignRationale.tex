\lsection{pureImage design rationale}
Several recent projects at our department included a considerable amount of work on (industrial) image processing algorithms: in these projects, the proper use of state-of-the-art methods, as well as the development of new methods from scratch was required. Typically, our projects dealt with optical inspection and quality control.

Often, these algorithms were first developed and tested within a \emph{project-specific} test tool, then integrated into a \emph{vendor-specific} runtime environment. This already made it necessary to design and implement the algorithms as modular as possible.

The requirement for an image processing superstructure finally lead to the development of the 
\pI{} framework.
\pI{} is not intended to be a replacement for other C++ image processing frameworks. It is designed as a framework that provides a plugin mechanism for algorithms from various sources, including methods from sophisticated frameworks, home-grown code, but also image processing procedures from non-C++ environments, such as \Matlab{}.

\lsubsection{Fundamental architecture decisions}

When we started to think about the architecture of our framework, we faced two fundamental design considerations. 

\begin{enumerate}
	\item The fundamental motivation for any plugin framework is the urge for code reuse. Code reuse can be accomplished in multiple ways:
		\begin{enumerate}
			\item using functions
			\item using templates
			\item using executable files
			\item using dynamic libraries
		\end{enumerate}
We agreed on using dynamic libraries, as they separate application and image processing logic better.
	\item
	We don't want to roll out another discussion on the benefits and disadvantages of different programming languages. As we develop image processing tools in the context of industrial applications we need a language which is efficient, relatively close to hardware, and comfortable to use.
	We agreed on C++ as main development language, and we wanted to use available methods (even from different environments or languages) by a uniform interface. In addition, we concluded that the possibility to export image processing functionality implemented in C++ to other environments is desirable.
\end{enumerate}

For various (technical) reasons, the combination of using dynamic libraries in conjunction with C++ is not straightforward (rule \#63 in \cite{cppcodingstd}), and requires some careful design considerations, as shown subsequently in \autoref{Crossing dynamic link library boundaries when using C++}.

\lsubsection{Framework design rationale -- a view from the outside of a plugin}

\lsubsubsection{Crossing dynamic link library boundaries when using C++}
C++ offers multiple ways to adopt dynamic link libraries, each with its individual shortcomings:
\begin{itemize}
	\item Exporting / importing \textbf{C++ classes and methods}:\\
While this possibility is probably the easiest to implement, it quickly turned out a bad choice, for many reasons: C++ class dynamic link library export involves name mangling, dependency on specific compiler, compiler version, compiler build target (debug / release) and even compiler flags.
	\item Using \textbf{abstract base classes} for dynamic link library export and import:\\
This possibility is more promising\cite{cppcom}, and is used by the prominent COM architecture\cite{insidecom}. Although we adopted this possibility for other projects, it too has its pitfalls: typically it is not possible to use virtual destructors, to provide any default implementation, it requires compilers to adopt the same vtable layout, yet worst, it does not allow to use complex data structures in the interface (such as vectors - their binary layout, which is independent from the interface, may differ) when different build environments are used.
	\item Using \textbf{C-only} dynamic link libraries:\\
Although being limited to C, regarding dynamic link library import and export, is a severe restriction, we agreed on this way: C provides a stable ABI, so none of the shortcomings of the other possibilities are suffered. In addition, we managed to circumvent the C restriction by the creation of an adapter framework to a certain degree.
\end{itemize}
\lparagraph{Adapter framework}\\
The adapter framework primarily has two responsibilities, namely the translation of image processing plugins written in various languages (C++, ...) to C plugins for dynamic link library export and vice versa, and the representation of C plugin arguments for different languages (C++, ...) for an easy usage.
The translation of image processing plugins are explained for C++ in the following, in two parts:
\begin{itemize}
	\item \textbf{C++ to C adapter}\\
This adapter converts any C++ image processing plugin, which implements our C++ class interface, to a C image processing plugin. In short, it wraps all C++ specifics from the class interface to proper C only representations. This includes the translation of any caught exception (C++) to simple error codes. It also takes care for the synchronization of C++ class and its C counterpart regarding object lifetime: If the C plugin is deleted, this is also true for the C++ class.
	\item \textbf{C to C++ adapter}\\
Because C++ is our main development language, we also want to use available image processing algorithms in C++. Therefore an adapter, which converts a C image processing plugin to its C++ counterpart, is also provided. Like before, this adapter also synchronizes object lifetimes.
\end{itemize}
In order to enable other languages to use \pI{} plugins, too, we use the C++ adapters as input for \SWIG{}\cite{swig}, which can automatically generate proper bindings for additional languages.\\
The representation of C plugin arguments for different languages follows different mechanisms, which are explained in \autoref{Integration into new target platforms}.

\lsubsection{Plugin design rationale -- a view from the inside of a plugin}

\lsubsubsection{Working function signature}
We want to enable simple filter-type plugins (one input image transferred to one output image), as well as plugins of arbitrary I/O signature complexity, regarding number and type. This has to be supported by ImagePlugin, more precisely, by its Execute(...) method. Considering the "..." part of "Execute(...)", i.e., the function signature, there are several possibilities:
\begin{enumerate}
	\item \C{Result Execute(InputArgument1, InputArgument2, ..., InputArgumentN)}
	\item \C{OutputArgumentList Execute(InputArgumentList)}
	\item \C{Execute(InputAndOutputArgumentList)}
	\item \C{Execute(InputAndOutputArgumentList\&)}
	\item \C{Execute(InputArgumentList, OutputArgumentList)}
	\item \C{Execute(InputArgumentList\&, OutputArgumentList\&)}
\end{enumerate}
Several considerations shortened the list above:
\begin{itemize}
	\item There might be more than one result (discards 1).
	\item We don't want to have manifold method signatures (especially, since we do NOT want to have one single base class "Argument", see below) (discards 1, again).
	\item We want to avoid copying where possible (discards 1, 2, 3, 5).
	\item Some arguments might be input as well as output, which we want to support mainly for performance reasons; that is, we want to support some kind of "C++ reference style" (discards 1, 2, 3, 5).
\end{itemize}
With only option 4 and 6 left, we finally settled on 6: first, it is slightly more convenient to use, for not every argument has to be tested if it is input or output. Second, it is easier to understand.

\lsubsubsection{Arguments for image processing plugins}

Assume that a plugin creates a new image, and returns it; or assume that the result image of one plugin is passed as an argument to another plugin. This scenario has a number of consequences:
\begin{itemize}
	\item The lifetime of plug-ins and arguments must be decoupled.
	\item Resources must not be managed by plugins (but by the runtime (\autoref{Runtime}, \autoref{Resource management})).
	\item For performance and memory reasons, call-by-value must be avoided with large data blocks.
	\item Resource leaks must be avoided.
	\item Allocations and de-allocations must happen on the same side of the DLL boundary.
	\item The result of a plugin can also be used as input argument, or even as parameter, by another plugin (\autoref{Control considerations}).
\end{itemize}
Regarding the implementation of arguments, there were several options:
\begin{itemize}
	\item Using \C{void*}, which is both unsafe and ugly.
	\item Deriving everything from one single base class (such as \emph{Object} in \textit{Java}). This option has several issues again:
		\begin{itemize}
			\item How to pass these "Objects" over the C ABI without performance loss?
			\item This might induce a compiler dependency, due to vtable layout.
			\item This would require RTTI\cite{rtti}, hence enlarging objects.
		\end{itemize}
	\item Serializing and deserializing everything, which most likely causes significant overhead, especially when dealing with larger types.
	\item Using C POD types\cite{pod} for arguments turned out as (compromise) solution:
		\begin{itemize}
			\item Slightly faster data access (in comparison to objects with base class).
			\item A combination of a C union (with data types focused on image processing, \autoref{Under the hood: arguments and parameters}) and a type id yields type safety to a certain degree.
			\item Unproblematic regarding dynamic link library boundaries, because of consistent object layout due to C ABI.
			\item For more convenient usage, it is possible to generate language-specific adapter code for arguments (\autoref{Generation of C++ adapters for arguments}).
		\end{itemize}
\end{itemize}

\lsubsubsection{Plugins call other plugins}
We want to reuse existing functionality (home grown, as well as foreign), therefore we want to enable a plugin to call another plugin (\autoref{Plugin delegation}). Once again, there were several design considerations:
\begin{itemize}
	\item A plugin should not be burdened by the plugin loading mechanism.
	\item There must be a centralized point where plugin loading is done (the application).
	\item There must be a simple, yet unambiguous way to query the availability of foreign plugins - we use an URL-style plugin registration mechanism, which we provide in terms of the \autoref{Runtime}.
\end{itemize}

\lsubsubsection{Plugin parameters}
We want plug-ins to be parametrized. During the design phase, it turned out that "Parameter" is similar to "Argument"; the distinction is just on a semantic level. The introduction of plugin parameters (\autoref{Parametrized plugins}) leads to a specific plugin lifecycle (\autoref{Plugin lifecyle}), and to plugin states (\autoref{Plugin states}).

\lsubsubsection{Plugin expectations}
A plugin must possess facilities to tell which kind of data it expects:
\begin{itemize}
	\item On the one hand, a plugin expects a (fixed) set of Parameters.
	\item On the other hand, a plugin has to provide means to query its signature. This is not limited to returning the types which \C{Initialize()} or \C{Execute()} expects; it should also be possible to communicate that the method expects, for instance, an image of certain colour depth and/or size.
\end{itemize}
Therefore, we introduce the notion of \emph{Signature}: a signature is an argument without actual data - it only yields meta-information about the kind of argument it expects. Every argument type has meta information.\\
Very important: We want plugins to be able to change their signatures, depending on given parameters (only during plugin initialization).\\

\lsubsubsection{Constraint parser}
Regarding the expectations of plugin arguments, we provide the means for a plugin developer to be more specific: for instance, if an image processing plugin is meant to operate on gray level images only, the programmer can specify a constraint string for an image argument in the following form:
\begin{snippet}
"channels == 1"
\end{snippet}
Using a single function call, a plugin can validate a given argument using its specified constraints. If its validation fails, the plugin execution is stopped due to the unfulfilled preconditions.

\lsubsubsection{Plugin lifecyle}
Several of the above points indicate that the plugin life cycle consists of separated steps (\autoref{Plugin construction, initialization and execution}):
\begin{itemize}
	\item Construction: after plugin construction, a plugin must be able to satisfy a request for its parameter signature.
	\item Initialization: is semantically equivalent to plugin parametrization, since the \C{Initialize()} method expects parameters. After plugin initialization, a plugin must be able to satisfy request for its input and output signatures (since they may be dependent on plugin parametrization). A plugin may be initialized many times (which of course leads to the loss of its inner state (\autoref{Plugin states})).
	\item Execution (possibly multiple times).
\end{itemize}

\lsubsubsection{Image argument}
We are focusing on image processing; we want to support as many image types as possible, as well as images with meta-data, images with multiple pages, etc.. We considered Boost.GIL, OpenCV, FreeImage, libtiff, and a home-grown type.\\
Finally, we agreed on a home-grown image container (\autoref{Image argument, final candidate}), which is, in terms of its binary layout, similar to the \FreeImage{} bitmap (C structure, byte-sized pixel components, padding on width dimension).

\lsubsubsection{Unified error handling}
Potentially, errors of various kinds can occur during plugin construction, initialization, execution and (de-)serialization. We agreed on the following:
\begin{itemize}
	\item We use exception handling, adhering to the standard C++ rules.
	\item We supply a set of image processing exceptions (\autoref{Exceptions}).
	\item Because errors regarding arguments are very common (at least in our experience), we also provide a set of functions to ease the parameter and argument validation.
\end{itemize}

\lsubsubsection{Argument / plugin persistation}
We want to store any result, as well as any plugin, to the harddisk and reload it when necessary. We agreed on the following (\autoref{Serialization}):
\begin{itemize}
	\item Regarding persisted argument types, they should be human readable. Also they should be intuitive - a matrix should look like a matrix -, and simple to edit. Therefore, we provide \JSON{}\cite{json} serialization for arguments. Note however that the used serialization protocol can easily be changed.
	\item Regarding plugins, we honour the freedom of a plugin developer: he may adopt any serialization mechanism (including XML, JSON, Yaml, etc.), the only requirement for a plugin with serialization support is the ability to deserialize properly.
\end{itemize}

\lsubsubsection{Decoupling of application and plugin logic}
We want to decouple application and plugin logic:
\begin{itemize}
	\item We adopt dynamic link libraries for the loose coupling.
	\item We split our framework into three components:
		\begin{enumerate}
			\item \pI{}, the core framework.
			\item \pIn{}s, a set of image processing plugins.
			\item a \pI{} application or scripting environment
		\end{enumerate}
\end{itemize}

\lsubsubsection{Additional programming languages}
We want to use languages in addition to C++. Due to the decision to base on a C ABI it is possible to enable other languages (with C bindings), as well.
\begin{itemize}
	\item C is natively supported.
	\item Any language with C bindings can possibly be used (with limited effort, thanks to \SWIG{}).
	\item At the time of writing, \pI{} can be used and developed for in: C, C++, Java
\end{itemize}

\lsubsubsection{All for plugin developers}
We want to favour plugin developers where possible:
\begin{itemize}
	\item Rather simple C++ interface, few methods to implement.
	\item Many convenience methods (especially for argument creation and validation).
	\item Automatic resource clean up (of Arguments, Parameters and foreign plugins, due to the usage of smart pointers).
	\item Few stumbling blocks
	\item Documentation: there are lots of code examples (home-grown, CImg, FreeImage, ...), Doxygen documentation, tutorial, unit tests, ...
\end{itemize}


\lsubsection{Design goals}

In the following, a list containing accomplished design goals and relevant design decisions is presented:

\begin{itemize}

	\item Above all, we favour plugin developers where possible: to ensure a simple and easy to understand interface, the internals of the \pI{} framework are rather complex. In other words: whenever there was a design decision regarding the simplicity of the framework for any role, plugin developers were favoured.

	\item \pI{} allows an easy reuse of existing functionality: any plugin can query the central registrar (which we call runtime - \autoref{Runtime}) for the availability of \emph{registered} other plugins and execute them, if desired, in a simple way.

	\item We use both C and C++ programming languages (\autoref{Languages}): C++ is the primary plugin development language (although any other language with C bindings can be used as well, provided that proper bindings for \pI{} are available\footnote{because of \SWIG{}, additional bindings can be generated almost without effort, Java bindings are already available.}), and C is used because of its binary compatibility. With the notable exception of the provided data types, a C++ developer using \pI{} is not constricted by C in any way. As consequence, C++ developers (or developers who prefer other languages, such as \Java{}) are not dependent on particular compiler vendors or settings.

	\item For \pI{}, the origin of an image processing plugin does not matter at all. Plugins can be used from source code, from static libraries or from DLLs, for the framework this is completely transparent.

	\item In the course of designing the framework, differentiating between three parts made design simpler:
		\begin{enumerate}
			\item \pI{}: the core library, providing the basic framework mechanisms
			\item \pIn{}: a collection of algorithms using \pI{}
			\item \pI{} application or scripting environment
		\end{enumerate}

	\item Resource efficiency: digital image processing often involves large data blocks (images, matrices, etc.). Their copying is problematic not only because of the memory usage, but also because of the time the copying takes. Therefore, \pI{} involves a central resource allocator, the runtime (\autoref{Resource management}, \autoref{Runtime}), which conceptually avoids multiple copies.

	\item To enforce that the data delivered from one plugin to another is the same all way, all available data types are implemented as C types\footnote{so callod POD - plain old data - types} (\autoref{C arguments}). They only involve reasonable overhead, they do not use void pointers at all, therefore a certain degree of type safety is archived. As C types, inheritance is not used for data (f.i. derive everything from "`Object"').
	
	\item \pI{} involves mechanisms for automatic resource deallocation for C++ developers (\autoref{Under the hood: arguments and parameters}). If any (exported) data block is no longer used, its memory is deleted automatically (using the central resource allocator, \autoref{Resource management}, \autoref{Runtime}). 

	\item Although the framework is kept as generic as possible, it still focuses on image processing. 
	
	\item \pI{} distinguishes between algorithm parameters and arguments on a pure semantic level: although both are, in fact, based on the same data types, they play a different role and enable regulation and control tasks for image processing (\autoref{Control considerations}).

	\item Serialization (\autoref{Serialization}) plays a major role for \pI{}: it not only enables the persistation of stateful plugins, it also enables the persistation of any parameter and argument supported, therefore image processing results can be easily preserved. Also, the plugin serialization mechanism is used to create stateful clones of plugin instances per default.

	\item \pI{} delegates 3rd party dependencies to the plugins: if a plugin is dependent on a 3rd party library, this is not visible outside to the outside, a \pI{}-enabled application need not be aware of any plugin dependency. At the FLLL, we organized our plugin projects according to their third-party dependencies.
	
\end{itemize}