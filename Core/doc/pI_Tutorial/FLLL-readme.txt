The tutorial uses the FLLL styles.
To enable these, copy the directory flll from \\Nagoya\install\ to %PROGRAMFILES%\<MiKTeX>\tex\latex\
and "Refresh FNDB" in the MiKTeX options: Programs > <MiKTeX> > Maintenance (Admin) > Settings (Admin).

(Tested with <MiKTeX> = MiKTeX 2.8, MiKTeX 2.9)
