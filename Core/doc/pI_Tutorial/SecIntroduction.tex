\lsection{Preface}

\lsubsection{Motivation}

In his 2003 monograph \cite{glass2003facts}, Robert L. Glass devotes several items to the broad topic of reuse in software engineering. He claims that
\begin{quotation}
Fact 16. Reuse-in-the-large (components) remains a mostly unsolved problem, even though everyone agrees it is important and desirable
\end{quotation}

as well as
\begin{quotation}
	Fact 17. Reuse-in-the-large works best in families of related systems and thus is domain-dependent. This narrows the potential applicability of reuse-in-the-large.
\end{quotation}

It was this quest for ``reuse-in-the-large'' in the domain of image processing, especially under C++, which brought together a project group at Fuzzy Logic Laboratorium Linz\footnote{\url{http://www.flll.jku.at/}}, a department of Johannes Kepler University Linz, in spring 2009. Being sick of reinventing the wheel over and over again, we tried to reduce our image processing experiences to a common denominator, which finally lead to the development of the \pI{} framework.

\lsubsection{What pureImage is -- and what it is not}

The essence of \pI{} can be described from one of two perspectives:

\textbf{\pI{} is a rapid-prototyping tool.} For developers who want to write image processing algorithms in C++ in a modular way, \pI{} is a framework that provides an easy way to create new plug-ins with a common interface, and allows to use them in a managed environments.

\textbf{\pI{} is a cross-language interface.} For developers who want to re-use algorithms from existing image processing libraries (such as CImg\footnote{\url{http://cimg.sourceforge.net/}}, OpenCV\footnote{\url{http://opencv.willowgarage.com/wiki/}}, etc.) in another environment (such as Java, \Matlab{}\footnote{\url{http://www.mathworks.com/}}, etc.), \pI{} is a plug-in framework that provides an uniform interface to image processing algorithms with respect to workflow and data structures.

That said, let us also state what \pI{} \emph{is not}:
\begin{itemize}
    \item \pI{} \emph{is not} a replacement for sophisticated C++ frameworks such as \OpenCV{}, neither can it nearly do what \Matlab{} can.
    \item \pI{} \emph{does not} (yet) provide as many image processing operations as other libraries.
    \item \pI{} \emph{does not} implement a(nother) scripting language for image processing, although the usage of \pI{} from a(nother) scripting language would be perfectly possible.
    \item \pI{} \emph{does not} yet provide a graphical programming environment, although it can perfectly used for such purposes because of its architecture -- a plugin to model complex execution graphs is already available.
    \item \pI{} \emph{does not} yield zero overhead -- but if there is no free lunch, \pI{} at least is a tasty meal at low price...
\end{itemize}

What \pI{} actually can do:
\begin{itemize}
    \item \pI{} can be seen as a superstructure for existing libraries -- as a common denominator.
    \item \pI{} aims at the reuse and composition of existing image processing functionality -- therefore it provides features to supply functionality for and to use functions from image processing algorithms.
    \item \pI{} may reduce development and testing times due to the plugin reuse and composition. It facilitates prototyping.
    \item \pI{} may enhance testing facilities for the same reasons.
    \item \pI{} can end wars -- the \Matlab{}, C++ and \Java{} fractions finally can work on the same projects and even profit from each other...
    \item \pI{} does not rely on a specific tool chain -- it is possible to mix plugins which are translated by compilers of different vendors.
    \item \pI{} helps at the modularization of image processing algorithms due to the homogeneous treatment of plugins.
    \item \pI{} can reduce compilation dependencies.
    \item \pI{} does the boring stuff: a plugin writer no longer has to worry about clean DLL imports and exports but can focus on more fruitful tasks, such as actual image processing algorithm development...
    \item \pI{} alleviates co-operation amongst multiple parties: third parties can implement and provide plugins, and if desired retain the source code. Existing systems can safely be extended.
    \item \pI{} allows hot swapping - the exchange of functionality during runtime.
    \item \pI{} allows the declaration of new complex data types in a generic and easy way.
    \item \pI{} can export these data types to various environments (C++, \Java{}, ...).
    \item \pI{} takes care of the memory allocation and deallocation for its data types.
    \item \pI{} can potentially be used by any high-level language which supports C bindings (thanks to \SWIG{}, \pI{} can be used on arbitrary environments without serious effort)
\end{itemize}


\lsubsection{Comparison to other frameworks}

As far as we know, there is no comparable meta image processing library. There are, however, a number of plug-in frameworks which served as an inspiration to us. The very first project proposal mentioned these three frameworks:

\begin{itemize}
	\item RapidMiner, \url{http://www.rapidminer.com}
    \item Virtual Studio Technology (VST), \url{http://www.steinberg.net}
    \item Buzz, \url{http://www.buzzmachines.com}
\end{itemize}

\cite{gip} evaluated a number of frameworks from different fields, namely:

\begin{itemize}
	\item Qt plug-ins, \url{http://qt.nokia.com}
    \item Adobe Photoshop plug-ins, see e.g. \url{http://thepluginsite.com}
    \item FxEngine framework, \url{http://www.smprocess.com}
    \item Linux Audio Developer's Simple Plug-In API (LADSPA), \url{http://www.ladspa.org}
    \item Virtual Studio Technology (VST), \url{http://www.steinberg.net}
\end{itemize}

Additionally, over time we considered some further frameworks, such as:

\begin{itemize}
    \item pure data, \url{http://puredata.info/}
    \item Vamp audio analysis plugin system, \url{http://vamp-plugins.org/}
\end{itemize}

\lsubsection{Structure of this document}

This tutorial is intended to give an introduction to \pI{}, to enable C++ programmers to develop and to exert image processing plugins using the \pI{} framework. It comes in four parts (which contain updated versions of \cite{fllltr1001} and \cite{fllltr1002}):
\begin{itemize}
    \item \autoref{pureImage design rationale}, Design rationale, presents the design considerations for \pI{}.
    \item \autoref{pureImage plugin developer tutorial}, Plugin developer tutorial, aims at putting an idea across the utilization of the \pI{} framework for image plugin development. In probably less than half an hour a C++ developer should be able to develop \pI{} plugins and utilize the provided features.
    \item \autoref{pureImage application developer tutorial}, Application developer tutorial, aims at providing an overview of the architectural layout of the \pI{} framework, its inner mechanisms and at the utilization of \pI{} in an image processing application. Although not strictly necessary, the study of this tutorial is also beneficial for plugin developers.
    \item \autoref{pureImage enhancement tutorial}, Enhancement tutorial, shows how certain aspects of the \pI{} runtime can be adapted for individual needs.
\end{itemize}


\lsubsection{Acknowledgements}

We would like to thank a number of people who were in some way involved in the development of \pI{}.

First of all, Thomas Klambauer's critical evaluation of plug-in techniques \cite{gip} served
as a starting point for many discussions on the architecture of \pI{}, which resulted in (hopefully)
sensible decisions as described in \autoref{pureImage design rationale}.

When the development of \pI{} was underway, David Tschumperl\'{e}, Markus Rauhut, Thomas Redenbach,
and Andreas Jablonski provided us with an ``outside'' view and valuable comments.

Thanks to the colleagues of FLLL who were discussion partners and beta testers.

This project was in part funded by the Upper Austrian Technology and Research Promotion.
