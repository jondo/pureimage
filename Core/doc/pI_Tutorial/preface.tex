

%**************************** preface.tex
%
% Some useful definitions for technical papers with LaTeX/pdfTeX
% Written by Roland Richter <roland@flll.jku.at> during 1998-2005.
%
%
% To be used as follows:
%
% \documentclass[a4paper]{article}
%
% \newcounter{chapter} % We need it anyway, but it is not defined in article
% \input{preface.tex}
%
%
%****************************

%****************************

\typeout{}
\typeout{ ------- LaTeX/pdfLaTeX preface ------- }
\typeout{ Something mysterious is formed, born in the silent void. }
\typeout{}
\typeout{}

%**************************** That's what we need:

\usepackage{enumitem}
\setdescription{font=\rmfamily\scshape}

\usepackage{latexsym}

\usepackage[intlimits]{amsmath}
\usepackage{amsfonts}

\usepackage{epigraph}
\setlength{\epigraphrule}{0pt}
\renewcommand{\textflush}{flushepinormal}
%\setlength{\epigraphwidth}{.4\textwidth}

\usepackage{caption}
\captionsetup{textfont=sc,labelsep=period,width=.85\textwidth}
\captionsetup[lstlisting]{font=sc,textfont=up,labelsep=period,width=.9\textwidth}

\usepackage{mparhack}

%**************************** Especially for use with PDF:

\usepackage{color}
\usepackage{hyperref}

\usepackage{listings} % Version 1.0 required
\definecolor{darkred}{rgb}{0.65,0,0}
\definecolor{darkgreen}{rgb}{0,0.4,0}
\definecolor{darkblue}{rgb}{0,0,0.4}
\lstloadlanguages{C++}
\lstset{language=C++,
        basicstyle=\sffamily\small,
        keywordstyle=\color{darkred}\bfseries,
        ndkeywordstyle={},
        commentstyle=\color{darkgreen}\itshape,
        stringstyle=\color{darkblue}\sffamily\small,
        showstringspaces=false,
        xleftmargin=1em,
        basewidth={0.5em,0.45em},
        morendkeywords={::},
        aboveskip=2ex,
        captionpos=b,
        floatplacement=htb
        }      % "<" for templates: in class<T>, "T" would be too close to "<"
               % looks better, however, if you place spaces around "T"

\lstnewenvironment{snippet}
  {}
  {}

\lstnewenvironment{code}[1][]
  {\lstset{float,
           frame=single,frameround=ftff,
           backgroundcolor=\color{white},
           #1 } }
  {}

\newcommand{\C}[1]{\lstinline$#1$}

%**************************** Some lengths:

\setlength{\parindent}{0pt}
\setlength{\parskip}{1ex plus.2ex minus.2ex}
\setlength{\fboxsep}{1ex}


%**************************** New bullets for new lists!

\newcommand{\BlBox}{$\,$\rule[0.3ex]{0.9ex}{0.9ex}$\,$}
\newcommand{\blBox}{$\,$\rule[0.3ex]{0.6ex}{0.6ex}$\,$}
\newcommand{\BlDot}{$\bullet$}
\newcommand{\blDot}{$\cdot$}


%**************************** Definiton of theorem style

\newcounter{ThmNumber}[section]
\renewcommand{\theThmNumber}{\thesection.\arabic{ThmNumber}}
% With documentstyle article, "chapter" should be replaced by "section".

\newcommand{\makeTitle }[1]{\refstepcounter{ThmNumber}
                           {\sc\theThmNumber{.\hspace{0.5em}{#1}}}}

\newlength{\ThmWidth}

\newenvironment{Thm}[1][Theorem.]
  { \setlength{\ThmWidth}{\textwidth} \addtolength{\ThmWidth}{-2.5em}
    \vspace{2.ex} \makeTitle{#1} \nopagebreak \\[1.ex]
    \hspace*{2.5em}\begin{minipage}[t]{\ThmWidth}
  }
  { \end{minipage} \vspace{2.5ex}
  }

\newenvironment{Prf}[1][Proof.]
  {{\vspace{-2.5ex}\\{\sc#1}\\} \nopagebreak
   \begin{small}%
  }
  { \hfill $\Box$ \end{small} \vspace{1ex}
  }

\newcommand{\Ref}[2]{\textsc{#1}~\ref{#2}}

% A typical theorem:
%
% \begin{Thm}[The theorem of Faucett (1954).\label{Faucett} }
% ...
% \end{Thm}
%

% \numberwithin{equation}{chapter}
% Looks better with article, but not with report and book!


%**************************** Frame (= title, text and frame around)

\newlength{\FrameWidth}
\newlength{\dummytw}
\setlength{\dummytw}{\textwidth}
\addtolength{\dummytw}{-24mm}
\setlength{\FrameWidth}{\dummytw}
\newsavebox{\FrameBox}

\newenvironment{Frame}[1]
%                             #1 ... title
  { \begin{lrbox}{\FrameBox}
      \begin{minipage}[t]{\FrameWidth}
        \makeTitle{#1} \\[1ex]
  }
  { \end{minipage}
    \end{lrbox}
    \addtolength{\FrameWidth}{8mm}
    \vspace{5ex} \hspace*{8mm}
    \framebox[\FrameWidth]{\usebox{\FrameBox}}
    \vspace{5ex}
  }

\newenvironment{NoFrame}[1]
%                             #1 ... title
  { \begin{lrbox}{\FrameBox}
      \begin{minipage}[t]{\FrameWidth}
         {#1} \\[1ex]
  }
  { \end{minipage}
    \end{lrbox}
    \addtolength{\FrameWidth}{8mm}
    \vspace{5ex} \hspace*{8mm}
    \makebox[\FrameWidth]{\usebox{\FrameBox}}
    \vspace{5ex}
  }


%**************************** Margin paragraphs:

\setlength{\marginparpush}{2ex}
\newcommand{\margin}[1]{\marginpar{\raggedright\footnotesize {#1}}}


%**************************** For beautiful pics:

\newcommand{\Fig}[2]
  { \begin{figure}[ht]
      \begin{center} {#1} \end{center}
      {\sc \caption{\rm {#2}}}
    \end{figure}
  }

\newcommand{\Pic}[1]
  { \begin{center} #1 \end{center}
  }

\newcommand{\Graphic}[2][]
  { \begin{center} \includegraphics[#1]{#2} \end{center}
  }

\newcommand{\Figure}[2]
  { \begin{figure}[ht]
      {#1}
      {\sc \caption{\rm {#2}}}
    \end{figure}
  }

\renewcommand{\thefigure}{{\sc \arabic{figure}}} % Article
% \renewcommand{\thefigure}{{\sc \thechapter}.{\sc \arabic{figure}}} % Report, Book


%**************************** Some undocumented features:

\newcommand{\META}[1]{ \textcolor{red}{\it #1} \marginpar{ \textcolor{red}{\huge !} } }

\newcommand{\cf}[1]{[{\sc #1}]}


%**************************** Mathematics

\newcommand{\defiff}{\null \, : \! \iff}

\newcommand{\thus}{\; \raisebox{.2em}{$\frown \!\!\!\! _\lhd\,$}}

\newcommand{\flash}{^{\angle}
                    \!\!\! \raisebox{ .7ex}{$_{_\not}$}
                    \!     \raisebox{-.9ex}{$\triangleright$} \,}

\newcommand{\existsOne}{\exists ! \,}

\newcommand{\into}{\longrightarrow}

\renewcommand{\i}{\!\symbol{23}\:\!\!\!\!\!\imath}

\newcommand{\zpi}{2 \pi \i}

\newcommand{\lbar}[1]{\overline{#1}}

\newcommand{\Ord}[1]{\mathcal{O}({#1})}

\newcommand{\set}[1]{\mathbb{#1}}

\newcommand{\compl}[1]{{\cal C}#1} % complementary set

\newcommand{\evalAt}[1]{\big\vert \null_{#1}} % 'evaluated at' (for functions)

\newcommand{\mustBe}[1]{\stackrel{!}{#1}} % for =, >, <, ... with a !

\newcommand{\norm}[1]{\left\|{#1}\right\|}

\newcommand{\inner}[2]{\left\langle{#1},{#2}\right\rangle}
