\lsection{pureImage enhancement tutorial}

\pI{} can be customized and extended in several ways, it is even possible to adapt the framework to totally different fields of application with limited effort.\\
The enhancement tutorial shows different ways how \pI{} can be altered to suit individual needs.


\lsubsection{Enhancing types}

\pI{} enables the generic addition of new data types, they are broken down into three kinds:
\begin{enumerate}
	\item \textbf{Intrinsics}: the basic data types more complex types are made of (strings, floats, etc.)
	\item \textbf{Structures}: compound intrinsics, similar to C structs
	\item \textbf{Arguments}: data container used for plugin interconnection; may consist of Intrinsics, Structures, Arguments and multi-dimensional arrays.
\end{enumerate}


\lsubsubsection{Intrinsics}

The intrinsic data types resemble the basic types of \pI{} and include characters, strings, integer and floating point values. They are meant as internal data types and \emph{bricks} for more complex types.\\
A reasonable set of standard type is already available, new types (such as pointers to data types of foreign libraries, render contexts, device contexts, etc.) can be added with limited effort:
\begin{enumerate}
	\item \textbf{Registration}: in order to register a new intrinsic type to \pI{}, the \texttt{Intrinsics.def} file must be extended as shown below:
\begin{snippet}
/* integral types */
TYPE(unsigned char, pI_byte)
TYPE(int32_t, pI_int)
TYPE(uint32_t, pI_unsigned)
TYPE(uint32_t, pI_size)

/* floating point types */
TYPE(float, pI_float)
TYPE(double, pI_double)

/* boolean types */
TYPE(pI_int, pI_bool)

/* character and string types */
TYPE(char, pI_char)
TYPE(pI_string_type, pI_str)
\end{snippet}
	\item \textbf{Implementation}: because more complex intrinsics usually involve special allocation strategies or serialization techniques, \pI{} must be taught how to handle them. Therefore it may be necessary to extend the file \texttt{IntrinsicRuntimeFunctions.c} and provide implementation for the following functionality:
\begin{itemize}
	\item \textbf{allocation} (\C{CRuntime_CreateIntrinsicDataType})
	\item \textbf{de-allocation} (\C{CRuntime_FreeIntrinsicDataType})
	\item \textbf{copying} (\C{CRuntime_CopyIntrinsicDataType})
	\item \textbf{serialization} (\C{CRuntime_SerializeIntrinsicDataType})
	\item \textbf{deserialization} (\C{CRuntime_DeserializeIntrinsicDataType})
\end{itemize}
It also might be necessary to extend the constraint parser (\autoref{Enhancing the constraint parser}).
\end{enumerate}


\lsubsubsection{Structures}

\pI{} structures are an analogue to C structs and can be used in a similar way. They are defined using a special meta-language (very simple, implemented using X-macros\cite{xmacros}, ANSI-C):
\begin{snippet}
/* Complex.def */
FIELD(pI_float, r)
FIELD(pI_float, i)
\end{snippet}
This structure definition allows \pI{} not only to generate the proper C struct, but also to store additional information regarding the types and the layout, which is mandatory for a generic treatment. The only disadvantage is that a new structure definition must happen in a new file. The command \C{FIELD} specifies a new structure member, with the first parameter as type and the second parameter as the variable name. A \pI{} structure can contain any intrinsic type and other structures (as long as they are declared and included in advance).\\
The usage of a \pI{} structure is as usual:
\begin{snippet}
/* Complex.def */
Complex c = {0.0f, 0.0f};
c.i = -1.0f;
\end{snippet}


\lsubsubsection{Arguments}

\pI{} arguments are the data types used for plugin interconnection. They can be used to represent input and output data, as well as plugin parameters. An argument may contain intrinsics, structures, other arguments and multi-dimensional fields of them.\\
An argument is a C structure:
\begin{snippet}
typedef struct _Argument {
  struct _ArgumentSignature signature;
  union _pI_ArgumentData data;
  struct _ArgumentDescription description;
  struct _CRuntime* runtime;
} Argument;
\end{snippet}
An argument contains:
\begin{itemize}
	\item \textbf{a description}\\
The argument description consists of a human readable argument name and a human readable textual description.
	\item \textbf{a pointer to the runtime (responsible for the argument)}\\
An argument knows the runtime responsible for its allocation, since an application may contain several runtime instances (possibly of different versions). Therefore, the runtime pointer is used for any operation on the argument (deallocate, copy, etc.).
	\item \textbf{argument data}\\
The argument data can be allocated by the runtime in a generic way. The data can be accessed dependent on the argument type (semi-typesafe approach).
	\item \textbf{information about the data}\\
The information about the data is referred to using the notion signature. The signature is declared as follows:
\begin{snippet}
typedef struct _ArgumentSignature {
  enum _pIArgumentType type;
  pI_bool readonly;
  union _pI_ArgumentSetup setup;
  pI_str constraints;
} ArgumentSignature;
\end{snippet}
The \textbf{type} field specifies how the argument-specific (f.i. ByteImageArgument or DoubleMatrixArgument) data and meta information can be accessed.\\
The \textbf{readonly} flag tells a plugin if the given argument is allowed to be altered or not.\\
The \textbf{setup} field grants access to argument-specific meta information (f.i. the number of elements in an array).\\
The \textbf{constraint} string allows the specification of type-dependent signature restrictions.
\end{itemize}
In the following, the capabilities of the \pI{} argument system are demonstrated on the example of a simple image type, which is supposed to store its pixel-components byte-wise and to allow gray scale as well as multi-channel images.
\lsubsubsection{Evolution of the ByteImageArgument}
\pI{} is primarily an image processing library, therefore a powerful, yet simple-to-use image type is mandatory. In order to demonstrate the capabilities of the \pI{} argument system, the current image type will evolved in the following.\\


\lparagraph{Argument declaration}\\

For the declaration of new arguments, \pI{} provides a meta-language (similar to the structure declaration mechanism) to setup argument data fields and argument data information. Meta information fields are created using the command \C{SETUP}, whereas data fields are created using commands which contain the notion \C{DATA}. Of course, there is a relationship between the meta information and the data itself:
\begin{snippet}
/* DoubleArrayArgument.def */

SETUP(pI_size, count)

DATA_1D(pI_double, data, count)
\end{snippet}
In this example, an array argument of double precision floating points is specified:
\begin{itemize}
	\item the meta information (\C{SETUP}) contains a variable \C{count} of type \C{pI\_size}, which is meant to specify the array length.
	\item the data itself is declared as one-dimensional field (\C{DATA\_1D}), which contains double precision floating point values (\C{pI\_double}) that can be accessed using the variable \C{data}. The length of the array is dependent on the variable \C{count} from the \C{SETUP} part.
\end{itemize}
In consequence, a user can, by adhering to a dedicated argument workflow, create arbitrary arguments in a generic way, that allows \pI{} to take care of:
\begin{itemize}
	\item argument \textbf{allocation}
	\item argument \textbf{de-allocation}
	\item argument \textbf{copying}
	\item argument \textbf{serialization}
	\item argument \textbf{de-serialization}
\end{itemize}
For instance, if an array of 100 double values is desired, a user can achieve this by simply:
\begin{enumerate}
	\item \textbf{create an argument} of type \textit{DoubleArrayArgument}
	\item \textbf{set the array length} (field count in the signature data) to 100
	\item let the runtime \textbf{create the data} (call to single function \C{CreateArgumentData})
\end{enumerate}


\lparagraph{Image argument, candidate one}\\

The simple image argument has to meet the following requirements:
\begin{itemize}
	\item \textbf{Byte-based image type.}
	\item \textbf{Arbitrary number of channels.}
	\item \textbf{Width, height and number of channels are required meta information.}
\end{itemize}
Using the knowledge of the previous example, this leads to the following argument definition:
\begin{snippet}
/* SimpleImage.def */

SETUP(pI_size, height)
SETUP(pI_size, width)
SETUP(pI_size, channels)

DATA_3D(pI_byte, data, height, width, channels)
\end{snippet}
\begin{itemize}
	\item The meta information consists of three variables: \textbf{height}, \textbf{width} and \textbf{channels}
	\item The data field is a three-dimensional array with the variable name \textbf{data}, its dimensions are dependent on height, width and channels of the meta information
\end{itemize}
To access the individual pixel components, the following code can be used:
\begin{snippet}
simple_image->data.Image->data[h][w][c] = 0;
\end{snippet}
\begin{itemize}
	\item Access the data union: \C{simple_image->data}
	\item Access type dependent data: \C{simple_image->data.Image}
	\item Access pixel components: \C{simple_image->data.Image->data[h][w][c]}
\end{itemize}
The actual memory layout of an image according to this definition is as follows:
\begin{figure}[H]
    \centering
        \includegraphics[width=0.8\textwidth]{Images/SimpleImageOneMemLayout.png}
    \caption{Memory layout of image candidate one}
    \label{fig:SimpleImageOneMemLayout}
\end{figure}
The properties of the memory layout can be summarized as follows:
\begin{itemize}
	\item The image is not stored in a single data block.
	\item There are many small continuous data blocks on lowest dimension (channels).
	\item Padding bytes are inserted in the continuous data blocks (padding is set to 4 bytes).
	\item The memory waste due to padding increases dependent on width and height (linear).
	\item Memory access of non continuous data requires pointer fields.
	\item Higher dimensions are pointer fields.
	\item The additional memory required for the pointer fields increases dependent on width and height (linear).
	\item The allocation of an image requires many allocations of small blocks, with all its consequences.
\end{itemize}


\lparagraph{Image argument, candidate two}\\

The second image argument candidate has additional requirements:
\begin{itemize}
	\item \textit{Byte-based image type.}
	\item \textit{Arbitrary number of channels.}
	\item \textit{Width, height and number of channels are required meta information.}
	\item \textbf{Continuous memory block for the pixel data.}
	\item \textbf{Pointer fields to ease usage} (not strictly necessary, but nice).
\end{itemize}
These requirements are met using a data-block directive in the argument declaration:
\begin{snippet}
/* SimpleImage.def */

SETUP(pI_size, height)
SETUP(pI_size, width)
SETUP(pI_size, channels)

DATA_3D_BLOCK(pI_byte, data, height, width, channels, 3, 1)
\end{snippet}
\begin{itemize}
	\item the \C{BLOCK} variant of the \C{DATA} directive has two additional parameters, the block dimension and the padding dimension (which will be discussed in later candidates). The \C{3} in this example means that \pI{} should create data blocks up to dimension three - in this example, one huge block which clasp all dimensions is created (since we have a three-dimensional array).
\end{itemize}
The actual memory layout of an image according to this definition is as follows:
\begin{figure}[H]
    \centering
        \includegraphics[width=0.8\textwidth]{Images/SimpleImageTwoMemLayout.png}
    \caption{Memory layout of image candidate two}
    \label{fig:SimpleImageTwoMemLayout}
\end{figure}
The properties of the memory layout can be summarized as follows:
\begin{itemize}
	\item The image is stored in a single data block.
	\item Padding bytes are inserted in the continuous data block at the lowest dimension (padding is set to 4 bytes).
	\item The memory waste due to padding increases dependent on width and height (linear).
	\item Memory access of the continuous data is simplified due to pointer fields.
	\item The additional memory required for the pointer fields increases dependent on width and height (linear).
	\item The allocation of an image requires an allocation of one large block, with all its consequences.
\end{itemize}
To access the individual pixel components, there are now two possibilities:
\begin{enumerate}
	\item Using the pointer fields:
\begin{snippet}
simple_image->data.Image->data[h][w][c] = 0;
\end{snippet}
	\item Accessing block memory:
\begin{snippet}
**(arg->data.Image->data)[0..h*w*(c+P)] = 0;
\end{snippet}
Note that block memory access requires the knowledge of the memory layout, therefore the number of inserted padding bytes (\C{P}) is relevant.
\end{enumerate}


\lparagraph{Image argument, candidate three}\\

The third image argument candidate has additional requirements:
\begin{itemize}
	\item \textit{Byte-based image type.}
	\item \textit{Arbitrary number of channels.}
	\item \textit{Width, height and number of channels are required meta information.}
	\item \textit{Continuous memory block for the pixel data.}
	\item \textit{Pointer fields to ease usage} (not strictly necessary, but nice).
	\item \textbf{Reduced memory footage regarding padding}
\end{itemize}
The additional requirement is met by changing the dependency order:
\begin{snippet}
/* SimpleImage.def */

SETUP(pI_size, height)
SETUP(pI_size, width)
SETUP(pI_size, channels)

DATA_3D_BLOCK(pI_byte, data, channels, height, width, 3, 1)
\end{snippet}
\begin{itemize}
	\item Instead of channels, now width is used as lowest dimension.
\end{itemize}
The actual memory layout of an image according to this definition is as follows:
\begin{figure}[H]
    \centering
        \includegraphics[width=0.8\textwidth]{Images/SimpleImageThreeMemLayout.png}
    \caption{Memory layout of image candidate three}
    \label{fig:SimpleImageThreeMemLayout}
\end{figure}
The properties of the memory layout can be summarized as follows:
\begin{itemize}
	\item The image is stored in a single data block.
	\item Padding bytes are inserted in the continuous data block at the lowest dimension (padding is set to 4 bytes).
	\item The memory waste due to padding is much lower than in the previous candidates.
	\item Memory access of the continuous data is simplified due to pointer fields.
	\item The additional memory required for the pointer fields increases dependent on width and height (linear).
	\item The allocation of an image requires an allocation of one large block, with all its consequences.
\end{itemize}
To access the individual pixel components, there are, as before, two possibilities:
\begin{enumerate}
	\item Using the pointer fields:
\begin{snippet}
simple_image->data.Image->data[c][h][w] = 0;
\end{snippet}
	\item Accessing block memory:
\begin{snippet}
**(arg->data.Image->data)[0..c*h*(w+P)] = 0;
\end{snippet}
Note that block memory access requires the knowledge of the memory layout, therefore the number of inserted padding bytes (\C{P}) is relevant.
\end{enumerate}


\lparagraph{Image argument, final candidate}\\

The fourth image argument candidate has additional requirements:
\begin{itemize}
	\item \textit{Byte-based image type.}
	\item \textit{Arbitrary number of channels.}
	\item \textit{Width, height and number of channels are required meta information.}
	\item \textit{Continuous memory block for the pixel data.}
	\item \textit{Pointer fields to ease usage} (not strictly necessary, but nice).
	\item \textit{Reduced memory footage regarding padding}
	\item \textbf{Pixel components must be consecutive} (RGB-like pixel access).
\end{itemize}
The additional requirement is met by a small alteration of the second candidate:
\begin{snippet}
/* SimpleImage.def */

SETUP(pI_size, height)
SETUP(pI_size, width)
SETUP(pI_size, channels)

DATA_3D_BLOCK(pI_byte, data, height, width, channels, 3, 2)
\end{snippet}
\begin{itemize}
	\item This image argument definition sets the padding dimension to two instead: only after \C{channels * width} (the lower two dimensions) bytes the insertion of padding bytes is possible.
\end{itemize}
The actual memory layout of an image according to this definition is as follows:
\begin{figure}[H]
    \centering
        \includegraphics[width=0.8\textwidth]{Images/SimpleImageFourMemLayout.png}
    \caption{Memory layout of image candidate four}
    \label{fig:SimpleImageFourMemLayout}
\end{figure}
The properties of the memory layout can be summarized as follows:
\begin{itemize}
	\item The image is stored in a single data block.
	\item Padding bytes are inserted in the continuous data block after width * channels bytes, since the padding dimension is two.
	\item The memory waste due to padding is much lower than in the first two candidates.
	\item Memory access of the continuous data is simplified due to pointer fields.
	\item The additional memory required for the pointer fields increases dependent on width and height (linear).
	\item The allocation of an image requires an allocation of one large block, with all its consequences.
\end{itemize}
To access the individual pixel components, there are, as before, two possibilities:
\begin{enumerate}
	\item Using the pointer fields:
\begin{snippet}
simple_image->data.Image->data[h][w][c] = 0;
\end{snippet}
	\item Accessing block memory:
\begin{snippet}
**(arg->data.Image->data)[0..h*(w*c+P)] = 0;
\end{snippet}
Note that block memory access requires the knowledge of the memory layout, therefore the number of inserted padding bytes (\C{P}) is relevant.
\end{enumerate}


\lsubsection{Serialization extension}

Another possibility to extend \pI{} regards serialization: any argument can innately be serialized and deserializes by the runtime - a user just needs to define an argument using the meta language described in the previous tutorial section (the meta language enables \pI{} to extract additional information necessary for generic argument operations). Per default, \pI{} uses \JSON{} as serialization language, as shown in the following example (serialization of a test image):
\begin{snippet}
{
 "pI version": 10000,
 "type": { "type class": "argument",
           "subclass id": 12,
           "subclass name": "pI_Argument_ByteImage" },
 "argument description": { "name": "", "description": "" },
 "argument signature": { 
  "type id": 12,
  "type": "pI_Argument_ByteImage",
  "read only": false,
  "constraints": "rows == cols"
 },
 "setup": {
  "height": {
   "type": { "type class": "intrinsic", "subclass id": 3, "subclass name": "pI_size" },
   "value": "5"
  },
  "width": {
   "type": { "type class": "intrinsic", "subclass id": 3, "subclass name": "pI_size" },
   "value": "4"
  },
  "channels": {
   "type": { "type class": "intrinsic", "subclass id": 3, "subclass name": "pI_size" },
   "value": "3" 
  }
 },
 "pitch": {
  "pitch": {
   "type": { "type class": "intrinsic", "subclass id": 3, "subclass name": "pI_size" },
   "value": "16"
  }
 },
 "path": {
  "path": {
   "type": { "type class": "intrinsic", "subclass id": 8, "subclass name": "pI_str" },
   "value": "c:\\foo.gif"
  }
 },
 "data": {
  "type": {
   "type class": "intrinsic",
   "subclass id": 0,
   "subclass name": "pI_byte"
  },
  "data": [
   [
    [ "1", "2", "3" ], [ "1", "2", "3" ], [ "1", "2", "3" ], [ "1", "2", "3" ]
   ],
   [
    [ "1", "2", "3" ], [ "1", "2", "3" ], [ "1", "2", "3" ], [ "1", "2", "3" ]
   ],
   [
    [ "1", "2", "3" ], [ "1", "2", "3" ], [ "1", "2", "3" ], [ "1", "2", "3" ]
   ],
   [
    [ "1", "2", "3" ], [ "1", "2", "3" ], [ "1", "2", "3" ], [ "1", "2", "3" ]
   ],
   [
    [ "1", "2", "3" ], [ "1", "2", "3" ], [ "1", "2", "3" ], [ "1", "2", "3" ]
   ]
  ]
 }
}
\end{snippet}
To change the way serialization is done, one has to:
\begin{itemize}
	\item Provide a serialization / deserialization function pair with a specific signature:
\begin{snippet}
extern pI_str pI_JSON_SerializeArgument (
 struct _CRuntime* runtime,
 struct _Argument* arg,
 pI_int* snlen);

extern struct _Argument* pI_JSON_DeserializeArgument (
 struct _CRuntime* runtime,
 pI_str arg_str,
 pI_int snlen);
\end{snippet}
	\item Bend the funtion pointers of the C runtime to the new function pair:
\begin{snippet}
runtime->SerializeArgument = pI_JSON_SerializeArgument;
runtime->DeserializeArgument = pI_JSON_DeserializeArgument;
\end{snippet}
\end{itemize}
Note however that the implementation of the function pair is not too easy, since it requires mechanisms to iterate arbitrary (even yet unknown) arguments (and structures and intrinsics). For further reading, see the \pI{} implementation for \JSON{} serialization (which uses the \C{JSON-C} library).


\lsubsection{Integration into new target platforms}

As a meta-framework, \pI{} is intended to supply a common interface for multiple problem domains, libraries and algorithms from different environments. As consequence, the integration of \pI{} into new environments must be as easy as possible.\\
There are several steps necessary for this integration:
\begin{itemize}
	\item Definition of desired data types and arguments (\autoref{Enhancing types}).
	\item Generation of C++ adapters for arguments (\autoref{Generation of C++ adapters for arguments}).
	\item Compilation of the runtime with the data types, compilation of the C++ adapters.
	\item Generation of desired language bindings out of the C++ classes using \SWIG{} (\autoref{Generation of language bindings}).
\end{itemize}


\lsubsubsection{Generation of C++ adapters for arguments}

Since we use C++ interfaces as basis for \SWIG{} we need C++ interfaces for the generic arguments, too\footnote{Although several languages with C bindings could work on the C arguments directly, the usage of dedicated class interfaces is more comfortable.}. Luckily, it is possible to generate proper C++ classes from the available meta-information in a generic way. Sadly, \SWIG{} needs physical class definitions, so the C preprocessor cannot be used directly. Therefore, we provide a \Java{} program that extracts proper class definitions from the argument declaration files directly, in a similar way the C X-macros are expanded. In order to make this process as easy as possible, we provide a batch file (\C{pI_cpp_arguments.bat}), which, when executed, takes care of the C++ adapter creation. In the following, the automatically generated C++ adapter for the \C{DoubleArrayArgument}, which was used in previous tutorial sections, will be shown:
\begin{snippet}
namespace pI {

class DoubleArray: public pI::ArgumentAdapter {
    
public:

  DoubleArray(pI::Runtime& runtime);

  DoubleArray(const DoubleArray& arg);

  DoubleArray(const CArgumentPtr c_ptr)
  throw(exception::IncompatibleArgumentException);

  DoubleArray& operator=(DoubleArray arg);
                
  DoubleArray(boost::weak_ptr<Argument> arg)
  throw(exception::IncompatibleArgumentException);  	

  DoubleArray(const CArgumentPtr c_ptr, pI_bool take_ownership)
  throw(exception::IncompatibleArgumentException);
        
  virtual ~DoubleArray();

  virtual DoubleArray& SetConstraints( pI_str value );
    
  virtual DoubleArray& SetName( pI_str value );
    
  virtual DoubleArray& SetDescription( pI_str value );
                    
  virtual DoubleArray& SetReadOnly(pI_bool value);

  virtual DoubleArray& CreateData();

  virtual DoubleArray& FreeData();

    
  pI_size GetCount();

  DoubleArray& SetCount(pI_size value)
  throw(exception::ArgumentException);


  pI_double GetData(pI_size count) 
  throw(exception::ArgumentException);
               
  pI_double* GetDataPtr()
  throw(exception::ArgumentException);

  DoubleArray& SetData(pI_size count, pI_double value);

}; // class DoubleArray

} // namespace pI
\end{snippet}


\lsubsubsection{Generation of language bindings}

\pI{} is a cross-language interface: it makes use of the stable ABI of the C language in order to interconnect different environments via dynamic link libraries. In \pI{}, there are low-level C definitions and implementations for:
\begin{itemize}
	\item plugins
	\item data types
	\item the runtime
\end{itemize}
So using \pI{}, it is possible to use and implement plugins using the C programming language. In addition to C, \pI{} also provides proper interfaces and adapters for C++, which is regarded as main plugin development language. The adapter classes for the generic arguments are generated automatically (\autoref{Enhancing types}), whereas the C++ plugin interface (as well as proper C/C++, C++/C adapters) and the C++ runtime are implemented manually. In general, the plugin development under C++ is way more convenient. Therefore, the C++ interfaces also form the foundations for the \pI{} interfaces in other languages - they are used as input for the interface compiler \SWIG{} \footnote{\url{http://www.swig.org/}}, which is specialized in generating C/C++ wrapper code for (scripting) languages. Though there are possibly two directions:
\begin{itemize}
	\item using \pI{} and \pI{} plugins, implemented in C/C++ from different environments,
	\item and implementing \pI{} plugins in different environments.
\end{itemize}
Only the first can be solved in a quasi-generic way. Establishing the latter direction is quite straight-forward too, though, it requires a good knowledge about inter-platform programming. The reason is, that there is no generic tool like \SWIG{} available for generating adapting code for the opposite direction. A rough estimate of the necessary effort can be made by browsing through the relevant C++ and \Java{} adapters. This is not treated in the tutorial.\\

\lparagraph{Using pureImage from different environments}\\

In order to use \pI{} from different environments, several steps are involved:
\begin{itemize}
	\item Generate the C++ adapters for argument definitions (\autoref{Generation of C++ adapters for arguments})
	\item Compile the \pI{} C++ runtime and its dependencies
	\item Run \SWIG{}.
\end{itemize}

For the latter it is necessary to prepare the platform-dependent wrappers. To do so, one needs knowledge about the target platform and \SWIG{} itself, which is beyond the scope of this tutorial.
For an easy start, we recommend to copy and manually edit the relevant files of our \Java{} implementation, which can be found under \C{include/Adapters/Java}. The root \SWIG{} input file is \C{pI_java.i}. There are more \SWIG{} files referenced from this root file. Some of them are platform independent and can be found in \C{include/Adapters/Swig}. Others contain code snippets which are written in the target language and address various tasks: cross-platform exception handling,
type conversions to platform built-in types, polymorphic factories, etc.
We also recommend to copy and edit a certain batch file (e.g., \C{build/swig_java_gnu.bat}) to enable one-click generation of the \SWIG{} files.\\

\lparagraph{Implementing pureImage plugins in different languages}\\

Creating mechanisms that allow development of \pI{} plugins in a different language unfortunately is quite an inconvenient task as there is (probably) no code generator tool like \SWIG{} available for the opposite direction, e.g., for adapting Java code for usage in C++. Though, it is quite straight-forward. Platform specific knowledge about cross platform interfacing is necessary, which can not be covered by this tutorial.

At a glance, the following steps are necessary:
\begin{itemize}
\item Create a plugin interface in the target language as straight-forward transfer of the C++ class \C{pIn}.
\item Create a C++ implementation that uses cross-platform interface code to delegate the C++ interface to an implementing class in the target platform.
\item Map exceptions from the target platform to C++.
\item Create a factory that
	\begin{itemize}
		\item starts the target platform (e.g., Java Virtual Machine),
		\item creates instances of plugins in the target platform.
	\end{itemize}
\end{itemize}

Additionally, you can provide a mechanism to generalize the factory by allowing
configuration by means of ini-files, for instance. To have an easier start, we recommend to have a look at the core project \C{pIRuntimeLibJava} (\C{pureImage/build/VC9/pIRuntimeLibJava}) and the plugin project \C{JavaAdapter} (\C{Plugins/JavaAdapter}). The first implements the delegation from C++ to a Java plugin and the latter is a rich factory implementation that takes care of starting the Java Virtual Machine and automatically looks up Java plugins implementations from jar-files.

\lsubsection{Enhancing the constraint parser}

The constraint parser enables the validation of preconditions certain arguments have to meet: images with fixed dimensions or number of channels, polygons of fixed sizes, etc.. The constraint system is based on strings, any variable in the type-dependent signature union of a C argument (width, height, channels of an image, number of elements in an array, ...) can be evaluated during runtime. The signature symbols the constraint parser can cope with are extracted directly from the argument definition (using X-macros), therefore the mechanism is available to new arguments from scratch, there is no need to code even a single line of code (for the parser symbols).\\
At the moment, the constraint parser supports:
\begin{itemize}
	\item types:
		\begin{itemize}
			\item constants of types: byte, int, float, string
			\item variables (setup symbols from arguments), with support for structure types
			\item implicit type-casting of operand values
		\end{itemize}
	\item operations:	
		\begin{itemize}
			\item Arithmetic comparison: \C{==}, \C{!=}, \C{<}, \C{>}, \C{<=}, \C{>=}
			\item Boolean expressions: \C{&&}, \C{||}, \C{!}
		\end{itemize}
	\item nested expressions (using parenthesis)
\end{itemize}
It is possible to extend the symbols, the grammar and the semantics of the constraint parser:


\lsubsubsection{Enhancing the constraint parser symbols}

When a given constraint string must be evaluated, it is first handed to the lexical analyzer, which recognizes text patterns. The scanner in \pI{} is generated using \Flex{} \footnote{\url{http://www.cs.princeton.edu/~appel/modern/c/software/flex/flex_toc.html}}, a tool that automatically generates source code for scanners using an input file as description of the syntax. In principle, it is possible to add additional scanner symbols by extending the input file (\C{PicScanner.l}). After editing, one has to:
\begin{enumerate}
	\item also enhance the parser (\autoref{Enhancing the constraint parser grammar}) and the evaluator (\autoref{Enhancing the constraint parser semantics}) if new tokens were introduced.
	\item execute \Flex{} on the input file (a proper batch file is provided (\C{gen_scanner.bat})).
	\item recompile the C runtime of \pI{} (since the C runtime contains the scanner), and all dependencies.
\end{enumerate}


\lsubsubsection{Enhancing the constraint parser grammar}

With the availability of a lexical analyzer, it is possible to use its output as input for a parser, which is responsible to syntactically analyze the scanner-tokens. After parser execution a symbol tree is available, which can easily be evaluated.\\
The parser in \pI{} is generated using \Bison{} \footnote{\url{http://www.gnu.org/software/bison/}}, a tool that automatically generates source code for parsers using an input file as description of the syntax.\\
When extending the parser, one has to:
\begin{itemize}
	\item specify the priorities of logical operations
	\item specify the priorities of boolean expressions
\end{itemize}
In order to do so, one has to edit the input file (\C{PicParser.y}) and perform the following steps:
\begin{enumerate}
	\item also enhance the scanner (\autoref{Enhancing the constraint parser symbols}) and the evaluator (\autoref{Enhancing the constraint parser semantics}) in case new tokens were introduced.
	\item execute \Bison{} on the input file (a proper batch file is provided (\C{gen_parser.bat})).
	\item recompile the C runtime of \pI{} (since the C runtime contains the parser), and all dependencies.
\end{enumerate}

\lsubsubsection{Enhancing the constraint parser semantics}

After the parser went through the scanner tokens, it is necessary to evaluate its output - the operations in the constraint string are finally executed. The source file responsible for the evaluation is \C{src/Runtime/Internal/ConstraintParser/StackEval.c}, it contains the implementation details for the available operations.
