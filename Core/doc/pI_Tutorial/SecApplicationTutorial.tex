\lsection{pureImage application developer tutorial}

\lsubsection{Dynamic link libraries}
The usage of dynamic link libraries for image plugins brings the benefit of decoupling application logic and, in our case, image processing algorithms. Applications must provide a DLL loading mechanism, plugins must export their functionality to a dedicated DLL signature.
\lsubsubsection{Plugin factory}
In the presented image processing framework, DLLs must provide a dedicated function to allow plugin instantiation, an example is shown in \autoref{code:DLLFactory}. This function is able to create all image processing functions, which are exported to the DLL, \emph{on the dll side}. Because of runtime restrictions, these instances must be deleted at the same place creation took place, which is automatically done by the framework on instance deletion. An application can make use of the creation function to create multiple instances of the same type.
\begin{code}[caption=DLL factory example,label=code:DLLFactory, breaklines=true,numbers=left]
extern "C" pI_API pI_bool CreatepIns (pI_int index, struct _CRuntime* runtime, struct _CpIn** plugin) {

  if ((runtime == 0) || (index < 0))
    return pI_FALSE;

  // create a plugin if it has not been created already
  switch (index) {
    EXPORT_PIN_SC (0, runtime, pI::pIns::FreeImageReader);
    EXPORT_PIN_SC (1, runtime, pI::pIns::FreeImageWriter);
    default:
      return pI_FALSE;
  }
}; /* extern "C" pI_API pI_bool CreatepIns (pI_int index, struct _CRuntime* runtime, struct _CpIn** plugin) */

\end{code}
\lsubsubsection{Loading plugins from different DLLs}
Because plugins may be dependent on other plugins right from plugin construction - in case the foreign dependency cannot be sated, plugin construction fails -, these dependencies must be considered in the DLL loading routine.\\
Therefore, we provide a dynamic link library load mechanism in the \C{Application} class, which is part of the C++ adapter framework. It takes care of the plugin dependenciesm its usage is shown in the following code snippet:
\begin{snippet}
  pI::Application app(runtime);
  pI_str plugin_dir("../../../libraries");
  // load plugins
  app.LoadPluginLibraries (plugin_dir);
\end{snippet}
In essence, our mechanism tries to instantiate all available plugins in all found DLLs over and over again, until the number of successfully created instances does not change in comparison to the successful instantiations of the last iteration. This way, it is ensured that failed plugin creations due to foreign plugin dependencies are no issue for any \pI{} application.\\
\textnote{this approach is a rather simple solution to the problem that plugins from one DLL are dependent on plugins from another DLL and vice versa. Another possible solution is the manual determination of the plugin instantiation order, which is inconvenient and error-prone.}

\lsubsection{Resource management}
In our framework, the application must take care of the resource management\footnote{Resource managment in this case refers to memory management}:
\begin{itemize}
    \item Decoupling of plugin and argument lifetimes.
    \item Allocation and deallocation on the same side of the application boundary.
    \item Avoiding memory leaks by proper data deallocation.
\end{itemize}
Both image plugins and arguments are subject to the application resource management.

\lsubsubsection{Plugin creation and deletion}
As stated before, it is imperative to place plugin creation and deletion on the same side of the application boundary. Plugin creation typically takes place within the DLL factory, the \pI{} framework takes care that plugin destruction is done on the same side.

\lsubsubsection{Argument / parameter creation and deletion}
The same is true for any data which is passed from one plugin to another. Argument data on the other hand is typically created on the application side\footnote{more precisely where the runtime resides - the runtime may be exported to a dynamic link library, too.}, where it is deleted from, too.\\
Fortunately, an application developer does not have to implement this functionality all by himself - the presented framework provides all that is needed within the plugin runtime (\autoref{Runtime}).

\lsubsection{Runtime}
The image plugin runtime is the part of the presented framework which greatly simplifies application development. At the moment, the runtime implementation offers the following relevant functionality:
\lsubsubsection{Argument and plugin creation and deletion}
Arguments, which are created, returned and used by multiple plugins, are \emph{always} allocated using the runtime, which has several benefits:
\begin{itemize}
    \item Argument and plugin lifetimes are decoupled, which is especially important because of the DLL boundaries.
    \item Simplified debugging and memory profiling, because of central allocation and deallocation.
    \item There is no need to track the resource owner - it is always the runtime.
    \item The argument creation methods simplify argument usage: a plugin developer can, for example, create a 10 x 10 floating point matrix with just one method call.
    \item The argument deletion method takes care of the non-residue deletion of arguments with arbitrary type - the usage of argument creation and deletion methods prevents argument-related memory leaks.
\end{itemize}

\lsubsubsection{Plugin registration mechanism}
The usage of foreign plugins requires the availability of a central plugin registrar. The runtime provides this plugin registration mechanism, the application developer can register any plugin instance to the runtime, which is subsequently available to other plugins. Typically, a plugin is instantiated using the DLL factory (\autoref{Plugin factory}) and immediately registered to the runtime, so that all plugins are available for the other plugins. It is also possible to register a plugin with a specific state - subsequent instantiations of this plugin will yield object copies instead of newly created, but empty objects. At any time, the application can deregister a plugin.

\lsubsection{Plugin execution}
When an application is going to execute a plugin, it has to adhere to the plugin lifecycle (\autoref{Plugin construction, initialization and execution}): a plugin must first be constructed (normally using one of the plugin creation methods of the runtime) and afterwards initialized with the proper parameters (the expected parameters can be queried right after construction). After the initialization, the plugin can be executed with the set of expected arguments (which can be queried after its initialization - the input and output signature can change depending on the initialization parameters).

\lsubsection{Exception handling}
An application must guard from exceptions thrown by plugins: a plugin may throw exceptions on construction (out of memory, missing dependency, etc.), on initialization (out of memory, missing dependency, incomplete / incompatible parameter set, etc.), on execution (out of memory, missing dependency, incomplete / incompatible input or output arguments, operation exception, not initialized exception, etc.) and on other method calls (\textbf{Create}, \textbf{Clone}, \textbf{Serialize}, \textbf{Deserialize}, etc.). The list of possible exceptions is shown in \autoref{code:ImagePluginException}.

%\lsubsection{Plugin composition}
%\lsubsubsection{Serial execution}
%\lsubsubsection{Parallel execution}
%\lsubsubsection{Execution branches}
%
\lsubsection{Control considerations}
The distinction between parameter and argument is on a pure semantic level: parameters are used to initialize a plugin, arguments are used as inputs and outputs for the actual processing - the underlying datatypes are identical. As consequence, it is possible to perform regulation and control tasks.

\lsubsubsection{Control - usage of plugin output as plugin parameter}
It is possible for an application or even a plugin which calls foreign plugins to use the output of some plugin as initialization parameter for another plugin. The normal restrictions regarding type safety apply - the type of the output argument must correspond to the type of the parameter.

\lsubsubsection{Regulation - usage of plugin output as input for the same plugin}
It is also possible to use a plugin output argument as input argument for subsequent plugin executions. In other words, loop-backs are supported. There are two ways a plugin may implement regulation: first, it may export an input and an output argument of the same type, which may be connected by the application or another plugin. Second, it may export reference-style arguments - input arguments, which are not read-only.

\lsubsection{Persistation}
At some point, an application using our framework may have to store its actual state, plugin states or even plugin results to the harddisk. These persistation tasks are addressed by \pI{}, as shown in the following.

\lsubsubsection{Argument persistation}
Any argument created by the \pI{} Runtime can be serialized and deserialized. Although the serialization mechanism is changeable during runtime (by setting up certain function pointers of the runtime), the provided method (\JSON{}) should suit many needs: 
\begin{itemize}
    \item Human readable / editable.
    \item It is intuitive - a matrix actually looks like a matrix.
    \item Many available tools for better visualization.
    \item Smaller overhead than other protocols.
\end{itemize}
Of cource, there are also shortcomings:
\begin{itemize}
    \item Larger than binary format.
    \item Dependency on 3rd party library (json-c).
\end{itemize}

\lsubsubsection{Plugins}
A plugin also provides a serialization and a deserialization method, although neither may actually do something: typically only stateful plugins offer implementations. Note that plugins possibly have three state dimensions (\autoref{Plugin states}).

\lsubsubsection{Execution graph}
An image processing application may allow the execution of compound image processing plugins, which may contain concurrent plugin executions or even branches. The \pI{} framework does not facilitate the graphical composition of image plugin graphs, neither has it an integrated script language - we concluded that this functionality is application specific and therefore should be provided there. On the other hand, such capabilities are rather easy developed for applications:
\begin{itemize}
    \item The whole image plugin workflow fits perfectly for compound plugin execution, which is already available for plugin developers from C or C++ context.
    \item There exists a proof-of-concept, yet fully functional graph image plugin, which may be used as code example or even as actual implementation.
    \item Plugin spawning and memory management functionality is done by the provided runtime implementation.
\end{itemize}

\lsubsection{Languages}
The \pI{} framework was developed having C++ as plugin development language in mind. Because C++ lacks a common ABI\footnote{application binary interface}, plugins are exported as DLLs using a C interface\footnote{It is of course possible to also implement plugins using the (less convenient) C interface.}, which has multiple benefits:
\begin{itemize}
    \item Stable ABI, which brings independence from particular compiler vendors and build configurations.
    \item Bindings to many high-level programming languages (C++, \Java{}, Python, \Matlab{} etc.).
\end{itemize}
Therefore, adapter implementations in order to export a C++ plugin to a C plugin interface and to use a C plugin as C++ plugin in a convenient way are necessary.

\lsubsubsection{C++ to C adapter}
\begin{figure}[H]
    \centering
        \includegraphics[width=0.6\textwidth]{Images/DIBL_Adapters.png}
    \caption{Adapter framework}
    \label{fig:Adapters}
\end{figure}
As shown in \autoref{fig:Adapters}, \pI{} has a C++ to C adapter which transforms a C++ image plugin to a C image plugin - it is an adapter which creates a C image plugin. It is declared in \C{CpInAdapter.hpp}. There are two variants:
\begin{itemize}
    \item The templatized \C{CpInAdapter} class, which creates a C image plugin out of an internal C++ class instance of the template type - hence the actual type must be known at compile time. This variant is typically used for DLL exports. Besides the template argument, also a valid runtime instance must be given to the construction method (shown in \autoref{code:CpInAdapter}).
\begin{code}[caption=CpInAdapter constructor (extract from CpInAdapter.hpp),label=code:CpInAdapter, breaklines=true,numbers=left]
template<class T>
class CpInAdapter: public CpInAdapterBase {

public:

  static const CpInAdapter* Create(Runtime& runtime);
\end{code}
    \item The \C{CpInCloningAdapter} class, which creates a C image plugin out of an existing C++ class instance of arbitrary and possibly unknown type - hence the actual type does not have to be known at compile time. This variant is primary used internally by the framework\footnote{especially for Cloning purposes, hence the name}, but the functionality may also be exploited by application developers for various reasons. This class is not dependent on a template parameter, but the construction method (shown in \autoref{code:CpInCloningAdapter}) expects a valid C++ image plugin instance, and a flag to tell if the adapter should take ownership of the C image plugin and therefore delete it on destruction.
\begin{code}[caption=CpInCloningAdapter constructor (extract from CpInAdapter.hpp),label=code:CpInCloningAdapter, breaklines=true,numbers=left]
class CpInCloningAdapter: public CpInAdapterBase {

public:

	static const CpInCloningAdapter* Create(pInPtr instance);
\end{code}
\end{itemize}

\lsubsubsection{C to C++ adapter}
As shown in \autoref{fig:Adapters}, there is also a C to C++ adapter which transforms a C image plugin to a C++ image plugin - it is an adapter which creates a C++ image plugin. The class constructor, which is shown in \autoref{code:pInAdapter}, expects a valid C++ image plugin instance, and a flag to tell if the adapter should take ownership of the C image plugin and therefore delete it on destruction.
\begin{code}[caption=pInAdapter constructor (extract from pInAdapter.hpp),label=code:pInAdapter, breaklines=true,numbers=left]
class pInAdapter: public pIn {

public:

  pInAdapter(
   const CpInPtr c_pin,
   const pI_bool take_ownership = pI_FALSE);
\end{code}

\lsubsubsection{C arguments}
One major shortcoming of this approach is the dependency on C POD type arguments - even C++ image plugins must use C type arguments, which are somewhat inconvenient to use - they do not possess a constructor, destructor, copy constructor or assignment operator, they cannot be inherited from, and they involve lots of unions and structs. In fact, all arguments are encoded in the same struct by using unions and type ids, which determine how the arguments must be interpreted. Every supported data type involves a struct holding the actual data and a struct for the meta information.\\
Therefore, specific adapters for the C arguments are provided to simplify the usage. These adapters can be generated automatically from the argument definition files for different languages, including C++. The C++ adapters involve mechanisms for memory management (automatic deletion in C++ due to smart pointers), they supply type-dependent data accessors and allow chaining.

\lsubsubsection{Additional bindings}
\pI{} can be seen as meta-framework for different image processing algorithms of various environments. Because these environments may also profit from the availability of \pI{}, we provide a \SWIG{} template for the deployment of \pI{} on these platforms. From scratch, it is possible to use \pI{} in C, C++ and Java environments and applications.
