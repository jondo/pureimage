#!/bin/bash

echo $1
echo $2

# The first argument is interpreted as a sequence of build flags
if [ -z "$1" ]; then FLAGS="cpe"; else FLAGS="$1"; fi

echo $FLAGS

# If FLAGS contain the character
#   'm': minimal configuration (ignores all other flags)
#   'c': add core 
#   'p': add plugins
#   'e': add examples
#   'd': add documentation
# Default is cpe, i.e. core, plugins, and examples without documentation

if expr index "$FLAGS" m != 0
then
    _M="-D ADD_PI_MINIMAL=ON"
    FLAGS="m"
    echo   minimal configuration
else
    _M="-D ADD_PI_MINIMAL=OFF"
fi

if expr index "$FLAGS" c != 0
then
    _C="-D ADD_PI_CORE=ON"
    echo   with core
else
    _C="-D ADD_PI_CORE=OFF"
    echo   no core
fi

if expr index "$FLAGS" p != 0
then
    _P="-D ADD_PI_PLUGINS=ON"
    echo   with plugins
else
    _P="-D ADD_PI_PLUGINS=OFF"
    echo   no plugins
fi

if expr index "$FLAGS" e != 0
then
    _E="-D ADD_PI_EXAMPLES=ON"
    echo   with examples
else
    _E="-D ADD_PI_EXAMPLES=OFF"
    echo   no examples
fi

if expr index "$FLAGS" d != 0
then
    _D="-D ADD_PI_DOCUMENTATION=ON"
    echo   with documentation
else
    _D="-D ADD_PI_DOCUMENTATION=OFF"
    echo   no documentation
fi

# If the second argument equals "debug", then so be it.
if [ "$2" = "debug" ]
then 
    _DEBUG="-D CMAKE_BUILD_TYPE=Debug"
    mkdir -p build-makefiles-debug
    cd build-makefiles-debug
else 
    _DEBUG=""
    mkdir -p build-makefiles
    cd build-makefiles
fi

cmake $_M $_C $_P $_E $_D $_DEBUG -G "Unix Makefiles" ..

cd ..

