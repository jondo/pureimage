# Shared CppRuntime and CppBaseApplication libraries (feature #207)
# require at least CMake 2.8.4, see issue #233.

cmake_minimum_required(VERSION 2.8.4) 

#
#   Configuration can be controlled with several boolean variables.
#   Pass to cmake either on command line using '-D<Var>=ON|OFF'
#   or set in an initial cache-file using CMake syntax.
#
# Options to control which subdirectories to add to the generated solution:
#   ADD_PI_MINIMAL (default=OFF):       add only a minimal set of plugins and applications
#   ADD_PI_CORE (default=ON):           add projects to build the pureImage core 
#   ADD_PI_PLUGINS (default=ON):        add projects to build all current pureImage plugins 
#   ADD_PI_EXAMPLES (default=ON):   	add projects to build all current pureImage examples
#   ADD_PI_DOCUMENTATION (default=OFF): add projects to generate tutorial, documentation, and readme
#
# Further options:
#   USE_SOLUTION_FOLDERS (default=OFF):   enable virtual project folders in Visual Studio Professional
#   SWIG_ENABLED (default=ON):            enable swig related projects (is disabled if swig can not be found)
#   PI_JAVA_ENABLED (default=ON):         enable pureImage Java bindings and dependent projects (is disabled if Java and JNI can not be found)
#	CREATE_PI_CONSTRAINT_PARSER (default=OFF):	if enabled, the constraint parser (+ scanner) is generated each built, otherwise pre-generated code is used
#

set_property(DIRECTORY ${PROJECT_SOURCE_DIR} APPEND PROPERTY COMPILE_DEFINITIONS_DEBUG "_DEBUG")

# NOTE: this feature is described in the CMake documentation, but currently it has not effect with generator for VS 2008
option(USE_SOLUTION_FOLDERS "Solution folders are only supported by Visual Studio Professional" OFF)
set_property(GLOBAL PROPERTY USE_FOLDERS ${USE_SOLUTION_FOLDERS})

include(CMakeDependentOption)

option(CREATE_PI_CONSTRAINT_PARSER       	"Turn ON to enable constraint parser build process." OFF)
option(ADD_PI_MINIMAL       				"Turn ON to add only a minimal set of plugins and applications." OFF)
CMAKE_DEPENDENT_OPTION(ADD_PI_CORE          "Turn ON to add the pureImage core." ON "NOT ADD_PI_MINIMAL" OFF)
CMAKE_DEPENDENT_OPTION(ADD_PI_PLUGINS       "Turn ON to add all current pureImage plugins." ON "NOT ADD_PI_MINIMAL" OFF)
CMAKE_DEPENDENT_OPTION(ADD_PI_EXAMPLES  	"Turn ON to add all current pureImage examples." ON "NOT ADD_PI_MINIMAL" OFF)
CMAKE_DEPENDENT_OPTION(ADD_PI_DOCUMENTATION "Turn ON to add tutorial, documentation, and readme." OFF "NOT ADD_PI_MINIMAL" OFF)

CMAKE_DEPENDENT_OPTION(ADD_PLUGIN_FREENECT  "Turn ON to add Freenect related projects." ON "NOT ADD_PI_MINIMAL" OFF)
CMAKE_DEPENDENT_OPTION(ADD_PLUGIN_PCL       "Turn ON to add PCL related projects." OFF "NOT ADD_PI_MINIMAL" OFF)

if(WIN32)
    # note: under windows this is more important to override win registry
    OPTION(USE_PATCHED_FIND_JNI "Use patched JNI module that allows to control search with JAVA_HOME" ON)
else()
    OPTION(USE_PATCHED_FIND_JNI "Use patched JNI module that allows to control search with JAVA_HOME" OFF)
endif()

if(${USE_PATCHED_FIND_JNI})
    set(_PATCHED_FIND_JNI_DIR ${CMAKE_CURRENT_SOURCE_DIR}/Tools/cmake-java/FindJNI)
    set(CMAKE_MODULE_PATH ${_PATCHED_FIND_JNI_DIR} ${CMAKE_MODULE_PATH})
endif()

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/Tools/cmake)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/Tools/cmake-java)

if(WIN32)
    set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/Tools/cmake-pcl)
endif()

project(pureImage)

# Pre-Requisites:
# ===============

# C preprocessor
# --------------

if(MSVC)
    set(CPP_COMMAND cl)
else()
    find_program(CPP_COMMAND cpp PATHS ENV PATH)
    if(${CPP_COMMAND-NOTFOUND})
        message(FATALERROR "Could not find cpp")
    endif()
endif()

if(CMAKE_COMPILER_IS_GNUCXX)
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fvisibility=hidden")
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fvisibility=hidden")
endif()

# Boost:
# ------
find_package(Boost REQUIRED) # For custom locations define BOOST_ROOT when calling CMake
mark_as_advanced(Boost_LIB_DIAGNOSTIC_DEFINITIONS)
if (${Boost_FOUND})
    set(BOOST_VERSION_COMPARABLE ${Boost_MAJOR_VERSION}.${Boost_MINOR_VERSION}.${Boost_SUBMINOR_VERSION} CACHE INTERNAL "")
else()
    message(WARNING "Could not find Boost. Need to be configured manually.")
    set (BOOST_ROOT "" CACHE FILEPATH "path to boost")
endif()

# Java:
# -----
set(PI_JAVA_ENABLED ON CACHE BOOL "") # default to ON
if(PI_JAVA_ENABLED)
    find_package(Java REQUIRED)

    if(EXISTS ${Java_JAVAC_EXECUTABLE})
        message(STATUS "   Using Java: ${Java_JAVAC_EXECUTABLE}")
        include(UseJava)
    else()
        message(WARNING "Could not find JDK in path. All Java related targets are deactivated.")
        set(PI_JAVA_ENABLED OFF CACHE BOOL "" FORCE)
    endif()
endif()

# Json-C:
# -------
# TODO: We should write (or get) a find module instead.
# (see http://www.itk.org/Wiki/CMake:How_To_Find_Libraries#Writing_find_modules)
if(WIN32)
    set (JSONC_ROOT "" CACHE FILEPATH "from http://oss.metaparadigm.com/json-c/")
    set(JSONC_LIBRARY JsonC)
    set(JSONC_INCLUDE_DIR ${JSONC_ROOT})
else()
    find_library(JSONC_LIBRARY json)
    find_path(JSONC_INCLUDE_DIR json.h PATHS /usr/include/json/)
    set(JSONC_INSTALLED ON)
endif()

if(JSONC_LIBRARY AND JSONC_INCLUDE_DIR)
    message(STATUS "---- Found " ${JSONC_LIBRARY})
    message(STATUS "---- Found " ${JSONC_INCLUDE_DIR})
#else()
#   message(FATAL_ERROR "Could not find JsonC")
endif()

# Poco:
# -------
# TODO: We should write (or get) a find module instead.
# (see http://www.itk.org/Wiki/CMake:How_To_Find_Libraries#Writing_find_modules)
if(WIN32)
    set(POCO_ROOT "" CACHE PATH "from http://pocoproject.org/")
    set(POCO_LIBRARY_DIRS "${POCO_ROOT}/lib")
    set(POCO_INCLUDE_DIRS ${POCO_ROOT}/Foundation/include)
    set(POCO_LIBRARIES "")
else()
    # TODO: find poco lib
    set(POCO_LIBRARY_DIRS /usr/lib)
    set(POCO_INCLUDE_DIRS /usr/include/Poco)
    set(POCO_LIBRARIES PocoFoundation)
endif()

if(ADD_PI_MINIMAL OR ADD_PI_CORE)

    # Swig:
    # -----
    # swig is used to generated pureImage language bindings for other languages (e.g., Java)
    if( NOT EXISTS ${SWIG_EXECUTABLE} )
        # try to find swig executable in path if it has not been set as global cache variable
        find_program( SWIG_EXECUTABLE swig2.0 PATHS ENV PATH )
    endif()
    
    if( NOT EXISTS ${SWIG_EXECUTABLE} )
        message(WARNING "Could not find swig in path. All swig related targets are deactivated.")
        set (SWIG_ENABLED OFF CACHE BOOL "")
    else()
        message(STATUS "   Using SWIG: ${SWIG_EXECUTABLE}")
        set (SWIG_ENABLED ON CACHE BOOL "")
    endif()
    mark_as_advanced(SWIG_EXECUTABLE)

endif()    
    
# JNI:
# ----
# jni is necessary for the pureImage java bindings

find_package(JNI REQUIRED)
message(STATUS "   Using JNI: ${JNI_INCLUDE_DIRS}")
if (NOT ${JNI_FOUND})
    message(WARNING "Could not find JNI headers and binaries (installed with JDK). Disabled pureImage for Java")
    set(PI_JAVA_ENABLED OFF CACHE BOOL "")
else()
    set(PI_JAVA_ENABLED ON CACHE BOOL "")
endif()
include(UseGlob)

if(ADD_PI_DOCUMENTATION)

    # Pandoc:
    # -------
    if(NOT EXISTS ${PANDOC_EXECUTABLE})
        find_program(PANDOC_EXECUTABLE pandoc)
        if(NOT EXISTS ${PANDOC_EXECUTABLE})
            message(FATAL_ERROR "Could not find pandoc used for document generation. Install pandoc, add to PATH, or set cache variable PANDOC_EXECUTABLE.")
            return()
        endif()
    endif()

    # Doxygen:
    # -------
    if(NOT EXISTS ${DOXYGEN_EXECUTABLE})
        find_program(DOXYGEN_EXECUTABLE doxygen)
        if(NOT EXISTS ${DOXYGEN_EXECUTABLE})
            message(FATAL_ERROR "Could not find doxygen used for documentation generation. Install doxygen, add to PATH, or set cache variable DOXYGEN_EXECUTABLE.")
            return()
        endif()
    endif()

endif()

# Internal Variables:
# ===================
# this is for convenience in sub projects
set(PI_ROOT_DIR ${CMAKE_CURRENT_SOURCE_DIR})
set(PI_CORE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/Core)
set(PI_THIRD_PARTY_DIR ${CMAKE_CURRENT_SOURCE_DIR}/Core/external)

# This self-made cmake macro file creates "libraries" folder and provides a macro 'copy_to_dist(target)'
include(DistFolder)

include(pITemplate)

# Use pre-expanded datatype headers
if(${USE_EXPANDED_DATATYPES})
    add_definitions(-DUSE_EXPANDED_DATATYPES)
endif()

# Prepare the location for 3rd part shared libraries
set(LIBRARIES ${CMAKE_BINARY_DIR}/libraries)
file(MAKE_DIRECTORY ${LIBRARIES})

# Sub-Configurations:
# ===================

if(ADD_PI_MINIMAL OR ADD_PI_CORE)
    add_subdirectory(${PI_THIRD_PARTY_DIR})
    add_subdirectory(Core/src/Runtime)
    add_subdirectory(Core/include/DataTypes/generator)
    add_subdirectory(Core/src/Adapters/C++)
    add_subdirectory(Core/src/Applications/C++)
    if(PI_JAVA_ENABLED)
        add_subdirectory(Core/src/Adapters/Java)
    endif()    
endif()

if(ADD_PI_MINIMAL)
    add_subdirectory(Plugins/CImg)
    add_subdirectory(Plugins/OpenCV) 
    if(PI_JAVA_ENABLED)
        add_subdirectory(Plugins/JavaAdapter)
    endif()    
elseif(ADD_PI_PLUGINS)
    add_subdirectory(Plugins/CImg)
    add_subdirectory(Plugins/FLLL)
    if(ADD_PLUGIN_FREENECT)
        add_subdirectory(Plugins/Freenect)
    endif()
    add_subdirectory(Plugins/OpenCV)
    if(ADD_PLUGIN_PCL)
        add_subdirectory(Plugins/PCL)
    endif()
    add_subdirectory(Plugins/Tesseract)

    if(PI_JAVA_ENABLED)
        add_subdirectory(Plugins/JavaAdapter)
        add_subdirectory(Plugins/JavaAdapter/pIns)
    endif()

endif()

if(ADD_PI_EXAMPLES)
	add_subdirectory(Examples/Basics/C++)
	add_subdirectory(Examples/Demos/C++)
	add_subdirectory(Examples/Demos/Java)
	add_subdirectory(Examples/Tools/C++)
	add_subdirectory(Examples/Viewer/C++)
endif()

if(ADD_PI_MINIMAL OR ADD_PI_EXAMPLES)
# Let the examples run in the build directory
execute_process(
    COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/Examples/Resources ${CMAKE_CURRENT_BINARY_DIR}/Examples/Resources
)
endif()

if(ADD_PI_DOCUMENTATION)
    add_subdirectory(Core/doc)
    add_subdirectory(Core/doc/pI_Tutorial)
    add_subdirectory(Core/doc/plugins)
endif()
